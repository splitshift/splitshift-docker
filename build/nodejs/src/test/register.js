var request = require('request');
var expect = require('chai').expect;
var url = 'http://localhost:5000/api/register';
require('../db');
var mongoose = require('mongoose');
var User = mongoose.model('User');

describe('Register API', function() {
  afterEach(function(done) {
    User.remove({
      email: 'mike@mail.com'
    },
    function(err) {
      if(err) console.log(err);
      done();
    });
  });

  it('candidate register', function(done) {
    request.post({
      url: url,
      json: {
        first_name: 'Mike',
        last_name: 'Pani',
        email: 'mike@mail.com',
        password: '123456',
        type: 'Candidate',
        register_type: 'Simple'
      }
    }, function(err, response, body) {
        expect(response.statusCode).to.equal(201);
        expect(body.token).to.be.ok;
        User.find({
          email: 'mike@mail.com'
        },
          function(err, users) {
            if(err) console.log(err);
            expect(users).not.to.be.empty;
            done();
          });
    });
  });

  it('hr register', function(done) {
    request.post({
      url: url,
      json: {
        name: 'Mikelopster',
        email: 'mike@mail.com',
        password: '123456',
        type: 'HR',
        register_type: 'Simple'
      }
    }, function(err, response, body) {
        expect(response.statusCode).to.equal(201);
        expect(body.token).to.be.ok;
        User.find({
          email: 'mike@mail.com'
        },
          function(err, users) {
            if(err) console.log(err);
            expect(users).not.to.be.empty;
            done();
          });
    });
  });

  it('duplicated email', function(done) {
    request.post({
      url: url,
      form: {
        first_name: 'Mike',
        last_name: 'Pani',
        email: 'mike@mail.com',
        password: '654321',
        type: 'Candidate',
        register_type: 'Simple'
      }
    }, function(err, response, body) {
      request.post({
        url: url,
        form: {
          first_name: 'Pani',
          last_name: 'Tani',
          email: 'mike@mail.com',
          password: '654321',
          type: 'Candidate',
          register_type: 'Simple'
        }
      }, function(err, response, body) {
          expect(response.statusCode).to.equal(409);
          done();
      });
    });
  });

  it('missing some fields', function(done) {
    request.post({
      url: url,
      form: {
        first_name: 'Mike',
        last_name: 'Pani',
        email: '',
        password: '654321',
        type: 'Candidate',
        register_type: 'Simple'
      }
    }, function(err, response, body) {
        expect(response.statusCode).to.equal(400);
        done();
    });
  });
});
