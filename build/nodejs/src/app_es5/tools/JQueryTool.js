"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var showDrawer = false;
var showSidebar = false;

var JQueryTool = {
  triggerUpload: function triggerUpload(name, evt) {
    console.log("Uploda Click");
    $("#" + name).click();
  },
  getMonth: function getMonth(date) {
    return (0, _moment2.default)(date).format("MM");
  },
  getYear: function getYear(date) {
    return (0, _moment2.default)(date).format("YYYY");
  },
  changeDateFormat: function changeDateFormat(date) {
    var lang = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'en';

    _moment2.default.locale(lang);
    return (0, _moment2.default)(date).add('years', lang === 'th' ? 543 : 0).format("MMMM YYYY");
  },
  changeDatePickerFormat: function changeDatePickerFormat(date) {
    return (0, _moment2.default)(date).format("YYYY-MM-DD");
  },
  changeBirthDayDatePickerFormat: function changeBirthDayDatePickerFormat(date) {
    return (0, _moment2.default)(date).format("DD-MM-YYYY");
  },
  changeBirthDayFormat: function changeBirthDayFormat(date) {
    return (0, _moment2.default)(date).format("DD MMMM YYYY");
  },
  changeDateAgo: function changeDateAgo(date) {
    return (0, _moment2.default)(date).fromNow();
  },
  diffExperienceDate: function diffExperienceDate(start_date, end_date) {
    var lang = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'en';

    var start = (0, _moment2.default)(start_date);
    var end = (0, _moment2.default)(end_date);

    var month_diff = end.diff(start, 'months');
    var year_diff = 0;

    if (month_diff > 12) {
      year_diff = parseInt(month_diff / 12);
      month_diff %= 12;
    }

    var wording = "";

    if (year_diff > 0) wording += year_diff + (lang === 'th' ? " ปี" : " Year") + (year_diff > 1 && lang === 'en' ? "s " : " ");

    wording += month_diff + (lang === 'th' ? " เดือน" : " Month") + (month_diff > 1 && lang === 'en' ? "s" : "");

    return wording;
  },
  clickEditMenu: function clickEditMenu(evt) {

    var $dropdown = $(evt.target).find('.edit-menu__dropdown');

    $dropdown.toggleClass('opened');

    if ($dropdown.hasClass('opened')) event.stopPropagation();

    $('body').one('click', function (event) {
      if (!$(event.target).closest('.edit-menu').length) $dropdown.removeClass('opened');
    });
  },
  triggerOwlCarousel: function triggerOwlCarousel() {
    $('.owl-carousel').owlCarousel({
      loop: false,
      margin: 20,
      nav: true,
      responsive: {
        300: {
          items: 1
        },
        550: {
          items: 2
        },
        730: {
          items: 3
        },
        1000: {
          items: 4
        },
        1200: {
          items: 5
        }
      },
      navText: ["", ""]
    });
  },
  drawer: function drawer() {
    $(".topbar__btn--toggle").click(function () {
      event.stopPropagation();
      if (showDrawer === false) {
        $(".drawer").addClass("opened");
        $("body").addClass("overflow");
        showDrawer = true;
      } else {
        $(".drawer").removeClass("opened");
        $("body").removeClass("overflow");
        showDrawer = false;
      }
    });
  },
  moreinfo: function moreinfo() {
    var checkinfo = true;
    var showDetail = false;

    $("#more-info").click(function () {
      if (checkinfo === true) {
        $("#more-info-detail").show();
        $(this).text("Less Infomation");
        showDetail = true;
        checkinfo = false;
      } else if (checkinfo === false) {
        $("#more-info-detail").hide();
        $(this).text("More Infomation");
        showDetail = false;
        checkinfo = true;
        if ($.contains('.more')) {
          $(window).scrollTop($('.more').offset().top);
        }
      }
    });
  },
  popupGallery: function popupGallery() {
    $('.popup-gallery').each(function () {
      $(this).magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
          enabled: true,
          navigateByImgClick: true,
          preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
          tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
          titleSrc: function titleSrc(item) {
            return item.el.attr('title');
          }
        }
      });
    });
  },
  backendDrawer: function backendDrawer() {
    var showDetail = true;
    var checkinfo = true;

    $(window).resize(function () {

      var windowsize = $(this).width();

      if (windowsize > 990) {
        $("#Mysidenav").css('width', "230px");
        $('#main header#admin__header').css('margin-left', '230px');
        $('.container-content').css('margin-left', '230px');
        // $('.container-content').css('margin-right','-230px');
        showSidebar = true;
      } else {
        if (showSidebar === true) {
          $("#Mysidenav").css('width', "0px");
          $('#main header#admin__header').css('margin-left', '0px');
          $('.container-content').css('margin-left', '0px');
          $('.container-content').css('margin-right', '0px');
          showSidebar = false;
        }
      }

      if (windowsize > 1000) {
        $("#more-info-detail").hide();
        $("#more-info").parent('div').parent('div').parent('div').hide();
        $("#more-info").text("More Infomation");
        showDetail = false;
        checkinfo = true;
        if ($.contains('.more')) $(window).scrollTop($('.more').offset().top - 100);
      } else {
        if (!showDetail) {
          $("#more-info").text("More Infomation");
          $("#more-info").parent('div').parent('div').parent('div').show();
          checkinfo = true;
          if ($.contains('.more')) $(window).scrollTop($('.more').offset().top - 100);
        }
      }
    });

    $('.sidebar__toggle').click(function () {
      if (showSidebar === false) {
        $("#Mysidenav").css('width', "230px");
        $('#main header#admin__header').css('margin-left', '230px');
        $('.container-content').css('margin-right', '-230px');
        $('.container-content').css('margin-left', '230px');
        showSidebar = true;
      } else if (showSidebar === true) {
        $("#Mysidenav").css('width', "0px");
        $('#main header#admin__header').css('margin-left', '0px');
        $('.container-content').css('margin-left', '0px');
        $('.container-content').css('margin-right', '0px');
        showSidebar = false;
      }
    });
  },
  sidebarToggle: function sidebarToggle() {

    var windowsize = $(window).width();

    if (windowsize <= 990) {
      if (showSidebar === false) {
        $("#Mysidenav").css('width', "230px");
        $('#main header#admin__header').css('margin-left', '230px');
        $('.container-content').css('margin-right', '-230px');
        $('.container-content').css('margin-left', '230px');
        showSidebar = true;
      } else if (showSidebar === true) {
        $("#Mysidenav").css('width', "0px");
        $('#main header#admin__header').css('margin-left', '0px');
        $('.container-content').css('margin-left', '0px');
        $('.container-content').css('margin-right', '0px');
        showSidebar = false;
      }
    }
  },
  showAdvanceSearch: function showAdvanceSearch(evt) {
    $('.jobbar__detail').css('display', 'block');
    $(evt.target).css('display', 'none');
  },
  toggleDrawer: function toggleDrawer() {
    $(".drawer").removeClass("opened");
    $("body").removeClass("overflow");
    showDrawer = false;
  },
  toPostRegister: function toPostRegister() {
    // Next Step
    $("#myModal__Register").modal('hide');
    setTimeout(function () {
      $("#myModal__Regisform").modal('show');
    }, 500);
  },
  animateScrollTo: function animateScrollTo(id) {
    $('html, body').animate({
      scrollTop: $(id).offset().top - 200
    }, 1000);
  }
};

exports.default = JQueryTool;