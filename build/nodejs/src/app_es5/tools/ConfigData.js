'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

// Date
var EnglistMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var _Months = [["01", "January"], ["02", "February"], ["03", "March"], ["04", "April"], ["05", "May"], ["06", "June"], ["07", "July"], ["08", "August"], ["09", "September"], ["10", "October"], ["11", "November"], ["12", "December"]];
var _ThaiMonths = [["01", "มกราคม"], ["02", "กุมภาพันธ์"], ["03", "มีนาคม"], ["04", "เมษายน"], ["05", "พฤษภาคม"], ["06", "มิถุนายน"], ["07", "กรกฎาคม"], ["08", "สิงหาคม"], ["09", "กันยายน"], ["10", "ตุลาคม"], ["11", "พฤศจิกายน"], ["12", "ธันวาคม"]];

// User Data
var _InterestList = ["Sport", "Art", "Photography", "Design", "Health/Fitness", "Music", "Movies", "Reading", "Traveling", "Cooking", "Shopping", "Fashion", "Food & Drinks", "Cocktails", "Technology", "Books", "Pets", "History", "Dancing", "Writing", "Social Media", "Cars", "Motorcycle", "Gaming", "Architecture", "Innovation", "Entrepreneur", "Finance", "Wealth", "Politics", "Nature", "Management", "Career", "Marketing", "Sales", "Philosophy", "Self-Improvement", "Education", "Other"];

var _BenefitList = ["5 Days Work Week", "Free Duty Meals", "Free Uniform", "Provident Fund", "Social Security Fund", "Health Insurance", "Workmen's Compensation Fund", "Out-patient Medical Claim", "Service Charge", "Paid Vacation and Public Holidays", "Wedding Leave & Allowance", "Child Birth Allowance", "Funeral Allowance", "Annual Bonus Plan", "Life Insurance", "Annual Check-up", "Training & Development", "Loyalty & Recognition Awards", "Staff Discounts"];

// Resource
var _ResourceDepartment = ["Cross-Departmental", "F&B Service", "Culinary", "Housekeeping", "Sales & Marketing", "Finance", "Front Office", "Human Resources", "Engineering", "IT", "Security", "Miscellaneous"];

var _ResourceCategory = ["Videos", "Seminars/Courses", "Training Manuals", "Templates/Checklists", "Articles/Links", "Virtual Mentorship"];

// Feed Privacy
var _UserPrivacy = { public: 'Public', timeline: 'My Timeline', connected: 'Connection', employer: 'Employer', onlyme: 'Only Me' };
var _CompanyPrivacy = { public: 'Public', timeline: 'My Timeline', employee: 'Employee' };

var _CompensationList = ["Based on experience", "< 9000 THB", "9,000 - 15,000 THB", "15,000 - 30,000 THB", "30,000 - 50,000 THB", "50,000 - 100,000 THB", "100,000 - 200,000 THB", "> 200,000 THB"];
var _CompanyTypeList = ["All", "Hotel", "Restaurant", "Bar", "Nightclub", "Retail", "Tourism", "Spa", "Catering", "Others"];
var _DistanceList = { 5000: "Within 5 km", 10000: "Within 10 km", 15000: "Within 15 km", 20000: "Within 20 km", 25000: "Within 25 km", 50000: "Within 50 km", 100000: "Within 100 km" };
var _DatePostList = { 1: "Past 24 Hours", 7: "Past Week", 14: "Past 2 Weeks", 30: "Past Month", 60: "Past 2 Months", 90: "Past 3 Months" };
var _JobTypeList = ["All Job Types", "Full Time", // Full-time
"Part Time", // Part-time
"Temporary", // Temporary
"Casual/Hourly", // Hourly
"Internship", // Internships
"Freelance", // Freelance
"Contract" // Contract
];
var _WorkExperienceList = { "[0,1]": "Less than one year", "[1,2]": "Between 1 and 2 years", "[2,3]": "Between 2 and 3 years", "[3,5]": "Between 3 and 5 years", "[5,100]": "More than 5 years" };

var _ApplyJobStatus = ["No Status", "Thank you for your application", "We are reviewing your application", "We regret to inform you that your application has not been successful"];

var ConfigData = {
  EnglishMonthList: function EnglishMonthList() {
    return EnglistMonths;
  },
  getEnglishMonth: function getEnglishMonth(id) {
    return EnglistMonths[id];
  },
  Months: function Months() {
    return _Months;
  },
  ThaiMonths: function ThaiMonths() {
    return _ThaiMonths;
  },
  Years: function Years() {
    var years = [];

    for (var i = new Date().getFullYear(); i >= 1984; i--) {
      years.push(i);
    }return years;
  },
  ConvertYoutubeToEmbed: function ConvertYoutubeToEmbed(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) return "//www.youtube.com/embed/" + match[2];else return 'error';
  },
  ResourceCategory: function ResourceCategory() {
    return _ResourceCategory;
  },
  ResourceCategoryWithLastest: function ResourceCategoryWithLastest() {
    var newCat = ["Lastest"].concat(_ResourceCategory);
    return newCat;
  },
  ResourceDepartment: function ResourceDepartment() {
    return _ResourceDepartment;
  },
  CompensationList: function CompensationList() {
    return _CompensationList;
  },
  InterestList: function InterestList() {
    return _InterestList;
  },
  BenefitList: function BenefitList() {
    return _BenefitList;
  },
  CompanyTypeList: function CompanyTypeList() {
    return _CompanyTypeList;
  },
  DistanceList: function DistanceList() {
    return _DistanceList;
  },
  DatePostList: function DatePostList() {
    return _DatePostList;
  },
  JobTypeList: function JobTypeList() {
    var start = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

    return _JobTypeList.slice(start);
  },
  WorkExperienceList: function WorkExperienceList() {
    return _WorkExperienceList;
  },
  ApplyJobStatus: function ApplyJobStatus() {
    return _ApplyJobStatus;
  },
  BackgroundTemplateList: function BackgroundTemplateList() {

    var templateURL = "/images/template/";

    var templateList = [];

    for (var i = 1; i <= 16; i++) {
      templateList.push(templateURL + i);
    }return templateList;
  },
  ConvertNotification: function ConvertNotification(type, data) {
    var response = "";

    if (type == 'comment') response = "commented on your timeline.";else if (type == 'like') response = 'likes your post.';else if (type == 'reply') response = 'reply on your comment in timeline.';else if (type == 'applyjob') response = 'applied for a job.';else if (type == 'feed') response = 'posted in Buzz.';else if (type == 'follow') response = 'is following you.';

    return response;
  },
  UserPrivacy: function UserPrivacy() {
    return _UserPrivacy;
  },
  CompanyPrivacy: function CompanyPrivacy() {
    return _CompanyPrivacy;
  },
  ConvertEmployeeLang: function ConvertEmployeeLang(text, lang) {
    if (lang === 'th') {
      if (text === 'Current Employee') return 'พนักงานปัจจุบัน';else return 'อดีตพนักงาน';
    } else return text;
  }
};

exports.default = ConfigData;