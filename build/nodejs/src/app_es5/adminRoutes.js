'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _AppContainer = require('./containers/Admin/AppContainer');

var _AppContainer2 = _interopRequireDefault(_AppContainer);

var _DashboardContainer = require('./containers/Admin/DashboardContainer');

var _DashboardContainer2 = _interopRequireDefault(_DashboardContainer);

var _ResourceContainer = require('./containers/Admin/ResourceContainer');

var _ResourceContainer2 = _interopRequireDefault(_ResourceContainer);

var _ResourceCreatorContainer = require('./containers/Admin/ResourceCreatorContainer');

var _ResourceCreatorContainer2 = _interopRequireDefault(_ResourceCreatorContainer);

var _NewsletterContainer = require('./containers/Admin/NewsletterContainer');

var _NewsletterContainer2 = _interopRequireDefault(_NewsletterContainer);

var _NewsletterCreateContainer = require('./containers/Admin/NewsletterCreateContainer');

var _NewsletterCreateContainer2 = _interopRequireDefault(_NewsletterCreateContainer);

var _UserContainer = require('./containers/Admin/UserContainer');

var _UserContainer2 = _interopRequireDefault(_UserContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _react2.default.createElement(
  _reactRouter.Route,
  { name: 'language', path: '/:language' },
  _react2.default.createElement(
    _reactRouter.Route,
    { name: 'admin', path: 'admin', component: _AppContainer2.default },
    _react2.default.createElement(_reactRouter.IndexRoute, { name: 'admin_dashboard', component: _DashboardContainer2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'admin_newsletter', path: 'newsletter/index', component: _NewsletterContainer2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'admin_newsletter_create', path: 'newsletter/create', component: _NewsletterCreateContainer2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'admin_resource', path: 'resource/index', component: _ResourceContainer2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'admin_resource_create', path: 'resource/create', component: _ResourceCreatorContainer2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'admin_resource_edit', path: 'resource/edit/:id', component: _ResourceCreatorContainer2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'admin_user', path: 'user/index', component: _UserContainer2.default })
  )
);