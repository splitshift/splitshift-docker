'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SettingAPI = {
  deleteAccount: function deleteAccount(token) {
    return _ajax2.default.deleteFetch('/api/account', token);
  },
  changePassword: function changePassword(token, info) {
    return _ajax2.default.putFetch('/api/change/password', token, info);
  },
  changePrimaryEmail: function changePrimaryEmail(token, info) {
    return _ajax2.default.putFetch('/api/email', token, info);
  }
};

exports.default = SettingAPI;