'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MessageAPI = {
  sendMessage: function sendMessage(token, info) {
    return _ajax2.default.postFetch('/api/message', token, info);
  },
  getAllMessage: function getAllMessage(token) {
    return _ajax2.default.getFetch('/api/chat/all', token);
  },
  getMessageWithPeople: function getMessageWithPeople(token, people_id) {
    return _ajax2.default.getFetch('/api/chat?key=' + people_id, token);
  }
};

exports.default = MessageAPI;