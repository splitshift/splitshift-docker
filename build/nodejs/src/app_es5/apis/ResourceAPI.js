'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ResourceAPI = {
  getResources: function getResources(token) {
    return _ajax2.default.getFetch('/api/resource', token);
  },
  searchResource: function searchResource(info) {
    var filter = {
      keyword: info.keyword ? info.keyword : "",
      category: info.category ? info.category : "",
      department: info.department ? info.department : ""
    };

    return _ajax2.default.getFetch('/api/search/resource?keyword=' + filter.keyword + '&category=' + filter.category + '&department=' + filter.department);
  },
  getResource: function getResource(id, token) {
    return _ajax2.default.getFetch('/api/resource/' + id, token);
  },
  getResourceIndex: function getResourceIndex() {
    return _ajax2.default.getFetch('/api/resource/index');
  },
  getAdminResource: function getAdminResource(token) {
    return _ajax2.default.getFetch('/api/admin/resource', token);
  },
  createResource: function createResource(token, info) {
    return _ajax2.default.postFetch('/api/resource/', token, info);
  },
  editResource: function editResource(token, info) {
    return _ajax2.default.putFetch('/api/resource/' + info._id, token, info);
  },
  likeResource: function likeResource(token, info) {
    return _ajax2.default.putFetch('/api/resource/' + info._id + '/like', token);
  },
  deleteResource: function deleteResource(token, info) {
    return _ajax2.default.deleteFetch('/api/resource/' + info._id, token);
  },
  commentResource: function commentResource(token, info, resource_id) {
    return _ajax2.default.postFetch('/api/resource/' + resource_id + '/comment', token, info);
  },
  deleteCommentResource: function deleteCommentResource(token, info, resource_id) {
    return _ajax2.default.deleteFetch('/api/resource/' + resource_id + '/comment/' + info._id, token);
  },
  forwardEmail: function forwardEmail(token, info) {
    return _ajax2.default.postFetch('/api/forward/resource', token, info);
  }
};

exports.default = ResourceAPI;