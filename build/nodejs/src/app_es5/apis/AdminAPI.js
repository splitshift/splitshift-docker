'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AdminAPI = {
  getUsers: function getUsers(token) {
    return _ajax2.default.getFetch('/api/admin/user', token);
  },
  approveResource: function approveResource(token, resource) {
    return _ajax2.default.putFetch('/api/admin/approve', token, { resource_id: resource._id });
  },
  deleteUser: function deleteUser(token, user) {
    return _ajax2.default.deleteFetch('/api/admin/user/' + user.key, token);
  }
};

exports.default = AdminAPI;