'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HRAPI = {
  getEmployee: function getEmployee(company_key) {
    return _ajax2.default.getFetch('/api/company/' + company_key + '/employee');
  },
  changeRank: function changeRank(token, info) {
    return _ajax2.default.postFetch('/api/company/rank', token, info);
  }
};

exports.default = HRAPI;