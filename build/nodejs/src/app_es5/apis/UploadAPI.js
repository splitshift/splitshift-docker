'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var API_URL = function API_URL() {
  return window.location.origin;
};

var API_HEADERS = { 'Accept': 'application/json', 'Content-Type': 'application/json' };

var status = void 0;

var UploadAPI = {
  postFetch: function postFetch(url, token, info) {

    return fetch(API_URL() + url, {
      method: 'post',
      headers: { 'Authorization': token },
      body: info
    }).then(function (response) {

      status = response.status;
      return response.json();
    }).then(function (data) {

      if (status != 201) throw new Error(data.message);

      return data;
    });
  },
  deleteFetch: function deleteFetch(url, token) {
    return fetch(API_URL() + url, {
      method: 'delete',
      headers: { 'Authorization': token }
    }).then(function (response) {
      status = response.status;
      return response.json();
    }).then(function (data) {
      if (status != 200) throw new Error(data.message);
      return data;
    });
  },
  uploadImage: function uploadImage(token, info) {
    var data = new FormData();
    data.append("avatar", info);

    return this.postFetch('/api/image', token, data);
  },
  deleteImage: function deleteImage(token, info) {
    return this.deleteFetch('/api/image', token, info);
  },
  uploadFile: function uploadFile(token, info) {

    var data = new FormData();
    data.append("file", info);

    return this.postFetch('/api/file', token, data);
  }
};

exports.default = UploadAPI;