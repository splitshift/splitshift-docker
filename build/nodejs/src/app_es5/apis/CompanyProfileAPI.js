'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CompanyProfileAPI = {
  getProfile: function getProfile(key, token) {
    return _ajax2.default.getFetch('/api/profile/' + key, token);
  },
  updateInformation: function updateInformation(token, info) {
    return _ajax2.default.putFetch('/api/company/info', token, info);
  },
  updateAddress: function updateAddress(token, info) {
    return _ajax2.default.putFetch('/api/company/address', token, info);
  },
  updateOverview: function updateOverview(token, info) {
    return _ajax2.default.putFetch('/api/company/overview', token, info);
  },
  updateCulture: function updateCulture(token, info) {
    return _ajax2.default.putFetch('/api/company/culture', token, info);
  },
  updateVideo: function updateVideo(token, info) {
    return _ajax2.default.putFetch('/api/company/video', token, info);
  },
  createBenefit: function createBenefit(token, info) {
    return _ajax2.default.postFetch('/api/company/benefit', token, info);
  },
  deleteBenefit: function deleteBenefit(token, info) {
    return _ajax2.default.deleteFetch('/api/company/benefit/' + info._id, token);
  },
  updateSocialLink: function updateSocialLink(token, info) {
    return _ajax2.default.putFetch('/api/company/link', token, info);
  },
  uploadProfileImage: function uploadProfileImage(token, info) {
    var data = new FormData();
    data.append("avatar", info);
    return _ajax2.default.putImageFetch('/api/profileimage', token, data);
  },
  uploadBannerImage: function uploadBannerImage(token, info) {
    var data = new FormData();
    data.append("avatar", info);
    return _ajax2.default.putImageFetch('/api/banner', token, data);
  },
  changeBannerImageWithTemplate: function changeBannerImageWithTemplate(token, data) {
    return _ajax2.default.putFetch('/api/banner/template', token, data);
  },
  reviewCompany: function reviewCompany(token, info) {
    return _ajax2.default.postFetch("/api/user/review", token, info);
  }
};

exports.default = CompanyProfileAPI;