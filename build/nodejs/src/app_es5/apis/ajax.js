'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

require('whatwg-fetch');

var API_URL = function API_URL() {
  return window.location.origin;
};

var API_HEADERS = { 'Accept': 'application/json', 'Content-Type': 'application/json' };


var status = void 0;

var ajax = {
  getFetch: function getFetch(url, token) {
    return fetch(API_URL() + url, {
      method: 'get',
      headers: { 'Content-Type': 'application/json', 'Authorization': token ? token : "" }
    }).then(function (response) {
      status = response.status;
      return response.json();
    }).then(function (data) {
      if (status != 200) throw new Error(data.message);

      return data;
    });
  },
  postFetch: function postFetch(url, token, info) {
    return fetch(API_URL() + url, {
      method: 'post',
      headers: { 'Content-Type': 'application/json', 'Authorization': token ? token : "" },
      body: JSON.stringify(info)
    }).then(function (response) {
      status = response.status;
      return response.json();
    }).then(function (data) {
      if (status != 200 && status != 201) throw new Error(data.message);
      return data;
    });
  },
  putFetch: function putFetch(url, token, info) {
    return fetch(API_URL() + url, {
      method: 'put',
      headers: { 'Content-Type': 'application/json', 'Authorization': token ? token : "" },
      body: JSON.stringify(info)
    }).then(function (response) {
      status = response.status;
      return response.json();
    }).then(function (data) {
      if (status != 200 && status != 201) throw new Error(data.message);
      return data;
    });
  },
  deleteFetch: function deleteFetch(url, token, info) {
    return fetch(API_URL() + url, {
      method: 'delete',
      headers: { 'Content-Type': 'application/json', 'Authorization': token },
      body: JSON.stringify(info)
    }).then(function (response) {
      status = response.status;
      return response.json();
    }).then(function (data) {
      if (status != 200) throw new Error(data.message);
      return data;
    });
  },
  putImageFetch: function putImageFetch(url, token, info) {
    return fetch(API_URL() + url, {
      method: 'put',
      headers: { 'Authorization': token },
      body: info
    }).then(function (response) {
      status = response.status;
      return response.json();
    }).then(function (data) {
      if (status != 200) throw new Error(data.message);
      return data;
    });
  }
};

exports.default = ajax;