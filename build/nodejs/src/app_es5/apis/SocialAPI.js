'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SocialAPI = {
  putFollow: function putFollow(token, user_key) {
    return _ajax2.default.putFetch('/api/following', token, { to: user_key });
  },
  getFollowers: function getFollowers(user_key) {
    return _ajax2.default.getFetch('/api/following/' + user_key);
  },
  getConnectRequestList: function getConnectRequestList(token) {
    return _ajax2.default.getFetch('/api/request', token);
  },
  getNotification: function getNotification(token) {
    return _ajax2.default.getFetch('/api/notification', token);
  },
  putNotificationSeen: function putNotificationSeen(token) {
    return _ajax2.default.putFetch('/api/notification/seen', token);
  },
  deleteNotification: function deleteNotification(token, notification_id) {
    return _ajax2.default.deleteFetch('/api/notification/' + notification_id, token);
  },
  postConnectRequest: function postConnectRequest(token, user_key) {
    return _ajax2.default.postFetch('/api/connect', token, { to: user_key });
  },
  putConnectResponse: function putConnectResponse(token, info) {
    return _ajax2.default.putFetch('/api/connect', token, info);
  },
  deleteConnect: function deleteConnect(token, user_key) {
    return _ajax2.default.deleteFetch('/api/connect', token, { to: user_key });
  },
  getConnections: function getConnections(user_key) {
    return _ajax2.default.getFetch('/api/connect/' + user_key);
  }
};

exports.default = SocialAPI;