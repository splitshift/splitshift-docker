'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProfileAPI = {
  getUserProfile: function getUserProfile(key, token) {

    return _ajax2.default.getFetch('/api/profile/' + key, token);
  },
  updateInfo: function updateInfo(token, info) {
    return _ajax2.default.putFetch("/api/user/info", token, info);
  },
  updateAbout: function updateAbout(token, info) {
    return _ajax2.default.putFetch("/api/user/about", token, info);
  },
  updateInterest: function updateInterest(token, info) {
    return _ajax2.default.putFetch("/api/user/interest", token, info);
  },
  createInterest: function createInterest(token, info) {

    var data = {
      category: info.category ? info.category : "Art",
      value: info.value ? info.value : "Art"
    };

    return _ajax2.default.postFetch("/api/user/interest", token, data);
  },
  editInterest: function editInterest(token, info) {
    return _ajax2.default.putFetch("/api/user/interest/" + info._id, token, info);
  },
  deleteInterest: function deleteInterest(token, info) {
    return _ajax2.default.deleteFetch("/api/user/interest/" + info._id, token);
  },
  createExperience: function createExperience(token, info) {
    var data = Object.assign({}, info);
    data.position = data.position ? data.position : "";
    data.workplace = data.workplace ? data.workplace : "";
    data.start_date = data.start_date ? data.start_date : "";
    data.end_date = data.end_date ? data.end_date : "";
    data.address = data.address ? data.address : "";
    data.company_key = data.company_key ? data.company_key : "";

    return _ajax2.default.postFetch("/api/user/experience", token, data);
  },
  editExperience: function editExperience(token, info) {
    return _ajax2.default.putFetch("/api/user/experience/" + info._id, token, info);
  },
  deleteExperience: function deleteExperience(token, info) {
    return _ajax2.default.deleteFetch("/api/user/experience/" + info._id, token);
  },
  updateSkill: function updateSkill(token, info) {
    return _ajax2.default.putFetch('/api/user/skill', token, info);
  },
  createEducation: function createEducation(token, info) {

    var data = {
      field: info.field ? info.field : "",
      school: info.school ? info.school : "",
      start_date: info.start_date ? info.start_date : "",
      graduate_date: info.graduate_date ? info.graduate_date : "",
      description: info.description ? info.description : "",
      gpa: info.gpa ? info.gpa : ""
    };

    console.log("Create Education");
    console.log(data);

    return _ajax2.default.postFetch("/api/user/education", token, data);
  },
  editEducation: function editEducation(token, info) {
    return _ajax2.default.putFetch("/api/user/education/" + info._id, token, info);
  },
  deleteEducation: function deleteEducation(token, info) {
    return _ajax2.default.deleteFetch("/api/user/education/" + info._id, token);
  },
  createCourse: function createCourse(token, info) {

    var data = {
      name: info.name ? info.name : "",
      start_date: info.start_date ? info.start_date : "",
      end_date: info.end_date ? info.end_date : "",
      description: info.description,
      link: info.link
    };

    return _ajax2.default.postFetch("/api/user/course", token, data);
  },
  editCourse: function editCourse(token, info) {
    return _ajax2.default.putFetch("/api/user/course/" + info._id, token, info);
  },
  deleteCourse: function deleteCourse(token, info) {
    return _ajax2.default.deleteFetch("/api/user/course/" + info._id, token);
  },
  createLanguage: function createLanguage(token, info) {
    return _ajax2.default.postFetch("/api/user/language", token, info);
  },
  editLanguage: function editLanguage(token, info) {
    return _ajax2.default.putFetch("/api/user/language/" + info._id, token, info);
  },
  deleteLanguage: function deleteLanguage(token, info) {
    return _ajax2.default.deleteFetch("/api/user/language/" + info._id, token);
  },
  uploadProfileImage: function uploadProfileImage(token, info) {
    var data = new FormData();
    data.append("avatar", info);
    return _ajax2.default.putImageFetch('/api/profileimage', token, data);
  },
  uploadBannerImage: function uploadBannerImage(token, info) {
    var data = new FormData();
    data.append("avatar", info);
    return _ajax2.default.putImageFetch('/api/banner', token, data);
  },
  changeBannerImageWithTemplate: function changeBannerImageWithTemplate(token, data) {
    return _ajax2.default.putFetch('/api/banner/template', token, data);
  },
  loadCompany: function loadCompany() {
    return _ajax2.default.getFetch('/api/company/all');
  }
};

exports.default = ProfileAPI;