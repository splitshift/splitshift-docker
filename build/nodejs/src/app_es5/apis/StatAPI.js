'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StatAPI = {
  getStat: function getStat() {
    return _ajax2.default.getFetch('/api/stat/all');
  },
  getAdminSummary: function getAdminSummary(token) {
    return _ajax2.default.getFetch('/api/admin/metric', token);
  },
  getUserSummary: function getUserSummary(token) {
    return _ajax2.default.getFetch('/api/dashboard', token);
  }
};

exports.default = StatAPI;