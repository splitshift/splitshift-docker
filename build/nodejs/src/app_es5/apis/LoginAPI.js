'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LoginAPI = {
  login: function login(info) {
    return _ajax2.default.postFetch('/api/login', '', info);
  }
};

exports.default = LoginAPI;