'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FeedAPI = {
  getFeed: function getFeed(user_key, token, page) {
    return _ajax2.default.getFetch('/api/feed/' + user_key + '?page=' + page, token);
  },
  getPhotosFeed: function getPhotosFeed(user_key, token) {
    return _ajax2.default.getFetch('/api/photo?user_key=' + user_key, token);
  },
  getAlbums: function getAlbums(user_key) {
    return _ajax2.default.getFetch('/api/album?user_key=' + user_key);
  },
  getPhotosInAlbum: function getPhotosInAlbum(user_key, info, token) {
    return _ajax2.default.getFetch('/api/album/' + info._id + '?user_key=' + user_key, token);
  },
  getBuzzFeed: function getBuzzFeed(page) {
    return _ajax2.default.getFetch('/api/buzz?page=' + page);
  },
  searchBuzzFeed: function searchBuzzFeed(keyword) {
    return _ajax2.default.getFetch('/api/search/buzz?keyword=' + keyword);
  },
  postFeed: function postFeed(token, info) {
    return _ajax2.default.postFetch('/api/feed', token, info);
  },
  editFeed: function editFeed(token, info) {
    return _ajax2.default.putFetch('/api/feed/' + info._id, token, info);
  },
  changePrivacy: function changePrivacy(token, info) {
    return _ajax2.default.putFetch('/api/feed/' + info._id + '/privacy', token, info);
  },
  deleteFeed: function deleteFeed(token, info) {
    return _ajax2.default.deleteFetch('/api/feed/' + info._id, token);
  },
  deleteBuzzFeed: function deleteBuzzFeed(token, info) {
    return _ajax2.default.deleteFetch('/api/buzz/' + info._id, token);
  },
  likeFeed: function likeFeed(token, info) {
    return _ajax2.default.postFetch('/api/feed/' + info._id + '/like', token, info);
  },
  unlikeFeed: function unlikeFeed(token, info) {
    return _ajax2.default.deleteFetch('/api/feed' + info._id + '/like', token);
  },
  postCommentFeed: function postCommentFeed(token, info) {
    return _ajax2.default.postFetch('/api/comment', token, info);
  },
  deleteCommentFeed: function deleteCommentFeed(token, info, feed) {
    return _ajax2.default.deleteFetch('/api/comment/' + info._id, token, feed);
  },
  postReplyComment: function postReplyComment(token, info) {
    return _ajax2.default.postFetch('/api/reply', token, info);
  },
  deleteReply: function deleteReply(token, info, reply) {
    return _ajax2.default.deleteFetch('/api/reply/' + info._id, token, reply);
  },
  putLikeFeed: function putLikeFeed(token, info) {
    return _ajax2.default.putFetch('/api/feed/' + info._id + '/like', token, { user_key: info.owner.key });
  }
};

exports.default = FeedAPI;