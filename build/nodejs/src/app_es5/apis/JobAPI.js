'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var JobAPI = {
  postNewJob: function postNewJob(token, info) {
    return _ajax2.default.postFetch('/api/company/job', token, info);
  },
  getAllJob: function getAllJob(token) {
    return _ajax2.default.getFetch('/api/company/job/all', token);
  },
  getEmployeeApplyJob: function getEmployeeApplyJob(token) {
    return _ajax2.default.getFetch('/api/user/applyjob', token);
  },
  getJob: function getJob(token, job_id) {
    return _ajax2.default.getFetch('/api/company/job/' + job_id, token);
  },
  putUpdateJob: function putUpdateJob(token, info) {
    return _ajax2.default.putFetch('/api/company/job/' + info._id, token, info);
  },
  deleteJob: function deleteJob(token, info) {
    return _ajax2.default.deleteFetch('/api/company/job/' + info._id, token);
  },
  activateJob: function activateJob(token, job_id) {
    return _ajax2.default.putFetch('/api/company/job/' + job_id + '/activate', token);
  },
  getAllApplyJob: function getAllApplyJob(token) {
    return _ajax2.default.getFetch('/api/applyjob', token);
  },
  getApplyJob: function getApplyJob(token, info) {
    return _ajax2.default.getFetch('/api/applyjob/' + info._id, token);
  },
  changeAppleJobStatus: function changeAppleJobStatus(token, applyjob_id, info) {
    return _ajax2.default.putFetch('/api/applyjob/' + applyjob_id + '/status', token, info);
  },
  postApplyJob: function postApplyJob(token, info) {
    return _ajax2.default.postFetch('/api/applyjob', token, info);
  },
  deleteApplyJob: function deleteApplyJob(token, apply_job_id) {
    return _ajax2.default.deleteFetch('/api/applyjob/' + apply_job_id, token);
  },
  deleteApplication: function deleteApplication(token, apply_job_id) {
    return _ajax2.default.deleteFetch('/api/user/applyjob/' + apply_job_id, token);
  },
  forwardEmail: function forwardEmail(token, info) {
    return _ajax2.default.postFetch('/api/forward/job', token, info);
  }
};

exports.default = JobAPI;