'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var RegisterAPI = {
  register: function register(info) {
    return _ajax2.default.postFetch('/api/register', '', info);
  },
  forgetPassword: function forgetPassword(info) {
    return _ajax2.default.postFetch('/api/forgot/password', '', info);
  },
  changePasswordFormForgot: function changePasswordFormForgot(info, token) {
    return _ajax2.default.putFetch('/api/forgot/password/' + token, '', info);
  }
};

exports.default = RegisterAPI;