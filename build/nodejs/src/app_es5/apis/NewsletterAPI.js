'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require('./ajax');

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FeedAPI = {
  getNewsletters: function getNewsletters(token) {
    return _ajax2.default.getFetch('/api/subscribe', token);
  },
  postNewsletter: function postNewsletter(info) {
    return _ajax2.default.postFetch('/api/subscribe', "", info);
  },
  sendEmailSingle: function sendEmailSingle(token, info) {
    return _ajax2.default.postFetch('/api/send/email', token, info);
  },
  sendEmailSubscibers: function sendEmailSubscibers(token, info) {
    return _ajax2.default.postFetch('/api/send/subscribe', token, info);
  }
};

exports.default = FeedAPI;