"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ajax = require("./ajax");

var _ajax2 = _interopRequireDefault(_ajax);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SearchAPI = {
  job: function job(info, page) {

    var filter = {
      keyword: info.keyword ? info.keyword : "",
      location: info.location ? info.location : "",
      point: info.point ? info.point : "",
      distance: info.distance ? info.distance : "",
      bounded_time: info.bounded_time ? info.bounded_time : "",
      type: info.type ? info.type : "",
      compensation: info.compensation ? info.compensation : "",
      employer: info.employer ? info.employer : ""
    };

    return _ajax2.default.getFetch('/api/search/jobs?keyword=' + filter.keyword + '&location=' + filter.location + '&point=' + filter.point + '&distance=' + filter.distance + '&bounded_time=' + filter.bounded_time + '&type=' + filter.type + '&compensation=' + filter.compensation + '&employer=' + filter.employer + '&page=' + page);
  },
  people: function people(info, page) {

    var filter = {
      keyword: info.keyword ? info.keyword : "",
      interest: info.interest ? info.interest : "",
      work_exp: info.work_exp ? info.work_exp : "",
      location: info.location ? info.location : ""
    };

    return _ajax2.default.getFetch('/api/search/people?keyword=' + filter.keyword + '&interest=' + filter.interest + '&work_exp=' + filter.work_exp + '&location=' + filter.location + '&page=' + page);
  },
  company: function company(info, page) {

    var filter = {
      keyword: info.keyword ? info.keyword : "",
      type: info.type ? info.type : "",
      overall: info.overall ? info.overall : "",
      location: info.location ? info.location : "",
      bounded_time: info.bounded_time ? info.bounded_time : "",
      compensation: info.compensation ? info.compensation : ""
    };

    return _ajax2.default.getFetch('/api/search/companies?keyword=' + filter.keyword + '&type=' + filter.type + '&overall=' + filter.overall + '&location=' + filter.location + '&bounded_time=' + filter.bounded_time + '&compensation=' + filter.compensation + '&page=' + page);
  },
  all: function all(info) {
    return _ajax2.default.getFetch('/api/search?keyword=' + info);
  }
};

exports.default = SearchAPI;