'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _SearchAPI = require('../apis/SearchAPI');

var _SearchAPI2 = _interopRequireDefault(_SearchAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SearchActionCreators = {
  Job: function Job(info) {
    var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;


    if (page === 0) {
      _AppDispatcher2.default.dispatchAsync(_SearchAPI2.default.job(info, page), {
        request: _constants2.default.SEARCH_JOB,
        success: _constants2.default.SEARCH_JOB_SUCCESS,
        failure: _constants2.default.SEARCH_JOB_FAIL
      });
    } else {
      _AppDispatcher2.default.dispatchAsync(_SearchAPI2.default.job(info, page), {
        request: _constants2.default.SEARCH_JOB_MORE,
        success: _constants2.default.SEARCH_JOB_MORE_SUCCESS,
        failure: _constants2.default.SEARCH_JOB_FAIL
      });
    }
  },
  People: function People(info) {
    var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;


    if (page === 0) {
      _AppDispatcher2.default.dispatchAsync(_SearchAPI2.default.people(info, page), {
        request: _constants2.default.SEARCH_PEOPLE,
        success: _constants2.default.SEARCH_PEOPLE_SUCCESS,
        failure: _constants2.default.SEARCH_PEOPLE_FAIL
      });
    } else {
      _AppDispatcher2.default.dispatchAsync(_SearchAPI2.default.people(info, page), {
        request: _constants2.default.SEARCH_PEOPLE_MORE,
        success: _constants2.default.SEARCH_PEOPLE_MORE_SUCCESS,
        failure: _constants2.default.SEARCH_PEOPLE_FAIL
      });
    }
  },
  Company: function Company(info) {
    var page = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;


    if (page === 0) {
      _AppDispatcher2.default.dispatchAsync(_SearchAPI2.default.company(info, page), {
        request: _constants2.default.SEARCH_COMPANY,
        success: _constants2.default.SEARCH_COMPANY_SUCCESS,
        failure: _constants2.default.SEARCH_COMPANY_FAIL
      });
    } else {
      _AppDispatcher2.default.dispatchAsync(_SearchAPI2.default.company(info, page), {
        request: _constants2.default.SEARCH_COMPANY_MORE,
        success: _constants2.default.SEARCH_COMPANY_MORE_SUCCESS,
        failure: _constants2.default.SEARCH_COMPANY_FAIL
      });
    }
  },
  All: function All(info) {
    _AppDispatcher2.default.dispatchAsync(_SearchAPI2.default.all(info), {
      request: _constants2.default.SEARCH_ALL,
      success: _constants2.default.SEARCH_ALL_SUCCESS,
      failure: _constants2.default.SEARCH_ALL_FAIL
    });
  },
  clearFilter: function clearFilter() {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CLEAR_FILTER
    });
  },
  updateFilter: function updateFilter(response) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.UPDATE_FILTER,
      payload: { response: response }
    });
  },
  chooseJobSearch: function chooseJobSearch(index) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CHOOSE_JOB_SEARCH,
      payload: { job_index: index }
    });
  }
};

exports.default = SearchActionCreators;