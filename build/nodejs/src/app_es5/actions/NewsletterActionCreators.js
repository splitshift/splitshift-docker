'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _reactRouter = require('react-router');

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _NewsletterAPI = require('../apis/NewsletterAPI');

var _NewsletterAPI2 = _interopRequireDefault(_NewsletterAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NewsletterActionCreators = {
  getNewsletters: function getNewsletters(token) {
    _AppDispatcher2.default.dispatchAsync(_NewsletterAPI2.default.getNewsletters(token), {
      request: _constants2.default.GET_NEWSLETTER,
      success: _constants2.default.GET_NEWSLETTER_SUCCESS,
      failure: _constants2.default.GET_NEWSLETTER_FAIL
    });
  },
  postNewsletter: function postNewsletter(info) {
    _AppDispatcher2.default.dispatchAsync(_NewsletterAPI2.default.postNewsletter(info), {
      request: _constants2.default.SEND_NEWSLETTER,
      success: _constants2.default.SEND_NEWSLETTER_SUCCESS,
      failure: _constants2.default.SEND_NEWSLETTER_FAIL
    });
  },
  sendEmailSingle: function sendEmailSingle(token, info) {
    _AppDispatcher2.default.dispatchAsync(_NewsletterAPI2.default.sendEmailSingle(token, info), {
      request: _constants2.default.SEND_EMAIL_SINGLE,
      success: _constants2.default.SEND_EMAIL_SUCCESS,
      failure: _constants2.default.SEND_EMAIL_FAIL
    });
  },
  sendEmailSubscibers: function sendEmailSubscibers(token, info) {
    _AppDispatcher2.default.dispatchAsync(_NewsletterAPI2.default.sendEmailSubscibers(token, info), {
      request: _constants2.default.SEND_EMAIL_SUBSCRIBER,
      success: _constants2.default.SEND_EMAIL_SUCCESS,
      failure: _constants2.default.SEND_EMAIL_FAIL
    });
  }
};

exports.default = NewsletterActionCreators;