'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _CompanyProfileAPI = require('../apis/CompanyProfileAPI');

var _CompanyProfileAPI2 = _interopRequireDefault(_CompanyProfileAPI);

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CompanyProfileActionCreators = {
	getProfile: function getProfile(key, token) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.getProfile(key, token), {
			request: _constants2.default.GET_COMPANY_PROFILE,
			success: _constants2.default.GET_COMPANY_PROFILE_SUCCESS,
			failure: _constants2.default.GET_COMPANY_PROFILE_FAIL
		});
	},
	updateProfile: function updateProfile(info) {
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.UPDATE_COMPANY_PROFILE,
			payload: {
				response: info
			}
		});
	},
	resetProfile: function resetProfile() {
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.RESET_COMPANY_PROFILE
		});
	},
	updateInformation: function updateInformation(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.updateInformation(token, info), {
			request: _constants2.default.UPDATE_INFORMATION_COMPANY_PROFILE,
			success: _constants2.default.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateOverview: function updateOverview(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.updateOverview(token, info), {
			request: _constants2.default.UPDATE_OVERVIEW_COMPANY_PROFILE,
			success: _constants2.default.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateCulture: function updateCulture(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.updateCulture(token, info), {
			request: _constants2.default.UPDATE_CULTURE_COMPANY_PROFILE,
			success: _constants2.default.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateAddress: function updateAddress(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.updateAddress(token, info), {
			request: _constants2.default.UPDATE_ADDRESS_COMPANY_PROFILE,
			success: _constants2.default.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateVideo: function updateVideo(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.updateVideo(token, info), {
			request: _constants2.default.UPDATE_VIDEO_COMPANY_PROFILE,
			success: _constants2.default.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	createBenefit: function createBenefit(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.createBenefit(token, info), {
			request: _constants2.default.UPDATE_BENEFIT_COMPANY_PROFILE,
			success: _constants2.default.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	deleteBenefit: function deleteBenefit(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.deleteBenefit(token, info), {
			request: _constants2.default.DELETE_BENEFIT_COMPANY_PROFILE,
			success: _constants2.default.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateSocialLink: function updateSocialLink(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.updateSocialLink(token, info), {
			request: _constants2.default.UPDATE_SOCIAL_LINK_COMPANY_PROFILE,
			success: _constants2.default.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	uploadProfileImage: function uploadProfileImage(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.uploadProfileImage(token, info), {
			request: _constants2.default.UPLOAD_PROFILE_IMAGE,
			success: _constants2.default.UPLOAD_PROFILE_IMAGE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	uploadBannerImage: function uploadBannerImage(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.uploadBannerImage(token, info), {
			request: _constants2.default.UPLOAD_BANNER_IMAGE,
			success: _constants2.default.UPLOAD_BANNER_IMAGE_SUCCESS,
			failure: _constants2.default.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	changeBannerImageWithTemplate: function changeBannerImageWithTemplate(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.changeBannerImageWithTemplate(token, info), {
			request: _constants2.default.CHANGE_COMPANY_BANNER_IMAGE_WITH_TEMPLATE,
			success: _constants2.default.CHANGE_COMPANY_BANNER_IMAGE_WITH_TEMPLATE_SUCCESS,
			failure: _constants2.default.CHANGE_COMPANY_BANNER_IMAGE_WITH_TEMPLATE_FAIL
		});
	},
	reviewCompany: function reviewCompany(token, info) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.reviewCompany(token, info), {
			request: _constants2.default.REVIEW_COMPANY,
			success: _constants2.default.REVIEW_COMPANY_SUCCESS,
			failure: _constants2.default.REVIEW_COMPANY_FAIL
		});
	}
};

exports.default = CompanyProfileActionCreators;