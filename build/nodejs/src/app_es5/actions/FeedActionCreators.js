'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _FeedAPI = require('../apis/FeedAPI');

var _FeedAPI2 = _interopRequireDefault(_FeedAPI);

var _UploadAPI = require('../apis/UploadAPI');

var _UploadAPI2 = _interopRequireDefault(_UploadAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FeedActionCreators = {
  setOwner: function setOwner(user) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.SET_OWNER_FEED,
      payload: { owner: user }
    });
  },
  setFeedDraft: function setFeedDraft(name, value) {

    var response = {};
    response[name] = value;

    _AppDispatcher2.default.dispatch({
      type: _constants2.default.EDIT_FEED_DRAFT,
      payload: { response: response }
    });
  },
  editImage: function editImage() {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.EDIT_FEED_IMAGE
    });
  },
  editVideo: function editVideo() {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.EDIT_FEED_VIDEO
    });
  },
  onReply: function onReply(comment) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.ON_REPLY,
      payload: comment
    });
  },
  resetFeed: function resetFeed() {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.RESET_FEED
    });
  },
  chooseEditFeed: function chooseEditFeed(feed, index, user) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CHOOSE_EDIT_FEED,
      payload: { feed: feed, index: index, owner: user }
    });
  },
  cancelEditFeed: function cancelEditFeed() {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CANCEL_EDIT_FEED
    });
  },
  uploadImage: function uploadImage(token, image) {
    _AppDispatcher2.default.dispatchAsync(_UploadAPI2.default.uploadImage(token, image), {
      request: _constants2.default.UPLOAD_FEED_PHOTO,
      success: _constants2.default.UPLOAD_FEED_PHOTO_SUCCESS,
      failure: _constants2.default.UPLOAD_FEED_PHOTO_FAIL
    });
  },
  deleteImage: function deleteImage(token, key) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.DELETE_FEED_PHOTO,
      payload: { image_key: key }
    });
  },
  getFeed: function getFeed(user_key, token) {
    var page = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;


    if (page == 0) {
      _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.getFeed(user_key, token, page), {
        request: _constants2.default.GET_FEED,
        success: _constants2.default.GET_FEED_SUCCESS,
        failure: _constants2.default.GET_FEED_FAIL
      });
    } else {
      _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.getFeed(user_key, token, page), {
        request: _constants2.default.GET_FEED_MORE,
        success: _constants2.default.GET_FEED_MORE_SUCCESS,
        failure: _constants2.default.GET_FEED_FAIL
      });
    }
  },
  getPhotosFeed: function getPhotosFeed(user_key, token) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.getPhotosFeed(user_key, token), {
      request: _constants2.default.GET_PHOTO_FEED,
      success: _constants2.default.GET_PHOTO_FEED_SUCCESS,
      failure: _constants2.default.GET_PHOTO_FEED_FAIL
    });
  },
  getAlbums: function getAlbums(user_key) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.getAlbums(user_key), {
      request: _constants2.default.GET_ALBUM,
      success: _constants2.default.GET_ALBUM_SUCCESS,
      failure: _constants2.default.GET_ALBUM_FAIL
    });
  },
  getPhotosInAlbum: function getPhotosInAlbum(user_key, info, token) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.getPhotosInAlbum(user_key, info, token), {
      request: _constants2.default.GET_PHOTO_IN_ALBUM,
      success: _constants2.default.GET_PHOTO_IN_ALBUM_SUCCESS,
      failure: _constants2.default.GET_PHOTO_IN_ALBUM_FAIL
    });
  },
  postFeed: function postFeed(token, info) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.postFeed(token, info), {
      request: _constants2.default.POST_FEED,
      success: _constants2.default.POST_FEED_SUCCESS,
      failure: _constants2.default.POST_FEED_FAIL
    });
  },
  editFeed: function editFeed(token, info) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.editFeed(token, info), {
      request: _constants2.default.EDIT_POST_FEED,
      success: _constants2.default.EDIT_POST_FEED_SUCCESS,
      failure: _constants2.default.POST_FEED_FAIL
    });
  },
  changePrivacy: function changePrivacy(token, info) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.changePrivacy(token, info), {
      request: _constants2.default.CHANGE_PRIVACY,
      success: _constants2.default.POST_FEED_SUCCESS,
      failure: _constants2.default.POST_FEED_FAIL
    });
  },
  deleteFeed: function deleteFeed(token, info) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.deleteFeed(token, info), {
      request: _constants2.default.DELETE_FEED,
      success: _constants2.default.DELETE_FEED_SUCCESS,
      failure: _constants2.default.DELETE_FEED_FAIL
    });
  },
  deleteBuzzFeed: function deleteBuzzFeed(token, info) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.deleteBuzzFeed(token, info), {
      request: _constants2.default.DELETE_FEED,
      success: _constants2.default.DELETE_FEED_SUCCESS,
      failure: _constants2.default.DELETE_FEED_FAIL
    });
  },
  postCommentFeed: function postCommentFeed(token, info) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.postCommentFeed(token, info), {
      request: _constants2.default.POST_COMMENT_FEED,
      success: _constants2.default.POST_COMMENT_FEED_SUCCESS,
      failure: _constants2.default.POST_COMMENT_FEED_FAIL
    });
  },
  putLikeFeed: function putLikeFeed(token, info) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.putLikeFeed(token, info), {
      request: _constants2.default.LIKE_FEED,
      success: _constants2.default.LIKE_FEED_SUCCESS,
      failure: _constants2.default.LIKE_FEED_FAIL
    });
  },
  deleteCommentFeed: function deleteCommentFeed(token, info, feed) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.deleteCommentFeed(token, info, feed), {
      request: _constants2.default.DELETE_COMMENT_FEED,
      success: _constants2.default.DELETE_COMMENT_FEED_SUCCESS,
      failure: _constants2.default.DELETE_COMMENT_FEED_FAIL
    });
  },
  postReplyComment: function postReplyComment(token, info) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.postReplyComment(token, info), {
      request: _constants2.default.POST_REPLY_COMMENT_FEED,
      success: _constants2.default.POST_REPLY_COMMENT_FEED_SUCCESS,
      failure: _constants2.default.POST_REPLY_COMMENT_FEED_FAIL
    });
  },
  deleteReply: function deleteReply(token, info, reply) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.deleteReply(token, info, reply), {
      request: _constants2.default.DELETE_REPLY_COMMENT_FEED,
      success: _constants2.default.DELETE_REPLY_COMMENT_FEED_SUCCESS,
      failure: _constants2.default.DELETE_REPLY_COMMENT_FEED_FAIL
    });
  },
  getBuzzFeed: function getBuzzFeed() {
    var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;


    if (page === 0) {
      _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.getBuzzFeed(page), {
        request: _constants2.default.GET_BUZZ_FEED,
        success: _constants2.default.GET_BUZZ_FEED_SUCCESS,
        failure: _constants2.default.GET_BUZZ_FEED_FAIL
      });
    } else {
      _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.getBuzzFeed(page), {
        request: _constants2.default.GET_BUZZ_FEED_MORE,
        success: _constants2.default.GET_BUZZ_FEED_MORE_SUCCESS,
        failure: _constants2.default.GET_BUZZ_FEED_FAIL
      });
    }
  },
  searchBuzzFeed: function searchBuzzFeed(keyword) {
    _AppDispatcher2.default.dispatchAsync(_FeedAPI2.default.searchBuzzFeed(keyword), {
      request: _constants2.default.SEARCH_BUZZ_FEED,
      success: _constants2.default.SEARCH_BUZZ_FEED_SUCCESS,
      failure: _constants2.default.SEARCH_BUZZ_FEED_FAIL
    });
  }
};

exports.default = FeedActionCreators;