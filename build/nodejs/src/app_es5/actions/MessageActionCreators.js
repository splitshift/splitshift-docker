'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _MessageAPI = require('../apis/MessageAPI');

var _MessageAPI2 = _interopRequireDefault(_MessageAPI);

var _UploadAPI = require('../apis/UploadAPI');

var _UploadAPI2 = _interopRequireDefault(_UploadAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MessageActionCreators = {
  setMessageDraft: function setMessageDraft(type, value, people_id) {
    var response = {};
    response.type = type;
    response.message = value;
    response.to = people_id;

    _AppDispatcher2.default.dispatch({
      type: _constants2.default.EDIT_MESSAGE_DRAFT,
      payload: { response: response }
    });
  },
  chooseOwner: function chooseOwner(user) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CHOOSE_OWNER_MESSAGE,
      payload: { response: user }
    });
  },
  chooseOther: function chooseOther(profile) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CHOOSE_OTHER_MESSAGE,
      payload: { response: profile }
    });
  },
  chooseMessage: function chooseMessage(message) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.GET_MESSAGE_SUCCESS,
      payload: { response: message }
    });
  },
  sendFile: function sendFile(token, type, value, people_id) {

    this.setMessageDraft(type, value, people_id);

    if (type == 'image') {
      _AppDispatcher2.default.dispatchAsync(_UploadAPI2.default.uploadImage(token, value), {
        request: _constants2.default.UPLOAD_MESSAGE_IMAGE,
        success: _constants2.default.UPLOAD_MESSAGE_IMAGE_SUCCESS,
        failure: _constants2.default.UPLOAD_MESSAGE_FILE_FAIL
      });
    } else {
      _AppDispatcher2.default.dispatchAsync(_UploadAPI2.default.uploadFile(token, value), {
        request: _constants2.default.UPLOAD_MESSAGE_FILE,
        success: _constants2.default.UPLOAD_MESSAGE_FILE_SUCCESS,
        failure: _constants2.default.UPLOAD_MESSAGE_FILE_FAIL
      });
    }
  },
  sendMessage: function sendMessage(token, info) {
    _AppDispatcher2.default.dispatchAsync(_MessageAPI2.default.sendMessage(token, info), {
      request: _constants2.default.SEND_MESSAGE,
      success: _constants2.default.SEND_MESSAGE_SUCCESS,
      failure: _constants2.default.SEND_MESSAGE_FAIL
    });
  },
  getAllMessage: function getAllMessage(token) {
    _AppDispatcher2.default.dispatchAsync(_MessageAPI2.default.getAllMessage(token), {
      request: _constants2.default.GET_ALL_MESSAGE,
      success: _constants2.default.GET_ALL_MESSAGE_SUCCESS,
      failure: _constants2.default.GET_ALL_MESSAGE_FAIL
    });
  },
  getMessageWithPeople: function getMessageWithPeople(token, people_id) {
    _AppDispatcher2.default.dispatchAsync(_MessageAPI2.default.getMessageWithPeople(token, people_id), {
      request: _constants2.default.GET_MESSAGE,
      success: _constants2.default.GET_MESSAGE_SUCCESS,
      failure: _constants2.default.GET_MESSAGE_FAIL
    });
  }
};

exports.default = MessageActionCreators;