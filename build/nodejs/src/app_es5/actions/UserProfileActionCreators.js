'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _UserProfileAPI = require('../apis/UserProfileAPI');

var _UserProfileAPI2 = _interopRequireDefault(_UserProfileAPI);

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserProfileActionCreators = {
	getUserProfile: function getUserProfile(key, token) {

		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.getUserProfile(key, token), {
			request: _constants2.default.GET_USER_PROFILE,
			success: _constants2.default.GET_USER_PROFILE_SUCCESS,
			failure: _constants2.default.GET_USER_PROFILE_FAIL
		});
	},
	reloadUserProfile: function reloadUserProfile(key) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.getUserProfile(key), {
			request: _constants2.default.RELOAD_USER_PROFILE,
			success: _constants2.default.GET_USER_PROFILE_SUCCESS,
			failure: _constants2.default.GET_USER_PROFILE_FAIL
		});
	},
	updateProfile: function updateProfile(info) {
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.UPDATE_PROFILE,
			payload: {
				response: info
			}
		});
	},
	resetProfile: function resetProfile() {
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.RESET_USER_PROFILE
		});
	},
	updateInfo: function updateInfo(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.updateInfo(token, info), {
			request: _constants2.default.UPDATE_INFO_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	updateAbout: function updateAbout(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.updateAbout(token, info), {
			request: _constants2.default.UPDATE_ABOUT_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	createInterest: function createInterest(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.createInterest(token, info), {
			request: _constants2.default.UPDATE_INTEREST_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	editInterest: function editInterest(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.editInterest(token, info), {
			request: _constants2.default.UPDATE_INTEREST_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	deleteInterest: function deleteInterest(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.deleteInterest(token, info), {
			request: _constants2.default.DELETE_INTEREST_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	createExperience: function createExperience(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.createExperience(token, info), {
			request: _constants2.default.UPDATE_EXPERIENCE_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	editExperience: function editExperience(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.editExperience(token, info), {
			request: _constants2.default.UPDATE_EXPERIENCE_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	deleteExperience: function deleteExperience(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.deleteExperience(token, info), {
			request: _constants2.default.DELETE_EXPERIENCE_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	createEducation: function createEducation(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.createEducation(token, info), {
			request: _constants2.default.UPDATE_EDUCATION_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	editEducation: function editEducation(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.editEducation(token, info), {
			request: _constants2.default.UPDATE_EDUCATION_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	deleteEducation: function deleteEducation(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.deleteEducation(token, info), {
			request: _constants2.default.DELETE_EDUCATION_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	updateSkill: function updateSkill(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.updateSkill(token, info), {
			request: _constants2.default.UPDATE_SKILL_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	createCourse: function createCourse(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.createCourse(token, info), {
			request: _constants2.default.UPDATE_COURSE_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	editCourse: function editCourse(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.editCourse(token, info), {
			request: _constants2.default.UPDATE_COURSE_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	deleteCourse: function deleteCourse(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.deleteCourse(token, info), {
			request: _constants2.default.DELETE_COURSE_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	createLanguage: function createLanguage(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.createLanguage(token, info), {
			request: _constants2.default.UPDATE_LANGUAGE_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	editLanguage: function editLanguage(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.editLanguage(token, info), {
			request: _constants2.default.UPDATE_LANGUAGE_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	deleteLanguage: function deleteLanguage(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.deleteLanguage(token, info), {
			request: _constants2.default.DELETE_LANGUAGE_PROFILE,
			success: _constants2.default.UPDATE_PROFILE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	uploadProfileImage: function uploadProfileImage(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.uploadProfileImage(token, info), {
			request: _constants2.default.UPLOAD_PROFILE_IMAGE,
			success: _constants2.default.UPLOAD_PROFILE_IMAGE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	uploadBannerImage: function uploadBannerImage(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.uploadBannerImage(token, info), {
			request: _constants2.default.UPLOAD_BANNER_IMAGE,
			success: _constants2.default.UPLOAD_BANNER_IMAGE_SUCCESS,
			failure: _constants2.default.UPDATE_PROFILE_ERROR
		});
	},
	changeBannerImageWithTemplate: function changeBannerImageWithTemplate(token, info) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.changeBannerImageWithTemplate(token, info), {
			request: _constants2.default.CHANGE_USER_BANNER_IMAGE_WITH_TEMPLATE,
			success: _constants2.default.CHANGE_USER_BANNER_IMAGE_WITH_TEMPLATE_SUCCESS,
			failure: _constants2.default.CHANGE_USER_BANNER_IMAGE_WITH_TEMPLATE_FAIL
		});
	},
	loadCompany: function loadCompany() {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.loadCompany(), {
			request: _constants2.default.LOAD_ALL_COMPANY,
			success: _constants2.default.LOAD_ALL_COMPANY_SUCCESS,
			failure: _constants2.default.LOAD_ALL_COMPANY_FAIL
		});
	}
};

exports.default = UserProfileActionCreators;