'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _ResourceAPI = require('../apis/ResourceAPI');

var _ResourceAPI2 = _interopRequireDefault(_ResourceAPI);

var _UploadAPI = require('../apis/UploadAPI');

var _UploadAPI2 = _interopRequireDefault(_UploadAPI);

var _AdminAPI = require('../apis/AdminAPI');

var _AdminAPI2 = _interopRequireDefault(_AdminAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ResourceActionCreators = {
  setAdminEditor: function setAdminEditor(name, status) {
    var response = {};
    response[name] = status;

    _AppDispatcher2.default.dispatch({
      type: _constants2.default.SET_ADMIN_EDITOR,
      payload: { response: response }
    });
  },
  setLanguage: function setLanguage(lang) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.SET_LANGUAGE,
      payload: {
        response: {
          language: lang
        }
      }
    });
  },
  setEditor: function setEditor(name, status) {

    var response = {};
    response[name] = status;

    _AppDispatcher2.default.dispatch({
      type: _constants2.default.EDIT_PROFILE_CARD,
      payload: { response: response }
    });
  },
  cancelEditor: function cancelEditor(name, status) {
    var response = {};
    response[name] = status;

    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CANCEL_PROFILE_EDIT,
      payload: { response: response }
    });
  },
  setDraft: function setDraft(name, value) {

    var response = {};
    response[name] = value;

    _AppDispatcher2.default.dispatch({
      type: _constants2.default.EDIT_DRAFT,
      payload: { response: response }
    });
  },
  setOldDraft: function setOldDraft(response) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.EDIT_DRAFT,
      payload: { response: response }
    });
  },


  // Resource
  resetResource: function resetResource() {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.RESET_RESOURCE
    });
  },
  editResourceDraft: function editResourceDraft(response) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.EDIT_RESOURCE_DRAFT,
      payload: { response: response }
    });
  },
  changeFilter: function changeFilter(response) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.EDIT_FILTER_RESOURCE_SEARCH,
      payload: { response: response }
    });
  },
  updateFeaturedImage: function updateFeaturedImage(token, info) {
    _AppDispatcher2.default.dispatchAsync(_UploadAPI2.default.uploadImage(token, info), {
      request: _constants2.default.UPLOAD_FEATURED_IMAGE,
      success: _constants2.default.UPLOAD_FEATURED_IMAGE_SUCCESS,
      failure: _constants2.default.UPLOAD_FEATURED_IMAGE_FAIL
    });
  },
  getAllResource: function getAllResource() {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.getResourceIndex(), {
      request: _constants2.default.GET_ALL_RESOURCE,
      success: _constants2.default.GET_ALL_RESOURCE_SUCCESS,
      failure: _constants2.default.GET_ALL_RESOURCE_FAIL
    });
  },
  getAdminResource: function getAdminResource(token) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.getAdminResource(token), {
      request: _constants2.default.GET_RESOURCE_ADMIN,
      success: _constants2.default.GET_RESOURCE_ADMIN_SUCCESS,
      failure: _constants2.default.GET_RESOURCE_ADMIN_FAIL
    });
  },
  getResources: function getResources(token) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.getResources(token), {
      request: _constants2.default.GET_RESOURCE_OTHER,
      success: _constants2.default.GET_RESOURCE_OTHER_SUCCESS,
      failure: _constants2.default.GET_RESOURCE_OTHER_FAIL
    });
  },
  getResource: function getResource(id, token) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.getResource(id, token), {
      request: _constants2.default.GET_RESOURCE_SINGLE,
      success: _constants2.default.GET_RESOURCE_SINGLE_SUCCESS,
      failure: _constants2.default.GET_RESOURCE_SINGLE_FAIL
    });
  },
  approveResource: function approveResource(token, resource) {
    _AppDispatcher2.default.dispatchAsync(_AdminAPI2.default.approveResource(token, resource), {
      request: _constants2.default.POST_APPROVE_RESOURCE,
      success: _constants2.default.POST_APPROVE_RESOURCE_SUCCESS,
      failure: _constants2.default.POST_APPROVE_RESOURCE_FAIL
    });
  },
  searchResource: function searchResource(info) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.searchResource(info), {
      request: _constants2.default.SEARCH_RESOURCE,
      success: _constants2.default.SEARCH_RESOURCE_SUCCESS,
      failure: _constants2.default.SEARCH_RESOURCE_FAIL
    });
  },
  createResource: function createResource(token, info) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.createResource(token, info), {
      request: _constants2.default.ADD_RESOURCE,
      success: _constants2.default.MANAGE_RESOURCE_SUCCESS,
      failure: _constants2.default.MANAGE_RESOURCE_FAIL
    });
  },
  editResource: function editResource(token, info) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.editResource(token, info), {
      request: _constants2.default.EDIT_RESOURCE,
      success: _constants2.default.MANAGE_RESOURCE_SUCCESS,
      failure: _constants2.default.MANAGE_RESOURCE_FAIL
    });
  },
  deleteResource: function deleteResource(token, info) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.deleteResource(token, info), {
      request: _constants2.default.DELETE_RESOURCE,
      success: _constants2.default.MANAGE_RESOURCE_SUCCESS,
      failure: _constants2.default.MANAGE_RESOURCE_FAIL
    });
  },
  likeResource: function likeResource(token, info) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.likeResource(token, info), {
      request: _constants2.default.LIKE_RESOURCE,
      success: _constants2.default.LIKE_RESOURCE_SUCCESS,
      failure: _constants2.default.LIKE_RESOURCE_FAIL
    });
  },
  commentResource: function commentResource(token, info, resource_id) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.commentResource(token, info, resource_id), {
      request: _constants2.default.COMMENT_RESOURCE,
      success: _constants2.default.COMMENT_RESOURCE_SUCCESS,
      failure: _constants2.default.COMMENT_RESOURCE_FAIL
    });
  },
  deleteCommentResource: function deleteCommentResource(token, info, resource_id) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.deleteCommentResource(token, info, resource_id), {
      request: _constants2.default.DELETE_COMMENT_RESOURCE,
      success: _constants2.default.DELETE_COMMENT_RESOURCE_SUCCESS,
      failure: _constants2.default.DELETE_COMMENT_RESOURCE_FAIL
    });
  },
  onFilterResource: function onFilterResource(department) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.FILTER_RESOURCE,
      payload: { department: department }
    });
  },
  forwardEmail: function forwardEmail(token, info) {
    _AppDispatcher2.default.dispatchAsync(_ResourceAPI2.default.forwardEmail(token, info), {
      request: _constants2.default.SEND_RESOURCE_FORWARD_EMAIL,
      success: _constants2.default.SEND_RESOURCE_FORWARD_EMAIL_SUCCESS,
      failure: _constants2.default.SEND_RESOURCE_FORWARD_EMAIL_FAIL
    });
  }
};

exports.default = ResourceActionCreators;