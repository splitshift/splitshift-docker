'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _HRAPI = require('../apis/HRAPI');

var _HRAPI2 = _interopRequireDefault(_HRAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HRManagementActionCreators = {
  getEmployee: function getEmployee(company_key) {
    _AppDispatcher2.default.dispatchAsync(_HRAPI2.default.getEmployee(company_key), {
      request: _constants2.default.GET_EMPLOYEE_IN_COMPANY,
      success: _constants2.default.GET_EMPLOYEE_IN_COMPANY_SUCCESS,
      failure: _constants2.default.GET_EMPLOYEE_IN_COMPANY_FAIL
    });
  },
  changeRank: function changeRank(token, info) {
    _AppDispatcher2.default.dispatchAsync(_HRAPI2.default.changeRank(token, info), {
      request: _constants2.default.CHANGE_RANK,
      success: _constants2.default.CHANGE_RANK_SUCCESS,
      failure: _constants2.default.CHANGE_RANK_FAIL
    });
  }
};

exports.default = HRManagementActionCreators;