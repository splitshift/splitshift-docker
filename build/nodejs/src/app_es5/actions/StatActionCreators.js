'use strict';

Object.defineProperty(exports, "__esModule", {
		value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _reactRouter = require('react-router');

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _StatAPI = require('../apis/StatAPI');

var _StatAPI2 = _interopRequireDefault(_StatAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StatActionCreators = {
		getStat: function getStat() {
				_AppDispatcher2.default.dispatchAsync(_StatAPI2.default.getStat(), {
						request: _constants2.default.GET_STAT,
						success: _constants2.default.GET_STAT_SUCCESS,
						failure: _constants2.default.GET_STAT_FAIL
				});
		},
		getAdminSummary: function getAdminSummary(token) {
				_AppDispatcher2.default.dispatchAsync(_StatAPI2.default.getAdminSummary(token), {
						request: _constants2.default.GET_ADMIN_DASHBOARD,
						success: _constants2.default.GET_ADMIN_DASHBOARD_SUCCESS,
						failure: _constants2.default.GET_ADMIN_DASHBOARD_FAIL
				});
		},
		getUserSummary: function getUserSummary(token) {
				_AppDispatcher2.default.dispatchAsync(_StatAPI2.default.getUserSummary(token), {
						request: _constants2.default.GET_USER_DASHBOARD,
						success: _constants2.default.GET_USER_DASHBOARD_SUCCESS,
						failure: _constants2.default.GET_USER_DASHBOARD_FAIL
				});
		}
};

exports.default = StatActionCreators;