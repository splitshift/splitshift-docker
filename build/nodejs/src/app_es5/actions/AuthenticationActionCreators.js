'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _reactRouter = require('react-router');

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _LoginAPI = require('../apis/LoginAPI');

var _LoginAPI2 = _interopRequireDefault(_LoginAPI);

var _RegisterAPI = require('../apis/RegisterAPI');

var _RegisterAPI2 = _interopRequireDefault(_RegisterAPI);

var _UserProfileAPI = require('../apis/UserProfileAPI');

var _UserProfileAPI2 = _interopRequireDefault(_UserProfileAPI);

var _CompanyProfileAPI = require('../apis/CompanyProfileAPI');

var _CompanyProfileAPI2 = _interopRequireDefault(_CompanyProfileAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AuthenticationActionCreators = {
	guest: function guest() {
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.AS_GUEST
		});
	},
	login: function login(info) {
		_AppDispatcher2.default.dispatchAsync(_LoginAPI2.default.login(info), {
			request: _constants2.default.LOGIN,
			success: _constants2.default.LOGIN_SUCCESS,
			failure: _constants2.default.LOGIN_FAIL
		});
	},
	updateLogin: function updateLogin(info) {
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.UPDATE_LOGIN,
			payload: { info: info }
		});
	},
	loginAlready: function loginAlready() {

		// Get lastest session data
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.LOGIN_ALREADY,
			payload: {
				response: {
					'token': localStorage.getItem('token'),
					'key': localStorage.getItem('key'),
					'type': localStorage.getItem('type')
				}
			}
		});
	},
	logout: function logout() {
		// Remove session data
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.LOGOUT,
			payload: {
				'token': localStorage.getItem('token'),
				'key': localStorage.getItem('key'),
				'type': localStorage.getItem('type')
			}
		});
	},
	createRegister: function createRegister(info) {
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.REGISTER_UPDATE,
			payload: { info: info }
		});
	},
	updateRegister: function updateRegister(info) {
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.REGISTER_UPDATE,
			payload: { info: info }
		});
	},
	register: function register(info) {
		// Verrify Data
		_AppDispatcher2.default.dispatchAsync(_RegisterAPI2.default.register(info), {
			request: _constants2.default.REGISTER,
			success: _constants2.default.REGISTER_SUCCESS,
			failure: _constants2.default.REGISTER_FAIL
		});
	},
	loadProfile: function loadProfile(key) {
		_AppDispatcher2.default.dispatchAsync(_UserProfileAPI2.default.getUserProfile(key), {
			request: _constants2.default.GET_USER_OWNER_PROFILE,
			success: _constants2.default.GET_USER_OWNER_PROFILE_SUCCESS,
			failure: _constants2.default.GET_USER_PROFILE_FAIL
		});
	},
	loadCompanyProfile: function loadCompanyProfile(key) {
		_AppDispatcher2.default.dispatchAsync(_CompanyProfileAPI2.default.getProfile(key), {
			request: _constants2.default.GET_USER_OWNER_PROFILE,
			success: _constants2.default.GET_USER_OWNER_PROFILE_SUCCESS,
			failure: _constants2.default.GET_USER_PROFILE_FAIL
		});
	},
	forgetPassword: function forgetPassword(info) {
		_AppDispatcher2.default.dispatchAsync(_RegisterAPI2.default.forgetPassword(info), {
			request: _constants2.default.FORGET_PASSWORD,
			success: _constants2.default.FORGET_PASSWORD_SUCCESS,
			failure: _constants2.default.FORGET_PASSWORD_FAIL
		});
	},
	changePasswordFormForgot: function changePasswordFormForgot(info, token) {
		_AppDispatcher2.default.dispatchAsync(_RegisterAPI2.default.changePasswordFormForgot(info, token), {
			request: _constants2.default.CHANGE_FORGET_PASSWORD,
			success: _constants2.default.CHANGE_FORGET_PASSWORD_SUCCESS,
			failure: _constants2.default.CHANGE_FORGET_PASSWORD_FAIL
		});
	}
};

exports.default = AuthenticationActionCreators;