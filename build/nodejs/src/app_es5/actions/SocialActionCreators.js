'use strict';

Object.defineProperty(exports, "__esModule", {
		value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _reactRouter = require('react-router');

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _SocialAPI = require('../apis/SocialAPI');

var _SocialAPI2 = _interopRequireDefault(_SocialAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SocialActionCreators = {
		putFollow: function putFollow(token, user_key) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.putFollow(token, user_key), {
						request: _constants2.default.FOLLOW_PEOPLE,
						success: _constants2.default.FOLLOW_PEOPLE_SUCCESS,
						failure: _constants2.default.FOLLOW_PEOPLE_FAIL
				});
		},
		setDraft: function setDraft(key, value) {

				var response = {};
				response[key] = value;

				_AppDispatcher2.default.dispatch({
						type: _constants2.default.SET_SOCIAL_DRAFT,
						payload: { response: response }
				});
		},
		getConnectRequestList: function getConnectRequestList(token) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.getConnectRequestList(token), {
						request: _constants2.default.GET_REQUEST_PEOPLE,
						success: _constants2.default.GET_REQUEST_PEOPLE_SUCCESS,
						failure: _constants2.default.GET_REQUEST_PEOPLE_FAIL
				});
		},
		getNotification: function getNotification(token) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.getNotification(token), {
						request: _constants2.default.GET_NOTIFICATION,
						success: _constants2.default.GET_NOTIFICATION_SUCCESS,
						failure: _constants2.default.GET_NOTIFICATION_FAIL
				});
		},
		putNotificationSeen: function putNotificationSeen(token) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.putNotificationSeen(token), {
						request: _constants2.default.SEEN_NOTIFICATION,
						success: _constants2.default.SEEN_NOTIFICATION_SUCCESS,
						failure: _constants2.default.SEEN_NOTIFICATION_FAIL
				});
		},
		deleteNotification: function deleteNotification(token, notification_id) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.deleteNotification(token, notification_id), {
						request: _constants2.default.DELETE_NOTIFICATION,
						success: _constants2.default.DELETE_NOTIFICATION_SUCCESS,
						failure: _constants2.default.DELETE_NOTIFICATION_FAIL
				});
		},
		postConnectRequest: function postConnectRequest(token, user_key) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.postConnectRequest(token, user_key), {
						request: _constants2.default.POST_SEND_REQUEST_PEOPLE,
						success: _constants2.default.REQUEST_PEOPLE_SUCCESS,
						failure: _constants2.default.REQUEST_PEOPLE_FAIL
				});
		},
		putConnectResponse: function putConnectResponse(token, info) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.putConnectResponse(token, info), {
						request: _constants2.default.PUT_RESPONSE_PEOPLE,
						success: _constants2.default.REQUEST_PEOPLE_SUCCESS,
						failure: _constants2.default.REQUEST_PEOPLE_FAIL
				});
		},
		deleteConnect: function deleteConnect(token, user_key) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.deleteConnect(token, user_key), {
						request: _constants2.default.DELETE_RESPONSE_PEOPLE,
						success: _constants2.default.REQUEST_PEOPLE_SUCCESS,
						failure: _constants2.default.REQUEST_PEOPLE_FAIL
				});
		},
		getFollowers: function getFollowers(user_key) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.getFollowers(user_key), {
						request: _constants2.default.GET_FOLLOWER,
						success: _constants2.default.GET_FOLLOWER_SUCCESS,
						failure: _constants2.default.GET_FOLLOWER_FAIL
				});
		},
		getConnections: function getConnections(user_key) {
				_AppDispatcher2.default.dispatchAsync(_SocialAPI2.default.getConnections(user_key), {
						request: _constants2.default.GET_CONNECTIONS,
						success: _constants2.default.GET_CONNECTIONS_SUCCESS,
						failure: _constants2.default.GET_CONNECTIONS_FAIL
				});
		}
};

exports.default = SocialActionCreators;