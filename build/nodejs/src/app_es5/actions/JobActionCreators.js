'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _UploadAPI = require('../apis/UploadAPI');

var _UploadAPI2 = _interopRequireDefault(_UploadAPI);

var _JobAPI = require('../apis/JobAPI');

var _JobAPI2 = _interopRequireDefault(_JobAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var JobActionCreators = {
  chooseJob: function chooseJob(job_index) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CHOOSE_JOB_COMPANY,
      payload: {
        response: job_index
      }
    });
  },
  updateJobDraft: function updateJobDraft(info) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.UPDATE_JOB_DRAFT,
      payload: {
        response: info
      }
    });
  },
  resetDraft: function resetDraft() {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.RESET_JOB_DRAFT
    });
  },
  changeApplyJobFilter: function changeApplyJobFilter(filter) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CHANGE_APPLY_JOB_FILTER,
      payload: {
        response: filter
      }
    });
  },
  updateApplyJobDraft: function updateApplyJobDraft(info) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.UPDATE_APPLY_JOB_DRAFT,
      payload: {
        response: info
      }
    });
  },
  uploadFileApplyJob: function uploadFileApplyJob(token, info) {
    _AppDispatcher2.default.dispatchAsync(_UploadAPI2.default.uploadFile(token, info), {
      request: _constants2.default.UPLOAD_APPLY_FILE,
      success: _constants2.default.UPLOAD_APPLY_FILE_SUCCESS,
      failure: _constants2.default.UPLOAD_APPLY_FILE_FAIL
    });
  },
  getJob: function getJob(token, job_id) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.getJob(token, job_id), {
      request: _constants2.default.GET_JOB,
      success: _constants2.default.GET_JOB_SUCCESS,
      failure: _constants2.default.GET_JOB_FAIL
    });
  },
  getAllJob: function getAllJob(token) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.getAllJob(token), {
      request: _constants2.default.GET_ALL_JOB,
      success: _constants2.default.GET_ALL_JOB_SUCCESS,
      failure: _constants2.default.GET_ALL_JOB_FAIL
    });
  },
  createNewJob: function createNewJob(token, info) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.postNewJob(token, info), {
      request: _constants2.default.POST_NEW_JOB,
      success: _constants2.default.UPDATE_JOB_SUCCESS,
      failure: _constants2.default.UPDATE_JOB_FAIL
    });
  },
  updateJob: function updateJob(token, info) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.putUpdateJob(token, info), {
      request: _constants2.default.UPDATE_JOB,
      success: _constants2.default.UPDATE_JOB_SUCCESS,
      failure: _constants2.default.UPDATE_JOB_FAIL
    });
  },
  deleteJob: function deleteJob(token, info) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.deleteJob(token, info), {
      request: _constants2.default.DELETE_JOB,
      success: _constants2.default.UPDATE_JOB_SUCCESS,
      failure: _constants2.default.UPDATE_JOB_FAIL
    });
  },
  activateJob: function activateJob(token, job_id) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.activateJob(token, job_id), {
      request: _constants2.default.UPDATE_ACTIVATE_JOB,
      success: _constants2.default.UPDATE_ACTIVATE_JOB_SUCCESS,
      failure: _constants2.default.UPDATE_ACTIVATE_JOB_FAIL
    });
  },
  getAllApplyJob: function getAllApplyJob(token) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.getAllApplyJob(token), {
      request: _constants2.default.GET_ALL_APPLY_JOB,
      success: _constants2.default.GET_APPLY_JOB_SUCCESS,
      failure: _constants2.default.GET_APPLY_JOB_FAIL
    });
  },
  getApplyJob: function getApplyJob(token, info) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.getApplyJob(token, info), {
      request: _constants2.default.GET_APPLY_JOB,
      success: _constants2.default.GET_APPLY_JOB_SUCCESS,
      failure: _constants2.default.GET_APPLY_JOB_FAIL
    });
  },
  searchApplyJob: function searchApplyJob(search) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.SEARCH_APPLY_JOB,
      payload: search
    });
  },
  getEmployeeApplyJob: function getEmployeeApplyJob(token) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.getEmployeeApplyJob(token), {
      request: _constants2.default.GET_EMPLOYEE_APPLY_JOB,
      success: _constants2.default.GET_EMPLOYEE_APPLY_JOB_SUCCESS,
      failure: _constants2.default.GET_EMPLOYEE_APPLY_JOB_FAIL
    });
  },
  sendApplyJob: function sendApplyJob(token, info) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.postApplyJob(token, info), {
      request: _constants2.default.POST_APPLY_JOB,
      success: _constants2.default.POST_APPLY_JOB_SUCCESS,
      failure: _constants2.default.POST_APPLY_JOB_FAIL
    });
  },
  changeAppleJobStatus: function changeAppleJobStatus(token, applyjob_id, info) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.changeAppleJobStatus(token, applyjob_id, info), {
      request: _constants2.default.CHANGE_APPLY_JOB_STATUS,
      success: _constants2.default.CHANGE_APPLY_JOB_STATUS_SUCCESS,
      failure: _constants2.default.CHANGE_APPLY_JOB_STATUS_FAIL
    });
  },
  deleteApplyJob: function deleteApplyJob(token, apply_job_id) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.deleteApplyJob(token, apply_job_id), {
      request: _constants2.default.DELETE_APPLY_JOB,
      success: _constants2.default.DELETE_APPLY_JOB_SUCCESS,
      failure: _constants2.default.DELETE_APPLY_JOB_FAIL
    });
  },
  deleteApplication: function deleteApplication(token, apply_job_id) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.deleteApplication(token, apply_job_id), {
      request: _constants2.default.DELETE_APPLY_JOB,
      success: _constants2.default.DELETE_APPLY_JOB_SUCCESS,
      failure: _constants2.default.DELETE_APPLY_JOB_FAIL
    });
  },
  forwardEmail: function forwardEmail(token, info) {
    _AppDispatcher2.default.dispatchAsync(_JobAPI2.default.forwardEmail(token, info), {
      request: _constants2.default.SEND_FORWARD_EMAIL,
      success: _constants2.default.SEND_FORWARD_EMAIL_SUCCESS,
      failure: _constants2.default.SEND_FORWARD_EMAIL_FAIL
    });
  }
};

exports.default = JobActionCreators;