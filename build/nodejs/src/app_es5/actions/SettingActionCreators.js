'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _SettingAPI = require('../apis/SettingAPI');

var _SettingAPI2 = _interopRequireDefault(_SettingAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SettingActionCreators = {
  changeDraft: function changeDraft(response) {
    _AppDispatcher2.default.dispatch({
      type: _constants2.default.CHANGE_SETTING_DRAFT,
      payload: { response: response }
    });
  },
  deleteAccount: function deleteAccount(token) {
    _AppDispatcher2.default.dispatchAsync(_SettingAPI2.default.deleteAccount(token), {
      request: _constants2.default.DELETE_ACCOUNT,
      success: _constants2.default.DELETE_ACCOUNT_SUCCESS,
      failure: _constants2.default.DELETE_ACCOUNT_FAIL
    });
  },
  changePassword: function changePassword(token, info) {
    _AppDispatcher2.default.dispatchAsync(_SettingAPI2.default.changePassword(token, info), {
      request: _constants2.default.CHANGE_PASSWORD,
      success: _constants2.default.CHANGE_PASSWORD_SUCCESS,
      failure: _constants2.default.CHANGE_PASSWORD_FAIL
    });
  },
  changePrimaryEmail: function changePrimaryEmail(token, info) {
    _AppDispatcher2.default.dispatchAsync(_SettingAPI2.default.changePrimaryEmail(token, info), {
      request: _constants2.default.CHANGE_PRIMARY_EMAIL,
      success: _constants2.default.CHANGE_PRIMARY_EMAIL_SUCCESS,
      failure: _constants2.default.CHANGE_PRIMARY_EMAIL_FAIL
    });
  }
};

exports.default = SettingActionCreators;