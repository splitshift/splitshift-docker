'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _reactRouter = require('react-router');

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _AdminAPI = require('../apis/AdminAPI');

var _AdminAPI2 = _interopRequireDefault(_AdminAPI);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AdminManagerActionCreators = {
	getUsers: function getUsers(token) {
		_AppDispatcher2.default.dispatchAsync(_AdminAPI2.default.getUsers(token), {
			request: _constants2.default.GET_ADMIN_ALL_USER,
			success: _constants2.default.GET_ADMIN_ALL_USER_SUCCESS,
			failure: _constants2.default.GET_ADMIN_ALL_USER_FAIL
		});
	},
	searchUser: function searchUser(keyword) {
		_AppDispatcher2.default.dispatch({
			type: _constants2.default.SEARCH_ADMIN_USER,
			payload: { keyword: keyword }
		});
	},
	deleteUser: function deleteUser(token, user) {
		_AppDispatcher2.default.dispatchAsync(_AdminAPI2.default.deleteUser(token, user), {
			request: _constants2.default.DELETE_USER,
			success: _constants2.default.DELETE_USER_SUCCESS,
			failure: _constants2.default.DELETE_USER_FAIL
		});
	}
};

exports.default = AdminManagerActionCreators;