'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _backendRoutes = require('./backendRoutes');

var _backendRoutes2 = _interopRequireDefault(_backendRoutes);

var _adminRoutes = require('./adminRoutes');

var _adminRoutes2 = _interopRequireDefault(_adminRoutes);

var _ConfirmEmail = require('./components/ConfirmEmail');

var _ConfirmEmail2 = _interopRequireDefault(_ConfirmEmail);

var _Privacy = require('./components/Frontend/Privacy');

var _Privacy2 = _interopRequireDefault(_Privacy);

var _UserAgreement = require('./components/Frontend/UserAgreement');

var _UserAgreement2 = _interopRequireDefault(_UserAgreement);

var _AppContainer = require('./containers/AppContainer');

var _AppContainer2 = _interopRequireDefault(_AppContainer);

var _HomeContainer = require('./containers/HomeContainer');

var _HomeContainer2 = _interopRequireDefault(_HomeContainer);

var _AboutusContainer = require('./containers/AboutusContainer');

var _AboutusContainer2 = _interopRequireDefault(_AboutusContainer);

var _ForgetPasswordContainer = require('./containers/ForgetPasswordContainer');

var _ForgetPasswordContainer2 = _interopRequireDefault(_ForgetPasswordContainer);

var _UserProfileContainer = require('./containers/UserProfileContainer');

var _UserProfileContainer2 = _interopRequireDefault(_UserProfileContainer);

var _CompanyProfileContainer = require('./containers/CompanyProfileContainer');

var _CompanyProfileContainer2 = _interopRequireDefault(_CompanyProfileContainer);

var _UserResumeContainer = require('./containers/UserResumeContainer');

var _UserResumeContainer2 = _interopRequireDefault(_UserResumeContainer);

var _CompanyResumeContainer = require('./containers/CompanyResumeContainer');

var _CompanyResumeContainer2 = _interopRequireDefault(_CompanyResumeContainer);

var _BuzzContainer = require('./containers/BuzzContainer');

var _BuzzContainer2 = _interopRequireDefault(_BuzzContainer);

var _SettingContainer = require('./containers/SettingContainer');

var _SettingContainer2 = _interopRequireDefault(_SettingContainer);

var _SearchContainer = require('./containers/SearchContainer');

var _SearchContainer2 = _interopRequireDefault(_SearchContainer);

var _PeopleSearchContainer = require('./containers/PeopleSearchContainer');

var _PeopleSearchContainer2 = _interopRequireDefault(_PeopleSearchContainer);

var _CompanySearchContainer = require('./containers/CompanySearchContainer');

var _CompanySearchContainer2 = _interopRequireDefault(_CompanySearchContainer);

var _JobSearchContainer = require('./containers/JobSearchContainer');

var _JobSearchContainer2 = _interopRequireDefault(_JobSearchContainer);

var _ResourceContainer = require('./containers/ResourceContainer');

var _ResourceContainer2 = _interopRequireDefault(_ResourceContainer);

var _ResourceSearchContainer = require('./containers/ResourceSearchContainer');

var _ResourceSearchContainer2 = _interopRequireDefault(_ResourceSearchContainer);

var _ResourceArticleContainer = require('./containers/ResourceArticleContainer');

var _ResourceArticleContainer2 = _interopRequireDefault(_ResourceArticleContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _react2.default.createElement(
  _reactRouter.Route,
  null,
  _react2.default.createElement(
    _reactRouter.Route,
    { path: '/', component: _AppContainer2.default },
    _react2.default.createElement(_reactRouter.IndexRoute, { name: 'home', component: _HomeContainer2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'confirm_email', path: '/api/confirm/:token', component: _ConfirmEmail2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'search', path: 'search', component: _SearchContainer2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'forget_password', path: 'forgot/password/:id', component: _ForgetPasswordContainer2.default }),
    _react2.default.createElement(
      _reactRouter.Route,
      { name: 'language', path: '/:language' },
      _react2.default.createElement(_reactRouter.IndexRoute, { name: 'home', component: _HomeContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'aboutus', path: 'aboutus', component: _AboutusContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'privacy', path: 'privacy', component: _Privacy2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'user_agreement', path: 'user-agreement', component: _UserAgreement2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'people-search', path: 'people-search', component: _PeopleSearchContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'setting', path: 'setting', component: _SettingContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'job-search', path: 'job-search', component: _JobSearchContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'company-search', path: 'company-search', component: _CompanySearchContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'user_profile', path: 'profile', component: _UserProfileContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'user_public_profile', path: 'user/:key', component: _UserProfileContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'user_image_profile', path: 'user/:key/photo', component: _UserProfileContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'user_feed_single', path: 'user/:key/feed/:feed_id', component: _UserProfileContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'company_profile', path: 'company', component: _CompanyProfileContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'company_public_profile', path: 'company/:key', component: _CompanyProfileContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'company_image_profile', path: 'company/:key/photo', component: _CompanyProfileContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'company_feed_single', path: 'company/:key/feed/:feed_id', component: _CompanyProfileContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'buzz', path: 'buzz', component: _BuzzContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'resource_index', path: 'resource', component: _ResourceContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'resource_search', path: 'resource/search', component: _ResourceSearchContainer2.default }),
      _react2.default.createElement(_reactRouter.Route, { name: 'resource_article', path: 'resource/article/:id', component: _ResourceArticleContainer2.default })
    )
  ),
  _backendRoutes2.default,
  _adminRoutes2.default,
  _react2.default.createElement(
    _reactRouter.Route,
    { name: 'resume_language', path: '/:language' },
    _react2.default.createElement(_reactRouter.Route, { name: 'user_resume_profile', path: 'user/:key/resume', component: _UserResumeContainer2.default }),
    _react2.default.createElement(_reactRouter.Route, { name: 'company_resume_profile', path: 'company/:key/profile', component: _CompanyResumeContainer2.default })
  )
);