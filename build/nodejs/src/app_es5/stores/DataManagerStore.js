'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _utils = require('flux/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DataManagerStore = function (_ReduceStore) {
	_inherits(DataManagerStore, _ReduceStore);

	function DataManagerStore() {
		_classCallCheck(this, DataManagerStore);

		return _possibleConstructorReturn(this, (DataManagerStore.__proto__ || Object.getPrototypeOf(DataManagerStore)).apply(this, arguments));
	}

	_createClass(DataManagerStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				users: [],
				temp_users: [],
				loading: false
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {

			var nextState = Object.assign({}, state);

			switch (action.type) {

				case _constants2.default.GET_ADMIN_ALL_USER_SUCCESS:
					nextState.users = action.payload.response;
					nextState.temp_users = action.payload.response;
					return nextState;

				case _constants2.default.SEARCH_ADMIN_USER:
					nextState.users = state.temp_users.filter(function (user) {
						return user.email.indexOf(action.payload.keyword) > -1 || user.name.indexOf(action.payload.keyword) > -1;
					});
					return nextState;

				case _constants2.default.DELETE_USER:
				case _constants2.default.POST_APPROVE_USER_TO_RESOURCE:
					nextState.loading = true;
					return nextState;

				case _constants2.default.DELETE_USER_SUCCESS:
				case _constants2.default.POST_APPROVE_USER_TO_RESOURCE_SUCCESS:
					nextState.loading = false;
					return nextState;

				case _constants2.default.GET_ADMIN_ALL_USER_FAIL:
				case _constants2.default.POST_APPROVE_USER_TO_RESOURCE_FAIL:
				case _constants2.default.DELETE_USER_FAIL:
					alert(action.payload.error);
					console.error(action.payload.error);
					return nextState;

				default:
					return nextState;
			}
		}
	}]);

	return DataManagerStore;
}(_utils.ReduceStore);

exports.default = new DataManagerStore(_AppDispatcher2.default);