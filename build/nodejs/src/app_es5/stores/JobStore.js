'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var counter = 0;

var JobStore = function (_ReduceStore) {
	_inherits(JobStore, _ReduceStore);

	function JobStore() {
		_classCallCheck(this, JobStore);

		return _possibleConstructorReturn(this, (JobStore.__proto__ || Object.getPrototypeOf(JobStore)).apply(this, arguments));
	}

	_createClass(JobStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				draft: {},
				apply_draft: {},
				apply_jobs: [],
				full_apply_jobs: [],
				apply_job_filter: "",
				employee_apply_jobs: [],
				jobs: [],
				job_index: null,
				edit_job_id: undefined,
				updating: false,
				deleting: false,
				sending_forward_email: false
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {

			var nextState = Object.assign({}, state);

			switch (action.type) {

				case _constants2.default.GET_ALL_JOB_SUCCESS:
					nextState.jobs = action.payload.response;
					return nextState;

				case _constants2.default.GET_APPLY_JOB_SUCCESS:
					nextState.apply_jobs = action.payload.response;
					nextState.full_apply_jobs = action.payload.response;
					nextState.deleting = false;
					return nextState;

				case _constants2.default.SEARCH_APPLY_JOB:
					nextState.apply_jobs = nextState.full_apply_jobs.filter(function (apply_job) {
						return apply_job.from.name.indexOf(action.payload) !== -1 || apply_job.email.indexOf(action.payload) !== -1;
					});
					return nextState;

				case _constants2.default.GET_EMPLOYEE_APPLY_JOB_SUCCESS:
					nextState.employee_apply_jobs = action.payload.response;
					return nextState;

				case _constants2.default.GET_JOB:
					nextState.edit_job_id = undefined;
					return nextState;

				case _constants2.default.GET_JOB_SUCCESS:
					nextState.edit_job_id = action.payload.response._id;
					nextState.draft = Object.assign({}, state.draft, action.payload.response);
					nextState.draft.old_expire_date = nextState.draft.expire_date;
					return nextState;

				case _constants2.default.DELETE_APPLY_JOB:
					nextState.deleting = true;
					return nextState;

				case _constants2.default.DELETE_APPLY_JOB_SUCCESS:
					nextState.deleting = false;
					return nextState;

				case _constants2.default.CHANGE_APPLY_JOB_FILTER:
					nextState.apply_job_filter = action.payload.response;
					return nextState;

				case _constants2.default.UPDATE_JOB_DRAFT:
					nextState.draft = Object.assign({}, nextState.draft, action.payload.response);
					return nextState;

				case _constants2.default.UPDATE_JOB_SUCCESS:
				case _constants2.default.UPDATE_ACTIVATE_JOB_SUCCESS:
					nextState.draft = {};
					nextState.updating = false;
					return nextState;

				case _constants2.default.RESET_JOB_DRAFT:
					nextState.draft = {};
					nextState.draft.expire_date = new Date(new Date().getTime() + 90 * 24 * 60 * 60 * 1000);
					nextState.draft.old_expire_date = nextState.draft.expire_date;
					return nextState;

				case _constants2.default.CHOOSE_JOB_COMPANY:
					nextState.job_index = action.payload.response;
					return nextState;

				case _constants2.default.UPDATE_APPLY_JOB_DRAFT:
					nextState.apply_draft = Object.assign({}, nextState.apply_draft, action.payload.response);
					return nextState;

				case _constants2.default.UPLOAD_APPLY_FILE_SUCCESS:
					if (typeof nextState.apply_draft.files === 'undefined') nextState.apply_draft.files = [];

					nextState.apply_draft.files.push(action.payload.response);

					return nextState;

				case _constants2.default.POST_APPLY_JOB_SUCCESS:
					$("#myModal__positions").modal('hide');
					alert("Apply to this position complete!");
					return nextState;

				case _constants2.default.SEND_FORWARD_EMAIL:
					nextState.sending_forward_email = true;
					return nextState;

				case _constants2.default.SEND_FORWARD_EMAIL_SUCCESS:
					nextState.sending_forward_email = false;
					alert("Send Forward Email Successful");
					return nextState;

				case _constants2.default.CHANGE_APPLY_JOB_STATUS_SUCCESS:
					alert("Change Apply Job Status Complete!");
					return nextState;

				case _constants2.default.UPDATE_ACTIVATE_JOB:
				case _constants2.default.POST_NEW_JOB:
				case _constants2.default.UPDATE_JOB:
				case _constants2.default.DELETE_JOB:
					nextState.updating = true;
					return nextState;

				case _constants2.default.UPDATE_JOB_FAIL:
				case _constants2.default.UPLOAD_APPLY_FILE_FAIL:
				case _constants2.default.POST_APPLY_JOB_FAIL:
				case _constants2.default.GET_APPLY_JOB_FAIL:
				case _constants2.default.DELETE_APPLY_JOB_FAIL:
				case _constants2.default.SEND_FORWARD_EMAIL_FAIL:
					alert(action.payload.error);
					console.error(action.payload.error);
				default:
					return state;
			}
		}
	}]);

	return JobStore;
}(_utils.ReduceStore);

exports.default = new JobStore(_AppDispatcher2.default);