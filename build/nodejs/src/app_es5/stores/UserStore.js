'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var counter = 0;

var browserStorage = typeof localStorage === 'undefined' ? new require('node-localstorage').LocalStorage('./scratch') : localStorage;

var UserStore = function (_ReduceStore) {
	_inherits(UserStore, _ReduceStore);

	function UserStore() {
		_classCallCheck(this, UserStore);

		return _possibleConstructorReturn(this, (UserStore.__proto__ || Object.getPrototypeOf(UserStore)).apply(this, arguments));
	}

	_createClass(UserStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				token: browserStorage.getItem('token') || undefined,
				key: browserStorage.getItem('key') || undefined,
				type: browserStorage.getItem('type') || undefined,
				complete: false,
				setting: {
					old_password: "",
					new_password: "",
					new_password_again: "",
					new_primary_email: ""
				}
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {

			var nextState = Object.assign({}, state);

			switch (action.type) {
				case _constants2.default.REGISTER_UPDATE:
					console.log("Register Update");
					return action.payload.info;
				case _constants2.default.REGISTER_FAIL:
					alert(action.payload.error);
					return state;
				case _constants2.default.REGISTER_SUCCESS:
					window.location.href = '/th';
					return action.payload.response;

				case _constants2.default.LOGIN_ALREADY:
					var response = action.payload.response;

					if (response.type === 'Admin') {
						response.profile_image = '/images/profiles/admin.jpg';
						response.company_name = 'Splitshift';
						response.complete = true;
					}

					return Object.assign({}, nextState, response);

				case _constants2.default.UPLOAD_PROFILE_IMAGE_SUCCESS:
					nextState.profile_image = action.payload.response.path + "?new=" + ++counter;
					return nextState;

				case _constants2.default.GET_USER_OWNER_PROFILE_SUCCESS:
					nextState = Object.assign({}, nextState, action.payload.response);
					nextState.complete = true;
					return nextState;

				case _constants2.default.GET_USER_PROFILE_FAIL:
				case _constants2.default.UPDATE_PROFILE_ERROR:
				case _constants2.default.GET_COMPANY_PROFILE_FAIL:
				case _constants2.default.UPDATE_COMPANY_PROFILE_FAIL:
				case _constants2.default.POST_FEED_FAIL:
				case _constants2.default.POST_COMMENT_FEED_FAIL:
				case _constants2.default.GET_FEED_FAIL:
				case _constants2.default.GET_ALL_MESSAGE_FAIL:
				case _constants2.default.GET_REQUEST_PEOPLE_FAIL:
				case _constants2.default.GET_NOTIFICATION_FAIL:

					if (action.payload.error.toString().indexOf("Token") < 0 && action.payload.error.toString().indexOf("User not found") < 0) return state;

				case _constants2.default.LOGOUT:
					localStorage.removeItem('type');
					localStorage.removeItem('token');
					localStorage.removeItem('key');
					return {};

				case _constants2.default.CHANGE_SETTING_DRAFT:
					nextState.setting = Object.assign({}, state.setting, action.payload.response);
					return nextState;

				case _constants2.default.CHANGE_PRIMARY_EMAIL_SUCCESS:

					alert("Change Primary Email Successful");

					nextState.email = nextState.setting.new_primary_email;
					nextState.setting = {
						old_password: "",
						new_password: "",
						new_password_again: "",
						new_primary_email: ""
					};

					return nextState;

				case _constants2.default.CHANGE_PASSWORD_SUCCESS:
					alert("Change Password Successful");

					nextState.setting = {
						old_password: "",
						new_password: "",
						new_password_again: "",
						new_primary_email: ""
					};

					return nextState;

				case _constants2.default.CHANGE_PASSWORD_FAIL:
				case _constants2.default.CHANGE_PRIMARY_EMAIL_FAIL:
				case _constants2.default.DELETE_ACCOUNT_FAIL:
					alert(action.payload.error);
					console.error(action.payload.error);

					return nextState;

				case _constants2.default.DELETE_ACCOUNT_SUCCESS:
					alert("Your account has been remove from splitshift.");

					localStorage.removeItem('token');
					localStorage.removeItem('type');
					localStorage.removeItem('email');

					window.location.href = '/th/';

					return state;

				default:
					return state;
			}
		}
	}]);

	return UserStore;
}(_utils.ReduceStore);

exports.default = new UserStore(_AppDispatcher2.default);