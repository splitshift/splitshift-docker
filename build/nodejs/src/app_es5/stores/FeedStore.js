'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _reactRouter = require('react-router');

var _utils = require('flux/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FeedStore = function (_ReduceStore) {
	_inherits(FeedStore, _ReduceStore);

	function FeedStore() {
		_classCallCheck(this, FeedStore);

		return _possibleConstructorReturn(this, (FeedStore.__proto__ || Object.getPrototypeOf(FeedStore)).apply(this, arguments));
	}

	_createClass(FeedStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				feeds: [],
				photos: [],
				load_type: "",
				add_video: false,
				add_photo: false,
				uploading: false,
				deleting: false,
				buzz: false,
				edit_feed_id: null,
				edit_index: null,
				owner: null,
				load_more: false,
				comment_for_reply_id: null,
				draft: {
					photos: []
				}
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {

			var nextState = Object.assign({}, state);

			switch (action.type) {

				case _constants2.default.RESET_FEED:
					nextState.feeds = [];
					return nextState;

				case _constants2.default.SET_OWNER_FEED:
					var owner = action.payload.owner;
					owner.name = action.payload.owner.type === 'Employee' ? owner.first_name + ' ' + owner.last_name : owner.company_name;
					nextState.owner = owner;
					return nextState;

				case _constants2.default.EDIT_FEED_DRAFT:
					nextState.draft = Object.assign({}, state.draft, action.payload.response);
					return nextState;

				case _constants2.default.GET_FEED:
				case _constants2.default.GET_FEED_MORE:
					nextState.load_more = false;
					return nextState;

				case _constants2.default.GET_FEED_SUCCESS:
					nextState.feeds = action.payload.response;

					if (action.payload.response.length >= 10) nextState.load_more = true;

					return nextState;

				case _constants2.default.GET_FEED_MORE_SUCCESS:
					nextState.feeds = nextState.feeds.concat(action.payload.response);

					if (action.payload.response.length >= 10) nextState.load_more = true;

					return nextState;

				case _constants2.default.GET_BUZZ_FEED:
				case _constants2.default.GET_BUZZ_FEED_MORE:
					nextState.load_more = false;
					return nextState;

				case _constants2.default.GET_BUZZ_FEED_SUCCESS:
					nextState.feeds = action.payload.response;

					if (action.payload.response.length >= 10) nextState.load_more = true;

					nextState.buzz = true;
					return nextState;

				case _constants2.default.GET_BUZZ_FEED_MORE_SUCCESS:
					nextState.feeds = nextState.feeds.concat(action.payload.response);

					if (action.payload.response.length >= 10) nextState.load_more = true;

					return nextState;

				case _constants2.default.GET_PHOTO_FEED_SUCCESS:
					nextState.photos = action.payload.response.photos;
					return nextState;

				case _constants2.default.CHOOSE_EDIT_FEED:
					nextState.edit_feed_id = action.payload.feed._id;
					nextState.edit_index = action.payload.index;
					nextState.draft = action.payload.feed;

					if (action.payload.owner) nextState.draft.owner = action.payload.owner;

					if (action.payload.feed.video) nextState.add_video = true;else if (action.payload.feed.photos.length > 0) nextState.add_photo = true;

					return nextState;

				case _constants2.default.LIKE_FEED_SUCCESS:

					if (action.payload.response.message.indexOf("Like") != -1) nextState.feeds[nextState.draft.like].likes.push(nextState.draft.owner_key);else nextState.feeds[nextState.draft.like].likes.splice(nextState.feeds[nextState.draft.like].likes.indexOf(nextState.draft.owner_key), 1);

					nextState.draft = {
						photos: []
					};

					return nextState;

				case _constants2.default.ON_REPLY:
					nextState.comment_for_reply_id = action.payload._id;
					return nextState;

				case _constants2.default.DELETE_FEED_PHOTO:

					if (nextState.edit_feed_id) {
						if (typeof nextState.draft.deleted_photos == 'undefined') nextState.draft.deleted_photos = [];

						nextState.draft.deleted_photos.push(nextState.draft.photos[action.payload.image_key]);
					}

					nextState.draft.photos.splice(action.payload.image_key, 1);
					return nextState;

				case _constants2.default.DELETE_COMMENT_FEED:
				case _constants2.default.DELETE_FEED:
				case _constants2.default.DELETE_REPLY_COMMENT_FEED:
					nextState.deleting = true;
					return nextState;

				case _constants2.default.DELETE_COMMENT_FEED_SUCCESS:
				case _constants2.default.DELETE_FEED_SUCCESS:
				case _constants2.default.DELETE_REPLY_COMMENT_FEED_SUCCESS:
					nextState.deleting = false;
					return nextState;

				case _constants2.default.EDIT_FEED_IMAGE:
					nextState.add_photo = !nextState.add_photo;
					nextState.add_video = false;

					return nextState;

				case _constants2.default.EDIT_FEED_VIDEO:
					nextState.add_video = !nextState.add_video;
					nextState.add_photo = false;

					if (!nextState.add_video && typeof nextState.draft.video !== 'undefined') nextState.draft.video = "";

					return nextState;

				case _constants2.default.UPLOAD_FEED_PHOTO:
					nextState.uploading = true;
					if (typeof nextState.draft.photos === 'undefined') nextState.draft.photos = [];

					if (nextState.edit_feed_id && typeof nextState.draft.new_photos === 'undefined') nextState.draft.new_photos = [];

					nextState.draft.photos.push('public/images/loading.svg');

					return nextState;

				case _constants2.default.UPLOAD_FEED_PHOTO_SUCCESS:

					nextState.add_video = false;

					if (nextState.edit_feed_id) nextState.draft.new_photos.push(action.payload.response.path);

					nextState.draft.photos.pop();
					nextState.draft.photos.push(action.payload.response.path);
					nextState.uploading = false;

					return nextState;

				case _constants2.default.GET_FEED_FAIL:
					nextState.feeds = [];
					return nextState;

				case _constants2.default.DELETE_COMMENT_FEED_FAIL:
				case _constants2.default.DELETE_FEED_FAIL:
				case _constants2.default.DELETE_REPLY_COMMENT_FEED_FAIL:
					nextState.deleting = false;
				case _constants2.default.POST_FEED_FAIL:
				case _constants2.default.POST_COMMENT_FEED_FAIL:
				case _constants2.default.POST_REPLY_COMMENT_FEED_FAIL:
					return nextState;

				case _constants2.default.POST_FEED_SUCCESS:
					var response = action.payload.response;

					response.owner = nextState.draft.owner;

					if (typeof nextState.feeds !== 'undefined') nextState.feeds.unshift(response);

					nextState.draft = {
						photos: []
					};

					nextState.add_video = false;
					nextState.add_photo = false;

					return nextState;

				case _constants2.default.POST_COMMENT_FEED_SUCCESS:
					var comment = action.payload.response;
					comment.owner = nextState.owner;
					comment.replies = [];
					nextState.feeds[nextState.draft.feed_index].comments.push(comment);

					return nextState;

				case _constants2.default.POST_REPLY_COMMENT_FEED_SUCCESS:
					nextState.feeds[nextState.draft.feed_index].comments[nextState.draft.comment_index].replies.push(action.payload.response);
					nextState.comment_for_reply_id = null;
					return nextState;

				case _constants2.default.SEARCH_BUZZ_FEED_SUCCESS:
					nextState.feeds = action.payload.response;
					return nextState;

				case _constants2.default.EDIT_POST_FEED_SUCCESS:
					nextState.draft.new_photos = [];
					nextState.feeds[nextState.edit_index] = nextState.draft;

				case _constants2.default.CANCEL_EDIT_FEED:
					nextState.edit_feed_id = null;
					nextState.edit_index = null;
					nextState.draft = {
						photos: []
					};
					nextState.add_video = false;
					nextState.add_photo = false;

				default:
					return nextState;

			}
		}
	}]);

	return FeedStore;
}(_utils.ReduceStore);

exports.default = new FeedStore(_AppDispatcher2.default);