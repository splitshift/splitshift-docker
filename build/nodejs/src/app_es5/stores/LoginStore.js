'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _reactRouter = require('react-router');

var _utils = require('flux/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LoginStore = function (_ReduceStore) {
	_inherits(LoginStore, _ReduceStore);

	function LoginStore() {
		_classCallCheck(this, LoginStore);

		return _possibleConstructorReturn(this, (LoginStore.__proto__ || Object.getPrototypeOf(LoginStore)).apply(this, arguments));
	}

	_createClass(LoginStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				email: '',
				password: '',
				error: false,
				forget: false
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {
			var nextState = Object.assign({}, state);

			switch (action.type) {
				case _constants2.default.UPDATE_LOGIN:
					return action.payload.info;

				case _constants2.default.LOGIN:
					nextState.error = false;
					nextState.forget = false;
					return nextState;

				case _constants2.default.LOGIN_SUCCESS:
					// HIDE Login Modal
					// $("#myModal__login").modal('hide');

					localStorage.setItem('type', action.payload.response.type);
					localStorage.setItem('token', action.payload.response.token);
					localStorage.setItem('key', action.payload.response.key);
					state.error = false;
					return { 'email': '', 'password': '' };

				case _constants2.default.LOGIN_ALREADY:
					return { 'email': '', 'password': '' };

				case _constants2.default.LOGIN_FAIL:
				case _constants2.default.FORGET_PASSWORD_FAIL:
					alert(action.payload.error);
					nextState.error = true;
					return nextState;

				case _constants2.default.FORGET_PASSWORD:
				case _constants2.default.CHANGE_FORGET_PASSWORD:
					nextState.forget = false;
					return nextState;

				case _constants2.default.FORGET_PASSWORD_SUCCESS:
					alert('Send link for change your password to your email');
					nextState.forget = true;
					return nextState;

				case _constants2.default.CHANGE_FORGET_PASSWORD_SUCCESS:
					alert('Change new password complete!');
					nextState.forget = true;
					return nextState;

				default:
					return state;
			}
		}
	}]);

	return LoginStore;
}(_utils.ReduceStore);

exports.default = new LoginStore(_AppDispatcher2.default);