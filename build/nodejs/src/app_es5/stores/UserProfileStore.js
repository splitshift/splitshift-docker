'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _utils = require('flux/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var counter = 0;
var initialState = {
	draft: {},
	company_list: [],
	followers: [],
	connections: [],
	key: "",
	loading: false
};

var resetState = {
	banner: "",
	profile_image: "",
	loading: true,
	address: {},
	courses: [],
	educations: [],
	experiences: [],
	interests: [],
	languages: [],
	first_name: "",
	last_name: "",
	about: undefined,
	occupation: {},
	key: ""
};

var UserProfileStore = function (_ReduceStore) {
	_inherits(UserProfileStore, _ReduceStore);

	function UserProfileStore() {
		_classCallCheck(this, UserProfileStore);

		return _possibleConstructorReturn(this, (UserProfileStore.__proto__ || Object.getPrototypeOf(UserProfileStore)).apply(this, arguments));
	}

	_createClass(UserProfileStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return initialState;
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {

			var nextState = Object.assign({}, state);

			switch (action.type) {
				case _constants2.default.LOGOUT:
					return initialState;
				case _constants2.default.RESET_USER_PROFILE:
				case _constants2.default.GET_USER_PROFILE:
					return Object.assign({}, nextState, resetState);
				case _constants2.default.GET_USER_PROFILE_SUCCESS:
					return Object.assign({}, state, { about: "" }, action.payload.response, { loading: false });
				case _constants2.default.GET_USER_PROFILE_FAIL:
					console.log(action.payload.error);
					return state;
				case _constants2.default.UPDATE_PROFILE:
					return Object.assign({}, state, action.payload.response);
				case _constants2.default.EDIT_DRAFT:
					nextState.draft = Object.assign({}, state.draft, action.payload.response);
					return nextState;

				case _constants2.default.UPDATE_PROFILE_SUCCESS:
				case _constants2.default.CANCEL_PROFILE_EDIT:
					nextState.draft = {};
					return nextState;

				case _constants2.default.UPLOAD_PROFILE_IMAGE:
					nextState.profile_image = '/images/loading.svg';
					return nextState;

				case _constants2.default.UPLOAD_PROFILE_IMAGE_SUCCESS:
					nextState.profile_image = action.payload.response.path + "?new=" + ++counter;
					return nextState;

				case _constants2.default.UPLOAD_BANNER_IMAGE:
					nextState.banner = '/images/loading.svg';
					return nextState;

				case _constants2.default.UPLOAD_BANNER_IMAGE_SUCCESS:
					nextState.banner = action.payload.response.path + "?new=" + ++counter;
					return nextState;
				case _constants2.default.REQUEST_PEOPLE_FAIL:
				case _constants2.default.UPDATE_PROFILE_ERROR:
					console.log(action.payload.error);
					return state;
				case _constants2.default.FOLLOW_PEOPLE_SUCCESS:
					if (action.payload.response.message.indexOf("Unfollowing") != -1) nextState.following = true;else nextState.following = false;
					return nextState;
				case _constants2.default.REQUEST_PEOPLE_SUCCESS:
					console.log("Send Request Successful.");
					console.log(action.payload.response);
					nextState.connected = "requesting";
					return nextState;

				case _constants2.default.CHANGE_USER_BANNER_IMAGE_WITH_TEMPLATE_SUCCESS:
					nextState.banner = state.draft.template_banner;
					nextState.draft = {};
					return nextState;

				case _constants2.default.LOAD_ALL_COMPANY_SUCCESS:
					nextState.company_list = action.payload.response;
					return nextState;

				case _constants2.default.GET_FOLLOWER_SUCCESS:
					nextState.followers = action.payload.response;
					return nextState;

				case _constants2.default.GET_CONNECTIONS_SUCCESS:
					nextState.connections = action.payload.response;
					return nextState;

				default:
					return state;
			}
		}
	}]);

	return UserProfileStore;
}(_utils.ReduceStore);

exports.default = new UserProfileStore(_AppDispatcher2.default);