'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _utils = require('flux/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MessageStore = function (_ReduceStore) {
	_inherits(MessageStore, _ReduceStore);

	function MessageStore() {
		_classCallCheck(this, MessageStore);

		return _possibleConstructorReturn(this, (MessageStore.__proto__ || Object.getPrototypeOf(MessageStore)).apply(this, arguments));
	}

	_createClass(MessageStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				users: [],
				messages: [],
				draft: {},
				owner_key: undefined,
				profile_key: undefined,
				sending: false,
				uploading: false
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {

			var nextState = Object.assign({}, state);

			switch (action.type) {
				case _constants2.default.EDIT_MESSAGE_DRAFT:
					nextState.draft = action.payload.response;
					return nextState;
				case _constants2.default.CHOOSE_MESSAGE:
					return Object.assign({}, nextState, action.payload.response);

				case _constants2.default.CHOOSE_OWNER_MESSAGE:
					nextState.owner_key = action.payload.response.key;
					return nextState;
				case _constants2.default.CHOOSE_OTHER_MESSAGE:
					nextState.profile_key = action.payload.response.key;
					return nextState;

				case _constants2.default.GET_MESSAGE:
					// nextState.users = [];
					// nextState.messages = [];
					// nextState.draft = {};
					// nextState.sending = false;
					// nextState.uploading = false;
					return nextState;

				case _constants2.default.GET_MESSAGE_SUCCESS:
					nextState = Object.assign({}, nextState, action.payload.response);
					console.log(action.payload.response);
					return nextState;
				case _constants2.default.UPLOAD_MESSAGE_IMAGE:
				case _constants2.default.UPLOAD_MESSAGE_FILE:
					nextState.uploading = true;
					return nextState;
				case _constants2.default.SEND_MESSAGE:
					nextState.sending = true;
					return nextState;
				case _constants2.default.SEND_MESSAGE_SUCCESS:
					nextState.draft = {};
					nextState.sending = false;
					return nextState;
				case _constants2.default.UPLOAD_MESSAGE_FILE_SUCCESS:
					console.log(action.payload.response);
					nextState.draft.message = action.payload.response.original_name;
					nextState.draft.path = action.payload.response.path;
					nextState.uploading = false;
					return nextState;

				case _constants2.default.UPLOAD_MESSAGE_IMAGE_SUCCESS:
					nextState.draft.message = action.payload.response.path;
					nextState.uploading = false;
					return nextState;

				case _constants2.default.GET_MESSAGE_FAIL:
				case _constants2.default.UPLOAD_MESSAGE_FILE_FAIL:
					console.error(action.payload.error);
					alert('Upload Fail');
					nextState.uploading = false;
					return nextState;
				default:
					return nextState;
			}
		}
	}]);

	return MessageStore;
}(_utils.ReduceStore);

exports.default = new MessageStore(_AppDispatcher2.default);