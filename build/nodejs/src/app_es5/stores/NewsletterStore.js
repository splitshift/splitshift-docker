'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _utils = require('flux/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var counter = 0;

var NewsletterStore = function (_ReduceStore) {
	_inherits(NewsletterStore, _ReduceStore);

	function NewsletterStore() {
		_classCallCheck(this, NewsletterStore);

		return _possibleConstructorReturn(this, (NewsletterStore.__proto__ || Object.getPrototypeOf(NewsletterStore)).apply(this, arguments));
	}

	_createClass(NewsletterStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				list: []
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {

			var nextState = Object.assign({}, state);

			switch (action.type) {

				case _constants2.default.GET_NEWSLETTER_SUCCESS:
					nextState.list = action.payload.response;
					return nextState;

				case _constants2.default.SEND_EMAIL_SUCCESS:
					alert("Send Email Successful");
					return nextState;

				default:
					return state;
			}
		}
	}]);

	return NewsletterStore;
}(_utils.ReduceStore);

exports.default = new NewsletterStore(_AppDispatcher2.default);