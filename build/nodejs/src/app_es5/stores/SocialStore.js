'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var counter = 0;

var SocialStore = function (_ReduceStore) {
	_inherits(SocialStore, _ReduceStore);

	function SocialStore() {
		_classCallCheck(this, SocialStore);

		return _possibleConstructorReturn(this, (SocialStore.__proto__ || Object.getPrototypeOf(SocialStore)).apply(this, arguments));
	}

	_createClass(SocialStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				actions: [],
				messages: [],
				friends: [],
				friend_index: null,
				seen_notification: false,
				deleting: false
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {

			var nextState = Object.assign({}, state);

			switch (action.type) {
				case _constants2.default.GET_REQUEST_PEOPLE_SUCCESS:
					nextState.friends = action.payload.response;
					return nextState;

				case _constants2.default.SET_SOCIAL_DRAFT:
					nextState = Object.assign({}, nextState, action.payload.response);
					return nextState;

				case _constants2.default.GET_NOTIFICATION_SUCCESS:
					nextState.actions = action.payload.response;
					return nextState;

				case _constants2.default.REQUEST_PEOPLE_SUCCESS:
					nextState.friends.splice(nextState.friend_index);
					nextState.friend_index = null;
					return nextState;

				case _constants2.default.GET_ALL_MESSAGE_SUCCESS:
					console.log("All Message");
					nextState.messages = action.payload.response;
					return nextState;

				case _constants2.default.SEEN_NOTIFICATION_SUCCESS:
					nextState.actions = Object.assign([], state.actions);
					for (var i = 0; i < nextState.actions.length; i++) {
						nextState.actions[i] = Object.assign({}, state.actions[i], { seen: true });
					}
					nextState.seen_notification = true;
					return nextState;

				case _constants2.default.DELETE_NOTIFICATION:
					nextState.deleting = true;
					return nextState;

				case _constants2.default.DELETE_NOTIFICATION_SUCCESS:
					nextState.deleting = false;
					return nextState;

				case _constants2.default.GET_MESSAGE_SUCCESS:

					nextState.messages = Object.assign([], state.messages);

					for (var _i = 0; _i < nextState.messages.length; _i++) {

						if (nextState.messages[_i]._id === action.payload.response._id) nextState.messages[_i] = Object.assign({}, state.messages[_i], { seen: action.payload.response.seen });
					}

					return nextState;

				case _constants2.default.SEEN_NOTIFICATION_FAIL:
				case _constants2.default.GET_ALL_MESSAGE_FAIL:
					console.error(action.payload.error);
					return nextState;

				default:
					return state;
			}
		}
	}]);

	return SocialStore;
}(_utils.ReduceStore);

exports.default = new SocialStore(_AppDispatcher2.default);