'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _utils = require('flux/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HRStore = function (_ReduceStore) {
	_inherits(HRStore, _ReduceStore);

	function HRStore() {
		_classCallCheck(this, HRStore);

		return _possibleConstructorReturn(this, (HRStore.__proto__ || Object.getPrototypeOf(HRStore)).apply(this, arguments));
	}

	_createClass(HRStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				users: [],
				editing: false
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {

			var nextState = Object.assign({}, state);

			switch (action.type) {

				case _constants2.default.GET_EMPLOYEE_IN_COMPANY_SUCCESS:
					nextState.users = action.payload.response;
					nextState.editing = false;
					return nextState;

				case _constants2.default.CHANGE_RANK_SUCCESS:
					nextState.editing = true;
					return nextState;

				case _constants2.default.CHANGE_RANK_FAIL:
					console.error(action.payload.error);
					return nextState;

				default:
					return nextState;

			}
		}
	}]);

	return HRStore;
}(_utils.ReduceStore);

exports.default = new HRStore(_AppDispatcher2.default);