'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AppDispatcher = require('../AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _utils = require('flux/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var counter = 0;

var initialProfile = {
	culture: "",
	video: "",
	overview: "",
	location: [0, 0],
	links: {},
	loading: false,
	key: ""
};

var CompanyProfileStore = function (_ReduceStore) {
	_inherits(CompanyProfileStore, _ReduceStore);

	function CompanyProfileStore() {
		_classCallCheck(this, CompanyProfileStore);

		return _possibleConstructorReturn(this, (CompanyProfileStore.__proto__ || Object.getPrototypeOf(CompanyProfileStore)).apply(this, arguments));
	}

	_createClass(CompanyProfileStore, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				draft: {},
				reviewing: false
			};
		}
	}, {
		key: 'reduce',
		value: function reduce(state, action) {
			var nextState = Object.assign({}, state);

			switch (action.type) {
				case _constants2.default.LOGOUT:
					return {};

				case _constants2.default.RESET_COMPANY_PROFILE:
					return Object.assign({}, nextState, initialProfile);

				case _constants2.default.GET_COMPANY_PROFILE:
					// nextState.key = "";
					delete nextState.following;
					return Object.assign({}, nextState, { loading: true });

				case _constants2.default.REVIEW_COMPANY:
					nextState.reviewing = true;
					return nextState;

				case _constants2.default.GET_COMPANY_PROFILE_SUCCESS:
					return Object.assign({}, nextState, initialProfile, action.payload.response, { loading: false });

				case _constants2.default.GET_COMPANY_PROFILE_FAIL:
				case _constants2.default.UPDATE_COMPANY_PROFILE_FAIL:
				case _constants2.default.REVIEW_COMPANY_FAIL:
					console.error(action.payload.error);
					alert(action.payload.error);
					return state;

				case _constants2.default.REVIEW_COMPANY_SUCCESS:
					nextState.reviewing = false;

				case _constants2.default.UPDATE_COMPANY_PROFILE_SUCCESS:
					nextState.draft = {};
					return nextState;

				case _constants2.default.UPLOAD_PROFILE_IMAGE:
					nextState.profile_image = '/images/loading.svg';
					return nextState;

				case _constants2.default.UPLOAD_PROFILE_IMAGE_SUCCESS:
					nextState.profile_image = action.payload.response.path + "?new=" + ++counter;
					return nextState;

				case _constants2.default.FOLLOW_PEOPLE_SUCCESS:
					nextState.following = !nextState.following;
					return nextState;

				case _constants2.default.UPLOAD_BANNER_IMAGE:
					nextState.banner = '/images/loading.svg';
					return nextState;

				case _constants2.default.UPLOAD_BANNER_IMAGE_SUCCESS:
					nextState.banner = action.payload.response.path + "?new=" + ++counter;
					return nextState;

				case _constants2.default.CHANGE_COMPANY_BANNER_IMAGE_WITH_TEMPLATE_SUCCESS:
					nextState.banner = state.draft.template_banner;
					nextState.draft = {};
					return nextState;

				case _constants2.default.EDIT_DRAFT:
					nextState.draft = Object.assign({}, state.draft, action.payload.response);
					return nextState;

				default:
					return state;
			}
		}
	}]);

	return CompanyProfileStore;
}(_utils.ReduceStore);

exports.default = new CompanyProfileStore(_AppDispatcher2.default);