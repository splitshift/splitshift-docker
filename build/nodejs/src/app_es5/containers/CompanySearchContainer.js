'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

var _CompanySearch = require('../components/Frontend/CompanySearch');

var _CompanySearch2 = _interopRequireDefault(_CompanySearch);

var _SearchStore = require('../stores/SearchStore');

var _SearchStore2 = _interopRequireDefault(_SearchStore);

var _ResourceStore = require('../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanySearchContainer = function (_Component) {
  _inherits(CompanySearchContainer, _Component);

  function CompanySearchContainer() {
    _classCallCheck(this, CompanySearchContainer);

    return _possibleConstructorReturn(this, (CompanySearchContainer.__proto__ || Object.getPrototypeOf(CompanySearchContainer)).apply(this, arguments));
  }

  _createClass(CompanySearchContainer, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_CompanySearch2.default, { search: this.state.search, language: this.state.resource.lang });
    }
  }]);

  return CompanySearchContainer;
}(_react.Component);

CompanySearchContainer.getStores = function () {
  return [_SearchStore2.default];
};
CompanySearchContainer.calculateState = function (prevState) {
  return {
    search: _SearchStore2.default.getState(),
    resource: _ResourceStore2.default.getState()
  };
};

exports.default = _utils.Container.create(CompanySearchContainer);