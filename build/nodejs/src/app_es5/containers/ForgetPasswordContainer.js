'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _LoginStore = require('../stores/LoginStore');

var _LoginStore2 = _interopRequireDefault(_LoginStore);

var _ForgetPassword = require('../components/Frontend/ForgetPassword');

var _ForgetPassword2 = _interopRequireDefault(_ForgetPassword);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ForgetPasswordContainer = function (_Component) {
  _inherits(ForgetPasswordContainer, _Component);

  function ForgetPasswordContainer() {
    _classCallCheck(this, ForgetPasswordContainer);

    return _possibleConstructorReturn(this, (ForgetPasswordContainer.__proto__ || Object.getPrototypeOf(ForgetPasswordContainer)).apply(this, arguments));
  }

  _createClass(ForgetPasswordContainer, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_ForgetPassword2.default, { login: this.state.login, forget_id: this.props.params.id });
    }
  }]);

  return ForgetPasswordContainer;
}(_react.Component);

ForgetPasswordContainer.getStores = function () {
  return [_LoginStore2.default];
};
ForgetPasswordContainer.calculateState = function (prevState) {
  return {
    login: _LoginStore2.default.getState()
  };
};

exports.default = _utils.Container.create(ForgetPasswordContainer);
;