'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _UserStore = require('../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _CompanyProfileStore = require('../stores/CompanyProfileStore');

var _CompanyProfileStore2 = _interopRequireDefault(_CompanyProfileStore);

var _Resume = require('../components/Frontend/Form/Company/Resume');

var _Resume2 = _interopRequireDefault(_Resume);

var _AuthenticationActionCreators = require('../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _CompanyProfileActionCreators = require('../actions/CompanyProfileActionCreators');

var _CompanyProfileActionCreators2 = _interopRequireDefault(_CompanyProfileActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanyResumeContainer = function (_Component) {
  _inherits(CompanyResumeContainer, _Component);

  function CompanyResumeContainer() {
    _classCallCheck(this, CompanyResumeContainer);

    return _possibleConstructorReturn(this, (CompanyResumeContainer.__proto__ || Object.getPrototypeOf(CompanyResumeContainer)).apply(this, arguments));
  }

  _createClass(CompanyResumeContainer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (localStorage.getItem('token')) {
        _AuthenticationActionCreators2.default.loginAlready();

        if (localStorage.getItem('type') == 'Employee') _AuthenticationActionCreators2.default.loadProfile(localStorage.getItem('key'));else if (localStorage.getItem('type') != 'Admin') _AuthenticationActionCreators2.default.loadCompanyProfile(localStorage.getItem('key'));
      } else {
        _AuthenticationActionCreators2.default.guest();
      }

      _CompanyProfileActionCreators2.default.getProfile(this.props.params.key);
    }
  }, {
    key: 'render',
    value: function render() {
      return this.state.profile.key ? _react2.default.createElement(_Resume2.default, { user: this.state.profile }) : _react2.default.createElement('div', null);
    }
  }]);

  return CompanyResumeContainer;
}(_react.Component);

CompanyResumeContainer.getStores = function () {
  return [_UserStore2.default, _CompanyProfileStore2.default];
};
CompanyResumeContainer.calculateState = function (prevState) {
  return {
    user: _UserStore2.default.getState(),
    profile: _CompanyProfileStore2.default.getState()
  };
};

exports.default = _utils.Container.create(CompanyResumeContainer);