'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

var _StatActionCreators = require('../actions/StatActionCreators');

var _StatActionCreators2 = _interopRequireDefault(_StatActionCreators);

var _Home = require('../components/Frontend/Home');

var _Home2 = _interopRequireDefault(_Home);

var _ResourceStore = require('../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

var _UserStore = require('../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _configLang = require('../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HomeContainer = function (_Component) {
  _inherits(HomeContainer, _Component);

  function HomeContainer() {
    _classCallCheck(this, HomeContainer);

    return _possibleConstructorReturn(this, (HomeContainer.__proto__ || Object.getPrototypeOf(HomeContainer)).apply(this, arguments));
  }

  _createClass(HomeContainer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _StatActionCreators2.default.getStat();
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_Home2.default, { resource: _configLang2.default[this.state.resource.lang], lang: this.state.resource.lang, stat: this.state.resource.stat, user: this.state.user });
    }
  }]);

  return HomeContainer;
}(_react.Component);

HomeContainer.getStores = function () {
  return [_ResourceStore2.default, _UserStore2.default];
};
HomeContainer.calculateState = function (prevState) {
  return {
    resource: _ResourceStore2.default.getState(),
    user: _UserStore2.default.getState()
  };
};

exports.default = _utils.Container.create(HomeContainer);