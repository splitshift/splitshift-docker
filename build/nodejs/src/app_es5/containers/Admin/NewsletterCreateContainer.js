'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

var _Create = require('../../components/Backend/Newsletter/Create');

var _Create2 = _interopRequireDefault(_Create);

var _UserStore = require('../../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _NewsletterStore = require('../../stores/NewsletterStore');

var _NewsletterStore2 = _interopRequireDefault(_NewsletterStore);

var _ResourceStore = require('../../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NewsletterCreateContainer = function (_Component) {
  _inherits(NewsletterCreateContainer, _Component);

  function NewsletterCreateContainer() {
    _classCallCheck(this, NewsletterCreateContainer);

    return _possibleConstructorReturn(this, (NewsletterCreateContainer.__proto__ || Object.getPrototypeOf(NewsletterCreateContainer)).apply(this, arguments));
  }

  _createClass(NewsletterCreateContainer, [{
    key: 'render',
    value: function render() {
      return typeof this.state.user.token !== 'undefined' ? _react2.default.createElement(_Create2.default, { user: this.state.user, newsletter: this.state.newsletter, language: this.state.language }) : _react2.default.createElement('div', null);
    }
  }]);

  return NewsletterCreateContainer;
}(_react.Component);

NewsletterCreateContainer.getStores = function () {
  return [_UserStore2.default, _NewsletterStore2.default];
};
NewsletterCreateContainer.calculateState = function (prevState) {
  return {
    user: _UserStore2.default.getState(),
    newsletter: _NewsletterStore2.default.getState(),
    language: _ResourceStore2.default.getState().lang
  };
};

exports.default = _utils.Container.create(NewsletterCreateContainer);