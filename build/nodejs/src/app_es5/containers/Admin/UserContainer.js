'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

var _AdminManagerActionCreators = require('../../actions/AdminManagerActionCreators');

var _AdminManagerActionCreators2 = _interopRequireDefault(_AdminManagerActionCreators);

var _Index = require('../../components/Backend/User/Index');

var _Index2 = _interopRequireDefault(_Index);

var _UserStore = require('../../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _DataManagerStore = require('../../stores/DataManagerStore');

var _DataManagerStore2 = _interopRequireDefault(_DataManagerStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AdminUserContainer = function (_Component) {
  _inherits(AdminUserContainer, _Component);

  function AdminUserContainer() {
    _classCallCheck(this, AdminUserContainer);

    return _possibleConstructorReturn(this, (AdminUserContainer.__proto__ || Object.getPrototypeOf(AdminUserContainer)).apply(this, arguments));
  }

  _createClass(AdminUserContainer, [{
    key: 'render',
    value: function render() {
      return this.state.user.token ? _react2.default.createElement(_Index2.default, { user: this.state.user, manager: this.state.manager }) : _react2.default.createElement('div', null);
    }
  }]);

  return AdminUserContainer;
}(_react.Component);

AdminUserContainer.getStores = function () {
  return [_UserStore2.default, _DataManagerStore2.default];
};
AdminUserContainer.calculateState = function (prevState) {
  return {
    user: _UserStore2.default.getState(),
    manager: _DataManagerStore2.default.getState()
  };
};

exports.default = _utils.Container.create(AdminUserContainer);