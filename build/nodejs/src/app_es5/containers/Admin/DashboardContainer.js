'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _StatActionCreators = require('../../actions/StatActionCreators');

var _StatActionCreators2 = _interopRequireDefault(_StatActionCreators);

var _AdminDashboard = require('../../components/Backend/AdminDashboard');

var _AdminDashboard2 = _interopRequireDefault(_AdminDashboard);

var _UserStore = require('../../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _ResourceStore = require('../../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AdminDashboardContainer = function (_Component) {
  _inherits(AdminDashboardContainer, _Component);

  function AdminDashboardContainer() {
    _classCallCheck(this, AdminDashboardContainer);

    return _possibleConstructorReturn(this, (AdminDashboardContainer.__proto__ || Object.getPrototypeOf(AdminDashboardContainer)).apply(this, arguments));
  }

  _createClass(AdminDashboardContainer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.state.user.token && this.state.user.type === 'Admin') _StatActionCreators2.default.getAdminSummary(this.state.user.token);

      _StatActionCreators2.default.getStat();
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_AdminDashboard2.default, { stat: this.state.resource.stat });
    }
  }]);

  return AdminDashboardContainer;
}(_react.Component);

AdminDashboardContainer.getStores = function () {
  return [_UserStore2.default, _ResourceStore2.default];
};
AdminDashboardContainer.calculateState = function (prevState) {
  return {
    user: _UserStore2.default.getState(),
    resource: _ResourceStore2.default.getState()
  };
};

exports.default = _utils.Container.create(AdminDashboardContainer);