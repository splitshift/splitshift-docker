'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

var _Admin = require('../../components/Backend/Header/Admin');

var _Admin2 = _interopRequireDefault(_Admin);

var _AdminSidebar = require('../../components/Backend/AdminSidebar');

var _AdminSidebar2 = _interopRequireDefault(_AdminSidebar);

var _AuthenticationActionCreators = require('../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _UserStore = require('../../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _ResourceStore = require('../../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

var _SocialStore = require('../../stores/SocialStore');

var _SocialStore2 = _interopRequireDefault(_SocialStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var owner_load = false;

var AdminAppContainer = function (_Component) {
  _inherits(AdminAppContainer, _Component);

  function AdminAppContainer() {
    _classCallCheck(this, AdminAppContainer);

    return _possibleConstructorReturn(this, (AdminAppContainer.__proto__ || Object.getPrototypeOf(AdminAppContainer)).apply(this, arguments));
  }

  _createClass(AdminAppContainer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (localStorage.getItem('token')) {

        if (localStorage.getItem('type') !== 'Admin') _reactRouter.browserHistory.push('/');

        _AuthenticationActionCreators2.default.loginAlready();
      } else _reactRouter.browserHistory.push('/');

      this.state.resource.lang = this.props.params.language || this.state.resource.lang;
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.props.params.language !== this.state.resource.lang) {
        var url = this.props.location.pathname;
        url = url.substr(3);
        _reactRouter.browserHistory.push('/' + this.state.resource.lang + url);
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var lang = this.props.params.language || this.state.resource.lang;
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { id: 'main' },
          _react2.default.createElement(_Admin2.default, { language: lang }),
          _react2.default.createElement(
            'div',
            { className: 'container-content' },
            _react2.default.cloneElement(this.props.children)
          )
        ),
        _react2.default.createElement(_AdminSidebar2.default, { language: lang, user: this.state.user })
      );
    }
  }]);

  return AdminAppContainer;
}(_react.Component);

AdminAppContainer.getStores = function () {
  return [_UserStore2.default, _ResourceStore2.default, _SocialStore2.default];
};
AdminAppContainer.calculateState = function (prevState) {
  return {
    resource: _ResourceStore2.default.getState(),
    user: _UserStore2.default.getState(),
    social: _SocialStore2.default.getState()
  };
};

exports.default = _utils.Container.create(AdminAppContainer);
;