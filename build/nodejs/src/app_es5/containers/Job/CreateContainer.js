'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _JobActionCreators = require('../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

var _Create = require('../../components/Backend/Job/Create');

var _Create2 = _interopRequireDefault(_Create);

var _ResourceStore = require('../../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

var _UserStore = require('../../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _JobStore = require('../../stores/JobStore');

var _JobStore2 = _interopRequireDefault(_JobStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var first_load = false;

var JobCreateContainer = function (_Component) {
  _inherits(JobCreateContainer, _Component);

  function JobCreateContainer() {
    _classCallCheck(this, JobCreateContainer);

    return _possibleConstructorReturn(this, (JobCreateContainer.__proto__ || Object.getPrototypeOf(JobCreateContainer)).apply(this, arguments));
  }

  _createClass(JobCreateContainer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _JobActionCreators2.default.resetDraft();
      first_load = false;
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {

      if (!first_load && typeof this.props.params.id !== 'undefined' && typeof this.state.user.token !== 'undefined') {
        _JobActionCreators2.default.getJob(this.state.user.token, this.props.params.id);
        first_load = true;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return typeof this.state.user.token !== 'undefined' && (typeof this.props.params.id === 'undefined' || typeof this.state.job.edit_job_id !== 'undefined') ? _react2.default.createElement(_Create2.default, { user: this.state.user, job: this.state.job, language: this.state.resource.lang }) : _react2.default.createElement('div', null);
    }
  }]);

  return JobCreateContainer;
}(_react.Component);

JobCreateContainer.getStores = function () {
  return [_ResourceStore2.default, _UserStore2.default, _JobStore2.default];
};
JobCreateContainer.calculateState = function (prevState) {
  return {
    resource: _ResourceStore2.default.getState(),
    user: _UserStore2.default.getState(),
    job: _JobStore2.default.getState()
  };
};

exports.default = _utils.Container.create(JobCreateContainer);