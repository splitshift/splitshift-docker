'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _ResourceActionCreators = require('../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _UserStore = require('../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _ResourceStore = require('../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

var _Article = require('../components/Frontend/Resource/Article');

var _Article2 = _interopRequireDefault(_Article);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResourceArticleContainer = function (_Component) {
  _inherits(ResourceArticleContainer, _Component);

  function ResourceArticleContainer() {
    _classCallCheck(this, ResourceArticleContainer);

    return _possibleConstructorReturn(this, (ResourceArticleContainer.__proto__ || Object.getPrototypeOf(ResourceArticleContainer)).apply(this, arguments));
  }

  _createClass(ResourceArticleContainer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _ResourceActionCreators2.default.getResource(this.props.params.id, this.state.user.token);
    }
  }, {
    key: 'render',
    value: function render() {
      return typeof this.state.resource.resource._id !== 'undefined' ? _react2.default.createElement(_Article2.default, { resource_id: this.props.params.id, user: this.state.user, resource: this.state.resource }) : _react2.default.createElement('div', null);
    }
  }]);

  return ResourceArticleContainer;
}(_react.Component);

ResourceArticleContainer.getStores = function () {
  return [_UserStore2.default, _ResourceStore2.default];
};
ResourceArticleContainer.calculateState = function (prevState) {
  return {
    user: _UserStore2.default.getState(),
    resource: _ResourceStore2.default.getState()
  };
};

exports.default = _utils.Container.create(ResourceArticleContainer);