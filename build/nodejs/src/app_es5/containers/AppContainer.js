'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

var _Main = require('../components/Frontend/Header/Main');

var _Main2 = _interopRequireDefault(_Main);

var _Admin = require('../components/Frontend/Header/Admin');

var _Admin2 = _interopRequireDefault(_Admin);

var _User = require('../components/Frontend/Header/User');

var _User2 = _interopRequireDefault(_User);

var _Footer = require('../components/Frontend/Footer');

var _Footer2 = _interopRequireDefault(_Footer);

var _UserStore = require('../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _LoginStore = require('../stores/LoginStore');

var _LoginStore2 = _interopRequireDefault(_LoginStore);

var _ResourceStore = require('../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

var _SocialStore = require('../stores/SocialStore');

var _SocialStore2 = _interopRequireDefault(_SocialStore);

var _SearchStore = require('../stores/SearchStore');

var _SearchStore2 = _interopRequireDefault(_SearchStore);

var _MessageStore = require('../stores/MessageStore');

var _MessageStore2 = _interopRequireDefault(_MessageStore);

var _AuthenticationActionCreators = require('../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _JQueryTool = require('../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Pages = ['home', 'profile', 'company', 'user_public_profile', 'company_public_profile'];
var NoLangPage = ['/search'];

var owner_load = false;

var AppContainer = function (_Component) {
  _inherits(AppContainer, _Component);

  function AppContainer() {
    _classCallCheck(this, AppContainer);

    return _possibleConstructorReturn(this, (AppContainer.__proto__ || Object.getPrototypeOf(AppContainer)).apply(this, arguments));
  }

  _createClass(AppContainer, [{
    key: 'getLanguage',
    value: function getLanguage(lang) {
      var url = this.props.location.pathname;

      if (NoLangPage.indexOf(url) !== -1 || url.indexOf('/forgot/password') !== -1 || url.indexOf('/api/confirm/') !== -1) return '';

      url = url.substr(3);

      if (lang == 'th' || lang == 'en' || lang == '') _reactRouter.browserHistory.push('/' + (lang !== "" ? lang : 'th') + url);else if (Pages.indexOf(lang) != -1) _reactRouter.browserHistory.push('/th/' + lang);else _reactRouter.browserHistory.push('/th/');
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      owner_load = false;
      if (localStorage.getItem('token')) {
        _AuthenticationActionCreators2.default.loginAlready();
        if (localStorage.getItem('type') == 'Employee') _AuthenticationActionCreators2.default.loadProfile(localStorage.getItem('key'));else if (localStorage.getItem('type') != 'Admin') _AuthenticationActionCreators2.default.loadCompanyProfile(localStorage.getItem('key'));
      } else {
        _AuthenticationActionCreators2.default.guest();
      }

      this.getLanguage(this.props.params.language || this.state.resource.lang);
      this.state.resource.lang = this.props.params.language || this.state.resource.lang;
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (typeof this.props.params.language !== 'undefined' && this.props.params.language !== this.state.resource.lang) this.getLanguage(this.state.resource.lang);
    }
  }, {
    key: 'render',
    value: function render() {

      var currentPage = this.props.routes[this.props.routes.length - 1].name;
      var currentLang = this.props.params.language || this.state.resource.lang;
      var header = "";

      if (typeof this.state.user.token !== 'undefined' && this.state.user.token) {
        if (this.state.user.type == 'Admin') header = _react2.default.createElement(_Admin2.default, { search: this.state.search, currentPage: currentPage, language: currentLang, user: this.state.user, social: this.state.social });else header = _react2.default.createElement(_User2.default, { search: this.state.search, currentPage: currentPage, message: this.state.message, language: currentLang, user: this.state.user, social: this.state.social });
      } else header = _react2.default.createElement(_Main2.default, { search: this.state.search, currentPage: currentPage, login: this.state.login, user: this.state.user, language: currentLang });

      return _react2.default.createElement(
        'div',
        null,
        header,
        _react2.default.createElement(
          'div',
          { style: { minHeight: "1000px" } },
          _react2.default.cloneElement(this.props.children)
        ),
        _react2.default.createElement(_Footer2.default, { user: this.state.user, language: currentLang })
      );
    }
  }]);

  return AppContainer;
}(_react.Component);

AppContainer.getStores = function () {
  return [_UserStore2.default, _LoginStore2.default, _ResourceStore2.default, _SocialStore2.default, _SearchStore2.default, _MessageStore2.default];
};
AppContainer.calculateState = function (prevState) {
  return {
    resource: _ResourceStore2.default.getState(),
    user: _UserStore2.default.getState(),
    login: _LoginStore2.default.getState(),
    social: _SocialStore2.default.getState(),
    search: _SearchStore2.default.getState(),
    message: _MessageStore2.default.getState()
  };
};

exports.default = _utils.Container.create(AppContainer);
;