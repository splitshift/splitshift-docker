'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

var _BuzzFeed = require('../components/Frontend/BuzzFeed');

var _BuzzFeed2 = _interopRequireDefault(_BuzzFeed);

var _FeedActionCreators = require('../actions/FeedActionCreators');

var _FeedActionCreators2 = _interopRequireDefault(_FeedActionCreators);

var _UserStore = require('../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _FeedStore = require('../stores/FeedStore');

var _FeedStore2 = _interopRequireDefault(_FeedStore);

var _ResourceStore = require('../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BuzzContainer = function (_Component) {
  _inherits(BuzzContainer, _Component);

  function BuzzContainer() {
    _classCallCheck(this, BuzzContainer);

    return _possibleConstructorReturn(this, (BuzzContainer.__proto__ || Object.getPrototypeOf(BuzzContainer)).apply(this, arguments));
  }

  _createClass(BuzzContainer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _FeedActionCreators2.default.getBuzzFeed();
    }
  }, {
    key: 'render',
    value: function render() {
      return this.state.feed.buzz ? _react2.default.createElement(_BuzzFeed2.default, { user: this.state.user, feed: this.state.feed, language: this.state.resource.lang }) : _react2.default.createElement('div', null);
    }
  }]);

  return BuzzContainer;
}(_react.Component);

BuzzContainer.getStores = function () {
  return [_UserStore2.default, _FeedStore2.default];
};
BuzzContainer.calculateState = function (prevState) {
  return {
    user: _UserStore2.default.getState(),
    feed: _FeedStore2.default.getState(),
    resource: _ResourceStore2.default.getState()
  };
};

exports.default = _utils.Container.create(BuzzContainer);