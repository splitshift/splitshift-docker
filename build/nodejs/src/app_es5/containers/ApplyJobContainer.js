'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

var _JobActionCreators = require('../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

var _ApplyJob = require('../components/Backend/ApplyJob');

var _ApplyJob2 = _interopRequireDefault(_ApplyJob);

var _ResourceStore = require('../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

var _UserStore = require('../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _JobStore = require('../stores/JobStore');

var _JobStore2 = _interopRequireDefault(_JobStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var first_load = false;

var ApplyJobContainer = function (_Component) {
  _inherits(ApplyJobContainer, _Component);

  function ApplyJobContainer() {
    _classCallCheck(this, ApplyJobContainer);

    return _possibleConstructorReturn(this, (ApplyJobContainer.__proto__ || Object.getPrototypeOf(ApplyJobContainer)).apply(this, arguments));
  }

  _createClass(ApplyJobContainer, [{
    key: 'render',
    value: function render() {
      return this.state.user.token && (this.state.user.company_name || this.state.user.rank) ? _react2.default.createElement(_ApplyJob2.default, {
        user: this.state.user,
        apply_jobs: this.state.job.apply_jobs,
        apply_job_filter: this.state.job.apply_job_filter,
        language: this.state.resource.lang,
        deleting: this.state.job.deleting }) : _react2.default.createElement('div', null);
    }
  }]);

  return ApplyJobContainer;
}(_react.Component);

ApplyJobContainer.getStores = function () {
  return [_ResourceStore2.default, _UserStore2.default, _JobStore2.default];
};
ApplyJobContainer.calculateState = function (prevState) {
  return {
    resource: _ResourceStore2.default.getState(),
    user: _UserStore2.default.getState(),
    job: _JobStore2.default.getState()
  };
};

exports.default = _utils.Container.create(ApplyJobContainer);