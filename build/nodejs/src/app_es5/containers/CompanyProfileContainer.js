'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _CompanyProfile = require('../components/Frontend/CompanyProfile');

var _CompanyProfile2 = _interopRequireDefault(_CompanyProfile);

var _CompanyProfileStore = require('../stores/CompanyProfileStore');

var _CompanyProfileStore2 = _interopRequireDefault(_CompanyProfileStore);

var _UserStore = require('../stores/UserStore');

var _UserStore2 = _interopRequireDefault(_UserStore);

var _ResourceStore = require('../stores/ResourceStore');

var _ResourceStore2 = _interopRequireDefault(_ResourceStore);

var _MessageStore = require('../stores/MessageStore');

var _MessageStore2 = _interopRequireDefault(_MessageStore);

var _FeedStore = require('../stores/FeedStore');

var _FeedStore2 = _interopRequireDefault(_FeedStore);

var _JobStore = require('../stores/JobStore');

var _JobStore2 = _interopRequireDefault(_JobStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// stores


var CompanyProfileContainer = function (_Component) {
	_inherits(CompanyProfileContainer, _Component);

	function CompanyProfileContainer() {
		_classCallCheck(this, CompanyProfileContainer);

		return _possibleConstructorReturn(this, (CompanyProfileContainer.__proto__ || Object.getPrototypeOf(CompanyProfileContainer)).apply(this, arguments));
	}

	_createClass(CompanyProfileContainer, [{
		key: 'render',
		value: function render() {
			return typeof this.props.params.key !== 'undefined' || typeof this.state.user.token !== 'undefined' ? _react2.default.createElement(_CompanyProfile2.default, {
				profile: this.state.profile,
				user: this.state.user,
				status: this.state.status,
				profile_key: this.props.params.key,
				feed_id: this.props.params.feed_id,
				lang: this.state.lang,
				msg: this.state.msg,
				feed: this.state.feed,
				job: this.state.job,
				page: this.props.routes[this.props.routes.length - 1].name,
				editor: this.props.routes[this.props.routes.length - 1].name == "company_profile" }) : _react2.default.createElement('div', null);
		}
	}]);

	return CompanyProfileContainer;
}(_react.Component);

CompanyProfileContainer.getStores = function () {
	return [_CompanyProfileStore2.default, _UserStore2.default, _ResourceStore2.default, _MessageStore2.default, _JobStore2.default];
};
CompanyProfileContainer.calculateState = function (prevState) {
	return {
		profile: _CompanyProfileStore2.default.getState(),
		user: _UserStore2.default.getState(),
		status: _ResourceStore2.default.getState().status,
		lang: _ResourceStore2.default.getState().lang,
		msg: _MessageStore2.default.getState(),
		feed: _FeedStore2.default.getState(),
		job: _JobStore2.default.getState()
	};
};

exports.default = _utils.Container.create(CompanyProfileContainer);