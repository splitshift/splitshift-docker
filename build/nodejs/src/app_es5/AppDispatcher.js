'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _flux = require('flux');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AppDispatcher = function (_Dispatcher) {
  _inherits(AppDispatcher, _Dispatcher);

  function AppDispatcher() {
    _classCallCheck(this, AppDispatcher);

    return _possibleConstructorReturn(this, (AppDispatcher.__proto__ || Object.getPrototypeOf(AppDispatcher)).apply(this, arguments));
  }

  _createClass(AppDispatcher, [{
    key: 'dispatch',
    value: function dispatch() {
      var _this2 = this;

      var action = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      // console.log("Dispatched", action.type);
      setTimeout(function () {
        _get(AppDispatcher.prototype.__proto__ || Object.getPrototypeOf(AppDispatcher.prototype), 'dispatch', _this2).call(_this2, action);
      }, 1);
    }

    /**
     * Dispatches three actions for an async operation represented by promise.
     */

  }, {
    key: 'dispatchAsync',
    value: function dispatchAsync(promise, types, payload) {
      var _this3 = this;

      var request = types.request,
          success = types.success,
          failure = types.failure;


      setTimeout(function () {
        _this3.dispatch({ type: request, payload: Object.assign({}, payload) });
      }, 1);

      promise.then(function (response) {
        _this3.dispatch({
          type: success,
          payload: Object.assign({}, payload, { response: response })
        });
      }, function (error) {
        return _this3.dispatch({
          type: failure,
          payload: Object.assign({}, payload, { error: error })
        });
      });
    }
  }]);

  return AppDispatcher;
}(_flux.Dispatcher);

exports.default = new AppDispatcher();