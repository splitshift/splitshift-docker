'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var map1 = {};
var marker1 = {};
var self = void 0;

var defaultLat = 13.7563309;
var defaultLng = 100.5017651;

var Map = function (_Component) {
  _inherits(Map, _Component);

  function Map() {
    _classCallCheck(this, Map);

    return _possibleConstructorReturn(this, (Map.__proto__ || Object.getPrototypeOf(Map)).apply(this, arguments));
  }

  _createClass(Map, [{
    key: 'handleEvent',
    value: function handleEvent(evt) {
      self.props.changeLatLng(evt.latLng.lat(), evt.latLng.lng());
    }
  }, {
    key: 'initialize',
    value: function initialize(LagLngObj) {

      // let LagLngObj = new google.maps.LatLng(defaultLat, defaultLng);

      var mapProp = {
        center: LagLngObj,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map1[this.props.map_id] = new google.maps.Map(document.getElementById(this.props.map_id), mapProp);
      marker1[this.props.map_id] = new google.maps.Marker({
        map: map1[this.props.map_id],
        position: LagLngObj,
        draggable: true
      });

      if (this.props.map_id === 'googleMap2') this.props.triggerMap(map1[this.props.map_id]);

      marker1[this.props.map_id].addListener('dragend', this.handleEvent);
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      self = this;

      // if(this.props.address)
      //   this.props.changeLatLng(this.props.address.lat ? this.props.address.lat : defaultLat,	this.props.address.lng ? this.props.address.lng : defaultLng);

      var newLatlng = void 0;

      if (this.props.address.location[0] === 1 && this.props.address.location[1]) {
        newLatlng = new google.maps.LatLng(defaultLat, defaultLng);
        self.props.changeLatLng(defaultLat, defaultLng);
      } else newLatlng = new google.maps.LatLng(this.props.address.location[1], this.props.address.location[0]);

      // this.props.changeLatLng

      google.maps.event.addDomListener(window, 'load', this.initialize(newLatlng));
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {

      // console.log("Location");
      // console.log(this.props.address.location);

      // marker1[this.props.map_id].setPosition( new google.maps.LatLng( this.props.address.location[1], this.props.address.location[0] ) );
      // map1[this.props.map_id].setCenter(new google.maps.LatLng( this.props.address.location[1], this.props.address.location[0] ));
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { id: this.props.map_id },
        _react2.default.createElement(
          'span',
          { style: { color: "#aaa" } },
          'loading...'
        )
      );
    }
  }]);

  return Map;
}(_react.Component);

exports.default = Map;