'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _NewsletterActionCreators = require('../../actions/NewsletterActionCreators');

var _NewsletterActionCreators2 = _interopRequireDefault(_NewsletterActionCreators);

var _ResourceActionCreators = require('../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Footer = function (_Component) {
  _inherits(Footer, _Component);

  function Footer() {
    _classCallCheck(this, Footer);

    return _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).apply(this, arguments));
  }

  _createClass(Footer, [{
    key: 'postNewsletter',
    value: function postNewsletter(evt) {
      evt.preventDefault();
      var info = { email: this.refs.newsletter_email.value };
      _NewsletterActionCreators2.default.postNewsletter(info);
    }
  }, {
    key: 'changeLanguage',
    value: function changeLanguage(evt) {
      _ResourceActionCreators2.default.setLanguage(evt.target.value);
    }
  }, {
    key: 'render',
    value: function render() {
      var data = _configLang2.default[this.props.language].footer;
      return _react2.default.createElement(
        'footer',
        null,
        _react2.default.createElement(
          'div',
          { className: 'container' },
          _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
              'div',
              { className: 'col-md-4 col-sm-3' },
              _react2.default.createElement(
                'h3',
                null,
                data.mail
              ),
              _react2.default.createElement(
                'a',
                { href: 'mailto:info@splitshift.com', id: 'email_footer' },
                'info@splitshift.com'
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'col-md-3 col-sm-3' },
              _react2.default.createElement(
                'h3',
                null,
                data.about[0]
              ),
              _react2.default.createElement(
                'ul',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.language + '/aboutus' },
                    data.about[1]
                  )
                ),
                typeof this.props.user.token === 'undefined' ? _react2.default.createElement(
                  'span',
                  null,
                  _react2.default.createElement(
                    'li',
                    null,
                    _react2.default.createElement(
                      'a',
                      { href: '#', 'data-toggle': 'modal', 'data-target': '#myModal__login' },
                      data.about[2]
                    )
                  ),
                  _react2.default.createElement(
                    'li',
                    null,
                    _react2.default.createElement(
                      'a',
                      { href: '#', 'data-toggle': 'modal', 'data-target': '#myModal__login' },
                      data.about[3]
                    )
                  )
                ) : "",
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.language + "/privacy" },
                    data.about[4]
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.language + "/user-agreement" },
                    data.about[5]
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'col-md-3 col-sm-3' },
              _react2.default.createElement(
                'h3',
                null,
                data.discover[0]
              ),
              _react2.default.createElement(
                'ul',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.language + "/buzz" },
                    data.discover[1]
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.language + "/job-search" },
                    data.discover[2]
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.language + "/company-search" },
                    data.discover[3]
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.language + "/people-search" },
                    data.discover[4]
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.language + "/resource" },
                    data.discover[5]
                  )
                ),
                typeof this.props.user.token === 'undefined' ? _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    'a',
                    { href: '#', 'data-toggle': 'modal', 'data-target': '#myModal__login' },
                    _react2.default.createElement(
                      'span',
                      null,
                      data.discover[6]
                    )
                  )
                ) : ""
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'col-md-2 col-sm-3' },
              _react2.default.createElement(
                'h3',
                null,
                data.setting[0]
              ),
              _react2.default.createElement(
                'div',
                { className: 'fstyled-select' },
                _react2.default.createElement(
                  'select',
                  { className: 'form-control', name: 'lang', id: 'lang', onChange: this.changeLanguage.bind(this) },
                  _react2.default.createElement(
                    'option',
                    { value: '' },
                    data.setting[1]
                  ),
                  _react2.default.createElement(
                    'option',
                    { value: 'th' },
                    'Thai'
                  ),
                  _react2.default.createElement(
                    'option',
                    { value: 'en' },
                    'English'
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'fstyled-select' },
                _react2.default.createElement(
                  'select',
                  { className: 'form-control', name: 'contries', id: 'contries' },
                  _react2.default.createElement(
                    'option',
                    { value: '' },
                    data.setting[2]
                  ),
                  _react2.default.createElement(
                    'option',
                    { value: 'THA' },
                    'Thailand'
                  )
                )
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'row', id: 'social' },
            _react2.default.createElement(
              'div',
              { className: 'col-md-6' },
              _react2.default.createElement(
                'div',
                { id: 'social_footer' },
                _react2.default.createElement(
                  'ul',
                  null,
                  _react2.default.createElement(
                    'li',
                    null,
                    _react2.default.createElement(
                      'a',
                      { href: 'https://www.facebook.com/groups/splitshiftasia/', target: '_blank' },
                      _react2.default.createElement('i', { className: 'icon-facebook' })
                    )
                  ),
                  _react2.default.createElement(
                    'li',
                    null,
                    _react2.default.createElement(
                      'a',
                      { href: 'https://plus.google.com/u/0/b/111900430980684152993/111900430980684152993/about', target: '_blank' },
                      _react2.default.createElement('i', { className: 'icon-google' })
                    )
                  ),
                  _react2.default.createElement(
                    'li',
                    null,
                    _react2.default.createElement(
                      'a',
                      { href: 'https://www.instagram.com/splitshiftthailand/', target: '_blank' },
                      _react2.default.createElement('i', { className: 'icon-instagram' })
                    )
                  ),
                  _react2.default.createElement(
                    'li',
                    null,
                    _react2.default.createElement(
                      'a',
                      { href: 'https://www.youtube.com/channel/UC2f6uj4wk7tOv6KUWaZgVDA', target: '_blank' },
                      _react2.default.createElement('i', { className: 'icon-youtube-play' })
                    )
                  ),
                  _react2.default.createElement(
                    'li',
                    null,
                    _react2.default.createElement(
                      'a',
                      { href: 'https://www.linkedin.com/company/6447802?trk=tyah&trkInfo=clickedVertical%3Acompany%2CentityType%3AentityHistoryName%2CclickedEntityId%3Acompany_6447802%2Cidx%3A0', target: '_blank' },
                      _react2.default.createElement('i', { className: 'icon-linkedin' })
                    )
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'col-md-5' },
              _react2.default.createElement(
                'div',
                { className: 'subscribe-form' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'strong',
                      null,
                      data.newsletter[0]
                    )
                  )
                ),
                _react2.default.createElement(
                  'form',
                  { onSubmit: this.postNewsletter.bind(this) },
                  _react2.default.createElement(
                    'div',
                    { className: 'input-group' },
                    _react2.default.createElement('input', { ref: 'newsletter_email', type: 'text', className: 'form-control', placeholder: data.newsletter[1] }),
                    _react2.default.createElement(
                      'span',
                      { className: 'input-group-btn' },
                      _react2.default.createElement(
                        'button',
                        { id: 'input-group-footer', className: 'btn btn-default' },
                        data.newsletter[2]
                      )
                    )
                  )
                )
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'credit' },
            _react2.default.createElement(
              'p',
              null,
              '\xA9 Splitshift 2017'
            )
          )
        )
      );
    }
  }]);

  return Footer;
}(_react.Component);

exports.default = Footer;