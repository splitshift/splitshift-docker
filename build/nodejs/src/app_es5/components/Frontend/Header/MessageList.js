'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _MessageActionCreators = require('../../../actions/MessageActionCreators');

var _MessageActionCreators2 = _interopRequireDefault(_MessageActionCreators);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MessageList = function (_Component) {
  _inherits(MessageList, _Component);

  function MessageList() {
    _classCallCheck(this, MessageList);

    return _possibleConstructorReturn(this, (MessageList.__proto__ || Object.getPrototypeOf(MessageList)).apply(this, arguments));
  }

  _createClass(MessageList, [{
    key: 'loadMessage',
    value: function loadMessage(other, evt) {

      _MessageActionCreators2.default.chooseOwner(this.props.user);
      _MessageActionCreators2.default.chooseOther({ key: other });

      _MessageActionCreators2.default.getMessageWithPeople(this.props.user.token, other);
    }
  }, {
    key: 'render',
    value: function render() {

      var message = this.props.message;
      var other = "";

      if (typeof message.users[0] !== 'string') other = message.users[0];else other = message.users[1];

      return _react2.default.createElement(
        'a',
        { href: '#' },
        _react2.default.createElement(
          'li',
          { className: this.props.responsive ? "modal__navitem" : "noti__navitem", onClick: this.loadMessage.bind(this, other.key), 'data-toggle': 'modal', 'data-target': '#myModal__message' },
          _react2.default.createElement('img', { className: 'photo__noti', src: other.profile_image }),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'b',
              null,
              other.name
            ),
            _react2.default.createElement(
              'span',
              { className: 'date' },
              _JQueryTool2.default.changeDateAgo(message.messages[message.messages.length - 1].date)
            )
          ),
          _react2.default.createElement(
            'div',
            null,
            message.messages[message.messages.length - 1].message_type == 'image' ? 'Sent an image.' : message.messages[message.messages.length - 1].message
          )
        )
      );
    }
  }]);

  return MessageList;
}(_react.Component);

exports.default = MessageList;