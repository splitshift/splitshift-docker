'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _SearchActionCreators = require('../../../actions/SearchActionCreators');

var _SearchActionCreators2 = _interopRequireDefault(_SearchActionCreators);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DrawerHeader = function (_Component) {
  _inherits(DrawerHeader, _Component);

  function DrawerHeader() {
    _classCallCheck(this, DrawerHeader);

    return _possibleConstructorReturn(this, (DrawerHeader.__proto__ || Object.getPrototypeOf(DrawerHeader)).apply(this, arguments));
  }

  _createClass(DrawerHeader, [{
    key: 'updateFilter',
    value: function updateFilter(name, evt) {
      var response = {};
      response[name] = evt.target.value;
      _SearchActionCreators2.default.updateFilter(response);
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {
      evt.preventDefault();
      var keyword = this.props.search.filter.all_keyword ? this.props.search.filter.all_keyword : "";
      _SearchActionCreators2.default.All(keyword);
      _reactRouter.browserHistory.push('/search');
      _JQueryTool2.default.toggleDrawer();
    }
  }, {
    key: 'toggleDrawer',
    value: function toggleDrawer() {
      _JQueryTool2.default.toggleDrawer();
    }
  }, {
    key: 'render',
    value: function render() {
      var headers = _configLang2.default[this.props.language].headers;

      return _react2.default.createElement(
        'div',
        { className: 'drawer', onClick: this.toggleDrawer.bind(this) },
        _react2.default.createElement(
          'div',
          { className: 'drawer__list' },
          _react2.default.createElement(
            'ul',
            { className: 'navbar-default' },
            _react2.default.createElement(
              'div',
              { className: 'drawer__btnlist' },
              _react2.default.createElement(
                'li',
                { className: 'drawer__btn--search' },
                _react2.default.createElement(
                  'form',
                  { onSubmit: this.onSearch.bind(this) },
                  _react2.default.createElement(
                    'div',
                    { className: 'input-group' },
                    _react2.default.createElement('input', { type: 'text', className: 'form-control', onChange: this.updateFilter.bind(this, 'all_keyword'), placeholder: 'search people, jobs, companies ...' }),
                    _react2.default.createElement(
                      'span',
                      { className: 'input-group-btn' },
                      _react2.default.createElement(
                        'button',
                        { className: 'btn btn-default', style: { marginLeft: 0 } },
                        _react2.default.createElement('i', { className: 'icon-search' })
                      )
                    )
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'drawer__navlist' },
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language + "/buzz", onClick: _JQueryTool2.default.toggleDrawer.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'drawer__navitem', role: 'presentation' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    headers[0]
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language + "/people-search", onClick: _JQueryTool2.default.toggleDrawer.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'drawer__navitem', role: 'presentation' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    headers[1]
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language + "/job-search", onClick: _JQueryTool2.default.toggleDrawer.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'drawer__navitem', role: 'presentation' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    headers[2]
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language + "/company-search", onClick: _JQueryTool2.default.toggleDrawer.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'drawer__navitem', role: 'presentation' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    headers[3]
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language + "/resource", onClick: _JQueryTool2.default.toggleDrawer.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'drawer__navitem', role: 'presentation' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    headers[4]
                  )
                )
              ),
              this.props.user && typeof this.props.user.token === 'undefined' ? _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', 'data-toggle': 'modal', 'data-target': '#myModal__login', onClick: _JQueryTool2.default.toggleDrawer.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'drawer__navitem', role: 'presentation' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    headers[5]
                  )
                )
              ) : "",
              this.props.user && typeof this.props.user.token === 'undefined' ? _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', 'data-toggle': 'modal', 'data-target': '#myModal__login', onClick: _JQueryTool2.default.toggleDrawer.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'drawer__navitem', role: 'presentation' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    headers[6]
                  )
                )
              ) : ""
            )
          )
        )
      );
    }
  }]);

  return DrawerHeader;
}(_react.Component);

exports.default = DrawerHeader;