'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _Login = require('../Auth/Login');

var _Login2 = _interopRequireDefault(_Login);

var _Register = require('../Auth/Register');

var _Register2 = _interopRequireDefault(_Register);

var _PostRegister = require('../Auth/PostRegister');

var _PostRegister2 = _interopRequireDefault(_PostRegister);

var _ForgetPassword = require('../Auth/ForgetPassword');

var _ForgetPassword2 = _interopRequireDefault(_ForgetPassword);

var _Drawer = require('./Drawer');

var _Drawer2 = _interopRequireDefault(_Drawer);

var _NavList = require('./NavList');

var _NavList2 = _interopRequireDefault(_NavList);

var _SearchBar = require('./SearchBar');

var _SearchBar2 = _interopRequireDefault(_SearchBar);

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var registerTrigger = false;

var MainHeader = function (_Component) {
  _inherits(MainHeader, _Component);

  function MainHeader() {
    _classCallCheck(this, MainHeader);

    return _possibleConstructorReturn(this, (MainHeader.__proto__ || Object.getPrototypeOf(MainHeader)).apply(this, arguments));
  }

  _createClass(MainHeader, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _JQueryTool2.default.drawer();
    }
  }, {
    key: 'changeLanguage',
    value: function changeLanguage(evt) {
      _ResourceActionCreators2.default.setLanguage(evt.target.value);
    }
  }, {
    key: 'registerBtnTrigger',
    value: function registerBtnTrigger(state) {
      registerTrigger = state;
      this.setState({});
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'header',
        { id: 'header--bar' },
        _react2.default.createElement(
          'div',
          { className: 'topbar' },
          _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
              'div',
              { className: 'container-fluid' },
              _react2.default.createElement(
                'nav',
                { className: 'navbar navbar-default' },
                _react2.default.createElement(
                  'button',
                  { type: 'button', className: 'topbar__btn--toggle' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement('i', { className: 'fa fa-bars' })
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'navbar-header' },
                  _react2.default.createElement(
                    'div',
                    { className: 'navbar__logo' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/' + this.props.language, className: 'navbar-brand' },
                      _react2.default.createElement('img', { className: 'logo__large', src: '/images/logo-splitshift.png', alt: 'logo' })
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'topbar__btn--language' },
                  _react2.default.createElement(
                    'div',
                    { className: 'form-group' },
                    _react2.default.createElement(
                      'select',
                      { className: 'form-control', id: 'lang', onChange: this.changeLanguage.bind(this), value: this.props.language },
                      _react2.default.createElement(
                        'option',
                        { value: 'th' },
                        'THA'
                      ),
                      _react2.default.createElement(
                        'option',
                        { value: 'en' },
                        'ENG'
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'appbar', id: 'myNavbar' },
                  _react2.default.createElement(
                    'div',
                    { className: 'nav__btnlist' },
                    _react2.default.createElement(
                      'ul',
                      { className: 'nav navbar-nav navbar-right' },
                      _react2.default.createElement(
                        'li',
                        { className: 'nav__btn--search' },
                        _react2.default.createElement(_SearchBar2.default, { language: this.props.language, search: this.props.search })
                      ),
                      _react2.default.createElement(
                        'li',
                        { className: 'nav__btn--employer' },
                        _react2.default.createElement(
                          'a',
                          { href: '#' },
                          _react2.default.createElement(
                            'button',
                            { className: 'btn btn-default', 'data-toggle': 'modal', 'data-target': '#myModal__login', type: 'button' },
                            this.props.language === 'th' ? 'นายจ้าง' : 'Employer'
                          )
                        )
                      ),
                      _react2.default.createElement(
                        'li',
                        { className: 'nav__btn--user' },
                        _react2.default.createElement(
                          'a',
                          { href: '#' },
                          _react2.default.createElement(
                            'button',
                            { className: 'btn btn-default', 'data-toggle': 'modal', 'data-target': '#myModal__login', type: 'button' },
                            _react2.default.createElement('i', { className: 'icon-user-1' }),
                            ' ',
                            this.props.language === 'th' ? 'เข้าสู่ระบบ' : 'Login'
                          )
                        )
                      )
                    )
                  ),
                  _react2.default.createElement(_NavList2.default, { currentPage: this.props.currentPage, language: this.props.language })
                )
              ),
              _react2.default.createElement(_Drawer2.default, { search: this.props.search, user: this.props.user, language: this.props.language })
            ),
            _react2.default.createElement(_Login2.default, { registerBtnTrigger: this.registerBtnTrigger.bind(this), state: this.props.login, language: this.props.language }),
            _react2.default.createElement(_Register2.default, { registerTrigger: registerTrigger, registerBtnTrigger: this.registerBtnTrigger.bind(this), language: this.props.language }),
            _react2.default.createElement(_PostRegister2.default, { user: this.props.user, language: this.props.language }),
            _react2.default.createElement(_ForgetPassword2.default, { login: this.props.login, language: this.props.language })
          )
        )
      );
    }
  }]);

  return MainHeader;
}(_react.Component);

exports.default = MainHeader;