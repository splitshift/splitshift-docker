'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _SocialActionCreators = require('../../../actions/SocialActionCreators');

var _SocialActionCreators2 = _interopRequireDefault(_SocialActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FriendList = function (_Component) {
  _inherits(FriendList, _Component);

  function FriendList() {
    _classCallCheck(this, FriendList);

    return _possibleConstructorReturn(this, (FriendList.__proto__ || Object.getPrototypeOf(FriendList)).apply(this, arguments));
  }

  _createClass(FriendList, [{
    key: 'onResponseConnect',
    value: function onResponseConnect(response, evt) {

      var result = {
        request_id: this.props.friend._id,
        response: response
      };

      _SocialActionCreators2.default.setDraft("friend_index", this.props.friend_index);
      _SocialActionCreators2.default.putConnectResponse(this.props.user.token, result);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'li',
        { className: this.props.type == "notification" ? "noti__navitem" : "modal__navitem" },
        _react2.default.createElement(
          _reactRouter.Link,
          { to: "/" + this.props.language + "/user/" + this.props.friend.from.key },
          _react2.default.createElement('img', { className: 'photo__noti', src: this.props.friend.from.profile_image ? this.props.friend.from.profile_image : "/images/avatar-image.jpg" }),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'b',
              null,
              this.props.friend.from.name
            ),
            _react2.default.createElement(
              'span',
              { className: 'dateconnect' },
              _JQueryTool2.default.changeDateAgo(this.props.friend.date)
            )
          ),
          _react2.default.createElement(
            'div',
            null,
            'wants to connect with you.'
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'connect__action' },
          _react2.default.createElement(
            'ul',
            null,
            _react2.default.createElement(
              'li',
              { onClick: this.onResponseConnect.bind(this, true) },
              _react2.default.createElement('i', { className: 'fa fa-check' })
            ),
            _react2.default.createElement(
              'li',
              { onClick: this.onResponseConnect.bind(this, false) },
              _react2.default.createElement('i', { className: 'fa fa-times' })
            )
          )
        )
      );
    }
  }]);

  return FriendList;
}(_react.Component);

exports.default = FriendList;