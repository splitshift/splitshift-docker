'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SearchActionCreators = require('../../../actions/SearchActionCreators');

var _SearchActionCreators2 = _interopRequireDefault(_SearchActionCreators);

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SearchBar = function (_Component) {
  _inherits(SearchBar, _Component);

  function SearchBar() {
    _classCallCheck(this, SearchBar);

    return _possibleConstructorReturn(this, (SearchBar.__proto__ || Object.getPrototypeOf(SearchBar)).apply(this, arguments));
  }

  _createClass(SearchBar, [{
    key: 'updateFilter',
    value: function updateFilter(name, evt) {
      var response = {};
      response[name] = evt.target.value;
      _SearchActionCreators2.default.updateFilter(response);
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {
      evt.preventDefault();
      var keyword = this.props.search.filter.all_keyword ? this.props.search.filter.all_keyword : "";
      _SearchActionCreators2.default.All(keyword);
      _reactRouter.browserHistory.push('/search');
      $(".dropdown-search").removeClass("open");
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'dropdown dropdown-search' },
        _react2.default.createElement(
          'a',
          { href: '#', className: 'dropdown-toggle', 'data-toggle': 'dropdown' },
          _react2.default.createElement('i', { className: 'icon-search' })
        ),
        _react2.default.createElement(
          'div',
          { className: 'dropdown-menu' },
          _react2.default.createElement(
            'form',
            { onSubmit: this.onSearch.bind(this) },
            _react2.default.createElement(
              'div',
              { className: 'input-group' },
              _react2.default.createElement('input', { type: 'text', onChange: this.updateFilter.bind(this, 'all_keyword'), className: 'form-control', placeholder: this.props.language === 'th' ? 'ค้นหา บุคคล งาน บริษัทต่างๆ' : 'search people, jobs, companies ...' }),
              _react2.default.createElement(
                'span',
                { className: 'input-group-btn' },
                _react2.default.createElement(
                  'button',
                  { className: 'btn btn-default' },
                  _react2.default.createElement('i', { className: 'icon-search' })
                )
              )
            )
          )
        )
      );
    }
  }]);

  return SearchBar;
}(_react.Component);

exports.default = SearchBar;