'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _ConfigData = require('../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _SocialActionCreators = require('../../../actions/SocialActionCreators');

var _SocialActionCreators2 = _interopRequireDefault(_SocialActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NotificationList = function (_Component) {
  _inherits(NotificationList, _Component);

  function NotificationList() {
    _classCallCheck(this, NotificationList);

    return _possibleConstructorReturn(this, (NotificationList.__proto__ || Object.getPrototypeOf(NotificationList)).apply(this, arguments));
  }

  _createClass(NotificationList, [{
    key: 'deleteNotification',

    //
    // componentDidMount () {
    //   console.log("Notification List");
    //   console.log(this.props.action);
    // }

    value: function deleteNotification(notification) {
      _SocialActionCreators2.default.deleteNotification(this.props.user.token, notification._id);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'li',
        { className: this.props.responsive ? "modal__navitem" : "noti__navitem" },
        _react2.default.createElement('div', { className: 'photo__noti', style: { background: "url(" + (this.props.action.from.profile_image || "/images/avatar-image.jpg") + ") no-repeat center center / cover", marginLeft: "0px" } }),
        _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            'b',
            null,
            this.props.action.from.name,
            ' '
          ),
          ' ',
          _ConfigData2.default.ConvertNotification(this.props.action.notification_type, this.props.action.feed)
        ),
        _react2.default.createElement(
          'div',
          null,
          _JQueryTool2.default.changeDateAgo(this.props.action.date)
        ),
        _react2.default.createElement(
          'div',
          { className: 'connect__action' },
          _react2.default.createElement(
            'li',
            { style: { padding: "4px", cursor: "pointer" }, onClick: this.deleteNotification.bind(this, this.props.action) },
            _react2.default.createElement('i', { className: 'fa fa-times' })
          )
        )
      );
    }
  }]);

  return NotificationList;
}(_react.Component);

exports.default = NotificationList;