'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NavList = function (_Component) {
  _inherits(NavList, _Component);

  function NavList() {
    _classCallCheck(this, NavList);

    return _possibleConstructorReturn(this, (NavList.__proto__ || Object.getPrototypeOf(NavList)).apply(this, arguments));
  }

  _createClass(NavList, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var headers = _configLang2.default[this.props.language].headers;
      var navLink = [{
        name: headers[0],
        link: "/" + this.props.language + "/buzz",
        route_name: 'buzz'
      }, {
        name: headers[1],
        link: "/" + this.props.language + "/people-search",
        route_name: "people-search"
      }, {
        name: headers[2],
        link: "/" + this.props.language + "/job-search",
        route_name: "job-search"
      }, {
        name: headers[3],
        link: "/" + this.props.language + '/company-search',
        route_name: "company-search"
      }, {
        name: headers[4],
        link: "/" + this.props.language + "/resource",
        route_name: "resource_index"
      }];

      return _react2.default.createElement(
        'div',
        { className: 'nav__navlist' },
        _react2.default.createElement(
          'ul',
          { className: 'nav navbar-nav navbar-right' },
          navLink.map(function (nav, index) {
            return _react2.default.createElement(
              'li',
              { key: index, className: _this2.props.currentPage == nav.route_name ? "navlist__item navlist__item--active" : "navlist__item", role: 'presentation' },
              _react2.default.createElement(
                _reactRouter.Link,
                { to: nav.link },
                _react2.default.createElement(
                  'span1',
                  null,
                  nav.name
                )
              )
            );
          })
        )
      );
    }
  }]);

  return NavList;
}(_react.Component);

exports.default = NavList;