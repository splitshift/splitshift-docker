'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _FriendList = require('./FriendList');

var _FriendList2 = _interopRequireDefault(_FriendList);

var _NotificationList = require('./NotificationList');

var _NotificationList2 = _interopRequireDefault(_NotificationList);

var _MessageList = require('./MessageList');

var _MessageList2 = _interopRequireDefault(_MessageList);

var _SocialActionCreators = require('../../../actions/SocialActionCreators');

var _SocialActionCreators2 = _interopRequireDefault(_SocialActionCreators);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _ConfigData = require('../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var menuClass = ['profile--menu', 'noti--menu', 'mesg--menu', 'connect--menu'];

var Notification = function (_Component) {
  _inherits(Notification, _Component);

  function Notification() {
    _classCallCheck(this, Notification);

    return _possibleConstructorReturn(this, (Notification.__proto__ || Object.getPrototypeOf(Notification)).apply(this, arguments));
  }

  _createClass(Notification, [{
    key: 'triggerMenu',
    value: function triggerMenu(name, evt) {
      evt.stopPropagation();

      menuClass.map(function (menu, index) {
        if (name == menu) $("." + name).toggle();else $("." + menu).hide();
      });

      if (name === 'noti--menu' && !this.props.social.seen_notification) _SocialActionCreators2.default.putNotificationSeen(this.props.user.token);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.social.deleting && this.props.social.deleting) _SocialActionCreators2.default.getNotification(this.props.user.token);

      return JSON.stringify(nextProps.user) !== JSON.stringify(this.props.user) || JSON.stringify(this.props.social.actions) !== JSON.stringify(nextProps.social.actions) || JSON.stringify(this.props.social.messages) !== JSON.stringify(nextProps.social.messages) || this.props.social.friends.length !== nextProps.social.friends.length || nextProps.language !== this.props.language;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this,
          _React$createElement;

      var menusContent = _configLang2.default[this.props.language].menus;
      var actionContent = menusContent.action;
      var notiContent = menusContent.notification;

      return _react2.default.createElement(
        'div',
        { className: 'nav__notification' },
        _react2.default.createElement(
          'ul',
          { className: 'nav navbar-nav navbar-right' },
          _react2.default.createElement(
            'li',
            null,
            _react2.default.createElement(
              'div',
              { className: 'dropdown dropdown-search noti--mesg', onClick: this.triggerMenu.bind(this, 'mesg--menu') },
              this.props.social.messages.filter(function (message) {
                return !message.seen[_this2.props.user.key];
              }).length ? _react2.default.createElement(
                'div',
                { className: 'noti__icon' },
                _react2.default.createElement('img', { src: '/images/icon-dot.png' }),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement(
                    'p',
                    null,
                    this.props.social.messages.filter(function (message) {
                      return !message.seen[_this2.props.user.key];
                    }).length
                  )
                )
              ) : "",
              _react2.default.createElement(
                'a',
                { className: 'headder__noti dropdown-toggle', 'data-toggle': 'dropdown' },
                _react2.default.createElement('i', { className: 'icon-mail' })
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: "mesg--menu " + (this.props.user.type !== 'Employee' && 'mesg--menu-company') },
            _react2.default.createElement(
              'div',
              { className: 'up--menu' },
              _react2.default.createElement(
                'b',
                null,
                _react2.default.createElement('i', { className: 'fa fa-caret-up' })
              )
            ),
            _react2.default.createElement(
              'nav',
              { className: 'navbar-default' },
              _react2.default.createElement(
                'div',
                { className: 'noti__navlist' },
                _react2.default.createElement(
                  'a',
                  { href: '#' },
                  _react2.default.createElement(
                    'li',
                    { className: 'profile__navitem' },
                    _react2.default.createElement(
                      'span',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        notiContent[0],
                        ' (',
                        this.props.social.messages.length,
                        ')'
                      )
                    )
                  )
                ),
                this.props.social.messages.map(function (message, index) {
                  return _react2.default.createElement(_MessageList2.default, { key: index, user: _this2.props.user, message: message, responsive: false });
                }),
                this.props.social.messages.length > 5 ? _react2.default.createElement(
                  'a',
                  { href: '#' },
                  _react2.default.createElement(
                    'li',
                    { className: 'seeall', 'data-toggle': 'modal', 'data-target': '#myModal__mesgmenu' },
                    _react2.default.createElement(
                      'span',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        'see all'
                      )
                    )
                  )
                ) : ""
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'modal fade', id: 'myModal__mesgmenu', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
            _react2.default.createElement(
              'div',
              { className: 'modal-dialog', role: 'document' },
              _react2.default.createElement(
                'div',
                { className: 'modal-content' },
                _react2.default.createElement(
                  'div',
                  { className: 'modal-header' },
                  _react2.default.createElement(
                    'div',
                    { className: 'modal-title' },
                    _react2.default.createElement(
                      'b',
                      null,
                      _react2.default.createElement(
                        'span',
                        null,
                        notiContent[0],
                        ' (',
                        this.props.social.messages.length,
                        ')'
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'modal-body' },
                  _react2.default.createElement(
                    'div',
                    { className: 'modal__navlist' },
                    this.props.social.messages.map(function (message, index) {
                      return _react2.default.createElement(_MessageList2.default, { key: index, user: _this2.props.user, message: message, responsive: true });
                    })
                  )
                )
              )
            )
          ),
          _react2.default.createElement(
            'li',
            null,
            _react2.default.createElement(
              'div',
              { className: 'dropdown dropdown-search noti--noti', onClick: this.triggerMenu.bind(this, 'noti--menu') },
              this.props.social.actions.filter(function (action) {
                return !action.seen;
              }).length > 0 ? _react2.default.createElement(
                'div',
                { className: 'noti__icon' },
                _react2.default.createElement('img', { src: '/images/icon-dot.png' }),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement(
                    'p',
                    null,
                    this.props.social.actions.length
                  )
                )
              ) : "",
              _react2.default.createElement(
                'a',
                { className: 'headder__noti dropdown-toggle', 'data-toggle': 'dropdown' },
                _react2.default.createElement('i', { className: 'icon-flag-empty' })
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: "noti--menu " + (this.props.user.type !== 'Employee' && 'noti--menu-company') },
            _react2.default.createElement(
              'div',
              { className: 'up--menu' },
              _react2.default.createElement(
                'b',
                null,
                _react2.default.createElement('i', { className: 'fa fa-caret-up' })
              )
            ),
            _react2.default.createElement(
              'nav',
              { className: 'navbar-default' },
              _react2.default.createElement(
                'div',
                { className: 'noti__navlist' },
                _react2.default.createElement(
                  'a',
                  { href: '#' },
                  _react2.default.createElement(
                    'li',
                    { className: 'profile__navitem' },
                    _react2.default.createElement(
                      'span',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        notiContent[1],
                        ' (',
                        this.props.social.actions.length,
                        ')'
                      )
                    )
                  )
                ),
                this.props.social.actions.slice(0, 5).map(function (action, index) {
                  return _react2.default.createElement(_NotificationList2.default, { key: index, action: action, user: _this2.props.user, responsive: false });
                }),
                this.props.social.actions.length > 5 ? _react2.default.createElement(
                  'a',
                  { href: '#' },
                  _react2.default.createElement(
                    'li',
                    { className: 'seeall', 'data-toggle': 'modal', 'data-target': '#myModal__notimenu' },
                    _react2.default.createElement(
                      'span',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        'see all'
                      )
                    )
                  )
                ) : ""
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'modal fade', id: 'myModal__notimenu', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
            _react2.default.createElement(
              'div',
              { className: 'modal-dialog', role: 'document' },
              _react2.default.createElement(
                'div',
                { className: 'modal-content' },
                _react2.default.createElement(
                  'div',
                  { className: 'modal-header' },
                  _react2.default.createElement(
                    'div',
                    { className: 'modal-title' },
                    _react2.default.createElement(
                      'b',
                      null,
                      _react2.default.createElement(
                        'span',
                        null,
                        notiContent[1],
                        ' (',
                        this.props.social.actions.length,
                        ')'
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'modal-body' },
                  _react2.default.createElement(
                    'div',
                    { className: 'modal__navlist' },
                    this.props.social.actions.map(function (action, index) {
                      return _react2.default.createElement(_NotificationList2.default, { key: index, action: action, user: _this2.props.user, responsive: true });
                    })
                  )
                )
              )
            )
          ),
          this.props.user.type == 'Employee' ? _react2.default.createElement(
            'li',
            null,
            _react2.default.createElement(
              'div',
              { className: 'dropdown dropdown-search noti--connect', onClick: this.triggerMenu.bind(this, 'connect--menu') },
              this.props.social.friends.length ? _react2.default.createElement(
                'div',
                { className: 'noti__icon' },
                _react2.default.createElement('img', { src: '/images/icon-dot.png' }),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement(
                    'p',
                    null,
                    this.props.social.friends.length
                  )
                )
              ) : "",
              _react2.default.createElement(
                'a',
                (_React$createElement = { className: 'headder__noti' }, _defineProperty(_React$createElement, 'className', 'dropdown-toggle'), _defineProperty(_React$createElement, 'data-toggle', 'dropdown'), _React$createElement),
                _react2.default.createElement('i', { className: 'icon-user' })
              )
            )
          ) : "",
          _react2.default.createElement(
            'div',
            { className: 'connect--menu' },
            _react2.default.createElement(
              'div',
              { className: 'up--menu' },
              _react2.default.createElement(
                'b',
                null,
                _react2.default.createElement('i', { className: 'fa fa-caret-up' })
              )
            ),
            _react2.default.createElement(
              'nav',
              { className: 'navbar-default' },
              _react2.default.createElement(
                'div',
                { className: 'noti__navlist' },
                _react2.default.createElement(
                  'a',
                  { href: '#' },
                  _react2.default.createElement(
                    'li',
                    { className: 'profile__navitem' },
                    _react2.default.createElement(
                      'span',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        notiContent[2],
                        ' (',
                        this.props.social.friends.length,
                        ')'
                      )
                    )
                  )
                ),
                this.props.social.friends.slice(0, 5).map(function (friend, index) {
                  return _react2.default.createElement(_FriendList2.default, { language: _this2.props.language, key: index, friend_index: index, friend: friend, user: _this2.props.user, type: 'notification' });
                }),
                this.props.social.friends.length > 5 ? _react2.default.createElement(
                  'a',
                  { href: '#' },
                  _react2.default.createElement(
                    'li',
                    { className: 'seeall', 'data-toggle': 'modal', 'data-target': '#myModal__connectmenu' },
                    _react2.default.createElement(
                      'span',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        'see all'
                      )
                    )
                  )
                ) : ""
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'modal fade', id: 'myModal__connectmenu', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
            _react2.default.createElement(
              'div',
              { className: 'modal-dialog', role: 'document' },
              _react2.default.createElement(
                'div',
                { className: 'modal-content' },
                _react2.default.createElement(
                  'div',
                  { className: 'modal-header' },
                  _react2.default.createElement(
                    'div',
                    { className: 'modal-title' },
                    _react2.default.createElement(
                      'b',
                      null,
                      _react2.default.createElement(
                        'span',
                        null,
                        notiContent[2],
                        ' (',
                        this.props.social.friends.length,
                        ')'
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'modal-body' },
                  _react2.default.createElement(
                    'div',
                    { className: 'modal__navlist' },
                    this.props.social.friends.map(function (friend, index) {
                      return _react2.default.createElement(_FriendList2.default, { key: index, friend_index: index, friend: friend, user: _this2.props.user, type: 'modal' });
                    })
                  )
                )
              )
            )
          ),
          _react2.default.createElement(
            'li',
            null,
            _react2.default.createElement(
              'div',
              { className: 'user__icon' },
              _react2.default.createElement('div', { className: 'user__iconimage', onClick: this.triggerMenu.bind(this, 'profile--menu'), style: { background: "url(" + (this.props.user.profile_image || "/images/avatar.jpg") + ") no-repeat center center / cover" } }),
              _react2.default.createElement(
                'div',
                { className: 'profile--menu' },
                _react2.default.createElement(
                  'div',
                  { className: 'up--menu' },
                  _react2.default.createElement(
                    'b',
                    null,
                    _react2.default.createElement('i', { className: 'fa fa-caret-up' })
                  )
                ),
                _react2.default.createElement(
                  'nav',
                  { className: 'navbar-default' },
                  _react2.default.createElement(
                    'div',
                    { className: 'profile__navlist' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: this.props.user.type === 'Employee' ? '/' + this.props.language + '/profile' : '/' + this.props.language + '/company' },
                      _react2.default.createElement(
                        'li',
                        { className: 'noti__navitem' },
                        _react2.default.createElement(
                          'span',
                          null,
                          actionContent[0]
                        )
                      )
                    ),
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/' + this.props.language + (this.props.user.type === 'Employee' ? "/dashboard/user" : "/dashboard/company") },
                      _react2.default.createElement(
                        'li',
                        { className: 'noti__navitem' },
                        _react2.default.createElement(
                          'span',
                          null,
                          actionContent[1]
                        )
                      )
                    ),
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/' + this.props.language + "/setting" },
                      _react2.default.createElement(
                        'li',
                        { className: 'noti__navitem' },
                        _react2.default.createElement(
                          'span',
                          null,
                          actionContent[2]
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'a',
                      { href: 'javascript:void(0)', onClick: this.props.onLogout.bind(null) },
                      _react2.default.createElement(
                        'li',
                        { className: 'noti__navitem' },
                        _react2.default.createElement(
                          'span',
                          null,
                          actionContent[3]
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return Notification;
}(_react.Component);

exports.default = Notification;

/*
  <a href="#"><li className="seeall" data-toggle="modal" data-target="#myModal__notimenu">
    <span><b>see all</b></span>
  </li></a>
*/