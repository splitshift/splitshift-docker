'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _Drawer = require('./Drawer');

var _Drawer2 = _interopRequireDefault(_Drawer);

var _NavList = require('./NavList');

var _NavList2 = _interopRequireDefault(_NavList);

var _SearchBar = require('./SearchBar');

var _SearchBar2 = _interopRequireDefault(_SearchBar);

var _AuthenticationActionCreators = require('../../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var menuClass = ['profile--menu', 'noti--menu', 'mesg--menu', 'connect--menu'];

var AdminHeader = function (_Component) {
  _inherits(AdminHeader, _Component);

  function AdminHeader() {
    _classCallCheck(this, AdminHeader);

    return _possibleConstructorReturn(this, (AdminHeader.__proto__ || Object.getPrototypeOf(AdminHeader)).apply(this, arguments));
  }

  _createClass(AdminHeader, [{
    key: 'onLogout',
    value: function onLogout() {
      _AuthenticationActionCreators2.default.logout();
    }
  }, {
    key: 'triggerMenu',
    value: function triggerMenu(name, evt) {
      evt.stopPropagation();

      menuClass.map(function (menu, index) {
        if (name == menu) $("." + name).toggle();else $("." + menu).hide();
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      _JQueryTool2.default.drawer();
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'header',
        { id: 'header--bar' },
        _react2.default.createElement(
          'div',
          { className: 'topbar' },
          _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
              'div',
              { className: 'container-fluid' },
              _react2.default.createElement(
                'nav',
                { className: 'navbar navbar-default' },
                _react2.default.createElement(
                  'button',
                  { type: 'button', className: 'topbar__btn--toggle' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement('i', { className: 'fa fa-bars' })
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'navbar-header' },
                  _react2.default.createElement(
                    'div',
                    { className: 'navbar__logo' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/' + this.props.language, className: 'navbar-brand' },
                      _react2.default.createElement('img', { className: 'logo--large', src: '/images/logo-splitshift.png', alt: 'logo' })
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'navbar__logo' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/' + this.props.language, className: 'navbar-brand' },
                      _react2.default.createElement('img', { className: 'logo--small', src: '/images/logo-splitshift-responsive.png', alt: 'logo' })
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'nav__notification nav__admin' },
                  _react2.default.createElement(
                    'ul',
                    { className: 'nav navbar-nav navbar-right' },
                    _react2.default.createElement(
                      'li',
                      null,
                      _react2.default.createElement(
                        'div',
                        { className: 'user__icon' },
                        _react2.default.createElement('img', { id: 'profile--menubtn', src: '/images/profiles/admin.png', onClick: this.triggerMenu.bind(this, 'profile--menu') }),
                        _react2.default.createElement(
                          'div',
                          { className: 'profile--menu' },
                          _react2.default.createElement(
                            'div',
                            { className: 'up--menu' },
                            _react2.default.createElement(
                              'b',
                              null,
                              _react2.default.createElement('i', { className: 'fa fa-caret-up' })
                            )
                          ),
                          _react2.default.createElement(
                            'nav',
                            { className: 'navbar-default' },
                            _react2.default.createElement(
                              'div',
                              { className: 'profile__navlist' },
                              _react2.default.createElement(
                                _reactRouter.Link,
                                { to: '/' + this.props.language + "/admin" },
                                _react2.default.createElement(
                                  'li',
                                  { className: 'noti__navitem' },
                                  _react2.default.createElement(
                                    'span',
                                    null,
                                    'Main Menu'
                                  )
                                )
                              ),
                              _react2.default.createElement(
                                'a',
                                { href: 'javascript:void(0)', onClick: this.onLogout.bind(null) },
                                _react2.default.createElement(
                                  'li',
                                  { className: 'noti__navitem' },
                                  _react2.default.createElement(
                                    'span',
                                    null,
                                    'Log Out'
                                  )
                                )
                              )
                            )
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'appbar', id: 'myNavbar' },
                  _react2.default.createElement(
                    'div',
                    { className: 'nav__btnlist' },
                    _react2.default.createElement(
                      'ul',
                      { className: 'nav navbar-nav navbar-right' },
                      _react2.default.createElement(
                        'li',
                        { className: 'nav__btn--search' },
                        _react2.default.createElement(_SearchBar2.default, { search: this.props.search })
                      )
                    )
                  ),
                  _react2.default.createElement(_NavList2.default, { currentPage: this.props.currentPage, language: this.props.language })
                )
              ),
              _react2.default.createElement(_Drawer2.default, { search: this.props.search, user: this.props.user, language: this.props.language })
            )
          )
        )
      );
    }
  }]);

  return AdminHeader;
}(_react.Component);

exports.default = AdminHeader;