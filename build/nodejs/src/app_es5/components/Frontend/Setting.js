'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SettingActionCreators = require('../../actions/SettingActionCreators');

var _SettingActionCreators2 = _interopRequireDefault(_SettingActionCreators);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Setting = function (_Component) {
  _inherits(Setting, _Component);

  function Setting() {
    _classCallCheck(this, Setting);

    return _possibleConstructorReturn(this, (Setting.__proto__ || Object.getPrototypeOf(Setting)).apply(this, arguments));
  }

  _createClass(Setting, [{
    key: 'changeDraft',
    value: function changeDraft(name, evt) {
      var response = {};
      response[name] = evt.target.value;

      _SettingActionCreators2.default.changeDraft(response);
    }
  }, {
    key: 'changePassword',
    value: function changePassword(evt) {
      evt.preventDefault();
      if (this.props.user.setting.new_password !== this.props.user.setting.new_password_again) {
        alert("New password not same");
        return "";
      }

      var result = {
        old_password: this.props.user.setting.old_password,
        new_password: this.props.user.setting.new_password
      };

      this.refs.old_password.value = "";
      this.refs.new_password.value = "";
      this.refs.new_password_again.value = "";

      _SettingActionCreators2.default.changePassword(this.props.user.token, result);
    }
  }, {
    key: 'changePrimaryEmail',
    value: function changePrimaryEmail(evt) {
      evt.preventDefault();

      var result = {
        email: this.props.user.setting.new_primary_email
      };

      _SettingActionCreators2.default.changePrimaryEmail(this.props.user.token, result);
    }
  }, {
    key: 'deleteAccount',
    value: function deleteAccount(evt) {
      if (confirm('confirm to delete your account')) _SettingActionCreators2.default.deleteAccount(this.props.user.token);
    }
  }, {
    key: 'render',
    value: function render() {
      var settingContent = _configLang2.default[this.props.language].setting;
      return _react2.default.createElement(
        'div',
        { className: 'container', style: { marginTop: "50px" } },
        _react2.default.createElement(
          'h1',
          { style: { marginBottom: "20px" } },
          settingContent.title
        ),
        _react2.default.createElement(
          'div',
          { className: 'panel panel-default' },
          _react2.default.createElement(
            'div',
            { className: 'panel-heading' },
            _react2.default.createElement(
              'h3',
              { className: 'panel-title' },
              settingContent.delete[0]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'panel-body' },
            _react2.default.createElement(
              'p',
              null,
              settingContent.delete[1]
            ),
            _react2.default.createElement(
              'button',
              { onClick: this.deleteAccount.bind(this), className: 'btn btn-danger' },
              settingContent.delete[2]
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'panel panel-default' },
          _react2.default.createElement(
            'div',
            { className: 'panel-heading' },
            _react2.default.createElement(
              'h3',
              { className: 'panel-title' },
              settingContent.password[0]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'panel-body' },
            _react2.default.createElement(
              'form',
              { onSubmit: this.changePassword.bind(this) },
              _react2.default.createElement(
                'p',
                null,
                settingContent.password[1],
                _react2.default.createElement('input', { type: 'password', ref: 'old_password', className: 'form-control form-control--mini', placeholder: settingContent.password[2], onChange: this.changeDraft.bind(this, 'old_password') }),
                ' ',
                _react2.default.createElement('br', null),
                settingContent.password[3],
                _react2.default.createElement('input', { type: 'password', ref: 'new_password', className: 'form-control form-control--mini', placeholder: settingContent.password[4], onChange: this.changeDraft.bind(this, 'new_password'), style: { marginTop: "10px" } }),
                _react2.default.createElement('input', { type: 'password', ref: 'new_password_again', className: 'form-control form-control--mini', placeholder: settingContent.password[5], onChange: this.changeDraft.bind(this, 'new_password_again') }),
                ' ',
                _react2.default.createElement('br', null)
              ),
              _react2.default.createElement(
                'button',
                { className: 'btn btn-default' },
                settingContent.password[6]
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'panel panel-default' },
          _react2.default.createElement(
            'div',
            { className: 'panel-heading' },
            _react2.default.createElement(
              'h3',
              { className: 'panel-title' },
              settingContent.primary[0]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'panel-body' },
            _react2.default.createElement(
              'form',
              { onSubmit: this.changePrimaryEmail.bind(this) },
              _react2.default.createElement(
                'p',
                null,
                _react2.default.createElement(
                  'p',
                  null,
                  settingContent.password[1],
                  ': ',
                  _react2.default.createElement(
                    'b',
                    null,
                    this.props.user.email
                  ),
                  ' '
                ),
                settingContent.password[2],
                _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', placeholder: settingContent.password[3], onChange: this.changeDraft.bind(this, 'new_primary_email') })
              ),
              _react2.default.createElement(
                'button',
                { className: 'btn btn-info' },
                settingContent.password[4]
              )
            )
          )
        )
      );
    }
  }]);

  return Setting;
}(_react.Component);

exports.default = Setting;