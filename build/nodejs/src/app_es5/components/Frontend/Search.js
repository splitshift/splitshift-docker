'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ApplyJobModal = require('./ApplyJobModal');

var _ApplyJobModal2 = _interopRequireDefault(_ApplyJobModal);

var _SearchActionCreators = require('../../actions/SearchActionCreators');

var _SearchActionCreators2 = _interopRequireDefault(_SearchActionCreators);

var _JobActionCreators = require('../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var userComplete = false;

var Search = function (_Component) {
  _inherits(Search, _Component);

  function Search() {
    _classCallCheck(this, Search);

    return _possibleConstructorReturn(this, (Search.__proto__ || Object.getPrototypeOf(Search)).apply(this, arguments));
  }

  _createClass(Search, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var keyword = this.props.search.filter.all_keyword ? this.props.search.filter.all_keyword : "";
      _SearchActionCreators2.default.All(keyword);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      _JQueryTool2.default.triggerOwlCarousel();

      if (!userComplete && this.props.user.complete) {
        var response = {};
        response['email'] = this.props.user.email;
        _JobActionCreators2.default.updateApplyJobDraft(response);
        userComplete = true;
      }
    }
  }, {
    key: 'updateFilter',
    value: function updateFilter(name, evt) {
      var response = {};
      response[name] = evt.target.value;
      _SearchActionCreators2.default.updateFilter(response);
    }
  }, {
    key: 'chooseJobSearch',
    value: function chooseJobSearch(index, evt) {
      _SearchActionCreators2.default.chooseJobSearch(index);
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {
      evt.preventDefault();
      var keyword = this.props.search.filter.all_keyword ? this.props.search.filter.all_keyword : "";
      _SearchActionCreators2.default.All(keyword);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'container' },
        _react2.default.createElement(
          'div',
          { className: 'headsearch' },
          _react2.default.createElement(
            'form',
            { onSubmit: this.onSearch.bind(this) },
            _react2.default.createElement(
              'div',
              { className: 'search--bar' },
              _react2.default.createElement('input', { type: 'text', className: 'form-control', onChange: this.updateFilter.bind(this, 'all_keyword'), placeholder: 'search, jobs, people, companies ...', defaultValue: this.props.search.filter.all_keyword }),
              _react2.default.createElement(
                'button',
                { className: 'btn btn-default' },
                _react2.default.createElement('i', { className: 'fa fa-search' })
              )
            )
          )
        ),
        _react2.default.createElement('hr', { className: 'hr-title' }),
        this.props.search.people.length > 0 ? _react2.default.createElement(
          'div',
          { className: 'search__people' },
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement(
              'h2',
              null,
              _react2.default.createElement(
                'b',
                null,
                _react2.default.createElement(
                  'span5',
                  null,
                  'PEOPLE'
                )
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'owl-carousel' },
            this.props.search.people.map(function (p, index) {
              return _react2.default.createElement(
                'div',
                { key: index, className: 'item' },
                _react2.default.createElement(
                  'div',
                  { className: 'search__pic' },
                  _react2.default.createElement('img', { src: p.profile_image ? p.profile_image : "/images/avatar-image.jpg" })
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'search__detail' },
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      _react2.default.createElement(
                        'span',
                        null,
                        p.profile.first_name,
                        ' ',
                        p.profile.last_name
                      )
                    )
                  ),
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'span5',
                      null,
                      p.profile.occupation.position
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'search__btn' },
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/th/user/' + p.key },
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default' },
                      'See Profile'
                    )
                  )
                )
              );
            }),
            _react2.default.createElement(
              _reactRouter.Link,
              { to: '/people-search' },
              _react2.default.createElement(
                'div',
                { className: 'item lastitem' },
                _react2.default.createElement(
                  'p',
                  null,
                  'SEE MORE + '
                )
              )
            )
          )
        ) : "",
        this.props.search.companies.length > 0 ? _react2.default.createElement(
          'div',
          { className: 'search__company' },
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement(
              'h2',
              null,
              _react2.default.createElement(
                'b',
                null,
                _react2.default.createElement(
                  'span5',
                  null,
                  'COMPANIES'
                )
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'owl-carousel' },
            this.props.search.companies.map(function (company, index) {
              return _react2.default.createElement(
                'div',
                { key: index, className: 'item' },
                _react2.default.createElement(
                  'div',
                  { className: 'search__pic' },
                  _react2.default.createElement('img', { src: company.profile_image ? company.profile_image : "/images/avatar-image.jpg" })
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'search__detail' },
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      _react2.default.createElement(
                        'span',
                        null,
                        company.profile.company_name
                      )
                    )
                  ),
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'span5',
                      null,
                      company.profile.information
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'search__btn' },
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/th/company/' + company.key },
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default' },
                      'See Company'
                    )
                  )
                )
              );
            }),
            _react2.default.createElement(
              _reactRouter.Link,
              { to: '/company-search' },
              _react2.default.createElement(
                'div',
                { className: 'item lastitem' },
                _react2.default.createElement(
                  'p',
                  null,
                  'SEE MORE + '
                )
              )
            )
          )
        ) : "",
        this.props.search.jobs.length > 0 ? _react2.default.createElement(
          'div',
          { className: 'search__jobs' },
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement(
              'h2',
              null,
              _react2.default.createElement(
                'b',
                null,
                _react2.default.createElement(
                  'span5',
                  null,
                  'JOBS'
                )
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'owl-carousel' },
            this.props.search.jobs.map(function (job, index) {
              return _react2.default.createElement(
                'div',
                { key: index, className: 'item' },
                _react2.default.createElement(
                  'div',
                  { className: 'search__position' },
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      job.position
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'search__pic' },
                  _react2.default.createElement('img', { src: job.owner.profile_image ? job.owner.profile_image : "/images/avatar-image.jpg" })
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'search__detail' },
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      _react2.default.createElement(
                        'span',
                        null,
                        job.owner.company_name
                      )
                    )
                  ),
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'span5',
                      null,
                      _JQueryTool2.default.changeDateAgo(job.date)
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'search__btn' },
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default', onClick: _this2.chooseJobSearch.bind(_this2, index), 'data-toggle': 'modal', 'data-target': '#myModal__positions' },
                    'See Details'
                  )
                )
              );
            }),
            _react2.default.createElement(
              _reactRouter.Link,
              { to: '/job-search' },
              _react2.default.createElement(
                'div',
                { className: 'item lastitem' },
                _react2.default.createElement(
                  'p',
                  null,
                  'SEE MORE + '
                )
              )
            )
          )
        ) : "",
        _react2.default.createElement(_ApplyJobModal2.default, { job: this.props.job, search: this.props.search, user: this.props.user })
      );
    }
  }]);

  return Search;
}(_react.Component);

exports.default = Search;