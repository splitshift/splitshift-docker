'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var currentMap = void 0;

var MapSearch = function (_Component) {
  _inherits(MapSearch, _Component);

  function MapSearch() {
    _classCallCheck(this, MapSearch);

    return _possibleConstructorReturn(this, (MapSearch.__proto__ || Object.getPrototypeOf(MapSearch)).apply(this, arguments));
  }

  _createClass(MapSearch, [{
    key: 'initialMap',
    value: function initialMap(myLatLng) {
      currentMap = new google.maps.Map(document.getElementById('map1'), {
        center: myLatLng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {

      currentMap = new google.maps.Map(document.getElementById('map1'), {
        center: this.props.choose_location,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {

      if (this.props.new_searchmap) {

        this.initialMap(this.props.choose_location);

        if (this.props.companies.length > 0) {
          this.props.companies.map(function (company, index) {

            if (company.address.location[0] === 1 || company.address.location[1] === 1) return "";

            var new_marker = new google.maps.Marker({
              position: { lng: company.address.location[0], lat: company.address.location[1] },
              map: currentMap
            });

            var mainwindow = new google.maps.InfoWindow({ content: "<b><a href='/th/company/" + company.key + "' >" + company.profile.openjobs + " job" + (company.profile.openjobs > 1 ? 's' : '') + "</a></b>" });
            mainwindow.open(currentMap, new_marker);
          });
        } else if (this.props.jobs.length > 0) {
          this.props.jobs.map(function (job, index) {

            if (job.owner.address.location[0] === 1 || job.owner.address.location[1] === 1) return "";

            var new_marker = new google.maps.Marker({
              position: { lng: job.owner.address.location[0], lat: job.owner.address.location[1] },
              map: currentMap
            });

            var mainwindow = new google.maps.InfoWindow({ content: "<b><a href='/th/company/" + job.owner.key + "' >" + job.position + "</a></b>" });
            mainwindow.open(currentMap, new_marker);
          });
        }

        currentMap.setCenter(this.props.choose_location);
        this.props.triggerSearchMap();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'jobs__map' },
        _react2.default.createElement('div', { id: 'map1' })
      );
    }
  }]);

  return MapSearch;
}(_react.Component);

exports.default = MapSearch;