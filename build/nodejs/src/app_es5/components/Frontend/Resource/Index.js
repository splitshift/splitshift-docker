'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ConfigData = require('../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

var _Card = require('./Card');

var _Card2 = _interopRequireDefault(_Card);

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResourceIndex = function (_Component) {
  _inherits(ResourceIndex, _Component);

  function ResourceIndex() {
    _classCallCheck(this, ResourceIndex);

    return _possibleConstructorReturn(this, (ResourceIndex.__proto__ || Object.getPrototypeOf(ResourceIndex)).apply(this, arguments));
  }

  _createClass(ResourceIndex, [{
    key: 'componentDidMount',
    value: function componentDidMount() {

      $('.next').click(function () {
        $('.container.btntab').animate({ scrollLeft: '+=250' });
      });

      $('.prev').click(function () {
        $('.container.btntab').animate({ scrollLeft: '-=250' });
      });

      _ResourceActionCreators2.default.getAllResource();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _ResourceActionCreators2.default.resetResource();
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {
      evt.preventDefault();

      var response = { keyword: this.refs.keyword.value };
      _ResourceActionCreators2.default.changeFilter(response);

      _reactRouter.browserHistory.push('/th/resource/search');
    }
  }, {
    key: 'onFilterResource',
    value: function onFilterResource(department, evt) {
      this.onClickFilter('department', department);
      _ResourceActionCreators2.default.onFilterResource(department);
    }
  }, {
    key: 'onChangeFilter',
    value: function onChangeFilter(name, evt) {
      var response = {};
      response[name] = evt.target.value;

      _ResourceActionCreators2.default.changeFilter(response);
    }
  }, {
    key: 'onClickFilter',
    value: function onClickFilter(name, value, evt) {
      var response = {};
      response[name] = value;

      _ResourceActionCreators2.default.changeFilter(response);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var resourceObj = this.props.resource.department_filter ? this.props.resource.filtered_resource : this.props.resource.resources;
      var resourceContent = _configLang2.default[this.props.language].resource;
      var resourceCategory = _configLang2.default[this.props.language].backend.resource.category;
      var resourceDepartment = _configLang2.default[this.props.language].backend.resource.department;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: 'resource__cover', style: { background: "url('/images/resource/Resources Banner.jpg') no-repeat center center", backgroundSize: "cover" } },
          _react2.default.createElement(
            'div',
            { className: 'cover__text cover__text--resource' },
            _react2.default.createElement(
              'div',
              { className: 'cover__text--center' },
              _react2.default.createElement(
                'div',
                { className: 'resource__text' },
                _react2.default.createElement(
                  'h1',
                  null,
                  _react2.default.createElement(
                    'span2',
                    null,
                    resourceContent.title
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'resource__search' },
                _react2.default.createElement(
                  'div',
                  { className: 'resource__btn--search' },
                  _react2.default.createElement(
                    'form',
                    { onSubmit: this.onSearch.bind(this) },
                    _react2.default.createElement(
                      'div',
                      { className: 'input-group' },
                      _react2.default.createElement('input', { ref: 'keyword', type: 'text', className: 'form-control', onChange: this.onChangeFilter.bind(this, 'keyword'), placeholder: resourceContent.search_placeholder }),
                      _react2.default.createElement(
                        'span',
                        { className: 'input-group-btn' },
                        _react2.default.createElement(
                          'a',
                          { href: '#', className: 'btn btn-default', onClick: this.onSearch.bind(this) },
                          _react2.default.createElement('i', { className: 'icon-search' })
                        )
                      )
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'resource__text' },
                _react2.default.createElement(
                  'h3',
                  { style: { color: "white" } },
                  resourceContent.subtitle
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'cover__btn' },
                this.props.user.token ? _react2.default.createElement(
                  _reactRouter.Link,
                  { to: '/' + this.props.language + (this.props.user.type === 'Admin' ? "/admin/resource/index" : "/dashboard/resource/index") },
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default', type: 'button', style: { height: "40px", padding: "5px 30px" } },
                    _react2.default.createElement(
                      'span1',
                      null,
                      resourceContent.add
                    )
                  )
                ) : _react2.default.createElement(
                  'button',
                  { className: 'btn btn-default', 'data-toggle': 'modal', 'data-target': '#myModal__login', type: 'button', style: { height: "40px", padding: "5px 30px" } },
                  _react2.default.createElement(
                    'span1',
                    null,
                    resourceContent.add
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'resource__menutab' },
          _react2.default.createElement(
            'div',
            { className: 'btncontrol prev' },
            _react2.default.createElement(
              'button',
              { className: 'btn btn-default' },
              _react2.default.createElement('i', { className: 'fa fa-chevron-left' })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'container btntab' },
            _react2.default.createElement(
              'div',
              { className: 'recource__btntab' },
              _react2.default.createElement(
                'ul',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    'button',
                    { onClick: this.onFilterResource.bind(this, ""), className: 'btn btn-default', style: { borderBottom: this.props.resource.department_filter === "" ? "5px #f60 solid" : "" } },
                    resourceContent.all
                  )
                ),
                _ConfigData2.default.ResourceDepartment().map(function (department, index) {
                  return _react2.default.createElement(
                    'li',
                    { key: index },
                    _react2.default.createElement(
                      'button',
                      { onClick: _this2.onFilterResource.bind(_this2, department.toLowerCase()), className: 'btn btn-default', style: { borderBottom: department.toLowerCase() === _this2.props.resource.department_filter ? "5px #f60 solid" : "" } },
                      resourceDepartment[index]
                    )
                  );
                })
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'btncontrol next' },
            _react2.default.createElement(
              'button',
              { className: 'btn btn-default' },
              _react2.default.createElement('i', { className: 'fa fa-chevron-right' })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container' },
          _react2.default.createElement(
            'div',
            { className: 'resource__type' },
            _react2.default.createElement(
              'div',
              { className: 'text--center' },
              _react2.default.createElement(
                'h2',
                null,
                _react2.default.createElement(
                  'span5',
                  null,
                  _react2.default.createElement(
                    'span',
                    null,
                    resourceContent.header1[0]
                  ),
                  ' ',
                  resourceContent.header1[1]
                )
              ),
              _react2.default.createElement('hr', { className: 'hr-title' })
            ),
            _react2.default.createElement(
              'div',
              { className: 'type__banner' },
              _react2.default.createElement(
                'div',
                { className: 'row' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4 col-sm-6' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--resource' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/th/resource/search', onClick: this.onClickFilter.bind(this, 'category', 'videos') },
                      _react2.default.createElement(
                        'div',
                        { className: 'resource--photo' },
                        _react2.default.createElement('img', { className: 'resourcetype__pic', src: '/images/resource/Videos.jpg' }),
                        _react2.default.createElement(
                          'div',
                          { className: 'resourcetype__detail' },
                          _react2.default.createElement(
                            'span1',
                            null,
                            resourceCategory[0]
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4 col-sm-6' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--resource' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/th/resource/search', onClick: this.onClickFilter.bind(this, 'category', 'seminars/courses') },
                      _react2.default.createElement(
                        'div',
                        { className: 'resource--photo' },
                        _react2.default.createElement('img', { className: 'resourcetype__pic', src: '/images/resource/Seminars Courses.jpg' }),
                        _react2.default.createElement(
                          'div',
                          { className: 'resourcetype__detail' },
                          _react2.default.createElement(
                            'span1',
                            null,
                            resourceCategory[1]
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4 col-sm-6' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--resource' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/th/resource/search', onClick: this.onClickFilter.bind(this, 'category', 'training manuals') },
                      _react2.default.createElement(
                        'div',
                        { className: 'resource--photo' },
                        _react2.default.createElement('img', { className: 'resourcetype__pic', src: '/images/resource/Training Manuals.jpg' }),
                        _react2.default.createElement(
                          'div',
                          { className: 'resourcetype__detail' },
                          _react2.default.createElement(
                            'span1',
                            null,
                            resourceCategory[2]
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4 col-sm-6' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--resource' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/th/resource/search', onClick: this.onClickFilter.bind(this, 'category', 'templates/checklists') },
                      _react2.default.createElement(
                        'div',
                        { className: 'resource--photo' },
                        _react2.default.createElement('img', { className: 'resourcetype__pic', src: '/images/resource/Templates Checklists.jpg' }),
                        _react2.default.createElement(
                          'div',
                          { className: 'resourcetype__detail' },
                          _react2.default.createElement(
                            'span1',
                            null,
                            resourceCategory[3]
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4 col-sm-6' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--resource' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/th/resource/search', onClick: this.onClickFilter.bind(this, 'category', 'articles/links') },
                      _react2.default.createElement(
                        'div',
                        { className: 'resource--photo' },
                        _react2.default.createElement('img', { className: 'resourcetype__pic', src: '/images/resource/Articles Links.jpg' }),
                        _react2.default.createElement(
                          'div',
                          { className: 'resourcetype__detail' },
                          _react2.default.createElement(
                            'span1',
                            null,
                            resourceCategory[4]
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4 col-sm-6' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--resource' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/th/resource/search', onClick: this.onClickFilter.bind(this, 'category', 'virtual mentorship') },
                      _react2.default.createElement(
                        'div',
                        { className: 'resource--photo' },
                        _react2.default.createElement('img', { className: 'resourcetype__pic', src: '/images/resource/Virtual Mentorship.jpg' }),
                        _react2.default.createElement(
                          'div',
                          { className: 'resourcetype__detail' },
                          _react2.default.createElement(
                            'span1',
                            null,
                            resourceCategory[5]
                          )
                        )
                      )
                    )
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'cover__btn', style: { textAlign: "center" } },
              this.props.user.token ? _react2.default.createElement(
                _reactRouter.Link,
                { to: this.props.user.type === 'Admin' ? "/admin/resource/index" : "/dashboard/resource/index" },
                _react2.default.createElement(
                  'button',
                  { className: 'btn btn-default', type: 'button', style: { height: "40px", padding: "5px 30px" } },
                  _react2.default.createElement(
                    'span1',
                    null,
                    resourceContent.add
                  )
                )
              ) : _react2.default.createElement(
                'button',
                { className: 'btn btn-default', 'data-toggle': 'modal', 'data-target': '#myModal__login', type: 'button', style: { height: "40px", padding: "5px 30px" } },
                _react2.default.createElement(
                  'span1',
                  null,
                  resourceContent.add
                )
              )
            )
          ),
          typeof resourceObj !== 'undefined' ? _ConfigData2.default.ResourceCategoryWithLastest().map(function (resource_index, index) {
            var resource_lower = resource_index.toLowerCase();
            if (typeof resourceObj[resource_lower] === 'undefined' || resourceObj[resource_lower].length == 0) return "";

            return _react2.default.createElement(
              'div',
              { className: 'resource__recent', key: index },
              _react2.default.createElement(
                'div',
                { className: 'text--center' },
                _react2.default.createElement(
                  'h2',
                  null,
                  _react2.default.createElement(
                    'span5',
                    null,
                    resource_lower === 'lastest' ? resourceContent.header2[0] : resourceCategory[_ConfigData2.default.ResourceCategory().indexOf(resource_index)],
                    ' ',
                    resource_lower === 'lastest' ? _react2.default.createElement(
                      'span',
                      null,
                      resourceContent.header2[1]
                    ) : ""
                  )
                ),
                _react2.default.createElement('hr', { className: 'hr-title' })
              ),
              resourceObj[resource_lower].map(function (resource, index) {
                return _react2.default.createElement(_Card2.default, { key: index, resource: resource });
              }),
              resourceObj[resource_lower].length > 4 ? _react2.default.createElement(
                'div',
                { className: 'btn__seemore' },
                _react2.default.createElement(
                  'button',
                  { className: 'btn btn-default' },
                  _this2.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
                )
              ) : ""
            );
          }) : ""
        )
      );
    }
  }]);

  return ResourceIndex;
}(_react.Component);

exports.default = ResourceIndex;

/*
style={{ borderBottom: "5px #f60 solid" }}
<h2><span5>LASTEST <span>RESOURCES</span></span5></h2>
*/