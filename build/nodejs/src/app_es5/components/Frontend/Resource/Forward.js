'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResourceForward = function (_Component) {
  _inherits(ResourceForward, _Component);

  function ResourceForward() {
    _classCallCheck(this, ResourceForward);

    return _possibleConstructorReturn(this, (ResourceForward.__proto__ || Object.getPrototypeOf(ResourceForward)).apply(this, arguments));
  }

  _createClass(ResourceForward, [{
    key: 'sendForward',
    value: function sendForward(evt) {

      evt.preventDefault();

      var email_sender = this.refs.forward_email.value;
      var resource_link = "http://www.splitshift.com/th/resource/article/" + this.props.resource._id;
      var name = this.props.user.rank === 'Employee' ? this.props.user.first_name + " " + this.props.user.last_name : this.props.user.company_name;

      var forwardData = "<p>" + name + " share article from www.splitshift.com</p>" + "<p><b>" + this.props.resource.title + "</b></p>" + "<p>" + this.props.resource.short_description + "</p>" + "<p>See Full Article " + resource_link + "</p>";

      var result = {
        to: email_sender,
        data: forwardData
      };

      // Sending Email
      _ResourceActionCreators2.default.forwardEmail(this.props.user.token, result);
    }
  }, {
    key: 'render',
    value: function render() {

      var draftEmail = "Share: " + this.props.resource.title + "\n" + this.props.resource.short_description + "\nLink: http://www.splitshift.com/th/resource/article/" + this.props.resource._id;

      return _react2.default.createElement(
        'form',
        { onSubmit: this.sendForward.bind(this) },
        _react2.default.createElement(
          'label',
          { htmlFor: 'letter' },
          'Forward this resource to'
        ),
        _react2.default.createElement('input', { type: 'text', ref: 'forward_email', className: 'form-control form-control--mini', placeholder: 'Email Address...' }),
        _react2.default.createElement(
          'label',
          { htmlFor: 'letter' },
          'Message'
        ),
        _react2.default.createElement('textarea', { rows: '10', placeholder: 'Dear..', id: 'letter', className: 'form-control form-control--mini', defaultValue: draftEmail, readOnly: true }),
        this.props.sending_forward_email && _react2.default.createElement(
          'span',
          null,
          'Sending...'
        ),
        _react2.default.createElement(
          'button',
          { className: 'btn btn-default' },
          'Send'
        )
      );
    }
  }]);

  return ResourceForward;
}(_react.Component);

exports.default = ResourceForward;