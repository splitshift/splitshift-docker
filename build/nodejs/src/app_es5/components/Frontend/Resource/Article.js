'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _Forward = require('./Forward');

var _Forward2 = _interopRequireDefault(_Forward);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var API_URL = function API_URL() {
  return window.location.origin;
};

var ResourceArticle = function (_Component) {
  _inherits(ResourceArticle, _Component);

  function ResourceArticle(argument) {
    _classCallCheck(this, ResourceArticle);

    var _this = _possibleConstructorReturn(this, (ResourceArticle.__proto__ || Object.getPrototypeOf(ResourceArticle)).call(this, argument));

    _this.state = {
      forward: false
    };
    return _this;
  }

  _createClass(ResourceArticle, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.props.resource.load) {
        this.refs.comment.value = "";
        _ResourceActionCreators2.default.getResource(this.props.resource_id, this.props.user.token);
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _ResourceActionCreators2.default.resetResource();
    }
  }, {
    key: 'likeResource',
    value: function likeResource(evt) {
      _ResourceActionCreators2.default.likeResource(this.props.user.token, this.props.resource.resource);
    }
  }, {
    key: 'commentResource',
    value: function commentResource(evt) {

      evt.preventDefault();

      var comment = {
        comment: this.refs.comment.value
      };

      _ResourceActionCreators2.default.commentResource(this.props.user.token, comment, this.props.resource.resource._id);
    }
  }, {
    key: 'deleteCommentResource',
    value: function deleteCommentResource(comment, evt) {
      _ResourceActionCreators2.default.deleteCommentResource(this.props.user.token, comment, this.props.resource.resource._id);
    }
  }, {
    key: 'ForwardResource',
    value: function ForwardResource(evt) {
      this.setState({ forward: true });
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return JSON.stringify(nextProps.resource) !== JSON.stringify(this.props.resource) || JSON.stringify(nextProps.user) !== JSON.stringify(this.props.user) || this.state.forward !== nextState.forward;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var share_url = API_URL() + '/th/resource/article/' + this.props.resource.resource._id;

      return _react2.default.createElement(
        'div',
        { className: 'container resource3' },
        _react2.default.createElement(
          'div',
          { className: 'resource__title' },
          _react2.default.createElement(
            'h2',
            null,
            _react2.default.createElement(
              'b',
              null,
              _react2.default.createElement(
                'span',
                null,
                this.props.resource.resource.title
              )
            )
          ),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'p',
              { style: { width: "100%" } },
              _react2.default.createElement(
                'span5',
                { style: { marginTop: "10px" } },
                'by',
                ' ',
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: this.props.resource.resource.owner.user_type === 'Employee' ? "/th/user/" + this.props.resource.resource.owner.key : this.props.resource.resource.owner.user_type !== 'Admin' ? "/th/company/" + this.props.resource.resource.owner.key : "/" },
                  this.props.resource.resource.owner.name
                )
              ),
              _react2.default.createElement(
                'span5',
                { style: { float: 'right' } },
                _react2.default.createElement('i', { className: 'fa fa-clock-o' }),
                ' ',
                _JQueryTool2.default.changeDateAgo(this.props.resource.resource.date)
              )
            )
          )
        ),
        this.props.resource.resource.featured_image ? _react2.default.createElement('div', { className: 'resource__photo', style: { background: "url(" + this.props.resource.resource.featured_image.substring(6) + ") no-repeat center center", backgroundSize: "cover" } }) : _react2.default.createElement(
          'div',
          { className: 'resource__video' },
          _react2.default.createElement('iframe', { src: this.props.resource.resource.featured_video })
        ),
        _react2.default.createElement(
          'div',
          { className: 'resource__content' },
          _react2.default.createElement(
            'div',
            { className: 'ressource__paragraph', style: { paddingBottom: "0px" } },
            _react2.default.createElement(
              'div',
              { style: { backgroundColor: "#eeeeee", padding: "10px", marginBottom: "10px" } },
              _react2.default.createElement(
                'p',
                null,
                this.props.resource.resource.short_description
              )
            ),
            _react2.default.createElement('div', { dangerouslySetInnerHTML: { __html: this.props.resource.resource.content } }),
            _react2.default.createElement(
              'div',
              { style: { textAlign: "right" } },
              _react2.default.createElement(
                'a',
                { className: 'share-btn', href: '#', 'data-href': "https://www.facebook.com/sharer/sharer.php?app_id=235025906884083&sdk=joey&u=" + share_url + "&display=popup&ref=plugin&src=share_button", onClick: function onClick(evt) {
                    return !window.open($(evt.currentTarget).data('href'), 'Facebook', 'width=640,height=580');
                  } },
                _react2.default.createElement(
                  'button',
                  { className: 'modal__btn--light' },
                  _react2.default.createElement('i', { className: 'icon-facebook-squared' }),
                  ' Share'
                ),
                ' '
              ),
              _react2.default.createElement(
                'a',
                { className: 'share-btn', href: '#', 'data-href': "http://www.linkedin.com/shareArticle?mini=true&url=" + share_url, onClick: function onClick(evt) {
                    return !window.open($(evt.currentTarget).data('href'), 'Linkedin', 'width=640,height=580');
                  } },
                _react2.default.createElement(
                  'button',
                  { className: 'modal__btn--light' },
                  _react2.default.createElement('i', { className: 'icon-linkedin-squared' }),
                  ' Share'
                ),
                ' '
              ),
              !this.state.forward && _react2.default.createElement(
                'button',
                { className: 'modal__btn--light', onClick: this.ForwardResource.bind(this) },
                'Forward'
              )
            ),
            this.state.forward && _react2.default.createElement(_Forward2.default, { user: this.props.user, resource: this.props.resource.resource, sending_forward_email: this.props.resource.sending_forward_email })
          ),
          _react2.default.createElement(
            'article',
            { className: 'feed', style: { marginTop: "0px", paddingTop: "0px" } },
            _react2.default.createElement(
              'div',
              { className: 'feed__actions', style: { textAlign: "left" } },
              _react2.default.createElement(
                'button',
                { className: 'feed__button', onClick: this.likeResource.bind(this), style: { color: this.props.resource.resource.like.indexOf(this.props.user.key) !== -1 ? "#f60" : "" } },
                _react2.default.createElement('i', { className: 'fa fa-thumbs-up', style: { color: this.props.resource.resource.like.indexOf(this.props.user.key) !== -1 ? "#f60" : "" } }),
                'Like'
              ),
              _react2.default.createElement(
                'div',
                { className: 'feed__counter' },
                this.props.resource.resource.comments.length,
                ' Comment',
                this.props.resource.resource.comments.length > 1 ? "s" : "",
                ' \u2022 ',
                this.props.resource.resource.like.length,
                ' Like',
                this.props.resource.resource.like.length > 1 ? "s" : ""
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'feed__tray' },
              _react2.default.createElement(
                'form',
                { onSubmit: this.commentResource.bind(this) },
                _react2.default.createElement(
                  'div',
                  { className: 'feed__comment' },
                  _react2.default.createElement(
                    'div',
                    { className: 'comment__pic' },
                    _react2.default.createElement('img', { src: this.props.user.profile_image ? this.props.user.profile_image : '/images/avatar-image.jpg' })
                  ),
                  _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement('input', { type: 'text', ref: 'comment', placeholder: 'Write a comment' })
                  ),
                  _react2.default.createElement(
                    'button',
                    { className: 'feed__send' },
                    _react2.default.createElement('i', { className: 'fa fa-paper-plane', style: { right: "-5px" } })
                  )
                )
              ),
              this.props.resource.resource.comments.map(function (comment, index) {
                return _react2.default.createElement(
                  'div',
                  { className: 'feed__comment', key: index },
                  _react2.default.createElement(
                    'div',
                    { className: 'comment__pic' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: comment.owner.user_type == 'Employee' ? "/th/user/" + comment.owner.key : "/th/company/" + comment.owner.key },
                      _react2.default.createElement('img', { src: comment.owner.profile_image ? comment.owner.profile_image : "/images/avatar-image.jpg" })
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { style: { textAlign: "left" } },
                    _react2.default.createElement(
                      'div',
                      null,
                      comment.owner.name,
                      ' ',
                      comment.owner.key == _this2.props.user.key ? _react2.default.createElement(
                        'a',
                        { onClick: _this2.deleteCommentResource.bind(_this2, comment), href: 'javascript:void(0)' },
                        'Delete'
                      ) : ""
                    ),
                    _react2.default.createElement(
                      'div',
                      null,
                      comment.comment
                    )
                  )
                );
              })
            )
          )
        )
      );
    }
  }]);

  return ResourceArticle;
}(_react.Component);

exports.default = ResourceArticle;