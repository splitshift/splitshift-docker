'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _ConfigData = require('../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResourceSearch = function (_Component) {
  _inherits(ResourceSearch, _Component);

  function ResourceSearch() {
    _classCallCheck(this, ResourceSearch);

    return _possibleConstructorReturn(this, (ResourceSearch.__proto__ || Object.getPrototypeOf(ResourceSearch)).apply(this, arguments));
  }

  _createClass(ResourceSearch, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $(".navlist__topic").click(function () {
        $(this).parent(".navlist__filter").children(".navlist__dropdown").toggleClass("open");

        var iconToggle = $(this).children("i");

        if (iconToggle.hasClass("icon-down-dir-1")) iconToggle.attr("class", "icon-right-dir-1");else iconToggle.attr("class", "icon-down-dir-1");
      });

      _ResourceActionCreators2.default.searchResource(this.props.resource.filter);
    }
  }, {
    key: 'onChangeFilter',
    value: function onChangeFilter(name, evt) {
      var response = {};
      response[name] = evt.target.value;

      _ResourceActionCreators2.default.changeFilter(response);
    }
  }, {
    key: 'onClickFilter',
    value: function onClickFilter(name, value, evt) {
      var response = {};
      response[name] = value;

      _ResourceActionCreators2.default.changeFilter(response);
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {
      evt.preventDefault();
      _ResourceActionCreators2.default.searchResource(this.props.resource.filter);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (JSON.stringify(nextProps.resource.filter) !== JSON.stringify(this.props.resource.filter)) _ResourceActionCreators2.default.searchResource(nextProps.resource.filter);
      return true;
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _ResourceActionCreators2.default.resetResource();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var resourceContent = _configLang2.default[this.props.language].resource;
      var resourceCategory = _configLang2.default[this.props.language].backend.resource.category;
      var resourceDepartment = _configLang2.default[this.props.language].backend.resource.department;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: 'headsearch__resource' },
          _react2.default.createElement(
            'div',
            { className: 'search--bar' },
            _react2.default.createElement(
              'form',
              { onSubmit: this.onSearch.bind(this) },
              _react2.default.createElement('input', { type: 'text', className: 'form-control', placeholder: 'keyword ...', onChange: this.onChangeFilter.bind(this, 'keyword'), defaultValue: this.props.resource.filter.keyword }),
              _react2.default.createElement(
                'button',
                { className: 'btn btn-default' },
                _react2.default.createElement('i', { className: 'fa fa-search' })
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'search__resource' },
          _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
              'div',
              { className: 'col-lg-offset-2 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 col-xs-12' },
              _react2.default.createElement(
                'div',
                { className: 'col-md-12 col-sm-6 col-xs-6' },
                _react2.default.createElement(
                  'div',
                  { className: 'filter__type' },
                  _react2.default.createElement(
                    'ul',
                    { className: 'nav navbar-nav' },
                    _react2.default.createElement(
                      'li',
                      { className: 'navlist__filter', role: 'presentation' },
                      _react2.default.createElement(
                        'span1',
                        { className: 'navlist__topic' },
                        _react2.default.createElement(
                          'b',
                          null,
                          resourceContent.department
                        ),
                        ' ',
                        _react2.default.createElement('i', { className: 'icon-down-dir-1' })
                      ),
                      _react2.default.createElement(
                        'ul',
                        { className: 'navlist__dropdown open' },
                        _react2.default.createElement(
                          'li',
                          { className: 'navlist__filter', role: 'presentation' },
                          _react2.default.createElement(
                            'a',
                            { href: 'javascript:void(0)', onClick: this.onClickFilter.bind(this, 'department', '') },
                            _react2.default.createElement(
                              'span1',
                              { className: typeof this.props.resource.filter.department === 'undefined' || this.props.resource.filter.department === '' ? "active" : "" },
                              resourceContent.all
                            )
                          )
                        ),
                        _ConfigData2.default.ResourceDepartment().map(function (department, index) {
                          return _react2.default.createElement(
                            'li',
                            { className: 'navlist__filter', role: 'presentation', key: index },
                            _react2.default.createElement(
                              'a',
                              { href: 'javascript:void(0)', onClick: _this2.onClickFilter.bind(_this2, 'department', department.toLowerCase()) },
                              _react2.default.createElement(
                                'span1',
                                { className: _this2.props.resource.filter.department && _this2.props.resource.filter.department == department.toLowerCase() ? "active" : "" },
                                resourceDepartment[index]
                              )
                            )
                          );
                        })
                      )
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'col-md-12 col-sm-6 col-xs-6' },
                _react2.default.createElement(
                  'div',
                  { className: 'filter__type' },
                  _react2.default.createElement(
                    'ul',
                    { className: 'nav navbar-nav' },
                    _react2.default.createElement(
                      'li',
                      { className: 'navlist__filter', role: 'presentation' },
                      _react2.default.createElement(
                        'span1',
                        { className: 'navlist__topic' },
                        _react2.default.createElement(
                          'b',
                          null,
                          resourceContent.category
                        ),
                        ' ',
                        _react2.default.createElement('i', { className: 'icon-down-dir-1' })
                      ),
                      _react2.default.createElement(
                        'ul',
                        { className: 'navlist__dropdown open' },
                        _react2.default.createElement(
                          'li',
                          { className: 'navlist__filter', role: 'presentation' },
                          _react2.default.createElement(
                            'a',
                            { href: 'javascript:void(0)', onClick: this.onClickFilter.bind(this, 'category', '') },
                            _react2.default.createElement(
                              'span1',
                              { className: typeof this.props.resource.filter.category === 'undefined' || this.props.resource.filter.category === '' ? "active" : "" },
                              resourceContent.all
                            )
                          )
                        ),
                        _ConfigData2.default.ResourceCategory().map(function (category, index) {
                          return _react2.default.createElement(
                            'li',
                            { className: 'navlist__filter', role: 'presentation', key: index },
                            _react2.default.createElement(
                              'a',
                              { href: 'javascript:void(0)', onClick: _this2.onClickFilter.bind(_this2, 'category', category.toLowerCase()) },
                              _react2.default.createElement(
                                'span1',
                                { className: _this2.props.resource.filter.category && _this2.props.resource.filter.category == category.toLowerCase() ? "active" : "" },
                                resourceCategory[index]
                              )
                            )
                          );
                        })
                      )
                    )
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'col-lg-6 col-md-8 col-sm-12 col-xs-12', style: { paddingBottom: "16px" } },
              this.props.resource.resource_array.map(function (resource, index) {
                return _react2.default.createElement(
                  _reactRouter.Link,
                  { className: 'resource-card', to: "/th/resource/article/" + resource._id, key: index, style: { color: "#4d4948" } },
                  _react2.default.createElement('img', { className: 'resource-card__image', src: resource.featured_image ? resource.featured_image.substring(6) : "http://img.youtube.com/vi/" + resource.featured_video.substring(24) + "/0.jpg" }),
                  _react2.default.createElement(
                    'div',
                    { className: 'resource-card__detail' },
                    _react2.default.createElement(
                      'div',
                      { className: 'resource-card__date' },
                      _JQueryTool2.default.changeDateAgo(resource.date)
                    ),
                    _react2.default.createElement(
                      'div',
                      { className: 'resource-card__title' },
                      resource.title
                    ),
                    _react2.default.createElement(
                      'div',
                      { className: 'resource-card__owner' },
                      resource.owner.name
                    ),
                    _react2.default.createElement(
                      'p',
                      { className: 'resource-card__text' },
                      resource.short_description
                    )
                  )
                );
              })
            )
          )
        )
      );
    }
  }]);

  return ResourceSearch;
}(_react.Component);

exports.default = ResourceSearch;
/*
<div className="btn__seemore">
  <button className="btn btn-default">See More..</button>
</div>
*/