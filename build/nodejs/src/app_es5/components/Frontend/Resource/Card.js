'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResourceCard = function (_Component) {
  _inherits(ResourceCard, _Component);

  function ResourceCard() {
    _classCallCheck(this, ResourceCard);

    return _possibleConstructorReturn(this, (ResourceCard.__proto__ || Object.getPrototypeOf(ResourceCard)).apply(this, arguments));
  }

  _createClass(ResourceCard, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'resource__item' },
        _react2.default.createElement(
          _reactRouter.Link,
          { to: '/th/resource/article/' + this.props.resource._id, style: { color: "#565a5c" } },
          _react2.default.createElement(
            'div',
            { className: 'resource__pic' },
            _react2.default.createElement('img', { src: this.props.resource.featured_image ? this.props.resource.featured_image.substring(6) : "http://img.youtube.com/vi/" + (this.props.resource.featured_video ? this.props.resource.featured_video.substring(24) : "") + "/0.jpg" }),
            _react2.default.createElement('br', null)
          ),
          _react2.default.createElement(
            'div',
            { className: 'resource__detail' },
            _react2.default.createElement(
              'p',
              { style: { height: "40px", "overflow": "hidden", "textOverflow": "ellipsis" } },
              _react2.default.createElement(
                'b',
                null,
                _react2.default.createElement(
                  'span',
                  null,
                  this.props.resource.title
                )
              )
            ),
            _react2.default.createElement(
              'p',
              { style: { height: "60px", "overflow": "hidden", "textOverflow": "ellipsis", marginBottom: "35px" } },
              this.props.resource.short_description
            ),
            _react2.default.createElement(
              'p',
              { style: { overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis", textAlign: "left", display: "inline-block", width: "120px" } },
              _react2.default.createElement(
                'b',
                null,
                'By ',
                this.props.resource.owner.name
              )
            ),
            _react2.default.createElement(
              'p',
              { style: { float: "right" } },
              _react2.default.createElement(
                'u',
                null,
                _JQueryTool2.default.changeDateAgo(this.props.resource.date)
              )
            )
          )
        )
      );
    }
  }]);

  return ResourceCard;
}(_react.Component);

exports.default = ResourceCard;