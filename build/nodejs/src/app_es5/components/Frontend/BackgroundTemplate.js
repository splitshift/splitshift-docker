'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ConfigData = require('../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BackgroundTemplate = function (_Component) {
  _inherits(BackgroundTemplate, _Component);

  function BackgroundTemplate() {
    _classCallCheck(this, BackgroundTemplate);

    return _possibleConstructorReturn(this, (BackgroundTemplate.__proto__ || Object.getPrototypeOf(BackgroundTemplate)).apply(this, arguments));
  }

  _createClass(BackgroundTemplate, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__background', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog', role: 'document' },
          _react2.default.createElement(
            'div',
            { className: 'modal-content' },
            _react2.default.createElement(
              'div',
              { className: 'modal-header' },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                _react2.default.createElement(
                  'span5',
                  { 'aria-hidden': 'true' },
                  '\xD7'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal-title' },
                _react2.default.createElement(
                  'b',
                  null,
                  _react2.default.createElement(
                    'span',
                    null,
                    'Choose Your Background'
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-body' },
              _react2.default.createElement(
                'div',
                { className: 'row' },
                _ConfigData2.default.BackgroundTemplateList().map(function (template, index) {
                  return _react2.default.createElement(
                    'div',
                    { key: index, className: 'col-md-3 col-xs-6', style: { paddingRight: "0px", paddingLeft: "0px", cursor: "pointer" } },
                    _react2.default.createElement('div', { onClick: _this2.props.changeBannerImageWithTemplate.bind(null, template + ".jpg"), className: 'photo__pic', style: { background: "url(" + template + "-small.jpg) no-repeat center center", backgroundSize: "cover" } })
                  );
                })
              )
            )
          )
        )
      );
    }
  }]);

  return BackgroundTemplate;
}(_react.Component);

exports.default = BackgroundTemplate;