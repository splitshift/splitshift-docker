'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _MapSearch = require('./MapSearch');

var _MapSearch2 = _interopRequireDefault(_MapSearch);

var _ApplyJobModal = require('./ApplyJobModal');

var _ApplyJobModal2 = _interopRequireDefault(_ApplyJobModal);

var _SearchActionCreators = require('../../actions/SearchActionCreators');

var _SearchActionCreators2 = _interopRequireDefault(_SearchActionCreators);

var _JobActionCreators = require('../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

var _ConfigData = require('../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var new_searchmap = false;
var userComplete = false;
var choose_location = { lat: 13.71129, lng: 100.50908 };

var JobSearch = function (_Component) {
  _inherits(JobSearch, _Component);

  function JobSearch() {
    _classCallCheck(this, JobSearch);

    return _possibleConstructorReturn(this, (JobSearch.__proto__ || Object.getPrototypeOf(JobSearch)).apply(this, arguments));
  }

  _createClass(JobSearch, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _SearchActionCreators2.default.clearFilter();
      _SearchActionCreators2.default.Job({});

      if (navigator.geolocation) navigator.geolocation.getCurrentPosition(this.showPosition);else console.log("Geolocation is not supported by this browser.");
    }
  }, {
    key: 'loadMore',
    value: function loadMore(evt) {
      _SearchActionCreators2.default.Job(this.props.search.filter, this.props.search.jobs.length / 10);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (!userComplete && this.props.user.complete) {
        var response = {};
        response['email'] = this.props.user.email;
        _JobActionCreators2.default.updateApplyJobDraft(response);
        userComplete = true;
      }
    }
  }, {
    key: 'chooseJobSearch',
    value: function chooseJobSearch(index, evt) {
      _SearchActionCreators2.default.chooseJobSearch(index);
    }
  }, {
    key: 'updateFilter',
    value: function updateFilter(name, evt) {
      var response = {};
      response[name] = evt.target.value;
      _SearchActionCreators2.default.updateFilter(response);
    }
  }, {
    key: 'updateAdvanceFilter',
    value: function updateAdvanceFilter(name, value, evt) {

      var response = {};

      if (value == 'All' || value == 'All Job Types') value = "";

      response[name] = value;
      _SearchActionCreators2.default.updateFilter(response);
    }
  }, {
    key: 'triggerSearchMap',
    value: function triggerSearchMap() {
      new_searchmap = false;
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {

      evt.preventDefault();

      $('html, body').animate({
        scrollTop: $(this.refs.jobs__jobs).offset().top - 200
      }, 1000);

      _SearchActionCreators2.default.Job(this.props.search.filter);
    }
  }, {
    key: 'showPosition',
    value: function showPosition(position) {
      choose_location.lat = position.coords.latitude;
      choose_location.lng = position.coords.longitude;
      // SearchActionCreators.Job({ point: "[" + choose_location.lng + "," + choose_location.lat +"]" });
      var response = {};
      response.point = "[" + choose_location.lng + "," + choose_location.lat + "]";
      _SearchActionCreators2.default.updateFilter(response);
      _SearchActionCreators2.default.Job(response);
      new_searchmap = true;
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (!nextProps.search.searching && this.props.search.searching) new_searchmap = true;
      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var content = _configLang2.default[this.props.language].job_search;
      var advancedContent = _configLang2.default[this.props.language].advanced_search;
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: 'container' },
          _react2.default.createElement(
            'div',
            { className: 'header__search' },
            _react2.default.createElement(
              'div',
              { className: 'job__title' },
              _react2.default.createElement(
                'p',
                null,
                _react2.default.createElement(
                  'span',
                  null,
                  content.intro
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'job__search' },
              _react2.default.createElement(
                'form',
                { onSubmit: this.onSearch.bind(this) },
                _react2.default.createElement(
                  'div',
                  { className: 'job__bar' },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'job' },
                    content.job[0]
                  ),
                  _react2.default.createElement('input', { id: 'job', type: 'text', className: 'form-control', onChange: this.updateFilter.bind(this, 'keyword'), placeholder: content.job[1] })
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'location__bar' },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'location' },
                    content.location[0]
                  ),
                  _react2.default.createElement('input', { id: 'location', type: 'text', className: 'form-control', onChange: this.updateFilter.bind(this, 'location'), placeholder: content.location[1] }),
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default ad__search ad__search--desktop', type: 'button', onClick: _JQueryTool2.default.showAdvanceSearch.bind(this) },
                    advancedContent.title
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'job__btn' },
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default' },
                    _react2.default.createElement('i', { className: 'fa fa-search' })
                  ),
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default ad__search ad__search--responsive', type: 'button', onClick: _JQueryTool2.default.showAdvanceSearch.bind(this) },
                    advancedContent.title
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'jobbar__detail' },
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.distance ? advancedContent.distance[this.props.search.filter.distance] : advancedContent.distance_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              Object.keys(_ConfigData2.default.DistanceList()).map(function (distance_key, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index, onClick: _this2.updateAdvanceFilter.bind(_this2, 'distance', distance_key) },
                  _react2.default.createElement(
                    'a',
                    { href: '#' },
                    advancedContent.distance[distance_key]
                  )
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.bounded_time ? advancedContent.date_posted[this.props.search.filter.bounded_time] : advancedContent.date_posted_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              Object.keys(_ConfigData2.default.DatePostList()).map(function (datepost, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'bounded_time', datepost) },
                    advancedContent.date_posted[datepost]
                  )
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.type ? advancedContent.job_type[_ConfigData2.default.JobTypeList().indexOf(this.props.search.filter.type)] : advancedContent.job_type_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              _ConfigData2.default.JobTypeList().map(function (jobtype, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'type', jobtype) },
                    advancedContent.job_type[index]
                  )
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.compensation ? advancedContent.compensation[_ConfigData2.default.CompensationList().indexOf(this.props.search.filter.compensation)] : advancedContent.compensation_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              _ConfigData2.default.CompensationList().map(function (compensation, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'compensation', compensation) },
                    advancedContent.compensation[index]
                  )
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.employer ? advancedContent.company_type[_ConfigData2.default.CompanyTypeList().indexOf(this.props.search.filter.employer)] : advancedContent.company_type_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              _ConfigData2.default.CompanyTypeList().map(function (employer, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'employer', employer) },
                    advancedContent.company_type[index]
                  )
                );
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container' },
          _react2.default.createElement(_MapSearch2.default, { companies: [], jobs: this.props.search.jobs, new_searchmap: new_searchmap, triggerSearchMap: this.triggerSearchMap.bind(this), choose_location: choose_location }),
          _react2.default.createElement(
            'div',
            { ref: 'jobs__jobs', className: 'jobs__jobs' },
            this.props.search.jobs.map(function (job, index) {
              return _react2.default.createElement(
                'a',
                { key: index, href: 'javascript:void(0)', onClick: _this2.chooseJobSearch.bind(_this2, index), 'data-toggle': 'modal', 'data-target': '#myModal__positions' },
                _react2.default.createElement(
                  'div',
                  { className: 'job__item' },
                  _react2.default.createElement(
                    'div',
                    { className: 'job__pic' },
                    _react2.default.createElement('img', { src: job.owner.profile_image ? job.owner.profile_image : "/images/avatar-image.jpg" })
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'search__detail' },
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        _react2.default.createElement(
                          'span',
                          null,
                          job.position,
                          ' '
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        _react2.default.createElement(
                          'span5',
                          null,
                          job.owner.company_name,
                          ' '
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement(
                        'span5',
                        null,
                        job.owner.address.city ? job.owner.address.city : "-",
                        ' '
                      )
                    ),
                    _react2.default.createElement(
                      'div',
                      { className: 'search__btn' },
                      _react2.default.createElement(
                        'button',
                        { className: 'btn btn-default' },
                        _JQueryTool2.default.changeDateAgo(job.date)
                      )
                    )
                  )
                )
              );
            }),
            _react2.default.createElement(
              'div',
              { className: 'btn__seemore' },
              this.props.search.load_more ? _react2.default.createElement(
                'button',
                { onClick: this.loadMore.bind(this), className: 'btn btn-default' },
                this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
              ) : ""
            )
          ),
          _react2.default.createElement(_ApplyJobModal2.default, { job: this.props.job, search: this.props.search, user: this.props.user })
        )
      );
    }
  }]);

  return JobSearch;
}(_react.Component);

exports.default = JobSearch;