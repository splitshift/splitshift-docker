'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _MapSearch = require('./MapSearch');

var _MapSearch2 = _interopRequireDefault(_MapSearch);

var _SearchActionCreators = require('../../actions/SearchActionCreators');

var _SearchActionCreators2 = _interopRequireDefault(_SearchActionCreators);

var _ConfigData = require('../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var new_searchmap = false;
var choose_location = { lat: 13.71129, lng: 100.50908 };

var CompanySearch = function (_Component) {
  _inherits(CompanySearch, _Component);

  function CompanySearch() {
    _classCallCheck(this, CompanySearch);

    return _possibleConstructorReturn(this, (CompanySearch.__proto__ || Object.getPrototypeOf(CompanySearch)).apply(this, arguments));
  }

  _createClass(CompanySearch, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _SearchActionCreators2.default.clearFilter();
      _SearchActionCreators2.default.Company({});
      new_searchmap = false;
    }
  }, {
    key: 'updateFilter',
    value: function updateFilter(name, evt) {
      var response = {};
      response[name] = evt.target.value;
      _SearchActionCreators2.default.updateFilter(response);
    }
  }, {
    key: 'updateAdvanceFilter',
    value: function updateAdvanceFilter(name, value, evt) {
      var response = {};

      if (value == 'All') value = "";

      response[name] = value;
      _SearchActionCreators2.default.updateFilter(response);
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {

      evt.preventDefault();

      $('html, body').animate({
        scrollTop: $(this.refs.jobs__company).offset().top - 200
      }, 1000);

      _SearchActionCreators2.default.Company(this.props.search.filter);
    }
  }, {
    key: 'thumbGenerator',
    value: function thumbGenerator(num) {
      return Array.apply(1, { length: num }).map(function (n, index) {
        return _react2.default.createElement(
          'span4',
          { key: index },
          _react2.default.createElement('i', { className: 'fa fa-thumbs-up', 'aria-hidden': 'true', style: { fontSize: "20px" } }),
          ' '
        );
      });
    }
  }, {
    key: 'triggerSearchMap',
    value: function triggerSearchMap() {
      new_searchmap = false;
    }
  }, {
    key: 'loadMore',
    value: function loadMore(evt) {
      _SearchActionCreators2.default.Company(this.props.search.filter, this.props.search.companies.length / 10);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (!nextProps.search.searching && this.props.search.searching) new_searchmap = true;
      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var content = _configLang2.default[this.props.language].company_search;
      var advancedContent = _configLang2.default[this.props.language].advanced_search;
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: 'container' },
          _react2.default.createElement(
            'div',
            { className: 'header__search company-search' },
            _react2.default.createElement(
              'div',
              { className: 'company__title' },
              _react2.default.createElement(
                'p',
                null,
                _react2.default.createElement(
                  'span',
                  null,
                  content.intro
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'job__search' },
              _react2.default.createElement(
                'form',
                { onSubmit: this.onSearch.bind(this) },
                _react2.default.createElement(
                  'div',
                  { className: 'job__bar' },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'company' },
                    content.company[0]
                  ),
                  _react2.default.createElement('input', { id: 'company', type: 'text', className: 'form-control', onChange: this.updateFilter.bind(this, 'keyword'), placeholder: content.company[1] })
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'location__bar' },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'location' },
                    content.location[0]
                  ),
                  _react2.default.createElement('input', { id: 'location', type: 'text', className: 'form-control', placeholder: content.location[1] }),
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default ad__search ad__search--desktop', type: 'button', onClick: _JQueryTool2.default.showAdvanceSearch.bind(this) },
                    advancedContent.title
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'job__btn' },
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default' },
                    _react2.default.createElement('i', { className: 'fa fa-search' })
                  ),
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default ad__search ad__search--responsive', type: 'button', onClick: _JQueryTool2.default.showAdvanceSearch.bind(this) },
                    advancedContent.title
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'jobbar__detail' },
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.type ? advancedContent.company_type[_ConfigData2.default.CompanyTypeList().indexOf(this.props.search.filter.type)] : advancedContent.company_type_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              _ConfigData2.default.CompanyTypeList().map(function (company, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'type', company) },
                    advancedContent.company_type[index]
                  )
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.overall ? this.thumbGenerator(this.props.search.filter.overall) : advancedContent.employee_rating_title,
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              [5, 4, 3, 2, 1].map(function (num, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'overall', num) },
                    _this2.thumbGenerator(num)
                  )
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.bounded_time ? advancedContent.date_posted[this.props.search.filter.bounded_time] : advancedContent.lastest_jobs_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              Object.keys(_ConfigData2.default.DatePostList()).map(function (datepost, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'bounded_time', datepost) },
                    advancedContent.date_posted[datepost]
                  )
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.compensation ? advancedContent.compensation[_ConfigData2.default.CompensationList().indexOf(this.props.search.filter.compensation)] : advancedContent.compensation_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              _ConfigData2.default.CompensationList().map(function (compensation, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'compensation', compensation) },
                    advancedContent.compensation[index]
                  )
                );
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container' },
          _react2.default.createElement(_MapSearch2.default, { companies: this.props.search.companies, jobs: [], new_searchmap: new_searchmap, triggerSearchMap: this.triggerSearchMap.bind(this), choose_location: choose_location }),
          _react2.default.createElement(
            'div',
            { ref: 'jobs__company', className: 'jobs__company' },
            this.props.search.companies.map(function (company, index) {
              return _react2.default.createElement(
                _reactRouter.Link,
                { key: index, to: '/th/company/' + company.key },
                _react2.default.createElement(
                  'div',
                  { className: 'company__item' },
                  _react2.default.createElement(
                    'div',
                    { className: 'job__pic' },
                    _react2.default.createElement('img', { src: company.profile_image ? company.profile_image : "/images/avatar-image.jpg" })
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'search__detail' },
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        _react2.default.createElement(
                          'span',
                          null,
                          company.profile.company_name
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement(
                        'span5',
                        null,
                        company.address.city ? company.address.city : "-"
                      )
                    ),
                    _react2.default.createElement(
                      'div',
                      { className: 'search__btn' },
                      _react2.default.createElement(
                        _reactRouter.Link,
                        { to: '/th/company/' + company.key },
                        _react2.default.createElement(
                          'button',
                          { className: 'btn btn-default' },
                          company.profile.openjobs,
                          ' open job',
                          company.profile.openjobs > 1 ? "s" : ""
                        )
                      )
                    )
                  )
                )
              );
            }),
            _react2.default.createElement(
              'div',
              { className: 'btn__seemore' },
              this.props.search.load_more ? _react2.default.createElement(
                'button',
                { onClick: this.loadMore.bind(this), className: 'btn btn-default' },
                this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
              ) : ""
            )
          )
        )
      );
    }
  }]);

  return CompanySearch;
}(_react.Component);

exports.default = CompanySearch;