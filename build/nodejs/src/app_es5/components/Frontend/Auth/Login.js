'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _AuthenticationActionCreators = require('../../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var loginTrigger = false;
var auth2 = void 0;
var loadGAPI = false;

var AuthLogin = function (_Component) {
  _inherits(AuthLogin, _Component);

  function AuthLogin() {
    _classCallCheck(this, AuthLogin);

    return _possibleConstructorReturn(this, (AuthLogin.__proto__ || Object.getPrototypeOf(AuthLogin)).apply(this, arguments));
  }

  _createClass(AuthLogin, [{
    key: 'facebookLogin',
    value: function facebookLogin() {
      var info = void 0,
          email = void 0,
          token = void 0;
      var self = this;
      FB.login(function (response) {
        //console.log(response);
        token = response.authResponse.accessToken;
        if (response.status === "connected") {
          FB.api('/me', { fields: 'first_name, last_name, gender ,age_range, email' }, function (response) {
            //console.log(response);

            email = response.email;
            info = {
              'email': email,
              'access_token': token,
              'login_type': 'Facebook'
            };

            _AuthenticationActionCreators2.default.login(info);
          });
        }
      }, { scope: 'email' });
    }
  }, {
    key: 'onSuccess',
    value: function onSuccess(googleUser) {

      var profile = googleUser.getBasicProfile();
      var email = profile.getEmail();
      var token = googleUser.getAuthResponse().id_token;

      var info = {
        email: profile.getEmail(),
        access_token: token,
        login_type: 'Google'
      };

      _AuthenticationActionCreators2.default.login(info);
    }
  }, {
    key: 'onLoginTrigger',
    value: function onLoginTrigger(evt) {
      loginTrigger = true;
    }
  }, {
    key: 'simpleLogin',
    value: function simpleLogin(evt) {
      evt.preventDefault();
      if (!/.+@.+[.].+/.test(this.props.state.email)) {
        alert('email not valid');
        console.error('email not valid');
        return;
      }
      var info = {
        'email': this.props.state.email,
        'password': this.props.state.password,
        'login_type': 'Simple'
      };
      _AuthenticationActionCreators2.default.login(info);
    }
  }, {
    key: 'getValue',
    value: function getValue(name, evt) {
      var temp = Object.assign({}, this.props.state);
      temp[name] = evt.target.value;
      _AuthenticationActionCreators2.default.updateLogin(temp);
    }
  }, {
    key: 'attachSignin',
    value: function attachSignin(element) {
      auth2.attachClickHandler(element, {}, this.onSuccess);
    }
  }, {
    key: 'initGoogleAuth',
    value: function initGoogleAuth() {
      var self = this;

      gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
          client_id: '187395441115-as5ie9or8dl1obnegmvmdk14dfeshnvi.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin'
        });
        self.attachSignin(document.getElementById('customBtn'));
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (typeof gapi !== 'undefined') {
        this.initGoogleAuth();
        loadGAPI = true;
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (typeof gapi !== 'undefined' && !loadGAPI) {
        this.initGoogleAuth();
        loadGAPI = true;
      }
    }
  }, {
    key: 'hideLogin',
    value: function hideLogin(evt) {
      this.props.registerBtnTrigger(true);
      setTimeout(function () {
        $("#myModal__Register").modal('show');
      }, 500);
    }
  }, {
    key: 'hideToForgot',
    value: function hideToForgot(evt) {
      $("#myModal__login").modal('hide');
      setTimeout(function () {
        $("#myModal__forgot").modal('show');
      }, 500);
    }
  }, {
    key: 'render',
    value: function render() {
      var formLoginContent = _configLang2.default[this.props.language].form.login;

      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__login', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog modal-sm', role: 'document' },
          _react2.default.createElement(
            'div',
            { className: 'modal-content' },
            _react2.default.createElement(
              'div',
              { className: 'modal-header' },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                _react2.default.createElement(
                  'span5',
                  { 'aria-hidden': 'true' },
                  '\xD7'
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-body' },
              _react2.default.createElement(
                'div',
                { id: 'modal__btn', className: 'row' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-xs-12 col-md-6' },
                  _react2.default.createElement(
                    'a',
                    { onClick: this.facebookLogin.bind(this), href: '#' },
                    _react2.default.createElement(
                      'button',
                      { type: 'button', className: 'modal__btn--fb ' },
                      _react2.default.createElement('i', { className: 'fa fa-facebook' }),
                      ' Facebook'
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-xs-12 col-md-6' },
                  _react2.default.createElement(
                    'button',
                    { id: 'customBtn', type: 'button', className: 'modal__btn--gg' },
                    _react2.default.createElement('i', { className: 'fa fa-google' }),
                    ' Google'
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'row' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-xs-12 col-md-12' },
                  _react2.default.createElement('hr', { id: 'hr-or' }),
                  _react2.default.createElement(
                    'p',
                    { className: 'or' },
                    'or'
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal__form' },
                _react2.default.createElement(
                  'div',
                  { className: 'form-group' },
                  _react2.default.createElement(
                    'form',
                    { onSubmit: this.simpleLogin.bind(this) },
                    _react2.default.createElement(
                      'label',
                      { htmlFor: 'usr' },
                      formLoginContent[0]
                    ),
                    _react2.default.createElement('input', { type: 'text', defaultValue: this.props.state.email, onChange: this.getValue.bind(this, 'email'), className: 'form-control', placeholder: formLoginContent[0] + ' ...', id: 'usr' }),
                    _react2.default.createElement(
                      'label',
                      { htmlFor: 'pwd' },
                      formLoginContent[1]
                    ),
                    _react2.default.createElement('input', { type: 'password', defaultValue: this.props.state.password, onChange: this.getValue.bind(this, 'password'), className: 'form-control', placeholder: formLoginContent[1] + ' ...', id: 'pwd' }),
                    _react2.default.createElement(
                      'div',
                      { className: 'modal__checkbox' },
                      _react2.default.createElement(
                        'div',
                        { className: 'checkbox--remember' },
                        _react2.default.createElement('input', { type: 'checkbox', id: 'remember' }),
                        _react2.default.createElement(
                          'strong',
                          null,
                          formLoginContent[2]
                        )
                      ),
                      _react2.default.createElement(
                        'div',
                        { className: 'modal__fpwd' },
                        _react2.default.createElement(
                          'a',
                          { className: 'text--fpwd', href: '#', onClick: this.hideToForgot.bind(this) },
                          _react2.default.createElement(
                            'strong',
                            null,
                            formLoginContent[3]
                          )
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'div',
                      null,
                      _react2.default.createElement(
                        'a',
                        { href: '#' },
                        _react2.default.createElement(
                          'button',
                          { className: 'modal__btn--dark' },
                          formLoginContent[4]
                        )
                      )
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal-footer' },
                _react2.default.createElement(
                  'p',
                  null,
                  formLoginContent[5],
                  ' '
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement(
                    'a',
                    { href: '#' },
                    _react2.default.createElement(
                      'button',
                      { className: 'modal__btn--light', 'data-dismiss': 'modal', onClick: this.hideLogin.bind(this) },
                      formLoginContent[6]
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return AuthLogin;
}(_react.Component);

exports.default = AuthLogin;

/*
<a href="#">
  <button type="button" className="modal__btn--gg" onClick={this.googleLogin.bind(this)}>
    <i className="fa fa-google"></i> Google
  </button>
</a>

*/