'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _AuthenticationActionCreators = require('../../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var self = void 0;
var auth2 = void 0;
var loadGAPI = void 0;

var AuthRegister = function (_Component) {
  _inherits(AuthRegister, _Component);

  function AuthRegister() {
    _classCallCheck(this, AuthRegister);

    return _possibleConstructorReturn(this, (AuthRegister.__proto__ || Object.getPrototypeOf(AuthRegister)).apply(this, arguments));
  }

  _createClass(AuthRegister, [{
    key: 'facebookRegister',
    value: function facebookRegister() {
      FB.login(function (response) {
        var loginInfo = response;

        if (response.status === "connected") {
          FB.api('/me', { fields: 'first_name, last_name, gender ,age_range, email' }, function (response) {

            _AuthenticationActionCreators2.default.createRegister({
              'first_name': response.first_name,
              'last_name': response.last_name,
              'email': response.email,
              'term': false,
              'register_type': 'Facebook',
              'type': 'Employee',
              'access_token': loginInfo.authResponse.accessToken
            });

            _JQueryTool2.default.toPostRegister();
          });
        }
      }, { scope: 'email' });
    }
  }, {
    key: 'googleRegister',
    value: function googleRegister(googleUser) {

      var profile = googleUser.getBasicProfile();
      var first_name = profile.getGivenName();
      var last_name = profile.getFamilyName();

      _AuthenticationActionCreators2.default.createRegister({
        'first_name': first_name,
        'last_name': last_name,
        'email': profile.getEmail(),
        'term': false,
        'register_type': 'Google',
        'type': 'Employee',
        'access_token': googleUser.getAuthResponse().id_token
      });

      _JQueryTool2.default.toPostRegister();
    }
  }, {
    key: 'emailRegister',
    value: function emailRegister() {

      _AuthenticationActionCreators2.default.createRegister({
        'first_name': '',
        'last_name': '',
        'email': '',
        'term': false,
        'register_type': 'Simple',
        'type': 'Employee',
        'password': '',
        'confirmPassword': ''
      });

      _JQueryTool2.default.toPostRegister();
    }
  }, {
    key: 'attachSignup',
    value: function attachSignup(element) {
      auth2.attachClickHandler(element, {}, this.googleRegister);
    }
  }, {
    key: 'initGoogleAuth',
    value: function initGoogleAuth() {
      gapi.load('auth2', function () {

        auth2 = gapi.auth2.init({
          client_id: '187395441115-as5ie9or8dl1obnegmvmdk14dfeshnvi.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin'
        });

        self.attachSignup(document.getElementById('customBtn2'));
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {

      self = this;

      if (typeof gapi !== 'undefined') {
        this.initGoogleAuth();
        loadGAPI = true;
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (typeof gapi !== 'undefined' && !loadGAPI) {
        this.initGoogleAuth();
        loadGAPI = true;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var formPreregisContent = _configLang2.default[this.props.language].form.pre_register;

      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__Register', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog modal-sm', role: 'document' },
          _react2.default.createElement(
            'div',
            { className: 'modal-content' },
            _react2.default.createElement(
              'div',
              { className: 'modal-header', style: { borderBottom: "0px", paddingBottom: "0px" } },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                _react2.default.createElement(
                  'span5',
                  { 'aria-hidden': 'true' },
                  '\xD7'
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-body' },
              _react2.default.createElement(
                'div',
                { id: 'modal__btn', className: 'row' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-xs-12 col-md-6' },
                  _react2.default.createElement(
                    'a',
                    { href: '#', 'data-toggle': 'modal' },
                    _react2.default.createElement(
                      'button',
                      { onClick: this.facebookRegister.bind(this), className: 'modal__btn--fb', type: 'button' },
                      _react2.default.createElement('i', { className: 'fa fa-facebook', 'aria-hidden': 'true' }),
                      ' Facebook'
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-xs-12 col-md-6' },
                  _react2.default.createElement(
                    'button',
                    { id: 'customBtn2', type: 'button', className: 'modal__btn--gg' },
                    _react2.default.createElement('i', { className: 'fa fa-google' }),
                    ' Google'
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'row' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-xs-12 col-md-12' },
                  _react2.default.createElement('hr', { id: 'hr-or' }),
                  _react2.default.createElement(
                    'p',
                    { className: 'or' },
                    'or'
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal__btnregis' },
                _react2.default.createElement(
                  'a',
                  { href: '#' },
                  _react2.default.createElement(
                    'button',
                    { onClick: this.emailRegister.bind(this), className: 'modal__btn--dark', type: 'button', 'data-toggle': 'modal' },
                    formPreregisContent[0]
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-footer' },
              _react2.default.createElement(
                'p',
                null,
                formPreregisContent[1]
              ),
              _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                  'a',
                  { href: '#' },
                  _react2.default.createElement(
                    'button',
                    { className: 'modal__btn--light', 'data-toggle': 'modal', 'data-target': '#myModal__login', 'data-dismiss': 'modal' },
                    'Log in'
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return AuthRegister;
}(_react.Component);

exports.default = AuthRegister;