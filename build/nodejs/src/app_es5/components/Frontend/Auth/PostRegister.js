'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _AuthenticationActionCreators = require('../../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AuthPostRegister = function (_Component) {
  _inherits(AuthPostRegister, _Component);

  function AuthPostRegister() {
    _classCallCheck(this, AuthPostRegister);

    return _possibleConstructorReturn(this, (AuthPostRegister.__proto__ || Object.getPrototypeOf(AuthPostRegister)).apply(this, arguments));
  }

  _createClass(AuthPostRegister, [{
    key: 'chooseCustomerType',
    value: function chooseCustomerType(evt) {
      var user = Object.assign({}, this.props.user);
      user['type'] = evt.target.value;
      _AuthenticationActionCreators2.default.updateRegister(user);
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var temp = Object.assign({}, this.props.user);
      temp.term = false;
      _AuthenticationActionCreators2.default.updateRegister(temp);
    }
  }, {
    key: 'thickCheckBox',
    value: function thickCheckBox(evt) {
      var temp = Object.assign({}, this.props.user);
      temp.term = !temp.term;
      _AuthenticationActionCreators2.default.updateRegister(temp);
    }
  }, {
    key: 'getValue',
    value: function getValue(name, evt) {
      var temp = Object.assign({}, this.props.user);
      temp[name] = evt.target.value;
      _AuthenticationActionCreators2.default.updateRegister(temp);
    }
  }, {
    key: 'hidePostRegister',
    value: function hidePostRegister(evt) {
      $("#myModal__Regisform").modal('hide');
    }
  }, {
    key: 'submitForm',
    value: function submitForm(evt) {

      evt.preventDefault();

      if (!/.+@.+[.].+/.test(this.props.user.email)) alert('email not valid');else if (this.props.user.register_type == 'Simple' && this.props.user.password == '') alert('please enter password');else if (this.props.user.register_type == 'Simple' && this.props.user.password != this.props.user.confirmPassword) alert('password and confirm password not the same');else if (!this.props.user.term) alert('Please accept our User Agreement');else _AuthenticationActionCreators2.default.register(this.props.user);
    }
  }, {
    key: 'render',
    value: function render() {

      var passwordForm = "";
      var formContent = _configLang2.default[this.props.language].form;
      var formRegisterContent = formContent.register;

      if (this.props.user.register_type == 'Simple') {
        passwordForm = _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            'label',
            { htmlFor: 'pwd' },
            formContent.password
          ),
          _react2.default.createElement('input', { type: 'password', onChange: this.getValue.bind(this, 'password'), className: 'form-control', placeholder: formContent.password + " ...", value: this.props.user.password }),
          _react2.default.createElement(
            'label',
            { htmlFor: 'pwd' },
            formContent.confirm_password
          ),
          _react2.default.createElement('input', { type: 'password', onChange: this.getValue.bind(this, 'confirmPassword'), className: 'form-control', placeholder: formContent.confirm_password + " ...", value: this.props.user.confirmPassword })
        );
      }

      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__Regisform', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog modal-sm', role: 'document' },
          _react2.default.createElement(
            'div',
            { className: 'modal-content' },
            _react2.default.createElement(
              'div',
              { className: 'modal-header', style: { borderBottom: "0px", paddingBottom: "0px" } },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                _react2.default.createElement(
                  'span5',
                  { 'aria-hidden': 'true' },
                  '\xD7'
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-body' },
              _react2.default.createElement(
                'div',
                { className: 'modal__form' },
                _react2.default.createElement(
                  'div',
                  { className: 'form__type' },
                  _react2.default.createElement('input', { type: 'radio', name: 'type', value: 'Employee', id: 'employee', onChange: this.chooseCustomerType.bind(this), defaultChecked: true }),
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'employee' },
                    _react2.default.createElement(
                      'span',
                      { style: { fontSize: "13px" } },
                      _react2.default.createElement(
                        'strong',
                        null,
                        formRegisterContent[0]
                      )
                    )
                  ),
                  _react2.default.createElement('input', { type: 'radio', name: 'type', value: 'Employer', id: 'employer', onChange: this.chooseCustomerType.bind(this) }),
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'employer' },
                    _react2.default.createElement(
                      'span',
                      { style: { fontSize: "13px" } },
                      _react2.default.createElement(
                        'strong',
                        null,
                        formRegisterContent[1]
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'modal__employ' },
                  _react2.default.createElement(
                    'div',
                    { className: 'form-group' },
                    _react2.default.createElement(
                      'form',
                      { onSubmit: this.submitForm.bind(this) },
                      this.props.user.type == "Employer" ? _react2.default.createElement(
                        'div',
                        null,
                        _react2.default.createElement(
                          'label',
                          { htmlFor: 'usr' },
                          formRegisterContent[2]
                        ),
                        _react2.default.createElement('input', { type: 'text', onChange: this.getValue.bind(this, 'company_name'), className: 'form-control', placeholder: formRegisterContent[2] + " ..." })
                      ) : "",
                      this.props.user.register_type === "Facebook" && this.props.user.first_name || this.props.user.register_type === "Google" && this.props.user.first_name || this.props.user.register_type == "Simple" ? _react2.default.createElement(
                        'div',
                        null,
                        _react2.default.createElement(
                          'label',
                          { htmlFor: 'usr' },
                          formRegisterContent[3]
                        ),
                        _react2.default.createElement('input', { type: 'text', onChange: this.getValue.bind(this, 'first_name'), className: 'form-control', placeholder: formRegisterContent[3] + ' ...', defaultValue: this.props.user.first_name }),
                        _react2.default.createElement(
                          'label',
                          { htmlFor: 'usr' },
                          formRegisterContent[4]
                        ),
                        _react2.default.createElement('input', { type: 'text', onChange: this.getValue.bind(this, 'last_name'), className: 'form-control', placeholder: formRegisterContent[4] + ' ...', defaultValue: this.props.user.last_name }),
                        _react2.default.createElement(
                          'label',
                          { htmlFor: 'pwd' },
                          formRegisterContent[5]
                        ),
                        _react2.default.createElement('input', { type: 'text', onChange: this.getValue.bind(this, 'email'), className: 'form-control', placeholder: formRegisterContent[5] + ' ...', defaultValue: this.props.user.email })
                      ) : "",
                      passwordForm,
                      _react2.default.createElement(
                        'div',
                        { className: 'modal__checkbox' },
                        _react2.default.createElement(
                          'div',
                          { className: 'checkbox--remember' },
                          _react2.default.createElement('input', { type: 'checkbox', id: 'remember', onChange: this.thickCheckBox.bind(this) }),
                          formRegisterContent[6],
                          ' ',
                          _react2.default.createElement(
                            _reactRouter.Link,
                            { onClick: this.hidePostRegister.bind(this), to: '/th/user-agreement' },
                            formRegisterContent[7]
                          ),
                          '.'
                        )
                      ),
                      _react2.default.createElement(
                        'div',
                        { style: { marginBottom: "10px" } },
                        _react2.default.createElement(
                          'a',
                          { href: '#' },
                          _react2.default.createElement(
                            'button',
                            { className: 'modal__btn--dark', type: 'submit' },
                            formRegisterContent[8]
                          )
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return AuthPostRegister;
}(_react.Component);

exports.default = AuthPostRegister;