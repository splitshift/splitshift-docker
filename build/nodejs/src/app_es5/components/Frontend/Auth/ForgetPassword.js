'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _AuthenticationActionCreators = require('../../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AuthForgetPassword = function (_Component) {
  _inherits(AuthForgetPassword, _Component);

  function AuthForgetPassword() {
    _classCallCheck(this, AuthForgetPassword);

    return _possibleConstructorReturn(this, (AuthForgetPassword.__proto__ || Object.getPrototypeOf(AuthForgetPassword)).apply(this, arguments));
  }

  _createClass(AuthForgetPassword, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $("#myModal__forgot").modal('hide');
    }
  }, {
    key: 'sendForgetPassword',
    value: function sendForgetPassword(evt) {
      evt.preventDefault();
      var result = { email: this.refs.email_forget.value };
      _AuthenticationActionCreators2.default.forgetPassword(result);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (nextProps.login.forget && !this.props.login.forget) $("#myModal__forgot").modal('hide');

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__forgot', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog modal-sm', role: 'document' },
          _react2.default.createElement(
            'div',
            { className: 'modal-content' },
            _react2.default.createElement(
              'div',
              { className: 'modal-header' },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                _react2.default.createElement(
                  'span5',
                  { 'aria-hidden': 'true' },
                  '\xD7'
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-body' },
              _react2.default.createElement(
                'div',
                { className: 'modal__form' },
                _react2.default.createElement(
                  'div',
                  { className: 'form-group' },
                  _react2.default.createElement(
                    'form',
                    { onSubmit: this.sendForgetPassword.bind(this) },
                    _react2.default.createElement(
                      'label',
                      { htmlFor: 'usr' },
                      'Email'
                    ),
                    _react2.default.createElement('input', { type: 'text', ref: 'email_forget', className: 'form-control', placeholder: 'Email...', id: 'usr' }),
                    _react2.default.createElement(
                      'div',
                      null,
                      _react2.default.createElement(
                        'button',
                        { className: 'modal__btn--dark' },
                        'SEND EMAIL TO RESET PASSWORD'
                      )
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal-footer' },
                _react2.default.createElement(
                  'p',
                  null,
                  'Don',
                  "'",
                  't have an account? '
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement(
                    'a',
                    { href: '#' },
                    _react2.default.createElement(
                      'button',
                      { className: 'modal__btn--light', 'data-toggle': 'modal', 'data-target': '#myModal__login', 'data-dismiss': 'modal' },
                      'Sign In'
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return AuthForgetPassword;
}(_react.Component);

exports.default = AuthForgetPassword;