'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _JobActionCreators = require('../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ApplyJobModal = function (_Component) {
  _inherits(ApplyJobModal, _Component);

  function ApplyJobModal() {
    _classCallCheck(this, ApplyJobModal);

    return _possibleConstructorReturn(this, (ApplyJobModal.__proto__ || Object.getPrototypeOf(ApplyJobModal)).apply(this, arguments));
  }

  _createClass(ApplyJobModal, [{
    key: 'uploadFile',
    value: function uploadFile(evt) {
      _JobActionCreators2.default.uploadFileApplyJob(this.props.user.token, evt.target.files[0]);
    }
  }, {
    key: 'updateDraft',
    value: function updateDraft(name, evt) {
      var response = {};
      response[name] = evt.target.value;

      _JobActionCreators2.default.updateApplyJobDraft(response);
    }
  }, {
    key: 'sendApplyJob',
    value: function sendApplyJob(evt) {
      var jobForm = this.props.job.apply_draft;
      var allFilePath = jobForm.files ? jobForm.files.map(function (file, key) {
        return file.path;
      }) : [];

      jobForm.job_id = this.props.search.jobs[this.props.search.job_index]._id;
      jobForm.to = this.props.search.jobs[this.props.search.job_index].owner.key;
      jobForm.files = allFilePath;

      _JobActionCreators2.default.sendApplyJob(this.props.user.token, jobForm);
    }
  }, {
    key: 'sendForward',
    value: function sendForward(evt) {

      var email_sender = this.refs.forward_email.value;
      var currentJob = this.props.search.jobs[this.props.search.job_index];

      var name = this.props.user.rank === 'Employee' ? this.props.user.first_name + " " + this.props.user.last_name : this.props.user.company_name;

      var forwardData = "<p>" + name + " found a job you may be interested in at www.splitshift.com</p><br />" + "<p><b>Position</b>: " + currentJob.position + "</p>" + "<p><b>Type</b>: " + currentJob.type + "</p>" + "<p><b>Min.Required Experience</b>: " + currentJob.min_year + "</p>" + "<p><b>Compensation</b>: " + currentJob.compensation + "</p>" + "<p><b>Job Details</b>:<br />" + currentJob.description + "</p>" + "<p><b>Required Skills</b>:<br />" + currentJob.required_skills.map(function (skill, index) {
        return skill;
      }) + "</p>" + "<p><br />See more detail: <a href='http://www.splitshift.com/th/company/" + currentJob.owner.key + "'>http://www.splitshift.com/th/company/" + currentJob.owner.key + "</a></p>";

      var result = {
        to: email_sender,
        data: forwardData
      };

      // Sending Email
      _JobActionCreators2.default.forwardEmail(this.props.user.token, result);
    }
  }, {
    key: 'openApplyJobForm',
    value: function openApplyJobForm(evt) {
      $(".btn-command").show();
      $(evt.target).hide();
      $(".forward--form").hide();
      $(".application--form").show();
    }
  }, {
    key: 'forwardApplyJob',
    value: function forwardApplyJob(evt) {
      $(".btn-command").show();
      $(evt.target).hide();
      $(".forward--form").show();
      $(".application--form").hide();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      $("#myModal__positions").modal('hide');
    }
  }, {
    key: 'render',
    value: function render() {

      var currentJob = this.props.search.jobs[this.props.search.job_index];
      var forwardText = this.props.search.job_index !== null ? "Position: " + currentJob.position + "\nType: " + currentJob.type + "\nMin.Required Experience: " + currentJob.min_year + "\nCompensation: " + currentJob.compensation + "\n\nJob Details:\n" + currentJob.description + "\n\nRequired Skills:\n" + currentJob.required_skills.map(function (skill, index) {
        return skill;
      }) : "";

      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__positions', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog', role: 'document' },
          _react2.default.createElement(
            'div',
            { className: 'modal-content' },
            _react2.default.createElement(
              'div',
              { className: 'modal-header' },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                _react2.default.createElement(
                  'span5',
                  { 'aria-hidden': 'true' },
                  '\xD7'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal-title' },
                _react2.default.createElement(
                  'b',
                  null,
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].position
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-body' },
              _react2.default.createElement(
                'div',
                { className: 'job--description' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'b',
                    null,
                    'Type'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].type
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Min.Required Experience'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].min_year,
                    ' years'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Compensation'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].compensation,
                    ' (Based on Experience)'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Job Details'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'div',
                    { className: 'text--detail' },
                    _react2.default.createElement(
                      'span',
                      null,
                      this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].description
                    )
                  ),
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Required Skills'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].required_skills.map(function (skill, index) {
                    return _react2.default.createElement(
                      'span',
                      { key: index },
                      _react2.default.createElement('i', { className: 'fa fa-star' }),
                      ' ',
                      skill,
                      ' ',
                      _react2.default.createElement('br', null)
                    );
                  })
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-footer' },
              _react2.default.createElement(
                'div',
                { style: { textAlign: "center" } },
                this.props.search.job_index != null && typeof this.props.user.token !== 'undefined' ? _react2.default.createElement(
                  'button',
                  { onClick: this.forwardApplyJob.bind(this), className: 'modal__btn--light modal__btn--left modal__btn--responsive btn-command' },
                  'Forward'
                ) : "",
                _react2.default.createElement(
                  'div',
                  null,
                  this.props.user.token && this.props.user.type === 'Employee' ? _react2.default.createElement(
                    'button',
                    { className: 'modal__btn--light modal__btn--right modal__btn--responsive btn-command applyjob none', onClick: this.openApplyJobForm.bind(this) },
                    'Apply Now'
                  ) : "",
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: this.props.search.job_index != null ? '/th/company/' + this.props.search.jobs[this.props.search.job_index].owner.key : '/th/company/' },
                    _react2.default.createElement(
                      'button',
                      { className: 'modal__btn--light modal__btn--right modal__btn--responsive' },
                      'See Company Profile'
                    )
                  )
                )
              ),
              this.props.user.token ? _react2.default.createElement(
                'div',
                { className: 'application--form', style: { clear: "both" } },
                _react2.default.createElement(
                  'label',
                  { htmlFor: 'letter' },
                  'Cover Letter'
                ),
                _react2.default.createElement('textarea', { rows: '5', onChange: this.updateDraft.bind(this, 'cover_letter'), placeholder: 'Dear..', id: 'letter' }),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-6 col-sm-12', style: { padding: "0px", paddingRight: "5px" } },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'phone' },
                    'Phone'
                  ),
                  _react2.default.createElement('input', { type: 'text', onChange: this.updateDraft.bind(this, 'tel'), className: 'form-control form-control--mini', placeholder: 'Phone Number...', id: 'phone' })
                ),
                this.props.user.complete ? _react2.default.createElement(
                  'div',
                  { className: 'col-md-6 col-sm-12', style: { padding: "0px" } },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'email' },
                    'Email'
                  ),
                  _react2.default.createElement('input', { type: 'text', onChange: this.updateDraft.bind(this, 'email'), className: 'form-control form-control--mini', placeholder: 'Email Address...', id: 'email', defaultValue: this.props.user.email })
                ) : "",
                _react2.default.createElement(
                  'div',
                  { className: 'attach__file' },
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      'File Attachments'
                    )
                  ),
                  this.props.search.job_index != null && typeof this.props.job.apply_draft.files !== "undefined" ? _react2.default.createElement(
                    'div',
                    { style: { textAlign: "left" } },
                    this.props.job.apply_draft.files.map(function (file, index) {
                      return _react2.default.createElement(
                        'div',
                        { key: index },
                        _react2.default.createElement('i', { className: 'fa fa-star' }),
                        ' ',
                        file.original_name,
                        ' ',
                        _react2.default.createElement('br', null)
                      );
                    })
                  ) : "",
                  _react2.default.createElement('input', { onChange: this.uploadFile.bind(this), type: 'file', name: 'attachfile' })
                ),
                _react2.default.createElement(
                  'button',
                  { onClick: this.sendApplyJob.bind(this), className: 'modal__btn--light modal__btn--responsive' },
                  'Submit Application'
                )
              ) : "",
              this.props.user.token && _react2.default.createElement(
                'div',
                { className: 'forward--form', style: { clear: "both" } },
                _react2.default.createElement(
                  'label',
                  { htmlFor: 'letter' },
                  'Forward this job to'
                ),
                _react2.default.createElement('input', { type: 'text', ref: 'forward_email', className: 'form-control form-control--mini', placeholder: 'Email Address...' }),
                _react2.default.createElement(
                  'label',
                  { htmlFor: 'letter' },
                  'Message'
                ),
                this.props.search.job_index != null && _react2.default.createElement('textarea', { rows: '10', placeholder: 'Dear..', id: 'letter', value: forwardText, readOnly: true }),
                this.props.job.sending_forward_email && _react2.default.createElement(
                  'span',
                  null,
                  'Sending...'
                ),
                ' ',
                _react2.default.createElement(
                  'button',
                  { onClick: this.sendForward.bind(this), className: 'modal__btn--light modal__btn--responsive', style: { marginTop: "10px" } },
                  'Forward'
                )
              )
            )
          )
        )
      );
    }
  }]);

  return ApplyJobModal;
}(_react.Component);

exports.default = ApplyJobModal;