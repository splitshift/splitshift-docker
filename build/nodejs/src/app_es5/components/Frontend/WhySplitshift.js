"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var WhySplitshift = function (_Component) {
  _inherits(WhySplitshift, _Component);

  function WhySplitshift() {
    _classCallCheck(this, WhySplitshift);

    return _possibleConstructorReturn(this, (WhySplitshift.__proto__ || Object.getPrototypeOf(WhySplitshift)).apply(this, arguments));
  }

  _createClass(WhySplitshift, [{
    key: "componentDidMount",
    value: function componentDidMount() {

      var self = this;

      $(".whysplitshift__btn--expand").click(function () {
        $(".whysplitshift__text--expand").toggle();
        var btnText = $(".whysplitshift__btn--expand").text();
        $(".whysplitshift__btn--expand").text(btnText === self.props.content.readmore ? self.props.content.readless : self.props.content.readmore);
      });
    }
  }, {
    key: "render",
    value: function render() {

      return _react2.default.createElement(
        "div",
        { className: "whysplitshift" },
        _react2.default.createElement(
          "div",
          { className: "container" },
          _react2.default.createElement(
            "div",
            { className: "text--center" },
            _react2.default.createElement("h2", { dangerouslySetInnerHTML: { __html: this.props.content.header } }),
            _react2.default.createElement("hr", { style: { width: "50%" } })
          ),
          _react2.default.createElement(
            "div",
            { className: "row" },
            _react2.default.createElement(
              "div",
              { className: "col-md-6 col-xs-12" },
              _react2.default.createElement(
                "div",
                { className: "whysplitshift__text" },
                _react2.default.createElement(
                  "p",
                  null,
                  this.props.content.paragraphs[0]
                ),
                _react2.default.createElement(
                  "p",
                  null,
                  this.props.content.paragraphs[1]
                ),
                _react2.default.createElement(
                  "p",
                  null,
                  this.props.content.paragraphs[2]
                )
              ),
              _react2.default.createElement(
                "div",
                { className: "whysplitshift__text--expand" },
                _react2.default.createElement(
                  "p",
                  null,
                  this.props.content.paragraphs[3]
                ),
                _react2.default.createElement("p", { dangerouslySetInnerHTML: { __html: this.props.content.paragraphs[4] } })
              ),
              _react2.default.createElement(
                "a",
                { className: "whysplitshift__btn--expand" },
                this.props.content.readmore
              )
            ),
            _react2.default.createElement(
              "div",
              { className: "col-md-6 col-xs-12" },
              _react2.default.createElement(
                "div",
                { className: "whysplitshift__video" },
                _react2.default.createElement(
                  "object",
                  { width: "100%", height: "300", data: "https://www.youtube.com/embed/aBJO1DQmf_4?rel=0" },
                  "Your browser does not support the video tag."
                )
              )
            )
          )
        )
      );
    }
  }]);

  return WhySplitshift;
}(_react.Component);

exports.default = WhySplitshift;