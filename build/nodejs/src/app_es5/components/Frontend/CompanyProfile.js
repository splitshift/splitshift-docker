'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _Header = require('./Form/Company/Header');

var _Header2 = _interopRequireDefault(_Header);

var _Position = require('./Form/Company/Position');

var _Position2 = _interopRequireDefault(_Position);

var _People = require('./Form/Company/People');

var _People2 = _interopRequireDefault(_People);

var _Culture = require('./Form/Company/Culture');

var _Culture2 = _interopRequireDefault(_Culture);

var _Benefit = require('./Form/Company/Benefit');

var _Benefit2 = _interopRequireDefault(_Benefit);

var _Video = require('./Form/Company/Video');

var _Video2 = _interopRequireDefault(_Video);

var _Location = require('./Form/Company/Location');

var _Location2 = _interopRequireDefault(_Location);

var _Overview = require('./Form/Company/Overview');

var _Overview2 = _interopRequireDefault(_Overview);

var _SocialLink = require('./Form/Company/SocialLink');

var _SocialLink2 = _interopRequireDefault(_SocialLink);

var _Job = require('./Form/Company/Job');

var _Job2 = _interopRequireDefault(_Job);

var _Review = require('./Form/Company/Review');

var _Review2 = _interopRequireDefault(_Review);

var _Gallery = require('./Form/Company/Gallery');

var _Gallery2 = _interopRequireDefault(_Gallery);

var _SummaryReview = require('./Form/Company/SummaryReview');

var _SummaryReview2 = _interopRequireDefault(_SummaryReview);

var _LastestReview = require('./Form/Company/LastestReview');

var _LastestReview2 = _interopRequireDefault(_LastestReview);

var _NewsFeed = require('./Form/Feed/NewsFeed');

var _NewsFeed2 = _interopRequireDefault(_NewsFeed);

var _Photo = require('./Form/Feed/Photo');

var _Photo2 = _interopRequireDefault(_Photo);

var _ResourceActionCreators = require('../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _CompanyProfileActionCreators = require('../../actions/CompanyProfileActionCreators');

var _CompanyProfileActionCreators2 = _interopRequireDefault(_CompanyProfileActionCreators);

var _MessageActionCreators = require('../../actions/MessageActionCreators');

var _MessageActionCreators2 = _interopRequireDefault(_MessageActionCreators);

var _SocialActionCreators = require('../../actions/SocialActionCreators');

var _SocialActionCreators2 = _interopRequireDefault(_SocialActionCreators);

var _BackgroundTemplate = require('./BackgroundTemplate');

var _BackgroundTemplate2 = _interopRequireDefault(_BackgroundTemplate);

var _Message = require('./Message');

var _Message2 = _interopRequireDefault(_Message);

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var currentMap = void 0;
var button_header_click = false;

var CompanyProfile = function (_Component) {
	_inherits(CompanyProfile, _Component);

	function CompanyProfile() {
		_classCallCheck(this, CompanyProfile);

		return _possibleConstructorReturn(this, (CompanyProfile.__proto__ || Object.getPrototypeOf(CompanyProfile)).apply(this, arguments));
	}

	_createClass(CompanyProfile, [{
		key: 'loadMessage',
		value: function loadMessage(evt) {
			_MessageActionCreators2.default.chooseOwner(this.props.user);
			_MessageActionCreators2.default.chooseOther(this.props.profile);
			_MessageActionCreators2.default.getMessageWithPeople(this.props.user.token, this.props.profile.key);
		}
	}, {
		key: 'triggerMap',
		value: function triggerMap(map) {
			currentMap = map;
		}
	}, {
		key: 'triggerButton',
		value: function triggerButton(evt) {
			button_header_click = true;
			_JQueryTool2.default.animateScrollTo(this.props.page != 'company_image_profile' ? '.newsfeed' : '.timeline-photo');
		}
	}, {
		key: 'changeBannerImageWithTemplate',
		value: function changeBannerImageWithTemplate(template_url, evt) {
			_ResourceActionCreators2.default.setDraft('template_banner', template_url);
			_CompanyProfileActionCreators2.default.changeBannerImageWithTemplate(this.props.user.token, { path: template_url });
			$("#myModal__background").modal('hide');
		}
	}, {
		key: 'toggleMoreInfo',
		value: function toggleMoreInfo(evt) {
			$("#more-info-detail").toggle();
			$(evt.target).html($(evt.target).html() === "More Information" ? "Less Information" : "More Information");
			if (currentMap) google.maps.event.trigger(currentMap, 'resize');
		}
	}, {
		key: 'loadNewCompany',
		value: function loadNewCompany(profile_key) {
			_CompanyProfileActionCreators2.default.getProfile(profile_key, this.props.user.token);
			_SocialActionCreators2.default.getFollowers(profile_key);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			var profile_key = "";
			button_header_click = false;

			if (typeof this.props.profile_key !== 'undefined') profile_key = this.props.profile_key;else if (typeof this.props.user.key !== 'undefined') profile_key = this.props.user.key;

			this.loadNewCompany(profile_key);
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps, nextState) {

			if (nextProps.profile_key && nextProps.profile_key !== this.props.profile_key) this.loadNewCompany(nextProps.profile_key);else if (typeof nextProps.profile_key === 'undefined' && typeof this.props.profile_key !== 'undefined' && this.props.user.token) this.loadNewCompany(this.props.user.key);

			return true;
		}
	}, {
		key: 'setEditor',
		value: function setEditor(name) {
			var oldData = {
				first_name: this.props.profile.first_name,
				last_name: this.props.profile.last_name,
				company_name: this.props.profile.company_name,
				type: this.props.profile.type ? this.props.profile.type : "Hotel"
			};
			_ResourceActionCreators2.default.setOldDraft(oldData);
			_ResourceActionCreators2.default.setEditor(name, true);
		}
	}, {
		key: 'onFollowPeople',
		value: function onFollowPeople(evt) {
			_SocialActionCreators2.default.putFollow(this.props.user.token, this.props.profile.key);
		}
	}, {
		key: 'render',
		value: function render() {
			var formState = {
				profile: this.props.profile,
				status: this.props.status,
				user: this.props.user,
				lang: this.props.lang,
				editor: this.props.editor,
				other: this.props.profile_key && this.props.profile_key != this.props.user.key
			};

			if (typeof this.props.profile.key === 'undefined') return _react2.default.createElement('div', null);

			var menusContent = _configLang2.default[this.props.lang].company_profile.menus;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(_Header2.default, formState),
				_react2.default.createElement(_Job2.default, { user: this.props.user, profile: this.props.profile, job: this.props.job }),
				_react2.default.createElement(_Review2.default, formState),
				_react2.default.createElement(
					'div',
					{ className: 'cover__nav' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col-md-8' },
							_react2.default.createElement(
								'div',
								{ className: 'navbar__btnlist' },
								_react2.default.createElement(
									_reactRouter.Link,
									{ to: '/' + this.props.lang + '/company/' + this.props.profile.key, onClick: this.triggerButton.bind(this), className: 'btn btn-default reverse' },
									menusContent[0]
								),
								_react2.default.createElement(
									_reactRouter.Link,
									{ to: '/' + this.props.lang + '/company/' + this.props.profile.key + '/photo', onClick: this.triggerButton.bind(this), className: 'btn btn-default reverse' },
									menusContent[1]
								),
								this.props.editor ? _react2.default.createElement(
									'span',
									{ className: 'navbar__editbtn' },
									_react2.default.createElement(
										_reactRouter.Link,
										{ className: 'btn btn-default reverse', to: '/' + this.props.lang + '/company/' + this.props.profile.key },
										menusContent[2]
									),
									_react2.default.createElement(
										'button',
										{ className: 'btn btn-default reverse', onClick: this.setEditor.bind(this, "edit_company_profile_card") },
										menusContent[3]
									)
								) : "",
								this.props.user.token !== 'undefined' && formState.other ? _react2.default.createElement(
									'button',
									{ onClick: this.loadMessage.bind(this), className: 'btn btn-default reverse', 'data-toggle': 'modal', 'data-target': '#myModal__message' },
									menusContent[4]
								) : ""
							),
							formState.other && this.props.profile.key ? _react2.default.createElement(
								'div',
								{ className: 'navbar__btnlist' },
								this.props.user.token && typeof this.props.profile.following !== 'undefined' ? _react2.default.createElement(
									'button',
									{ className: this.props.profile.following ? "btn btn-default reverse" : "btn btn-default", onClick: this.onFollowPeople.bind(this) },
									this.props.profile.following ? menusContent[6] : menusContent[7]
								) : ""
							) : "",
							_react2.default.createElement(
								'div',
								{ className: 'navbar__btnlist' },
								_react2.default.createElement(
									'a',
									{ href: '/' + this.props.lang + "/company/" + this.props.profile.key + "/profile", target: '_blank' },
									_react2.default.createElement(
										'button',
										{ className: 'btn btn-default reverse' },
										menusContent[5]
									)
								)
							)
						),
						this.props.profile.links ? _react2.default.createElement(
							'div',
							{ className: 'col-md-4' },
							_react2.default.createElement(
								'div',
								{ id: 'social_footer' },
								_react2.default.createElement(
									'ul',
									null,
									this.props.profile.links.facebook ? _react2.default.createElement(
										'li',
										null,
										_react2.default.createElement(
											'a',
											{ href: this.props.profile.links.facebook, target: '_blank' },
											_react2.default.createElement('i', { className: 'icon-facebook' })
										)
									) : "",
									this.props.profile.links.instagram ? _react2.default.createElement(
										'li',
										null,
										_react2.default.createElement(
											'a',
											{ href: this.props.profile.links.instagram, target: '_blank' },
											_react2.default.createElement('i', { className: 'icon-instagram' })
										)
									) : "",
									this.props.profile.links.youtube ? _react2.default.createElement(
										'li',
										null,
										_react2.default.createElement(
											'a',
											{ href: this.props.profile.links.youtube, target: '_blank' },
											_react2.default.createElement('i', { className: 'icon-youtube-play' })
										)
									) : "",
									this.props.profile.links.linkedin ? _react2.default.createElement(
										'li',
										null,
										_react2.default.createElement(
											'a',
											{ href: this.props.profile.links.linkedin, target: '_blank' },
											_react2.default.createElement('i', { className: 'icon-linkedin' })
										)
									) : "",
									this.props.profile.links.website ? _react2.default.createElement(
										'li',
										null,
										_react2.default.createElement(
											'a',
											{ href: this.props.profile.links.website, target: '_blank' },
											_react2.default.createElement('i', { className: 'icon-website' })
										)
									) : ""
								)
							)
						) : ""
					)
				),
				_react2.default.createElement(_BackgroundTemplate2.default, { changeBannerImageWithTemplate: this.changeBannerImageWithTemplate.bind(this) }),
				_react2.default.createElement(
					'div',
					{ className: 'container' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col-md-4', style: { paddingRight: "0px" } },
							_react2.default.createElement(
								'div',
								{ className: 'box_style_detail', style: { marginTop: "16px" } },
								this.props.profile.jobs && this.props.profile.jobs.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Position2.default, formState),
									' ',
									_react2.default.createElement('hr', null)
								) : "",
								this.props.profile.employees && this.props.profile.formers && (this.props.profile.employees.length > 0 || this.props.profile.formers.length > 0) ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_People2.default, formState),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.editor || this.props.profile.culture ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Culture2.default, _extends({}, formState, { responsive: false })),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.editor || this.props.profile.benefits && this.props.profile.benefits.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Benefit2.default, _extends({}, formState, { responsive: false })),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.editor || this.props.profile.video ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Video2.default, _extends({}, formState, { responsive: false })),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.profile.key ? _react2.default.createElement(_Gallery2.default, _extends({ triggerButton: this.triggerButton.bind(this), feed: this.props.feed }, formState)) : "",
								this.props.editor || this.props.profile.address && this.props.profile.address.information ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Location2.default, _extends({}, formState, { responsive: false, triggerMap: this.triggerMap.bind(this), map_id: 'googleMap1' })),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.editor ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_SocialLink2.default, formState),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.profile.key && this.props.profile.review.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_SummaryReview2.default, formState),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.profile.key && this.props.profile.review.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_LastestReview2.default, formState),
									_react2.default.createElement('hr', null)
								) : ""
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'col-md-8' },
							_react2.default.createElement(
								'div',
								{ className: 'row' },
								_react2.default.createElement(
									'div',
									{ className: 'col-md-12' },
									_react2.default.createElement(
										'div',
										{ className: 'profile-card' },
										_react2.default.createElement(
											'div',
											{ className: 'row' },
											_react2.default.createElement(
												'div',
												{ className: 'col-md-12' },
												this.props.profile.jobs && this.props.profile.jobs.length > 0 ? _react2.default.createElement(_Position2.default, _extends({}, formState, { responsive: true })) : "",
												this.props.profile.employees && this.props.profile.formers && (this.props.profile.employees.length > 0 || this.props.profile.formers.length > 0) ? _react2.default.createElement(_People2.default, _extends({}, formState, { responsive: true })) : "",
												this.props.editor || this.props.profile.overview ? _react2.default.createElement(_Overview2.default, _extends({}, formState, { responsive: true })) : "",
												this.props.editor || this.props.profile.culture ? _react2.default.createElement(_Culture2.default, _extends({}, formState, { responsive: true })) : "",
												this.props.editor || this.props.profile.benefits && this.props.profile.benefits.length > 0 ? _react2.default.createElement(_Benefit2.default, _extends({}, formState, { responsive: true })) : "",
												_react2.default.createElement(
													'div',
													{ id: 'more-info-detail', style: { display: "none" } },
													this.props.editor || this.props.profile.video ? _react2.default.createElement(_Video2.default, _extends({}, formState, { responsive: true })) : "",
													this.props.profile.key ? _react2.default.createElement(_Gallery2.default, _extends({ triggerButton: this.triggerButton.bind(this), feed: this.props.feed }, formState, { responsive: true })) : "",
													this.props.editor || this.props.profile.address && this.props.profile.address.information ? _react2.default.createElement(_Location2.default, _extends({}, formState, { responsive: true, triggerMap: this.triggerMap.bind(this), map_id: 'googleMap2' })) : "",
													this.props.profile.key && this.props.profile.review.length > 0 ? _react2.default.createElement(_SummaryReview2.default, _extends({}, formState, { responsive: true })) : "",
													this.props.profile.key && this.props.profile.review.length > 0 ? _react2.default.createElement(_LastestReview2.default, _extends({}, formState, { responsive: true })) : ""
												),
												_react2.default.createElement(
													'div',
													{ className: 'profile-responsive', style: { marginBottom: "10px" } },
													_react2.default.createElement(
														'div',
														{ className: 'col-md-12' },
														_react2.default.createElement(
															'div',
															{ className: 'btn-cover', style: { margin: "20px 10px" } },
															_react2.default.createElement(
																'button',
																{ onClick: this.toggleMoreInfo.bind(this), className: 'btn btn-default', style: { width: "100%", margin: "auto" } },
																'More Information'
															)
														)
													)
												)
											)
										)
									)
								)
							),
							this.props.editor || this.props.profile.overview ? _react2.default.createElement(_Overview2.default, _extends({}, formState, { responsive: false })) : "",
							this.props.profile.key && (this.props.page !== 'company_image_profile' ? _react2.default.createElement(_NewsFeed2.default, { button_header_click: button_header_click, editor: this.props.editor, profile: this.props.profile, user: this.props.user, owner: !formState.other, feed: this.props.feed, feed_id: this.props.feed_id, language: this.props.lang }) : _react2.default.createElement(_Photo2.default, { button_header_click: button_header_click, editor: this.props.editor, profile: this.props.profile, user: this.props.user, owner: !formState.other, feed: this.props.feed, language: this.props.lang }))
						)
					)
				)
			);
		}
	}]);

	return CompanyProfile;
}(_react.Component);

exports.default = CompanyProfile;