'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _MessageActionCreators = require('../../actions/MessageActionCreators');

var _MessageActionCreators2 = _interopRequireDefault(_MessageActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var lastImageLength = 0;

var Message = function (_Component) {
  _inherits(Message, _Component);

  function Message() {
    _classCallCheck(this, Message);

    return _possibleConstructorReturn(this, (Message.__proto__ || Object.getPrototypeOf(Message)).apply(this, arguments));
  }

  _createClass(Message, [{
    key: 'updateDraft',
    value: function updateDraft(type, evt) {
      var value = evt.target.value;
      _MessageActionCreators2.default.setMessageDraft(type, value, this.props.profile_key);
    }
  }, {
    key: 'setFileType',
    value: function setFileType(type, evt) {
      $(this.refs.upload_file).data('name', type);
      $(this.refs.upload_file).click();
    }
  }, {
    key: 'sendMessage',
    value: function sendMessage(evt) {
      evt.preventDefault();
      this.refs.message_box.value = "";

      _MessageActionCreators2.default.sendMessage(this.props.user.token, this.props.msg.draft);
    }
  }, {
    key: 'sendFile',
    value: function sendFile(evt) {
      _MessageActionCreators2.default.sendFile(this.props.user.token, $(evt.target).data('name'), evt.target.files[0], this.props.profile_key);
    }
  }, {
    key: 'reloadMessage',
    value: function reloadMessage(evt) {
      _MessageActionCreators2.default.getMessageWithPeople(this.props.user.token, this.props.profile_key);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {

      if ($('.popup-photo').length > lastImageLength) {
        $('.popup-photo').magnificPopup({
          type: 'image'
        });
        lastImageLength = $('.popup-photo').length;
      }

      if ($(".mesg__chat")[0].scrollHeight > 0) $(".mesg__chat").scrollTop($(".mesg__chat")[0].scrollHeight);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.msg.uploading && this.props.msg.uploading) {
        _MessageActionCreators2.default.sendMessage(nextProps.user.token, nextProps.msg.draft);
      } else if (!nextProps.msg.sending && this.props.msg.sending) this.reloadMessage();

      return JSON.stringify(nextProps.msg) !== JSON.stringify(this.props.msg);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__message', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog', role: 'document' },
          _react2.default.createElement(
            'form',
            { onSubmit: this.sendMessage.bind(this) },
            _react2.default.createElement(
              'div',
              { className: 'modal-content' },
              _react2.default.createElement(
                'div',
                { className: 'modal-header' },
                _react2.default.createElement(
                  'div',
                  { className: 'modal-title' },
                  _react2.default.createElement(
                    'button',
                    { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                    _react2.default.createElement(
                      'span5',
                      { 'aria-hidden': 'true' },
                      '\xD7'
                    )
                  ),
                  _react2.default.createElement(
                    'b',
                    null,
                    _react2.default.createElement(
                      'span',
                      null,
                      'Send Message'
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal-body' },
                _react2.default.createElement(
                  'div',
                  { className: 'mesg__chat' },
                  this.props.msg.messages.length > 0 ? this.props.msg.messages.map(function (message, index) {
                    return _react2.default.createElement(
                      'div',
                      { key: index, className: _this2.props.user.key == message[0].owner ? "mesg--send" : "mesg--recieve" },
                      _react2.default.createElement(
                        'div',
                        { className: 'mesg__pic' },
                        _react2.default.createElement('img', { src: _this2.props.msg.users[index % 2].profile_image ? _this2.props.msg.users[index % 2].profile_image : "/images/avatar-image.jpg" })
                      ),
                      _react2.default.createElement(
                        'div',
                        { className: 'mesg_conversation' },
                        message.map(function (m, i) {
                          return _react2.default.createElement(
                            'div',
                            { key: i },
                            m.message_type == 'text' ? _react2.default.createElement(
                              'p',
                              null,
                              m.message
                            ) : "",
                            m.message_type == 'image' ? _react2.default.createElement(
                              'a',
                              { className: 'popup-photo', href: m.message.substring(6), style: { display: "block", marginBottom: "20px" } },
                              _react2.default.createElement('img', { src: m.message.substring(6), style: { maxWidth: "200px" } })
                            ) : "",
                            m.message_type == 'file' ? _react2.default.createElement(
                              'a',
                              { href: m.path ? m.path.substring(6) : m.message.substring(6), target: '_blank', download: true },
                              _react2.default.createElement(
                                'p',
                                null,
                                m.message
                              )
                            ) : ""
                          );
                        }),
                        _this2.props.msg.uploading && _react2.default.createElement(
                          'div',
                          null,
                          _react2.default.createElement(
                            'p',
                            null,
                            'Uploading...'
                          )
                        )
                      )
                    );
                  }) : ""
                ),
                _react2.default.createElement('textarea', { ref: 'message_box', onChange: this.updateDraft.bind(this, 'text'), rows: '1', placeholder: 'Type a message..' })
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal-footer' },
                _react2.default.createElement(
                  'div',
                  { className: 'mesg__function' },
                  _react2.default.createElement(
                    'a',
                    { href: '#' },
                    _react2.default.createElement(
                      'button',
                      { className: 'modal__btn--light', type: 'button', onClick: this.setFileType.bind(this, "image") },
                      _react2.default.createElement('i', { className: 'fa fa-picture-o', 'aria-hidden': 'true' })
                    )
                  ),
                  ' ',
                  _react2.default.createElement(
                    'a',
                    { href: '#' },
                    _react2.default.createElement(
                      'button',
                      { className: 'modal__btn--light', type: 'button', onClick: this.setFileType.bind(this, "file") },
                      _react2.default.createElement('i', { className: 'fa fa-paperclip', 'aria-hidden': 'true' })
                    )
                  ),
                  _react2.default.createElement('input', { ref: 'upload_file', onChange: this.sendFile.bind(this), type: 'file', 'data-name': 'file', style: { display: "none" } })
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement(
                    'a',
                    { className: 'hide-refresh', href: 'javascript:void(0)', onClick: this.reloadMessage.bind(this) },
                    'Refresh ',
                    _react2.default.createElement('i', { className: 'fa fa-refresh', 'aria-hidden': 'true', style: { fontSize: '16px' } })
                  ),
                  _react2.default.createElement(
                    'a',
                    { href: '#' },
                    _react2.default.createElement(
                      'button',
                      { className: 'modal__btn--light' },
                      'Send'
                    )
                  ),
                  _react2.default.createElement(
                    'a',
                    { className: 'hide-refresh__responsive', href: 'javascript:void(0)', onClick: this.reloadMessage.bind(this) },
                    'Refresh ',
                    _react2.default.createElement('i', { className: 'fa fa-refresh', 'aria-hidden': 'true', style: { fontSize: '16px' } })
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return Message;
}(_react.Component);

exports.default = Message;