'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Post = require('./Form/Feed/Post');

var _Post2 = _interopRequireDefault(_Post);

var _Card = require('./Form/Feed/Card');

var _Card2 = _interopRequireDefault(_Card);

var _FeedActionCreators = require('../../actions/FeedActionCreators');

var _FeedActionCreators2 = _interopRequireDefault(_FeedActionCreators);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var firstUser = false;

var BuzzFeed = function (_Component) {
  _inherits(BuzzFeed, _Component);

  function BuzzFeed() {
    _classCallCheck(this, BuzzFeed);

    return _possibleConstructorReturn(this, (BuzzFeed.__proto__ || Object.getPrototypeOf(BuzzFeed)).apply(this, arguments));
  }

  _createClass(BuzzFeed, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      firstUser = false;
      _FeedActionCreators2.default.getBuzzFeed();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (!firstUser && this.props.user.complete) {
        _FeedActionCreators2.default.setOwner(this.props.user);
        firstUser = true;
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _FeedActionCreators2.default.resetFeed();
    }
  }, {
    key: 'loadMore',
    value: function loadMore(evt) {
      _FeedActionCreators2.default.getBuzzFeed(this.props.feed.feeds.length / 10);
    }
  }, {
    key: 'searchBuzzFeed',
    value: function searchBuzzFeed(evt) {
      evt.preventDefault();
      _FeedActionCreators2.default.searchBuzzFeed(this.refs.buzz_search_desktop.value || this.refs.buzz_search_responsive.value);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.feed.deleting && this.props.feed.deleting) _FeedActionCreators2.default.getBuzzFeed();

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var buzzContent = _configLang2.default[this.props.language].buzz;

      return _react2.default.createElement(
        'div',
        { className: 'container', style: { marginBottom: "100px", paddingRight: "0px", paddingLeft: "0px" } },
        _react2.default.createElement('div', { className: 'desktop__search', style: { paddingTop: "80px" } }),
        _react2.default.createElement(
          'div',
          { className: 'suggest__responsive' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-12' },
            _react2.default.createElement(
              'form',
              { onSubmit: this.searchBuzzFeed.bind(this) },
              _react2.default.createElement(
                'div',
                { className: 'buzz__search', style: { marginTop: "70px", marginBottom: "30px", backgroundColor: "white", paddingBottom: "10px", boxShadow: "0 1px 4px -1px rgba(170,170,170,.1)", paddingTop: "20px", height: "80px", borderRadius: "4px" } },
                _react2.default.createElement('input', { ref: 'buzz_search_responsive', type: 'text', className: 'form-control form-control--mini', placeholder: buzzContent.search }),
                _react2.default.createElement(
                  'button',
                  { className: 'btn btn-default' },
                  _react2.default.createElement('i', { className: 'fa fa-search' })
                )
              )
            )
          ),
          typeof this.props.user.token !== 'undefined' ? _react2.default.createElement(
            'div',
            { className: 'col-md-12' },
            _react2.default.createElement(_Post2.default, { language: this.props.language, user: this.props.user, profile: this.props.user, feed: this.props.feed })
          ) : ""
        ),
        _react2.default.createElement(
          'div',
          { className: 'col-md-6 buzz__newsfeed left' },
          _react2.default.createElement(
            'div',
            { className: 'newsfeed' },
            typeof this.props.user.token !== 'undefined' ? _react2.default.createElement(
              'div',
              { className: 'desktop__search' },
              _react2.default.createElement(_Post2.default, { language: this.props.language, user: this.props.user, profile: this.props.user, feed: this.props.feed })
            ) : "",
            this.props.feed.feeds.map(function (b, index) {
              return index < _this2.props.feed.feeds.length / 2 ? _react2.default.createElement(_Card2.default, { language: _this2.props.language, key: index, user: _this2.props.user, feed: b, index: index, comment_id: _this2.props.feed.comment_for_reply_id, buzz: true }) : "";
            })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'col-md-6 buzz__newsfeed right' },
          _react2.default.createElement(
            'div',
            { className: 'newsfeed' },
            _react2.default.createElement(
              'form',
              { onSubmit: this.searchBuzzFeed.bind(this) },
              _react2.default.createElement(
                'div',
                { className: 'buzz__search desktop__search', style: { backgroundColor: "white", paddingBottom: "10px", boxShadow: "0 1px 4px -1px rgba(170,170,170,.1)", paddingTop: "20px", height: "80px", borderRadius: "4px" } },
                _react2.default.createElement('input', { ref: 'buzz_search_desktop', type: 'text', className: 'form-control form-control--mini', placeholder: buzzContent.search }),
                _react2.default.createElement(
                  'button',
                  { className: 'btn btn-default' },
                  _react2.default.createElement('i', { className: 'fa fa-search' })
                )
              )
            ),
            this.props.feed.feeds.map(function (b, index) {
              return index >= _this2.props.feed.feeds.length / 2 ? _react2.default.createElement(_Card2.default, { language: _this2.props.language, key: index, user: _this2.props.user, feed: b, index: index, comment_id: _this2.props.feed.comment_for_reply_id, buzz: true }) : "";
            })
          )
        ),
        this.props.feed.load_more ? _react2.default.createElement(
          'div',
          { className: 'col-md-12' },
          _react2.default.createElement(
            'div',
            { className: 'btn__seemore' },
            _react2.default.createElement(
              'button',
              { onClick: this.loadMore.bind(this), className: 'btn btn-default' },
              this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
            )
          )
        ) : ""
      );
    }
  }]);

  return BuzzFeed;
}(_react.Component);

exports.default = BuzzFeed;