"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Aboutus = function (_Component) {
  _inherits(Aboutus, _Component);

  function Aboutus() {
    _classCallCheck(this, Aboutus);

    return _possibleConstructorReturn(this, (Aboutus.__proto__ || Object.getPrototypeOf(Aboutus)).apply(this, arguments));
  }

  _createClass(Aboutus, [{
    key: "render",
    value: function render() {
      return _react2.default.createElement(
        "div",
        null,
        _react2.default.createElement(
          "div",
          { className: "cover cover__aboutus", style: { background: "url('/images/aboutus.jpg') no-repeat center center", backgroundSize: "cover" } },
          _react2.default.createElement(
            "div",
            { className: "cover__text" },
            _react2.default.createElement(
              "div",
              { className: "cover__text--center" },
              _react2.default.createElement(
                "h1",
                null,
                _react2.default.createElement(
                  "span2",
                  null,
                  this.props.aboutus.header
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          "div",
          null,
          _react2.default.createElement(
            "div",
            { className: "whatweoffer" },
            _react2.default.createElement(
              "div",
              { className: "container container__aboutme" },
              _react2.default.createElement(
                "div",
                { className: "text--center" },
                _react2.default.createElement("h2", { dangerouslySetInnerHTML: { __html: this.props.aboutus.vision.header } }),
                _react2.default.createElement("hr", { style: { width: "50%" } }),
                _react2.default.createElement(
                  "div",
                  { className: "row" },
                  _react2.default.createElement(
                    "div",
                    { className: "col-md-12" },
                    _react2.default.createElement(
                      "div",
                      { className: "whysplitshift__text" },
                      this.props.aboutus.vision.detail
                    )
                  )
                )
              )
            )
          ),
          _react2.default.createElement(
            "div",
            { className: "whatweoffer" },
            _react2.default.createElement(
              "div",
              { className: "container container__aboutme" },
              _react2.default.createElement(
                "div",
                { className: "text--center" },
                _react2.default.createElement("h2", { dangerouslySetInnerHTML: { __html: this.props.aboutus.whatis.header } }),
                _react2.default.createElement("hr", { style: { width: "50%" } }),
                _react2.default.createElement(
                  "div",
                  { className: "row" },
                  _react2.default.createElement(
                    "div",
                    { className: "col-md-12" },
                    _react2.default.createElement(
                      "div",
                      { className: "whysplitshift__text" },
                      this.props.aboutus.whatis.detail
                    )
                  )
                )
              )
            )
          ),
          _react2.default.createElement(
            "div",
            { className: "whatweoffer" },
            _react2.default.createElement(
              "div",
              { className: "container container__aboutme" },
              _react2.default.createElement(
                "div",
                { className: "text--center" },
                _react2.default.createElement("h2", { dangerouslySetInnerHTML: { __html: this.props.aboutus.why.header } }),
                _react2.default.createElement("hr", { style: { width: "50%" } }),
                _react2.default.createElement(
                  "div",
                  { className: "row" },
                  _react2.default.createElement(
                    "div",
                    { className: "col-md-12" },
                    _react2.default.createElement(
                      "div",
                      { className: "whysplitshift__text" },
                      this.props.aboutus.why.detail.map(function (d, index) {
                        return _react2.default.createElement(
                          "p",
                          { key: index },
                          d
                        );
                      })
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return Aboutus;
}(_react.Component);

exports.default = Aboutus;