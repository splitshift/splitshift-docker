'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Privacy = function (_Component) {
  _inherits(Privacy, _Component);

  function Privacy() {
    _classCallCheck(this, Privacy);

    return _possibleConstructorReturn(this, (Privacy.__proto__ || Object.getPrototypeOf(Privacy)).apply(this, arguments));
  }

  _createClass(Privacy, [{
    key: 'render',
    value: function render() {

      var policyList = [['1. ACCESS TO AND USE OF THE SITE AND THE SERVICES', 'You represent and warrant that you are fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in this Agreement. By using the Service you agree that you are thirteen (13) years of age or older. If your access or use of the Service is prohibited or restricted in any way by the laws, regulations or other governmental requirements of the jurisdiction in which you live, or if, for any reason and at any time, you do not agree with this Privacy Policy, please discontinue using our Service immediately. If you are under 13 years of age, then please do not use the Site or Service.'], ['2. COLLECTION OF PERSONAL INFORMATION', 'You acknowledge that Splitshift may collect and process “personal information” (i.e. information that could be used to contact you, such as full name, postal address, user name and password, phone number or e-mail address) or “demographic and usage information” (i.e. information that you submit, or that we collect, that is not personal information but necessary for the proper functioning of our Service, such as the date regarding the start and end and the extent of your usage of the Service and your personal interests and preferences), in connection with the Service. Such information collected by Splitshift may be stored and processed in Thailand or any other country in which Splitshfit maintains facilities. By using the Service, you consent to any such transfer of information outside of your country. Splitshift makes no representation or warranty with respect to any duty to permanently store any personal information you may provide. By using our Services and providing us with personal information, you waive any claims that may arise under your own or any other local or national laws, rules or regulations or international treaties. We may from time to time, transfer or merge your personal information collected off-line to our online databases or store off-line information in an electronic format. We may also combine personal information we collect online with information available from other sources, including information received from our advertisers and promotional partners. This Privacy Policy applies to your personal information for as long as your personal information is in our possession, even if you terminate or discontinue your use of the Service.'], ['3.  CHANGE OR UPDATE REGISTRATION DATA', 'You may request a change to or update the Registration Data you provide Splitshift by changing your profile in the “Edit Profile” section of your personal page. It is your responsibility to maintain and promptly update the Registration Data to keep it true, accurate, current and complete.'], ['4.  USE OF PERSONAL INFORMATION', 'Splitshift uses your personal information to identify you and, in some cases, to improve and customize the content you see on the Site. Your personal information may also be aggregated and used by Splitshift for market research, project planning, troubleshooting problems, detecting and protecting against error, fraud or other criminal activity, to enforce the Terms of Use or any applicable Rules, for other internal purposes, and as otherwise described to you at the time of collection. Splitshift will obtain your consent for any use of your personal information beyond those set forth in this Privacy Policy and the original uses for which you provided your consent. \
        You agree that Splitshift may use personal information about you to analyze Site usage, improve or customize Splitshift’s Services, customize the Site’s content, and as otherwise set forth in this Privacy Policy. You also agree that Splitshift may use your personal information to contact you. Unless you specifically opt out, you also agree that Splitshift may use your personal information to contact you and deliver information to you that, in some cases, is targeted to your interests, such as targeted banner advertisements, administrative notices, product offerings, and communications relevant to your use of the Site or the Services. By accepting the Terms of Use and this Privacy Policy, you expressly agree to receive this information, unless you specifically opt out in applicable circumstances. \
        Any personal information or content that you voluntarily disclose online (on discussion boards, in public comments, within your public profile page, etc.) may become publicly available and may be collected and used by others. Your username and/or your email address may be displayed to other Users when you upload content or send messages through the Site. Any content that you submit to the Site may be redistributed through the Internet and other media channels, and may be viewed by the general public.'], ['5. DISCLOSURE OF PERSONAL INFORMATION', "Except as expressly provided herein, absent your prior consent, Splitshift does not share your personal information with any unrelated third parties. Additionally, Splitshift does not permit unrelated third parties to perform “reverse searches” whereby they could identify users from email addresses. In addition, the following describes some of the ways that your personal information may be disclosed in the normal scope of business to provide you with the Services: \
        Public Profile and Comments. Splitshift may post certain information as a part of your Public Profile and Comments, which will be viewable by other Users and visitors to the site, including but not limited to your username and location. Splitshift will not disclose your home address, email address, phone number in connection with your Public Profile. Messages. Splitshit will not disclose your private messages. Messages are only shared among those who are involved in each conversation. \
        Notwithstanding the foregoing, Splitshift reserves the right, and you authorize Splitshift, to share or disclose your personal information, including, without limitation, any content, records or electronic communications of any kind, when Splitshift determines, in its sole discretion, that the disclosure of your personally identifiable information is necessary to identify, contact, or bring legal action against you if: \
        you are or may be violating the Terms of Use, this Privacy Policy or any applicable Rules; \
        you are interfering with Splitshift’s or a third party's rights or property; \
        you are violating any applicable law, rule or regulation; \
        necessary or required by any applicable law, rule or regulation; and/or \
        requested by governmental authorities in the event of any investigation, act of terrorism or instance of local, regional or national emergency. \
        In addition, Splitshfit cannot ensure that all of your private communications and other personal information will never be disclosed in ways not otherwise described in this Privacy Policy. By way of example, without limiting the foregoing, Splitshift may be forced to disclose personal information to the government or third parties under certain circumstances or third parties may unlawfully intercept or access your transmissions or communications. Therefore, although Splitshift uses industry standard practices to protect your privacy, Splitshift does not promise, and you should not expect, that your personally identifiable information or private communications will always remain private. However, Splitshift shall use reasonable commercial and legal efforts to protect its users’ information and to maintain the privacy and anonymity of users. \
        In the event of a merger, acquisition, sale of substantially all of its assets or change of control of Splitshift, personal information collected by Splitshift about you will be considered a business asset and will be transferred to the applicable new entity."], ['6. NEWSLETTERS, ETC.', 'Splitshift maintains a strict “no-spam” policy. If you register for the Service, you may receive Splitshift’s newsletter, notices and/or special offers via email, unless you have requested otherwise at the time of registration or as set forth below. \
        You may unsubscribe from Splitshift’s electronic newsletters, notices and/or special offers at any time by following the instructions contained at the end of every such email, or if a third-party vendor provides Splitshift’s newsletter, in accordance with the instructions provided by such third party. While all efforts are made to insure this information is viewable in all email browsers, there may be cases where certain email browsers do not display the unsubscribe information correctly. If you are having problems unsubscribing please forward the entire newsletter here [<a href="mailto:info@splitshift.com">info@splitshift.com</a>] with the word “UNSUBSCRIBE” in the Subject line and you will be removed within seven (7) business days. Please note that direct replies to any newsletter are processed by computer and are not always viewed by a human.'], ['7. COOKIES', '“Cookies” are alphanumeric identifiers in the form of text files that are inserted and stored by your web browser on your computer’s hard drive. Splitshift and third party advertisers may set and access cookies on your computer to track and store preferential information about you. Splitshift and third party advertisers gather anonymous information about their users through cookie technology on an aggregate level only. Such aggregated information is used within Splitshift’s internal organization and is only shared with third party advertisers on an aggregated and non-personally identifiable basis.'], ['8. WEB BEACONS', 'Web Beacons, also known as pixel tags and clear GIFs, (“Web Beacons”), are electronic images that allow a website to access cookies and help track marketing campaigns and general usage patterns of visitors to those websites. Web Beacons can recognize certain types of information, such as cookie numbers, time and date of a page view and a description of the page where the Web Beacons are placed. No personally identifiable information about you is shared with third parties through the use of Web Beacons on the Site. However, through Web Beacons, we may collect general information that will not personally identify you, such as: Internet browser, operating system, IP address, date of visit, time of visit and path taken through the Site. \
        Internal use of Web Beacons. Splitshift may use Web Beacons internally to count visitors and recognize visitors through cookies. Access to cookies helps Splitshift personalize your experience when you visit the Site. \
        Email. Splitshift may include Web Beacons in HTML-formatted email messages that Splitshift sends to you. Web Beacons in email messages help Splitshift determine your interest in and the effectiveness of such emails. \
        External use of Web Beacons. Splitshift may also use Web Beacons externally. For example, Splitshift may report aggregate information about visitors, such as demographic and usage information, to its affiliates and other third parties. Splitshift may also include other company’s Web Beacons within the Site.'], ['9. SECURITY', 'Personal information you submit to Splitshift is password protected for your privacy and security. In the unlikely event that an unauthorized third party compromises Splitshift’s security measures, Splitshift will not be responsible for any damages directly or indirectly caused by an unauthorized third party’s ability to view, use or disseminate such information.'], ['10. LINKS TO THIRD PARTIES', 'The Site may include links to third party websites. Such links are not an endorsement by Splitshift of those websites and/or the products or services offered on those websites. Third party websites may have different privacy policies, and Splitshift is not responsible for the privacy practices of those third party websites. If you click on a link to a third party website, Splitshift encourages you to check the privacy policy of that website, as it may differ substantially from that of this Privacy Policy. Splitshift makes no representations or warranties nor is Splitshift responsible for the privacy policies of any websites other than the Site. If you decide to access any third party links appearing on the Site, you do so at your own risk.'], ['11. TRANSMISSIONS, SUBMISSIONS AND POSTINGS', 'Splitshift welcomes the submission of comments, information and feedback regarding the Site and the Services. However, Splitshift does not want to receive certain kinds of information from you, including any confidential or proprietary information, without prior written consent of Splitshift. Although Splitshift does not regularly review your transmissions, submissions or postings, Splitshift reserves the right (but not the obligation), at its sole discretion and at any time, to edit, refuse to post or remove your transmissions, submissions or postings. You hereby acknowledge and agree that Splitshift may review transmissions, submissions or postings made by you to determine, in its sole discretion, your compliance with the Terms of Use Data Policy and Privacy Policy and that Splitshift shall have the unrestricted right to use such information for any purpose and without compensation to you.'], ['12. OTHER TERMS', 'To review other applicable terms and conditions that apply to this Privacy Policy, including, without limitation, intellectual property rights, disclaimer of warranties, limitation of liability and resolving disputes, please review the Terms of Use [<a href="/user-agreement">http://splitshift.com/user-agreement</a>].'], ['13. CHANGES TO THIS PRIVACY POLICY', 'Splitshift reserves the right, at its sole discretion, to update, amend and/or change this Privacy Policy without prior notice and at any time. Updates to this Privacy Policy will be posted here. You are encouraged to revisit this Privacy Policy from time to time in order to review any changes that have been made. Your continued use of the Site and any Services following the posting of any such changes shall automatically be deemed your acceptance of all changes.'], ['14. CHILDRENS\' PRIVACY', 'If you are under 13 years of age, then please do not use or access the Site at any time or in any manner. If Splitshift learns that personally identifiable information of persons under 13 years of age has been collected on the Site without verified parental consent, then Splitshift will take the appropriate steps to delete this information.'], ['15. SPECIAL NOTE TO INTERNATIONAL USERS', 'The Site is hosted in Thailand. If you are a User accessing the Site from the United States, the European Union or any other region with laws or regulations governing personal data collection, use, and disclosure, that differ from Thai laws, please be advised that through your continued use of the Site, which is governed by Thai law, this Privacy Policy, and our Terms of Use [http://splitshift.com/agreement], you are transferring your personal information to Thailand and you consent to that transfer'], ['16. IN THE EVENT OF MERGER, SALE, OR BANKRUPTCY', "In the event that Splitshift is acquired by or merged with a third party entity, or sells all or substantially all of its assets, we reserve the right, in any of these circumstances, to transfer or assign the information and content we have received and collected from our Users and Members as part of such merger, acquisition, sale, or other change of control. In the unlikely event of our bankruptcy, insolvency, reorganization, receivership, or assignment for the benefit of creditors, or the application of laws or equitable principles affecting creditors' rights generally, we may not be able to control how your personal information is treated, transferred, or used."], ['17. QUESTIONS OR CONCERNS', 'If you have questions, comments, concerns or feedback regarding this Privacy Policy, please contact us via the email set forth below: \
        <a href="mailto:info@splitshift.com">info@splitshift.com</a>']];

      return _react2.default.createElement(
        'div',
        { style: { marginTop: "100px" } },
        _react2.default.createElement(
          'div',
          { className: 'whatweoffer' },
          _react2.default.createElement(
            'div',
            { className: 'container container__privacy' },
            _react2.default.createElement(
              'h2',
              null,
              'Privacy Policy'
            ),
            _react2.default.createElement(
              'p',
              null,
              'Splitshift.com (\u201CSplitshift\u201D) respects your privacy, is seriously committed to protecting the privacy of its users (\u201CUsers\u201D) and has published this Privacy Policy to explain how Splitshift treats information you submit to Splitshift. Please read this Privacy Policy carefully prior to your access to and/or use of\xA0www.splitshift.com\xA0(the \u201CSite\u201D) or your registration for or use of the Services (as defined in Statement of Rights and Responsibilities \u2013 Terms of Use (the \u201CTerms of Use\u201D [http://splitshift.com/agreement]), incorporated herein by reference). If you do not agree to abide by this Privacy Policy, please do not access or use the Site or register as a Member of the Site or Services. By submitting information to Splitshift, either by using the Site or registering for or using the Services, you are accepting the practices described in this Privacy Policy and the Terms of Use. This Privacy Policy is governed by the Terms of Use, which includes all disclaimers of warranties and limitation of liabilities. All capitalized terms not defined in this Privacy Policy will have the meaning set forth in the Terms of Use.'
            ),
            policyList.map(function (policy, index) {
              return _react2.default.createElement(
                'div',
                { key: index },
                _react2.default.createElement(
                  'h4',
                  null,
                  policy[0]
                ),
                _react2.default.createElement('p', { dangerouslySetInnerHTML: { __html: policy[1] } })
              );
            }),
            '\xA0',
            _react2.default.createElement(
              'h2',
              null,
              'Data Policy'
            ),
            _react2.default.createElement(
              'p',
              null,
              'We give you the power to share as part of our mission to make the world more open and connected. This policy describes what information we collect and how it is used and shared. As you review our policy, keep in mind that it applies to all Splitshift brands, products and services that do not have a separate privacy policy or that link to this policy, which we call the\xA0\u201CSplitshift Services\u201D or \u201CServices\u201D.'
            ),
            _react2.default.createElement(
              'h4',
              null,
              'What kinds of information do we collect?'
            ),
            _react2.default.createElement(
              'p',
              null,
              'Depending on which Services you use, we collect different kinds of information from or about you.'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Things you do and information you provide.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We collect the content and other information you provide when you use our Services, including when you sign up for an account, create or share, and message or communicate with others. This can include information in or about the content you provide, such as the location of a photo or the date a file was created. We also collect information about how you use our Services, such as the types of content you view or engage with or the frequency and duration of your activities.'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Things others do and information they provide.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We also collect content and information that other people provide when they use our Services, including information about you, such as when they share a photo of you, send a message to you, or upload, sync or import your contact information. Your networks and connections. We collect information about the people you are connected to and how you interact with them, such as the people you communicate with the most or you like to share with. We also collect contact information you provide if you upload, sync or import this information (such as an address book) from a device.'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Device information.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We collect information from or about the computers, phones, or other devices where you install or access our Services, depending on the permissions you\u2019ve granted. We may associate the information we collect from your different devices, which helps us provide consistent Services across your devices. Here are some examples of the device information we collect:\xA0',
              _react2.default.createElement(
                'ul',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'Attributes such as the operating system, hardware version, device settings, file and software names and types, battery and signal strength, and device identifiers.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Device locations, including specific geographic locations, such as through GPS, Bluetooth, or WiFi signals.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Connection information such as the name of your mobile operator or ISP, browser type, language and time zone, mobile phone number and IP address.'
                )
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Information from websites and apps that use our Services.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We collect information when you visit or use third-party websites and apps that use our Services (like when they offer our Like button or Splitshift Log In or use our measurement and advertising services). This includes information about the websites and apps you visit, your use of our Services on those websites and apps, as well as information the developer or publisher of the app or website provides to you or us.'
            ),
            _react2.default.createElement(
              'h4',
              null,
              'How do we use this information?'
            ),
            _react2.default.createElement(
              'p',
              null,
              'We are passionate about creating engaging and customized experiences for people. We use all of the information we have to help us provide and support our Services. Here\u2019s how:'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Provide, improve and develop Services.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We are able to deliver our Services, personalized content, and make suggestions for you by using this information to understand how you use and interact with our Services and the people or things you\u2019re connected to and interested in on and off our Services.\xA0\u2028\u2028We conduct surveys and\xA0research, test features in development, and analyze the information we have to evaluate and improve products and services, develop new products or features, and conduct audits and troubleshooting activities.'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Communicate with you.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We use your information to send you marketing communications, communicate with you about our Services and let you know about our policies and terms. We also use your information to respond to you when you contact us.'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Promote safety and security.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We use the information we have to help verify accounts and activity, and to promote safety and security on and off of our Services, such as by investigating suspicious activity or violations of our terms or policies. We work hard to protect your account using teams of engineers, automated systems, and advanced technology such as encryption and machine learning. We also offer easy-to-use security tools that add an extra layer of security to your account.'
            ),
            _react2.default.createElement(
              'p',
              null,
              'We use cookies and similar technologies to provide and support our Services and each of the uses outlined and described in this section of our policy. Read our Privacy Policy to learn more.'
            ),
            _react2.default.createElement(
              'h4',
              null,
              'How is this information shared?'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Sharing On Our Services'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'People use our Services to connect and share with others. We make this possible by sharing your information in the following ways:'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'People you share and communicate with.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'When you share and communicate using our Services, you choose the audience who can see what you share. For example, when you post on Splitshift, you select the audience for the post, such as all of your Connections.'
            ),
            _react2.default.createElement(
              'p',
              null,
              'Public information\xA0is any information you share with a public audience, as well as information in your\xA0Public Profile, or content you share on a Splitshift Page or another public forum. Public information is available to anyone on or off our Services and can be seen or accessed through online search engines, APIs, and offline media, such as on TV.\u2028\u2028In some cases, people you share and communicate with may download or re-share this content with others on and off our Services. When you comment on another person\u2019s post or like their content on Splitshift, that person decides the audience who can see your comment or like. If their audience is public, your comment will also be public.'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'People that see content others share about you.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'Other people may use our Services to share content about you with the audience they choose. For example, people may share a photo of you or share information about you that you shared with them. If you have concerns with someone\'s post, please report them via email: ',
              _react2.default.createElement(
                'a',
                { href: 'mailto:support@splitshift.com' },
                'support@splitshift.com.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Apps, websites and third-party integrations on or using our Services.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'When you use third-party apps, websites or other services that use, or are integrated with, our Services, they may receive information about what you post or share. For example, when you play a game with your Splitshift friends or use the Splitshift Comment or Share button on a website, the game developer or website may get information about your activities in the game or receive a comment or link that you share from their website on Splitshift. In addition, when you download or use such third-party services, they can access your\xA0Public Profile, which includes your\xA0username or user ID, your age range and country/language, your list of friends, as well as any information that you share with them. Information collected by these apps, websites or integrated services is subject to their own terms and policies.\xA0'
            ),
            '\u2028            ',
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Sharing within Splitshift companies.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We share information we have about you within the family of companies that are part of Splitshift.\xA0'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'New owner.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'If the ownership or control of all or part of our Services or their assets changes, we may transfer your information to the new owner.\xA0'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Sharing With Third-Party Partners and Customers'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We work with third party companies who help us provide and improve our Services or related products, which makes it possible to operate our companies and provide free services to people around the world.\xA0\u2028\u2028Here are the types of third parties we can share information with about you:'
            ),
            _react2.default.createElement(
              'p',
              null,
              'Advertising, Measurement and Analytics Services (Non-Personally Identifiable Information Only).'
            ),
            _react2.default.createElement(
              'p',
              null,
              _react2.default.createElement(
                'b',
                null,
                'Vendors, service providers and other partners.'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              'We transfer information to vendors, service providers, and other partners who globally support our business, such as providing technical infrastructure services, analyzing how our Services are used, measuring the effectiveness of services, providing customer service, or conducting academic research and surveys. These partners must adhere to strict confidentiality obligations in a way that is consistent with this Data Policy and the agreements we enter into with them.'
            ),
            _react2.default.createElement(
              'h4',
              null,
              'How can I manage or delete information about me?'
            ),
            _react2.default.createElement(
              'p',
              null,
              'We store data for as long as it is necessary to provide products and services to you and others, including those described above. Information associated with your account will be kept until your account is deleted, unless we no longer need the data to provide products and services.\xA0\u2028\u2028You can delete your account any time. When you delete your account, we delete things you have posted, such as your photos and status updates. If you do not want to delete your account, but want to temporarily stop using Splitshift, you may deactivate your account instead. Keep in mind that information that others have shared about you is not part of your account and will not be deleted when you delete your account.'
            ),
            _react2.default.createElement(
              'h4',
              null,
              'How do we respond to legal requests or prevent harm?'
            ),
            _react2.default.createElement(
              'p',
              null,
              'We may access, preserve and share your information in response to a legal request (like a search warrant, court order or subpoena) if we have a good faith belief that the law requires us to do so. This may include responding to legal requests from jurisdictions outside of Thailand where we have a good faith belief that the response is required by law in that jurisdiction, affects users in that jurisdiction, and is consistent with internationally recognized standards. We may also access, preserve and share information when we have a good faith belief it is necessary to: detect, prevent and address fraud and other illegal activities; to protect ourselves, you and others, including as part of investigations; or to prevent death or imminent bodily harm. For example, we may provide information to third-party partners about the reliability of your account to prevent fraud and abuse on and off of our Services. Information we receive about you, including financial transaction data related to purchases made with Splitshift, may be accessed, processed and retained for an extended period of time when it is the subject of a legal request or obligation, governmental investigation, or investigations concerning possible violations of our terms or policies, or otherwise to prevent harm. We also may retain information from accounts disabled for violations of our terms for at least a year to prevent repeat abuse or other violations of our terms.'
            ),
            _react2.default.createElement(
              'h4',
              null,
              'How will we notify you of changes to this policy?'
            ),
            _react2.default.createElement(
              'p',
              null,
              'We\u2019ll notify you before we make changes to this policy and give you the opportunity to review and comment on the revised policy before continuing to use our Services.'
            ),
            _react2.default.createElement(
              'h4',
              null,
              'How to contact Splitshift with questions'
            ),
            _react2.default.createElement(
              'p',
              null,
              'If you have questions about this policy, please contact us under ',
              _react2.default.createElement(
                'a',
                { href: 'mailto:info@splitshift.com' },
                'info@splitshift.com.'
              )
            )
          )
        )
      );
    }
  }]);

  return Privacy;
}(_react.Component);

exports.default = Privacy;