'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _SearchActionCreators = require('../../actions/SearchActionCreators');

var _SearchActionCreators2 = _interopRequireDefault(_SearchActionCreators);

var _ConfigData = require('../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PeopleSearch = function (_Component) {
  _inherits(PeopleSearch, _Component);

  function PeopleSearch() {
    _classCallCheck(this, PeopleSearch);

    return _possibleConstructorReturn(this, (PeopleSearch.__proto__ || Object.getPrototypeOf(PeopleSearch)).apply(this, arguments));
  }

  _createClass(PeopleSearch, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.ad__search').click(function () {
        $('.jobbar__detail').css('display', 'block');
        $(this).css('display', 'none');
      });

      _SearchActionCreators2.default.clearFilter();
      _SearchActionCreators2.default.People({});
    }
  }, {
    key: 'updateFilter',
    value: function updateFilter(name, evt) {
      var response = {};
      response[name] = evt.target.value;
      _SearchActionCreators2.default.updateFilter(response);
    }
  }, {
    key: 'updateAdvanceFilter',
    value: function updateAdvanceFilter(name, value, evt) {
      var response = {};

      if (value == 'All') value = "";

      response[name] = value;
      _SearchActionCreators2.default.updateFilter(response);
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {

      evt.preventDefault();

      $('html, body').animate({
        scrollTop: $(this.refs.jobs__people).offset().top - 200
      }, 1000);

      _SearchActionCreators2.default.People(this.props.search.filter);
    }
  }, {
    key: 'loadMore',
    value: function loadMore(evt) {
      _SearchActionCreators2.default.People(this.props.search.filter, this.props.search.people.length / 15);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var content = _configLang2.default[this.props.language].people_search;
      var advancedContent = _configLang2.default[this.props.language].advanced_search;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: 'container' },
          _react2.default.createElement(
            'div',
            { className: 'header__search people-search' },
            _react2.default.createElement(
              'div',
              { className: 'people__title' },
              _react2.default.createElement(
                'p',
                null,
                _react2.default.createElement(
                  'span',
                  null,
                  content.intro
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'job__search' },
              _react2.default.createElement(
                'form',
                { onSubmit: this.onSearch.bind(this) },
                _react2.default.createElement(
                  'div',
                  { className: 'job__bar' },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'job' },
                    content.people[0]
                  ),
                  _react2.default.createElement('input', { id: 'job', type: 'text', className: 'form-control', onChange: this.updateFilter.bind(this, 'keyword'), placeholder: content.people[1] })
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'location__bar' },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'location' },
                    content.location[0]
                  ),
                  _react2.default.createElement('input', { id: 'location', type: 'text', onChange: this.updateFilter.bind(this, 'location'), className: 'form-control', placeholder: content.location[1] }),
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default ad__search ad__search--desktop', type: 'button', onClick: _JQueryTool2.default.showAdvanceSearch.bind(this) },
                    advancedContent.title
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'job__btn' },
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default' },
                    _react2.default.createElement('i', { className: 'fa fa-search' })
                  ),
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default ad__search ad__search--responsive', type: 'button', onClick: _JQueryTool2.default.showAdvanceSearch.bind(this) },
                    advancedContent.title
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'jobbar__detail' },
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.interest ? advancedContent.interest[_ConfigData2.default.InterestList().indexOf(this.props.search.filter.interest)] : advancedContent.interest_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              _ConfigData2.default.InterestList().map(function (interest, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'interest', interest) },
                    advancedContent.interest[index]
                  )
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'btn-group' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'btn btn-default dropdown-toggle', 'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false' },
              this.props.search.filter.work_exp ? advancedContent.workexp[this.props.search.filter.work_exp] : advancedContent.workexp_title,
              ' ',
              _react2.default.createElement('i', { className: 'fa fa-caret-down' })
            ),
            _react2.default.createElement(
              'ul',
              { className: 'dropdown-menu' },
              Object.keys(_ConfigData2.default.WorkExperienceList()).map(function (work, index) {
                return _react2.default.createElement(
                  'li',
                  { key: index },
                  _react2.default.createElement(
                    'a',
                    { href: '#', onClick: _this2.updateAdvanceFilter.bind(_this2, 'work_exp', work) },
                    advancedContent.workexp[work]
                  )
                );
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'container', style: { minHeight: "500px" } },
          _react2.default.createElement(
            'div',
            { className: 'text--center' },
            _react2.default.createElement(
              'h3',
              null,
              _react2.default.createElement(
                'b',
                null,
                _react2.default.createElement(
                  'span5',
                  null,
                  content.header
                )
              )
            )
          ),
          _react2.default.createElement('hr', { className: 'hr-title' }),
          _react2.default.createElement(
            'div',
            { ref: 'jobs__people', className: 'jobs__people' },
            this.props.search.people.map(function (p, index) {
              return _react2.default.createElement(
                _reactRouter.Link,
                { key: index, to: '/' + _this2.props.language + '/user/' + p.key },
                _react2.default.createElement(
                  'div',
                  { className: 'company__item' },
                  _react2.default.createElement(
                    'div',
                    { className: 'job__pic' },
                    _react2.default.createElement('img', { src: p.profile_image ? p.profile_image : "/images/avatar.jpg" })
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'search__detail' },
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement(
                        'b',
                        null,
                        _react2.default.createElement(
                          'span',
                          null,
                          p.profile.first_name + ' ' + p.profile.last_name
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement(
                        'span5',
                        null,
                        p.profile.occupation.position ? p.profile.occupation.position : " "
                      )
                    ),
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement(
                        'span6',
                        null,
                        p.profile.occupation.workplace ? p.profile.occupation.workplace : " "
                      )
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'search__btn' },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/th/user/' + p.key },
                      _react2.default.createElement(
                        'button',
                        { className: 'btn btn-default' },
                        content.see_profile
                      )
                    )
                  )
                )
              );
            }),
            _react2.default.createElement(
              'div',
              { className: 'btn__seemore' },
              this.props.search.load_more && _react2.default.createElement(
                'button',
                { onClick: this.loadMore.bind(this), className: 'btn btn-default' },
                this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
              )
            )
          )
        )
      );
    }
  }]);

  return PeopleSearch;
}(_react.Component);

exports.default = PeopleSearch;