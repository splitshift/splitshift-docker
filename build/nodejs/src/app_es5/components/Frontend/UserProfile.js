'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('flux/utils');

var _reactRouter = require('react-router');

var _ProfileCard = require('./Form/User/ProfileCard');

var _ProfileCard2 = _interopRequireDefault(_ProfileCard);

var _About = require('./Form/User/About');

var _About2 = _interopRequireDefault(_About);

var _Interest = require('./Form/User/Interest');

var _Interest2 = _interopRequireDefault(_Interest);

var _WorkExperience = require('./Form/User/WorkExperience');

var _WorkExperience2 = _interopRequireDefault(_WorkExperience);

var _Skill = require('./Form/User/Skill');

var _Skill2 = _interopRequireDefault(_Skill);

var _Education = require('./Form/User/Education');

var _Education2 = _interopRequireDefault(_Education);

var _Course = require('./Form/User/Course');

var _Course2 = _interopRequireDefault(_Course);

var _Following = require('./Form/User/Following');

var _Following2 = _interopRequireDefault(_Following);

var _Language = require('./Form/User/Language');

var _Language2 = _interopRequireDefault(_Language);

var _NewsFeed = require('./Form/Feed/NewsFeed');

var _NewsFeed2 = _interopRequireDefault(_NewsFeed);

var _Photo = require('./Form/Feed/Photo');

var _Photo2 = _interopRequireDefault(_Photo);

var _BackgroundTemplate = require('./BackgroundTemplate');

var _BackgroundTemplate2 = _interopRequireDefault(_BackgroundTemplate);

var _Message = require('./Message');

var _Message2 = _interopRequireDefault(_Message);

var _UserProfileActionCreators = require('../../actions/UserProfileActionCreators');

var _UserProfileActionCreators2 = _interopRequireDefault(_UserProfileActionCreators);

var _ResourceActionCreators = require('../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _MessageActionCreators = require('../../actions/MessageActionCreators');

var _MessageActionCreators2 = _interopRequireDefault(_MessageActionCreators);

var _SocialActionCreators = require('../../actions/SocialActionCreators');

var _SocialActionCreators2 = _interopRequireDefault(_SocialActionCreators);

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var button_header_click = false;

var UserProfile = function (_Component) {
	_inherits(UserProfile, _Component);

	function UserProfile() {
		_classCallCheck(this, UserProfile);

		return _possibleConstructorReturn(this, (UserProfile.__proto__ || Object.getPrototypeOf(UserProfile)).apply(this, arguments));
	}

	_createClass(UserProfile, [{
		key: 'changeBannerImage',
		value: function changeBannerImage(evt) {
			_UserProfileActionCreators2.default.uploadBannerImage(this.props.user.token, evt.target.files[0]);
		}
	}, {
		key: 'changeBannerImageWithTemplate',
		value: function changeBannerImageWithTemplate(template_url, evt) {
			_ResourceActionCreators2.default.setDraft('template_banner', template_url);
			_UserProfileActionCreators2.default.changeBannerImageWithTemplate(this.props.user.token, { path: template_url });
			$("#myModal__background").modal('hide');
		}
	}, {
		key: 'triggerButton',
		value: function triggerButton(evt) {
			button_header_click = true;
			_JQueryTool2.default.animateScrollTo(this.props.page !== 'user_image_profile' ? '.newsfeed' : '.timeline-photo');
		}
	}, {
		key: 'loadNewUser',
		value: function loadNewUser(profile_key) {
			_UserProfileActionCreators2.default.getUserProfile(profile_key, this.props.user.token);
			_SocialActionCreators2.default.getFollowers(profile_key);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			var profile_key = "";
			button_header_click = false;

			// Load Resource
			_UserProfileActionCreators2.default.loadCompany();
			_JQueryTool2.default.moreinfo();

			if (typeof this.props.profile_key !== 'undefined') profile_key = this.props.profile_key;else if (typeof this.props.user.key !== 'undefined') profile_key = this.props.user.key;

			this.loadNewUser(profile_key);
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps, nextState) {

			if (nextProps.profile_key && nextProps.profile_key !== this.props.profile_key) this.loadNewUser(nextProps.profile_key);else if (typeof nextProps.profile_key === 'undefined' && typeof this.props.profile_key !== 'undefined' && this.props.user.token) this.loadNewUser(this.props.user.key);

			return true;
		}
	}, {
		key: 'toggleBackgroundMenu',
		value: function toggleBackgroundMenu() {
			$(this.refs.menu__background).toggleClass("opened");
		}
	}, {
		key: 'render',
		value: function render() {
			var formState = {
				user: this.props.user,
				profile: this.props.profile,
				status: this.props.status,
				lang: this.props.lang,
				editor: this.props.editor,
				other: this.props.profile_key && this.props.profile_key != this.props.user.key
			};

			var backgroundStyle = {};
			var editorBannerContent = _configLang2.default[this.props.lang].user_profile.editor.upload.banner;

			if (this.props.profile.banner) {
				backgroundStyle = {
					background: "url(" + this.props.profile.banner + ") no-repeat center center / cover"
				};
				if (this.props.profile.banner === '/images/loading.svg') backgroundStyle.backgroundSize = "80px 60px";
			} else {
				backgroundStyle = {
					backgroundColor: "#ccc"
				};
			}

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					'div',
					{ style: backgroundStyle, 'data-natural-width': '1400', 'data-natural-height': '470' },
					_react2.default.createElement(
						'div',
						{ className: this.props.editor ? "profile-cover profile-cover-hover" : "profile-cover" },
						this.props.editor && _react2.default.createElement(
							'div',
							{ className: 'edit__cover' },
							_react2.default.createElement(
								'span',
								{ onClick: this.toggleBackgroundMenu.bind(this), className: 'edit__header' },
								editorBannerContent[0],
								_react2.default.createElement('i', { className: 'fa fa-camera' })
							),
							_react2.default.createElement(
								'div',
								{ className: 'edit-menu' },
								_react2.default.createElement(
									'div',
									{ ref: 'menu__background', className: 'edit-menu__dropdown' },
									_react2.default.createElement(
										'div',
										{ className: 'edit-menu__item', onClick: _JQueryTool2.default.triggerUpload.bind(this, "uploadBannerImage") },
										_react2.default.createElement('i', { className: 'fa fa-pencil' }),
										' ',
										editorBannerContent[1]
									),
									_react2.default.createElement(
										'div',
										{ className: 'edit-menu__item', 'data-toggle': 'modal', 'data-target': '#myModal__background' },
										_react2.default.createElement('i', { className: 'fa fa-picture-o' }),
										' ',
										editorBannerContent[2]
									)
								)
							),
							_react2.default.createElement('input', { id: 'uploadBannerImage', type: 'file', onChange: this.changeBannerImage.bind(this), style: { display: "none" } })
						)
					)
				),
				_react2.default.createElement(_BackgroundTemplate2.default, { changeBannerImageWithTemplate: this.changeBannerImageWithTemplate.bind(this) }),
				_react2.default.createElement(
					'div',
					{ className: 'container' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'col-md-8 col-profile' },
							_react2.default.createElement(
								'div',
								{ className: 'row' },
								_react2.default.createElement(
									'div',
									{ className: 'col-md-12' },
									_react2.default.createElement(
										'div',
										{ className: 'profile-card' },
										_react2.default.createElement(_ProfileCard2.default, _extends({}, formState, { triggerButton: this.triggerButton.bind(this) })),
										_react2.default.createElement(
											'div',
											{ className: 'row' },
											_react2.default.createElement(
												'div',
												{ className: 'col-md-12' },
												this.props.editor || this.props.profile.about ? _react2.default.createElement(_About2.default, formState) : "",
												this.props.editor || this.props.profile.interests && this.props.profile.interests.length > 0 ? _react2.default.createElement(_Interest2.default, formState) : "",
												this.props.editor || this.props.profile.skills && this.props.profile.skills.length > 0 ? _react2.default.createElement(_Skill2.default, _extends({}, formState, { responsive: true })) : "",
												this.props.editor || this.props.profile.experiences && this.props.profile.experiences.length > 0 ? _react2.default.createElement(_WorkExperience2.default, _extends({}, formState, { responsive: true })) : "",
												this.props.editor || this.props.profile.educations && this.props.profile.educations.length > 0 ? _react2.default.createElement(_Education2.default, _extends({}, formState, { responsive: true })) : "",
												_react2.default.createElement(
													'div',
													{ id: 'more-info-detail', style: { display: "none" } },
													this.props.editor || this.props.profile.courses && this.props.profile.courses.length > 0 ? _react2.default.createElement(_Course2.default, _extends({}, formState, { responsive: true })) : "",
													this.props.profile.followers.length > 0 ? _react2.default.createElement(
														'div',
														null,
														_react2.default.createElement(_Following2.default, { profile: this.props.profile, lang: formState.lang, responsive: true })
													) : "",
													this.props.editor || this.props.profile.languages && this.props.profile.languages.length > 0 ? _react2.default.createElement(_Language2.default, _extends({}, formState, { responsive: true })) : ""
												),
												_react2.default.createElement(
													'div',
													{ className: 'profile-responsive', style: { marginBottom: "10px" } },
													_react2.default.createElement(
														'div',
														{ className: 'col-md-12' },
														_react2.default.createElement(
															'div',
															{ className: 'btn-cover', style: { margin: "0 10px" } },
															_react2.default.createElement(
																'button',
																{ id: 'more-info', className: 'btn btn-default', style: { width: "100%", margin: "auto" } },
																'More Information'
															)
														)
													)
												)
											)
										)
									)
								)
							),
							this.props.page !== 'user_image_profile' && this.props.profile.key && !this.props.profile.loading ? _react2.default.createElement(_NewsFeed2.default, { button_header_click: button_header_click, editor: this.props.editor, profile: this.props.profile, user: this.props.user, owner: !formState.other, feed: this.props.feed, feed_id: this.props.feed_id, language: this.props.lang }) : "",
							this.props.page === 'user_image_profile' && this.props.profile.key && !this.props.profile.loading ? _react2.default.createElement(_Photo2.default, { button_header_click: button_header_click, editor: this.props.editor, profile: this.props.profile, user: this.props.user, owner: !formState.other, feed: this.props.feed, language: this.props.lang }) : ""
						),
						_react2.default.createElement(
							'div',
							{ className: 'col-md-4 col-detail' },
							_react2.default.createElement(
								'div',
								{ className: 'box_style_detail', style: { marginTop: "15px" } },
								this.props.editor || this.props.profile.experiences && this.props.profile.experiences.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_WorkExperience2.default, _extends({}, formState, { responsive: false })),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.editor || this.props.profile.skills && this.props.profile.skills.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Skill2.default, _extends({}, formState, { responsive: false })),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.editor || this.props.profile.educations && this.props.profile.educations.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Education2.default, _extends({}, formState, { responsive: false })),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.editor || this.props.profile.courses && this.props.profile.courses.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Course2.default, _extends({}, formState, { responsive: false })),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.profile.followers.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Following2.default, { lang: formState.lang, profile: this.props.profile, responsive: false }),
									_react2.default.createElement('hr', null)
								) : "",
								this.props.editor || this.props.profile.languages && this.props.profile.languages.length > 0 ? _react2.default.createElement(
									'div',
									null,
									_react2.default.createElement(_Language2.default, _extends({}, formState, { responsive: false })),
									_react2.default.createElement('hr', null),
									' '
								) : ""
							)
						)
					)
				)
			);
		}
	}]);

	return UserProfile;
}(_react.Component);

exports.default = UserProfile;