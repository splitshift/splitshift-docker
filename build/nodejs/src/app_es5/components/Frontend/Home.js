'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _WhySplitshift = require('./WhySplitshift');

var _WhySplitshift2 = _interopRequireDefault(_WhySplitshift);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Landing = function (_Component) {
  _inherits(Landing, _Component);

  function Landing() {
    _classCallCheck(this, Landing);

    return _possibleConstructorReturn(this, (Landing.__proto__ || Object.getPrototypeOf(Landing)).apply(this, arguments));
  }

  _createClass(Landing, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      $('.banner--what').mouseenter(function () {
        $("body").addClass("overflow");
      });
      $('.banner--what').mouseleave(function () {
        $("body").removeClass("overflow");
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: 'cover', style: { background: "url('/images/landing-black.jpg') no-repeat center center", backgroundSize: "cover" } },
          _react2.default.createElement(
            'div',
            { className: 'cover__text' },
            _react2.default.createElement(
              'div',
              { className: 'cover__text--center' },
              _react2.default.createElement(
                'h1',
                null,
                _react2.default.createElement(
                  'span2',
                  null,
                  this.props.resource.heroCover[0]
                ),
                ' ',
                _react2.default.createElement(
                  'span2',
                  null,
                  this.props.resource.heroCover[1]
                )
              ),
              _react2.default.createElement(
                'h1',
                { style: { marginTop: "0px" } },
                _react2.default.createElement(
                  'span4',
                  null,
                  this.props.resource.heroDetail
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'cover__btn' },
                _react2.default.createElement(
                  'button',
                  { 'data-toggle': 'modal', 'data-target': '#myModal__login', className: 'btn btn-default', type: 'button' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    this.props.resource.registerBtn
                  )
                )
              ),
              _react2.default.createElement(
                'h2',
                null,
                _react2.default.createElement(
                  'span1',
                  null,
                  this.props.resource.registerHeader
                )
              )
            )
          )
        ),
        _react2.default.createElement(_WhySplitshift2.default, { content: this.props.resource.whysplitshift }),
        _react2.default.createElement(
          'div',
          { className: 'whatweoffer' },
          _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
              'div',
              { className: 'text--center' },
              _react2.default.createElement(
                'h2',
                null,
                _react2.default.createElement(
                  'span5',
                  null,
                  this.props.resource.wwo[0],
                  ' ',
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.resource.wwo[1]
                  ),
                  ' ',
                  this.props.resource.wwo[2]
                )
              ),
              _react2.default.createElement('hr', { style: { width: "50%" } })
            ),
            _react2.default.createElement(
              'div',
              { className: 'gallery' },
              _react2.default.createElement(
                'div',
                { className: 'row' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--what' },
                    _react2.default.createElement(
                      'div',
                      { className: 'banner--photo' },
                      _react2.default.createElement('img', { className: 'banner__pic', src: '/images/landing/Showcase your Profile.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'banner__detail' },
                        _react2.default.createElement(
                          'span1',
                          null,
                          this.props.resource.wwoHeader[0]
                        )
                      )
                    ),
                    _react2.default.createElement('div', { className: 'overlay' }),
                    _react2.default.createElement(
                      'div',
                      { className: 'hover--banner hover__left' },
                      _react2.default.createElement('img', { className: 'hover--pic', src: '/images/landing/wwo/Show Case Your Unique Profile.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'hover--detail' },
                        _react2.default.createElement(
                          'p',
                          null,
                          _react2.default.createElement(
                            'span1',
                            null,
                            this.props.resource.wwoDetail[0]
                          )
                        ),
                        this.props.user.token ? _react2.default.createElement(
                          _reactRouter.Link,
                          { to: this.props.user.type === 'Employee' ? '/' + this.props.lang + '/profile' : '/' + this.props.lang + '/company' },
                          _react2.default.createElement(
                            'button',
                            { className: 'btn btn-default reverse' },
                            'GET STARTED'
                          )
                        ) : _react2.default.createElement(
                          'button',
                          { className: 'btn btn-default reverse', 'data-toggle': 'modal', 'data-target': '#myModal__login' },
                          'GET STARTED'
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--what' },
                    _react2.default.createElement(
                      'div',
                      { className: 'banner--photo' },
                      _react2.default.createElement('img', { className: 'banner__pic', src: '/images/landing/Connect with People.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'banner__detail' },
                        _react2.default.createElement(
                          'span1',
                          null,
                          this.props.resource.wwoHeader[1]
                        )
                      )
                    ),
                    _react2.default.createElement('div', { className: 'overlay' }),
                    _react2.default.createElement(
                      'div',
                      { className: 'hover--banner hover__center' },
                      _react2.default.createElement('img', { className: 'hover--pic', src: '/images/landing/wwo/Connect with People.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'hover--detail' },
                        _react2.default.createElement(
                          'p',
                          null,
                          _react2.default.createElement(
                            'span1',
                            null,
                            this.props.resource.wwoDetail[1]
                          )
                        ),
                        _react2.default.createElement(
                          _reactRouter.Link,
                          { to: '/' + this.props.lang + '/people-search' },
                          _react2.default.createElement(
                            'button',
                            { className: 'btn btn-default reverse' },
                            'CONNECT NOW'
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--what' },
                    _react2.default.createElement(
                      'div',
                      { className: 'banner--photo' },
                      _react2.default.createElement('img', { className: 'banner__pic', src: '/images/landing/Search Jobs.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'banner__detail' },
                        _react2.default.createElement(
                          'span1',
                          null,
                          this.props.resource.wwoHeader[2]
                        )
                      )
                    ),
                    _react2.default.createElement('div', { className: 'overlay' }),
                    _react2.default.createElement(
                      'div',
                      { className: 'hover--banner hover__right' },
                      _react2.default.createElement('img', { className: 'hover--pic', src: '/images/landing/wwo/Search Jobs.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'hover--detail' },
                        _react2.default.createElement(
                          'p',
                          null,
                          _react2.default.createElement(
                            'span1',
                            null,
                            this.props.resource.wwoDetail[2]
                          )
                        ),
                        _react2.default.createElement(
                          _reactRouter.Link,
                          { to: '/' + this.props.lang + '/job-search' },
                          _react2.default.createElement(
                            'button',
                            { className: 'btn btn-default reverse' },
                            'SEARCH JOBS'
                          )
                        )
                      )
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'row' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--what' },
                    _react2.default.createElement(
                      'div',
                      { className: 'banner--photo' },
                      _react2.default.createElement('img', { className: 'banner__pic', src: '/images/landing/Explore Companies.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'banner__detail' },
                        _react2.default.createElement(
                          'span1',
                          null,
                          this.props.resource.wwoHeader[3]
                        )
                      )
                    ),
                    _react2.default.createElement('div', { className: 'overlay' }),
                    _react2.default.createElement(
                      'div',
                      { className: 'hover--banner hover__left' },
                      _react2.default.createElement('img', { className: 'hover--pic', src: '/images/landing/wwo/Explore Companies.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'hover--detail' },
                        _react2.default.createElement(
                          'p',
                          null,
                          _react2.default.createElement(
                            'span1',
                            null,
                            this.props.resource.wwoDetail[3]
                          )
                        ),
                        _react2.default.createElement(
                          _reactRouter.Link,
                          { to: '/' + this.props.lang + '/company-search' },
                          _react2.default.createElement(
                            'button',
                            { className: 'btn btn-default reverse' },
                            'BROWSE COMPANIES'
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--what' },
                    _react2.default.createElement(
                      'div',
                      { className: 'banner--photo' },
                      _react2.default.createElement('img', { className: 'banner__pic', src: '/images/landing/Online Eduction Resources.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'banner__detail' },
                        _react2.default.createElement(
                          'span1',
                          null,
                          this.props.resource.wwoHeader[4]
                        )
                      )
                    ),
                    _react2.default.createElement('div', { className: 'overlay' }),
                    _react2.default.createElement(
                      'div',
                      { className: 'hover--banner hover__center' },
                      _react2.default.createElement('img', { className: 'hover--pic', src: '/images/landing/wwo/Online Education Resources.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'hover--detail' },
                        _react2.default.createElement(
                          'p',
                          null,
                          _react2.default.createElement(
                            'span1',
                            null,
                            this.props.resource.wwoDetail[4]
                          )
                        ),
                        _react2.default.createElement(
                          _reactRouter.Link,
                          { to: '/' + this.props.lang + '/resource' },
                          _react2.default.createElement(
                            'button',
                            { className: 'btn btn-default reverse' },
                            'START LEARNING'
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-4' },
                  _react2.default.createElement(
                    'div',
                    { className: 'banner--what' },
                    _react2.default.createElement(
                      'div',
                      { className: 'banner--photo' },
                      _react2.default.createElement('img', { className: 'banner__pic', src: '/images/landing/Community Buzz.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'banner__detail' },
                        _react2.default.createElement(
                          'span1',
                          null,
                          this.props.resource.wwoHeader[5]
                        )
                      )
                    ),
                    _react2.default.createElement('div', { className: 'overlay' }),
                    _react2.default.createElement(
                      'div',
                      { className: 'hover--banner hover__right' },
                      _react2.default.createElement('img', { className: 'hover--pic', src: '/images/landing/wwo/Community Buzz.jpg' }),
                      _react2.default.createElement(
                        'div',
                        { className: 'hover--detail' },
                        _react2.default.createElement(
                          'p',
                          null,
                          _react2.default.createElement(
                            'span1',
                            null,
                            this.props.resource.wwoDetail[5]
                          )
                        ),
                        _react2.default.createElement(
                          _reactRouter.Link,
                          { to: '/' + this.props.lang + '/buzz' },
                          _react2.default.createElement(
                            'button',
                            { className: 'btn btn-default reverse' },
                            'DISCOVER NOW'
                          )
                        )
                      )
                    )
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'gallery__responsive' },
              _react2.default.createElement(
                'div',
                { className: 'card__what' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      this.props.resource.wwoHeader[0]
                    )
                  )
                ),
                _react2.default.createElement('img', { className: 'card--photo', src: '/images/landing/wwo/Show Case Your Unique Profile.jpg' }),
                _react2.default.createElement(
                  'div',
                  { className: 'card--detail' },
                  _react2.default.createElement(
                    'p',
                    null,
                    this.props.resource.wwoDetail[0]
                  ),
                  this.props.user.token ? _react2.default.createElement(
                    _reactRouter.Link,
                    { to: this.props.user.type === 'Employee' ? '/th/profile' : '/th/company' },
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default reverse' },
                      'GET STARTED'
                    )
                  ) : _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default reverse', 'data-toggle': 'modal', 'data-target': '#myModal__login' },
                    'GET STARTED'
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'card__what' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      this.props.resource.wwoHeader[1]
                    )
                  )
                ),
                _react2.default.createElement('img', { className: 'card--photo', src: '/images/landing/wwo/Connect with People.jpg' }),
                _react2.default.createElement(
                  'div',
                  { className: 'card--detail' },
                  _react2.default.createElement(
                    'p',
                    null,
                    this.props.resource.wwoDetail[1]
                  ),
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.lang + '/people-search' },
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default reverse' },
                      'CONNECT NOW'
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'card__what' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      this.props.resource.wwoHeader[2]
                    )
                  )
                ),
                _react2.default.createElement('img', { className: 'card--photo', src: '/images/landing/wwo/Search Jobs.jpg' }),
                _react2.default.createElement(
                  'div',
                  { className: 'card--detail' },
                  _react2.default.createElement(
                    'p',
                    null,
                    this.props.resource.wwoDetail[2]
                  ),
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.lang + '/job-search' },
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default reverse' },
                      'SEARCH JOBS'
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'card__what' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      this.props.resource.wwoHeader[3]
                    )
                  )
                ),
                _react2.default.createElement('img', { className: 'card--photo', src: '/images/landing/wwo/Explore Companies.jpg' }),
                _react2.default.createElement(
                  'div',
                  { className: 'card--detail' },
                  _react2.default.createElement(
                    'p',
                    null,
                    this.props.resource.wwoDetail[3]
                  ),
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.lang + '/company-search' },
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default reverse' },
                      'BROWSE COMPANIES'
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'card__what' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      this.props.resource.wwoHeader[4]
                    )
                  )
                ),
                _react2.default.createElement('img', { className: 'card--photo', src: '/images/landing/wwo/Online Education Resources.jpg' }),
                _react2.default.createElement(
                  'div',
                  { className: 'card--detail' },
                  _react2.default.createElement(
                    'p',
                    null,
                    this.props.resource.wwoDetail[4]
                  ),
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.lang + '/resource' },
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default reverse' },
                      'START LEARNING'
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'card__what' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      this.props.resource.wwoHeader[5]
                    )
                  )
                ),
                _react2.default.createElement('img', { className: 'card--photo', src: '/images/landing/wwo/Community Buzz.jpg' }),
                _react2.default.createElement(
                  'div',
                  { className: 'card--detail' },
                  _react2.default.createElement(
                    'p',
                    null,
                    this.props.resource.wwoDetail[5]
                  ),
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.lang + '/buzz' },
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default reverse' },
                      'DISCOVER NOW'
                    )
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'text--center' },
              _react2.default.createElement(
                'div',
                { className: 'whysplitshift__btn--profile' },
                _react2.default.createElement(
                  'button',
                  { 'data-toggle': 'modal', 'data-target': '#myModal__login', className: 'btn btn-default', type: 'button' },
                  _react2.default.createElement(
                    'span1',
                    null,
                    this.props.resource.registerBtn
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { id: 'splitshiftstat' },
          _react2.default.createElement(
            'div',
            { className: 'splitshiftstatcontainer', style: { background: "url('/images/stats.jpg') no-repeat center center", backgroundSize: "cover", filter: "alpha(opacity=50)" } },
            _react2.default.createElement(
              'div',
              { className: 'text--center' },
              _react2.default.createElement(
                'h2',
                null,
                _react2.default.createElement(
                  'span1',
                  { style: { marginBottom: "50px" } },
                  this.props.resource.stat[0],
                  ' ',
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.resource.stat[1]
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'splitshiftstat__stat' },
              _react2.default.createElement(
                'div',
                { className: 'stat__item' },
                _react2.default.createElement('img', { src: '/images/stats/member.png' }),
                _react2.default.createElement(
                  'h5',
                  null,
                  this.props.stat.members
                ),
                _react2.default.createElement(
                  'h6',
                  null,
                  this.props.resource.stat[2]
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'stat__item' },
                _react2.default.createElement('img', { src: '/images/stats/job.png' }),
                _react2.default.createElement(
                  'h5',
                  null,
                  this.props.stat.jobs
                ),
                _react2.default.createElement(
                  'h6',
                  null,
                  this.props.resource.stat[3]
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'stat__item' },
                _react2.default.createElement('img', { src: '/images/stats/company.png' }),
                _react2.default.createElement(
                  'h5',
                  null,
                  this.props.stat.companies
                ),
                _react2.default.createElement(
                  'h6',
                  null,
                  this.props.resource.stat[4]
                )
              )
            )
          )
        )
      );
    }
  }]);

  return Landing;
}(_react.Component);

exports.default = Landing;