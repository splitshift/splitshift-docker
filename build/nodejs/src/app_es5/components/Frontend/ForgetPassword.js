'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _AuthenticationActionCreators = require('../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ForgetPassword = function (_Component) {
  _inherits(ForgetPassword, _Component);

  function ForgetPassword() {
    _classCallCheck(this, ForgetPassword);

    return _possibleConstructorReturn(this, (ForgetPassword.__proto__ || Object.getPrototypeOf(ForgetPassword)).apply(this, arguments));
  }

  _createClass(ForgetPassword, [{
    key: 'changePassword',
    value: function changePassword(evt) {
      evt.preventDefault();
      var info = { password: this.refs.password.value };
      _AuthenticationActionCreators2.default.changePasswordFormForgot(info, this.props.forget_id);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (nextProps.login.forget && !this.props.login.forget) {
        alert("Change Password Complete!");
        _reactRouter.browserHistory.push('/th/');
      }

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'container', style: { marginTop: "100px" } },
        _react2.default.createElement(
          'form',
          { onSubmit: this.changePassword.bind(this) },
          _react2.default.createElement(
            'label',
            { htmlFor: 'pwd' },
            'Password'
          ),
          _react2.default.createElement('input', { ref: 'password', type: 'password', className: 'form-control', placeholder: 'Password...', id: 'pwd' }),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'button',
              { className: 'modal__btn--dark' },
              'CHANGE PASSWORD'
            )
          )
        )
      );
    }
  }]);

  return ForgetPassword;
}(_react.Component);

exports.default = ForgetPassword;