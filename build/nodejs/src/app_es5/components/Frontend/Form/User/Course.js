'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _UserProfileActionCreators = require('../../../../actions/UserProfileActionCreators');

var _UserProfileActionCreators2 = _interopRequireDefault(_UserProfileActionCreators);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _ConfigData = require('../../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormUserCourse = function (_Component) {
  _inherits(FormUserCourse, _Component);

  function FormUserCourse() {
    _classCallCheck(this, FormUserCourse);

    return _possibleConstructorReturn(this, (FormUserCourse.__proto__ || Object.getPrototypeOf(FormUserCourse)).apply(this, arguments));
  }

  _createClass(FormUserCourse, [{
    key: 'createCourse',
    value: function createCourse() {
      var result = this.props.profile.draft;

      if (typeof result.start_date === 'undefined') result.start_date = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;

      if (typeof result.end_date === 'undefined' || result.end_date || typeof result.current_job === 'undefined' || !result.current_job) result.end_date = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

      _UserProfileActionCreators2.default.createCourse(this.props.user.token, result);
    }
  }, {
    key: 'editCourse',
    value: function editCourse() {
      _UserProfileActionCreators2.default.editCourse(this.props.user.token, this.props.profile.draft);
    }
  }, {
    key: 'deleteCourse',
    value: function deleteCourse(id) {
      this.setEditor("edit_course");
      _UserProfileActionCreators2.default.deleteCourse(this.props.user.token, this.props.profile.courses[id]);
    }
  }, {
    key: 'setDraftEditCourse',
    value: function setDraftEditCourse(id) {
      _ResourceActionCreators2.default.setOldDraft(this.props.profile.courses[id]);
      this.setEditor("edit_course");
    }
  }, {
    key: 'updateCourse',
    value: function updateCourse(name, evt) {
      var value = evt.target.value;

      if (name == 'start_date') value = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;else if (name == 'end_date') value = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

      _ResourceActionCreators2.default.setDraft(name, value);
    }
  }, {
    key: 'toResumeContent',
    value: function toResumeContent() {
      _ResourceActionCreators2.default.cancelEditor('edit_course', false);
    }
  }, {
    key: 'setEditor',
    value: function setEditor(name, evt) {
      _ResourceActionCreators2.default.setEditor(name, true);
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var _this2 = this;

      var onEdit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var courseList = this.props.profile.courses ? this.props.profile.courses.map(function (course, id) {
        return _react2.default.createElement(
          'li',
          { key: id },
          _react2.default.createElement(
            'b',
            null,
            course.name
          ),
          ' ',
          course.link && _react2.default.createElement(
            'span',
            null,
            _react2.default.createElement(
              'a',
              { href: course.link, target: '_blank' },
              editorContent.link
            )
          ),
          ' ',
          onEdit && _react2.default.createElement(
            'span',
            null,
            _react2.default.createElement(
              'a',
              { href: 'javascript:void(0)', onClick: _this2.setDraftEditCourse.bind(_this2, id) },
              editorContent.edit
            ),
            ' ',
            _react2.default.createElement(
              'a',
              { href: 'javascript:void(0)', onClick: _this2.deleteCourse.bind(_this2, id) },
              editorContent.delete
            )
          ),
          '  ',
          _react2.default.createElement('br', null),
          course.description && _react2.default.createElement(
            'span4',
            null,
            course.description,
            _react2.default.createElement('br', null)
          ),
          _react2.default.createElement(
            'span',
            { style: { color: "#aaa" } },
            _JQueryTool2.default.changeDateFormat(course.start_date),
            ' - ',
            _JQueryTool2.default.changeDateFormat(course.end_date),
            ' (',
            _JQueryTool2.default.diffExperienceDate(course.start_date, course.end_date),
            ')'
          ),
          ' ',
          _react2.default.createElement('br', null),
          _react2.default.createElement('br', null)
        );
      }) : "";

      return _react2.default.createElement(
        'ul',
        { style: { marginBottom: "0px" } },
        courseList
      );
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      var addContent = _configLang2.default[this.props.lang].user_profile.course.add;
      return _react2.default.createElement(
        'div',
        null,
        this.props.status.edit_course ? this.courseForm() : _react2.default.createElement(
          'button',
          { className: 'btn btn-default btn-mini', onClick: this.setEditor.bind(this, "edit_course") },
          addContent
        ),
        this.props.status.edit_course ? "" : this.contentRender(true)
      );
    }
  }, {
    key: 'courseForm',
    value: function courseForm() {
      var _this3 = this;

      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var editorCourseContent = editorContent.course;
      return this.props.profile ? _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement('input', { type: 'text', onChange: this.updateCourse.bind(this, 'name'), className: 'form-control form-control--mini', placeholder: editorCourseContent[0], defaultValue: this.props.profile.draft.name }),
        _react2.default.createElement('input', { type: 'text', onChange: this.updateCourse.bind(this, 'link'), className: 'form-control form-control--mini', placeholder: editorCourseContent[1], defaultValue: this.props.profile.draft.link }),
        _react2.default.createElement('input', { type: 'text', onChange: this.updateCourse.bind(this, 'description'), className: 'form-control form-control--mini', placeholder: editorCourseContent[2], defaultValue: this.props.profile.draft.description }),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-12' },
            _react2.default.createElement(
              'p',
              { style: { paddingTop: "5px", margin: "0 0 10px" } },
              editorCourseContent[3]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini ', ref: 'start_date_year', onChange: this.updateCourse.bind(this, 'start_date'), defaultValue: this.props.profile.draft.start_date ? _JQueryTool2.default.getYear(this.props.profile.draft.start_date) : "" },
              _ConfigData2.default.Years().map(function (year, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: year },
                  _this3.props.lang === 'th' ? year + 543 : year
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini ', ref: 'start_date_month', onChange: this.updateCourse.bind(this, 'start_date'), defaultValue: this.props.profile.draft.start_date ? _JQueryTool2.default.getMonth(this.props.profile.draft.start_date) : "" },
              (this.props.lang === 'th' ? _ConfigData2.default.ThaiMonths() : _ConfigData2.default.Months()).map(function (month, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: month[0] },
                  month[1]
                );
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-12' },
            _react2.default.createElement(
              'p',
              { style: { paddingTop: "5px", margin: "0 0 10px" } },
              editorCourseContent[4]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini', ref: 'end_date_year', onChange: this.updateCourse.bind(this, 'end_date'), defaultValue: this.props.profile.draft.end_date ? _JQueryTool2.default.getYear(this.props.profile.draft.end_date) : "" },
              _ConfigData2.default.Years().map(function (year, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: year },
                  _this3.props.lang === 'th' ? year + 543 : year
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini ', ref: 'end_date_month', onChange: this.updateCourse.bind(this, 'end_date'), defaultValue: this.props.profile.draft.end_date ? _JQueryTool2.default.getMonth(this.props.profile.draft.end_date) : "" },
              (this.props.lang === 'th' ? _ConfigData2.default.ThaiMonths() : _ConfigData2.default.Months()).map(function (month, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: month[0] },
                  month[1]
                );
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { style: { marginTop: "5px" } },
          _react2.default.createElement(
            'button',
            { onClick: this.props.profile.draft._id ? this.editCourse.bind(this) : this.createCourse.bind(this), className: 'btn btn-default btn-mini' },
            this.props.profile.draft._id ? editorContent.edit : editorContent.add
          ),
          ' ',
          _react2.default.createElement(
            'a',
            { onClick: this.toResumeContent.bind(this), href: 'javascript:void(0)' },
            editorContent.cancel
          )
        )
      ) : "";
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.status.edit_course && this.props.status.edit_course) _UserProfileActionCreators2.default.reloadUserProfile(this.props.user.key);

      return JSON.stringify(nextProps.profile.courses) !== JSON.stringify(this.props.profile.courses) || nextProps.status.edit_course !== this.props.status.edit_course || nextProps.editor !== this.props.editor || nextProps.lang !== this.props.lang;
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].user_profile.course.header;
      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          this.props.editor ? this.editorRender() : this.contentRender()
        )
      );
    }
  }]);

  return FormUserCourse;
}(_react.Component);

exports.default = FormUserCourse;

/*

<input type="text" data-name="start_date" className="form-control form-control--mini form-datepicker" placeholder="Start Date" defaultValue={this.props.profile.draft.start_date ? JQueryTool.changeDatePickerFormat(this.props.profile.draft.start_date) : ""} />
<input type="text" data-name="end_date" className="form-control form-control--mini form-datepicker" placeholder="End Date" defaultValue={this.props.profile.draft.end_date ? JQueryTool.changeDatePickerFormat(this.props.profile.draft.end_date) : ""} />

*/