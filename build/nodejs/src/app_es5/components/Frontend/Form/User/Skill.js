'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _UserProfileActionCreators = require('../../../../actions/UserProfileActionCreators');

var _UserProfileActionCreators2 = _interopRequireDefault(_UserProfileActionCreators);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormUserSkill = function (_Component) {
  _inherits(FormUserSkill, _Component);

  function FormUserSkill() {
    _classCallCheck(this, FormUserSkill);

    return _possibleConstructorReturn(this, (FormUserSkill.__proto__ || Object.getPrototypeOf(FormUserSkill)).apply(this, arguments));
  }

  _createClass(FormUserSkill, [{
    key: 'verifyData',
    value: function verifyData(skill) {
      return skill.trim() !== '';
    }
  }, {
    key: 'createSkill',
    value: function createSkill(evt) {
      evt.preventDefault();
      if (this.verifyData(this.refs.new_skill.value)) {
        var result = {
          skills: this.props.profile.skills.concat(this.refs.new_skill.value)
        };
        _UserProfileActionCreators2.default.updateProfile(result);
        _UserProfileActionCreators2.default.updateSkill(this.props.user.token, result);
        this.refs.new_skill.value = "";
      } else alert(this.props.lang === 'th' ? ' พิมพ์ทักษะในการทำงานของคุณ' : "Please type your skill.");
    }
  }, {
    key: 'deleteSkill',
    value: function deleteSkill(id, evt) {
      var result = {
        skills: this.props.profile.skills.filter(function (skill, index) {
          return index != id;
        })
      };
      _UserProfileActionCreators2.default.updateProfile(result);
      _UserProfileActionCreators2.default.updateSkill(this.props.user.token, result);
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var _this2 = this;

      var onEdit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;

      var skillsList = this.props.profile.skills ? this.props.profile.skills.map(function (skill, id) {
        return _react2.default.createElement(
          'li',
          { key: id },
          _react2.default.createElement('i', { className: 'fa fa-book' }),
          ' ',
          skill,
          ' ',
          ' ',
          onEdit ? _react2.default.createElement(
            'a',
            { href: 'javascript:void(0)', onClick: _this2.deleteSkill.bind(_this2, id) },
            editorContent.delete
          ) : ""
        );
      }) : "";

      return _react2.default.createElement(
        'ul',
        null,
        skillsList
      );
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return JSON.stringify(nextProps.profile.skills) !== JSON.stringify(this.props.profile.skills) || nextProps.status.edit_skill !== this.props.status.edit_skill || nextProps.editor !== this.props.editor || nextProps.lang !== this.props.lang;
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var placeholder = _configLang2.default[this.props.lang].user_profile.skill.placeholder;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'form',
          { onSubmit: this.createSkill.bind(this) },
          this.contentRender(true),
          _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
              'div',
              { className: 'col-md-9', style: { paddingLeft: "0px", paddingRight: "0px" } },
              _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', ref: 'new_skill', placeholder: placeholder })
            ),
            _react2.default.createElement(
              'div',
              { className: 'col-md-3', style: { paddingRight: "0px", paddingLeft: "0px" } },
              _react2.default.createElement(
                'button',
                { className: 'btn btn-default btn-mini--right' },
                editorContent.add
              )
            )
          )
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].user_profile.skill.header;

      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          this.props.editor ? this.editorRender() : this.contentRender()
        )
      );
    }
  }]);

  return FormUserSkill;
}(_react.Component);

exports.default = FormUserSkill;