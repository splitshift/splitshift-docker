'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _UserProfileActionCreators = require('../../../../actions/UserProfileActionCreators');

var _UserProfileActionCreators2 = _interopRequireDefault(_UserProfileActionCreators);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _ConfigData = require('../../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormUserEducation = function (_Component) {
  _inherits(FormUserEducation, _Component);

  function FormUserEducation() {
    _classCallCheck(this, FormUserEducation);

    return _possibleConstructorReturn(this, (FormUserEducation.__proto__ || Object.getPrototypeOf(FormUserEducation)).apply(this, arguments));
  }

  _createClass(FormUserEducation, [{
    key: 'createEducation',
    value: function createEducation(evt) {
      evt.preventDefault();

      var result = this.props.profile.draft;

      if (typeof result.start_date === 'undefined') result.start_date = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;

      if (typeof result.graduate_date === 'undefined' || result.graduate_date || typeof result.current_job === 'undefined' || !result.current_job) result.graduate_date = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

      _UserProfileActionCreators2.default.createEducation(this.props.user.token, result);
    }
  }, {
    key: 'editEducation',
    value: function editEducation(evt) {
      evt.preventDefault();
      _UserProfileActionCreators2.default.editEducation(this.props.user.token, this.props.profile.draft);
    }
  }, {
    key: 'deleteEducation',
    value: function deleteEducation(id) {

      var result = {
        educations: this.props.profile.educations.filter(function (education, index) {
          return index != id;
        })
      };

      _UserProfileActionCreators2.default.updateProfile(result);
      _UserProfileActionCreators2.default.deleteEducation(this.props.user.token, this.props.profile.educations[id]);
    }
  }, {
    key: 'setEditor',
    value: function setEditor(name, evt) {
      _ResourceActionCreators2.default.setEditor(name, true);
    }
  }, {
    key: 'updateEducation',
    value: function updateEducation(name, evt) {

      var value = evt.target.value;

      if (name == 'start_date') value = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;else if (name == 'graduate_date') value = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

      _ResourceActionCreators2.default.setDraft(name, value);
    }
  }, {
    key: 'toResumeContent',
    value: function toResumeContent() {
      _ResourceActionCreators2.default.cancelEditor("edit_education", false);
    }
  }, {
    key: 'setDraftEditExperience',
    value: function setDraftEditExperience(id) {
      _ResourceActionCreators2.default.setOldDraft(this.props.profile.educations[id]);
      this.setEditor("edit_education");
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var _this2 = this;

      var onEdit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var educationsList = this.props.profile.educations ? this.props.profile.educations.map(function (education, id) {
        return _react2.default.createElement(
          'li',
          { key: id },
          _react2.default.createElement(
            'p',
            null,
            _react2.default.createElement('i', { className: 'fa fa-graduation-cap' }),
            ' ',
            _react2.default.createElement(
              'b',
              null,
              education.field
            ),
            ' ',
            _react2.default.createElement('br', null),
            education.description ? _react2.default.createElement(
              'span4',
              null,
              ' ',
              education.description,
              ' ',
              _react2.default.createElement('br', null)
            ) : "",
            education.school ? _react2.default.createElement(
              'span4',
              null,
              ' ',
              education.school,
              ' ',
              _react2.default.createElement('br', null)
            ) : "",
            _react2.default.createElement(
              'span',
              { style: { color: "#aaa" } },
              _JQueryTool2.default.changeDateFormat(education.start_date),
              ' - ',
              education.graduate_date == "" ? "Present" : _JQueryTool2.default.changeDateFormat(education.graduate_date),
              ' (',
              _JQueryTool2.default.diffExperienceDate(education.start_date, education.graduate_date),
              ')'
            ),
            ' ',
            _react2.default.createElement('br', null),
            onEdit ? _react2.default.createElement(
              'span',
              null,
              _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: _this2.setDraftEditExperience.bind(_this2, id) },
                editorContent.edit
              ),
              ' ',
              _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: _this2.deleteEducation.bind(_this2, id) },
                editorContent.delete
              )
            ) : ""
          )
        );
      }) : "";

      return _react2.default.createElement(
        'ul',
        { style: { marginBottom: "0px" } },
        educationsList
      );
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      var addContent = _configLang2.default[this.props.lang].user_profile.education.add;
      return _react2.default.createElement(
        'div',
        null,
        this.props.status.edit_education && !this.props.status.deleting ? this.educationForm() : _react2.default.createElement(
          'button',
          { className: 'btn btn-default btn-mini', onClick: this.setEditor.bind(this, "edit_education") },
          addContent
        ),
        this.props.status.edit_education ? "" : this.contentRender(true)
      );
    }
  }, {
    key: 'educationForm',
    value: function educationForm() {
      var _this3 = this;

      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var editorEducationContent = editorContent.education;

      return this.props.profile ? _react2.default.createElement(
        'form',
        { onSubmit: this.props.profile.draft._id ? this.editEducation.bind(this) : this.createEducation.bind(this) },
        _react2.default.createElement('input', { type: 'text', onChange: this.updateEducation.bind(this, "school"), className: 'form-control form-control--mini', placeholder: editorEducationContent[0], defaultValue: this.props.profile.draft.school }),
        _react2.default.createElement('input', { type: 'text', onChange: this.updateEducation.bind(this, "field"), className: 'form-control form-control--mini', placeholder: editorEducationContent[1], defaultValue: this.props.profile.draft.field }),
        _react2.default.createElement('input', { type: 'text', onChange: this.updateEducation.bind(this, "description"), className: 'form-control form-control--mini', placeholder: editorEducationContent[2], defaultValue: this.props.profile.draft.description }),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-12' },
            _react2.default.createElement(
              'p',
              { style: { paddingTop: "5px", margin: "0 0 10px" } },
              editorEducationContent[3]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini ', ref: 'start_date_year', onChange: this.updateEducation.bind(this, 'start_date'), defaultValue: this.props.profile.draft.start_date ? _JQueryTool2.default.getMonth(this.props.profile.draft.start_date) : "" },
              _ConfigData2.default.Years().map(function (year, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: year },
                  _this3.props.lang === 'th' ? year + 543 : year
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini ', ref: 'start_date_month', onChange: this.updateEducation.bind(this, 'start_date'), defaultValue: this.props.profile.draft.start_date ? _JQueryTool2.default.getMonth(this.props.profile.draft.start_date) : "" },
              (this.props.lang === 'th' ? _ConfigData2.default.ThaiMonths() : _ConfigData2.default.Months()).map(function (month, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: month[0] },
                  month[1]
                );
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-12' },
            _react2.default.createElement(
              'p',
              { style: { paddingTop: "5px", margin: "0 0 10px" } },
              editorEducationContent[4]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini', ref: 'end_date_year', onChange: this.updateEducation.bind(this, 'graduate_date'), defaultValue: this.props.profile.draft.graduate_date ? _JQueryTool2.default.getYear(this.props.profile.draft.graduate_date) : "" },
              _ConfigData2.default.Years().map(function (year, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: year },
                  _this3.props.lang === 'th' ? year + 543 : year
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini ', ref: 'end_date_month', onChange: this.updateEducation.bind(this, 'graduate_date'), defaultValue: this.props.profile.draft.graduate_date ? _JQueryTool2.default.getMonth(this.props.profile.draft.graduate_date) : "" },
              (this.props.lang === 'th' ? _ConfigData2.default.ThaiMonths() : _ConfigData2.default.Months()).map(function (month, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: month[0] },
                  month[1]
                );
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { style: { marginTop: "5px" } },
          _react2.default.createElement(
            'button',
            { className: 'btn btn-default btn-mini' },
            this.props.profile.draft._id ? editorContent.edit : editorContent.add
          ),
          ' ',
          _react2.default.createElement(
            'a',
            { href: 'javascript:void(0)', onClick: this.toResumeContent.bind(this) },
            editorContent.cancel
          )
        )
      ) : "";
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.status.edit_education && this.props.status.edit_education || !nextProps.status.deleting && this.props.status.deleting) _UserProfileActionCreators2.default.reloadUserProfile(this.props.user.key);

      return JSON.stringify(nextProps.profile.educations) !== JSON.stringify(this.props.profile.educations) || nextProps.status.edit_education !== this.props.status.edit_education || nextProps.editor !== this.props.editor || nextProps.lang !== this.props.lang;
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].user_profile.education.header;
      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive more" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          this.props.editor ? this.editorRender() : this.contentRender()
        )
      );
    }
  }]);

  return FormUserEducation;
}(_react.Component);

exports.default = FormUserEducation;

/*
<div className="text--expand"></div>
<a className="btn--expand">Read more..</a>

<input type="text" data-name="graduate_date" className="form-control form-control--mini form-datepicker" placeholder="End Date" defaultValue={this.props.profile.draft.graduate_date ? JQueryTool.changeDatePickerFormat(this.props.profile.draft.graduate_date) : ""} />
<input type="text" onChange={this.updateEducation.bind(this, "start_date")} ref="start_date" data-provide="datepicker" className="form-control form-control--mini form-datepicker" placeholder="Start Date" defaultValue={this.props.profile.draft.start_date} />

*/