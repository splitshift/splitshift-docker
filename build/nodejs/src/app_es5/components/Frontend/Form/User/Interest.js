'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _UserProfileActionCreators = require('../../../../actions/UserProfileActionCreators');

var _UserProfileActionCreators2 = _interopRequireDefault(_UserProfileActionCreators);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _ConfigData = require('../../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InterestShow = new Array(_ConfigData2.default.InterestList().length).fill(true);
var current_category = "";

var FormUserInterest = function (_Component) {
  _inherits(FormUserInterest, _Component);

  function FormUserInterest() {
    _classCallCheck(this, FormUserInterest);

    return _possibleConstructorReturn(this, (FormUserInterest.__proto__ || Object.getPrototypeOf(FormUserInterest)).apply(this, arguments));
  }

  _createClass(FormUserInterest, [{
    key: 'deleteInterest',
    value: function deleteInterest(id, evt) {

      if (!this.props.status.edit_interest) _ResourceActionCreators2.default.setEditor('edit_interest', true);

      var result = {
        interests: this.props.profile.interests.filter(function (interest, index) {
          return index != id;
        })
      };

      _UserProfileActionCreators2.default.updateProfile(result);
      _UserProfileActionCreators2.default.deleteInterest(this.props.user.token, this.props.profile.interests[id]);
    }
  }, {
    key: 'createInterest',
    value: function createInterest(evt) {
      evt.preventDefault();

      var category = this.props.profile.draft.category ? this.props.profile.draft.category : this.refs.category.value;

      var interestData = {
        category: category,
        value: category === 'Other' ? this.props.profile.draft.value : category
      };

      var result = {
        interests: this.props.profile.interests.concat(interestData)
      };

      if (interestData.category === 'Other') this.refs.other.value = "";

      _UserProfileActionCreators2.default.updateProfile(result);
      _UserProfileActionCreators2.default.createInterest(this.props.user.token, interestData);
    }
  }, {
    key: 'updateInterest',
    value: function updateInterest(name, evt) {

      if (!this.props.status.edit_interest) _ResourceActionCreators2.default.setEditor('edit_interest', true);

      _ResourceActionCreators2.default.setDraft(name, evt.target.value);
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var _this2 = this;

      var onEdit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      var interestContent = _configLang2.default[this.props.lang].user_profile.interest;
      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;

      return this.props.profile.interests ? this.props.profile.interests.map(function (interest, id) {
        return _react2.default.createElement(
          'div',
          { className: 'profile_interest', key: id },
          _react2.default.createElement('img', { src: '/images/interests/' + interest.category.toLowerCase() + '.svg' }),
          _react2.default.createElement(
            'h6',
            { className: 'mini' },
            interestContent.list[_ConfigData2.default.InterestList().indexOf(interest.value)]
          ),
          onEdit ? _react2.default.createElement(
            'a',
            { onClick: _this2.deleteInterest.bind(_this2, id), href: 'javascript:void(0)' },
            editorContent.delete
          ) : ""
        );
      }) : _react2.default.createElement('div', null);
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var interestContent = _configLang2.default[this.props.lang].user_profile.interest;

      InterestShow = new Array(_ConfigData2.default.InterestList().length).fill(true);

      if (this.props.profile && this.props.profile.interests) {
        this.props.profile.interests.map(function (interest, id) {

          if (interest.category !== 'Other') InterestShow[_ConfigData2.default.InterestList().indexOf(interest.category)] = false;
        });
      }

      return _react2.default.createElement(
        'form',
        { onSubmit: this.createInterest.bind(this) },
        _react2.default.createElement(
          'div',
          { className: 'row' },
          this.contentRender(true)
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-2', style: { paddingRight: "0px", paddingLeft: "0px" } },
            _react2.default.createElement(
              'select',
              { ref: 'category', className: 'form-control form-control--mini', onChange: this.updateInterest.bind(this, 'category'), defaultValue: this.props.profile.draft.category },
              _ConfigData2.default.InterestList().map(function (interest, id) {
                return InterestShow[id] ? _react2.default.createElement(
                  'option',
                  { key: id, value: interest },
                  interestContent.list[id]
                ) : '';
              })
            )
          ),
          (current_category === 'Other' || this.props.profile.draft.category === 'Other') && _react2.default.createElement(
            'div',
            { className: 'col-md-4', style: { paddingRight: "0px", paddingLeft: "0px" } },
            _react2.default.createElement('input', { ref: 'other', type: 'text', className: 'form-control form-control--mini', onChange: this.updateInterest.bind(this, 'value'), defaultValue: this.props.profile.draft.value })
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4', style: { paddingRight: "0px", paddingLeft: "0px" } },
            _react2.default.createElement(
              'button',
              { className: 'btn btn-default btn-mini--right' },
              editorContent.add
            ),
            ' '
          )
        )
      );
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      _ResourceActionCreators2.default.setDraft("category", _ConfigData2.default.InterestList()[InterestShow.indexOf(true)]);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      current_category = _ConfigData2.default.InterestList()[InterestShow.indexOf(true)];
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (!nextProps.status.edit_interest && this.props.status.edit_interest) _UserProfileActionCreators2.default.reloadUserProfile(this.props.user.key);

      return JSON.stringify(nextProps.profile.interests) !== JSON.stringify(this.props.profile.interests) || nextProps.status.edit_interest || nextProps.editor !== this.props.editor || nextProps.lang !== this.props.lang;
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].user_profile.interest.header;
      return _react2.default.createElement(
        'div',
        { className: 'box_style_detail_2' },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-12', style: { paddingRight: "0px", paddingLeft: "0px" } },
            this.props.editor ? this.editorRender() : this.contentRender()
          )
        )
      );
    }
  }]);

  return FormUserInterest;
}(_react.Component);

exports.default = FormUserInterest;