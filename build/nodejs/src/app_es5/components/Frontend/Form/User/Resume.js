'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Resume = function (_Component) {
  _inherits(Resume, _Component);

  function Resume(props) {
    _classCallCheck(this, Resume);

    var _this = _possibleConstructorReturn(this, (Resume.__proto__ || Object.getPrototypeOf(Resume)).call(this, props));

    _this.state = {
      img: 0
    };
    return _this;
  }

  _createClass(Resume, [{
    key: 'onPrint',
    value: function onPrint(evt) {
      window.print();
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      $("img").on('load', function () {
        _this2.setState({ img: _this2.state.img + 1 });
      });
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.state.img === 2) {
        window.print();
        this.setState({ img: 0 });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var resumeContent = _configLang2.default[this.props.language].resume;
      return _react2.default.createElement(
        'div',
        { className: 'resume' },
        _react2.default.createElement(
          'div',
          { className: 'resume__bio' },
          _react2.default.createElement(
            'div',
            { className: 'resume__avatar' },
            _react2.default.createElement('img', { src: this.props.user.profile_image ? this.props.user.profile_image : "" }),
            _react2.default.createElement(
              'h1',
              null,
              this.props.user.first_name,
              ' ',
              this.props.user.last_name
            ),
            _react2.default.createElement(
              'div',
              { style: { marginTop: "20px" } },
              _react2.default.createElement(
                'b',
                null,
                'Date of Birth:'
              ),
              ' ',
              this.props.user.birth_day ? _JQueryTool2.default.changeBirthDayFormat(this.props.user.birth_day) : "-",
              ' ',
              _react2.default.createElement('br', null),
              _react2.default.createElement(
                'b',
                null,
                'Home Address:'
              ),
              ' ',
              this.props.user.address ? this.props.user.address.information : "-",
              ' ',
              _react2.default.createElement('br', null),
              _react2.default.createElement(
                'b',
                null,
                'Email'
              ),
              ' ',
              this.props.user.email
            )
          ),
          this.props.user.interests.length > 0 ? _react2.default.createElement(
            'div',
            { className: 'resume__block' },
            _react2.default.createElement(
              'h1',
              null,
              resumeContent.interest
            ),
            _react2.default.createElement(
              'ul',
              null,
              this.props.user.interests.map(function (interest, id) {
                return _react2.default.createElement(
                  'li',
                  { key: id },
                  interest.value
                );
              })
            )
          ) : "",
          this.props.user.skills.length > 0 ? _react2.default.createElement(
            'div',
            { className: 'resume__block' },
            _react2.default.createElement(
              'h1',
              null,
              resumeContent.skill
            ),
            _react2.default.createElement(
              'ul',
              null,
              this.props.user.skills.map(function (skill, id) {
                return _react2.default.createElement(
                  'li',
                  { key: id },
                  skill
                );
              })
            )
          ) : ""
        ),
        _react2.default.createElement(
          'div',
          { className: 'resume__profile' },
          _react2.default.createElement(
            'div',
            { className: 'resume__block' },
            _react2.default.createElement(
              'h1',
              null,
              resumeContent.about_me
            ),
            _react2.default.createElement('p', { dangerouslySetInnerHTML: { __html: this.props.user.about.replace("\n", "<br />") } })
          ),
          _react2.default.createElement(
            'div',
            { className: 'resume__block' },
            _react2.default.createElement(
              'h1',
              null,
              resumeContent.work_exp
            ),
            _react2.default.createElement(
              'ul',
              { style: { listStyle: "none" } },
              this.props.user.experiences.map(function (experience, index) {
                return _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    'h2',
                    null,
                    experience.position,
                    _react2.default.createElement(
                      'small',
                      null,
                      '\u2013 ',
                      experience.workplace,
                      '\u2013',
                      experience.company_key ? experience.address.information : experience.address.address + ", " + experience.address.city + ", " + experience.address.district + ", " + experience.address.zip_code
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    null,
                    _JQueryTool2.default.changeDateFormat(experience.start_date),
                    ' - ',
                    experience.end_date ? _JQueryTool2.default.changeDateFormat(experience.end_date) : "Present",
                    '  ',
                    ' ',
                    ' (',
                    _JQueryTool2.default.diffExperienceDate(experience.start_date, experience.end_date),
                    ')'
                  )
                );
              })
            )
          ),
          this.props.user.educations.length > 0 ? _react2.default.createElement(
            'div',
            { className: 'resume__block' },
            _react2.default.createElement(
              'h1',
              null,
              'Education'
            ),
            _react2.default.createElement(
              'ul',
              null,
              this.props.user.educations.map(function (education, index) {
                return _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    'h2',
                    null,
                    education.field,
                    ' \u2013 ',
                    _react2.default.createElement(
                      'small',
                      null,
                      education.school
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    null,
                    _JQueryTool2.default.changeDateFormat(education.start_date),
                    ' - ',
                    education.graduate_date == "" ? "Present" : _JQueryTool2.default.changeDateFormat(education.graduate_date),
                    ' (',
                    _JQueryTool2.default.diffExperienceDate(education.start_date, education.graduate_date),
                    ')'
                  )
                );
              })
            )
          ) : "",
          this.props.user.courses.length > 0 ? _react2.default.createElement(
            'div',
            { className: 'resume__block' },
            _react2.default.createElement(
              'h1',
              null,
              resumeContent.course
            ),
            _react2.default.createElement(
              'ul',
              { style: { listStyle: "none" } },
              this.props.user.courses.map(function (course, index) {
                return _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    'h2',
                    null,
                    course.name,
                    ' \u2013 ',
                    _react2.default.createElement(
                      'small',
                      null,
                      course.description
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    null,
                    _JQueryTool2.default.changeDateFormat(course.start_date),
                    ' - ',
                    course.end_date == "" ? "Present" : _JQueryTool2.default.changeDateFormat(course.end_date),
                    ' (',
                    _JQueryTool2.default.diffExperienceDate(course.start_date, course.end_date),
                    ')'
                  )
                );
              })
            )
          ) : "",
          this.props.user.languages.length > 0 ? _react2.default.createElement(
            'div',
            { className: 'resume__block' },
            _react2.default.createElement(
              'h1',
              null,
              resumeContent.language
            ),
            _react2.default.createElement(
              'ul',
              { style: { listStyle: "none" } },
              this.props.user.languages.map(function (language, id) {
                return _react2.default.createElement(
                  'li',
                  { key: id },
                  _react2.default.createElement(
                    'b',
                    null,
                    language.language
                  ),
                  ' \u2013 ',
                  language.proficiency
                );
              })
            )
          ) : ""
        ),
        _react2.default.createElement(
          'div',
          { className: 'resume__banner' },
          _react2.default.createElement(
            'button',
            { className: 'no-print', onClick: this.onPrint.bind(this) },
            'Print'
          ),
          ' ',
          _react2.default.createElement('img', { src: '/images/splitshift_720.png' })
        )
      );
    }
  }]);

  return Resume;
}(_react.Component);

exports.default = Resume;