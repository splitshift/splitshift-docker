'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _UserProfileActionCreators = require('../../../../actions/UserProfileActionCreators');

var _UserProfileActionCreators2 = _interopRequireDefault(_UserProfileActionCreators);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _ConfigData = require('../../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var count = 0;

var FormUserWorkExperience = function (_Component) {
  _inherits(FormUserWorkExperience, _Component);

  function FormUserWorkExperience() {
    _classCallCheck(this, FormUserWorkExperience);

    return _possibleConstructorReturn(this, (FormUserWorkExperience.__proto__ || Object.getPrototypeOf(FormUserWorkExperience)).apply(this, arguments));
  }

  _createClass(FormUserWorkExperience, [{
    key: 'createExperience',
    value: function createExperience() {
      var result = this.props.profile.draft;

      if (typeof result.start_date === 'undefined') result.start_date = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;

      if (typeof result.end_date === 'undefined' || result.end_date || typeof result.current_job === 'undefined' || !result.current_job) result.end_date = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

      result.city = result.city || '-';
      result.district = result.district || '-';
      result.workplace = result.workplace || '-';
      result.zip_code = result.zip_code || '-';

      _UserProfileActionCreators2.default.createExperience(this.props.user.token, result);
    }
  }, {
    key: 'editExperience',
    value: function editExperience() {
      var result = this.props.profile.draft;
      result.city = result.city || '-';
      result.district = result.district || '-';
      result.workplace = result.workplace || '-';
      result.zip_code = result.zip_code || '-';
      _UserProfileActionCreators2.default.editExperience(this.props.user.token, result);
    }
  }, {
    key: 'deleteExperience',
    value: function deleteExperience(id) {
      _UserProfileActionCreators2.default.deleteExperience(this.props.user.token, this.props.profile.experiences[id]);
    }
  }, {
    key: 'setDraftEditExperience',
    value: function setDraftEditExperience(id) {
      var selectedExperience = this.props.profile.experiences[id];
      var currentAddress = selectedExperience.address;
      selectedExperience = Object.assign({}, selectedExperience, currentAddress);

      if (typeof selectedExperience.end_date === 'undefined') selectedExperience.current_job = true;

      _ResourceActionCreators2.default.setOldDraft(selectedExperience);
      this.setEditor("edit_experience");
    }
  }, {
    key: 'setEditor',
    value: function setEditor(name, evt) {
      _ResourceActionCreators2.default.setEditor(name, true);
    }
  }, {
    key: 'toResumeContent',
    value: function toResumeContent() {
      _ResourceActionCreators2.default.cancelEditor("edit_experience", false);
    }
  }, {
    key: 'updateExperience',
    value: function updateExperience(name, evt) {

      if (name == 'workplace') _ResourceActionCreators2.default.setDraft('company_key', '');

      var value = evt.target.value;

      if (name == 'current_job') {
        value = evt.target.checked;
        _ResourceActionCreators2.default.setDraft("end_date", "");
      } else if (name == 'start_date') value = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;else if (name == 'end_date') value = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

      _ResourceActionCreators2.default.setDraft(name, value);
    }
  }, {
    key: 'selectChooseCompany',
    value: function selectChooseCompany(name, key, evt) {
      this.refs.workplace.value = name;
      _ResourceActionCreators2.default.setDraft('workplace', name);
      _ResourceActionCreators2.default.setDraft('company_key', key);
      $(".search-hint__list").removeClass("opened");
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.status.edit_experience && this.props.status.edit_experience || !nextProps.status.deleting && this.props.status.deleting) _UserProfileActionCreators2.default.reloadUserProfile(this.props.user.key);

      return JSON.stringify(nextProps.profile.experiences) !== JSON.stringify(this.props.profile.experiences) || nextProps.status.edit_experience || nextProps.status.edit_experience !== this.props.status.edit_experience || nextProps.editor !== this.props.editor || nextProps.lang !== this.props.lang;
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var _this2 = this;

      var onEdit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var experiencesList = this.props.profile && this.props.profile.experiences ? this.props.profile.experiences.map(function (experience, id) {
        return _react2.default.createElement(
          'li',
          { key: id },
          _react2.default.createElement(
            'p',
            null,
            experience.company_key ? _react2.default.createElement(
              _reactRouter.Link,
              { to: '/th/company/' + experience.company_key },
              experience.position
            ) : _react2.default.createElement(
              'b',
              null,
              experience.position
            ),
            ' ',
            ' ',
            onEdit ? _react2.default.createElement(
              'span',
              null,
              '(',
              _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: _this2.setDraftEditExperience.bind(_this2, id) },
                editorContent.edit
              ),
              ' ',
              _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: _this2.deleteExperience.bind(_this2, id) },
                editorContent.delete
              ),
              ')'
            ) : "",
            ' ',
            _react2.default.createElement('br', null),
            experience.workplace,
            ' ',
            _react2.default.createElement('br', null),
            experience.company_key ? experience.address.information : experience.address.address + ", " + experience.address.city + ", " + experience.address.district + ", " + experience.address.zip_code,
            _react2.default.createElement('br', null),
            _react2.default.createElement(
              'span',
              { style: { color: "#aaa" } },
              _JQueryTool2.default.changeDateFormat(experience.start_date, _this2.props.lang),
              ' - ',
              experience.end_date ? _JQueryTool2.default.changeDateFormat(experience.end_date, _this2.props.lang) : _this2.props.lang === 'th' ? 'ปัจจุบัน' : "Present",
              '  ',
              ' ',
              '(',
              _JQueryTool2.default.diffExperienceDate(experience.start_date, experience.end_date, _this2.props.lang),
              ')'
            )
          )
        );
      }) : "";

      return _react2.default.createElement(
        'ul',
        null,
        experiencesList,
        _react2.default.createElement('div', { className: 'text--expand' })
      );
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      var expContent = _configLang2.default[this.props.lang].user_profile.workexp;

      return _react2.default.createElement(
        'div',
        null,
        this.props.status.edit_experience ? this.experienceForm() : _react2.default.createElement(
          'button',
          { className: 'btn btn-default btn-mini', onClick: this.setEditor.bind(this, "edit_experience") },
          expContent.add
        ),
        this.props.status.edit_experience ? "" : this.contentRender(true)
      );
    }
  }, {
    key: 'experienceForm',
    value: function experienceForm() {
      var _this3 = this;

      count = 0;
      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var editorExpContent = editorContent.workexp;

      return this.props.profile ? _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: 'search-hint' },
          _react2.default.createElement('input', { type: 'text', ref: 'workplace', onChange: this.updateExperience.bind(this, "workplace"), className: 'search-hint__input form-control form-control--mini', placeholder: editorExpContent[0], defaultValue: this.props.profile.draft.workplace }),
          _react2.default.createElement(
            'ul',
            { className: 'search-hint__list' },
            this.props.profile.company_list.map(function (company, index) {
              if (company.profile.company_name.indexOf(_this3.props.profile.draft.workplace) === -1 || count > 10) return "";
              count++;
              return _react2.default.createElement(
                'li',
                { key: index, onClick: _this3.selectChooseCompany.bind(_this3, company.profile.company_name, company.key), className: 'search-hint__item' },
                company.profile.company_name
              );
            })
          )
        ),
        _react2.default.createElement('input', { type: 'text', onChange: this.updateExperience.bind(this, "position"), className: 'form-control form-control--mini', placeholder: editorExpContent[1], defaultValue: this.props.profile.draft.position }),
        this.props.profile.draft.company_key ? "" : _react2.default.createElement(
          'span',
          null,
          _react2.default.createElement('input', { type: 'text', onChange: this.updateExperience.bind(this, "address"), className: 'form-control form-control--mini', placeholder: editorExpContent[2], defaultValue: this.props.profile.draft.address ? this.props.profile.draft.address : "" }),
          _react2.default.createElement('input', { type: 'text', onChange: this.updateExperience.bind(this, "district"), className: 'form-control form-control--mini', placeholder: editorExpContent[3], defaultValue: this.props.profile.draft.address ? this.props.profile.draft.district : "" }),
          _react2.default.createElement('input', { type: 'text', onChange: this.updateExperience.bind(this, "city"), className: 'form-control form-control--mini', placeholder: editorExpContent[4], defaultValue: this.props.profile.draft.address ? this.props.profile.draft.city : "" }),
          _react2.default.createElement('input', { type: 'text', onChange: this.updateExperience.bind(this, "zip_code"), className: 'form-control form-control--mini', placeholder: editorExpContent[5], defaultValue: this.props.profile.draft.address ? this.props.profile.draft.zip_code : "" })
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-12' },
            _react2.default.createElement(
              'p',
              { style: { paddingTop: "5px", margin: "0 0 10px" } },
              editorExpContent[6]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini ', ref: 'start_date_year', onChange: this.updateExperience.bind(this, 'start_date'), defaultValue: this.props.profile.draft.start_date ? _JQueryTool2.default.getYear(this.props.profile.draft.start_date) : "" },
              _ConfigData2.default.Years().map(function (year, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: year },
                  _this3.props.lang === 'th' ? year + 543 : year
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini ', ref: 'start_date_month', onChange: this.updateExperience.bind(this, 'start_date'), defaultValue: this.props.profile.draft.start_date ? _JQueryTool2.default.getMonth(this.props.profile.draft.start_date) : "" },
              (this.props.lang === 'th' ? _ConfigData2.default.ThaiMonths() : _ConfigData2.default.Months()).map(function (month, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: month[0] },
                  month[1]
                );
              })
            )
          )
        ),
        this.props.profile.draft.current_job ? "" : _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-12' },
            _react2.default.createElement(
              'p',
              { style: { paddingTop: "5px", margin: "0 0 10px" } },
              editorExpContent[7]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini', ref: 'end_date_year', onChange: this.updateExperience.bind(this, 'end_date'), defaultValue: this.props.profile.draft.end_date ? _JQueryTool2.default.getYear(this.props.profile.draft.end_date) : "" },
              _ConfigData2.default.Years().map(function (year, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: year },
                  _this3.props.lang === 'th' ? year + 543 : year
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-4 col-xs-6' },
            _react2.default.createElement(
              'select',
              { className: 'form-control form-control--mini ', ref: 'end_date_month', onChange: this.updateExperience.bind(this, 'end_date'), defaultValue: this.props.profile.draft.end_date ? _JQueryTool2.default.getMonth(this.props.profile.draft.end_date) : "" },
              (this.props.lang === 'th' ? _ConfigData2.default.ThaiMonths() : _ConfigData2.default.Months()).map(function (month, index) {
                return _react2.default.createElement(
                  'option',
                  { key: index, value: month[0] },
                  month[1]
                );
              })
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'checkbox--remember', style: { float: "right" } },
          _react2.default.createElement('input', { type: 'checkbox', onClick: this.updateExperience.bind(this, 'current_job'), defaultChecked: this.props.profile.draft.current_job ? true : false }),
          ' ',
          editorExpContent[8]
        ),
        _react2.default.createElement(
          'div',
          { style: { marginTop: "5px" } },
          _react2.default.createElement(
            'button',
            { onClick: this.props.profile.draft._id ? this.editExperience.bind(this) : this.createExperience.bind(this), className: 'btn btn-default btn-mini' },
            this.props.profile.draft._id ? editorContent.edit : editorContent.add
          ),
          ' ',
          _react2.default.createElement(
            'a',
            { onClick: this.toResumeContent.bind(this), href: 'javascript:void(0)' },
            editorContent.cancel
          )
        )
      ) : "";
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.props.status.edit_experience) {
        $('.search-hint').each(function () {
          var $input = $(this).children('.search-hint__input');
          var $list = $(this).children('.search-hint__list');

          $input.focus(function () {
            $list.addClass('opened');
          });

          $input.blur(function () {
            setTimeout(function () {
              $list.removeClass('opened');
            }, 500);
          });
        });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].user_profile.workexp.header;
      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          this.props.editor ? this.editorRender() : this.contentRender()
        )
      );
    }
  }]);

  return FormUserWorkExperience;
}(_react.Component);

/*
<div className="text--expand">
  <li>
    <p>
      <b>Head Sommelier</b> <br />
      U Sathorn Bangkok – J’AIME Restaurant <br />
      <span style={{ color:"#aaa" }}>Feb. 2015 to Present (1 year 5 months)</span>
    </p>
  </li>
</div>
<a className="btn--expand">Read more..</a>

<input type="text" className="form-control form-control--mini form-datepicker" data-name="start_date" placeholder="Start Date" defaultValue={this.props.profile.draft.start_date ? JQueryTool.changeDatePickerFormat(this.props.profile.draft.start_date) : ''} />
*/

exports.default = FormUserWorkExperience;