'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _Connection = require('./Connection');

var _Connection2 = _interopRequireDefault(_Connection);

var _UserProfileActionCreators = require('../../../../actions/UserProfileActionCreators');

var _UserProfileActionCreators2 = _interopRequireDefault(_UserProfileActionCreators);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _SocialActionCreators = require('../../../../actions/SocialActionCreators');

var _SocialActionCreators2 = _interopRequireDefault(_SocialActionCreators);

var _MessageActionCreators = require('../../../../actions/MessageActionCreators');

var _MessageActionCreators2 = _interopRequireDefault(_MessageActionCreators);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var mapPopupRender = false;
var cancelEdit = false;

var FormUserProfileCard = function (_Component) {
  _inherits(FormUserProfileCard, _Component);

  function FormUserProfileCard() {
    _classCallCheck(this, FormUserProfileCard);

    return _possibleConstructorReturn(this, (FormUserProfileCard.__proto__ || Object.getPrototypeOf(FormUserProfileCard)).apply(this, arguments));
  }

  _createClass(FormUserProfileCard, [{
    key: 'toEditor',
    value: function toEditor(evt) {
      cancelEdit = false;
      _ResourceActionCreators2.default.setEditor('edit_profile_card', true);
    }
  }, {
    key: 'cancelEditor',
    value: function cancelEditor(evt) {
      cancelEdit = true;
      _ResourceActionCreators2.default.setEditor('edit_profile_card', false);
    }
  }, {
    key: 'toEdit',
    value: function toEdit(evt) {

      var result = {
        first_name: this.props.profile.first_name,
        last_name: this.props.profile.last_name,
        birth_day: this.props.profile.birth_day || "",
        gender: this.props.profile.gender || "",
        address: {
          information: this.props.profile.address.information,
          lat: 1,
          lng: 1
        }
      };

      _UserProfileActionCreators2.default.updateInfo(this.props.user.token, result);
    }
  }, {
    key: 'loadMessage',
    value: function loadMessage(evt) {
      _MessageActionCreators2.default.chooseOwner(this.props.user);
      _MessageActionCreators2.default.chooseOther(this.props.profile);

      _MessageActionCreators2.default.getMessageWithPeople(this.props.user.token, this.props.profile.key);
    }
  }, {
    key: 'onUpdateProfile',
    value: function onUpdateProfile(name, evt) {

      var result = {};

      if (name === 'address') {
        result = {
          address: {
            information: evt.target.value,
            lat: 1,
            lng: 1
          }
        };
      } else if (name === 'name') {
        result = {
          first_name: evt.target.value.split(' ')[0],
          last_name: evt.target.value.split(' ')[1]
        };
      } else result[name] = evt.target.value;

      _UserProfileActionCreators2.default.updateProfile(result);
    }
  }, {
    key: 'onFollowPeople',
    value: function onFollowPeople(evt) {
      _SocialActionCreators2.default.putFollow(this.props.user.token, this.props.profile.key);
    }
  }, {
    key: 'onConnectRequest',
    value: function onConnectRequest(evt) {
      _SocialActionCreators2.default.postConnectRequest(this.props.user.token, this.props.profile.key);
    }
  }, {
    key: 'onDisconnectRequest',
    value: function onDisconnectRequest(evt) {
      _SocialActionCreators2.default.deleteConnect(this.props.user.token, this.props.profile.key);
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      var editorContent = _configLang2.default[this.props.lang].user_profile.editor.card;

      return this.props.profile ? _react2.default.createElement(
        'div',
        { className: 'profile-card__info', style: { width: "100%", marginBottom: "20px" } },
        _react2.default.createElement(
          'h3',
          null,
          editorContent[0]
        ),
        _react2.default.createElement(
          'h3',
          null,
          _react2.default.createElement('input', { type: 'text', className: 'form-control', onChange: this.onUpdateProfile.bind(this, 'name'), placeholder: editorContent[1], defaultValue: this.props.profile.first_name + ' ' + this.props.profile.last_name })
        ),
        _react2.default.createElement(
          'p',
          null,
          _react2.default.createElement('input', { type: 'text', className: 'form-control', onChange: this.onUpdateProfile.bind(this, 'address'), placeholder: editorContent[2], defaultValue: this.props.profile.address && this.props.profile.address.information }),
          _react2.default.createElement(
            'p',
            null,
            editorContent[3]
          )
        ),
        ' ',
        _react2.default.createElement(
          'p',
          null,
          _react2.default.createElement('input', { type: 'text', className: 'form-control', onChange: this.onUpdateProfile.bind(this, 'birth_day'), placeholder: editorContent[4], defaultValue: this.props.profile.birth_day && _JQueryTool2.default.changeBirthDayDatePickerFormat(this.props.profile.birth_day) }),
          _react2.default.createElement(
            'p',
            null,
            editorContent[5]
          )
        ),
        ' ',
        _react2.default.createElement(
          'p',
          null,
          _react2.default.createElement(
            'select',
            { className: 'form-control', onChange: this.onUpdateProfile.bind(this, 'gender'), defaultValue: this.props.profile.gender },
            _react2.default.createElement(
              'option',
              { value: '' },
              editorContent[6]
            ),
            _react2.default.createElement(
              'option',
              { value: 'male' },
              editorContent[7]
            ),
            _react2.default.createElement(
              'option',
              { value: 'female' },
              editorContent[8]
            )
          )
        ),
        ' ',
        _react2.default.createElement(
          'button',
          { className: 'btn btn-default', onClick: this.toEdit.bind(this) },
          editorContent[9]
        ),
        ' ',
        _react2.default.createElement(
          'a',
          { href: 'javascript:void(0)', onClick: this.cancelEditor.bind(this) },
          editorContent[10]
        )
      ) : "";
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var menus = _configLang2.default[this.props.lang].user_profile.menus;

      return _react2.default.createElement(
        'div',
        { className: 'profile-card__info' },
        _react2.default.createElement(
          'h3',
          null,
          this.props.profile.first_name,
          ' ',
          this.props.profile.last_name,
          ' ',
          _react2.default.createElement('br', null),
          _react2.default.createElement(
            'span',
            { className: 'job' },
            this.props.profile.experiences && this.props.profile.experiences.length ? this.props.profile.occupation.position + (this.props.profile.occupation.experience_id == "" ? " (Past)" : "") : ''
          )
        ),
        _react2.default.createElement(
          'p',
          null,
          this.props.profile.occupation && this.props.profile.occupation.address ? this.props.profile.occupation.company_key ? _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              _reactRouter.Link,
              { to: '/th/company/' + this.props.profile.occupation.company_key },
              (this.props.profile.occupation.workplace ? this.props.profile.occupation.workplace + ", " : "") + this.props.profile.occupation.address.city
            ),
            this.props.profile.occupation.address && this.props.profile.occupation.address.location ? _react2.default.createElement(
              'a',
              { className: 'popup-gmaps', href: "https://maps.google.com/maps?q=" + this.props.profile.occupation.address.location[1] + ',' + this.props.profile.occupation.address.location[0] },
              _react2.default.createElement(
                'span',
                null,
                _react2.default.createElement('i', { className: 'fa fa-map-marker', 'aria-hidden': 'true', style: { fontSize: "24px" } })
              )
            ) : ""
          ) : (this.props.profile.occupation.workplace ? this.props.profile.occupation.workplace + ", " : "") + this.props.profile.occupation.address.city : "",
          _react2.default.createElement('br', null),
          this.props.editor && menus[0] + ": " + (this.props.profile.address && this.props.profile.address.information)
        ),
        _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            _reactRouter.Link,
            { to: '/th/user/' + this.props.profile.key, onClick: this.props.triggerButton.bind(null), className: 'btn btn-default reverse' },
            menus[1]
          ),
          ' ',
          _react2.default.createElement(
            _reactRouter.Link,
            { to: '/th/user/' + this.props.profile.key + '/photo', onClick: this.props.triggerButton.bind(null), className: 'btn btn-default reverse' },
            menus[2]
          ),
          ' ',
          typeof this.props.user.token !== 'undefined' && (this.props.profile.key === this.props.user.key || this.props.user.type !== 'Employee') && _react2.default.createElement(
            'a',
            { href: '/' + this.props.lang + '/user/' + this.props.profile.key + '/resume', className: 'btn btn-default reverse', target: '_blank' },
            menus[3]
          ),
          ' ',
          this.props.other && typeof this.props.user.token !== 'undefined' && typeof this.props.profile.key !== 'undefined' && this.props.user.token && _react2.default.createElement(
            'span',
            null,
            (!this.props.profile.connected || this.props.user.type !== 'Employee') && _react2.default.createElement(
              'button',
              { onClick: this.loadMessage.bind(this), className: 'btn btn-default reverse', 'data-toggle': 'modal', 'data-target': '#myModal__message' },
              menus[4]
            ),
            ' ',
            _react2.default.createElement(
              'button',
              { className: this.props.profile.following ? 'btn btn-default reverse' : 'btn btn-default', onClick: this.onFollowPeople.bind(this) },
              this.props.profile.following ? menus[5] : menus[6]
            ),
            ' ',
            ' ',
            this.props.user.type === 'Employee' && _react2.default.createElement(
              'button',
              { className: this.props.profile.connected && this.props.profile.connected !== 'requesting' ? 'btn btn-default reverse' : 'btn btn-default', onClick: this.props.profile.connected && this.props.profile.connected !== 'requesting' ? this.onConnectRequest.bind(this) : this.onDisconnectRequest.bind(this) },
              this.props.profile.connected ? this.props.profile.connected === 'requesting' ? menus[7] : menus[8] : menus[9]
            ),
            ' '
          )
        ),
        _react2.default.createElement(
          'p',
          { className: 'connection' },
          _react2.default.createElement(
            'a',
            { href: '#', 'data-toggle': 'modal', 'data-target': '#myModal__connections' },
            this.props.profile.connected_count,
            ' ',
            menus[10],
            this.props.lang === 'en' && this.props.profile.connected_count > 1 ? 's' : ''
          )
        ),
        this.props.profile.key && _react2.default.createElement(_Connection2.default, { profile: this.props.profile }),
        this.props.editor && _react2.default.createElement(
          'div',
          { className: 'profile-card__editor' },
          _react2.default.createElement(
            _reactRouter.Link,
            { to: '/' + this.props.lang + '/user/' + this.props.profile.key },
            _react2.default.createElement(
              'button',
              { className: 'btn btn-default' },
              menus[11]
            )
          ),
          ' ',
          _react2.default.createElement(
            'button',
            { className: 'btn btn-default', onClick: this.toEditor.bind(this) },
            menus[12]
          )
        )
      );
    }
  }, {
    key: 'changeProfileImage',
    value: function changeProfileImage(evt) {
      _UserProfileActionCreators2.default.uploadProfileImage(this.props.user.token, evt.target.files[0]);
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      mapPopupRender = false;
      _JQueryTool2.default.popupGallery();
      this.toEditor();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (!mapPopupRender && this.props.profile.occupation && this.props.profile.occupation.address && this.props.profile.occupation.address.location) {
        $('.popup-gmaps').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false
        });

        mapPopupRender = true;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      return _react2.default.createElement(
        'div',
        { className: 'profile-card__container' },
        _react2.default.createElement(
          'div',
          { className: 'profile-card__image' },
          _react2.default.createElement(
            'div',
            { className: 'popup-gallery' },
            _react2.default.createElement(
              'a',
              { href: this.props.profile.profile_image ? this.props.profile.profile_image : "/images/avatar-image.jpg" },
              _react2.default.createElement('div', { className: 'profile-image', style: { background: "url(" + (this.props.profile.profile_image || "/images/avatar.jpg") + ") no-repeat top center / cover" } })
            )
          ),
          this.props.editor && _react2.default.createElement(
            'div',
            { className: 'profile__edit' },
            _react2.default.createElement(
              'p',
              { onClick: _JQueryTool2.default.triggerUpload.bind(this, "uploadImage") },
              _react2.default.createElement('i', { className: 'fa fa-camera' }),
              ' ',
              editorContent.upload.profile
            ),
            _react2.default.createElement('input', { id: 'uploadImage', type: 'file', onChange: this.changeProfileImage.bind(this), style: { display: "none" } })
          )
        ),
        this.props.profile.key && _react2.default.createElement(
          'div',
          { className: 'profile-card__content' },
          this.props.status.edit_profile_card && this.props.editor ? this.editorRender() : this.contentRender()
        )
      );
    }
  }]);

  return FormUserProfileCard;
}(_react.Component);

exports.default = FormUserProfileCard;

//  { this.props.profile.occupation }