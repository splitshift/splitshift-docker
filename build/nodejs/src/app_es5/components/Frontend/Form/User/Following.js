'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _SocialActionCreators = require('../../../../actions/SocialActionCreators');

var _SocialActionCreators2 = _interopRequireDefault(_SocialActionCreators);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormUserFollowing = function (_Component) {
  _inherits(FormUserFollowing, _Component);

  function FormUserFollowing(argument) {
    _classCallCheck(this, FormUserFollowing);

    var _this = _possibleConstructorReturn(this, (FormUserFollowing.__proto__ || Object.getPrototypeOf(FormUserFollowing)).call(this, argument));

    _this.state = {
      num: 9
    };
    return _this;
  }

  _createClass(FormUserFollowing, [{
    key: 'SeeMoreFollower',
    value: function SeeMoreFollower() {
      this.setState({ num: this.state.num + 3 });
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].user_profile.following;
      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header,
          ' '
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'ul',
            { style: { marginBottom: "0px" } },
            _react2.default.createElement(
              'li',
              null,
              _react2.default.createElement(
                'div',
                { className: this.props.responsive ? "people--responsive" : "row" },
                this.props.profile.followers.slice(0, this.state.num).map(function (follower, index) {
                  return _react2.default.createElement(
                    'div',
                    { key: index, className: 'col-md-4', style: { padding: "0px 2px" } },
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: follower.type === 'Employee' ? '/th/user/' + follower.key : '/th/company/' + follower.key },
                      _react2.default.createElement(
                        'div',
                        { className: 'people__pic' },
                        _react2.default.createElement('img', { src: follower.profile_image ? follower.profile_image : "/images/avatar-image.jpg" }),
                        _react2.default.createElement(
                          'div',
                          { className: 'people__name' },
                          _react2.default.createElement(
                            'span1',
                            null,
                            follower.name
                          )
                        )
                      )
                    )
                  );
                }),
                this.props.profile.followers.length > this.state.num ? _react2.default.createElement(
                  'div',
                  { className: 'row' },
                  _react2.default.createElement(
                    'div',
                    { className: 'col-md-12' },
                    _react2.default.createElement(
                      'a',
                      { href: 'javascript:void(0)', onClick: this.SeeMoreFollower.bind(this), style: { color: "#aaa", textDecoration: "underline", float: "right" } },
                      this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
                    )
                  )
                ) : ""
              )
            )
          )
        )
      );
    }
  }]);

  return FormUserFollowing;
}(_react.Component);

exports.default = FormUserFollowing;

/*

*/