'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _UserProfileActionCreators = require('../../../../actions/UserProfileActionCreators');

var _UserProfileActionCreators2 = _interopRequireDefault(_UserProfileActionCreators);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var initialAboutTextarea = void 0;
var expanding = false;

var FormUserAbout = function (_Component) {
  _inherits(FormUserAbout, _Component);

  function FormUserAbout() {
    _classCallCheck(this, FormUserAbout);

    return _possibleConstructorReturn(this, (FormUserAbout.__proto__ || Object.getPrototypeOf(FormUserAbout)).apply(this, arguments));
  }

  _createClass(FormUserAbout, [{
    key: 'toEdit',
    value: function toEdit() {
      _UserProfileActionCreators2.default.updateAbout(this.props.user.token, {
        about: this.props.profile.about
      });
    }
  }, {
    key: 'onUpdateProfile',
    value: function onUpdateProfile(evt) {

      if (!this.props.status.edit_about) {
        _ResourceActionCreators2.default.setEditor('edit_about', true);
        $(this.refs.about).expanding();
        $(this.refs.about).focus();
      }

      _UserProfileActionCreators2.default.updateProfile({ about: evt.target.value });
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var aboutmePlaceholder = _configLang2.default[this.props.lang].user_profile.aboutme_placeholder;
      return this.props.profile && typeof this.props.profile.about !== "undefined" ? _react2.default.createElement(
        'div',
        null,
        this.contentRender(this.props.profile.draft.about || this.props.profile.about),
        _react2.default.createElement('textarea', { id: 'abc', ref: 'about', className: 'form-control form-control--mini', onChange: this.onUpdateProfile.bind(this), defaultValue: this.props.profile.about, placeholder: aboutmePlaceholder }),
        _react2.default.createElement(
          'div',
          { style: { marginTop: "5px" } },
          _react2.default.createElement(
            'button',
            { className: 'btn btn-default btn-mini', onClick: this.toEdit.bind(this) },
            editorContent.add
          )
        )
      ) : "";
    }
  }, {
    key: 'contentRender',
    value: function contentRender(text) {

      var draftText = text;
      draftText = draftText.replace(/\n/g, "<br/>");

      return _react2.default.createElement('p', { style: { wordWrap: "break-word" }, dangerouslySetInnerHTML: { __html: draftText } });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      expanding = false;
      $(this.refs.about).expanding();
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return nextProps.profile.about !== this.props.profile.about || nextProps.profile.draft.about !== this.props.profile.draft.about || nextProps.editor !== this.props.editor || nextProps.lang !== this.props.lang;
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].user_profile.aboutme;

      return _react2.default.createElement(
        'div',
        { className: 'box_style_detail_2' },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-12', style: { paddingRight: "0px", paddingLeft: "0px" } },
            this.props.editor ? this.editorRender() : this.contentRender(this.props.profile.about)
          )
        )
      );
    }
  }]);

  return FormUserAbout;
}(_react.Component);

exports.default = FormUserAbout;