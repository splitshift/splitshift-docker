'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _UserProfileActionCreators = require('../../../../actions/UserProfileActionCreators');

var _UserProfileActionCreators2 = _interopRequireDefault(_UserProfileActionCreators);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormUserLanguage = function (_Component) {
  _inherits(FormUserLanguage, _Component);

  function FormUserLanguage() {
    _classCallCheck(this, FormUserLanguage);

    return _possibleConstructorReturn(this, (FormUserLanguage.__proto__ || Object.getPrototypeOf(FormUserLanguage)).apply(this, arguments));
  }

  _createClass(FormUserLanguage, [{
    key: 'verifyData',
    value: function verifyData(result) {
      return typeof result.language !== 'undefined' && result.language.trim() !== '';
    }
  }, {
    key: 'createLanguage',
    value: function createLanguage(evt) {
      evt.preventDefault();

      var result = this.props.profile.draft;

      if (typeof result.proficiency === 'undefined') result.proficiency = this.refs.proficiency.value;

      if (this.verifyData(result)) _UserProfileActionCreators2.default.createLanguage(this.props.user.token, result);else alert("Please type your language.");
    }
  }, {
    key: 'editLanguage',
    value: function editLanguage(evt) {
      evt.preventDefault();

      if (this.verifyData(this.props.profile.draft)) _UserProfileActionCreators2.default.editLanguage(this.props.user.token, this.props.profile.draft);else alert("Please type your language.");
    }
  }, {
    key: 'deleteLanguage',
    value: function deleteLanguage(id) {

      var result = {
        languages: this.props.profile.languages.filter(function (language, index) {
          return index != id;
        })
      };

      _UserProfileActionCreators2.default.updateProfile(result);
      _UserProfileActionCreators2.default.deleteLanguage(this.props.user.token, this.props.profile.languages[id]);
    }
  }, {
    key: 'updateLanguage',
    value: function updateLanguage(name, evt) {
      _ResourceActionCreators2.default.setDraft(name, evt.target.value);
    }
  }, {
    key: 'setDraftEditCourse',
    value: function setDraftEditCourse(id) {
      _ResourceActionCreators2.default.setOldDraft(this.props.profile.languages[id]);
      this.setEditor("edit_language");
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var _this2 = this;

      var onEdit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;

      var languageList = this.props.profile.languages ? this.props.profile.languages.map(function (language, id) {
        return _react2.default.createElement(
          'li',
          { key: id },
          _react2.default.createElement(
            'p',
            null,
            language.language,
            ' ',
            _react2.default.createElement('br', null),
            _react2.default.createElement(
              'span',
              { style: { color: "#aaa" } },
              language.proficiency
            ),
            onEdit ? _react2.default.createElement(
              'span',
              null,
              _react2.default.createElement('br', null),
              ' ',
              _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: _this2.setDraftEditCourse.bind(_this2, id) },
                editorContent.edit
              ),
              ' ',
              _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: _this2.deleteLanguage.bind(_this2, id) },
                editorContent.delete
              )
            ) : ""
          )
        );
      }) : "";

      return _react2.default.createElement(
        'ul',
        null,
        languageList
      );
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      var addContent = _configLang2.default[this.props.lang].user_profile.language.add;
      return _react2.default.createElement(
        'div',
        null,
        this.props.status.edit_language ? this.languageForm() : _react2.default.createElement(
          'button',
          { className: 'btn btn-default btn-mini', onClick: this.setEditor.bind(this, "edit_language") },
          addContent
        ),
        this.props.status.edit_language ? "" : this.contentRender(true)
      );
    }
  }, {
    key: 'languageForm',
    value: function languageForm() {
      var editorContent = _configLang2.default[this.props.lang].user_profile.editor;
      var editorLanguageContent = editorContent.language;
      return this.props.profile ? _react2.default.createElement(
        'form',
        { onSubmit: this.props.profile.draft._id ? this.editLanguage.bind(this) : this.createLanguage.bind(this) },
        _react2.default.createElement('input', { type: 'text', onChange: this.updateLanguage.bind(this, 'language'), className: 'form-control form-control--mini', defaultValue: this.props.profile.draft.language, placeholder: editorLanguageContent[0] }),
        _react2.default.createElement(
          'select',
          { ref: 'proficiency', onChange: this.updateLanguage.bind(this, 'proficiency'), className: 'form-control form-control--mini', defaultValue: this.props.profile.draft.proficiency },
          _react2.default.createElement(
            'option',
            { value: 'Native Language' },
            editorLanguageContent[1]
          ),
          _react2.default.createElement(
            'option',
            { value: 'Fluent' },
            editorLanguageContent[2]
          ),
          _react2.default.createElement(
            'option',
            { value: 'Advanced' },
            editorLanguageContent[3]
          ),
          _react2.default.createElement(
            'option',
            { value: 'Intermediate' },
            editorLanguageContent[4]
          ),
          _react2.default.createElement(
            'option',
            { value: 'Beginner' },
            editorLanguageContent[5]
          )
        ),
        _react2.default.createElement(
          'div',
          { style: { marginTop: "5px" } },
          _react2.default.createElement(
            'button',
            { className: 'btn btn-default  btn-mini' },
            this.props.profile.draft._id ? editorContent.edit : editorContent.add
          ),
          ' ',
          _react2.default.createElement(
            'a',
            { onClick: this.toResumeContent.bind(this), href: 'javascript:void(0)' },
            editorContent.cancel
          )
        )
      ) : "";
    }
  }, {
    key: 'setEditor',
    value: function setEditor(name, evt) {
      _ResourceActionCreators2.default.setEditor(name, true);
    }
  }, {
    key: 'toResumeContent',
    value: function toResumeContent() {
      _ResourceActionCreators2.default.setEditor('edit_language', false);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.status.edit_language && this.props.status.edit_language) _UserProfileActionCreators2.default.reloadUserProfile(this.props.user.key);

      return JSON.stringify(nextProps.profile.languages) !== JSON.stringify(this.props.profile.languages) || nextProps.status.edit_language !== this.props.status.edit_language || nextProps.editor !== this.props.editor || nextProps.lang !== this.props.lang;
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].user_profile.language.header;
      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          this.props.editor ? this.editorRender() : this.contentRender()
        )
      );
    }
  }]);

  return FormUserLanguage;
}(_react.Component);

exports.default = FormUserLanguage;