'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _SocialActionCreators = require('../../../../actions/SocialActionCreators');

var _SocialActionCreators2 = _interopRequireDefault(_SocialActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormUserConnection = function (_Component) {
  _inherits(FormUserConnection, _Component);

  function FormUserConnection() {
    _classCallCheck(this, FormUserConnection);

    return _possibleConstructorReturn(this, (FormUserConnection.__proto__ || Object.getPrototypeOf(FormUserConnection)).apply(this, arguments));
  }

  _createClass(FormUserConnection, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (typeof this.props.backend === 'undefined') _SocialActionCreators2.default.getConnections(this.props.profile.key);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount(evt) {
      $("#myModal__connections").modal('hide');
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__connections', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog', role: 'document' },
          _react2.default.createElement(
            'div',
            { className: 'modal-content' },
            _react2.default.createElement(
              'div',
              { className: 'modal-header' },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                _react2.default.createElement(
                  'span5',
                  { 'aria-hidden': 'true' },
                  '\xD7'
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-body' },
              _react2.default.createElement(
                'div',
                { className: 'modal__navlist' },
                this.props.profile.connections.map(function (connection, index) {
                  return _react2.default.createElement(
                    _reactRouter.Link,
                    { key: index, to: '/th/user/' + connection.key },
                    _react2.default.createElement(
                      'li',
                      { className: 'modal__navitem' },
                      _react2.default.createElement('div', { className: 'photo__noti', style: { background: "url(" + (connection.profile_image || "/images/avatar-image.jpg") + ") no-repeat center center / cover", marginLeft: "0px", marginTop: "-10px" } }),
                      _react2.default.createElement(
                        'div',
                        null,
                        _react2.default.createElement(
                          'b',
                          null,
                          connection.name
                        )
                      )
                    )
                  );
                })
              )
            )
          )
        )
      );
    }
  }]);

  return FormUserConnection;
}(_react.Component);

exports.default = FormUserConnection;