'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FeedActionCreators = require('../../../../actions/FeedActionCreators');

var _FeedActionCreators2 = _interopRequireDefault(_FeedActionCreators);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormFeedPhoto = function (_Component) {
  _inherits(FormFeedPhoto, _Component);

  function FormFeedPhoto() {
    _classCallCheck(this, FormFeedPhoto);

    return _possibleConstructorReturn(this, (FormFeedPhoto.__proto__ || Object.getPrototypeOf(FormFeedPhoto)).apply(this, arguments));
  }

  _createClass(FormFeedPhoto, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.button_header_click) _JQueryTool2.default.animateScrollTo(this.refs.photos_feed);

      _FeedActionCreators2.default.getPhotosFeed(this.props.profile.key, this.props.user.token);
      _JQueryTool2.default.popupGallery();
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { ref: 'photos_feed', className: 'timeline-photo' },
        _react2.default.createElement(
          'div',
          { className: 'timeline-photo__tab' },
          _react2.default.createElement(
            'div',
            { className: 'row', style: { margin: "0px 1px" } },
            _react2.default.createElement(
              'div',
              { className: 'col-md-3' },
              _react2.default.createElement(
                'h3',
                null,
                _react2.default.createElement(
                  'b',
                  null,
                  _react2.default.createElement(
                    'span',
                    null,
                    _react2.default.createElement('i', { className: 'fa fa-picture-o' }),
                    ' ',
                    this.props.language === 'th' ? 'รูปภาพ' : 'Photos'
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'timeline-photo__pic' },
          _react2.default.createElement(
            'div',
            { className: 'row', style: { margin: "0px 10px" } },
            _react2.default.createElement(
              'div',
              { ref: 'photo_timeline', className: 'popup-gallery' },
              this.props.feed.photos.slice(0, 9).map(function (photo, index) {
                return _react2.default.createElement(
                  'a',
                  { key: index, href: photo.path.substring(6) },
                  _react2.default.createElement(
                    'div',
                    { className: 'col-md-3' },
                    _react2.default.createElement(
                      'div',
                      { className: 'photo__pic', style: { background: "url(" + (photo.path ? photo.path.substring(6) : "") + ") no-repeat center center", backgroundSize: "cover" } },
                      _react2.default.createElement(
                        'div',
                        { className: 'photo--detail' },
                        _react2.default.createElement(
                          'button',
                          { className: 'btn btn-default reverse detail--photo' },
                          _react2.default.createElement('i', { className: 'fa fa-pencil' })
                        )
                      )
                    )
                  )
                );
              })
            )
          )
        )
      );
    }
  }]);

  return FormFeedPhoto;
}(_react.Component);

exports.default = FormFeedPhoto;