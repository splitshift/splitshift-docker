'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FeedActionCreators = require('../../../../actions/FeedActionCreators');

var _FeedActionCreators2 = _interopRequireDefault(_FeedActionCreators);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormFeedPostComment = function (_Component) {
  _inherits(FormFeedPostComment, _Component);

  function FormFeedPostComment() {
    _classCallCheck(this, FormFeedPostComment);

    return _possibleConstructorReturn(this, (FormFeedPostComment.__proto__ || Object.getPrototypeOf(FormFeedPostComment)).apply(this, arguments));
  }

  _createClass(FormFeedPostComment, [{
    key: 'postComment',
    value: function postComment(evt) {
      evt.preventDefault();
      var comment = {
        "user_key": this.props.feed.owner.key,
        "feed_id": this.props.feed._id,
        "comment": this.refs.comment.value
      };
      this.refs.comment.value = "";
      _FeedActionCreators2.default.setFeedDraft('feed_index', this.props.feed_index);
      _FeedActionCreators2.default.postCommentFeed(this.props.user.token, comment);
    }
  }, {
    key: 'render',
    value: function render() {
      var feedContent = _configLang2.default[this.props.language].feed;
      return _react2.default.createElement(
        'form',
        { onSubmit: this.postComment.bind(this) },
        _react2.default.createElement(
          'div',
          { className: 'feed__comment' },
          _react2.default.createElement(
            'div',
            { className: 'comment__pic' },
            _react2.default.createElement('div', { style: { background: "url(" + (this.props.user.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px", marginLeft: "0px" } })
          ),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement('input', { ref: 'comment', type: 'text', placeholder: feedContent.card.comment })
          ),
          _react2.default.createElement(
            'div',
            { className: 'feed__send', onClick: this.postComment.bind(this) },
            _react2.default.createElement('i', { className: 'fa fa-paper-plane' })
          )
        )
      );
    }
  }]);

  return FormFeedPostComment;
}(_react.Component);

exports.default = FormFeedPostComment;