'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FeedActionCreators = require('../../../../actions/FeedActionCreators');

var _FeedActionCreators2 = _interopRequireDefault(_FeedActionCreators);

var _ConfigData = require('../../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserPrivacy = { public: 'Public', timeline: 'My Timeline', connected: 'Connection', employer: 'Employer', onlyme: 'Only Me' };
var CompanyPrivacy = { public: 'Public', timeline: 'My Timeline', employee: 'Employee' };

var FormFeedPost = function (_Component) {
  _inherits(FormFeedPost, _Component);

  function FormFeedPost() {
    _classCallCheck(this, FormFeedPost);

    return _possibleConstructorReturn(this, (FormFeedPost.__proto__ || Object.getPrototypeOf(FormFeedPost)).apply(this, arguments));
  }

  _createClass(FormFeedPost, [{
    key: 'updateDraft',
    value: function updateDraft(name, evt) {
      var result = evt.target.value;

      if (name == 'video') {
        var youtube_embed = _ConfigData2.default.ConvertYoutubeToEmbed(result);
        if (youtube_embed != 'error') result = youtube_embed;
      }

      _FeedActionCreators2.default.setFeedDraft(name, result);
    }
  }, {
    key: 'uploadImage',
    value: function uploadImage(evt) {
      _FeedActionCreators2.default.uploadImage(this.props.user.token, evt.target.files[0]);
    }
  }, {
    key: 'deleleImage',
    value: function deleleImage(key, evt) {
      _FeedActionCreators2.default.deleteImage(this.props.user.token, key);
    }
  }, {
    key: 'triggerUpload',
    value: function triggerUpload(evt) {
      $(this.refs.upload_feed_image).click();
    }
  }, {
    key: 'cancelEditFeed',
    value: function cancelEditFeed(evt) {
      _FeedActionCreators2.default.cancelEditFeed();
    }
  }, {
    key: 'editVideo',
    value: function editVideo() {
      _FeedActionCreators2.default.editVideo();
    }
  }, {
    key: 'editImage',
    value: function editImage() {
      _FeedActionCreators2.default.editImage();
    }
  }, {
    key: 'editFeed',
    value: function editFeed(evt) {
      evt.preventDefault();

      var result = this.props.feed.draft;

      if (this.props.feed.add_photo) result.video = "";else if (this.props.feed.add_video) {
        result.deleted_photos = result.photos;
        result.photos = [];
        delete result.new_photos;
      }

      _FeedActionCreators2.default.editFeed(this.props.user.token, result);
    }
  }, {
    key: 'createFeed',
    value: function createFeed(evt) {
      evt.preventDefault();
      var result = this.props.feed.draft;

      if (typeof result.privacy === 'undefined') result.privacy = "public";

      if (this.props.feed.add_video) delete result.photos;else if (this.props.feed.add_photo) delete result.video;

      this.refs.status.value = "";
      _FeedActionCreators2.default.setFeedDraft('owner', { key: this.props.user.key, name: this.props.user.type === 'Employee' ? this.props.user.first_name + ' ' + this.props.user.last_name : this.props.user.company_name, profile_image: this.props.user.profile_image });

      _FeedActionCreators2.default.postFeed(this.props.user.token, result);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var FeedPrivacy = this.props.user.type === 'Employee' ? UserPrivacy : CompanyPrivacy;
      var feedContent = _configLang2.default[this.props.language || 'th'].feed;

      return _react2.default.createElement(
        'article',
        { className: 'feed' },
        _react2.default.createElement(
          'form',
          { onSubmit: this.props.feed.edit_feed_id ? this.editFeed.bind(this) : this.createFeed.bind(this) },
          this.props.feed.edit_feed_id ? _react2.default.createElement('div', { className: 'edit-close-feed', onClick: this.cancelEditFeed.bind(this) }) : "",
          _react2.default.createElement(
            'div',
            { className: 'feed__header' },
            _react2.default.createElement(
              'a',
              { className: 'feed__avatar', href: '#' },
              _react2.default.createElement('div', { style: { background: "url(" + (this.props.user.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px" } })
            ),
            _react2.default.createElement(
              'div',
              { className: 'feed__title' },
              _react2.default.createElement(
                'div',
                null,
                this.props.user.rank === 'Employee' ? this.props.user.first_name + " " + this.props.user.last_name : this.props.user.company_name
              ),
              _react2.default.createElement(
                'div',
                null,
                'Thailand'
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'feed__content' },
            _react2.default.createElement('textarea', { ref: 'status', onChange: this.updateDraft.bind(this, 'text'), className: 'feed__input', placeholder: this.props.user.type === 'Employee' ? feedContent.post.employee : feedContent.post.employer, defaultValue: this.props.feed.draft.text }),
            this.props.feed.add_video ? _react2.default.createElement('input', { className: 'form-control', type: 'text', onChange: this.updateDraft.bind(this, "video"), placeholder: 'place your video url', defaultValue: this.props.feed.draft.video }) : "",
            this.props.feed.add_photo ? _react2.default.createElement(
              'div',
              { className: 'photo-gallery' },
              this.props.feed.draft.photos.map(function (photo, index) {
                return _react2.default.createElement(
                  'div',
                  { key: index, className: 'photo-gallery__item' },
                  _react2.default.createElement('div', { className: 'photo-gallery__overlay' }),
                  _react2.default.createElement('i', { className: 'photo-gallery__remove fa fa-remove', onClick: _this2.deleleImage.bind(_this2, index) }),
                  _react2.default.createElement('div', { className: 'photo-gallery__image', style: { backgroundImage: "url(" + (photo ? photo.substring(6) : "") + ")" } })
                );
              }),
              _react2.default.createElement(
                'div',
                { className: 'photo-gallery__item', onClick: this.triggerUpload.bind(this) },
                _react2.default.createElement('div', { className: 'photo-gallery__image', style: { backgroundImage: "url(/images/add-icon.png)" } })
              )
            ) : ""
          ),
          _react2.default.createElement(
            'div',
            { className: 'feed__actions' },
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'feed__button', onClick: this.editImage.bind(this) },
              _react2.default.createElement('i', { className: 'fa fa-picture-o' }),
              feedContent.card.type[0]
            ),
            _react2.default.createElement('input', { type: 'file', ref: 'upload_feed_image', onChange: this.uploadImage.bind(this), style: { display: "none" } }),
            _react2.default.createElement(
              'button',
              { type: 'button', className: 'feed__button', onClick: this.editVideo.bind(this) },
              _react2.default.createElement('i', { className: 'fa fa-video-camera' }),
              feedContent.card.type[1]
            ),
            _react2.default.createElement(
              'div',
              { className: 'feed__post' },
              _react2.default.createElement(
                'button',
                { className: 'feed__button primary' },
                _react2.default.createElement('i', { className: 'fa fa-bullhorn' }),
                this.props.feed.edit_feed_id ? feedContent.post.action[1] : feedContent.post.action[0]
              ),
              _react2.default.createElement(
                'div',
                { className: 'feed__privacy' },
                _react2.default.createElement(
                  'div',
                  { className: 'privacy-select' },
                  _react2.default.createElement(
                    'select',
                    { className: 'form-control', onChange: this.updateDraft.bind(this, 'privacy'), defaultValue: this.props.feed.draft.privacy },
                    Object.keys(FeedPrivacy).map(function (key, index) {
                      return _react2.default.createElement(
                        'option',
                        { key: index, value: key },
                        feedContent.status[key]
                      );
                    })
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return FormFeedPost;
}(_react.Component);

exports.default = FormFeedPost;