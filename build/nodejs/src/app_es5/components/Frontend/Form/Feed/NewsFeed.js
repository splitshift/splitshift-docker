'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FeedActionCreators = require('../../../../actions/FeedActionCreators');

var _FeedActionCreators2 = _interopRequireDefault(_FeedActionCreators);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _Post = require('./Post');

var _Post2 = _interopRequireDefault(_Post);

var _Card = require('./Card');

var _Card2 = _interopRequireDefault(_Card);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var setOwner = false;

var FormFeedNewsFeed = function (_Component) {
  _inherits(FormFeedNewsFeed, _Component);

  function FormFeedNewsFeed() {
    _classCallCheck(this, FormFeedNewsFeed);

    return _possibleConstructorReturn(this, (FormFeedNewsFeed.__proto__ || Object.getPrototypeOf(FormFeedNewsFeed)).apply(this, arguments));
  }

  _createClass(FormFeedNewsFeed, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.button_header_click) _JQueryTool2.default.animateScrollTo(this.refs.news_feed);

      _FeedActionCreators2.default.getFeed(this.props.profile.key, this.props.user.token);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (!setOwner && this.props.user.complete) {
        _FeedActionCreators2.default.setOwner(this.props.user);
        setOwner = true;
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _FeedActionCreators2.default.resetFeed();
    }
  }, {
    key: 'loadMore',
    value: function loadMore(evt) {
      _FeedActionCreators2.default.getFeed(this.props.profile.key, this.props.user.token, this.props.feed.feeds.length / 10);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (!nextProps.feed.deleting && this.props.feed.deleting) _FeedActionCreators2.default.getFeed(this.props.profile.key, this.props.user.token);

      if (this.props.profile.key != nextProps.profile.key || nextProps.user.token && typeof this.props.user.token === 'undefined') _FeedActionCreators2.default.getFeed(nextProps.profile.key, nextProps.user.token);

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var feedState = {
        profile: this.props.profile,
        user: this.props.user,
        feed: this.props.feed,
        language: this.props.language
      };

      return _react2.default.createElement(
        'div',
        { ref: 'news_feed', className: 'newsfeed', style: { marginTop: "16px", marginBottom: "16px" } },
        !this.props.feed.edit_feed_id && this.props.profile.key === this.props.user.key && typeof this.props.feed_id === 'undefined' ? _react2.default.createElement(_Post2.default, feedState) : "",
        this.props.feed.feeds.map(function (f, index) {
          return _this2.props.feed.edit_feed_id != null && f._id == _this2.props.feed.edit_feed_id ? _react2.default.createElement(_Post2.default, _extends({ key: index }, feedState)) : typeof _this2.props.feed_id === 'undefined' || _this2.props.feed_id && _this2.props.feed_id == f._id ? _react2.default.createElement(_Card2.default, { language: _this2.props.language, key: index, user: _this2.props.user, feed: f, index: index, comment_id: _this2.props.feed.comment_for_reply_id }) : "";
        }),
        this.props.feed.load_more ? _react2.default.createElement(
          'div',
          { className: 'btn__seemore' },
          _react2.default.createElement(
            'button',
            { onClick: this.loadMore.bind(this), className: 'btn btn-default' },
            this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
          )
        ) : ""
      );
    }
  }]);

  return FormFeedNewsFeed;
}(_react.Component);

exports.default = FormFeedNewsFeed;