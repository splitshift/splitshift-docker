'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _FeedActionCreators = require('../../../../actions/FeedActionCreators');

var _FeedActionCreators2 = _interopRequireDefault(_FeedActionCreators);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormFeedComment = function (_Component) {
  _inherits(FormFeedComment, _Component);

  function FormFeedComment() {
    _classCallCheck(this, FormFeedComment);

    return _possibleConstructorReturn(this, (FormFeedComment.__proto__ || Object.getPrototypeOf(FormFeedComment)).apply(this, arguments));
  }

  _createClass(FormFeedComment, [{
    key: 'onReply',
    value: function onReply(evt) {
      _FeedActionCreators2.default.onReply(this.props.comment);
    }
  }, {
    key: 'onPress',
    value: function onPress(evt) {
      if (evt.charCode == 13 || evt.keyCode == 13) this.postReplyComment();
    }
  }, {
    key: 'postReplyComment',
    value: function postReplyComment(evt) {
      var reply = {
        "user_key": this.props.feed.owner.key,
        "feed_id": this.props.feed._id,
        "comment_id": this.props.comment_id,
        "reply": this.refs.reply.value
      };

      this.refs.reply.value = "";
      _FeedActionCreators2.default.setFeedDraft('feed_index', this.props.feed_index);
      _FeedActionCreators2.default.setFeedDraft('comment_index', this.props.comment_index);
      _FeedActionCreators2.default.postReplyComment(this.props.user.token, reply);
    }
  }, {
    key: 'deleteComment',
    value: function deleteComment(evt) {

      var feed = {
        user_key: this.props.feed.owner.key,
        feed_id: this.props.feed._id
      };

      _FeedActionCreators2.default.deleteCommentFeed(this.props.user.token, this.props.comment, feed);
    }
  }, {
    key: 'deleteReply',
    value: function deleteReply(reply, evt) {

      var feed_info = {
        user_key: this.props.feed.owner.key,
        feed_id: this.props.feed._id,
        comment_id: this.props.comment._id
      };

      _FeedActionCreators2.default.deleteReply(this.props.user.token, reply, feed_info);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'feed__comment' },
        _react2.default.createElement(
          'div',
          { className: 'comment__pic' },
          _react2.default.createElement(
            _reactRouter.Link,
            { to: this.props.comment.owner.user_type == 'Employee' ? "/th/user/" + this.props.comment.owner.key : "/th/company/" + this.props.comment.owner.key },
            _react2.default.createElement('div', { style: { background: "url(" + (this.props.comment.owner.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px" } })
          )
        ),
        _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            'div',
            null,
            this.props.comment.owner.name,
            ' ',
            this.props.user.key == this.props.comment.owner.key ? _react2.default.createElement(
              'a',
              { onClick: this.deleteComment.bind(this), href: 'javascript:void(0)' },
              'Delete'
            ) : ""
          ),
          _react2.default.createElement(
            'div',
            null,
            this.props.comment.comment,
            ' ',
            _react2.default.createElement('br', null),
            _react2.default.createElement(
              'span',
              { style: { color: "#999" } },
              _JQueryTool2.default.changeDateAgo(this.props.comment.date),
              ' \u2022 ',
              this.props.comment.replies.length,
              ' ',
              this.props.comment.replies.length < 2 ? 'Reply' : 'Replies'
            )
          )
        ),
        typeof this.props.user.token !== 'undefined' && this.props.comment_id != this.props.comment._id ? _react2.default.createElement(
          'a',
          { onClick: this.onReply.bind(this), className: 'feed__reply' },
          'Reply'
        ) : "",
        this.props.comment_id == this.props.comment._id ? _react2.default.createElement(
          'div',
          { className: 'feed__comment' },
          _react2.default.createElement(
            'div',
            { className: 'comment__pic' },
            _react2.default.createElement('div', { style: { background: "url(" + (this.props.user.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px", marginLeft: "0px" } })
          ),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement('input', { ref: 'reply', onKeyPress: this.onPress.bind(this), type: 'text', placeholder: 'Write a reply' })
          ),
          _react2.default.createElement(
            'div',
            { className: 'feed__send', onClick: this.postReplyComment.bind(this) },
            _react2.default.createElement('i', { className: 'fa fa-paper-plane' })
          )
        ) : "",
        this.props.comment.replies.map(function (reply, index) {
          return _react2.default.createElement(
            'div',
            { key: index, className: 'feed__comment' },
            _react2.default.createElement(
              'div',
              { className: 'comment__pic' },
              _react2.default.createElement(
                _reactRouter.Link,
                { to: reply.owner.user_type == 'Employee' ? "/th/user/" + reply.owner.key : "/th/company/" + reply.owner.key },
                _react2.default.createElement('div', { style: { background: "url(" + (reply.owner.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px" } })
              )
            ),
            _react2.default.createElement(
              'div',
              null,
              _this2.props.user.key == reply.owner.key ? _react2.default.createElement(
                'div',
                null,
                reply.owner.name,
                ' ',
                _react2.default.createElement(
                  'a',
                  { href: 'javascript:void(0)', onClick: _this2.deleteReply.bind(_this2, reply) },
                  'Delete'
                )
              ) : "",
              _react2.default.createElement(
                'div',
                null,
                reply.reply,
                ' ',
                _react2.default.createElement('br', null),
                _react2.default.createElement(
                  'span',
                  { style: { color: "#999" } },
                  _JQueryTool2.default.changeDateAgo(_this2.props.comment.date)
                )
              )
            )
          );
        })
      );
    }
  }]);

  return FormFeedComment;
}(_react.Component);

exports.default = FormFeedComment;