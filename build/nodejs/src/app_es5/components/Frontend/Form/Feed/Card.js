'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _FeedActionCreators = require('../../../../actions/FeedActionCreators');

var _FeedActionCreators2 = _interopRequireDefault(_FeedActionCreators);

var _Comment = require('./Comment');

var _Comment2 = _interopRequireDefault(_Comment);

var _PostComment = require('./PostComment');

var _PostComment2 = _interopRequireDefault(_PostComment);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

var _ConfigData = require('../../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var API_URL = function API_URL() {
  return window.location.origin;
};

var FormFeedCard = function (_Component) {
  _inherits(FormFeedCard, _Component);

  function FormFeedCard() {
    _classCallCheck(this, FormFeedCard);

    return _possibleConstructorReturn(this, (FormFeedCard.__proto__ || Object.getPrototypeOf(FormFeedCard)).apply(this, arguments));
  }

  _createClass(FormFeedCard, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _JQueryTool2.default.popupGallery();
    }
  }, {
    key: 'likeFeed',
    value: function likeFeed(evt) {
      _FeedActionCreators2.default.setFeedDraft('like', this.props.index);
      _FeedActionCreators2.default.setFeedDraft('owner_key', this.props.user.key);
      _FeedActionCreators2.default.putLikeFeed(this.props.user.token, this.props.feed);
    }
  }, {
    key: 'chooseEditFeed',
    value: function chooseEditFeed(evt) {
      $(".edit-menu__dropdown").removeClass('opened');
      _FeedActionCreators2.default.chooseEditFeed(this.props.feed, this.props.index);
    }
  }, {
    key: 'deleteFeed',
    value: function deleteFeed(evt) {
      $(".edit-menu__dropdown").removeClass('opened');

      if (this.props.user.type === 'Admin') _FeedActionCreators2.default.deleteBuzzFeed(this.props.user.token, this.props.feed);else _FeedActionCreators2.default.deleteFeed(this.props.user.token, this.props.feed);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var isBuzz = typeof this.props.buzz !== 'undefined' ? true : false;
      var isPublic = this.props.feed.privacy === 'public';
      var feedContent = _configLang2.default[this.props.language].feed;

      return _react2.default.createElement(
        'article',
        { className: isBuzz ? 'feed feed__card' : 'feed' },
        !isBuzz && isPublic ? _react2.default.createElement(
          'div',
          { className: 'feed__buzz' },
          _react2.default.createElement(
            'span1',
            null,
            'Buzz'
          ),
          _react2.default.createElement('img', { src: '/images/company/bookmark-ribbon.png' })
        ) : "",
        typeof this.props.user.key !== 'undefined' && (this.props.feed.owner.key == this.props.user.key || this.props.user.type === 'Admin' && typeof this.props.buzz !== 'undefined') ? _react2.default.createElement(
          'div',
          { className: 'edit-menu', onClick: _JQueryTool2.default.clickEditMenu.bind(this) },
          _react2.default.createElement(
            'div',
            { className: 'edit-menu__dropdown' },
            this.props.feed.owner.key == this.props.user.key ? _react2.default.createElement(
              'div',
              { className: 'edit-menu__item' },
              _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: this.chooseEditFeed.bind(this) },
                _react2.default.createElement('i', { className: 'fa fa-pencil' }),
                ' Edit post'
              )
            ) : "",
            _react2.default.createElement(
              'div',
              { className: 'edit-menu__item' },
              _react2.default.createElement(
                'a',
                { href: 'javascript:void(0)', onClick: this.deleteFeed.bind(this) },
                _react2.default.createElement('i', { className: 'fa fa-trash' }),
                ' Remove post'
              )
            )
          )
        ) : "",
        _react2.default.createElement(
          'div',
          { className: 'feed__header' },
          _react2.default.createElement(
            _reactRouter.Link,
            { className: 'feed__avatar', to: this.props.feed.owner.user_type === 'Employee' ? "/th/user/" + this.props.feed.owner.key : this.props.feed.owner.user_type !== 'Admin' ? "/th/company/" + this.props.feed.owner.key : "/" },
            _react2.default.createElement('div', { style: { background: "url(" + (this.props.feed.owner.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px" } })
          ),
          _react2.default.createElement(
            'div',
            { className: 'feed__title' },
            _react2.default.createElement(
              'div',
              null,
              this.props.feed.owner.name
            ),
            _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _reactRouter.Link,
                { to: this.props.feed.owner.user_type == 'Employee' ? "/th/user/" + this.props.feed.owner.key + "/feed/" + this.props.feed._id : "/th/company/" + this.props.feed.owner.key + "/feed/" + this.props.feed._id },
                _JQueryTool2.default.changeDateAgo(this.props.feed.date)
              ),
              ' ',
              '\u2022',
              ' ',
              feedContent.status[this.props.feed.privacy]
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'feed__content' },
          _react2.default.createElement(
            'div',
            { className: 'feed__paragraph' },
            this.props.feed.text
          ),
          _react2.default.createElement(
            'div',
            { className: 'popup-gallery' },
            this.props.feed.photos && this.props.feed.photos.map(function (photo, index) {
              var notLastPhoto = index === 3 && _this2.props.feed.photos.length > 4;
              return _react2.default.createElement(
                'a',
                { className: 'photo__card', key: index, href: photo ? photo.substring(6) : "", title: _this2.props.feed.text, style: { display: index > 3 && 'none' } },
                notLastPhoto && _react2.default.createElement(
                  'span',
                  null,
                  '+',
                  _this2.props.feed.photos.length - 4
                ),
                _react2.default.createElement('img', { className: index > 0 ? 'feed__album' : notLastPhoto ? 'feed__photo feed__photo--last' : 'feed__photo', src: index == 0 && photo ? photo.substring(6) : "", style: { background: (notLastPhoto ? "linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5))," : "") + "url('" + (photo ? photo.substring(6) : "") + "')  top center no-repeat", backgroundSize: "cover" } })
              );
            })
          ),
          this.props.feed.video ? _react2.default.createElement('iframe', { className: 'feed__clip', height: '315', src: this.props.feed.video, frameBorder: '0', allowFullScreen: true }) : ""
        ),
        _react2.default.createElement(
          'div',
          { className: 'feed__actions' },
          typeof this.props.user.token !== 'undefined' && _react2.default.createElement(
            'span4',
            null,
            _react2.default.createElement(
              'button',
              { className: 'feed__button', onClick: this.likeFeed.bind(this), style: { color: this.props.feed.likes.indexOf(this.props.user.key) != -1 ? "#f60" : "" } },
              _react2.default.createElement('i', { className: 'fa fa-thumbs-up', style: { color: this.props.feed.likes.indexOf(this.props.user.key) != -1 ? "#f60" : "" } }),
              feedContent.card.action[0]
            ),
            _react2.default.createElement(
              'button',
              { className: 'feed__button',
                'data-href': "https://www.facebook.com/sharer/sharer.php?app_id=235025906884083&sdk=joey&u=" + API_URL() + (this.props.feed.owner.user_type === 'Employee' ? "/th/user/" + this.props.feed.owner.key + "/feed/" + this.props.feed._id : "/th/company/" + this.props.feed.owner.key + "/feed/" + this.props.feed._id) + "&display=popup&ref=plugin&src=share_button",
                onClick: function onClick(evt) {
                  return !window.open($(evt.currentTarget).data('href'), 'Facebook', 'width=640,height=580');
                } },
              _react2.default.createElement('i', { className: 'fa fa-share' }),
              feedContent.card.action[1]
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'feed__counter' },
            this.props.feed.comments.length,
            ' ',
            feedContent.card.action[2],
            this.props.feed.comments.length > 1 && this.props.language === 'en' ? 's' : '',
            ' \u2022 ',
            this.props.feed.likes.length,
            ' ',
            feedContent.card.action[3],
            this.props.feed.likes.length > 1 && this.props.language === 'en' ? 's' : ''
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'feed__tray' },
          typeof this.props.user.token !== 'undefined' && _react2.default.createElement(_PostComment2.default, { language: this.props.language, user: this.props.user, feed: this.props.feed, feed_index: this.props.index }),
          this.props.feed.comments.map(function (comment, index) {
            return _react2.default.createElement(_Comment2.default, { key: index, user: _this2.props.user, feed: _this2.props.feed, feed_index: _this2.props.index, comment_id: _this2.props.comment_id, comment_index: index, comment: comment });
          })
        )
      );
    }
  }]);

  return FormFeedCard;
}(_react.Component);

exports.default = FormFeedCard;