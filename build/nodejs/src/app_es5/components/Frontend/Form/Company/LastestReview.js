'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ConfigData = require('../../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LastestReview = function (_Component) {
  _inherits(LastestReview, _Component);

  function LastestReview() {
    _classCallCheck(this, LastestReview);

    return _possibleConstructorReturn(this, (LastestReview.__proto__ || Object.getPrototypeOf(LastestReview)).apply(this, arguments));
  }

  _createClass(LastestReview, [{
    key: 'thumbRating',
    value: function thumbRating(num) {
      return [1, 2, 3, 4, 5].map(function (n, index) {
        return n <= num ? _react2.default.createElement(
          'span',
          { key: index },
          _react2.default.createElement('i', { className: 'fa fa-thumbs-up', 'aria-hidden': 'true' }),
          ' '
        ) : _react2.default.createElement(
          'span5',
          { key: index },
          _react2.default.createElement('i', { className: 'fa fa-thumbs-up', 'aria-hidden': 'true' }),
          ' '
        );
      });
    }
  }, {
    key: 'circleRating',
    value: function circleRating(num) {
      return [1, 2, 3, 4, 5].map(function (n, index) {
        return n <= num ? _react2.default.createElement(
          'span',
          { key: index },
          _react2.default.createElement('i', { className: 'fa fa-circle', 'aria-hidden': 'true' }),
          ' '
        ) : _react2.default.createElement(
          'span5',
          { key: index },
          _react2.default.createElement('i', { className: 'fa fa-circle', 'aria-hidden': 'true' }),
          ' '
        );
      });
    }
  }, {
    key: 'toggleReview',
    value: function toggleReview(evt) {
      $(evt.target).children('i').toggleClass("fa-chevron-down");
      $(evt.target).children('i').toggleClass("fa-chevron-up");
      $($(evt.target).data('target')).toggle();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var reviewContent = _configLang2.default[this.props.lang].company_profile.review;

      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          reviewContent.lastest,
          ' '
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          this.props.profile.review.map(function (review, index) {
            return _react2.default.createElement(
              'div',
              { key: index },
              _react2.default.createElement(
                'div',
                { className: 'latest__reviewer' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'b',
                    null,
                    _JQueryTool2.default.changeBirthDayFormat(review.date),
                    ' | ',
                    review.from.type === 'employee' ? _ConfigData2.default.ConvertEmployeeLang('Current Employee', _this2.props.lang) : _ConfigData2.default.ConvertEmployeeLang('Former Employee', _this2.props.lang)
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'latest__rating' },
                  _react2.default.createElement(
                    'p',
                    { className: 'title__review overall' },
                    reviewContent.side[0],
                    _react2.default.createElement(
                      'button',
                      { onMouseDown: _this2.toggleReview.bind(_this2), onTouchEnd: _this2.toggleReview.bind(_this2), className: 'btn btn-default reverse review--btnexpand', 'data-target': ".review--expand" + index },
                      _react2.default.createElement('i', { className: 'fa fa-chevron-down' })
                    )
                  ),
                  _react2.default.createElement(
                    'p',
                    { className: 'point__review thumb' },
                    _react2.default.createElement(
                      'span5',
                      null,
                      _this2.thumbRating(review.overall),
                      review.overall.toFixed(1)
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: "review--expand review--expand" + index },
                  _react2.default.createElement(
                    'div',
                    { className: 'latest__rating' },
                    _react2.default.createElement(
                      'p',
                      { className: 'title__review' },
                      reviewContent.side[1]
                    ),
                    _react2.default.createElement(
                      'p',
                      { className: 'point__review' },
                      _react2.default.createElement(
                        'span5',
                        null,
                        _this2.circleRating(review.work),
                        review.work.toFixed(1)
                      )
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'latest__rating' },
                    _react2.default.createElement(
                      'p',
                      { className: 'title__review' },
                      reviewContent.side[2]
                    ),
                    _react2.default.createElement(
                      'p',
                      { className: 'point__review' },
                      _react2.default.createElement(
                        'span5',
                        null,
                        _this2.circleRating(review.benefit),
                        review.benefit.toFixed(1)
                      )
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'latest__rating' },
                    _react2.default.createElement(
                      'p',
                      { className: 'title__review' },
                      reviewContent.side[3]
                    ),
                    _react2.default.createElement(
                      'p',
                      { className: 'point__review' },
                      _react2.default.createElement(
                        'span5',
                        null,
                        _this2.circleRating(review.career),
                        review.career.toFixed(1)
                      )
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'latest__rating' },
                    _react2.default.createElement(
                      'p',
                      { className: 'title__review' },
                      reviewContent.side[4]
                    ),
                    _react2.default.createElement(
                      'p',
                      { className: 'point__review' },
                      _react2.default.createElement(
                        'span5',
                        null,
                        _this2.circleRating(review.recommend),
                        review.recommend.toFixed(1)
                      )
                    )
                  )
                )
              ),
              _react2.default.createElement('hr', { className: 'hr-latest__reviewer' })
            );
          })
        )
      );
    }
  }]);

  return LastestReview;
}(_react.Component);

exports.default = LastestReview;