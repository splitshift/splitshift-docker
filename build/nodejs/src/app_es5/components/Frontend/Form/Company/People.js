'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanyPeople = function (_Component) {
  _inherits(CompanyPeople, _Component);

  function CompanyPeople() {
    _classCallCheck(this, CompanyPeople);

    return _possibleConstructorReturn(this, (CompanyPeople.__proto__ || Object.getPrototypeOf(CompanyPeople)).apply(this, arguments));
  }

  _createClass(CompanyPeople, [{
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].company_profile.header;

      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header.people
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'ul',
            null,
            _react2.default.createElement(
              'li',
              null,
              this.props.profile.employees.length > 0 ? _react2.default.createElement(
                'div',
                { className: this.props.responsive ? "people--responsive" : "row" },
                _react2.default.createElement(
                  'b',
                  null,
                  'Present'
                ),
                this.props.profile.employees.length > 3 && _react2.default.createElement(
                  'a',
                  { href: '#', style: { color: "#aaa", textDecoration: "underline", float: "right" } },
                  this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
                ),
                _react2.default.createElement('br', null),
                this.props.profile.employees.map(function (employee, index) {
                  return _react2.default.createElement(
                    _reactRouter.Link,
                    { key: index, to: '/th/user/' + employee.key },
                    _react2.default.createElement(
                      'div',
                      { className: 'col-md-4', style: { padding: "0px 2px" } },
                      _react2.default.createElement(
                        'div',
                        { className: 'people__pic' },
                        _react2.default.createElement('img', { src: employee.profile_image }),
                        _react2.default.createElement(
                          'div',
                          { className: 'people__name' },
                          _react2.default.createElement(
                            'span1',
                            null,
                            employee.profile.first_name,
                            ' '
                          ),
                          ' ',
                          _react2.default.createElement('br', null),
                          _react2.default.createElement(
                            'span1',
                            null,
                            employee.profile.last_name
                          )
                        )
                      )
                    )
                  );
                })
              ) : ""
            ),
            _react2.default.createElement('br', null),
            _react2.default.createElement(
              'li',
              null,
              this.props.profile.formers.length > 0 ? _react2.default.createElement(
                'div',
                { className: this.props.responsive ? "people--responsive" : "row" },
                _react2.default.createElement(
                  'b',
                  null,
                  'Past'
                ),
                this.props.profile.formers.length > 3 ? _react2.default.createElement(
                  'a',
                  { href: '#', style: { color: "#aaa", textDecoration: "underline", float: "right" } },
                  this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
                ) : "",
                _react2.default.createElement('br', null),
                this.props.profile.formers.map(function (former, index) {
                  return _react2.default.createElement(
                    _reactRouter.Link,
                    { key: index, to: '/th/user/' + former.key },
                    _react2.default.createElement(
                      'div',
                      { className: 'col-md-4', style: { padding: "0px 2px" } },
                      _react2.default.createElement(
                        'div',
                        { className: 'people__pic' },
                        _react2.default.createElement('img', { src: former.profile_image }),
                        _react2.default.createElement(
                          'div',
                          { className: 'people__name' },
                          _react2.default.createElement(
                            'span1',
                            null,
                            former.profile.first_name,
                            ' '
                          ),
                          ' ',
                          _react2.default.createElement('br', null),
                          _react2.default.createElement(
                            'span1',
                            null,
                            former.profile.last_name
                          )
                        )
                      )
                    )
                  );
                })
              ) : ""
            )
          )
        )
      );
    }
  }]);

  return CompanyPeople;
}(_react.Component);

exports.default = CompanyPeople;