'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _CompanyProfileActionCreators = require('../../../../actions/CompanyProfileActionCreators');

var _CompanyProfileActionCreators2 = _interopRequireDefault(_CompanyProfileActionCreators);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

var _ConfigData = require('../../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanyVideo = function (_Component) {
  _inherits(CompanyVideo, _Component);

  function CompanyVideo() {
    _classCallCheck(this, CompanyVideo);

    return _possibleConstructorReturn(this, (CompanyVideo.__proto__ || Object.getPrototypeOf(CompanyVideo)).apply(this, arguments));
  }

  _createClass(CompanyVideo, [{
    key: 'updateDraft',
    value: function updateDraft(name, evt) {

      var result = evt.target.value;

      if (name == 'video') {
        var youtube_embed = _ConfigData2.default.ConvertYoutubeToEmbed(result);
        if (youtube_embed != 'error') result = youtube_embed;
      }

      _ResourceActionCreators2.default.setDraft(name, result);
    }
  }, {
    key: 'updateVideo',
    value: function updateVideo(evt) {
      evt.preventDefault();

      _ResourceActionCreators2.default.setEditor('edit_company_video', true);
      _CompanyProfileActionCreators2.default.updateVideo(this.props.user.token, this.props.profile.draft);
    }
  }, {
    key: 'contentRender',
    value: function contentRender(videoURL) {
      return videoURL ? _react2.default.createElement(
        'div',
        { className: 'row' },
        _react2.default.createElement('iframe', { className: 'feed__clip', height: '250', src: videoURL, frameBorder: '0', allowFullScreen: '' })
      ) : "";
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {

      return this.props.profile && this.props.profile.key ? _react2.default.createElement(
        'form',
        { onSubmit: this.updateVideo.bind(this) },
        this.contentRender(this.props.profile.draft.video ? this.props.profile.draft.video : this.props.profile.video),
        _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', defaultValue: this.props.profile.video, onChange: this.updateDraft.bind(this, 'video') }),
        _react2.default.createElement(
          'button',
          { className: 'btn btn-mini btn-default' },
          'Save'
        )
      ) : "";
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.status.edit_company_video && this.props.status.edit_company_video) _CompanyProfileActionCreators2.default.getProfile(this.props.user.key);

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].company_profile.header;
      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header.videos
        ),
        this.props.editor ? this.editorRender() : this.contentRender(this.props.profile.video)
      );
    }
  }]);

  return CompanyVideo;
}(_react.Component);

exports.default = CompanyVideo;