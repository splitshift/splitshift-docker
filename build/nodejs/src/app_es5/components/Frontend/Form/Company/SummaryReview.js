'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanySummaryReview = function (_Component) {
  _inherits(CompanySummaryReview, _Component);

  function CompanySummaryReview() {
    _classCallCheck(this, CompanySummaryReview);

    return _possibleConstructorReturn(this, (CompanySummaryReview.__proto__ || Object.getPrototypeOf(CompanySummaryReview)).apply(this, arguments));
  }

  _createClass(CompanySummaryReview, [{
    key: 'thumbRating',
    value: function thumbRating(num) {
      return [1, 2, 3, 4, 5].map(function (n, index) {
        return n <= num ? _react2.default.createElement(
          'span',
          { key: index },
          _react2.default.createElement('i', { className: 'fa fa-thumbs-up', 'aria-hidden': 'true' }),
          ' '
        ) : _react2.default.createElement(
          'span5',
          { key: index },
          _react2.default.createElement('i', { className: 'fa fa-thumbs-up', 'aria-hidden': 'true' }),
          ' '
        );
      });
    }
  }, {
    key: 'circleRating',
    value: function circleRating(num) {
      return [1, 2, 3, 4, 5].map(function (n, index) {
        return n <= num ? _react2.default.createElement(
          'span',
          { key: index },
          _react2.default.createElement('i', { className: 'fa fa-circle', 'aria-hidden': 'true' }),
          ' '
        ) : _react2.default.createElement(
          'span5',
          { key: index },
          _react2.default.createElement('i', { className: 'fa fa-circle', 'aria-hidden': 'true' }),
          ' '
        );
      });
    }
  }, {
    key: 'sumReview',
    value: function sumReview(reviews) {

      var sum = {
        benefit: 0,
        career: 0,
        recommend: 0,
        work: 0
      };

      reviews.map(function (review) {
        sum.benefit += review.benefit;
        sum.career += review.career;
        sum.recommend += review.recommend;
        sum.work += review.work;
      });

      if (reviews.length > 0) {
        sum.benefit /= reviews.length;
        sum.career /= reviews.length;
        sum.recommend /= reviews.length;
        sum.work /= reviews.length;
      }

      return sum;
    }
  }, {
    key: 'render',
    value: function render() {
      var sumReview = this.props.profile.review ? this.sumReview(this.props.profile.review) : { benefit: 0, career: 0, recommend: 0, work: 0 };
      var reviewContent = _configLang2.default[this.props.lang].company_profile.review;

      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner company__header', style: { marginBottom: "0px" } },
          reviewContent.summary
        ),
        _react2.default.createElement(
          'div',
          { className: 'row company__ratingbox' },
          _react2.default.createElement(
            'div',
            { className: 'company__rating' },
            _react2.default.createElement(
              'p',
              { className: 'title__review overall' },
              reviewContent.side[0]
            ),
            _react2.default.createElement(
              'p',
              { className: 'point__review thumb' },
              _react2.default.createElement(
                'span5',
                null,
                this.thumbRating(this.props.profile.overall),
                this.props.profile.overall.toFixed(1)
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'company__rating' },
            _react2.default.createElement(
              'p',
              { className: 'title__review' },
              reviewContent.side[1]
            ),
            _react2.default.createElement(
              'p',
              { className: 'point__review' },
              _react2.default.createElement(
                'span5',
                null,
                this.circleRating(sumReview.work),
                sumReview.work.toFixed(1)
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'company__rating' },
            _react2.default.createElement(
              'p',
              { className: 'title__review' },
              reviewContent.side[2]
            ),
            _react2.default.createElement(
              'p',
              { className: 'point__review' },
              _react2.default.createElement(
                'span5',
                null,
                this.circleRating(sumReview.benefit),
                sumReview.benefit.toFixed(1)
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'company__rating' },
            _react2.default.createElement(
              'p',
              { className: 'title__review' },
              reviewContent.side[3]
            ),
            _react2.default.createElement(
              'p',
              { className: 'point__review' },
              _react2.default.createElement(
                'span5',
                null,
                this.circleRating(sumReview.career),
                sumReview.career.toFixed(1)
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'company__rating' },
            _react2.default.createElement(
              'p',
              { className: 'title__review' },
              reviewContent.side[4]
            ),
            _react2.default.createElement(
              'p',
              { className: 'point__review' },
              _react2.default.createElement(
                'span5',
                null,
                this.circleRating(sumReview.recommend),
                sumReview.recommend.toFixed(1)
              )
            )
          )
        )
      );
    }
  }]);

  return CompanySummaryReview;
}(_react.Component);

exports.default = CompanySummaryReview;