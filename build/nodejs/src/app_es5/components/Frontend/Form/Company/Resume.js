'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanyResume = function (_Component) {
  _inherits(CompanyResume, _Component);

  function CompanyResume(props) {
    _classCallCheck(this, CompanyResume);

    var _this = _possibleConstructorReturn(this, (CompanyResume.__proto__ || Object.getPrototypeOf(CompanyResume)).call(this, props));

    _this.state = {
      img: 0
    };
    return _this;
  }

  _createClass(CompanyResume, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      $("img").on('load', function () {
        _this2.setState({ img: _this2.state.img + 1 });
      });
    }
  }, {
    key: 'onPrint',
    value: function onPrint(evt) {
      window.print();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.state.img === 2) {
        window.print();
        this.setState({ img: 0 });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'resume' },
        _react2.default.createElement(
          'div',
          { className: 'resume__bio' },
          _react2.default.createElement(
            'div',
            { className: 'resume__avatar' },
            _react2.default.createElement('img', { src: this.props.user.profile_image ? this.props.user.profile_image : "" }),
            _react2.default.createElement(
              'h1',
              null,
              this.props.user.company_name
            ),
            _react2.default.createElement(
              'div',
              { style: { marginTop: "20px" } },
              _react2.default.createElement(
                'div',
                { className: 'resume__block' },
                _react2.default.createElement(
                  'h1',
                  null,
                  'Contact Details'
                ),
                _react2.default.createElement(
                  'b',
                  null,
                  'Address:'
                ),
                ' ',
                this.props.user.address.district,
                ',  ',
                this.props.user.address.zip_code,
                ', ',
                this.props.user.address.city,
                ' ',
                _react2.default.createElement('br', null),
                _react2.default.createElement(
                  'b',
                  null,
                  'Telephone:'
                ),
                ' ',
                this.props.user.tel,
                ' ',
                _react2.default.createElement('br', null),
                _react2.default.createElement(
                  'b',
                  null,
                  'Email Address:'
                ),
                '  ',
                this.props.user.email
              ),
              _react2.default.createElement(
                'div',
                { className: 'resume__block' },
                _react2.default.createElement(
                  'h1',
                  null,
                  'Benefits'
                ),
                _react2.default.createElement(
                  'ul',
                  null,
                  this.props.user.benefits.map(function (benefit, index) {
                    return _react2.default.createElement(
                      'li',
                      { key: index },
                      _react2.default.createElement(
                        'b',
                        null,
                        benefit.category
                      )
                    );
                  })
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'resume__block' },
                _react2.default.createElement(
                  'h1',
                  null,
                  'Open Positions'
                ),
                _react2.default.createElement(
                  'ul',
                  null,
                  this.props.user.jobs.map(function (job, index) {
                    return _react2.default.createElement(
                      'li',
                      { key: index },
                      _react2.default.createElement(
                        'b',
                        null,
                        job.position
                      )
                    );
                  })
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'resume__profile' },
          _react2.default.createElement(
            'div',
            { className: 'resume__block' },
            _react2.default.createElement(
              'h1',
              null,
              'Overview'
            ),
            _react2.default.createElement('p', { dangerouslySetInnerHTML: { __html: this.props.user.overview.replace(/\n/g, "<br/>") } })
          ),
          _react2.default.createElement(
            'div',
            { className: 'resume__block' },
            _react2.default.createElement(
              'h1',
              null,
              'Culture'
            ),
            _react2.default.createElement('p', { dangerouslySetInnerHTML: { __html: this.props.user.culture.replace(/\n/g, "<br/>") } })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'resume__banner' },
          _react2.default.createElement(
            'button',
            { className: 'no-print', onClick: this.onPrint.bind(this) },
            'Print'
          ),
          ' ',
          _react2.default.createElement('img', { src: '/images/splitshift_720.png' })
        )
      );
    }
  }]);

  return CompanyResume;
}(_react.Component);

exports.default = CompanyResume;