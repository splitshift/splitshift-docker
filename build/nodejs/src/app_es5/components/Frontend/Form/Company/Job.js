'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _JobActionCreators = require('../../../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var userComplete = false;

var CompanyJob = function (_Component) {
  _inherits(CompanyJob, _Component);

  function CompanyJob() {
    _classCallCheck(this, CompanyJob);

    return _possibleConstructorReturn(this, (CompanyJob.__proto__ || Object.getPrototypeOf(CompanyJob)).apply(this, arguments));
  }

  _createClass(CompanyJob, [{
    key: 'updateDraft',
    value: function updateDraft(name, evt) {
      var response = {};
      response[name] = evt.target.value;

      _JobActionCreators2.default.updateApplyJobDraft(response);
    }
  }, {
    key: 'uploadFile',
    value: function uploadFile(evt) {
      _JobActionCreators2.default.uploadFileApplyJob(this.props.user.token, evt.target.files[0]);
    }
  }, {
    key: 'sendApplyJob',
    value: function sendApplyJob(evt) {
      var jobForm = this.props.job.apply_draft;
      var allFilePath = jobForm.files ? jobForm.files.map(function (file, key) {
        return file.path;
      }) : [];

      jobForm.job_id = this.props.profile.jobs[this.props.job.job_index]._id;
      jobForm.to = this.props.profile.key;
      jobForm.files = allFilePath;

      _JobActionCreators2.default.sendApplyJob(this.props.user.token, jobForm);
    }
  }, {
    key: 'openApplyJobForm',
    value: function openApplyJobForm(evt) {
      $(evt.target).hide();
      $(".application--form").show();
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      userComplete = false;
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (!userComplete && this.props.user.complete) {
        var response = {};
        response['email'] = this.props.user.email;
        _JobActionCreators2.default.updateApplyJobDraft(response);
        userComplete = true;
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      $("#myModal__positions").modal('hide');
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__positions', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog', role: 'document' },
          _react2.default.createElement(
            'div',
            { className: 'modal-content' },
            _react2.default.createElement(
              'div',
              { className: 'modal-header' },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                _react2.default.createElement(
                  'span5',
                  { 'aria-hidden': 'true' },
                  '\xD7'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal-title' },
                _react2.default.createElement(
                  'b',
                  null,
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].position : ""
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-body' },
              _react2.default.createElement(
                'div',
                { className: 'job--description' },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'b',
                    null,
                    'Type'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].type : ""
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Min.Required Experience'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].min_year : "",
                    ' years'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Compensation'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'span',
                    null,
                    this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].compensation : ""
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Job Details'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'div',
                    { className: 'text--detail' },
                    _react2.default.createElement(
                      'span',
                      null,
                      this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].description : ""
                    )
                  ),
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Required Skills'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  this.props.job.job_index != null ? _react2.default.createElement(
                    'span',
                    null,
                    this.props.profile.jobs[this.props.job.job_index].required_skills.map(function (skill, index) {
                      return _react2.default.createElement(
                        'span',
                        { key: index },
                        _react2.default.createElement('i', { className: 'fa fa-star' }),
                        ' ',
                        skill,
                        ' ',
                        _react2.default.createElement('br', null)
                      );
                    })
                  ) : ""
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-footer' },
              this.props.user.type === 'Employee' ? _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                  'button',
                  { className: 'modal__btn--light modal__btn--responsive applyjob none', onClick: this.openApplyJobForm.bind(this) },
                  'Apply Now'
                )
              ) : "",
              _react2.default.createElement(
                'div',
                { className: 'application--form' },
                _react2.default.createElement(
                  'label',
                  { htmlFor: 'letter' },
                  'Cover Letter'
                ),
                _react2.default.createElement('textarea', { onChange: this.updateDraft.bind(this, 'cover_letter'), className: 'form-control form-control--mini', rows: '5', placeholder: 'Dear..', id: 'letter' }),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-6 col-sm-12', style: { padding: "0px", paddingRight: "5px" } },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'phone' },
                    'Phone'
                  ),
                  _react2.default.createElement('input', { type: 'text', onChange: this.updateDraft.bind(this, 'tel'), className: 'form-control form-control--mini', placeholder: 'Phone Number...', id: 'phone' })
                ),
                this.props.user.complete ? _react2.default.createElement(
                  'div',
                  { className: 'col-md-6 col-sm-12', style: { padding: "0px" } },
                  _react2.default.createElement(
                    'label',
                    { htmlFor: 'email' },
                    'Email'
                  ),
                  _react2.default.createElement('input', { type: 'text', onChange: this.updateDraft.bind(this, 'email'), className: 'form-control form-control--mini', defaultValue: this.props.user.email, placeholder: 'Email Address...', id: 'email' })
                ) : "",
                _react2.default.createElement(
                  'div',
                  { className: 'attach__file' },
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      'File Attachments'
                    )
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  this.props.job.job_index != null && typeof this.props.job.apply_draft.files !== "undefined" ? _react2.default.createElement(
                    'div',
                    { style: { textAlign: "left" } },
                    this.props.job.apply_draft.files.map(function (file, index) {
                      return _react2.default.createElement(
                        'div',
                        { key: index },
                        _react2.default.createElement('i', { className: 'fa fa-star' }),
                        ' ',
                        file.original_name,
                        ' ',
                        _react2.default.createElement('br', null)
                      );
                    })
                  ) : "",
                  _react2.default.createElement('input', { onChange: this.uploadFile.bind(this), type: 'file', name: 'attachfile' })
                ),
                _react2.default.createElement(
                  'button',
                  { onClick: this.sendApplyJob.bind(this), className: 'modal__btn--light' },
                  'Submit Application'
                )
              )
            )
          )
        )
      );
    }
  }]);

  return CompanyJob;
}(_react.Component);

exports.default = CompanyJob;