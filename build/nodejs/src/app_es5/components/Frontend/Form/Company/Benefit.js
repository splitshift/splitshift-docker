'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _CompanyProfileActionCreators = require('../../../../actions/CompanyProfileActionCreators');

var _CompanyProfileActionCreators2 = _interopRequireDefault(_CompanyProfileActionCreators);

var _ConfigData = require('../../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BenefitShow = new Array(_ConfigData2.default.BenefitList().length).fill(true);

var CompanyBenefit = function (_Component) {
  _inherits(CompanyBenefit, _Component);

  function CompanyBenefit() {
    _classCallCheck(this, CompanyBenefit);

    return _possibleConstructorReturn(this, (CompanyBenefit.__proto__ || Object.getPrototypeOf(CompanyBenefit)).apply(this, arguments));
  }

  _createClass(CompanyBenefit, [{
    key: 'updateDraft',
    value: function updateDraft(name, evt) {

      _ResourceActionCreators2.default.setDraft('category', evt.target.value);
      _ResourceActionCreators2.default.setDraft('description', evt.target.value);
    }
  }, {
    key: 'createBenefit',
    value: function createBenefit(evt) {
      evt.preventDefault();

      var response = {};
      response.category = this.props.profile.draft.category ? this.props.profile.draft.category : this.refs.benefit.value;
      response.description = response.category;

      var result = {
        benefits: this.props.profile.benefits.concat(response)
      };

      _ResourceActionCreators2.default.setEditor('edit_company_benefit', true);

      _CompanyProfileActionCreators2.default.updateProfile(result);
      _CompanyProfileActionCreators2.default.createBenefit(this.props.user.token, response);
    }
  }, {
    key: 'deleteBenefit',
    value: function deleteBenefit(id, evt) {
      evt.preventDefault();
      if (!this.props.status.edit_company_benefit) _ResourceActionCreators2.default.setEditor('edit_company_benefit', true);

      var result = {
        benefits: this.props.profile.benefits.filter(function (benefit, index) {
          return index != id;
        })
      };

      _CompanyProfileActionCreators2.default.updateProfile(result);
      _CompanyProfileActionCreators2.default.deleteBenefit(this.props.user.token, this.props.profile.benefits[id]);
    }
  }, {
    key: 'benefitListRender',
    value: function benefitListRender(benefits, onEdit) {
      var rows = [];
      var companyContent = _configLang2.default[this.props.lang].company_profile;

      for (var i = 0; i < benefits.length; i += 3) {
        var cols = [];
        for (var j = 0; j < 3 && i + j < benefits.length; j++) {
          cols.push(_react2.default.createElement(
            'div',
            { key: i + j, className: 'col-md-4 col-sm-4 col-xs-6' },
            _react2.default.createElement(
              'div',
              { className: 'company_benefits' },
              _react2.default.createElement('img', { src: '/images/benefits/' + benefits[i + j].category.toLowerCase() + '.svg' }),
              _react2.default.createElement(
                'h6',
                null,
                companyContent.benefit[_ConfigData2.default.BenefitList().indexOf(benefits[i + j].description)]
              ),
              onEdit ? _react2.default.createElement(
                'a',
                { onClick: this.deleteBenefit.bind(this, i + j), href: 'javascript:void(0)' },
                'Delete'
              ) : ""
            )
          ));
        }
        rows.push(_react2.default.createElement(
          'div',
          { key: i, className: 'row' },
          cols
        ));
      }

      return rows;
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var onEdit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      return this.props.profile.benefits ? this.benefitListRender(this.props.profile.benefits, onEdit) : "";
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      BenefitShow = new Array(_ConfigData2.default.BenefitList().length).fill(true);

      if (this.props.profile && this.props.profile.benefits) {
        this.props.profile.benefits.map(function (benefit, id) {
          BenefitShow[_ConfigData2.default.BenefitList().indexOf(benefit.category)] = false;
        });
      }

      var companyContent = _configLang2.default[this.props.lang].company_profile;

      return _react2.default.createElement(
        'form',
        { onSubmit: this.createBenefit.bind(this) },
        this.contentRender(true),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'select',
              { ref: 'benefit', className: 'form-control form-control--mini ', onChange: this.updateDraft.bind(this) },
              _ConfigData2.default.BenefitList().map(function (benefit, index) {
                return BenefitShow[index] && _react2.default.createElement(
                  'option',
                  { key: index, value: benefit },
                  companyContent.benefit[index]
                );
              })
            )
          ),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'button',
              { className: 'btn btn-mini btn-default' },
              'ADD'
            )
          )
        )
      );
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (!nextProps.status.edit_company_benefit && this.props.status.edit_company_benefit) _CompanyProfileActionCreators2.default.getProfile(this.props.user.key);

      return JSON.stringify(nextProps.profile.benefits) !== JSON.stringify(this.props.profile.benefits) || nextProps.status.edit_company_benefit !== this.props.status.edit_company_benefit || nextProps.editor !== this.props.editor;
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].company_profile.header;
      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header.benefits,
          ' '
        ),
        ' ',
        _react2.default.createElement('br', null),
        this.props.editor ? this.editorRender() : this.contentRender()
      );
    }
  }]);

  return CompanyBenefit;
}(_react.Component);

exports.default = CompanyBenefit;