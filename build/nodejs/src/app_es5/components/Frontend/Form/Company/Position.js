'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _JobActionCreators = require('../../../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanyPosition = function (_Component) {
  _inherits(CompanyPosition, _Component);

  function CompanyPosition() {
    _classCallCheck(this, CompanyPosition);

    return _possibleConstructorReturn(this, (CompanyPosition.__proto__ || Object.getPrototypeOf(CompanyPosition)).apply(this, arguments));
  }

  _createClass(CompanyPosition, [{
    key: 'onChooseJob',
    value: function onChooseJob(job_index, evt) {
      _JobActionCreators2.default.chooseJob(job_index);
    }
  }, {
    key: 'seeMore',
    value: function seeMore(evt) {
      $(".position_more").show();
      $(evt.target).hide();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var header = _configLang2.default[this.props.lang].company_profile.header;
      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header.positions,
          ' '
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'ul',
            null,
            typeof this.props.profile.jobs !== 'undefined' ? this.props.profile.jobs.map(function (job, index) {
              return _react2.default.createElement(
                'li',
                { key: index, onClick: _this2.onChooseJob.bind(_this2, index), className: index >= 3 ? "position_more" : "", style: { display: index >= 3 ? "none" : "" } },
                _react2.default.createElement(
                  'div',
                  { className: 'position--content', 'data-toggle': 'modal', 'data-target': '#myModal__positions' },
                  _react2.default.createElement(
                    'p',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      job.position
                    ),
                    _react2.default.createElement('br', null),
                    'Type:',
                    ' ',
                    _react2.default.createElement(
                      'span',
                      { style: { color: "#aaa" } },
                      job.type
                    ),
                    ' ',
                    _react2.default.createElement('br', null),
                    'Min.Required Experience:',
                    ' ',
                    _react2.default.createElement(
                      'span',
                      { style: { color: "#aaa" } },
                      job.min_year,
                      ' years'
                    ),
                    ' ',
                    _react2.default.createElement('br', null),
                    'Compensation:',
                    ' ',
                    _react2.default.createElement(
                      'span',
                      { style: { color: "#aaa" } },
                      job.compensation
                    )
                  )
                )
              );
            }) : "",
            this.props.profile.jobs.length > 3 ? _react2.default.createElement(
              'a',
              { href: 'javascript:void(0)', style: { color: "#aaa", textDecoration: "underline", float: "right" }, onClick: this.seeMore.bind(this) },
              this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
            ) : ""
          )
        )
      );
    }
  }]);

  return CompanyPosition;
}(_react.Component);

exports.default = CompanyPosition;

/*

<a className="btn--expand" style={{ color:"#aaa", textDecoration: "underline", float: "right", marginBottom: "16px" }}>Read more..</a>
*/