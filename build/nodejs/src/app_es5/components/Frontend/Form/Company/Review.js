'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _CompanyProfileActionCreators = require('../../../../actions/CompanyProfileActionCreators');

var _CompanyProfileActionCreators2 = _interopRequireDefault(_CompanyProfileActionCreators);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var overall_calcurate = false;

var CompanyReview = function (_Component) {
  _inherits(CompanyReview, _Component);

  function CompanyReview() {
    _classCallCheck(this, CompanyReview);

    return _possibleConstructorReturn(this, (CompanyReview.__proto__ || Object.getPrototypeOf(CompanyReview)).apply(this, arguments));
  }

  _createClass(CompanyReview, [{
    key: 'onUpdateReview',
    value: function onUpdateReview(name, value, evt) {
      overall_calcurate = false;
      _ResourceActionCreators2.default.setDraft(name, value);
    }
  }, {
    key: 'sendReview',
    value: function sendReview(evt) {
      var response = this.props.profile.draft;
      response.to = this.props.profile.key;

      if (this.refs.agreement.checked) _CompanyProfileActionCreators2.default.reviewCompany(this.props.user.token, response);else alert("Please accept term of use");
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(evt) {
      if (!overall_calcurate && typeof this.props.profile.draft !== 'undefined' && typeof this.props.profile.draft.work !== 'undefined' && typeof this.props.profile.draft.benefit !== 'undefined' && typeof this.props.profile.draft.career !== 'undefined' && typeof this.props.profile.draft.recommend !== 'undefined') {
        _ResourceActionCreators2.default.setDraft('overall', (this.props.profile.draft.work + this.props.profile.draft.benefit + this.props.profile.draft.career + this.props.profile.draft.recommend) / 4);
        overall_calcurate = true;
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      // click review1
      $('#myModal__review .first-item1').click(function () {
        $("i.first-item1").prevAll().addClass("reviewed");
        $("i.first-item1").nextAll().removeClass("reviewed");
        $("i.first-item1").addClass("reviewed");
      });

      $('#myModal__review .second-item1').click(function () {
        $("i.second-item1").prevAll().addClass("reviewed");
        $("i.second-item1").nextAll().removeClass("reviewed");
        $("i.second-item1").addClass("reviewed");
      });

      $('#myModal__review .third-item1').click(function () {
        $("i.third-item1").prevAll().addClass("reviewed");
        $("i.third-item1").nextAll().removeClass("reviewed");
        $("i.third-item1").addClass("reviewed");
      });

      $('#myModal__review .forth-item1').click(function () {
        $("i.forth-item1").prevAll().addClass("reviewed");
        $("i.forth-item1").nextAll().removeClass("reviewed");
        $("i.forth-item1").addClass("reviewed");
      });

      $('#myModal__review .fifth-item1').click(function () {
        $("i.fifth-item1").prevAll().addClass("reviewed");
        $("i.fifth-item1").addClass("reviewed");
      });

      // click review2
      $('#myModal__review .first-item2').click(function () {
        $("i.first-item2").prevAll().addClass("reviewed");
        $("i.first-item2").nextAll().removeClass("reviewed");
        $("i.first-item2").addClass("reviewed");
      });

      $('#myModal__review .second-item2').click(function () {
        $("i.second-item2").prevAll().addClass("reviewed");
        $("i.second-item2").nextAll().removeClass("reviewed");
        $("i.second-item2").addClass("reviewed");
      });

      $('#myModal__review .third-item2').click(function () {
        $("i.third-item2").prevAll().addClass("reviewed");
        $("i.third-item2").nextAll().removeClass("reviewed");
        $("i.third-item2").addClass("reviewed");
      });

      $('#myModal__review .forth-item2').click(function () {
        $("i.forth-item2").prevAll().addClass("reviewed");
        $("i.forth-item2").nextAll().removeClass("reviewed");
        $("i.forth-item2").addClass("reviewed");
      });

      $('#myModal__review .fifth-item2').click(function () {
        $("i.fifth-item2").prevAll().addClass("reviewed");
        $("i.fifth-item2").addClass("reviewed");
      });

      // click review3
      $('#myModal__review .first-item3').click(function () {
        $("i.first-item3").prevAll().addClass("reviewed");
        $("i.first-item3").nextAll().removeClass("reviewed");
        $("i.first-item3").addClass("reviewed");
      });

      $('#myModal__review .second-item3').click(function () {
        $("i.second-item3").prevAll().addClass("reviewed");
        $("i.second-item3").nextAll().removeClass("reviewed");
        $("i.second-item3").addClass("reviewed");
      });

      $('#myModal__review .third-item3').click(function () {
        $("i.third-item3").prevAll().addClass("reviewed");
        $("i.third-item3").nextAll().removeClass("reviewed");
        $("i.third-item3").addClass("reviewed");
      });

      $('#myModal__review .forth-item3').click(function () {
        $("i.forth-item3").prevAll().addClass("reviewed");
        $("i.forth-item3").nextAll().removeClass("reviewed");
        $("i.forth-item3").addClass("reviewed");
      });

      $('#myModal__review .fifth-item3').click(function () {
        $("i.fifth-item3").prevAll().addClass("reviewed");
        $("i.fifth-item3").addClass("reviewed");
      });

      // click overall review
      $('#myModal__review .first-item4').click(function () {
        $("i.first-item4").prevAll().addClass("reviewed");
        $("i.first-item4").nextAll().removeClass("reviewed");
        $("i.first-item4").addClass("reviewed");
      });

      $('#myModal__review .second-item4').click(function () {
        $("i.second-item4").prevAll().addClass("reviewed");
        $("i.second-item4").nextAll().removeClass("reviewed");
        $("i.second-item4").addClass("reviewed");
      });

      $('#myModal__review .third-item4').click(function () {
        $("i.third-item4").prevAll().addClass("reviewed");
        $("i.third-item4").nextAll().removeClass("reviewed");
        $("i.third-item4").addClass("reviewed");
      });

      $('#myModal__review .forth-item4').click(function () {
        $("i.forth-item4").prevAll().addClass("reviewed");
        $("i.forth-item4").nextAll().removeClass("reviewed");
        $("i.forth-item4").addClass("reviewed");
      });

      $('#myModal__review .fifth-item4').click(function () {
        $("i.fifth-item4").prevAll().addClass("reviewed");
        $("i.fifth-item4").addClass("reviewed");
      });
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.profile.reviewing && this.props.profile.reviewing) {
        alert("Review Company Complete!");
        $("#myModal__review").modal('hide');
        _CompanyProfileActionCreators2.default.getProfile(this.props.profile.key, this.props.user.token);
      }

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'modal fade', id: 'myModal__review', tabIndex: '-1', role: 'dialog', 'aria-labelledby': 'myModalLabel' },
        _react2.default.createElement(
          'div',
          { className: 'modal-dialog', role: 'document' },
          _react2.default.createElement(
            'div',
            { className: 'modal-content' },
            _react2.default.createElement(
              'div',
              { className: 'modal-header' },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' },
                _react2.default.createElement(
                  'span5',
                  { 'aria-hidden': 'true' },
                  '\xD7'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'modal-title' },
                _react2.default.createElement(
                  'b',
                  null,
                  _react2.default.createElement(
                    'span',
                    null,
                    'Rate this Company'
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-body' },
              _react2.default.createElement(
                'b',
                null,
                _react2.default.createElement(
                  'p',
                  null,
                  'It only takes a minute!'
                ),
                _react2.default.createElement(
                  'p',
                  null,
                  'Your anonymous review will help other job seekers.'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'review--content' },
                _react2.default.createElement(
                  'p',
                  null,
                  '1. How do you rate the work environment and company culture ?'
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review first-item1', onClick: this.onUpdateReview.bind(this, 'work', 1), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review second-item1', onClick: this.onUpdateReview.bind(this, 'work', 2), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review third-item1', onClick: this.onUpdateReview.bind(this, 'work', 3), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review forth-item1', onClick: this.onUpdateReview.bind(this, 'work', 4), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review fifth-item1', onClick: this.onUpdateReview.bind(this, 'work', 5), 'aria-hidden': 'true' })
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'review--content' },
                _react2.default.createElement(
                  'p',
                  null,
                  '2. How do you rate the compensation and benefits ?'
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review first-item2', onClick: this.onUpdateReview.bind(this, 'benefit', 1), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review second-item2', onClick: this.onUpdateReview.bind(this, 'benefit', 2), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review third-item2', onClick: this.onUpdateReview.bind(this, 'benefit', 3), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review forth-item2', onClick: this.onUpdateReview.bind(this, 'benefit', 4), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review fifth-item2', onClick: this.onUpdateReview.bind(this, 'benefit', 5), 'aria-hidden': 'true' })
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'review--content' },
                _react2.default.createElement(
                  'p',
                  null,
                  '3. How do you rate the career opportunities within this company ?'
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review first-item3', onClick: this.onUpdateReview.bind(this, 'career', 1), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review second-item3', onClick: this.onUpdateReview.bind(this, 'career', 2), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review third-item3', onClick: this.onUpdateReview.bind(this, 'career', 3), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review forth-item3', onClick: this.onUpdateReview.bind(this, 'career', 4), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review fifth-item3', onClick: this.onUpdateReview.bind(this, 'career', 5), 'aria-hidden': 'true' })
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'review--content' },
                _react2.default.createElement(
                  'p',
                  null,
                  '4. Would you recommend this company to your friends ?'
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review first-item4', onClick: this.onUpdateReview.bind(this, 'recommend', 1), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review second-item4', onClick: this.onUpdateReview.bind(this, 'recommend', 2), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review third-item4', onClick: this.onUpdateReview.bind(this, 'recommend', 3), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review forth-item4', onClick: this.onUpdateReview.bind(this, 'recommend', 4), 'aria-hidden': 'true' }),
                  _react2.default.createElement('i', { className: 'fa fa-circle pointer__review fifth-item4', onClick: this.onUpdateReview.bind(this, 'recommend', 5), 'aria-hidden': 'true' })
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'review--content', style: { marginTop: "20px" } },
                _react2.default.createElement(
                  'p',
                  null,
                  _react2.default.createElement(
                    'b',
                    null,
                    'Overall Rating'
                  )
                ),
                _react2.default.createElement(
                  'div',
                  null,
                  typeof this.props.profile.draft !== 'undefined' && typeof this.props.profile.draft.overall !== 'undefined' ? [1, 2, 3, 4, 5].map(function (value, index) {
                    return _react2.default.createElement('i', { key: index, className: 'fa fa-thumbs-up overall__review', 'aria-hidden': 'true', style: { color: value <= _this2.props.profile.draft.overall ? "#f60" : "" } });
                  }) : [1, 2, 3, 4, 5].map(function (value, index) {
                    return _react2.default.createElement('i', { key: index, className: 'fa fa-thumbs-up overall__review', 'aria-hidden': 'true' });
                  })
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'review--policy' },
                _react2.default.createElement(
                  'div',
                  { className: 'modal__checkbox' },
                  _react2.default.createElement(
                    'div',
                    { className: 'checkbox--remember' },
                    _react2.default.createElement('input', { type: 'checkbox', ref: 'agreement', name: 'remember' }),
                    'I agree to the splitshift\'s ',
                    _react2.default.createElement(
                      'span',
                      null,
                      'Term of Use'
                    ),
                    '. This review of my experience at my current or former employer is truthful.'
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'modal-footer' },
              _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                  'a',
                  { href: 'javascript:void(0)', onClick: this.sendReview.bind(this) },
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default float' },
                    'Submit Review'
                  )
                ),
                _react2.default.createElement(
                  'a',
                  { href: '#' },
                  _react2.default.createElement(
                    'button',
                    { className: 'btn btn-default float reviewform__btn', 'data-dismiss': 'modal' },
                    'Cancel'
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return CompanyReview;
}(_react.Component);

exports.default = CompanyReview;