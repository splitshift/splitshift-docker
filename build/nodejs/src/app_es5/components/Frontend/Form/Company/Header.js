'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _CompanyProfileActionCreators = require('../../../../actions/CompanyProfileActionCreators');

var _CompanyProfileActionCreators2 = _interopRequireDefault(_CompanyProfileActionCreators);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var mapPopupRender = false;

var CompanyHeader = function (_Component) {
  _inherits(CompanyHeader, _Component);

  function CompanyHeader() {
    _classCallCheck(this, CompanyHeader);

    return _possibleConstructorReturn(this, (CompanyHeader.__proto__ || Object.getPrototypeOf(CompanyHeader)).apply(this, arguments));
  }

  _createClass(CompanyHeader, [{
    key: 'changeProfileImage',
    value: function changeProfileImage(evt) {
      _CompanyProfileActionCreators2.default.uploadProfileImage(this.props.user.token, evt.target.files[0]);
    }
  }, {
    key: 'changeBannerImage',
    value: function changeBannerImage(evt) {
      _CompanyProfileActionCreators2.default.uploadBannerImage(this.props.user.token, evt.target.files[0]);
    }
  }, {
    key: 'updateDraft',
    value: function updateDraft(name, evt) {
      _ResourceActionCreators2.default.setDraft(name, evt.target.value);
    }
  }, {
    key: 'updateProfile',
    value: function updateProfile(evt) {
      evt.preventDefault();

      var result = {
        first_name: this.props.profile.draft.first_name ? this.props.profile.draft.first_name : this.props.profile.first_name,
        last_name: this.props.profile.draft.last_name ? this.props.profile.draft.last_name : this.props.profile.last_name,
        company_name: this.props.profile.draft.company_name ? this.props.profile.draft.company_name : this.props.profile.company_name,
        type: this.props.profile.draft.type ? this.props.profile.draft.type : this.props.profile.type
      };

      _CompanyProfileActionCreators2.default.updateInformation(this.props.user.token, result);
    }
  }, {
    key: 'setEditor',
    value: function setEditor(name) {

      var oldData = {
        first_name: this.props.profile.first_name,
        last_name: this.props.profile.last_name,
        company_name: this.props.profile.company_name,
        type: this.props.profile.type ? this.props.profile.type : "Hotel"
      };

      _ResourceActionCreators2.default.setOldDraft(oldData);
      _ResourceActionCreators2.default.setEditor(name, true);
    }
  }, {
    key: 'openUploadMenu',
    value: function openUploadMenu(evt) {
      $(".edit-menu__upload").toggleClass("opened");
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var menusContent = _configLang2.default[this.props.lang].company_profile.menus;

      return _react2.default.createElement(
        'div',
        { className: 'detail--name' },
        _react2.default.createElement(
          'h2',
          null,
          _react2.default.createElement(
            'b',
            null,
            _react2.default.createElement(
              'span1',
              { className: 'company--name' },
              this.props.profile.company_name
            )
          )
        ),
        _react2.default.createElement(
          'h3',
          null,
          _react2.default.createElement(
            'b',
            null,
            _react2.default.createElement(
              'span1',
              null,
              this.props.profile.address && this.props.profile.address.city,
              ' ',
              this.props.profile.address && this.props.profile.address.location ? _react2.default.createElement(
                'a',
                { className: 'popup-gmaps', href: "https://maps.google.com/maps?q=" + this.props.profile.address.location[1] + ',' + this.props.profile.address.location[0] },
                _react2.default.createElement(
                  'span1',
                  null,
                  _react2.default.createElement('i', { className: 'fa fa-map-marker', 'aria-hidden': 'true' })
                )
              ) : ""
            )
          )
        ),
        this.props.editor ? _react2.default.createElement(
          'span',
          { className: 'detail--editbtn' },
          _react2.default.createElement(
            _reactRouter.Link,
            { className: 'btn btn-default reverse', to: '/' + this.props.lang + '/company/' + this.props.profile.key },
            _react2.default.createElement(
              'span1',
              null,
              menusContent[2]
            )
          ),
          _react2.default.createElement(
            'button',
            { className: 'btn btn-default reverse', style: { margin: "10px 0 10px 5px" }, onClick: this.setEditor.bind(this, "edit_company_profile_card") },
            _react2.default.createElement(
              'span1',
              null,
              menusContent[3]
            )
          )
        ) : ""
      );
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      return this.props.profile ? _react2.default.createElement(
        'div',
        { className: 'detail--name' },
        _react2.default.createElement(
          'h2',
          null,
          _react2.default.createElement(
            'b',
            null,
            _react2.default.createElement(
              'span1',
              null,
              'Edit Profile'
            )
          )
        ),
        _react2.default.createElement(
          'form',
          { onSubmit: this.updateProfile.bind(this) },
          _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.updateDraft.bind(this, 'first_name'), defaultValue: this.props.profile.first_name, placeholder: 'First Name' }),
          _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.updateDraft.bind(this, 'last_name'), defaultValue: this.props.profile.last_name, placeholder: 'Last Name' }),
          _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.updateDraft.bind(this, 'company_name'), defaultValue: this.props.profile.company_name, placeholder: 'Company Name' }),
          _react2.default.createElement(
            'select',
            { className: 'form-control form-control--mini', onChange: this.updateDraft.bind(this, 'type'), defaultValue: this.props.profile.type },
            _react2.default.createElement(
              'option',
              null,
              'Hotel'
            ),
            _react2.default.createElement(
              'option',
              null,
              'Restaurant'
            ),
            _react2.default.createElement(
              'option',
              null,
              'Bar'
            ),
            _react2.default.createElement(
              'option',
              null,
              'Nightclub'
            )
          ),
          _react2.default.createElement(
            'button',
            { className: 'btn btn-default reverse' },
            _react2.default.createElement(
              'span1',
              null,
              'Save Profile Changes'
            )
          )
        )
      ) : "";
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.status.edit_company_profile_card && this.props.status.edit_company_profile_card) _CompanyProfileActionCreators2.default.getProfile(this.props.user.key);

      return true;
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      mapPopupRender = false;
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {

      if (!mapPopupRender && this.props.profile.address && this.props.profile.address.location) {

        $('.popup-gmaps').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false
        });

        mapPopupRender = true;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var backgroundStyle = {};

      if (this.props.profile.draft.template_banner || this.props.profile.banner) {
        backgroundStyle = {
          background: "url(" + (this.props.profile.draft.template_banner || this.props.profile.banner) + ") no-repeat center center / cover"
        };

        if (this.props.profile.banner === '/images/loading.svg') backgroundStyle.backgroundSize = "80px 60px";
      } else {
        backgroundStyle = {
          backgroundColor: "#ccc"
        };
      }

      var editorContent = _configLang2.default[this.props.lang].company_profile.editor;

      return _react2.default.createElement(
        'div',
        { style: backgroundStyle },
        _react2.default.createElement(
          'div',
          { className: this.props.editor ? "company-cover company-cover-hover" : "company-cover" },
          this.props.editor ? _react2.default.createElement(
            'div',
            { className: 'edit__banner', style: { zIndex: "10" } },
            _react2.default.createElement(
              'span',
              { className: 'edit__header', onClick: this.openUploadMenu.bind(this) },
              editorContent.upload.banner[0],
              _react2.default.createElement('i', { className: 'fa fa-camera' })
            ),
            _react2.default.createElement(
              'div',
              { className: 'edit-menu-company' },
              _react2.default.createElement(
                'div',
                { className: 'edit-menu__dropdown edit-menu__upload' },
                _react2.default.createElement(
                  'div',
                  { className: 'edit-menu__item', onClick: _JQueryTool2.default.triggerUpload.bind(this, "upload-company-banner-image") },
                  _react2.default.createElement('i', { className: 'fa fa-pencil', style: { color: "black" } }),
                  ' ',
                  editorContent.upload.banner[1]
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'edit-menu__item', 'data-toggle': 'modal', 'data-target': '#myModal__background' },
                  _react2.default.createElement('i', { className: 'fa fa-picture-o', style: { color: "black" } }),
                  ' ',
                  editorContent.upload.banner[2]
                )
              )
            ),
            _react2.default.createElement('input', { id: 'upload-company-banner-image', type: 'file', onChange: this.changeBannerImage.bind(this), style: { display: "none" } })
          ) : "",
          _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
              'div',
              { className: 'company__detail' },
              _react2.default.createElement(
                'div',
                { className: 'cover__detail' },
                _react2.default.createElement(
                  'div',
                  { className: 'detail--pic' },
                  _react2.default.createElement('img', { src: this.props.profile.profile_image ? this.props.profile.profile_image : "/images/avatar-image.jpg" }),
                  this.props.editor ? _react2.default.createElement(
                    'div',
                    { className: 'profilepic__edit', onClick: _JQueryTool2.default.triggerUpload.bind(this, "upload-company-profile") },
                    _react2.default.createElement(
                      'p',
                      null,
                      _react2.default.createElement('i', { className: 'fa fa-camera' }),
                      ' ',
                      editorContent.upload.profile
                    ),
                    _react2.default.createElement('input', { id: 'upload-company-profile', type: 'file', onChange: this.changeProfileImage.bind(this), style: { display: "none" } })
                  ) : ""
                ),
                this.props.status.edit_company_profile_card ? this.editorRender() : this.contentRender()
              )
            )
          ),
          !this.props.status.edit_company_profile_card ? _react2.default.createElement(
            'div',
            { className: 'cover__review' },
            _react2.default.createElement(
              'div',
              { className: 'review--star' },
              _react2.default.createElement(
                'h3',
                null,
                _react2.default.createElement(
                  'b',
                  null,
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'p',
                      null,
                      'Overall Employee'
                    ),
                    ' ',
                    _react2.default.createElement(
                      'p',
                      null,
                      'Satisfaction'
                    ),
                    [1, 2, 3, 4, 5].map(function (value, index) {
                      return value <= _this2.props.profile.overall ? _react2.default.createElement(
                        'span',
                        { key: index },
                        _react2.default.createElement('i', { className: 'fa fa-thumbs-up', 'aria-hidden': 'true' }),
                        ' '
                      ) : _react2.default.createElement(
                        'span6',
                        { key: index },
                        _react2.default.createElement('i', { className: 'fa fa-thumbs-up', 'aria-hidden': 'true' }),
                        ' '
                      );
                    }),
                    ' ',
                    this.props.profile.overall ? Math.round(this.props.profile.overall * 10) / 10 : "-"
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'review--btn' },
              this.props.user.type === 'Employee' && this.props.profile.employees && this.props.profile.employees.filter(function (employee) {
                return employee.key == _this2.props.user.key;
              }).length > 0 ? _react2.default.createElement(
                'button',
                { className: 'btn btn-default reverse', 'data-toggle': 'modal', 'data-target': '#myModal__review' },
                _react2.default.createElement(
                  'span1',
                  null,
                  'Review'
                )
              ) : "",
              _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                  'span1',
                  null,
                  this.props.profile.review ? this.props.profile.review.length : "0",
                  ' Review',
                  this.props.profile.review && this.props.profile.review.length > 1 ? "s" : ""
                )
              )
            )
          ) : ""
        )
      );
    }
  }]);

  return CompanyHeader;
}(_react.Component);

exports.default = CompanyHeader;