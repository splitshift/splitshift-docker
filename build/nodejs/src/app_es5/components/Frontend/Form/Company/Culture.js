'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _CompanyProfileActionCreators = require('../../../../actions/CompanyProfileActionCreators');

var _CompanyProfileActionCreators2 = _interopRequireDefault(_CompanyProfileActionCreators);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var expanding = 0;

var CompanyCulture = function (_Component) {
  _inherits(CompanyCulture, _Component);

  function CompanyCulture() {
    _classCallCheck(this, CompanyCulture);

    return _possibleConstructorReturn(this, (CompanyCulture.__proto__ || Object.getPrototypeOf(CompanyCulture)).apply(this, arguments));
  }

  _createClass(CompanyCulture, [{
    key: 'updateDraft',
    value: function updateDraft(name, evt) {

      if (!this.props.status.edit_company_culture) {
        _ResourceActionCreators2.default.setEditor('edit_company_culture', true);
        $(this.refs.culture).expanding();
        $(this.refs.culture)[0].focus();
      }

      _ResourceActionCreators2.default.setDraft(name, evt.target.value);
    }
  }, {
    key: 'updateCulture',
    value: function updateCulture(evt) {
      evt.preventDefault();
      _CompanyProfileActionCreators2.default.updateCulture(this.props.user.token, this.props.profile.draft);
    }
  }, {
    key: 'contentRender',
    value: function contentRender(text) {

      var draftText = text;
      draftText = draftText.replace(/\n/g, "<br/>");

      return _react2.default.createElement(
        'div',
        { className: 'row' },
        _react2.default.createElement('p', { dangerouslySetInnerHTML: { __html: draftText } })
      );
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      return this.props.profile && typeof this.props.profile.culture !== "undefined" ? _react2.default.createElement(
        'form',
        { onSubmit: this.updateCulture.bind(this) },
        _react2.default.createElement('textarea', { ref: 'culture', className: 'form-control form-control--mini', defaultValue: this.props.profile.culture, onChange: this.updateDraft.bind(this, 'culture') }),
        _react2.default.createElement(
          'button',
          { className: 'btn btn-mini btn-default' },
          'Save'
        )
      ) : "";
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.status.edit_company_culture && this.props.status.edit_company_culture) _CompanyProfileActionCreators2.default.getProfile(this.props.user.key);

      return true;
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      expanding = 0;
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (expanding < 2 && this.refs.culture) {
        $(this.refs.culture).expanding();
        expanding++;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].company_profile.header;

      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header.culture
        ),
        this.props.editor ? this.editorRender() : this.contentRender(this.props.profile.culture)
      );
    }
  }]);

  return CompanyCulture;
}(_react.Component);

exports.default = CompanyCulture;

/*
<div className="text--expand">
  <p>
    This reflects our core brand values of Crafted Comfort, Authentic Culture and Destination Discovery; a belief that we invite our guests to experience and ‘Live the Remarkable’ at our hotels and residences.
  </p>
</div>
<a className="btn--expand">Read more...</a>
*/