'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _FeedActionCreators = require('../../../../actions/FeedActionCreators');

var _FeedActionCreators2 = _interopRequireDefault(_FeedActionCreators);

var _JQueryTool = require('../../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanyGallery = function (_Component) {
  _inherits(CompanyGallery, _Component);

  function CompanyGallery() {
    _classCallCheck(this, CompanyGallery);

    return _possibleConstructorReturn(this, (CompanyGallery.__proto__ || Object.getPrototypeOf(CompanyGallery)).apply(this, arguments));
  }

  _createClass(CompanyGallery, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _FeedActionCreators2.default.getPhotosFeed(this.props.profile.key, this.props.user.token);
      _JQueryTool2.default.popupGallery();
    }
  }, {
    key: 'render',
    value: function render() {
      var headerContent = _configLang2.default[this.props.lang].company_profile.header.gallery;
      return this.props.feed.photos.length > 0 ? _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
          _react2.default.createElement(
            'h3',
            { className: 'inner' },
            headerContent
          ),
          _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
              'div',
              { className: 'popup-gallery' },
              this.props.feed.photos.map(function (photo, index) {
                return _react2.default.createElement(
                  'a',
                  { key: index, href: photo.path.substring(6) },
                  _react2.default.createElement(
                    'div',
                    { className: 'people--responsive' },
                    _react2.default.createElement(
                      'div',
                      { className: 'col-md-4 col-sm-6 col-xs-6', style: { padding: "0px 2px" } },
                      _react2.default.createElement(
                        'div',
                        { className: 'people__pic' },
                        _react2.default.createElement('img', { src: photo.path.substring(6) })
                      )
                    )
                  )
                );
              })
            )
          ),
          _react2.default.createElement(
            _reactRouter.Link,
            { to: "/th/company/" + this.props.profile.key + "/photo", onClick: this.props.triggerButton.bind(null), style: { color: "#aaa", textDecoration: "underline", float: "right" } },
            this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More'
          )
        ),
        !this.props.responsive ? _react2.default.createElement('hr', null) : ""
      ) : _react2.default.createElement('div', null);
    }
  }]);

  return CompanyGallery;
}(_react.Component);

exports.default = CompanyGallery;