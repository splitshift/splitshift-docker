'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _CompanyProfileActionCreators = require('../../../../actions/CompanyProfileActionCreators');

var _CompanyProfileActionCreators2 = _interopRequireDefault(_CompanyProfileActionCreators);

var _configLang = require('../../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

var _Map = require('../../Map');

var _Map2 = _interopRequireDefault(_Map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var choose_location = void 0;

var CompanyLocation = function (_Component) {
  _inherits(CompanyLocation, _Component);

  function CompanyLocation() {
    _classCallCheck(this, CompanyLocation);

    return _possibleConstructorReturn(this, (CompanyLocation.__proto__ || Object.getPrototypeOf(CompanyLocation)).apply(this, arguments));
  }

  _createClass(CompanyLocation, [{
    key: 'changeLatLng',
    value: function changeLatLng(lat, lng, evt) {
      _ResourceActionCreators2.default.setDraft('lat', lat);
      _ResourceActionCreators2.default.setDraft('lng', lng);
    }
  }, {
    key: 'updateDraft',
    value: function updateDraft(name, evt) {
      _ResourceActionCreators2.default.setDraft(name, evt.target.value);
    }
  }, {
    key: 'updateLocation',
    value: function updateLocation(evt) {
      evt.preventDefault();

      _ResourceActionCreators2.default.setEditor('edit_company_location', true);

      // Update Address
      var result = {
        information: this.props.profile.draft.information ? this.props.profile.draft.information : this.props.profile.address && this.props.profile.address.information ? this.props.profile.address.information : "",
        description: this.props.profile.draft.description ? this.props.profile.draft.description : this.props.profile.address && this.props.profile.address.description ? this.props.profile.address.description : "",
        city: this.props.profile.draft.city ? this.props.profile.draft.city : this.props.profile.address.city || "",
        country: "Thailand",
        district: this.props.profile.draft.district ? this.props.profile.draft.district : this.props.profile.address.district || "",
        zip_code: this.props.profile.draft.zip_code ? this.props.profile.draft.zip_code : this.props.profile.address.zip_code || "",
        tel: this.props.profile.draft.tel ? this.props.profile.draft.tel : this.props.profile.tel,
        contact_email: this.props.profile.draft.contact_email ? this.props.profile.draft.contact_email : this.props.profile.contact_email,
        lat: this.props.profile.draft.lat ? this.props.profile.draft.lat : this.props.profile.address && this.props.profile.address.lat ? this.props.profile.address.lat : "13.7563309",
        lng: this.props.profile.draft.lng ? this.props.profile.draft.lng : this.props.profile.address && this.props.profile.address.lng ? this.props.profile.address.lng : "100.5017651"
      };

      _CompanyProfileActionCreators2.default.updateAddress(this.props.user.token, result);
    }
  }, {
    key: 'contentRender',
    value: function contentRender() {
      var onEdit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      return _react2.default.createElement(
        'div',
        null,
        !onEdit ? _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            'p',
            null,
            this.props.profile.address && this.props.profile.address.information ? this.props.profile.address.information + ", " : "",
            this.props.profile.address && this.props.profile.address.district ? this.props.profile.address.district + ", " : "",
            this.props.profile.address && this.props.profile.address.city ? this.props.profile.address.city + " " : "",
            this.props.profile.address && this.props.profile.address.zip_code ? this.props.profile.address.zip_code + " " : "",
            'Thailand'
          ),
          _react2.default.createElement(
            'p',
            null,
            'Tel: ',
            this.props.profile.tel
          ),
          _react2.default.createElement(
            'p',
            null,
            'E-mail: ',
            this.props.profile.contact_email
          ),
          _react2.default.createElement(
            'h6',
            null,
            'Map'
          ),
          _react2.default.createElement(
            'p',
            null,
            this.props.profile.address && this.props.profile.address.description
          )
        ) : "",
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'location__map' },
            _react2.default.createElement(_Map2.default, { map_id: this.props.map_id, changeLatLng: this.changeLatLng, address: this.props.profile.address, triggerMap: this.props.triggerMap.bind(null) })
          )
        )
      );
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      return this.props.profile && this.props.profile.address ? _react2.default.createElement(
        'form',
        { onSubmit: this.updateLocation.bind(this) },
        this.contentRender(true),
        _react2.default.createElement(
          'p',
          null,
          'You can move pin to your location'
        ),
        _react2.default.createElement(
          'div',
          null,
          'Tel'
        ),
        _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', defaultValue: this.props.profile.tel, onChange: this.updateDraft.bind(this, 'tel'), placeholder: 'Tel' }),
        _react2.default.createElement(
          'div',
          null,
          'Contact Email'
        ),
        _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', defaultValue: this.props.profile.contact_email, onChange: this.updateDraft.bind(this, 'contact_email'), placeholder: 'Contact Email' }),
        _react2.default.createElement(
          'div',
          null,
          'Address'
        ),
        _react2.default.createElement('input', { id: 'location', type: 'text', className: 'form-control form-control--mini', defaultValue: this.props.profile.address.information, onChange: this.updateDraft.bind(this, 'information'), placeholder: 'Address' }),
        _react2.default.createElement(
          'div',
          null,
          'District'
        ),
        _react2.default.createElement('input', { id: 'location', type: 'text', className: 'form-control form-control--mini', defaultValue: this.props.profile.address.district, onChange: this.updateDraft.bind(this, 'district'), placeholder: 'District' }),
        _react2.default.createElement(
          'div',
          null,
          'City'
        ),
        _react2.default.createElement('input', { id: 'location', type: 'text', className: 'form-control form-control--mini', defaultValue: this.props.profile.address.city, onChange: this.updateDraft.bind(this, 'city'), placeholder: 'City' }),
        _react2.default.createElement(
          'div',
          null,
          'Zip Code'
        ),
        _react2.default.createElement('input', { id: 'location', type: 'text', className: 'form-control form-control--mini', defaultValue: this.props.profile.address.zip_code, onChange: this.updateDraft.bind(this, 'zip_code'), placeholder: 'Zip Code' }),
        _react2.default.createElement(
          'div',
          null,
          'Descripe your location'
        ),
        _react2.default.createElement('textarea', { className: 'form-control form-control--mini', rows: '5', defaultValue: this.props.profile.address.description, onChange: this.updateDraft.bind(this, 'description'), placeholder: 'Description' }),
        _react2.default.createElement(
          'button',
          { className: 'btn btn-mini btn-default' },
          'Save'
        )
      ) : "";
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.status.edit_company_location && this.props.status.edit_company_location) {
        _CompanyProfileActionCreators2.default.getProfile(this.props.user.key);
      }

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var header = _configLang2.default[this.props.lang].company_profile.header;

      return _react2.default.createElement(
        'div',
        { className: this.props.responsive ? "box_style_detail_2 profile-responsive" : "" },
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          header.location,
          ' '
        ),
        this.props.editor ? this.editorRender() : this.contentRender()
      );
    }
  }]);

  return CompanyLocation;
}(_react.Component);

exports.default = CompanyLocation;

/*
<input type="text" className="form-control form-control--mini" defaultValue={this.props.profile.address.location && this.props.profile.address.location[0]} onChange={this.updateDraft.bind(this, 'lat')} placeholder="Lat"/>
<input type="text" className="form-control form-control--mini" defaultValue={this.props.profile.address.location && this.props.profile.address.location[1]} onChange={this.updateDraft.bind(this, 'lng')} placeholder="Lng"/>

*/