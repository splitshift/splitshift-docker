'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ResourceActionCreators = require('../../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _CompanyProfileActionCreators = require('../../../../actions/CompanyProfileActionCreators');

var _CompanyProfileActionCreators2 = _interopRequireDefault(_CompanyProfileActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanySocialLink = function (_Component) {
  _inherits(CompanySocialLink, _Component);

  function CompanySocialLink() {
    _classCallCheck(this, CompanySocialLink);

    return _possibleConstructorReturn(this, (CompanySocialLink.__proto__ || Object.getPrototypeOf(CompanySocialLink)).apply(this, arguments));
  }

  _createClass(CompanySocialLink, [{
    key: 'updateDraft',
    value: function updateDraft(name, evt) {

      if (!this.props.status.edit_company_social_link) _ResourceActionCreators2.default.setEditor('edit_company_social_link', true);

      _ResourceActionCreators2.default.setDraft(name, evt.target.value);
    }
  }, {
    key: 'updateSocialLink',
    value: function updateSocialLink(evt) {
      evt.preventDefault();

      var socialLink = Object.assign({}, this.props.profile.links, this.props.profile.draft);
      _CompanyProfileActionCreators2.default.updateSocialLink(this.props.user.token, socialLink);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.status.edit_company_social_link && this.props.status.edit_company_social_link) _CompanyProfileActionCreators2.default.getProfile(this.props.user.key);

      return true;
    }
  }, {
    key: 'editorRender',
    value: function editorRender() {
      return this.props.profile && this.props.profile.key ? _react2.default.createElement(
        'form',
        { onSubmit: this.updateSocialLink.bind(this) },
        _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.updateDraft.bind(this, 'facebook'), placeholder: 'Facebook Link', defaultValue: this.props.profile.links.facebook }),
        _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.updateDraft.bind(this, 'instagram'), placeholder: 'Instagram Link', defaultValue: this.props.profile.links.instagram }),
        _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.updateDraft.bind(this, 'youtube'), placeholder: 'Youtube Link', defaultValue: this.props.profile.links.youtube }),
        _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.updateDraft.bind(this, 'linkedin'), placeholder: 'Linkedin Link', defaultValue: this.props.profile.links.linkedin }),
        _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.updateDraft.bind(this, 'website'), placeholder: 'Website Link', defaultValue: this.props.profile.links.website }),
        _react2.default.createElement(
          'span6',
          null,
          'Social media icon will be displayed below background picture.'
        ),
        ' ',
        _react2.default.createElement('br', null),
        _react2.default.createElement(
          'button',
          { className: 'btn btn-mini btn-default' },
          'Save'
        )
      ) : "";
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'h3',
          { className: 'inner' },
          'Social Link'
        ),
        this.editorRender()
      );
    }
  }]);

  return CompanySocialLink;
}(_react.Component);

exports.default = CompanySocialLink;