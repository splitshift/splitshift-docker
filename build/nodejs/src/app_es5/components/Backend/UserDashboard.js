'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _Connection = require('../Frontend/Form/User/Connection');

var _Connection2 = _interopRequireDefault(_Connection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UserPrivacy = { public: 'Public', timeline: 'My Timeline', connected: 'Connection', employer: 'Employer', onlyme: 'Private' };

var UserDashboard = function (_Component) {
  _inherits(UserDashboard, _Component);

  function UserDashboard() {
    _classCallCheck(this, UserDashboard);

    return _possibleConstructorReturn(this, (UserDashboard.__proto__ || Object.getPrototypeOf(UserDashboard)).apply(this, arguments));
  }

  _createClass(UserDashboard, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var cardStyle = {
        backgroundColor: "#ddd",
        margin: "10px",
        padding: "10px"
      };

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: 'dashboardmenu__responsive' },
          _react2.default.createElement(
            'h4',
            null,
            'Main Menu'
          ),
          _react2.default.createElement(
            _reactRouter.Link,
            { to: '/' + this.props.language + '/dashboard/application', className: 'btn btn-default' },
            'Application'
          ),
          ' ',
          _react2.default.createElement(
            _reactRouter.Link,
            { to: '/' + this.props.language + '/dashboard/resource/index', className: 'btn btn-default' },
            'Add Resource'
          ),
          ' ',
          _react2.default.createElement(
            _reactRouter.Link,
            { to: '/' + this.props.language + '/setting', className: 'btn btn-default' },
            'Setting'
          )
        ),
        _react2.default.createElement(
          'h2',
          null,
          'My Statistics'
        ),
        _react2.default.createElement('hr', null),
        _react2.default.createElement(
          'h4',
          null,
          'Posts'
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Total'
            ),
            this.props.stat.feed.all
          ),
          Object.keys(UserPrivacy).map(function (privacy, index) {
            return _react2.default.createElement(
              'div',
              { className: 'col-md-3', key: index, style: cardStyle },
              _react2.default.createElement(
                'h5',
                null,
                UserPrivacy[privacy]
              ),
              _this2.props.stat.feed[privacy] || "0"
            );
          })
        ),
        _react2.default.createElement('hr', null),
        _react2.default.createElement(
          'h4',
          null,
          'Resources'
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Total'
            ),
            this.props.stat.resource.all
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Approved'
            ),
            this.props.stat.resource.activated
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Pending Approval'
            ),
            this.props.stat.resource.deactivated
          )
        ),
        _react2.default.createElement('hr', null),
        _react2.default.createElement(
          'h4',
          null,
          'Connections'
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Total'
            ),
            this.props.stat.connections.length
          )
        ),
        _react2.default.createElement(
          'a',
          { href: '#', 'data-toggle': 'modal', 'data-target': '#myModal__connections' },
          'See Connections'
        ),
        _react2.default.createElement(_Connection2.default, { profile: this.props.stat, backend: true }),
        _react2.default.createElement('hr', null),
        _react2.default.createElement(
          'h4',
          null,
          'Applications'
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Total'
            ),
            this.props.stat.applyjob.all
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Open'
            ),
            this.props.stat.applyjob.open
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Decline'
            ),
            this.props.stat.applyjob.declined
          )
        )
      );
    }
  }]);

  return UserDashboard;
}(_react.Component);

exports.default = UserDashboard;