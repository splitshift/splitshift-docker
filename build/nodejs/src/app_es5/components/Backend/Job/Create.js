'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _JobActionCreators = require('../../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _ConfigData = require('../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var JobCreate = function (_Component) {
  _inherits(JobCreate, _Component);

  function JobCreate() {
    _classCallCheck(this, JobCreate);

    return _possibleConstructorReturn(this, (JobCreate.__proto__ || Object.getPrototypeOf(JobCreate)).apply(this, arguments));
  }

  _createClass(JobCreate, [{
    key: 'onEdit',
    value: function onEdit(name, evt) {
      var response = {};
      var result = evt.target.value;

      if (name == 'expire_date') {
        if (result) result = new Date(new Date().getTime() + result * 24 * 60 * 60 * 1000);else result = this.props.job.draft.old_expire_date;
      }

      response[name] = result;
      _JobActionCreators2.default.updateJobDraft(response);
    }
  }, {
    key: 'onAddSkill',
    value: function onAddSkill(evt) {

      evt.preventDefault();

      var skill = this.refs.new_skill.value;
      this.refs.new_skill.value = "";

      var response = this.props.job.draft;

      if (typeof response.required_skills === 'undefined') response.required_skills = [skill];else response.required_skills.push(skill);

      _JobActionCreators2.default.updateJobDraft(response);
    }
  }, {
    key: 'onRemoveSkill',
    value: function onRemoveSkill(index, evt) {

      var response = this.props.job.draft;
      response.required_skills.splice(index, 1);

      _JobActionCreators2.default.updateJobDraft(response);
    }
  }, {
    key: 'createJob',
    value: function createJob(evt) {
      evt.preventDefault();

      var result = this.props.job.draft;

      if (typeof result.type === 'undefined') result.type = this.refs.job_type.value;

      if (typeof result.compensation === 'undefined') result.compensation = this.refs.compensation.value;

      _JobActionCreators2.default.createNewJob(this.props.user.token, result);
    }
  }, {
    key: 'editJob',
    value: function editJob(evt) {
      evt.preventDefault();

      _JobActionCreators2.default.updateJob(this.props.user.token, this.props.job.draft);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _JobActionCreators2.default.resetDraft();
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.job.updating && this.props.job.updating) {

        var alert_word = "Add Job Complete!";

        if (this.props.job.draft._id) alert_word = "Edit Job Complete!";

        alert(alert_word);

        _reactRouter.browserHistory.push('/' + this.props.language + '/dashboard/job');
      }

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'form',
          null,
          _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
              'div',
              { className: 'col-md-8 col-md-offset-2' },
              'Job Title',
              _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini ', placeholder: 'Position', onChange: this.onEdit.bind(this, 'position'), defaultValue: this.props.job.draft.position }),
              'Job Type',
              _react2.default.createElement(
                'select',
                { ref: 'job_type', className: 'form-control form-control--mini ', onChange: this.onEdit.bind(this, 'type'), defaultValue: this.props.job.draft.type },
                _ConfigData2.default.JobTypeList(1).map(function (job, index) {
                  return _react2.default.createElement(
                    'option',
                    { key: index, value: job },
                    job
                  );
                })
              ),
              'Job Description',
              _react2.default.createElement('textarea', { className: 'form-control form-control--mini ', onChange: this.onEdit.bind(this, 'description'), defaultValue: this.props.job.draft.description }),
              'Minimum Year Experience (Number only)',
              _react2.default.createElement('input', { type: 'number', className: 'form-control form-control--mini', placeholder: 'Min Year', onChange: this.onEdit.bind(this, 'min_year'), defaultValue: this.props.job.draft.min_year }),
              'Compensation',
              _react2.default.createElement(
                'select',
                { ref: 'compensation', className: 'form-control form-control--mini ', onChange: this.onEdit.bind(this, 'compensation'), defaultValue: this.props.job.draft.compensation },
                _ConfigData2.default.CompensationList().map(function (compensation, index) {
                  return _react2.default.createElement(
                    'option',
                    { key: index },
                    compensation
                  );
                })
              ),
              'Required Skill',
              _react2.default.createElement(
                'div',
                { className: 'row' },
                _react2.default.createElement(
                  'form',
                  { onSubmit: this.onAddSkill.bind(this) },
                  _react2.default.createElement(
                    'div',
                    { className: 'col-md-12' },
                    typeof this.props.job.draft.required_skills !== 'undefined' ? _react2.default.createElement(
                      'table',
                      { className: 'table table-bordered' },
                      this.props.job.draft.required_skills.map(function (skill, index) {
                        return _react2.default.createElement(
                          'tr',
                          { key: index },
                          _react2.default.createElement(
                            'th',
                            null,
                            ' ',
                            skill,
                            ' '
                          ),
                          _react2.default.createElement(
                            'th',
                            null,
                            ' ',
                            _react2.default.createElement(
                              'a',
                              { href: 'javascript:void(0)', onClick: _this2.onRemoveSkill.bind(_this2, index) },
                              'Remove'
                            ),
                            ' '
                          )
                        );
                      })
                    ) : ""
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'col-md-9' },
                    _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', ref: 'new_skill', placeholder: 'Job Skill...' })
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'col-md-3' },
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default btn-mini--right' },
                      'ADD'
                    )
                  )
                )
              ),
              'Expired Date on ',
              _react2.default.createElement(
                'b',
                null,
                _JQueryTool2.default.changeDatePickerFormat(this.props.job.draft.expire_date)
              ),
              _react2.default.createElement(
                'div',
                { className: 'row' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-9' },
                  _react2.default.createElement(
                    'select',
                    { className: 'form-control form-control--mini ', onChange: this.onEdit.bind(this, 'expire_date') },
                    _react2.default.createElement(
                      'option',
                      { value: '' },
                      'Default'
                    ),
                    _react2.default.createElement(
                      'option',
                      { value: '60' },
                      'in 60 days from now'
                    ),
                    _react2.default.createElement(
                      'option',
                      { value: '30' },
                      'in 30 days from now'
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'button',
                { type: 'button', onClick: this.props.job.draft._id ? this.editJob.bind(this) : this.createJob.bind(this), className: 'btn btn-default btn-mini' },
                this.props.job.draft._id ? 'Edit Job' : 'Create Job'
              ),
              ' ',
              _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + '/dashboard/job', className: 'btn btn-default btn-mini' },
                'Cancel'
              )
            )
          )
        )
      );
    }
  }]);

  return JobCreate;
}(_react.Component);

exports.default = JobCreate;