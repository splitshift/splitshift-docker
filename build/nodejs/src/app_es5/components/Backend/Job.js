'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ResourceActionCreators = require('../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _JobActionCreators = require('../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

var _AuthenticationActionCreators = require('../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Job = function (_Component) {
  _inherits(Job, _Component);

  function Job() {
    _classCallCheck(this, Job);

    return _possibleConstructorReturn(this, (Job.__proto__ || Object.getPrototypeOf(Job)).apply(this, arguments));
  }

  _createClass(Job, [{
    key: 'onDeleteJob',
    value: function onDeleteJob(job, evt) {
      if (confirm("Sure to remove ?")) _JobActionCreators2.default.deleteJob(this.props.user.token, job);
    }
  }, {
    key: 'onActivateJob',
    value: function onActivateJob(job_id, evt) {
      _JobActionCreators2.default.activateJob(this.props.user.token, job_id);
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      _JobActionCreators2.default.getAllJob(this.props.user.token);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.job.updating && this.props.job.updating) _JobActionCreators2.default.getAllJob(this.props.user.token);

      return JSON.stringify(this.props.job.jobs) !== JSON.stringify(nextProps.job.jobs);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'row' },
        _react2.default.createElement(
          'div',
          { className: 'col-md-12' },
          _react2.default.createElement(
            'h4',
            null,
            _react2.default.createElement(
              'span',
              { style: { float: "right" } },
              _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + '/dashboard/job/create', className: 'btn btn-default' },
                'Add New Job'
              )
            ),
            'You have ',
            this.props.job.jobs.filter(function (job) {
              return job.activated;
            }).length,
            ' Active Jobs'
          ),
          _react2.default.createElement(
            'table',
            { className: 'table' },
            _react2.default.createElement(
              'thead',
              { style: { backgroundColor: "white" } },
              _react2.default.createElement(
                'tr',
                null,
                _react2.default.createElement(
                  'th',
                  null,
                  'Job'
                ),
                _react2.default.createElement(
                  'th',
                  null,
                  'Expiry Date'
                ),
                _react2.default.createElement(
                  'th',
                  null,
                  'Status'
                ),
                _react2.default.createElement('th', null)
              )
            ),
            _react2.default.createElement(
              'tbody',
              null,
              typeof this.props.job.jobs !== 'undefined' ? this.props.job.jobs.map(function (job, index) {
                return _react2.default.createElement(
                  'tr',
                  { key: index },
                  _react2.default.createElement(
                    'th',
                    null,
                    _react2.default.createElement(
                      'span',
                      { style: { color: "#f60" } },
                      job.position
                    )
                  ),
                  _react2.default.createElement(
                    'th',
                    null,
                    _JQueryTool2.default.changeDatePickerFormat(job.expire_date)
                  ),
                  _react2.default.createElement(
                    'th',
                    null,
                    _react2.default.createElement(
                      'button',
                      { onClick: _this2.onActivateJob.bind(_this2, job._id), className: 'btn btn-default btn-mini' },
                      !job.activated ? 'Deactivated' : 'Activated'
                    )
                  ),
                  _react2.default.createElement(
                    'th',
                    null,
                    _react2.default.createElement(
                      'div',
                      { className: 'optionbtn__responsive' },
                      _react2.default.createElement(
                        _reactRouter.Link,
                        { to: '/' + _this2.props.language + '/dashboard/job/edit/' + job._id, className: 'btn btn-default btn-mini reverse' },
                        'Edit'
                      ),
                      ' ',
                      _react2.default.createElement(
                        'button',
                        { className: 'btn btn-default btn-mini reverse', onClick: _this2.onDeleteJob.bind(_this2, job, index) },
                        'Delete'
                      )
                    )
                  )
                );
              }) : ""
            )
          )
        )
      );
    }
  }]);

  return Job;
}(_react.Component);

exports.default = Job;