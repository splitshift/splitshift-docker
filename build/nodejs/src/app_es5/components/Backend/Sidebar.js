'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Sidebar = function (_Component) {
  _inherits(Sidebar, _Component);

  function Sidebar() {
    _classCallCheck(this, Sidebar);

    return _possibleConstructorReturn(this, (Sidebar.__proto__ || Object.getPrototypeOf(Sidebar)).apply(this, arguments));
  }

  _createClass(Sidebar, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _JQueryTool2.default.backendDrawer();
    }
  }, {
    key: 'render',
    value: function render() {
      var SidebarContent = _configLang2.default[this.props.language].backend.sidebar;
      var headerContent = _configLang2.default[this.props.language].headers;

      return _react2.default.createElement(
        'div',
        { id: 'Mysidenav', className: 'sidenav' },
        _react2.default.createElement(
          'div',
          { className: 'sidebar__list' },
          _react2.default.createElement(
            'ul',
            { className: 'navbar-default' },
            _react2.default.createElement(
              'div',
              { className: 'sidebar__mainnavlist' },
              _react2.default.createElement(
                'a',
                { href: '#' },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__titlemain', role: 'presentation' },
                  _react2.default.createElement('img', { src: this.props.user.profile_image }),
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      this.props.user.type === 'Employee' ? this.props.user.first_name + ' ' + this.props.user.last_name : this.props.user.company_name
                    )
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + (this.props.user.type === 'Employee' ? "/profile" : "/company") },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    this.props.user.type === 'Employee' ? SidebarContent[0] : SidebarContent[1]
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + (this.props.user.type === 'Employee' ? '/dashboard/user' : '/dashboard/company'), onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    SidebarContent[2]
                  )
                )
              ),
              this.props.user.type !== 'Employee' && _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + "/dashboard/job", onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    SidebarContent[3]
                  )
                )
              ),
              (this.props.user.type !== 'Employee' || this.props.user.rank === 'HR') && _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + "/dashboard/apply", onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    SidebarContent[4]
                  )
                )
              ),
              this.props.user.type === 'Employee' && _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + "/dashboard/application", onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    SidebarContent[5]
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + "/dashboard/resource/index", onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    SidebarContent[6]
                  )
                )
              ),
              this.props.user.type !== 'Employee' && _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + "/dashboard/hr", onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    SidebarContent[7]
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + '/setting' },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    SidebarContent[8]
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'sidebar' },
          _react2.default.createElement(
            'div',
            { className: 'sidebar__list' },
            _react2.default.createElement(
              'ul',
              { className: 'navbar-default' },
              _react2.default.createElement(
                'div',
                { className: 'sidebar__navlist' },
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: '/' + this.props.language + "/buzz" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      headerContent[0]
                    )
                  )
                ),
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: '/' + this.props.language + "/people-search" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      headerContent[1]
                    )
                  )
                ),
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: '/' + this.props.language + "/job-search" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      headerContent[2]
                    )
                  )
                ),
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: '/' + this.props.language + "/company-search" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      headerContent[3]
                    )
                  )
                ),
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: '/' + this.props.language + "/resource" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      headerContent[4]
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'sidebar__btnlist' },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__btn--search' },
                  _react2.default.createElement(
                    'form',
                    null,
                    _react2.default.createElement(
                      'div',
                      { className: 'input-group' },
                      _react2.default.createElement('input', { type: 'text', className: 'form-control', placeholder: 'search, jobs, people, companies ...' }),
                      _react2.default.createElement(
                        'span',
                        { className: 'input-group-btn' },
                        _react2.default.createElement(
                          _reactRouter.Link,
                          { to: '/th/', className: 'btn btn-default', type: 'button' },
                          _react2.default.createElement('i', { className: 'icon-search' })
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return Sidebar;
}(_react.Component);

exports.default = Sidebar;