'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ResourceActionCreators = require('../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _JobActionCreators = require('../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

var _AuthenticationActionCreators = require('../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _ConfigData = require('../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var applyJobStatusList = {};

var ApplyJob = function (_Component) {
  _inherits(ApplyJob, _Component);

  function ApplyJob() {
    _classCallCheck(this, ApplyJob);

    return _possibleConstructorReturn(this, (ApplyJob.__proto__ || Object.getPrototypeOf(ApplyJob)).apply(this, arguments));
  }

  _createClass(ApplyJob, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _JobActionCreators2.default.getAllApplyJob(this.props.user.token);
    }
  }, {
    key: 'changeAppleJobStatus',
    value: function changeAppleJobStatus(apply_job, apply_job_index, evt) {
      _JobActionCreators2.default.changeAppleJobStatus(this.props.user.token, apply_job._id, { status: this.refs['applyjob_status_' + apply_job_index].value });
    }
  }, {
    key: 'changeApplyJobFilter',
    value: function changeApplyJobFilter(filter, evt) {
      _JobActionCreators2.default.changeApplyJobFilter(filter);
    }
  }, {
    key: 'deleteApplyJob',
    value: function deleteApplyJob(apply_job, evt) {
      _JobActionCreators2.default.deleteApplyJob(this.props.user.token, apply_job._id);
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {
      _JobActionCreators2.default.searchApplyJob(evt.target.value);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.deleting && this.props.deleting) _JobActionCreators2.default.getAllApplyJob(this.props.user.token);

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var decline_status = _ConfigData2.default.ApplyJobStatus()[_ConfigData2.default.ApplyJobStatus().length - 1].toLowerCase();
      var apply_jobs = this.props.apply_jobs.filter(function (apply_job, index) {
        return _this2.props.apply_job_filter ? apply_job.status === _this2.props.apply_job_filter : apply_job.status !== decline_status;
      });

      return _react2.default.createElement(
        'div',
        { className: 'row' },
        _react2.default.createElement(
          'div',
          { className: 'col-md-12' },
          _react2.default.createElement(
            'h4',
            null,
            this.props.user.type === 'Employee' ? _react2.default.createElement(
              'div',
              null,
              'You are HR in ',
              this.props.user.occupation.workplace,
              ' Company ',
              _react2.default.createElement('br', null),
              ' ',
              _react2.default.createElement('br', null)
            ) : "",
            'You have ',
            apply_jobs.length,
            ' Job Applications'
          ),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.onSearch.bind(this), placeholder: 'search from name, email' })
          ),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'button',
              { onClick: this.changeApplyJobFilter.bind(this, ""), className: 'btn btn-default btn-mini' },
              'Current Applications'
            ),
            ' ',
            _react2.default.createElement(
              'button',
              { onClick: this.changeApplyJobFilter.bind(this, decline_status), className: 'btn btn-mini' },
              'Declined Applications'
            )
          ),
          _react2.default.createElement(
            'table',
            { className: 'table' },
            _react2.default.createElement(
              'thead',
              { style: { backgroundColor: "white" } },
              _react2.default.createElement(
                'tr',
                null,
                _react2.default.createElement(
                  'th',
                  { width: '25%' },
                  'Owner'
                ),
                _react2.default.createElement(
                  'th',
                  { width: '45%' },
                  'Application From '
                ),
                _react2.default.createElement(
                  'th',
                  { width: '30%' },
                  'Status'
                )
              )
            ),
            _react2.default.createElement(
              'tbody',
              null,
              apply_jobs.map(function (apply_job, index) {
                return _react2.default.createElement(
                  'tr',
                  { key: index },
                  _react2.default.createElement(
                    'td',
                    null,
                    _react2.default.createElement('img', { src: apply_job.from.profile_image ? apply_job.from.profile_image : "/images/avatar-image.jpg", style: { width: "100px" } }),
                    _react2.default.createElement(
                      'h5',
                      null,
                      apply_job.from.name
                    ),
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/' + _this2.props.language + '/user/' + apply_job.from.key },
                      'See Profile'
                    ),
                    ' ',
                    _react2.default.createElement('br', null),
                    _react2.default.createElement(
                      'a',
                      { href: '/' + _this2.props.language + '/user/' + apply_job.from.key + '/resume', target: '_blank' },
                      'See Resume'
                    )
                  ),
                  _react2.default.createElement(
                    'td',
                    null,
                    _react2.default.createElement(
                      'div',
                      null,
                      _react2.default.createElement(
                        'b',
                        { style: { color: "#f60" } },
                        'Cover Letter '
                      ),
                      ' ',
                      _react2.default.createElement('br', null),
                      apply_job.cover_letter
                    ),
                    _react2.default.createElement('br', null),
                    _react2.default.createElement(
                      'div',
                      null,
                      _react2.default.createElement(
                        'b',
                        { style: { color: "#f60" } },
                        ' Email '
                      ),
                      '  ',
                      _react2.default.createElement('br', null),
                      apply_job.email
                    ),
                    _react2.default.createElement('br', null),
                    _react2.default.createElement(
                      'div',
                      null,
                      _react2.default.createElement(
                        'b',
                        { style: { color: "#f60" } },
                        ' Tel '
                      ),
                      '  ',
                      _react2.default.createElement('br', null),
                      apply_job.tel
                    ),
                    _react2.default.createElement('br', null),
                    _react2.default.createElement(
                      'div',
                      null,
                      _react2.default.createElement(
                        'b',
                        { style: { color: "#f60" } },
                        ' Date Submitted '
                      ),
                      '  ',
                      _react2.default.createElement('br', null),
                      _JQueryTool2.default.changeBirthDayFormat(apply_job.date)
                    ),
                    _react2.default.createElement('br', null),
                    apply_job.files.length > 0 ? _react2.default.createElement(
                      'div',
                      null,
                      _react2.default.createElement(
                        'b',
                        { style: { color: "#f60" } },
                        ' File Attachment '
                      ),
                      '  ',
                      _react2.default.createElement('br', null),
                      apply_job.files.map(function (file, index) {
                        return _react2.default.createElement(
                          'div',
                          { key: index },
                          _react2.default.createElement(
                            'a',
                            { href: file.substring(6), target: '_blank' },
                            'File ',
                            index + 1
                          )
                        );
                      })
                    ) : ""
                  ),
                  _react2.default.createElement(
                    'td',
                    null,
                    apply_job.status !== decline_status ? _react2.default.createElement(
                      'div',
                      { className: 'optionbtn-apply__responsive' },
                      _react2.default.createElement(
                        'select',
                        { ref: "applyjob_status_" + index, className: 'form-control', defaultValue: apply_job.status },
                        _ConfigData2.default.ApplyJobStatus().map(function (status, index) {
                          return _react2.default.createElement(
                            'option',
                            { key: index, value: status.toLowerCase() },
                            status
                          );
                        })
                      ),
                      _react2.default.createElement(
                        'button',
                        { onClick: _this2.changeAppleJobStatus.bind(_this2, apply_job, index), className: 'btn btn-default btn-mini' },
                        'Change Status'
                      ),
                      ' ',
                      _react2.default.createElement(
                        'button',
                        { onClick: _this2.deleteApplyJob.bind(_this2, apply_job), className: 'btn btn-danger btn-mini' },
                        'Remove Application'
                      )
                    ) : _react2.default.createElement(
                      'div',
                      null,
                      'Declined ',
                      _react2.default.createElement('br', null),
                      _react2.default.createElement(
                        'button',
                        { onClick: _this2.deleteApplyJob.bind(_this2, apply_job), className: 'btn btn-danger btn-mini' },
                        'Remove Application'
                      )
                    )
                  )
                );
              })
            )
          )
        )
      );
    }
  }]);

  return ApplyJob;
}(_react.Component);

exports.default = ApplyJob;