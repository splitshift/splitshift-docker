'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanyPrivacy = { public: 'Public', timeline: 'My Timeline', employee: 'Employee' };
var UserPrivacy = { public: 'Public', timeline: 'My Timeline', connected: 'Connection', employer: 'Employer', onlyme: 'Private' };

var AdminDashboard = function (_Component) {
  _inherits(AdminDashboard, _Component);

  function AdminDashboard() {
    _classCallCheck(this, AdminDashboard);

    return _possibleConstructorReturn(this, (AdminDashboard.__proto__ || Object.getPrototypeOf(AdminDashboard)).apply(this, arguments));
  }

  _createClass(AdminDashboard, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      console.log(this.props.stat);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var cardStyle = {
        backgroundColor: "#ddd",
        margin: "10px",
        padding: "10px"
      };

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'h2',
          null,
          'Splitshift Statistics '
        ),
        _react2.default.createElement('hr', null),
        _react2.default.createElement(
          'h4',
          null,
          'Posts'
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Total'
            ),
            this.props.stat.feed.all || "0"
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Employee Total'
            ),
            this.props.stat.feed.company.all || "0"
          ),
          Object.keys(CompanyPrivacy).map(function (privacy, index) {
            return _react2.default.createElement(
              'div',
              { className: 'col-md-3', style: cardStyle, key: index },
              _react2.default.createElement(
                'h5',
                null,
                'Employee ',
                CompanyPrivacy[privacy]
              ),
              _this2.props.stat.feed.company[privacy] || "0"
            );
          })
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'User Total'
            ),
            this.props.stat.feed.user.all || "0"
          ),
          Object.keys(UserPrivacy).map(function (privacy, index) {
            return _react2.default.createElement(
              'div',
              { className: 'col-md-3', style: cardStyle, key: index },
              _react2.default.createElement(
                'h5',
                null,
                'User ',
                UserPrivacy[privacy]
              ),
              _this2.props.stat.feed.user[privacy] || "0"
            );
          })
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Admin Total'
            ),
            this.props.stat.feed.admin || "0"
          )
        ),
        _react2.default.createElement('hr', null),
        _react2.default.createElement(
          'h4',
          null,
          'Resource'
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Total Approved'
            ),
            this.props.stat.resource.approved.all
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'User Approved'
            ),
            this.props.stat.resource.approved.user || "0"
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Employer Approved'
            ),
            this.props.stat.resource.approved.company || "0"
          )
        ),
        _react2.default.createElement('hr', null),
        _react2.default.createElement(
          'h4',
          null,
          'Jobs'
        ),
        _react2.default.createElement(
          'div',
          { className: 'row' },
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Total'
            ),
            this.props.stat.job.all
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Active Jobs'
            ),
            this.props.stat.job.activated
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Deactivated Jobs'
            ),
            this.props.stat.job.deactivated
          ),
          _react2.default.createElement(
            'div',
            { className: 'col-md-3', style: cardStyle },
            _react2.default.createElement(
              'h5',
              null,
              'Applications'
            ),
            typeof this.props.stat.applyjob === 'number' ? this.props.stat.applyjob : '0'
          )
        ),
        _react2.default.createElement('hr', null),
        _react2.default.createElement(
          'div',
          { style: { fontSize: "20" } },
          _react2.default.createElement(
            'p',
            null,
            'Number of Users ',
            _react2.default.createElement(
              'b',
              null,
              this.props.stat.people
            )
          ),
          _react2.default.createElement(
            'p',
            null,
            'Number of Employers ',
            _react2.default.createElement(
              'b',
              null,
              this.props.stat.companies
            )
          ),
          _react2.default.createElement(
            'p',
            null,
            'Number of Messages ',
            _react2.default.createElement(
              'b',
              null,
              this.props.stat.message
            )
          ),
          _react2.default.createElement(
            'p',
            null,
            'Number of Likes ',
            _react2.default.createElement(
              'b',
              null,
              this.props.stat.like
            )
          ),
          _react2.default.createElement(
            'p',
            null,
            'Number of Comments ',
            _react2.default.createElement(
              'b',
              null,
              this.props.stat.comment
            )
          )
        )
      );
    }
  }]);

  return AdminDashboard;
}(_react.Component);

exports.default = AdminDashboard;