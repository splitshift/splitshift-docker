'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _HRManagementActionCreators = require('../../actions/HRManagementActionCreators');

var _HRManagementActionCreators2 = _interopRequireDefault(_HRManagementActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var new_load = false;

var HRManagement = function (_Component) {
  _inherits(HRManagement, _Component);

  function HRManagement() {
    _classCallCheck(this, HRManagement);

    return _possibleConstructorReturn(this, (HRManagement.__proto__ || Object.getPrototypeOf(HRManagement)).apply(this, arguments));
  }

  _createClass(HRManagement, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _HRManagementActionCreators2.default.getEmployee(this.props.user.key);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {

      console.log(this.props.hr.users);

      if (new_load) {
        _HRManagementActionCreators2.default.getEmployee(this.props.user.key);
        new_load = false;
      }
    }
  }, {
    key: 'changeToHR',
    value: function changeToHR(user, key) {

      var result = {
        user_key: user.key,
        rank: user.rank === "Employee" ? "HR" : "Employee"
      };

      _HRManagementActionCreators2.default.changeRank(this.props.user.token, result);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (nextProps.hr.editing && !this.props.hr.editing) new_load = true;

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'h2',
          null,
          'HR Management'
        ),
        _react2.default.createElement(
          'p',
          null,
          'The people show in this page is user work in your company.'
        ),
        _react2.default.createElement(
          'table',
          { className: 'table' },
          _react2.default.createElement(
            'thead',
            { style: { backgroundColor: "white" } },
            _react2.default.createElement(
              'tr',
              null,
              _react2.default.createElement(
                'th',
                null,
                'Name'
              ),
              _react2.default.createElement(
                'th',
                null,
                'Status'
              ),
              _react2.default.createElement('th', null)
            )
          ),
          _react2.default.createElement(
            'tbody',
            null,
            this.props.hr.users.map(function (user, index) {
              return _react2.default.createElement(
                'tr',
                { key: index },
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/th/user/' + user.key },
                    _react2.default.createElement('img', { src: user.profile_image, width: '100px' }),
                    _react2.default.createElement(
                      'p',
                      null,
                      user.name
                    )
                  )
                ),
                _react2.default.createElement(
                  'td',
                  null,
                  user.rank
                ),
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(
                    'button',
                    { onClick: _this2.changeToHR.bind(_this2, user), className: 'btn btn-default btn-mini' },
                    user.rank !== 'HR' ? "Change to HR" : "Remove from HR"
                  ),
                  ' ',
                  _react2.default.createElement('br', null)
                )
              );
            })
          )
        )
      );
    }
  }]);

  return HRManagement;
}(_react.Component);

exports.default = HRManagement;