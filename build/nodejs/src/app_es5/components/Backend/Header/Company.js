'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CompanyHeader = function (_Component) {
  _inherits(CompanyHeader, _Component);

  function CompanyHeader() {
    _classCallCheck(this, CompanyHeader);

    return _possibleConstructorReturn(this, (CompanyHeader.__proto__ || Object.getPrototypeOf(CompanyHeader)).apply(this, arguments));
  }

  _createClass(CompanyHeader, [{
    key: 'changeLanguage',
    value: function changeLanguage(evt) {
      _ResourceActionCreators2.default.setLanguage(evt.target.value);
    }
  }, {
    key: 'render',
    value: function render() {
      var headerContent = _configLang2.default[this.props.language].headers;

      return _react2.default.createElement(
        'header',
        { id: 'admin__header' },
        _react2.default.createElement(
          'div',
          { className: 'topbar' },
          _react2.default.createElement(
            'div',
            { className: 'container-fluid' },
            _react2.default.createElement(
              'nav',
              { className: 'navbar navbar-default' },
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'sidebar__toggle' },
                _react2.default.createElement(
                  'span1',
                  null,
                  _react2.default.createElement('i', { className: 'fa fa-bars' })
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'navbar-header' },
                _react2.default.createElement(
                  'div',
                  { className: 'navbar__logo' },
                  _react2.default.createElement(
                    _reactRouter.Link,
                    { to: '/' + this.props.language, className: 'navbar-brand' },
                    _react2.default.createElement('img', { className: 'logo__large', src: '/images/logo-splitshift.png', alt: 'logo' })
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'topbar__btn--language' },
                _react2.default.createElement(
                  'form',
                  { role: 'form' },
                  _react2.default.createElement(
                    'div',
                    { className: 'form-group' },
                    _react2.default.createElement(
                      'select',
                      { onChange: this.changeLanguage.bind(this), className: 'form-control', id: 'lang', value: this.props.language },
                      _react2.default.createElement(
                        'option',
                        { value: 'th' },
                        'THA'
                      ),
                      _react2.default.createElement(
                        'option',
                        { value: 'en' },
                        'ENG'
                      )
                    )
                  )
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'appbar', id: 'myNavbar' },
                _react2.default.createElement(
                  'div',
                  { className: 'nav__btnlist' },
                  _react2.default.createElement(
                    'ul',
                    { className: 'nav navbar-nav navbar-right' },
                    _react2.default.createElement(
                      'li',
                      { className: 'nav__btn--search' },
                      _react2.default.createElement(
                        'div',
                        { className: 'dropdown dropdown-search' },
                        _react2.default.createElement(
                          'a',
                          { href: '#', className: 'dropdown-toggle', 'data-toggle': 'dropdown' },
                          _react2.default.createElement('i', { className: 'icon-search' })
                        ),
                        _react2.default.createElement(
                          'div',
                          { className: 'dropdown-menu' },
                          _react2.default.createElement(
                            'form',
                            { action: 'search.html' },
                            _react2.default.createElement(
                              'div',
                              { className: 'input-group' },
                              _react2.default.createElement('input', { type: 'text', className: 'form-control', placeholder: 'search, jobs, people, companies ...' }),
                              _react2.default.createElement(
                                'span',
                                { className: 'input-group-btn' },
                                _react2.default.createElement(
                                  'a',
                                  { href: 'search.html', className: 'btn btn-default', type: 'submit' },
                                  _react2.default.createElement('i', { className: 'icon-search' })
                                )
                              )
                            )
                          )
                        )
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'nav__navlist' },
                  _react2.default.createElement(
                    'ul',
                    { className: 'nav navbar-nav navbar-right' },
                    _react2.default.createElement(
                      'li',
                      { className: 'navlist__item', role: 'presentation' },
                      _react2.default.createElement(
                        _reactRouter.Link,
                        { to: '/' + this.props.language + "/buzz" },
                        _react2.default.createElement(
                          'span1',
                          null,
                          headerContent[0]
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'li',
                      { className: 'navlist__item', role: 'presentation' },
                      _react2.default.createElement(
                        _reactRouter.Link,
                        { to: '/' + this.props.language + "/people-search" },
                        _react2.default.createElement(
                          'span1',
                          null,
                          headerContent[1]
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'li',
                      { className: 'navlist__item', role: 'presentation' },
                      _react2.default.createElement(
                        _reactRouter.Link,
                        { to: '/' + this.props.language + "/job-search" },
                        _react2.default.createElement(
                          'span1',
                          null,
                          headerContent[2]
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'li',
                      { className: 'navlist__item', role: 'presentation' },
                      _react2.default.createElement(
                        _reactRouter.Link,
                        { to: '/' + this.props.language + "/company-search" },
                        _react2.default.createElement(
                          'span1',
                          null,
                          headerContent[3]
                        )
                      )
                    ),
                    _react2.default.createElement(
                      'li',
                      { className: 'navlist__item', role: 'presentation' },
                      _react2.default.createElement(
                        _reactRouter.Link,
                        { to: '/' + this.props.language + "/resource" },
                        _react2.default.createElement(
                          'span1',
                          null,
                          headerContent[4]
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return CompanyHeader;
}(_react.Component);

exports.default = CompanyHeader;