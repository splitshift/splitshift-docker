'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _AuthenticationActionCreators = require('../../actions/AuthenticationActionCreators');

var _AuthenticationActionCreators2 = _interopRequireDefault(_AuthenticationActionCreators);

var _JQueryTool = require('../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AdminSidebar = function (_Component) {
  _inherits(AdminSidebar, _Component);

  function AdminSidebar() {
    _classCallCheck(this, AdminSidebar);

    return _possibleConstructorReturn(this, (AdminSidebar.__proto__ || Object.getPrototypeOf(AdminSidebar)).apply(this, arguments));
  }

  _createClass(AdminSidebar, [{
    key: 'onLogout',
    value: function onLogout() {
      _AuthenticationActionCreators2.default.logout();
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      _JQueryTool2.default.backendDrawer();
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { id: 'Mysidenav', className: 'sidenav' },
        _react2.default.createElement(
          'div',
          { className: 'sidebar__list' },
          _react2.default.createElement(
            'ul',
            { className: 'navbar-default' },
            _react2.default.createElement(
              'div',
              { className: 'sidebar__mainnavlist' },
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__titlemain', role: 'presentation' },
                  _react2.default.createElement('img', { src: this.props.user.profile_image }),
                  _react2.default.createElement(
                    'span1',
                    null,
                    _react2.default.createElement(
                      'b',
                      null,
                      'Admin'
                    )
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language + "/admin", onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    'Main Menu'
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language + "/admin/resource/index", onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    'Resource'
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language + "/admin/newsletter/index", onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    'Newsletter'
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: "/" + this.props.language + "/admin/user/index", onClick: _JQueryTool2.default.sidebarToggle.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    'User Management'
                  )
                )
              ),
              _react2.default.createElement(
                _reactRouter.Link,
                { to: '#', onClick: this.onLogout.bind(this) },
                _react2.default.createElement(
                  'li',
                  { className: 'sidebar__mainnavitem', role: 'presentation' },
                  _react2.default.createElement(
                    'div',
                    { className: 'icon__mainnavitem' },
                    _react2.default.createElement('i', { className: 'fa fa-user', 'aria-hidden': 'true' })
                  ),
                  _react2.default.createElement(
                    'span1',
                    null,
                    'Logout'
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'sidebar' },
          _react2.default.createElement(
            'div',
            { className: 'sidebar__list' },
            _react2.default.createElement(
              'ul',
              { className: 'navbar-default' },
              _react2.default.createElement(
                'div',
                { className: 'sidebar__navlist' },
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: "/" + this.props.language + "/buzz" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      'BUZZ'
                    )
                  )
                ),
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: "/" + this.props.language + "/people-search" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      'PEOPLE'
                    )
                  )
                ),
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: "/" + this.props.language + "/job-search" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      'JOBS'
                    )
                  )
                ),
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: "/" + this.props.language + "/company-search" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      'COMPANIES'
                    )
                  )
                ),
                _react2.default.createElement(
                  _reactRouter.Link,
                  { to: "/" + this.props.language + "/resource" },
                  _react2.default.createElement(
                    'li',
                    { className: 'sidebar__navitem', role: 'presentation' },
                    _react2.default.createElement(
                      'span1',
                      null,
                      'RESOURCES'
                    )
                  )
                )
              )
            )
          )
        )
      );
    }
  }]);

  return AdminSidebar;
}(_react.Component);

exports.default = AdminSidebar;