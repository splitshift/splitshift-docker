'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _NewsletterActionCreators = require('../../../actions/NewsletterActionCreators');

var _NewsletterActionCreators2 = _interopRequireDefault(_NewsletterActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NewsletterCreate = function (_Component) {
  _inherits(NewsletterCreate, _Component);

  function NewsletterCreate() {
    _classCallCheck(this, NewsletterCreate);

    return _possibleConstructorReturn(this, (NewsletterCreate.__proto__ || Object.getPrototypeOf(NewsletterCreate)).apply(this, arguments));
  }

  _createClass(NewsletterCreate, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      CKEDITOR.replace('editor2');
    }
  }, {
    key: 'sendSubscriberEmail',
    value: function sendSubscriberEmail(evt) {
      evt.preventDefault();
      var title = this.refs.title.value;
      var content = CKEDITOR.instances.editor2.getData();

      var info = {
        subject: this.refs.title.value,
        html: CKEDITOR.instances.editor2.getData()
      };

      _NewsletterActionCreators2.default.sendEmailSubscibers(this.props.user.token, info);
    }
  }, {
    key: 'sendExampleEmail',
    value: function sendExampleEmail(evt) {

      var info = {
        email: this.refs.example_email.value,
        subject: this.refs.title.value,
        html: CKEDITOR.instances.editor2.getData()
      };

      _NewsletterActionCreators2.default.sendEmailSingle(this.props.user.token, info);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'form',
          { onSubmit: this.sendSubscriberEmail.bind(this) },
          _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
              'div',
              { className: 'col-md-12' },
              _react2.default.createElement(
                'h2',
                null,
                'Newsletter Create'
              ),
              _react2.default.createElement('hr', null)
            ),
            _react2.default.createElement(
              'div',
              { className: 'col-md-9 col-md-offset-1' },
              'Title',
              _react2.default.createElement('input', { ref: 'title', type: 'text', className: 'form-control form-control--mini', placeholder: 'Title' }),
              'Content',
              _react2.default.createElement('textarea', { name: 'editor2', ref: 'content', className: 'form-control form-control--mini' }),
              _react2.default.createElement(
                'div',
                { style: { textAlign: "right", padding: "10px" } },
                'Send to Test Email',
                _react2.default.createElement('input', { ref: 'example_email', type: 'text' }),
                _react2.default.createElement(
                  'button',
                  { type: 'button', onClick: this.sendExampleEmail.bind(this), className: 'btn btn-default btn-mini' },
                  'Send'
                )
              ),
              _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                  'button',
                  { className: 'btn btn-default' },
                  'Send Email to All Subscribers'
                )
              )
            )
          )
        )
      );
    }
  }]);

  return NewsletterCreate;
}(_react.Component);

exports.default = NewsletterCreate;