'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _NewsletterActionCreators = require('../../../actions/NewsletterActionCreators');

var _NewsletterActionCreators2 = _interopRequireDefault(_NewsletterActionCreators);

var _JQueryTool = require('../../../tools/JQueryTool');

var _JQueryTool2 = _interopRequireDefault(_JQueryTool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NewsletterIndex = function (_Component) {
  _inherits(NewsletterIndex, _Component);

  function NewsletterIndex() {
    _classCallCheck(this, NewsletterIndex);

    return _possibleConstructorReturn(this, (NewsletterIndex.__proto__ || Object.getPrototypeOf(NewsletterIndex)).apply(this, arguments));
  }

  _createClass(NewsletterIndex, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _NewsletterActionCreators2.default.getNewsletters(this.props.user.token);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'h4',
          null,
          'Newsletter',
          _react2.default.createElement(
            'span',
            { style: { float: "right" } },
            _react2.default.createElement(
              _reactRouter.Link,
              { to: '/' + this.props.language + '/admin/newsletter/create', className: 'btn btn-default' },
              'Send Newsletter'
            )
          )
        ),
        _react2.default.createElement(
          'table',
          { className: 'table' },
          _react2.default.createElement(
            'thead',
            { style: { backgroundColor: "#f60", color: "white" } },
            _react2.default.createElement(
              'tr',
              null,
              _react2.default.createElement(
                'th',
                { width: '60%' },
                'Email'
              ),
              _react2.default.createElement(
                'th',
                { width: '20%' },
                'Date'
              ),
              _react2.default.createElement('th', null)
            )
          ),
          _react2.default.createElement(
            'tbody',
            null,
            this.props.newsletter.list.map(function (newsletter, index) {
              return _react2.default.createElement(
                'tr',
                { key: index },
                _react2.default.createElement(
                  'td',
                  null,
                  newsletter.email
                ),
                _react2.default.createElement(
                  'td',
                  null,
                  _JQueryTool2.default.changeBirthDayFormat(newsletter.date)
                ),
                _react2.default.createElement('td', null)
              );
            })
          )
        )
      );
    }
  }]);

  return NewsletterIndex;
}(_react.Component);

exports.default = NewsletterIndex;