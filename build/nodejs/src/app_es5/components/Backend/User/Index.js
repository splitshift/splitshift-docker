'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _AdminManagerActionCreators = require('../../../actions/AdminManagerActionCreators');

var _AdminManagerActionCreators2 = _interopRequireDefault(_AdminManagerActionCreators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AdminUserIndex = function (_Component) {
  _inherits(AdminUserIndex, _Component);

  function AdminUserIndex() {
    _classCallCheck(this, AdminUserIndex);

    return _possibleConstructorReturn(this, (AdminUserIndex.__proto__ || Object.getPrototypeOf(AdminUserIndex)).apply(this, arguments));
  }

  _createClass(AdminUserIndex, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _AdminManagerActionCreators2.default.getUsers(this.props.user.token);
    }
  }, {
    key: 'toDeleteUser',
    value: function toDeleteUser(user, evt) {

      if (confirm('Are you sure to remove user ?')) _AdminManagerActionCreators2.default.deleteUser(this.props.user.token, user);
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {
      _AdminManagerActionCreators2.default.searchUser(evt.target.value);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.manager.loading && this.props.manager.loading) _AdminManagerActionCreators2.default.getUsers(this.props.user.token);

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'row' },
        _react2.default.createElement(
          'div',
          { className: 'col-md-12' },
          _react2.default.createElement(
            'h4',
            null,
            'User Management'
          ),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement('input', { onChange: this.onSearch.bind(this), type: 'text', className: 'form-control', placeholder: 'Search Email, Name' })
          ),
          _react2.default.createElement(
            'table',
            { className: 'table' },
            _react2.default.createElement(
              'thead',
              { style: { backgroundColor: "#f60", color: "white" } },
              _react2.default.createElement(
                'tr',
                null,
                _react2.default.createElement(
                  'th',
                  { width: '30%' },
                  'Email'
                ),
                _react2.default.createElement(
                  'th',
                  { width: '15%' },
                  'Type'
                ),
                _react2.default.createElement(
                  'th',
                  { width: '30%' },
                  'Name'
                ),
                _react2.default.createElement(
                  'th',
                  { width: '15%' },
                  'Status'
                ),
                _react2.default.createElement('th', null)
              )
            ),
            _react2.default.createElement(
              'tbody',
              null,
              this.props.manager.users.map(function (user, index) {
                return _react2.default.createElement(
                  'tr',
                  { key: index, style: { backgroundColor: user.type === 'Employee' ? 'white' : '' } },
                  _react2.default.createElement(
                    'td',
                    null,
                    user.email,
                    ' ',
                    _react2.default.createElement('br', null),
                    user.confirm ? _react2.default.createElement(
                      _reactRouter.Link,
                      { to: user.type === 'Employee' ? '/th/user/' + user.key : '/th/company/' + user.key },
                      'Go To Profile'
                    ) : ""
                  ),
                  _react2.default.createElement(
                    'td',
                    null,
                    user.type
                  ),
                  _react2.default.createElement(
                    'td',
                    null,
                    user.name
                  ),
                  _react2.default.createElement(
                    'td',
                    null,
                    user.confirm ? "Confirmed" : "Not confirm"
                  ),
                  _react2.default.createElement(
                    'td',
                    null,
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default btn-mini reverse', onClick: _this2.toDeleteUser.bind(_this2, user) },
                      'Delete'
                    )
                  )
                );
              })
            )
          )
        )
      );
    }
  }]);

  return AdminUserIndex;
}(_react.Component);

exports.default = AdminUserIndex;