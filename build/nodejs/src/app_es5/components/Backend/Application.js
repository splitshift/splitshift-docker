'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _JobActionCreators = require('../../actions/JobActionCreators');

var _JobActionCreators2 = _interopRequireDefault(_JobActionCreators);

var _configLang = require('../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Application = function (_Component) {
  _inherits(Application, _Component);

  function Application() {
    _classCallCheck(this, Application);

    return _possibleConstructorReturn(this, (Application.__proto__ || Object.getPrototypeOf(Application)).apply(this, arguments));
  }

  _createClass(Application, [{
    key: 'deleteApplication',
    value: function deleteApplication(apply_job, evt) {
      _JobActionCreators2.default.deleteApplication(this.props.user.token, apply_job._id);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.deleting && this.props.deleting) _JobActionCreators2.default.getEmployeeApplyJob(this.props.user.token);

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var AppContent = _configLang2.default[this.props.language].backend.user.application;
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'h4',
          null,
          AppContent[0]
        ),
        _react2.default.createElement(
          'table',
          { className: 'table' },
          _react2.default.createElement(
            'thead',
            { style: { backgroundColor: "white" } },
            _react2.default.createElement(
              'tr',
              null,
              _react2.default.createElement(
                'th',
                null,
                AppContent[1]
              ),
              _react2.default.createElement(
                'th',
                null,
                AppContent[2]
              ),
              _react2.default.createElement(
                'th',
                null,
                AppContent[3]
              ),
              _react2.default.createElement('th', null)
            )
          ),
          _react2.default.createElement(
            'tbody',
            null,
            this.props.job.employee_apply_jobs.map(function (apply_job, index) {
              return _react2.default.createElement(
                'tr',
                { key: index },
                _react2.default.createElement(
                  'td',
                  null,
                  apply_job.to.name,
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'button',
                    { onClick: _this2.deleteApplication.bind(_this2, apply_job), className: 'btn btn-danger btn-mini' },
                    'Remove Application'
                  )
                ),
                _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(
                    'b',
                    null,
                    'Email:'
                  ),
                  ' ',
                  apply_job.email,
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Tel:'
                  ),
                  ' ',
                  apply_job.tel,
                  ' ',
                  _react2.default.createElement('br', null),
                  _react2.default.createElement(
                    'b',
                    null,
                    'Cover Letter:'
                  ),
                  ' ',
                  _react2.default.createElement('br', null),
                  apply_job.cover_letter,
                  ' ',
                  _react2.default.createElement('br', null)
                ),
                _react2.default.createElement(
                  'td',
                  { style: { verticalAlign: "middle" } },
                  _react2.default.createElement(
                    'b',
                    null,
                    apply_job.status
                  )
                )
              );
            })
          )
        )
      );
    }
  }]);

  return Application;
}(_react.Component);

exports.default = Application;