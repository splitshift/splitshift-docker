'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _ConfigData = require('../../../tools/ConfigData');

var _ConfigData2 = _interopRequireDefault(_ConfigData);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResourceCreate = function (_Component) {
  _inherits(ResourceCreate, _Component);

  function ResourceCreate() {
    _classCallCheck(this, ResourceCreate);

    return _possibleConstructorReturn(this, (ResourceCreate.__proto__ || Object.getPrototypeOf(ResourceCreate)).apply(this, arguments));
  }

  _createClass(ResourceCreate, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      CKEDITOR.replace('editor1');
    }
  }, {
    key: 'createResource',
    value: function createResource() {
      var result = Object.assign({}, this.props.resource.resource, this.props.resource.draft);

      result.content = CKEDITOR.instances.editor1.getData();

      if (typeof result.department === 'undefined') result.department = this.refs.department.value;

      if (typeof result.category === 'undefined') result.category = this.refs.category.value;

      if (this.props.resource.resource._id) _ResourceActionCreators2.default.editResource(this.props.user.token, result);else _ResourceActionCreators2.default.createResource(this.props.user.token, result);
    }
  }, {
    key: 'onCancelEditResource',
    value: function onCancelEditResource() {
      _ResourceActionCreators2.default.setAdminEditor('edit_resource', false);
    }
  }, {
    key: 'editDraft',
    value: function editDraft(name, evt) {
      var response = {};
      response[name] = evt.target.value;

      if (name === 'featured_video') {
        var youtube_embed = _ConfigData2.default.ConvertYoutubeToEmbed(evt.target.value);
        if (youtube_embed != 'error') response[name] = youtube_embed;
      }

      _ResourceActionCreators2.default.editResourceDraft(response);
    }
  }, {
    key: 'uploadFeaturedImage',
    value: function uploadFeaturedImage(evt) {
      _ResourceActionCreators2.default.updateFeaturedImage(this.props.user.token, evt.target.files[0]);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      if (!nextProps.resource.editing && this.props.resource.editing) {
        alert("Add Resource Complete!");
        _ResourceActionCreators2.default.setAdminEditor('edit_resource', false);

        if (this.props.user.type === 'Admin') _reactRouter.browserHistory.push('/' + this.props.language + '/admin/resource/index');else _reactRouter.browserHistory.push('/' + this.props.language + '/dashboard/resource/index');
      }
      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var resourceContent = _configLang2.default[this.props.language].backend.resource;
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'form',
          null,
          _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
              'div',
              { className: 'col-md-12' },
              _react2.default.createElement(
                'h2',
                null,
                resourceContent.title
              ),
              _react2.default.createElement('hr', null)
            ),
            _react2.default.createElement(
              'div',
              { className: 'col-md-9 col-md-offset-1' },
              resourceContent.form[0],
              _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.editDraft.bind(this, 'title'), placeholder: resourceContent.form[0], defaultValue: this.props.resource.resource.title }),
              resourceContent.form[1],
              _react2.default.createElement(
                'select',
                { ref: 'category', className: 'form-control form-control--mini', onChange: this.editDraft.bind(this, 'category'), defaultValue: this.props.resource.resource.category },
                _ConfigData2.default.ResourceCategory().map(function (category, index) {
                  return _react2.default.createElement(
                    'option',
                    { key: index, value: category.toLowerCase() },
                    resourceContent.category[index]
                  );
                })
              ),
              resourceContent.form[2],
              _react2.default.createElement(
                'select',
                { ref: 'department', className: 'form-control form-control--mini', onChange: this.editDraft.bind(this, 'department'), defaultValue: this.props.resource.resource.department },
                _ConfigData2.default.ResourceDepartment().map(function (department, index) {
                  return _react2.default.createElement(
                    'option',
                    { key: index, value: department.toLowerCase() },
                    resourceContent.department[index]
                  );
                })
              ),
              resourceContent.form[3],
              _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement('input', { type: 'radio', name: 'type', value: 'image', id: 'featured_image', onChange: this.editDraft.bind(this, 'feature_type'), defaultChecked: typeof this.props.resource.resource.featured_image !== 'undefined' && this.props.resource.resource.featured_image !== null ? true : false }),
                ' ',
                _react2.default.createElement(
                  'label',
                  { htmlFor: 'featured_image' },
                  _react2.default.createElement(
                    'span',
                    { style: { fontSize: "13px" } },
                    _react2.default.createElement(
                      'strong',
                      null,
                      resourceContent.form[4]
                    )
                  )
                ),
                ' ',
                _react2.default.createElement('input', { type: 'radio', name: 'type', value: 'video', id: 'featured_video', onChange: this.editDraft.bind(this, 'feature_type'), defaultChecked: typeof this.props.resource.resource.featured_video !== 'undefined' && this.props.resource.resource.featured_video !== null ? true : false }),
                ' ',
                _react2.default.createElement(
                  'label',
                  { htmlFor: 'featured_video' },
                  _react2.default.createElement(
                    'span',
                    { style: { fontSize: "13px" } },
                    _react2.default.createElement(
                      'strong',
                      null,
                      resourceContent.form[5]
                    )
                  )
                )
              ),
              typeof this.props.resource.draft.feature_type !== 'undefined' || typeof this.props.resource.resource._id !== 'undefined' ? _react2.default.createElement(
                'div',
                null,
                this.props.resource.draft.feature_type === 'video' || typeof this.props.resource.draft.feature_type === 'undefined' && typeof this.props.resource.resource.featured_video !== 'undefined' && this.props.resource.resource.featured_video !== null ? _react2.default.createElement(
                  'span',
                  null,
                  'Video Preview',
                  typeof this.props.resource.draft.featured_video !== 'undefined' || typeof this.props.resource.resource.featured_video !== 'undefined' ? _react2.default.createElement('iframe', { className: 'feed__clip', height: '315', src: this.props.resource.draft.featured_video ? this.props.resource.draft.featured_video : this.props.resource.resource.featured_video, frameBorder: '0', allowFullScreen: '' }) : "",
                  _react2.default.createElement('input', { type: 'text', className: 'form-control form-control--mini', onChange: this.editDraft.bind(this, 'featured_video'), placeholder: 'Video URL', defaultValue: this.props.resource.resource.featured_video })
                ) : _react2.default.createElement(
                  'span',
                  null,
                  typeof this.props.resource.draft.featured_image !== 'undefined' || typeof this.props.resource.resource.featured_image !== 'undefined' ? _react2.default.createElement('img', { src: this.props.resource.draft.featured_image ? this.props.resource.draft.featured_image.substring(6) : this.props.resource.resource.featured_image ? this.props.resource.resource.featured_image.substring(6) : "", style: { width: "100%" } }) : "",
                  'Upload Image',
                  _react2.default.createElement('input', { type: 'file', className: 'form-control', onChange: this.uploadFeaturedImage.bind(this) })
                )
              ) : "",
              resourceContent.form[6],
              _react2.default.createElement('textarea', { className: 'form-control form-control--mini ', onChange: this.editDraft.bind(this, 'short_description'), defaultValue: this.props.resource.resource.short_description }),
              resourceContent.form[7],
              _react2.default.createElement('textarea', { name: 'editor1', ref: 'content', className: 'form-control form-control--mini', defaultValue: this.props.resource.resource.content }),
              _react2.default.createElement(
                'button',
                { type: 'button', className: 'btn btn-default', onClick: this.createResource.bind(this) },
                this.props.resource.resource._id ? resourceContent.action[1] : resourceContent.action[0]
              ),
              ' ',
              _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + (this.props.user.type === 'Admin' ? '/admin/resource/index' : '/dashboard/resource/index'), className: 'btn btn-default', onClick: this.onCancelEditResource.bind(this) },
                resourceContent.action[2]
              )
            )
          )
        )
      );
    }
  }]);

  return ResourceCreate;
}(_react.Component);

exports.default = ResourceCreate;