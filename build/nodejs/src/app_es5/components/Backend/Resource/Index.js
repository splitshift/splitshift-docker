'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _ResourceActionCreators = require('../../../actions/ResourceActionCreators');

var _ResourceActionCreators2 = _interopRequireDefault(_ResourceActionCreators);

var _configLang = require('../../../tools/configLang');

var _configLang2 = _interopRequireDefault(_configLang);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResourceIndex = function (_Component) {
  _inherits(ResourceIndex, _Component);

  function ResourceIndex() {
    _classCallCheck(this, ResourceIndex);

    return _possibleConstructorReturn(this, (ResourceIndex.__proto__ || Object.getPrototypeOf(ResourceIndex)).apply(this, arguments));
  }

  _createClass(ResourceIndex, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.user.type !== 'Admin') _ResourceActionCreators2.default.getResources(this.props.user.token);else _ResourceActionCreators2.default.getAdminResource(this.props.user.token);
    }
  }, {
    key: 'toDeleteResource',
    value: function toDeleteResource(resource, evt) {
      _ResourceActionCreators2.default.deleteResource(this.props.user.token, resource);
    }
  }, {
    key: 'approveResource',
    value: function approveResource(resource, evt) {
      _ResourceActionCreators2.default.approveResource(this.props.user.token, resource);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {

      if (!nextProps.resource.editing && this.props.resource.editing) {
        if (this.props.user.type !== 'Admin') _ResourceActionCreators2.default.getResources(this.props.user.token);else _ResourceActionCreators2.default.getAdminResource(this.props.user.token);
      }

      return true;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var resource_list = void 0;
      var resourceContent = _configLang2.default[this.props.language].backend.resource;

      if (this.props.user.type !== 'Admin') resource_list = this.props.resource.resource_array.filter(function (resource, index) {
        return resource.owner.key == _this2.props.user.key;
      });else resource_list = this.props.resource.resource_array;

      return _react2.default.createElement(
        'div',
        { className: 'row' },
        _react2.default.createElement(
          'div',
          { className: 'col-md-12' },
          _react2.default.createElement(
            'h4',
            null,
            resourceContent.header[0],
            _react2.default.createElement(
              'span',
              { style: { float: "right" } },
              _react2.default.createElement(
                _reactRouter.Link,
                { to: '/' + this.props.language + (this.props.user.type === 'Admin' ? '/admin/resource/create' : '/dashboard/resource/create'), className: 'btn btn-default' },
                resourceContent.add
              )
            )
          ),
          _react2.default.createElement(
            'table',
            { className: 'table' },
            _react2.default.createElement(
              'thead',
              { style: { backgroundColor: "white" } },
              _react2.default.createElement(
                'tr',
                null,
                _react2.default.createElement(
                  'th',
                  null,
                  resourceContent.header[1]
                ),
                _react2.default.createElement(
                  'th',
                  null,
                  resourceContent.header[2]
                ),
                _react2.default.createElement(
                  'th',
                  null,
                  resourceContent.header[3]
                ),
                _react2.default.createElement('th', null)
              )
            ),
            _react2.default.createElement(
              'tbody',
              null,
              resource_list.map(function (resource, index) {
                return _react2.default.createElement(
                  'tr',
                  { key: index },
                  _react2.default.createElement(
                    'td',
                    null,
                    resource.is_approved ? _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/th/resource/article/' + resource._id },
                      resource.title
                    ) : _react2.default.createElement(
                      'div',
                      null,
                      resource.title,
                      ' ',
                      _react2.default.createElement('br', null),
                      _this2.props.user.type === 'Admin' || resource.owner.key === _this2.props.user.key ? _react2.default.createElement(
                        _reactRouter.Link,
                        { to: '/th/resource/article/' + resource._id },
                        'Preview'
                      ) : ""
                    )
                  ),
                  _react2.default.createElement(
                    'td',
                    null,
                    resource.owner.name,
                    ' (',
                    resource.owner.user_type,
                    ')'
                  ),
                  _react2.default.createElement(
                    'td',
                    null,
                    _this2.props.user.type === 'Admin' ? _react2.default.createElement(
                      'button',
                      { className: "btn btn-default btn-mini" + (!resource.is_approved ? " reverse" : ""), onClick: _this2.approveResource.bind(_this2, resource) },
                      !resource.is_approved ? 'Approve' : 'Approved'
                    ) : _react2.default.createElement(
                      'div',
                      null,
                      !resource.is_approved ? 'Pending' : _react2.default.createElement(
                        'span',
                        null,
                        'Approved'
                      )
                    )
                  ),
                  _react2.default.createElement(
                    'td',
                    null,
                    _react2.default.createElement(
                      _reactRouter.Link,
                      { to: '/' + _this2.props.language + (_this2.props.user.type === 'Admin' ? '/admin/resource/edit/' + resource._id : '/dashboard/resource/edit/' + resource._id), className: 'btn btn-default btn-mini reverse', style: { marginBottom: "5px" } },
                      'Edit'
                    ),
                    ' ',
                    _react2.default.createElement(
                      'button',
                      { className: 'btn btn-default btn-mini reverse', style: { marginBottom: "5px" }, onClick: _this2.toDeleteResource.bind(_this2, resource) },
                      'Delete'
                    )
                  )
                );
              })
            )
          )
        )
      );
    }
  }]);

  return ResourceIndex;
}(_react.Component);

exports.default = ResourceIndex;