'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ConfirmEmail = function (_Component) {
  _inherits(ConfirmEmail, _Component);

  function ConfirmEmail(argument) {
    _classCallCheck(this, ConfirmEmail);

    return _possibleConstructorReturn(this, (ConfirmEmail.__proto__ || Object.getPrototypeOf(ConfirmEmail)).call(this, argument));
  }

  _createClass(ConfirmEmail, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var user = JSON.parse($("#user").html());

      localStorage.setItem('key', user.key);
      localStorage.setItem('token', user.token);
      localStorage.setItem('type', user.type);

      setTimeout(function () {

        if (user.type === 'Employee') window.location.href = '/th/profile';else window.location.href = '/th/company';
      }, 3000);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { style: { marginTop: '100px', textAlign: 'center' } },
        _react2.default.createElement(
          'h2',
          null,
          'Confirm Email Complete'
        ),
        _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            'h4',
            null,
            'Splitshift is redirecting to Profile Page...'
          )
        )
      );
    }
  }]);

  return ConfirmEmail;
}(_react.Component);

exports.default = ConfirmEmail;