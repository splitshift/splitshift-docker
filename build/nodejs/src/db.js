var mongoose = require('mongoose');
var dbURI = "mongodb://db_splitshift:27017/splitshift";

mongoose.Promise = global.Promise;
mongoose.connect(dbURI);

mongoose.connection.on('connected', function () {
  console.log('Mongoose connected to ' + dbURI);
});

mongoose.connection.on('error',function (err) {
  console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('disconnected', function () {
  console.log('Mongoose disconnected');
});

mongoose.set('debug', 'true');
// require db
require('./models/user');
require('./models/user/address');

require('./models/user/languages');
require('./models/user/courses');
require('./models/user/experiences');
require('./models/user/educations');
require('./models/user/day_off_jobs');
require('./models/user/interests');

require('./models/company/jobs');
require('./models/company/benefits');
require('./models/company/links');
require('./models/company/review');

require('./models/feeds.js');
require('./models/files.js');
require('./models/activities.js');
require('./models/notifications.js');
require('./models/resource.js');
