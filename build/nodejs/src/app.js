'use strict';

// React Part
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
var _fs = require('fs');
var _fs2 = _interopRequireDefault(_fs);
var _express = require('express');
var _express2 = _interopRequireDefault(_express);
var _react = require('react');
var _react2 = _interopRequireDefault(_react);
var _server = require('react-dom/server');
var _reactRouter = require('react-router');
var _routes = require('./app_es5/routes');
var _routes2 = _interopRequireDefault(_routes);
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Nodejs Part
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// DB Connect
require('./db.js');

// Get Express
var routes = require('./routes/index');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "POST, PUT, DELETE, OPTIONS");
	next();
});

app.use('/api', routes);

var renderRoute = function renderRoute(response, renderProps) {

  response.render('index', {
    // content: (0, _server.renderToString)(_react2.default.createElement(_reactRouter.RouterContext, renderProps))
    content: ""
  });
}

var meta = require('./services/metaTag.js');

app.get('*', function (request, response) {
  (0, _reactRouter.match)({ routes: _routes2.default, location: request.url }, function (error, redirectLocation, renderProps) {
    if (error) {
      response.status(500).send(error.message);
    } else {
      var lastRoute = renderProps.routes[renderProps.routes.length - 1];
      var type, id;
      var route = request.url;

      if(lastRoute.name == 'resource_article') type = 'resource';
      else if(lastRoute.name.indexOf('feed_single')) type = 'feed';
      id = request.url.length>24?request.url.slice(-24):null;

      meta.getMeta(route, type, id, function(og) {
        response.render('index', {
          content: "",
          url: og.url,
          type: og.type,
          image: og.image,
          title: og.title,
          description: og.description
        });
      });
    }
  });

  // response.render('index', { content: "" });
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
