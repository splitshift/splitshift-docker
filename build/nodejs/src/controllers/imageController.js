var multer = require('multer');
var fs = require('fs');
var path = require('path');
var mongoose = require('mongoose');
var Image = mongoose.model('Image');
var User = mongoose.model('User');
var Activity = mongoose.model('Activity');
var Request = mongoose.model('Request');
var Notification = mongoose.model('Notification');
var Job = mongoose.model('Job');
var ApplyJob = mongoose.model('ApplyJob');
var Resource = mongoose.model('Resource');
var Feed = mongoose.model('Feed');

var tools = require('./tools');
var verifyToken = tools.verifyToken;
var verifyKey = tools.verifyKey;
var updateData = tools.updateData;

var resize = require('../services/resize')

var maxSize = 5 * 1024 * 1024;//byte

module.exports.putProfileImage = function(req, res) {
	console.log('Put Profile Image.');
	var token = req.headers.authorization;
	verifyToken(token, res, function(err, decoded) {
		if(decoded) {
			var key = decoded.key;
			var json = req.body;
			console.log("JSON IMAGE");
			console.log(json);
			var type;
			verifyKey(key, res, function(err, user) {
				if(user) {
					var dir = 'public/images/profiles';
					var old_img = user.profile_image;
					if(!fs.existsSync(dir)) {
						fs.mkdirSync(dir);
					}
					var storage = multer.diskStorage({
					  destination: function(req, file, cb) {
					    cb(null, dir);
					  },
						filename: function(req, file, cb) {
							type = path.extname(file.originalname);
				      cb(null, key+type);
				    }
					});
					var upload = multer({
						storage: storage,
						limits: { fileSize: maxSize },
						fileFilter: function(req, file, cb) {
							if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
								return cb({"message": "Only image files are allowed!"});
							}
							cb(null, true);
						}
					}).single('avatar');
					upload(req, res, function(err) {
						if(err) {
							res.status(400).send(err);
						}
						else {
							if(req.file) {
								user.profile_image = req.file.path.slice(6);
								updateData(key, res, user, function(err) {
									if(!err) {
										//remove old image
										if(old_img && user.profile_image.toLowerCase() !== old_img.toLowerCase()) {
											fs.stat('public'+old_img, function(err) {
												if(!err) fs.unlink('public'+old_img);
											});
										}
										resize(req.file.path, function(err) {
											if(!req.file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
												if(err)  return res.status(400).send(err);
											}
											res.status(200).send({"path":user.profile_image});
										});
										//update feed
										Feed.update({'owner.key': key},{'owner.profile_image': user.profile_image},{multi: true}, function(err) {
											if(err) console.error(err);
										});
										//update comment and reply
										Activity.find({key: key}, function(err, activities) {
											if(err) res.status(400).send({"message": err});
											else {
												activities.forEach(function(act, index) {
													var query = {};
													if(act.activity.resource_id) {
														Resource.findById(act.activity.resource_id, function(err, resource) {
															if(resource) {
																var comment_ind = tools.findArray(resource.comments, act.activity.comment_id);
																query['comments.'+comment_ind+'.owner.profile_image'] = user.profile_image;
																Resource.update({_id: act.activity.resource_id}, query, function(err) {
																	if(err) console.error(err);
																});
															}
														});
													}
													else {
														Feed.findOne({_id: act.activity.feed_id}, function(err, feed) {
															if(feed) {
																var comment_ind = tools.findArray(feed.comments, act.activity.comment_id);
																if(act.activity.reply_id) {
																	var reply_ind = tools.findArray(feed.comments[comment_ind].replies, act.activity.reply_id);
																	query['comments.'+comment_ind+'.replies.'+reply_ind+'.owner.profile_image'] = user.profile_image;
																}
																else query['comments.'+comment_ind+'.owner.profile_image'] = user.profile_image;
																Feed.update({_id: act.activity.feed_id}, query, function(err) {
																	if(err) console.error(err);
																});
															}
														});
													}
												});
											}
										});
										//update request
										Request.update({'from.key': key},{'from.profile_image': user.profile_image},{multi: true}, function(err) {
											if(err) console.error(err);
										});
										//update noti
										Notification.update({'from.key': key},{'from.profile_image': user.profile_image},{multi: true}, function(err) {
											if(err) console.error(err);
										});
										//update resource
										Resource.update({'owner.key': key},{'owner.profile_image': user.profile_image},{multi: true}, function(err) {
											if(err) console.error(err);
										});
										//update job and applyjob for employer
										if(user.type == 'Employer') {
											Job.update({'owner.key': key}, {'owner.profile_image': user.profile_image}, {multi: true}, function(err) {
												if(err) console.error(err);
											});
											ApplyJob.update({'to.key': key}, {'to.profile_image': user.profile_image}, {multi: true}, function(err) {
												if(err) console.error(err);
											});
										}
										//update review and applyjob for employee
										if(user.type == 'Employee') {
											User.update({type: 'Employer', 'review.from.key': user.key}, {'review.$.from.profile_image': user.profile_image}, {multi: true}, function(err) {
												if(err) console.error(err);
											});
											ApplyJob.update({'from.key': key}, {'from.profile_image': user.profile_image}, {multi: true},function(err) {
												if(err) console.error(err);
											});
										}
									}
								});
							}
							else {
								res.status(400).send({"message":"File not found."});
							}
						}
					});
				}
			});
		}
	});
};

module.exports.putBanner = function(req, res) {
	console.log('Put Banner.');
	var token = req.headers.authorization;
	verifyToken(token, res, function(err, decoded) {
		if(decoded) {
			var key = decoded.key;
			var json = req.body;
			var type;
			verifyKey(key, res, function(err, user) {
				if(user) {
					var dir = 'public/images/banners';
					var old_img = user.banner;
					if(!fs.existsSync(dir)) {
						fs.mkdirSync(dir);
					}
					var storage = multer.diskStorage({
					  destination: function(req, file, cb) {
					    cb(null, dir);
					  },
						filename: function(req, file, cb) {
							type = path.extname(file.originalname);
				      cb(null, key+type);
				    }
					});
					var upload = multer({
						storage: storage,
						limits: { fileSize: maxSize },
						fileFilter: function(req, file, cb) {
							if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
								return cb({"message": "Only image files are allowed!"});
							}
							cb(null, true);
						}
					}).single('avatar');
					upload(req, res, function(err) {
						if(err) {
							res.status(400).send(err);
						}
						else {
							if(req.file) {
								user.banner = req.file.path.slice(6);
								updateData(key, res, user, function(err) {
									if(!err) {
										if(!(/template/.test(old_img))) {
											if(old_img && user.banner.toLowerCase() !== old_img.toLowerCase()) {
												fs.stat('public'+old_img, function(err) {
													if(!err) fs.unlink('public'+old_img);
												});
											}
										}
										resize(req.file.path, function(err) {
											if(!req.file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
												if(err)  return res.status(400).send(err);
											}
											res.status(200).send({"path":user.banner});
										});
									}
								});
							}
							else {
								res.status(400).send({"message":"File not found."});
							}
						}
					});
				}
			});
		}
	});
};

module.exports.getGallery = function(req, res) {
	console.log('Get Gallery.');
	var token = req.headers.authorization;
	verifyToken(token, res, function(err, decoded) {
		if(decoded) {
			var key = decoded.key;
			var json = req.body;
			verifyKey(key, res, function(err, user) {
				if(user) {
					var update = user.gallery;
					res.status(200).send(update);
				}
			});
		}
	});
};

module.exports.postImage = function(req, res) {
	console.log('Post Image.');
  var update = new Image();
  var filename = update._id;
  var dir = 'public/uploads/images';
  var type = "";
  if(!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  var storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, dir);
    },
    filename: function(req, file, cb) {
      var old = file.originalname;
      type = ".jpg";
      if(old.length>=4) {
        if(old.length-4==".") {
          type = old.slice(old.length-4,old.length);
        }
      }
      console.log(type);
      cb(null, filename+type);
    }
  });
  var upload = multer({storage:storage, limits: { fileSize: maxSize }}).single('avatar');
  upload(req, res, function(err) {
    if(err) {
      res.status(400).send({"message":"Upload file is error."});
    }
    else {
      if(req.file) {
        update.path = dir+'/'+filename+type;
        update.save(function(err) {
          if(err) {
            res.status(400).send(err);
          }
          else {
            res.status(201).send({"path":dir+'/'+filename+type});
          }
        });
      }
      else {
        res.status(400).send({"message":"File's not found."});
      }
    }
  });
};

module.exports.putBannerTemplate = function(req, res) {
	console.log('Put Banner Template.');
	var user = req.user;
	var path = req.body.path;

	User.update({key: user.key}, {banner: path}, function(err) {
		if(err) res.status(400).send(err);
		else res.status(200).send({"message": "Change banner complete."});
	});

};
