var mongoose = require('mongoose');
var User = mongoose.model('User');
var Notification = mongoose.model('Notification');

var tools = require('./tools');
var findKeyArray = tools.findKeyArray;

module.exports.putFollow = function(req, res) {
  console.log('Post Follow');
  var key = req.user.key;
  var json = req.body;
  if(json.to) {
    var user1 = req.user;
    if(user1.type == 'Admin') {
      return res.status(400).send({"message": "Admin cannot follow other user."});
    }
    tools.verifyKey(json.to, res, function(err, user2) {
      if(err) {
        res.status(400).send({"message":err});
      }
      else if(user2.type == 'Admin') {
        return res.status(400).send({"message": "Cannot follow admin."});
      }
      else if(user1.key == user2.key) {
        res.status(400).send({"message":"Don't follow yourself."});
      }
      else {
        if(!user1.following) {
          user1.following = [];
        }
        if(findKeyArray(user1.following, user2.key) != user1.following.length) {
          user1.following.splice(i, 1);
          var follow = false;
        }
        else {
          user1.following.push(user2.key);
          var follow = true;
        }

        User.update({key:user1.key}, {following:user1.following}, function(err) {
          if(err) {
            res.status(400).send({"message":"User1 save error."});
          }
          else {
            if(!user2.followers) {
              user2.followers = [];
            }
            if(follow) user2.followers.push(user1.key);
            else user2.followers.splice(user2.followers.indexOf(user1.key), 1);
            User.update({key:user2.key}, {followers:user2.followers}, function(err) {
              if(err) {
                res.status(400).send({"message":"User2 save error."});
              }
              else {
                if(follow) {
                  if(key !== user2.key) {
                    var New = new Notification({
                      from : {
                        key: key,
                        name: user1.profile.company_name?user1.profile.company_name:user1.profile.first_name+' '+user1.profile.last_name,
                        profile_image: user1.profile_image,
                        user_type: req.user.type
                      },
                      to : user2.key,
                      notification_type: 'follow',
                      date: new Date()
                    });
                    New.save(function(err) {
                      if(err) console.error(err);
                    });
                  }
                  res.status(200).send({"message":"Following complete."});
                }
                else {
                  res.status(200).send({"message":"Unfollowing complete."});
                }
              }
            });
          }
        });
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.getFollowing = function(req, res) {
  console.log("Get Following.");
  tools.verifyKey(req.params.user_key, res, function(err, user) {
    if(user) {
      if(user.type == 'Admin') {
        return res.status(400).send({"message": "Admin does not have followings."});
      }
      var following = user.following;
      User.find({key:{$in:following}}, function(err, user) {
        if(err) {
          res.status(400).send({"message":"Find error."});
        }
        else {
          var data = [];
          for(var i=0;i<user.length;i++) {
            data[i] = {
              _id : user[i]._id,
              key : user[i].key,
              name : user[i].profile.company_name?user[i].profile.company_name:user[i].profile.first_name+' '+user[i].profile.last_name,
              profile_image : user[i].profile_image,
              type : user[i].type
            };
          }
          res.status(200).send(data);
        }
      });
    }
  });
};

module.exports.getFollowers = function(req, res) {
  console.log("Get Followers.");
  tools.verifyKey(req.params.user_key, res, function(err, user) {
    if(user) {
      if(user.type == 'Admin') {
        return res.status(400).send({"message": "Admin does not have followers."});
      }
      var followers = user.followers;
      User.find({key:{$in:followers}}, function(err, user) {
        if(err) {
          res.status(400);
          res.send({"message":"Find error."});
        }
        else {
          var data = [];
          for(var i=0;i<user.length;i++) {
            data[i] = {
              _id : user[i]._id,
              key : user[i].key,
              name : user[i].profile.company_name?user[i].profile.company_name:user[i].profile.first_name+' '+user[i].profile.last_name,
              profile_image: user[i].profile_image,
              type : user[i].type
            };
          }
          res.status(200).send(data);
        }
      });
    }
  });
};
