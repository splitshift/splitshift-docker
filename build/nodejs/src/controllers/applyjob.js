var mongoose = require('mongoose');
var ApplyJob = mongoose.model('ApplyJob');
var Job = mongoose.model('Job');
var Notification = mongoose.model('Notification');

var tools = require('./tools');
var checkNull = tools.checkNull;
var findArray = tools.findArray;
var verifyKey = tools.verifyKey;

module.exports.PostApplyJob = function(req, res) {
  console.log('Post ApplyJob');
  var key = req.user.key;
  var json = req.body;

  if(checkNull(json.to, json.email, json.tel, json.cover_letter, json.job_id)) {
    verifyKey(json.to, res, function(err, user){
      if(err) res.status(400).send(err);
      else if(!user) res.status(400).send({"message": "User not found"});
      else if(user.type == "Employer") {
        Job.findOne({_id:json.job_id}, {owner: 0}, function(err, job) {
          if(err) res.status(400).send(err);
          else if(!job) res.status(400).send({"message":"Job id invalid"});
          else {
            var New = new ApplyJob({
              to : {
                key: user.key,
                name: user.profile.company_name,
                profile_image: user.profile_image,
                user_type: user.type
              },
              from : {
                key: key,
                name: req.user.profile.first_name+' '+req.user.profile.last_name,
                profile_image: req.user.profile_image,
                user_type: req.user.type
              },
              email : json.email,
              date : new Date(),
              cover_letter : json.cover_letter,
              tel : json.tel,
              files: json.files,
              job: job,
              status: 'no status'
            });
            New.save(function(err) {
              if(err) {
                res.status(400).send({"message":"Save error."});
              }
              else {
                if(key !== json.to) {
                  var newNoti = new Notification({
                    from : {
                      key: key,
                      name: req.user.profile.first_name+' '+req.user.profile.last_name,
                      profile_image: req.user.profile_image,
                      user_type: req.user.type
                    },
                    to : json.to,
                    notification_type: 'applyjob',
                    applyjob: New._id,
                    date: new Date()
                  });
                  newNoti.save(function(err) {
                    if(err) console.error(err);
                  });
                }
                res.status(201).send({"message":"Save Complete."});
              }
            });
          }
        });
      }
      else {
        res.status(400).send({"message":"Reciever must be employer."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.GetApplyJob = function(req, res) {
  console.log('Get ApplyJob');
  var json = req.body;
  var id = req.params.id;
  ApplyJob.findOne({_id:id}, function(err, user) {
    if(err) {
      res.status(404).send({"message":"Finding error."});
    }
    else {
      res.status(200).send(user);
    }
  });
};

module.exports.GetAllApplyJob = function(req, res) {
  console.log('Get All ApplyJob');
  var json = req.body;
  var key = req.company.key;
  ApplyJob.find({'to.key':key}, {}, {sort: {date: -1}}, function(err, user) {
    if(err) {
      res.status(404).send({"message":"Finding error."});
    }
    else {
      res.status(200).send(user);
    }
  });
};

module.exports.searchApplyJob = function(req, res) {
  var company = req.company;
  var query = {$and: []};

  var keyword = req.query.keyword;
	if(keyword) keyword = keyword.replace(/\s+/g,'|');
	query.$and.push({'from.name': new RegExp(keyword,'gi')});

  if(req.query.job_id) query.$and.push({'job._id': mongoose.Types.ObjectId(req.query.job_id)});

  query.$and.push({to: company.key});

  var status = req.query.status;
  if(status) status = status.replace(/\s+/g,'|');
	query.$and.push({'status': new RegExp(status,'gi')});

  ApplyJob.find(query, function(err, jobs) {
    if(err) res.status(200).send(err);
    else res.status(200).send(jobs);
  });

};

module.exports.putStatus = function(req, res) {
  var id = req.params.id;
  var status = req.body.status;

  ApplyJob.update({_id: id}, {status: status}, function(err) {
    if(err) res.status(400).send(err);
    else res.status(200).send({"message": "Status has been changed."});
  });
};

module.exports.delApplyJob = function(req, res) {
  var id = req.params.id;
  var key = req.company.key;

  ApplyJob.findById(id, function(err, applyjob) {
    if(err) res.status(400).send(err);
    else if(applyjob) {
      if(applyjob.to.key == key) {
        applyjob.remove(function(err) {
          if(err) res.status(400).send(err);
          else res.status(200).send({"message": "applyjob has been remove."});
        });
      }
      else res.status(200).send({"message": "you cannot remove applyjob."});
    }
    else res.status(400).send({"message": "applyjob not found."});
  });
};
