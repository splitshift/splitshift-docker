var mongoose = require('mongoose');

var ApplyJob = mongoose.model('ApplyJob');
var Resource = mongoose.model('Resource');
var Feed = mongoose.model('Feed');
var Job = mongoose.model('Job');
var User = mongoose.model('User');

module.exports.getStat = function(req, res) {
  var user = req.user;
  var result = {};

  var feed = new Promise(function(resolve, reject) {
    Feed.find({'owner.key': user.key}, function(err, feeds) {
      if(err) reject(err);
      else {
        var type = {all: 0};
        feeds.forEach(function(feed) {
          type.all += 1
          if(type[feed.privacy] == undefined) type[feed.privacy] = 1;
          else type[feed.privacy] += 1;
        });
        resolve(type);
      }
    });
  });

  var resource = new Promise(function(resolve, reject) {
    Resource.find({'owner.key': user.key}, function(err, resources) {
      if(err) reject(err);
      else {
        var all = 0;
        var activated = 0;
        var deactivated = 0;
        resources.forEach(function(resource) {
          all += 1;
          if(resource.is_approved) activated += 1;
          else deactivated += 1;
        });
        resolve({all: all, activated: activated, deactivated: deactivated});
      }
    });
  });

  var applyjob = new Promise(function(resolve, reject) {
    ApplyJob.find({
      $or: [
        {'to.key': user.key},
        {'from.key': user.key}
      ],
      $and: [
        {status: {$ne: 'no status'}},
        {status: {$ne: 'No Status'}}
      ]
    }, function(err, apps) {
      if(err) reject(err);
      else {
        var apply = {all: 0,open: 0, declined: 0};

        apps.forEach(function(app) {
          if(app.status == "We regret to inform you that your application has not been successful") apply.declined += 1;
          else apply.open += 1;
          apply.all += 1;
        });

        resolve(apply);
      }
    });
  });

  var job = new Promise(function(resolve, reject) {
    Job.find({'owner.key': user.key}, function(err, jobs) {
      if(err) reject(err);
      else {
        var rsjob = {all: 0, activated: 0, deactivated: 0};

        jobs.forEach(function(job) {
          if(job.activated) rsjob.activated += 1;
          else rsjob.deactivated += 1;
          rsjob.all += 1;
        });

        resolve(rsjob);
      }
    });
  });

  var hr = new Promise(function(resolve, reject) {
    if(user.type == 'Employer') {
      var employees = user.profile.employees;

      User.count({key: {$in: employees}, 'profile.rank': 'HR'}, function(err, hr) {
        if(err) reject(err);
        else resolve(hr);
      });
    }
    else resolve(0);
  });

  Promise.all([feed, resource, applyjob, job, hr])
  .then(function(val) {
    var result = {};
    result.feed = val[0];
    result.resource = val[1];
    result.applyjob = val[2];
    result.job = val[3];
    result.hr = val[4];

    res.status(200).send(result);
  })
  .catch(function(reason) {
    res.status(400).send(reason);
  });
};
