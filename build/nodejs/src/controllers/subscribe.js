var mongoose = require('mongoose');
var Subscribe = mongoose.model('Subscribe');
var config = require('../config');

var sendGrid = require('sendgrid').mail;
var sg = require('sendgrid').SendGrid(config.sendgrid.api_key);

var fs = require('fs');
var jade = require('jade');

var tools = require('./tools');
var checkNull = tools.checkNull;

module.exports.getSubscribe = function(req, res) {
  console.log("Get Subscribe");
  Subscribe.find({}, function(err, user) {
    if(err) {
      res.status(400).send({"message":err});
    }
    else {
      res.status(200).send(user);
    }
  });
};

module.exports.postSubscribe = function(req, res) {
  console.log('Post Subscrube');
  var json = req.body;
  Subscribe.findOne({email:json.email}, function(err, user) {
    if(user) {
      res.status(409);
      res.send({"message":"Email address already taken."});
    }
    else if(checkNull(json.email)) {
      var New = new Subscribe({
        email: json.email,
        date: new Date()
      });
      if(json.type == "Employer") {
        New.company_name = json.company_name;
      }
      fs.readFile('./views/Email.html', 'utf8', function(err, data) {
        if(err){
          res.status(404);
          res.send({"message":"File not found." + err});
        }
        else {
          // console.log(data);
          var from_email = new sendGrid.Email("info@splitshift.com","Splitshift");
          var to_email = new sendGrid.Email(New.email);
          var subject = "Congrats and Welcome to Splitshift!";
          var content = new sendGrid.Content("text/html", data);
          var email = new sendGrid.Mail(from_email, subject, to_email, content);
          var requestBody = email.toJSON();
          var request = sg.emptyRequest();
          request.method = 'POST';
          request.path = '/v3/mail/send';
          request.body = requestBody;
          sg.API(request, function (response) {
            if(response.body.errors) {
              res.status(response.statusCode);
              res.send({"message": response.body});
            }
            else if(response.statusCode == 202){
              New.save(function(err) {
                if(err) {
                  res.status(400);
                  res.send({"message":"Save error."});
                }
                else {
                  res.status(201);
                  res.send({"message":"Subscribe complete."});
                }
              });
            }
            else {
              res.status(response.statusCode);
              res.send({"message":response.body});
            }
          });
        }
      });
    }
    else {
      res.status(400);
      res.send({"message":"Data is missing."});
    }
  });
};
