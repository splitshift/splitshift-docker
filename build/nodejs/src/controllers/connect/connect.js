var mongoose = require('mongoose');
var User = mongoose.model('User');
var Request = mongoose.model('Request');
var Notification = mongoose.model('Notification');

var tools = require('../tools');
var findKeyArray = tools.findKeyArray;

module.exports.deleteConnect = function(req, res) {
  console.log("Delete Connect.");
  var key = req.user.key;
  var json = req.body;
  if(tools.checkNull(json.to)) {
    tools.verifyKey(json.to, res, function(err, user) {
      var i = findKeyArray(req.user.connected, user.key);
      var j = findKeyArray(user.connected, req.user.key);
      if(user) {
        if(i != req.user.connected.length && j != user.connected.length) {
          req.user.connected.splice(i,1);
          user.connected.splice(j,1);
          User.update({key:req.user.key}, {connected:req.user.connected}, function(err) {
            if(err) {
              res.status(400);
              res.send({"message":"Update 1 error."});
            }
            else {
              User.update({key:user.key}, {connected:user.connected}, function(err2) {
                if(err2) {
                  res.status(400);
                  res.send({"message":"Update 2 error."});
                }
                else {
                  res.status(200);
                  res.send({"message":"Update complete."});
                }
              });
            }
          });
        }
        else {
          res.status(400);
          res.send({"message":"You aren't friend."});
        }
      }
    });
  }
  else {
    res.status(400);
    res.send({"message":"Data is missing."});
  }
};

module.exports.postConnect = function(req, res) {
  console.log("Post Connect.");
  var key = req.user.key;
  var json = req.body;
  if(tools.checkNull(json.to)) {
    tools.verifyKey(json.to, res, function(err, user) {
      if((req.user.type != "Employee" || user.type != "Employee")) {
        res.status(400);
        res.send({"message":"Only employee and employee can connect"});
      }
      else if(findKeyArray(user.connected, key)==user.connected.length&&findKeyArray(req.user.connected, user.key)==req.user.connected.length){
        Request.findOne({'from.key':key, to:user.key}, function(err, old){
          if(old) {
            res.status(400);
            res.send({"message":"Your have already request."});
          }
          else {
            var New = new Request({
              from : {
                key: key,
                name: req.user.profile.first_name+' '+req.user.profile.last_name,
                profile_image: req.user.profile_image,
                occupation: req.user.profile.occupation.position,
                user_type: req.user.type
              },
              to : user.key,
              date: new Date()
            });
            New.save(function(err) {
              if(err) {
                res.status(400);
                res.send({"message":err});
              }
              else {
                res.status(201);
                res.send({"message":"Request complete."});
              }
            });
          }
        });
      }
      else {
        res.status(400);
        res.send({"message":"You have been already friend."});
      }
    });
  }
  else {
    res.status(400);
    res.send({"message":"Data is missing"});
  }
};

module.exports.getAllRequest = function(req, res) {
  console.log("Get All Request.");
  Request.find({to: req.user.key}, function(err, reqs) {
    if(err) {
      res.status(400).send({"message":err});
    }
    else {
      Request.update({to: req.user.key}, {seen: true}, {multi: true}, function(err) {
        if(err) console.error(err);
      });
      res.status(200).send(reqs);
    }
  });
};

module.exports.putRequest = function(req, res) {
  console.log("Put Request.");
  var key = req.user.key;
  var json = req.body;
  if(tools.checkNull(json.request_id) ) {
    Request.findById(json.request_id, function(err, user) {
      if(user) {
        if(json.response) {
          tools.verifyKey(user.to, res, function(err, to) {
            if(to) {
              tools.verifyKey(user.from.key, res, function(err, from) {
                if(to.key!=key && from){
                  res.status(400);
                  res.send({"message":"You can't response other request."});
                }
                else {
                  to.connected.push(from.key);
                  from.connected.push(to.key);
                  User.update({key:to.key}, {connected:to.connected}, function(err) {
                    if(err) {
                      res.status(400);
                      res.send({"message":"Update to error."});
                    }
                    else {
                      User.update({key:from.key}, {connected:from.connected}, function(err2) {
                        if(err && err2) {
                          res.status(400);
                          res.send({"message":"Response complete but can't save. please send request again."});
                        }
                        if(err2) {
                            res.status(400);
                            res.send({"message":"Update from error."});
                        }
                        else {
                          removeRequest(json.request_id, res);
                          var newNoti = new Notification({
                            from : {
                              key: to.key,
                              name: to.profile.company_name?to.profile.company_name:to.profile.first_name+' '+to.profile.last_name,
                              profile_image: to.profile_image,
                              user_type: to.type
                            },
                            to : from.key,
                            notification_type: 'connect',
                            date: new Date()
                          });
                          newNoti.save(function(err) {
                            if(err) console.error(err);
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
        else {
          removeRequest(json.request_id, res);
        }
      }
      else {
        res.status(400);
        res.send({"message":"No this request's id."});
      }
    });
  }
  else {
    res.status(400);
    res.send({"message":"Data is missing."});
  }
};

var removeRequest = function(id, res) {
  console.log("Remove Request.");
  Request.remove({_id:id}, function(err, remove) {
    if(err) {
      res.status(400);
      res.send({"message":err});
    }
    else {
      res.status(200);
      res.send({"message":"Response complete."});
    }
  });
};

module.exports.getConnect = function(req, res) {
  console.log("Get Connect.");
  tools.verifyKey(req.params.user_key, res, function(err, user) {
    if(user) {
      if(user.type == 'Admin') {
        return res.status(400).send({"message": "Admin does not have connected."});
      }
      if(user.type == 'Employer') {
        return res.status(400).send({"message": "Employer does not have connected."});
      }
      var connected = user.connected;
      User.find({key:{$in:connected}}, function(err, user) {
        if(err) {
          res.status(400).send({"message":"Find error."});
        }
        else {
          var data = [];
          for(var i=0;i<user.length;i++) {
            data[i] = {
              _id : user[i]._id,
              key : user[i].key,
              name : user[i].profile.company_name?user[i].profile.company_name:user[i].profile.first_name+' '+user[i].profile.last_name,
              profile_image : user[i].profile_image,
              type : user[i].type
            };
          }
          res.status(200).send(data);
        }
      });
    }
  });
};
