var mongoose = require('mongoose');
var fs = require('fs');
var User = mongoose.model('User');
var Notification = mongoose.model('Notification');
var Activity = mongoose.model('Activity');
var Job = mongoose.model('Job');
var Chat = mongoose.model('Chat');
var Image = mongoose.model('Image');
var File = mongoose.model('File');
var Request = mongoose.model('Request');
var ApplyJob = mongoose.model('ApplyJob');
var Resource = mongoose.model('Resource');
var Feed = mongoose.model('Feed');

var tools = require('../tools');

module.exports.getAllUser = function(req, res) {
  User.find({
    type: {$ne: 'Admin'}
  }, {}, {
    sort: {created: -1}
  }, function(err, users) {
    if(err) res.status(400).send(err);
    else {
      var result = [];
      users.forEach(function(user) {
        result.push({
          key: user.key,
          name: user.profile.company_name?user.profile.company_name:user.profile.first_name+' '+user.profile.last_name,
          email: user.email,
          profile_image: user.profile_image,
          confirm: user.confirm,
          type: user.type
        });
      });
      res.status(200).send(result);
    }
  });
};

module.exports.deleteUser = function(req, res) {
  var key = req.params.key;

  tools.verifyKey(key, res, function(err, user) {
    if(user) {
      User.remove({key: key}, function(err) {
        if(err) res.status(400).send(err);
        else {
          User.update({
            $or: [
              {following: key},
              {followers: key},
              {connected: key}
            ]
          }, {
            $pull: {
              followers: key,
              following: key,
              connected: key
            }
          }, {multi: true}, function(err) {
            if(err) console.error(err);
          });
          fs.stat('public'+user.profile_image, function(err) {
            if(!err) fs.unlink('public'+user.profile_image);
          });
          if(!(/template/.test(user.banner))) {
            fs.stat('public'+user.banner, function(err) {
              if(!err) fs.unlink('public'+user.banner);
            });
          }
          Notification.remove({'from.key': key}, function() {
            if(err) console.error(err);
          });
          //update comment and reply
  				Activity.find({key: key}, function(err, activities) {
  					if(err) res.status(400).send({"message": err});
  					else {
  						activities.forEach(function(act, index) {
  							var query = {$pull:{}};
  							if(act.activity.resource_id) {
  								Resource.findById(act.activity.resource_id, function(err, resource) {
                    if(resource) {
    									query.$pull['comments'] = {_id: act.activity.comment_id};
    									Resource.update({_id: act.activity.resource_id}, query, function(err) {
    										if(err) console.error(err);
    									});
                    }
  								});
  							}
  							else {
  								Feed.findOne({_id: act.activity.feed_id}, function(err, feed) {
                    if(feed) {
    									if(act.activity.reply_id) {
                        var comment_ind = tools.findArray(feed.comments, act.activity.comment_id);
    										query.$pull['comments.'+comment_ind+'.replies'] = {_id: act.activity.reply_id};
    									}
    									else query.$pull['comments'] = {_id: act.activity.comment_id};
    									Feed.update({_id: act.activity.feed_id}, query, function(err) {
    										if(err) console.error(err);
    									});
                    }
  								});
  							}
  						});
              Activity.remove({key: key}, function(err) {
                if(err) console.error(err);
              });
  					}
  				});
          Job.remove({'owner.key': key}, function(err) {
            if(err) console.error(err);
          });
          Chat.remove({users: key}, function(err) {
            if(err) console.error(err);
          });
          Request.remove({'from.key': key}, function(err) {
            if(err) console.error(err);
          });
          ApplyJob.remove({$or:[{'from.key': key},{'to.key': key}]}, function(err) {
            if(err) console.error(err);
          });
          Resource.remove({'owner.key': key}, function(err) {
            if(err) console.error(err);
          });
          Feed.remove({'owner.key': key}, function(err) {
            if(err) console.error(err);
          });
          Image.find({owner: key}, function(err, images) {
            if(err) console.error(err);
            images.forEach(function(image) {
              try {
                fs.statSync(image.path);
                console.log('exists');
                fs.unlink(image.path);
              }
              catch(err) {
                console.log('does not exists');
              }
            });
            Image.remove({owner: key}, function(err) {
              if(err) console.error(err);
            });
          });
          File.find({owner: key}, function(err, files) {
            if(err) console.error(err);
            files.forEach(function(file) {
              try {
                fs.statSync(file.path);
                console.log('exists');
                fs.unlink(file.path);
              }
              catch(err) {
                console.log('does not exists');
              }
            });
            File.remove({owner: key}, function(err) {
              if(err) console.error(err);
            });
          });
          User.update({type: 'Employer'}, {$pull: {'profile.employees': key, 'profile.formers': key}},function(err) {
            if(err) console.error(err);
          });
          res.status(200).send({"message": "User has been deleted."});
        }
      });
    }
  });
};
