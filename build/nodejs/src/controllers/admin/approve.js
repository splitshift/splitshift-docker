var mongoose = require('mongoose');
var Resource = mongoose.model('Resource');

module.exports.putApprove = function(req, res) {
  var json = req.body;

  Resource.findById(json.resource_id, function(err, resource) {
    if(err) res.status(400).send(err);
    if(resource) {
      Resource.update({_id: json.resource_id}, {"is_approved": !resource.is_approved}, function(err) {
        if(err) res.status(400).send({"message": err});
        else res.status(200).send({"message": !resource.is_approved?"Resource approved.":"Resource unapproved."});
      });
    }
    else res.status(400).send({"message": "Resource not found."});
  });
};
