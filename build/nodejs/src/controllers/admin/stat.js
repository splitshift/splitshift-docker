var mongoose = require('mongoose');

var Feed = mongoose.model('Feed');
var Resource = mongoose.model('Resource');
var Chat = mongoose.model('Chat');
var Subscribe = mongoose.model('Subscribe');
var Job = mongoose.model('Job');
var ApplyJob = mongoose.model('ApplyJob');

module.exports.getStat = function(req, res) {
  var result = {};

  var feed = new Promise(function(resolve, reject) {
    Feed.find({}, function(err, feeds) {
      if(err) reject(err);
      else {
        var all = 0;
        var user = {all: 0, public: 0, timeline: 0, connected: 0, employer: 0, onlyme: 0};
        var company = {all: 0, public: 0, timeline: 0, employee: 0};
        var admin = 0;
        var like = 0;
        var comment = 0;

        feeds.forEach(function(feed) {
          all += 1
          like += feed.likes.length;
          comment += feed.comments.length;

          if(feed.owner.user_type == 'Employee') {
            user['all'] += 1;
            user[feed.privacy] += 1;
          }
          else if(feed.owner.user_type == 'Employer') {
            company['all'] += 1;
            company[feed.privacy] += 1;
          }
          else admin += 1;
        });
        resolve({feed: {all: all, user: user, company: company, admin: admin}, like: like, comment: comment});
      }
    });
  });

  var resource = new Promise(function(resolve, reject) {
    Resource.find({}, function(err, resources) {
      if(err) reject(err);
      if(err) reject(err);
      else {
        var all = 0;
        var approved = {all: 0, user: 0, company: 0};

        resources.forEach(function(resource) {
          all += 1;
          if(resource.is_approved) {
            approved.all += 1;
            if(resource.owner.user_type == 'Employee') approved.user += 1;
            else if(resource.owner.user_type == 'Employer') approved.company += 1;
          }
        });
        resolve({all: all, approved: approved});
      }
    });
  });

  var message = new Promise(function(resolve, reject) {
    Chat.find({}, function(err, chats) {
      if(err) reject(err);
      else {
        var messages = 0;

        chats.forEach(function(chat) {
          messages += chat.messages.length;
        });
        resolve(messages);
      }
    });
  });

  var subscribe = new Promise(function(resolve, reject) {
    Subscribe.count({}, function(err, subs) {
      resolve(subs);
    });
  });

  var job = new Promise(function(resolve, reject) {
    Job.find({}, function(err, jobs) {
      if(err) reject(err);
      var resjob = {all: 0, activated: 0, deactivated: 0};

      jobs.forEach(function(job) {
        resjob.all += 1;
        if(job.activated) resjob.activated += 1;
        else resjob.deactivated += 1;
      });

      resolve(resjob);
    });
  });

  var applyjob = new Promise(function(resolve, reject) {
    ApplyJob.count({}, function(err, apps) {
      if(err) reject(err);
      resolve(apps);
    });
  });

  Promise.all([feed, resource, message, subscribe, job, applyjob])
  .then(function(val) {
    var result = {};
    result.feed = val[0].feed;
    result.like = val[0].like;
    result.comment = val[0].comment;
    result.resource = val[1];
    result.message = val[2];
    result.subscribe = val[3];
    result.job = val[4];
    result.applyjob = val[5];

    res.status(200).send(result);
  })
  .catch(function(reason) {
    res.status(400).send(reason);
  });

};
