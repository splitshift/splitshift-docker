var mongoose = require('mongoose');
var Resource = mongoose.model('Resource');

module.exports.getAllResource = function(req, res) {
  Resource.find({}, {_id: 1, title: 1, owner: 1, is_approved: 1,date: 1}, function(err, resources) {
    if(err) res.status(400).send(err);
    else res.status(200).send(resources);
  });
};
