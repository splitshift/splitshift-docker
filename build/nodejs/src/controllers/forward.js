var fs = require('fs');

var config = require('../config');
//sendgrid
var sendGrid = require('sendgrid').mail;
var sg = require('sendgrid').SendGrid(config.sendgrid.api_key);

module.exports.sendForward = function(req, res) {
  var user = req.user;
  var name = user.profile.company_name?user.profile.company_name:user.profile.first_name+' '+user.profile.last_name;
  var to = req.body.to;
  var data = req.body.data;
  var type = req.params.type;

  if(to && data && type) {
    fs.readFile('./views/forward.html', 'utf8', function(err, forward) {
      if(err) console.error("File not found." + err);
      else {
        mail = new sendGrid.Mail();
        email = new sendGrid.Email("info@splitshift.com", "Splitshift");
        mail.setFrom(email);
        mail.setSubject(name+" wants to share a "+type+" with you from Splitshift.com");

        personalization = new sendGrid.Personalization();
        email = new sendGrid.Email(to);
        personalization.addTo(email);

        mail.addPersonalization(personalization);
        content = new sendGrid.Content("text/html", forward);
        mail.addContent(content);

        substitution = new sendGrid.Substitution("%content%", data);
        personalization.addSubstitution(substitution);

        var requestBody = mail.toJSON();
        var request = sg.emptyRequest();
        request.method = 'POST';
        request.path = '/v3/mail/send';
        request.body = requestBody;
        sg.API(request, function (response) {
          console.log(response.statusCode);
          console.log(response.body);
          res.status(200).send({"message": "Forward complete."});
        });
      }
    });
  }
  else res.status(400).send({"message": "email, data and type cannot be empty"});
};
