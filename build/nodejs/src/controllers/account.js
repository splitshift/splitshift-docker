var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var fs = require('fs');

var User = mongoose.model('User');
var Notification = mongoose.model('Notification');
var Activity = mongoose.model('Activity');
var Job = mongoose.model('Job');
var Chat = mongoose.model('Chat');
var Image = mongoose.model('Image');
var File = mongoose.model('File');
var Request = mongoose.model('Request');
var ApplyJob = mongoose.model('ApplyJob');
var Resource = mongoose.model('Resource');
var Feed = mongoose.model('Feed');

var tools = require('./tools');

var config = require('../config');
//sendgrid
var sendGrid = require('sendgrid').mail;
var sg = require('sendgrid').SendGrid(config.sendgrid.api_key);

module.exports.deleteAccount = function(req, res) {
  var user = req.user;
  var key = user.key;

  User.findOne({key: user.key}, function(err, user) {
    if(err) res.status(400).send(err);
    else if(user) {
      User.remove({key: user.key}, function(err) {
        if(err) res.status(400).send(err);
        else {
          User.update({
            $or: [
              {following: user.key},
              {followers: user.key},
              {connected: user.key}
            ]
          }, {
            $pull: {
              followers: user.key,
              following: user.key,
              connected: user.key
            }
          }, {multi: true}, function(err) {
            if(err) console.error(err);
          });
          fs.stat('public'+user.profile_image, function(err) {
            if(!err) fs.unlink('public'+user.profile_image);
          });
          if(!(/template/.test(user.banner))) {
            fs.stat('public'+user.banner, function(err) {
              if(!err) fs.unlink('public'+user.banner);
            });
          }
          Notification.remove({'from.key': key}, function() {
            if(err) console.error(err);
          });
          //update comment and reply
  				Activity.find({key: key}, function(err, activities) {
  					if(err) res.status(400).send({"message": err});
  					else {
  						activities.forEach(function(act, index) {
  							var query = {$pull:{}};
  							if(act.activity.resource_id) {
  								Resource.findById(act.activity.resource_id, function(err, resource) {
                    if(resource) {
    									query.$pull['comments'] = {_id: act.activity.comment_id};
    									Resource.update({_id: act.activity.resource_id}, query, function(err) {
    										if(err) console.error(err);
    									});
                    }
  								});
  							}
  							else {
  								Feed.findOne({_id: act.activity.feed_id}, function(err, feed) {
                    if(feed) {
    									if(act.activity.reply_id) {
                        var comment_ind = tools.findArray(feed.comments, act.activity.comment_id);
    										query.$pull['comments.'+comment_ind+'.replies'] = {_id: act.activity.reply_id};
    									}
    									else query.$pull['comments'] = {_id: act.activity.comment_id};
    									Feed.update({_id: act.activity.feed_id}, query, function(err) {
    										if(err) console.error(err);
    									});
                    }
  								});
  							}
  						});
              Activity.remove({key: key}, function(err) {
                if(err) console.error(err);
              });
  					}
  				});
          Job.remove({'owner.key': key}, function(err) {
            if(err) console.error(err);
          });
          Chat.remove({users: key}, function(err) {
            if(err) console.error(err);
          });
          Request.remove({'from.key': key}, function(err) {
            if(err) console.error(err);
          });
          ApplyJob.remove({$or:[{'from.key': key},{'to.key': key}]}, function(err) {
            if(err) console.error(err);
          });
          Resource.remove({'owner.key': key}, function(err) {
            if(err) console.error(err);
          });
          Feed.remove({'owner.key': key}, function(err) {
            if(err) console.error(err);
          });
          Image.find({owner: key}, function(err, images) {
            if(err) console.error(err);
            images.forEach(function(image) {
              try {
                fs.statSync(image.path);
                console.log('exists');
                fs.unlink(image.path);
              }
              catch(err) {
                console.log('does not exists');
              }
            });
            Image.remove({owner: key}, function(err) {
              if(err) console.error(err);
            });
          });
          File.find({owner: key}, function(err, files) {
            if(err) console.error(err);
            files.forEach(function(file) {
              try {
                fs.statSync(file.path);
                console.log('exists');
                fs.unlink(file.path);
              }
              catch(err) {
                console.log('does not exists');
              }
            });
            File.remove({owner: key}, function(err) {
              if(err) console.error(err);
            });
          });
          User.update({type: 'Employer'}, {$pull: {'profile.employees': key, 'profile.formers': key}},function(err) {
            if(err) console.error(err);
          });
          res.status(200).send({"message": "User has been removed."})
        }
      });
    }
    else res.status(400).send({"message": "invalid key"});
  });

};

module.exports.changePassword = function(req, res) {
  var user = req.user;
  var old_password = req.body.old_password;
  var new_password = req.body.new_password;

  User.findOne({key: user.key}, function(err, user) {
    if(err) res.status(400).send(err);
    else if(user) {
      if(bcrypt.compareSync(old_password, user.password)) {
        user.password = bcrypt.hashSync(new_password, bcrypt.genSaltSync(8));
        user.save(function(err) {
          if(err) res.status(400).send(err);
          else res.status(200).send({"message": "change password complete."});
        });
      }
      else res.status(400).send({"message": "old password is incorrect"});
    }
    else res.status(400).send({"message": "invalid key"});
  });
};

module.exports.confirmEmail = function(req, res) {
  var token = req.params.token;

  if(token) {
    jwt.verify(token, config.jwt.secret, function(err, decoded) {
      if (err) {
				if(err.name == 'JsonWebTokenError') {
          res.status(400).send({"message": "Token invalid"});
        }
        else {
          res.status(400).send({"message": "Token is expired"});
        }
			}
			else if(decoded) {
        if(decoded.method == 'confirm') {
  			  User.findOne({key: decoded.user_key}, function(err, user) {
            if(err) res.status(400).send(err);
            else if(user) {
              user.update({confirm: true}, function(err) {
                if(err) res.status(400).send(err);
                // else res.redirect(config.redirect_url);
                else {
                  var token = jwt.sign({key: user.key, type: user.type}, config.jwt.secret, { expiresIn : '2h' });
                  var user_json = { token:token, type:user.type, key:user.key };
                  user_json = JSON.stringify(user_json);
                  res.render('index', {
                    user: user_json
                  });
                }
              });
            }
            else res.status(400).send({"message": "User not found."});
          });
        }
        else res.status(400).send({"message": "Token invalid!!"});
	    }
			else {
        res.status(400).send({"message": "Token invalid!!"});
			}
    });
  }
  else {
    res.status(400).send({"message": "Token invalid!!"});
  }
};

module.exports.putEmail = function(req, res) {
  var user = req.user;
  var email = req.body.email;

  if(email == user.email) return res.status(400).send({"message": "This email is your old email."});
  User.findOne({email: email}, function(err, dup) {
    if(err) res.status(400).send(err);
    else if(dup) res.status(400).send({"message": "This email alraedy used."});
    else {
      User.findById(user._id, function(err, user) {
        if(err) res.status(400).send(err);
        else if(user) {
          user.update({email: email}, function(err) {
            if(err) res.status(400).send(err);
            else res.status(200).send({"message": "Change primary email complete."});
          });
        }
        else res.status(400).send({"message": "User not found."});
      });
    }
  });
};

module.exports.forgotPassword = function(req, res) {
  var email = req.body.email;

  User.findOne({email: email}, function(err, user) {
    if(err) res.status(400).send(err);
    else if(user) {
      fs.readFile('./views/forgot_password_mail.html', 'utf8', function(err, data) {
        if(err) console.error("File not found." + err);
        else {
          mail = new sendGrid.Mail();
          email = new sendGrid.Email("info@splitshift.com", "Splitshift");
          mail.setFrom(email);
          mail.setSubject("Change password");

          personalization = new sendGrid.Personalization();
          email = new sendGrid.Email(user.email);
          personalization.addTo(email);

          substitution = new sendGrid.Substitution("%firstName%", user.profile.first_name);
          personalization.addSubstitution(substitution);
          substitution = new sendGrid.Substitution("%lastName%", user.profile.last_name);
          personalization.addSubstitution(substitution);
          var token = jwt.sign({user_key: user.key, method: 'forgot'}, config.jwt.secret);
          substitution = new sendGrid.Substitution("%url%", config.forgot_url+token);
          personalization.addSubstitution(substitution);

          mail.addPersonalization(personalization);
          content = new sendGrid.Content("text/html", data);
          mail.addContent(content);

          var requestBody = mail.toJSON();
          var request = sg.emptyRequest();
          request.method = 'POST';
          request.path = '/v3/mail/send';
          request.body = requestBody;
          sg.API(request, function (response) {
            console.log(response.statusCode);
            console.log(response.body);
            res.status(200).send({"message": "Please check your email."});
          });
        }
      });
    }
    else res.status(400).send({"message": "User not found."});
  });
};

module.exports.changeForgotPassword = function(req, res) {
  var token = req.params.token;
  var password = req.body.password;

  if(token) {
    jwt.verify(token, config.jwt.secret, function(err, decoded) {
      if (err) {
				if(err.name == 'JsonWebTokenError') {
          res.status(400).send({"message": "Token invalid"});
        }
        else {
          res.status(400).send({"message": "Token is expired"});
        }
			}
			else if(decoded) {
        if(decoded.method == 'forgot') {
  			  User.findOne({key: decoded.user_key}, function(err, user) {
            if(err) res.status(400).send(err);
            else if(user) {
                user.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8));
                user.save(function(err) {
                  if(err) res.status(400).send(err);
                  else res.status(200).send({"message": "change password complete."});
                });
            }
            else res.status(400).send({"message": "User not found."});
          });
        }
        else res.status(400).send({"message": "Token invalid!!"});
	    }
			else {
        res.status(400).send({"message": "Token invalid!!"});
			}
    });
  }
  else {
    res.status(400).send({"message": "Token invalid!!"});
  }
};
