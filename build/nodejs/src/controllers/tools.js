var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var config = require('../config');

module.exports.verifyToken = function(token, res, callback) {
	if(token) {
    jwt.verify(token, config.jwt.secret, function(err, decoded) {
	    if (err) {
				if(err.name == 'JsonWebTokenError') {
          res.status(400).send({"message": "Token invalid"});
          return callback("Token invalid", null);
        }
        else {
          res.status(400).send({"message": "Token is expired"});
          return callback("Token is expired", null);
        }
			}
			else if(decoded) {
			  var now = new Date();
			  var refresh_time = new Date(decoded.exp * 1000);
			  refresh_time.setMinutes(refresh_time.getMinutes() - 45);

				if(now >= refresh_time) {
		      var newToken = jwt.sign({key: decoded.key, type: decoded.type}, config.jwt.secret, {expiresIn: '2h'});
		      res.set({'Token': newToken});
				}
				return callback(null, decoded);
	    }
			else {
				return callback("User not found", null);
			}
		});
	}
	else {
		res.status(400).send({'message': 'The token has not found.'});
		return callback('The token has not found.', null);
	}
};

module.exports.verifyKey = function(key, res, callback) {
	User.findOne({key:key}, function(err, user) {
		if(!user) {
			res.status(400);
			res.send({"message" : "Key is invalid."});
			return callback("Key is invalid.",null);
		}
		else {
			return callback(null, user);
		}
	});
};

module.exports.updateData = function(key, res, update, callback) {
	User.update({key:key}, update, function(err) {
		if(err) {
			res.status(400);
			res.send({"message":err});
			return callback(err);
		}
		else {
			return callback(null);
		}
	});
};

module.exports.checkNull = function() {
	for(var i=0;i<arguments.length;i++) {
		if(!arguments[i] || arguments[i] === "" || arguments[i] === []) {
			return false;
		}
	}
	return true;
};

module.exports.saveUser = function(newUser, res, callback) {
  console.log('Save user.');
	User.findOne({
		$or: [
			{email: newUser.email},
			{type: 'Employer', 'profile.company_name': newUser.profile.company_name}
		]
	}, function(err, user) {
		if(err) return callback(err);
		if(user) {
			if(user.email == newUser.email) {
				res.status(409).send({"message": "Email address already taken."});
				return callback("Email address already taken.");
			}
			else if(user.profile.company_name == newUser.profile.company_name) {
				res.status(409).send({"message": "Company name already taken."});
				return callback("Company name already taken.");
			}
		}
		else {
			newUser.save(function(err) {
				if(err) {
					return callback(err);
				}
				else {
					return callback(null);
				}
			});
		}
	});
};

module.exports.findArray = function(array, id) {
	console.log('Find Array.');
	for(var i=0;i<array.length;i++) {
		if(array[i]._id.equals(id)) {
			break;
		}
	}
	return i;
};

module.exports.findKeyArray = function(array, key) {
	console.log("Find value Array.");
  for(i=0;i<array.length;i++) {
    if(array[i].toString() == key.toString()) {
      break;
    }
  }
  return i;
};
