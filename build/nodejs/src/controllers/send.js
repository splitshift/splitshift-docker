var mongoose = require('mongoose');
var Subscribe = mongoose.model('Subscribe');
var config = require('../config');

var sendGrid = require('sendgrid').mail;
var sg = require('sendgrid').SendGrid(config.sendgrid.api_key);

module.exports.sendEmailToSubscribe = function(req, res) {
  var html = req.body.html;
  var subject = req.body.subject;

  if(html && subject) {
    Subscribe.distinct('email', function(err, user) {
      if(err) {
        res.status(400);
        res.send({"message":err});
      }
      else {
        var request;
        var i=0;
        res.status(200).send({"message": "Send email to all subcriber."});
        sendMail(i, user, subject, html);
      }
    });
  }
  else {
    res.status(400).send({"message": "Subject and html cannot be empty."});
  }
};

var sendMail = function(i, user, subject, html) {
  if(user[i]) {
    mail = new sendGrid.Mail();
    email = new sendGrid.Email("info@splitshift.com", "Splitshift");
    mail.setFrom(email);
    mail.setSubject(subject);
    //personalization <= 1000/request
    console.log('round '+(i/1000+1));
     do {
      personalization = new sendGrid.Personalization();
      email = new sendGrid.Email(user[i]);
      personalization.addTo(email);
      mail.addPersonalization(personalization);
      console.log(i+':'+user[i]);
      i++;
    } while(i % 1000 !== 0 && user[i]);
    content = new sendGrid.Content("text/html", html);
    mail.addContent(content);

    var requestBody = mail.toJSON();
    var request = sg.emptyRequest();
    request.method = 'POST';
    request.path = '/v3/mail/send';
    request.body = requestBody;
    sg.API(request, function (response) {
      console.log(response.statusCode);
      console.log(response.body);
      sendMail(i, user, subject, html);
    })
  }
};

module.exports.sendEmailToSingle = function(req, res) {
  var html = req.body.html;
  var subject = req.body.subject;
  var email = [req.body.email];

  if(html && subject) {
    Subscribe.distinct('email', function(err, user) {
      if(err) {
        res.status(400);
        res.send({"message":err});
      }
      else {
        var request;
        var i=0;
        res.status(200).send({"message": "Send email to all subcriber."});
        sendMail(i, email, subject, html);
      }
    });
  }
  else {
    res.status(400).send({"message": "Subject and html cannot be empty."});
  }
};
