var mongoose = require('mongoose');
var User = mongoose.model('User');
var Album = mongoose.model('Album');

var jwt = require('jsonwebtoken');
var tools = require('../tools');
var checkNull = tools.checkNull;
var config = require('../../config');

module.exports.postAlbum = function(req, res) {
  console.log('Post Album.');
  var json = req.body;
  if(checkNull(json.name, json.photos)){
    var user = req.user;
    var New = new Album({
      name : json.name,
      photos : json.photos,
      date : new Date()
    });
    for(var i=0;i<json.photos.length;i++) {
      var j = tools.findArray(user.photos, json.photos[i]);
      if(j>=user.photos.length) {
        res.status(400).send({"message":"There are one or more photos that aren't your photos."});
        break;
      }
      if(checkNull(user.photos[j].album_id)) {
        res.status(400).send({"message":"Ther are one or more photos that already are in other album."});
        break;
      }
      user.photos[j].album_id = New._id;
    }
    if(i>=json.photos.length) {
      console.log(New);
      user.albums.push(New);
      tools.updateData(user.key, res, {albums:user.albums, photos:user.photos}, function(err) {
        if(!err) {
          res.status(200).send({"message":"Create Album complete."});
        }
      });
    }
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.putAlbum = function(req, res) {
  console.log('Put Album.');
  var json = req.body;
  if(checkNull(json.name, json.photos)){
    var user = req.user;
    var id = req.params.id;
    var album = tools.findArray(user.albums, id);
    var j, k;
    if(album<user.albums.length) {
      for(j=0;j<user.photos.length;j++) {
        k = tools.findKeyArray(json.photos, user.photos[j]._id);
        if(k>=json.photos.length) {
          if(user.photos[j].album_id.equals(user.albums[album]._id)) {
            user.photos[j].album_id = "";
          }
        }
        else {
          user.photos[j].album_id = user.albums[album]._id;
        }
      }
      var New = new Album({
        _id : user.albums[album]._id,
        name : name,
        photos : json.photos,
        date : user.albums[album].date
      });
      user.albums[album] = New;
      tools.updateData(user.key, res, {albums:user.albums, photos:user.photos}, function(err) {
        if(!err) {
          res.status(200).send({"message":"Update Album complete."});
        }
      });
    }
    else {
      res.status(400).send({"message":"Wrong album ID."});
    }
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.deleteAlbum = function(req, res) {
  console.log("Delete Album.");
  var id = req.params.id;
  var user = req.user;
  var i = tools.findArray(user.albums, id);
  if(i<user.albums.length) {
    for(j=0;j<user.photos.length;j++) {
      if(checkNull(user.photos[j].album_id)) {
        if(user.photos[j].album_id.equals(user.albums[i]._id)) {
          user.photos[j].album_id = "";
        }
      }
    }
    user.albums.splice(i, 1);
    tools.updateData(user.key, res, {albums:user.albums, photos:user.photos}, function(err) {
      if(!err) {
        res.status(200).send({"message":"Delete Album complete."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Wrong album ID."});
  }
};

module.exports.getAlbum = function(req, res) {
  console.log("Get Album.");
  var json = req.query;
  if(checkNull(json.user_key)) {
    User.findOne({key:json.user_key}, function(err, user) {
      if(checkNull(user)) {
        if(checkNull(user.albums)) {
          var data = {"albums":[]};
          for(var i=0;i<user.albums.length;i++) {
            data.albums[i] = {
              _id : user.albums[i]._id,
              name : user.albums[i].name,
              date : user.albums[i].date
            };
          }
          res.status(200).send(data);
        }
        else {
          res.status(200).send({"albums":[]});
        }
      }
      else {
        res.status(400).send({"message":"User not found."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.getPhotoAlbum = function(req, res) {
  console.log("Get Photo in Album.");
  var json = req.query;
  var token = req.headers.authorization;
  if(token) {
    try {
      var decoded = jwt.verify(token, config.jwt.secret);
      if(decoded) {
        var now = new Date();
        var refresh_time = new Date(decoded.exp * 1000);
        refresh_time.setMinutes(refresh_time.getMinutes() - 45);

        if(now >= refresh_time) {
          var newToken = jwt.sign({key: decoded.key, type: decoded.type}, config.jwt.secret, {expiresIn: '2h'});
          res.set({'Token': newToken});
        }
        req.user = decoded;
      }
      else {
        return res.status(400).send({"message": "User not found"});
      }
    } catch(err) {console.log(err);
      if(err.name == 'JsonWebTokenError') {
        return res.status(400).send({"message": "Token invalid"});
      }
      else {
        return res.status(400).send({"message": "Token is expired"});
      }
    }
	}

  if(checkNull(json.user_key)) {
    User.findOne({key:json.user_key}, function(err, user) {
      if(checkNull(user)) {
        var id =req.params.id;
        var i = tools.findArray(user.albums, id);
        var k,j;
        if(i<user.albums.length) {
          var photo = user.albums[i];
          var data = {photos:[]};
          for(j=0;j<photo.photos.length;j++) {
            k = tools.findArray(user.photos, photo.photos[j]);
            if(k<user.photos.length) {
              if(checkPrivacyPhoto(user, req.user, user.photos[k].privacy)) {
                data.photos.push(user.photos[k]);
              }
            }
          }
          res.status(200).send(data);
        }
        else {
          res.status(400).send({"message":"Wrong album ID."});
        }
      }
      else {
        res.status(400).send({"message":"User not found."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.getPhoto = function(req, res) {
  console.log("Get Photo.");
  var json = req.query;
  var token = req.headers.authorization;
  if(token) {
    try {
      var decoded = jwt.verify(token, config.jwt.secret);
      if(decoded) {
        var now = new Date();
        var refresh_time = new Date(decoded.exp * 1000);
        refresh_time.setMinutes(refresh_time.getMinutes() - 45);

        if(now >= refresh_time) {
          var newToken = jwt.sign({key: decoded.key, type: decoded.type}, config.jwt.secret, {expiresIn: '2h'});
          res.set({'Token': newToken});
        }
        req.user = decoded;
      }
      else {
        return res.status(400).send({"message": "User not found"});
      }
    } catch(err) {console.log(err);
      if(err.name == 'JsonWebTokenError') {
        return res.status(400).send({"message": "Token invalid"});
      }
      else {
        return res.status(400).send({"message": "Token is expired"});
      }
    }
	}

  if(checkNull(json.user_key)) {
    User.findOne({key:json.user_key}, function(err, user) {
      if(checkNull(user)) {
        var photo = user.photos;
        var data = {photos:[]};
        for(var i=0;i<photo.length;i++) {
          if(checkPrivacyPhoto(user, req.user, photo[i].privacy)) {
            data.photos.push(photo[i]);
          }
        }
        res.status(200).send(data);
      }
      else {
        res.status(400).send({"message":"User not found."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

var checkPrivacyPhoto = function(user, me, privacy) {
  console.log("Check permission.");
  if(me) {
    var i = tools.findKeyArray(user.connected, me.key);
    if(user.key == me.key) {
      return true;
    }
    else if(privacy == "public" || privacy == "timeline") {
      return true;
    }
    else if(privacy == "employer" && me.type == "Employer") {
      return true;
    }
    else if(privacy == "employee" && user.type == "Employer") {
      var j = tools.findKeyArray(user.profile.employees, me.key);
      if(j<user.profile.employees.length) {
        return true;
      }
      else {
        return false;
      }
    }
    else if(privacy == "connected" && i<user.connected.length) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    if(privacy == "public" || privacy == "timeline") {
      return true;
    }
    return false;
  }
};
