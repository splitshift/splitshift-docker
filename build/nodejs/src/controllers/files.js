var multer = require('multer');
var fs = require('fs');
var path = require('path');
var mongoose = require('mongoose');
var File = mongoose.model('File');

var tools = require('./tools');
var maxSize = 2 * 1024 * 1024;//byte

module.exports.postFile = function(req, res) {
  var update = new File();
  var filename = update._id;
  var dir = 'public/uploads/files';
  var type = "";
  if(!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  var storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, dir);
    },
    filename: function(req, file, cb) {
      update.original_name = file.originalname;
      type = path.extname(file.originalname);
      cb(null, filename+type);
    }
  });
  var upload = multer({
    storage:storage,
    limits: { fileSize: maxSize }
  }).single('file');
  upload(req, res, function(err) {
    if(err) {
      res.status(400).send({"message":"Upload file is error."});
    }
    else {
      if(req.file) {
        update.path = dir+'/'+filename+type;
        update.owner = req.user.key;
        update.save(function(err) {
          if(err) {
            res.status(400).send({"message":err});
          }
          else {
            res.status(201).send(update);
          }
        });
      }
      else {
        res.status(400).send({"message":"File's not found."});
      }
    }
  });
};

module.exports.deleteFile = function(req, res) {
	var json = req.body;
  File.find({path:{$in:json.files}}, function(err, user) {
    if(err) {
      res.status(400).send({"message":"File isn't found."});
    }
    else if(tools.checkNull(user)){
      for(var i=0;i<user.length;i++) {
        fs.unlink(user[i].path);
      }
      File.remove({path:{$in:json.files}}, function(err) {
        if(err) {
          res.status(400).send({"message":err});
        }
        else {
          res.status(200).send({"message":"The work has complete."});
        }
      });
    }
    else {
      res.status(400).send({"message":"File's not found."});
    }
  });
};
