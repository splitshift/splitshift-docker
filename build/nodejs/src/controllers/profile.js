var mongoose = require('mongoose');
var User = mongoose.model('User');
var Job = mongoose.model('Job');
var Request = mongoose.model('Request');
var jwt = require('jsonwebtoken');

var tools = require('./tools');
var verifyKey = tools.verifyKey;
var findArray = tools.findArray;
var config = require('../config');

module.exports.getProfile = function(req, res) {
  console.log('Get user.');
  var token = req.headers.authorization;
  var key = req.params.id;
  var own = false;

  try {
    own = jwt.verify(token, config.jwt.secret);
  }
  catch(err) {
    own = false;
  }
  var following, connected;

  User.findOne({key:key}, function(err, user) {
    if(!user) {
      res.status(404).send({"message":"User not found."});
    }
    else {
			var data;
      if(user.key == own.key) own = false;
      if(user.type == "Employer") {
        Job.find({'owner.key': key, activated: true}, {owner: 0}, {sort:{date: -1}}, function(err, jobs) {
          User.find({
            $or: [
              {key: {$in: user.profile.employees}},
              {key: {$in: user.profile.formers}}
            ]
          }, {
            "profile.first_name": 1,
            "profile.last_name": 1,
            profile_image: 1,
            email: 1,
            birth_day: 1,
            key: 1
          }, function(err, users) {
            var employees = [];
            var formers = [];
            //filter employees and formers
            users.forEach(function(user_tmp) {
              if(user.profile.employees.indexOf(user_tmp.key) !== -1)
                employees.push(user_tmp);
              else if(user.profile.formers.indexOf(user_tmp.key) !== -1)
                formers.push(user_tmp);
            });

            if(own) following = own?(user.followers.indexOf(own.key) == -1?true:false):false;

            var review = [];
            user.profile.review.forEach(function(rev) {
              if(user.profile.employees.indexOf(rev.from.key) !== -1)
                rev.from['type'] = 'employee';
              else
                rev.from['type'] = 'former';
              review.push(rev);
            });

            data = {
              first_name : user.profile.first_name,
              last_name : user.profile.last_name,
              company_name : user.profile.company_name,
              profile_image : user.profile_image,
              banner : user.banner,
              address : user.address,
              type : user.profile.type,
              overview : user.profile.overview,
              gallery : user.gallery,
              jobs : jobs,
              links : user.profile.links,
              culture : user.profile.culture,
              benefits : user.profile.benefits,
              video : user.profile.video,
              review : review,
              overall : user.profile.overall,
              key : user.key,
              employees : employees,
              formers : formers,
              following : following,
              tel : user.profile.tel,
              contact_email : user.profile.contact_email,
              is_approved: user.is_approved,
              email: user.email
            };
            res.status(200).send(data);
          });
        });
      }
      else if(user.type == 'Employee') {
        Request.findOne({
          $or: [
            {to: key, 'from.key': own.key},
            {to: own.key, 'from.key': key}
          ]
        }, function(err, request) {
          if(err) res.status(400).send({"message": err});
          else {
            user.profile.experiences.sort(function(a, b) {
              return b.start_date - a.start_date;
            });
            user.profile.educations.sort(function(a, b) {
              return b.start_date - a.start_date;
            });
            user.profile.courses.sort(function(a, b) {
              return b.start_date - a.start_date;
            });
            if(request) var requesting = 'requesting';

            if(own) {
              following = own?(user.followers.indexOf(own.key) == -1?true:false):false;
              connected = own?(requesting?requesting:(user.connected.indexOf(own.key) == -1?true:false)):false;
            }

            data = {
              first_name : user.profile.first_name,
              last_name : user.profile.last_name,
              profile_image : user.profile_image,
              banner : user.banner,
              occupation : user.profile.occupation,
              address : user.address,
              about : user.profile.about,
              interests : user.profile.interests,
              experiences : user.profile.experiences,
              educations : user.profile.educations,
              skills : user.profile.skills,
              languages : user.profile.languages,
              courses : user.profile.courses,
              rank : user.profile.rank,
              key: user.key,
              birth_day: user.profile.birth_day,
              gender: user.profile.gender,
              following: following,
              connected: connected,
              connected_count: user.connected.length,
              is_approved: user.is_approved,
              email: user.email,
              rank: user.profile.rank
            };
            res.status(200).send(data);
          }
        });
      }
      else {
        data = {
          company_name : user.profile.company_name,
          profile_image : user.profile_image,
          banner : user.banner
        };
        res.status(200).send(data);
      }
    }
  });
};
