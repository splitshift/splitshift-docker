var mongoose = require('mongoose');
var User = mongoose.model('User');
var Chat = mongoose.model('Chat');

var tools = require('./tools');
var checkNull = tools.checkNull;

module.exports.postChat = function(req, res) {
  console.log('postChat');
  var key = req.user.key;
  var name = req.user.profile.company_name?req.user.profile.company_name:req.user.profile.first_name+' '+req.user.profile.last_name;
  var json = req.body;

  if(checkNull(json.to, json.message, json.type)) {
    if(key == json.to) res.status(400).send({"message": "Cannot send message to yourself."});
    else tools.verifyKey(json.to, res, function(err, user) {
      if(user) {
        var newMessage = {
          message: json.message,
          message_type: json.type,
          date: new Date(),
          owner: key
        };
        if(json.type == 'file') newMessage.path = json.path;
        var seen = {};
        seen[key] = true;
        seen[user.key] = false;
        console.log('seen:',seen);
        Chat.update({
          $and: [
            {users: {$in: [key]}},
            {users: {$in: [user.key]}}
          ]
        },
        {
          $setOnInsert: {users: [key, user.key]},
          $push: { messages: newMessage },
          seen: seen
        }, {
          upsert: true
        }, function(err) {
          if(err) res.status(400).send({"message": err});
          else res.status(201).send(newMessage);
        });
      }
    });
  }
  else res.status(400).send({"message": "Data missing."});
};

module.exports.getAllChat = function(req, res) {
  var key = req.user.key;
  var result = [];

  Chat.find({users: key}, function(err, chats) {
    if(err) res.status(400).send({"message": err});
    else {
      var users = [];
      chats.forEach(function(chat) {
        if(chat.users[0] == key) users.push(chat.users[1]);
        else users.push(chat.users[0]);
      });
      User.find({key:{$in: users}}, function(err, users) {
        if(err) res.status(400).send({"message": err});
        else {
          var ind;
          chats.forEach(function(chat) {
            if(chat.users[0] == key) ind = 1;
            else ind = 0;
            for(var i=0;i<users.length;i++) {
              if(users[i].key == chat.users[ind]) {
                chat.users[ind] = {
                  key: users[i].key,
                  name: users[i].profile.company_name?users[i].profile.company_name:users[i].profile.first_name+' '+users[i].profile.last_name,
                  profile_image: users[i].profile_image
                };
                chat.messages = chat.messages.slice(-1);
                break;
              }
            }
            result.push(chat);
          });
          result.sort(function(a, b) {
            return b.messages[0].date - a.messages[0].date;
          });
          res.status(200).send(result);
        }
      });
    }
  });
};

module.exports.getChat = function(req, res) {
  var key = req.user.key;
  var query = req.query.chat_id?{_id: req.query.chat_id,users: key}:{$and: [{users: {$in: [key]}},{users: {$in: [req.query.key]}}]};
  var update = {};
  update['seen.'+key] = true;

  Chat.findOneAndUpdate(query, update, {new: true}, function(err, chat) {
    if(err) res.status(400).send({"message": err});
    else if(!chat) res.status(200).send([]);
    else groupMessage(res, chat);
  });
};

groupMessage = function(res, chat) {
  User.findOne({key: chat.users[0]},function(err, user) {
    if(err) res.status(400).send({"message": err});
    else {
      var owner1 = {
        key: user.key,
        name: user.profile.company_name?user.profile.company_name:user.profile.first_name+' '+user.profile.last_name,
        profile_image: user.profile_image || ''
      };
      User.findOne({key: chat.users[1]},function(err, user) {
        if(err) res.status(400).send({"message": err});
        else {
          var owner2 = {
            key: user.key,
            name: user.profile.company_name?user.profile.company_name:user.profile.first_name+' '+user.profile.last_name,
            profile_image: user.profile_image || ''
          };
          var result = {
            _id: chat._id,
            users: [owner1, owner2],
            messages: chat.messages,
            seen: chat.seen
          };
          result.messages = [];

          var group = [];
          chat.messages.forEach(function(message, index, arr) {
            if(index-1 >= 0) {
              if(message.owner == arr[index-1].owner) {
                group.push(message);
              }
              else {
                result.messages.push(group);
                group = [];
                group.push(message);
              }
            }
            else {
              group.push(message);
            }
          });
          result.messages.push(group);
          return res.status(200).send(result);
        }
      });
    }
  });
};
