var mongoose = require('mongoose');
var User = mongoose.model('User');
var Resource = mongoose.model('Resource');
var CommentResource = mongoose.model('CommentResource');
var Activity = mongoose.model('Activity');
var Notification = mongoose.model('Notification');

var tools = require('./tools');
var checkNull = tools.checkNull;

module.exports.postResource = function(req, res) {
  console.log('post resource');
  var user = req.user;
  var json = req.body;

  if(checkNull(json.featured_video) ^ checkNull(json.featured_image)) {
    if(checkNull(json.title, json.category, json.department)) {
      var newResource = new Resource({
        title: json.title,
        short_description: json.short_description,
        content: json.content,
        featured_image: json.featured_image,
        featured_video: json.featured_video,
        category: json.category,
        department: json.department,
        owner: {
          key: user.key,
          name: user.profile.company_name?user.profile.company_name:user.profile.first_name+' '+user.profile.last_name,
          profile_image: user.profile_image,
          user_type: user.type
        },
        date: new Date()
      });
      if(user.type == 'Admin') {
        newResource.is_approved = true;
      }
      newResource.save(function(err, updatedResource) {
        if(err) res.status(400).send(err);
        else res.status(201).send(updatedResource);
      });
    }
    else res.status(400).send({"message": "Title, category and department cannot be blank."});
  }
  else res.status(400).send({"message": "You must choose featured image or featured video."});
};

module.exports.getAllResource = function(req, res) {
  var user = req.user;

  Resource.find({'owner.key': user.key}, {
    _id: 1,
    title: 1,
    category: 1,
    department: 1,
    owner: 1,
    date: 1,
    is_approved: 1
  }, {sort: {date: -1}}, function(err, resouces) {
    if(err) res.status(400).send(err);
    else {
      res.status(200).send(resouces);
    }
  });
};

module.exports.getResource = function(req, res) {
  Resource.aggregate([
    {
      $match: { is_approved: true }
    },
    {
      $sort: { date: -1 }
    },
    {
      $group: {
        _id: '$category',
        data: {$push: '$$CURRENT'}
      }
    }
  ], function(err, resouces) {
    if(err) res.status(400).send(err);
    else {
      var result = {};
      resouces.forEach(function(resouce) {
        result[resouce._id] = resouce.data.slice(0, 4);
      });
      Resource.find({ is_approved: true }, {}, {sort: {date: -1}, limit: 4}, function(err, lastest) {
        result['lastest'] = lastest;
        res.status(200).send(result);
      });
    }
  });
};

module.exports.getSingleResource = function(req, res) {
  console.log('get single resource');
  var token = req.headers.authorization;
  var id = req.params.id;

  Resource.findOne({_id: id}, function(err, resource) {
    if(err) res.status(400).send(err);
    else if(resource) {
      if(token) {
        tools.verifyToken(token, res, function(err, decoded) {
          if(!err) {
            if(decoded) {
              if(decoded.type == 'Admin' || resource.owner.key == decoded.key || resource.is_approved) {
                return res.status(200).send(resource);
              }
            }
            return res.status(400).send({"message": "resource not found."});
          }
        });
      }
      else {
        if(resource.is_approved) return res.status(200).send(resource);
        return res.status(400).send({"message": "resource not found."});
      }
    }
    else return res.status(400).send({"message": "resource not found."});
  });
};

module.exports.putResource = function(req, res) {
  console.log('put resource');
  var json = req.body;
  var user = req.user;
  var id = req.params.id;

  if(checkNull(json.featured_video) ^ checkNull(json.featured_image)) {
    if(checkNull(json.title, json.category, json.department)) {
      Resource.findById(id, function(err, resource) {
        if(err) res.status(400).send(err);
        else if(!resource) res.status(400).send({"message": "Resource id invalid."});
        else if(user.key !== resource.owner.key) res.status(400).send({"message": "You cannot edit this resource."});
        else {
          json['featured_video'] = json.featured_video;
          json['featured_image'] = json.featured_image;
          resource.update(json, function(err) {
            if(err) res.status(400).send(err);
            else res.status(200).send({"message": "Resource has been updated."});
          });
        }
      });
    }
    else res.status(400).send({"message": "Title, category and department cannot be blank."});
  }
  else res.status(400).send({"message": "You must choose featured image or featured video."});
};

module.exports.deleteResource = function(req, res) {
  console.log('delete resource');
  var user = req.user;
  var id = req.params.id;

  Resource.findById(id, function(err, resource) {
    if(err) res.status(400).send(err);
    else if(!resource) res.status(400).send({"message": "Resource id invalid."});
    else if(user.type !== 'Admin' && user.key !== resource.owner.key) res.status(400).send({"message": "You cannot remove this resource."});
    else {
      resource.remove(function(err) {
        if(err) res.status(400).send(err);
        else res.status(200).send({"message": "Resource has been removed."});
      });
      Activity.remove({'activity.resource_id': id}, function(err) {
        if(err) console.error(err);
      });
      Notification.remove({'resource.resource_id': id}, function(err) {
        if(err) console.error(err);
      });
    }
  });
};

module.exports.getSearchResource = function(req, res) {
  var keyword = req.query.keyword;
  if(keyword) {
    keyword = keyword.replace(/\s+/g,'|');
    keyword = new RegExp(keyword,'gi');
  }
  else keyword = new RegExp('','gi');

  var category = req.query.category?new RegExp(req.query.category,'gi'):new RegExp('','gi');
  var department = req.query.department?new RegExp(req.query.department,'gi'):new RegExp('','gi');

  Resource.find({
    $or: [
      {title: keyword},
      {short_description: keyword}
    ],
    category: category,
    department: department,
    is_approved: true
  }, {}, {sort: {date: -1}}, function(err, resources) {
    if(err) res.status(400).send(err);
    else res.status(200).send(resources);
  });
};

module.exports.postComment = function(req, res) {
  var user = req.user;
  var id = req.params.id;
  var json = req.body;

  if(json.comment) {
    comment = new CommentResource({
      comment: json.comment,
      owner: {
        key: user.key,
        name: user.profile.company_name?user.profile.company_name:user.profile.first_name+' '+user.profile.last_name,
        profile_image: user.profile_image,
        user_type: user.type
      },
      date: new Date()
    });
    Resource.findById(id, function(err, resource) {
      if(err) res.status(400).send(err);
      else if(!resource) res.status(400).send({"message": "Reosurce id invalid."});
      else {
        Resource.update({_id: id}, {$push: {comments: comment}}, function(err) {
          if(err) res.status(400).send(err);
          else {
            var activity = new Activity({
              key: user.key,
              activity: {
                resource_id: id,
                comment_id: comment._id
              }
            });
            activity.save(function(err) {
              if(err) console.error(err);
            });

            if(user.key !== resource.owner.key) {
              var newNoti = new Notification({
                from : {
                  key: user.key,
                  name: user.profile.company_name?user.profile.company_name:user.profile.first_name+' '+user.profile.last_name,
                  profile_image: user.profile_image,
                  user_type: user.type
                },
                to : resource.owner.key,
                notification_type: 'commentResource',
                resource: {
                  resource_id: id,
                  comment_id: comment._id
                },
                date: new Date()
              });
              newNoti.save(function(err) {
                if(err) console.error(err);
              });
            }

            res.status(201).send(comment);
          }
        });
      }
    });
  }
  else res.status(400).send({"message": "Missing comment field."});
};

module.exports.deleteComment = function(req, res) {
  var user = req.user;
  var resource_id = req.params.id;
  var comment_id = req.params.comment_id;

  Resource.findById(resource_id, function(err, resource) {
    if(err) res.status(400).send(err);
    else if(!resource) res.status(400).send({"message": "Resource id invalid."})
    else {
      var is_owner = false;
      resource.comments.forEach(function(comment) {
        if(comment._id == comment_id && comment.owner.key == user.key) is_owner = true;
      });

      if(is_owner || resource.owner.key == user.key) {
        Resource.update({_id: resource_id}, {$pull: {comments: {_id: mongoose.Types.ObjectId(comment_id)}}}, function(err) {
          if(err) res.status(400).send(err);
          else {
            Activity.remove({'activity.comment_id': comment_id}, function(err) {
              if(err) console.error(err);
            });
            Notification.remove({'resource.comment_id': comment_id}, function(err) {
              if(err) console.error(err);
            });
            res.status(200).send({"message": "Delete comment complete."});
          }
        });
      }
      else res.status(400).send({"message": "You cannot delete this comment."});
    }
  });
};

module.exports.putLike = function(req, res) {
  var user = req.user;
  var id = req.params.id;

  Resource.findById(id, function(err, resource) {
    if(err) res.status(400).send(err);
    else if(!resource) res.status(400).send({"message": "Resource id invalid."})
    else {
      var update = {$push: {like: user.key}};
      var mesg = 'Like this resource.';
      var like = true;
      resource.like.forEach(function(key) {
        if(key == user.key) {
          update = {$pull: {like: user.key}};
          mesg = 'Unlike this resource.';
          like = false;
        }
      });

      if(like) {
        if(user.key !== resource.owner.key) {
          var newNoti = new Notification({
            from : {
              key: user.key,
              name: user.profile.company_name?user.profile.company_name:user.profile.first_name+' '+user.profile.last_name,
              profile_image: user.profile_image,
              user_type: user.type
            },
            to : resource.owner.key,
            notification_type: 'likeResource',
            resource: {
              resource_id: id
            },
            date: new Date()
          });
          newNoti.save(function(err) {
            if(err) console.error(err);
          });
        }
      }
      else {
        Notification.remove({
          'from.key': user.key,
          notification_type: 'likeResource',
          'resource.resource_id': id
        }, function(err) {
          if(err) console.error(err);
        });
      }
      Resource.update({_id: id}, update, function(err) {
        if(err) res.status(400).send(err);
        else res.status(200).send({"message": mesg});
      });
    }
  });
};
