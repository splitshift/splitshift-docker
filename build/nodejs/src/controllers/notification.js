var mongoose = require('mongoose');
var Notification = mongoose.model('Notification');

module.exports.getNotification = function(req, res) {
  var user = req.user;

  Notification.find({to: user.key}, {}, {sort: {date: -1}}, function(err, notis) {
    if(err) res.status(400).send({"message": err});
    else res.status(200).send(notis);
  });
};

module.exports.putNotification = function(req, res) {
  var user = req.user;

  Notification.update({to: user.key}, {seen: true}, {multi: true}, function(err) {
    if(err) res.status(400).send({"message": err});
    else res.status(200).send({"message": "Notification has seen."});
  });
};

module.exports.delNotification = function(req, res) {
  var id = req.params.id;
  var user = req.user;

  Notification.findOne({_id: id, to: user.key}, function(err, noti) {
    if(err) res.status(400).send(err);
    else if(noti) {
      noti.remove(function(err) {
        if(err) res.status(400).send(err);
        else res.status(200).send({"message": "delete notification complete."});
      });
    }
    else res.status(400).send({"message": "notification not found."});
  });

};
