var mongoose = require('mongoose');
var User = mongoose.model('User');
var Feed = mongoose.model('Feed');
var Reply = mongoose.model('Reply');
var Activity = mongoose.model('Activity');
var Notification = mongoose.model('Notification');

var tools = require('../tools');
var checkNull = tools.checkNull;

module.exports.postReply = function(req, res) {
  console.log("Post Reply.");
  var json = req.body;

  if(checkNull(json.feed_id, json.comment_id, json.user_key, json.reply)) {
    User.findOne({key:json.user_key}, function(err, user) {
      if(checkNull(user)) {
          Feed.findOne({_id: json.feed_id, 'owner.key': json.user_key}, function(err, feed) {
            if(err) res.status(400).send(err);
            else if(feed) {
              var i = tools.findArray(feed.comments, json.comment_id);
              if(i < feed.comments.length) {
                var New = new Reply({
                  reply : json.reply,
                  owner : {
                  key: req.user.key,
                  name: req.user.profile.company_name?req.user.profile.company_name:req.user.profile.first_name+' '+req.user.profile.last_name,
                  profile_image: req.user.profile_image,
                  user_type: req.user.type
                },
                date: new Date()
              });

              Feed.update({
                _id: json.feed_id, 'comments._id': mongoose.Types.ObjectId(json.comment_id)
              }, {
                $push: {'comments.$.replies': New}
              }, function(err) {
                if(err) res.status(400).send(err);
                else {
                  var activity = new Activity({
                    key: req.user.key,
                    activity: {
                      owner: json.user_key,
                      feed_id: json.feed_id,
                      comment_id: json.comment_id,
                      reply_id: New._id
                    }
                  });
                  activity.save(function(err) {
                    if(err) console.error(err);
                  });
                  if(req.user.key !== json.user_key) {
                    var newNoti = new Notification({
                      from : {
                        key: req.user.key,
                        name: req.user.profile.company_name?req.user.profile.company_name:req.user.profile.first_name+' '+req.user.profile.last_name,
                        profile_image: req.user.profile_image,
                        user_type: req.user.type
                      },
                      to : json.user_key,
                      notification_type: 'reply',
                      feed: {
                        owner: json.user_key,
                        feed_id: json.feed_id,
                        comment_id: json.comment_id,
                        reply_id: New._id
                      },
                      date: new Date()
                    });
                    newNoti.save(function(err) {
                      if(err) console.error(err);
                    });
                  }
                  res.status(201).send(New);
                }
              });
            }
            else {
              res.status(400).send({"message": "Comment not found."});
            }
          }
          else {
            res.status(400).send({"message": "Feed not found."});
          }
        });
      }
      else {
        res.status(400).send({"message":"User not found."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.deleteReply = function(req, res) {
  console.log("Delete Reply.");
  var json = req.body;

  if(checkNull(json.feed_id, json.user_key, json.comment_id)) {
    User.findOne({key:json.user_key}, function(err, user) {
      if(checkNull(user)) {
        Feed.findOne({_id: json.feed_id, 'owner.key': json.user_key}, function(err, feed) {
          if(err) res.status(400).send(err);
          else if(feed) {
            var i = tools.findArray(feed.comments, json.comment_id);
            if(i < feed.comments.length) {
              var id = req.params.id;
              var j = tools.findArray(feed.comments[i].replies, id);
              if(j < feed.comments[i].replies.length) {
                if(req.user.key==user.key||req.user.key==feed.comments[i].owner.key||req.user.key==feed.comments[i].replies[j].owner.key) {
                  var pull = {};
                  pull['comments.'+i+'.replies'] = {_id: mongoose.Types.ObjectId(id)};
                  Feed.update({
                    _id: json.feed_id, 'owner.key': json.user_key
                  }, {
                    $pull: pull
                  }, function(err) {
                    if(err) res.status(400).send(err);
                    else {
                      Activity.remove({'activity.reply_id': id}, function(err) {
                        if(err) res.status(400).send(err);
                        else res.status(200).send({"message": "Delete reply complete."});
                      });
                      Notification.remove({'feed.reply_id': id}, function(err) {
                        if(err) console.error(err);
                      });
                    }
                  });
                }
                else {
                  res.status(400).send({"message": "Only feed owner, this comment owner or this reply owner can delete."});
                }
              }
              else {
                res.status(400).send({"message": "Reply not found."});
              }
            }
            else {
              res.status(400).send({"message": "Comment not found."});
            }
          }
          else {
            res.status(400).send({"message": "Feed not found."});
          }
        });
      }
      else {
        res.status(400).send({"message":"User not found."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};
