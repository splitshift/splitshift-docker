var mongoose = require('mongoose');
var User = mongoose.model('User');
var Feed = mongoose.model('Feed');
var Comment = mongoose.model('Comment');
var Activity = mongoose.model('Activity');
var Notification = mongoose.model('Notification');

var tools = require('../tools');
var checkNull = tools.checkNull;

module.exports.postComment = function(req, res) {
  console.log("Post Comment.");
  var json = req.body;
  if(checkNull(json.feed_id, json.comment, json.user_key)) {
    User.findOne({key:json.user_key}, function(err, user) {
      if(checkNull(user)) {
        Feed.findOne({_id: json.feed_id, 'owner.key': json.user_key}, function(err, feed) {
          if(err) res.status(400).send(err);
          else if(feed) {
            var New = new Comment({
              comment : json.comment,
              owner : {
                key: req.user.key,
                name: req.user.profile.company_name?req.user.profile.company_name:req.user.profile.first_name+' '+req.user.profile.last_name,
                profile_image: req.user.profile_image,
                user_type: req.user.type
              },
              date: new Date()
            });

            Feed.update({_id: json.feed_id, 'owner.key': json.user_key}, {$push: {comments: New}}, function(err) {
              if(err) res.status(400).send(err);
              else {
                var activity = new Activity({
                  key: req.user.key,
                  activity: {
                    owner: json.user_key,
                    feed_id: json.feed_id,
                    comment_id: New._id
                  }
                });
                activity.save(function(err) {
                  if(err) console.error(err);
                });
                if(req.user.key !== json.user_key) {
                  var newNoti = new Notification({
                    from : {
                      key: req.user.key,
                      name: req.user.profile.company_name?req.user.profile.company_name:req.user.profile.first_name+' '+req.user.profile.last_name,
                      profile_image: req.user.profile_image,
                      user_type: req.user.type
                    },
                    to : json.user_key,
                    notification_type: 'comment',
                    feed: {
                      owner: json.user_key,
                      feed_id: json.feed_id,
                      comment_id: New._id
                    },
                    date: new Date()
                  });
                  newNoti.save(function(err) {
                    if(err) console.error(err);
                  });
                }
                res.status(201).send(New);
              }
            });
          }
          else res.status(400).send({"message": "Feed not found."});
        });
      }
      else {
        res.status(400).send({"message":"User not found."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.deleteComment = function(req, res) {
  console.log("Delete Comment.");
  var id = req.params.id;
  var json = req.body;

  if(checkNull(json.feed_id, json.user_key)) {
    User.findOne({key:json.user_key}, function(err, user) {
      if(err) res.status(400).send(err);
      else if(user) {
        Feed.findOne({_id: json.feed_id, 'owner.key': json.user_key}, function(err, feed) {
          if(err) res.status(400).send(err);
          else if(feed) {
            var i = tools.findArray(feed.comments, id);
            if(i < feed.comments.length) {
              if(req.user.key==feed.owner.key||req.user.key==feed.comments[i].owner.key) {
                Feed.update({_id: json.feed_id, 'owner.key': json.user_key}, {
                  $pull: {comments: {_id: mongoose.Types.ObjectId(id)}}
                }, function(err) {
                  if(err) res.status(400).send(err);
                  else {
                    Activity.remove({'activity.comment_id': id}, function(err) {
                      if(err) console.error(err);
                    });
                    Notification.remove({'feed.comment_id': id}, function(err) {
                      if(err) console.error(err);
                    });
                    res.status(200).send({"message": "Delete comment complete."});
                  }
                });
              }
              else {
                res.status(400).send({"message": "Only feed owner or this comment owner can delete."});
              }
            }
            else {
              res.status(400).send({"message": "Comment not found."});
            }
          }
          else {
            res.status(400).send({"message": "Feed not found."});
          }
        });
      }
      else {
        res.status(400).send({"message": "User not found."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};
