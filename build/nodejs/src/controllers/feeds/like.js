var mongoose = require('mongoose');
var User = mongoose.model('User');
var Feed = mongoose.model('Feed');
var Notification = mongoose.model('Notification');

var tools = require('../tools');
var checkNull = tools.checkNull;

module.exports.putLike = function(req, res) {
  console.log("Post Like Feed");
  var json = req.body;
  var id = req.params.id;

  if(checkNull(json.user_key)) {
    User.findOne({key:json.user_key}, function(err, user) {
      if(checkNull(user)) {
        var id = req.params.id;
        Feed.findOne({_id: id, 'owner.key': json.user_key}, function(err, feed) {
          if(err) res.status(400).send(err);
          else if(feed) {
            var i = tools.findKeyArray(feed.likes, req.user.key);
            if(i==feed.likes.length) {
              feed.likes.push(req.user.key);
              var like = true;
            }
            else {
              feed.likes.splice(i, 1);
              var like = false;
            }
            Feed.update({_id: id}, {likes: feed.likes}, function(err) {
              if(err) res.status(400).send({"message":"Update error."});
              else if(like) {
                res.status(200).send({"message":"Like complete."});
                if(req.user.key !== user.key) {
                  var newNoti = new Notification({
                    from : {
                      key: req.user.key,
                      name: req.user.profile.company_name?req.user.profile.company_name:req.user.profile.first_name+' '+req.user.profile.last_name,
                      profile_image: req.user.profile_image,
                      user_type: req.user.type
                    },
                    to : user.key,
                    notification_type: 'like',
                    feed: {
                      owner: user.key,
                      feed_id: feed._id
                    },
                    date: new Date()
                  });
                  newNoti.save(function(err) {
                    if(err) console.error(err);
                  });
                }
              }
              else {
                Notification.remove({
                  'from.key': req.user.key,
                  notification_type: 'like',
                  'feed.feed_id': feed._id
                }, function(err) {
                  if(err) console.error(err);
                });
                res.status(200).send({"message":"Unlike complete."});
              }
            });
          }
          else {
            res.status(400).send({"message": "Feed not found."});
          }
        });
      }
      else {
        res.status(400).send({"message":"User not found."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};
