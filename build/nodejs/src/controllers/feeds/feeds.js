var mongoose = require('mongoose');
var User = mongoose.model('User');
var Feed = mongoose.model('Feed');
var Photo = mongoose.model('Photo');
var Image = mongoose.model('Image');
var Notification = mongoose.model('Notification');

var tools = require('../tools');
var checkNull = tools.checkNull;

checkPrivacy = function(user, me) {
  var privacy = ['public', 'timeline'];

  if(me) {
    if(user.key == me.key)
      privacy = ['public', 'timeline', 'employee', 'employer', 'connected', 'onlyme'];
    if(user.profile.employees) if(user.profile.employees.indexOf(me.key) !== -1)
      privacy.push('employee');
    if(me.type == 'Employer')
      privacy.push('employer');
    if(user.connected.indexOf(me.key) !== -1)
      privacy.push('connected');
  }

  return privacy;
};

module.exports.getFeed = function(req, res) {
  console.log('Get Feed.');
  var is_public = req.query.is_public || false;
  var feed_id = req.query.feed_id;
  var key = req.params.key;
  var result = [];
  var token = req.headers.authorization;
  var privacy;
  var page = req.query.page || 0;

  tools.verifyKey(key, res, function(err, user) {
    if(err) return console.error('38', err);
    if(token) {
      tools.verifyToken(token, res, function(err, decoded) {
        if(err) return console.error('41', err);
        if(decoded) {
          tools.verifyKey(decoded.key, res, function(err, me) {
            if(err) return console.error('44', err);
            if(me) {
              privacy = checkPrivacy(user, me);

              if(err) res.status(400).send({"message": err});
              else if(feed_id) {
                Feed.findOne({_id: feed_id, privacy: {$in: privacy}}, function(err, feed) {
                  if(err) res.status(400).send(err);
                  else {
                    if(feed) res.status(200).send(feed);
                    else res.status(400).send({"message": "Feed not found."});
                  }
                });
              }
              else if(me.key != key || is_public) {
                Feed.find({
                  'owner.key': key, privacy: {$in: privacy}
                }, {}, {
                  sort: {date: -1},
                  skip: page*10,
                  limit: 10
                }, function(err, feeds) {
                  if(err) res.status(400).send(err);
                  else res.status(200).send(feeds);
                });
              }
              else {
                Feed.find({
                  $or: [
                    {'owner.key': key}
                    // {
                    //   'owner.key': {$in: user.following},
                    //   privacy: 'public'
                    // }
                  ]
                }, {}, {
                  sort: {date: -1},
                  skip: page*10,
                  limit: 10
                }, function(err, feeds) {
                  if(err) res.status(400).send(err);
                  else res.status(200).send(feeds);
                });
              }
            }
          });
        }
      });
    }
    else {
      privacy = checkPrivacy(user, null);

      Feed.find({
        'owner.key': key, privacy: {$in: privacy}
      }, {}, {
        sort: {date: -1},
        skip: page*10,
        limit: 10
      }, function(err, feeds) {
        if(err) res.status(400).send(err);
        else res.status(200).send(feeds);
      });
    }
  });
};

module.exports.postFeed = function(req, res) {
  console.log('Post Feed.');
  var user = req.user;
  var json = req.body;

  if((checkNull(json.text)||checkNull(json.photos)||checkNull(json.video)) && checkNull(json.privacy)) {
    if(!json.text||!/\S/.test(json.text)) {
      if(json.photos) {
        if(!json.photos.length && !json.video)
          return res.status(400).send({"message": "text cannot be empty."});
      }
      else {
        if(!json.photos && !json.video)
          return res.status(400).send({"message": "text cannot be empty."});
      }
    }

    if(!checkNull(json.photos,json.video)) {
      var photos = [];

      if(json.photos) {
        json.photos.forEach(function(photo) {
      	  if(photo) {
      			photos.push(photo);
      		}
      	});
      }
      var New = new Feed({
        text: json.text,
        photos: photos,
        video: json.video,
        privacy: json.privacy,
        date: new Date(),
        owner : {
          key: req.user.key,
          name: req.user.profile.company_name?req.user.profile.company_name:req.user.profile.first_name+' '+req.user.profile.last_name,
          profile_image: req.user.profile_image,
          user_type: req.user.type
        }
      });
      Image.count({path:{$in:photos}}, function(err, count) {
        if(err) {
          res.status(400).send({"message":err});
        }
        else {
          if(count!==photos.length){
            res.status(400).send({"message":"Some photos is missing in database."});
          }
          else {
            New.save(function(err) {
              if(err) res.status(400).send(err);
              else {
                var id = New._id;
                if(checkNull(photos)) {
                  for(var i=0;i<photos.length;i++) {
                    var new_photo = new Photo({
                      path: photos[i],
                      feed_id: id,
                      privacy: json.privacy,
                      date: new Date()
                    });
                    user.photos.unshift(new_photo);
                  }
                  tools.updateData(user.key, res, {photos:user.photos}, function(err) {
                    if(!err) {
                      res.status(201).send(New);
                    }
                  });
                }
                else {
                  tools.updateData(user.key, res, {photos:user.photos}, function(err) {
                    if(!err) {
                      res.status(201).send(New);
                    }
                  });
                }

                if(New.privacy == 'public' || New.privacy == 'timeline') {
                  user.followers.forEach(function(key) {
                    var newNoti = new Notification({
                      from : {
                        key: user.key,
                        name: user.profile.company_name?user.profile.company_name:user.profile.first_name+' '+user.profile.last_name,
                        profile_image: user.profile_image,
                        user_type: user.type
                      },
                      to : key,
                      notification_type: 'feed',
                      feed: {
                        owner: user.key,
                        feed_id: New._id
                      },
                      date: new Date()
                    });
                    newNoti.save(function(err) {
                      if(err) console.error(err);
                    });
                  });
                }
              }
            });
          }
        }
      });
    }
    else {
      res.status(400);
      res.send({"message":"Can't post photo and video in same feed."});
    }
  }
  else {
    res.status(400);
    res.send({"message":"Data is missing."});
  }
};

module.exports.putFeedPrivacy = function(req, res) {
  console.log("Put Feed Privacy.");
  var json = req.body;

  if(checkNull(json.privacy)) {
    var user = req.user;
    var id = req.params.id;

    Feed.findById(id, function(err, feed) {
      if(err) res.status(400).send(err);
      else if(feed) {
        if(feed.owner.key !== user.key) return res.status(400).send({"message": "you cannot edit feed privacy."});
        feed.update({privacy: json.privacy}, function(err) {
          if(err) res.status(400).send(err);
          else {
            for(var j=0;j<user.photos.length;j++) {
              if(user.photos[j].feed_id.equals(id)) {
                user.photos[j].privacy = json.privacy;
              }
            }
            User.update({key:user.key}, {photos:user.photos}, function(err) {
              if(err) {
                res.status(400).send({"message":"Update error."});
              }
              else {
                res.status(200).send({"message":"Update privacy complete."});
              }
            });
          }
        });
      }
      else res.status(400).send({"message": "feed not found."});
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.putFeed = function(req, res, next) {
  console.log('Put Feed.');
  var json = req.body;
  var user = req.user;
  var id = req.params.id;console.log(json);

  if((checkNull(json.text)||checkNull(json.new_photos)||checkNull(json.video))&&checkNull(json.privacy)) {
    if(json.text) {
      if(!/\S/.test(json.text)) return res.status(400).send({"message": "text cannot be empty."});
    }

    Feed.findById(id, function(err, feed) {
      if(err) res.status(400).send(err);
      else if(feed) {
        if(feed.owner.key !== user.key) return res.status(400).send({"message": "you cannot edit feed."});
        if(!checkNull(feed.video, json.new_photos)||!checkNull(feed.photos, json.video)) {
          console.log("update image privacy");
          feed.text = json.text;
          feed.video = json.video?json.video:null;
          var j, k;
          if(feed.privacy != json.privacy) {
            for(j=0;j<user.photos.length;j++) {
              if(user.photos[j].feed_id.equals(id)) {
                user.photos[j].privacy = json.privacy;
              }
            }
          }
          feed.privacy = json.privacy;
          console.log("delete image");
          if(checkNull(json.deleted_photos)) {
            for(j=0;j<json.deleted_photos.length;j++) {
              k = tools.findKeyArray(feed.photos, json.deleted_photos[j]);
              if(k<feed.photos.length) {
                feed.photos.splice(k, 1);
              }
            }
          }
          console.log("upload new image");
          if(checkNull(json.new_photos)) {
            var photos = [];

          	json.new_photos.forEach(function(photo, idx) {
          	  if(photo) {
          			photos.push(photo);
          		}
          	});
            feed.photos = feed.photos.concat(photos);
            Image.count({path:{$in:photos}}, function(er, count) {
              if(er) {
                res.status(400).send({"message":er});
              }
              else if(count==photos.length){
                for(j=0;j<photos.length;j++) {
                  New = new Photo({
                    path: photos[j],
                    feed_id: id,
                    privacy: json.privacy
                  });
                  user.photos.unshift(New);
                }
                tools.updateData(user.key, res, {photos:user.photos}, function(err) {
                  if(!err) {
                    feed.update(feed, function(err) {
                      if(err) {
                        res.status(400).send({"message":"Update error."});
                      }
                    });
                    deletePhoto(json.deleted_photos, user, id, req, res, next);
                  }
                });
              }
              else {
                res.status(400).send({"message":"Some photos missing in database."});
              }
            });
          }
          else {
            console.log("update user");
            User.update({key:user.key}, {photos:user.photos}, function(err) {
              if(err) {
                res.status(400).send({"message":"Update error."});
              }
              else {
                feed.update(feed, function(err) {
                  if(err) {
                    res.status(400).send({"message":"Update error."});
                  }
                });
                deletePhoto(json.deleted_photos, user, id, req, res, next);
              }
            });
          }
        }
        else {
          res.status(400).send({"message":"You can't show video and picture in a post."});
        }
      }
      else res.status(400).send({"message": "feed not found."});
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.deleteFeed = function(req, res, next) {
  console.log('Delete Feed.');
  var json = req.body;
  var user = req.user;
  var id = req.params.id;

  Feed.findById(id, function(err, feed) {
    if(err) res.status(400).send(err);
    else if(feed) {
      if(feed.owner.key !== user.key) return res.status(400).send({"message": "you cannot delete feed."});
      var photo = feed.photos;
      feed.remove(function(err) {
        if(err) res.status(400).send(err);
        else {
          deletePhoto(photo, user, id, req, res, next);
        }
      });
    }
    else res.status(400).send({"message": "feed not found."});
  });
};

var deletePhoto = function(photo, user, id, req, res, next) {
  console.log("Delete photo from photos");
  if(checkNull(photo)) {
    req.body.images = photo.slice();
  }
  else  {
    photo = [];
  }
  var user_photos = user.photos.filter(function(userPhoto) {
    if(userPhoto.feed_id == id) {
      for(var i=0;i<photo.length;i++) {
        if(userPhoto.path==photo[i]) {
          if(checkNull(userPhoto.album_id)) {
            console.log("check album.");
            var album = tools.findArray(user.albums, userPhoto.album_id);
            var pic = tools.findKeyArray(user.albums[album].photos, userPhoto._id);
            user.albums[album].photos.splice(pic, 1);
          }
          photo.splice(i, 1);
          return false;
        }
      }
    }
    return true;
  });
  User.update({key:user.key}, {photos:user_photos, albums:user.albums}, function(err) {
    if(err) {
      res.status(400);
      res.send({"message":"Update error.photo and album isn't change."});
    }
    else {
      next();
    }
  });
};
