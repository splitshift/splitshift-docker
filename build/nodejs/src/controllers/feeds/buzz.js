var mongoose = require('mongoose');
var User = mongoose.model('User');
var Feed = mongoose.model('Feed');
var Photo = mongoose.model('Photo');
var Image = mongoose.model('Image');

var tools = require('../tools');
var checkNull = tools.checkNull;

module.exports.searchBuzz = function(req, res) {
  var keyword = req.query.keyword;
	if(keyword) keyword = keyword.replace(/\s+/g,'|');
	keyword = new RegExp(keyword,'gi');

  var page = req.query.page || 0;

  Feed.find({
    $or: [
      {text: keyword},
      {'owner.name': keyword}
    ],
    privacy: 'public'
  }, {}, {
    sort: {date: -1},
    skip: page*10,
    limit: 10
  }, function(err, feeds) {
    if(err) res.status(400).send(err);
    else res.status(200).send(feeds);
  });
};

module.exports.getBuzz = function(req, res) {
  console.log('Get Buzz');
  var feeds = [];
  var page = req.query.page || 0;

  Feed.find({privacy: 'public'}, {}, {
    sort: {date: -1},
    skip: page*10,
    limit: 10
  }, function(err, feeds) {
    if(err) res.status(400).send(err);
    else res.status(200).send(feeds);
  });
};

module.exports.deleteBuzz = function(req, res, next) {
  console.log('Delete buzz.');
  var id = req.params.id;

  Feed.findById(id, function(err, feed) {
    if(err) res.status(400).send(err);
    else if(feed) {
      if(feed.privacy != 'public') return res.status(400).send({"message":"Admin only delete buzz feed."});
      else {
        var photo = feed.photos;

        Feed.remove({_id: id}, function(err) {
          if(err) res.status(400).send(err);
          else {
            User.findOne({key: feed.owner.key},function(err, user) {
              deletePhoto(photo, user, id, req, res, next);
            });
          }
        });
      }
    }
    else {
      res.status(400).send({"message":"Feed not found."});
    }
  });
};

var deletePhoto = function(photo, user, id, req, res, next) {
  console.log("Delete photo from photos");
  if(checkNull(photo)) {
    req.body.images = photo.slice();
  }
  else  {
    photo = [];
  }
  var user_photos = user.photos.filter(function(userPhoto) {
    if(userPhoto.feed_id == id) {
      for(var i=0;i<photo.length;i++) {
        if(userPhoto.path==photo[i]) {
          if(checkNull(userPhoto.album_id)) {
            console.log("check album.");
            var album = tools.findArray(user.albums, userPhoto.album_id);
            var pic = tools.findKeyArray(user.albums[album].photos, userPhoto._id);
            user.albums[album].photos.splice(pic, 1);
          }
          photo.splice(i, 1);
          return false;
        }
      }
    }
    return true;
  });
  User.update({key:user.key}, {photos:user_photos, albums:user.albums}, function(err) {
    if(err) {
      res.status(400);
      res.send({"message":"Update error.photo and album isn't change."});
    }
    else {
      next();
    }
  });
};
