var Jimp = require('jimp');
var multer = require('multer');
var fs = require('fs');
var path = require('path');
var mongoose = require('mongoose');
var Image = mongoose.model('Image');

var resize = require('../../services/resize');

var tools = require('../tools');
var maxSize = 5 * 1024 * 1024;//byte

module.exports.postImage = function(req, res) {
	console.log('Post Image.');
  var update = new Image();
  var filename = update._id;
  var dir = 'public/images/feed';
  var type = "";
  if(!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  var storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, dir);
    },
    filename: function(req, file, cb) {
			type = path.extname(file.originalname);
      cb(null, filename+type);
    }
  });
  var upload = multer({
		storage: storage,
		limits: { fileSize: maxSize },
		fileFilter: function(req, file, cb) {
			if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
				return cb({"message": "Only image files are allowed!"});
			}
			cb(null, true);
		}
	}).single('avatar');
  upload(req, res, function(err) {
    if(err) {
      res.status(400).send(err);
    }
    else {
      if(req.file) {
				// update.path = dir+'/'+filename+type;
				update.path = req.file.path;
				update.owner = req.user.key;
				update.save(function(err) {
					if(err) {
						res.status(400).send(err);
					}
					else {
						resize(req.file.path, function(err) {
							if(!req.file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
								if(err)  return res.status(400).send(err);
							}
							res.status(201).send({"path":dir+'/'+filename+type});
						});
					}
				});
      }
      else {
        res.status(400).send({"message":"File's not found."});
      }
    }
  });
};

module.exports.deleteImage = function(req, res) {
	console.log('Delete Image.');
	var json = req.body;
  Image.find({path:{$in:json.images}}, function(err, user) {
    if(err) {
      res.status(400).send({"message":"Image isn't found."});
    }
    else if(tools.checkNull(user)){
      for(var i=0;i<user.length;i++) {
        fs.unlink(user[i].path);
      }
      Image.remove({path:{$in:json.images}}, function(err) {
        if(err) {
          res.status(400).send(err);
        }
        else {
          res.status(200).send({"message":"The work has complete."});
        }
      });
    }
    else {
      res.status(400).send({"message":"Image's not found."});
    }
  });
};
