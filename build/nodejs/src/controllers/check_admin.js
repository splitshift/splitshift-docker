var mongoose = require('mongoose');
var User = mongoose.model('User');

var tools = require('./tools');

module.exports = function(req, res) {
  var token = req.headers.authorization;

  tools.verifyToken(token, res, function(err, decoded) {
    if(decoded) {
      tools.verifyKey(decoded.key, res, function(err, user) {
        if(user) {
          return res.status(200).send((user.type == "Admin"));
        }
        res.status(400).send({message: "user not found!!"});
      });
    }
  });
};
