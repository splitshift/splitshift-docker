var mongoose = require('mongoose');
var User = mongoose.model('User');

var tools = require('../tools');

module.exports.getSearch = function(req, res) {
  console.log("Get Search.");
  var address = req.query.address;
  var keyword = req.query.keyword;
  User.find({}, function(err, user) {
    if(err) {
      res.status(400);
      res.send({"message":"Find error."});
    }
    else {
      var data = {people:[], companies:[], jobs:[]};
      for(var i=0;i<user.length;i++) {
        if(user[i].type=="Employer") {
          data = searchEmployer(user[i], data, address, keyword);
        }
        else {
          data = searchEmployee(user, user[i], data, address, keyword);
        }
      }
      res.status(200);
      res.send(data);
    }
  });
};

searchEmployer = function(user, data, address, keyword) {
  var information = "";
  if(user.profile.address) {
    information = user.profile.address.information;
  }
  if(strFind(user.profile.company_name, keyword) && strFind(information, address)) {
    var newCompany = {
      key : user.key,
      company_name : user.profile.company_name,
      address : information
    };
    data.companies.push(newCompany);
  }
  if(strFind(information, address)) {
    var jobs = user.profile.jobs;
    for(var i=0;i<jobs.length;i++) {
      if(strFind(jobs[i].position, keyword)) {
        var newJob = {
          _id : jobs[i]._id,
          position : jobs[i].position
        };
        data.jobs.push(newJob);
      }
    }
  }
  return data;
};

searchEmployee = function(all, user, data, address, keyword) {
  var information = "";
  if(user.profile.address) {
    information = user.profile.address.information;
  }
  var locationWork = false;
  var found;
  if(user.profile.occupation) {
    var i = tools.findArray(user.profile.experiences, user.profile.occupation);
    var work = user.profile.experiences[i];
    found = findWorkLocation(all, work.workplace);
    console.log(found);
    if(found) {
      locationWork = strFind(found, address);
    }
    console.log(locationWork);
  }
  if((strFind(user.profile.first_name, keyword) || strFind(user.profile.last_name, keyword)) && (strFind(information, address)||locationWork)) {
    var New = {
      key : user.key,
      first_name : user.profile.first_name,
      last_name : user.profile.last_name
    };
    data.people.push(New);
  }
  return data;
};

strFind = function(word, keyword) {
  if(!keyword) {
    return true;
  }
  else if(!word) {
    return false;
  }
  for(var i=0;i<word.length;i++) {
    if(word[i]==keyword[0]) {
      for(var j=0;j<keyword.length&i+j<word.length;j++) {
        if(word[i+j]!=keyword[j]) {
          break;
        }
        if(j==keyword.length-1) {
          return true;
        }
      }
    }
  }
  return false;
};

findWorkLocation = function(user, company) {
  console.log("Find work location.");
  console.log(user);
  for(var i=0;i<user.length;i++) {
    if(user[i].type != "Employer") {
      continue;
    }
    console.log(company+user[i].profile.company_name);
    if(company == user[i].profile.company_name) {
      console.log("Find.");
      if(user[i].profile.address) {
        console.log(user[i].profile.address.information);
        return user[i].profile.address.information;
      }
      else {
        return false;
      }
    }
  }
  return false;
};
