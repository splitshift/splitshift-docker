var mongoose = require('mongoose');
var User = mongoose.model('User');
var Job = mongoose.model('Job');

// var tools = require('../tools');
module.exports.getPeople = function(req, res) {
	var condition={};
	condition.$and=[];

	//type
	condition.$and.push({ 'type': 'Employee' });

	//keyword
	var keyword = req.query.keyword;
	if(keyword) {
		// keyword = keyword.replace(/\s+/g,'|');
		// keyword = new RegExp('[^\\w]*'+keyword+'[^\\w]*','gi');
		keyword = new RegExp('(\\s|^)'+keyword+'(\\s|$)', 'gi');
		condition.$and.push({$or:[
			{ 'profile.first_name': keyword },
			{ 'profile.last_name': keyword },
			{ 'profile.occupation.position': keyword },
			{ 'profile.occupation.workplace': keyword },
			{ 'profile.skills': keyword }
		]});
	}

	//location
	var location = req.query.location;
	if(location) {
		// location=location.replace(/\s+/g,'|');
		location=new RegExp(location,'gi');
		condition.$and.push({$or:[
			{ 'address.information': location },
			{ 'address.district': location },
			{ 'address.city': location },
			{ 'address.country': location },
			{ 'profile.occupation.address.address': location },
			{ 'profile.occupation.address.district': location },
			{ 'profile.occupation.address.city': location },
			{ 'profile.occupation.address.country': location }
		]});
	}

	//interest
	var interest = req.query.interest;
	if(interest) {
		condition.$and.push({ 'profile.interests.category' : interest });
	}

	//work_exp
	var work_exp = req.query.work_exp;
	work_exp = work_exp?JSON.parse(work_exp):[0,99];

	//page
	var page = req.query.page || 0;

	condition.$and.push({ confirm : true });

	//search
	search_people(condition,work_exp,page,function(err,users) {
		res.status(200).send(users);
	});
};

function search_people(condition,work_exp,page,callback) {
	console.log(work_exp);
	work_exp[0]=new Date(work_exp[0],0,0,0,0,0,0);
	work_exp[1]=new Date(work_exp[1],0,0,0,0,0,0);
	User.find(condition,{
			key: 1,
			'profile.first_name': 1,
			'profile.last_name': 1,
			'profile.occupation': 1,
			'profile.experiences': 1,
			profile_image: 1
		},
		{
			sort: { created: -1 },
			skip: 15*page,
			limit: 15
		}, function(err,users) { //search experience
			data=[];

			var fn=function countExp(user) {
				exp_time=0;
				user.profile.experiences.forEach(function(experience) {
					var end_date=experience.end_date?new Date(experience.end_date):new Date();
					exp_time+=end_date-new Date(experience.start_date);
				});
				exp_time=new Date(0,0,0,0,0,0,exp_time);
				if(work_exp[0]<=exp_time&&exp_time<=work_exp[1]) {
					data.push(user);
				}
				return new Promise(resolve => {resolve()});
			};
			var p1=Promise.all(users.map(fn)).then(function() {
				console.log(data.length);
				callback(err,data);
			});
		}
	);
}

module.exports.getJobs = function (req,res) {
	var condition={};
	condition.$and=[];

	//keyword
	var keyword = req.query.keyword;
	if(keyword) {
		// keyword = keyword.replace(/\s+/g,'|');
		keyword = new RegExp(keyword,'gi');
		condition.$and.push({$or:[
			// { 'description': keyword },
			{ 'position': keyword },
			{ 'owner.company_name': keyword }
		]});
	}

	//location
	var location = req.query.location;
	if(location) {
		// location=location.replace(/\s+/g,'|');
		location=new RegExp(location,'gi');
		condition.$and.push({$or:[
			{ 'owner.address.information': location },
			{ 'owner.address.district': location },
			{ 'owner.address.city': location },
			{ 'owner.address.country': location }
		]});
	}

	//distance
	var distance = req.query.distance;
	distance=distance?distance:Infinity;

	//point [lat,lng]
	var point = req.query.point;
	if(point) {
		point=JSON.parse(point);
		condition.$and.push({
			'owner.address.location': {
				$near: {
					$geometry: {
						type: "Point",
						coordinates: point
					},
					$maxDistance: distance
				}
			}
		});
	}

	//date
	var bounded_time=req.query.bounded_time;
	var day=86400000;
	if(bounded_time) {
		bounded_time=new Date()-bounded_time*day;
		condition.$and.push({ 'date' : { $gte: bounded_time } });
	}

	//company type
	var type = req.query.type;
	if(type) {
		condition.$and.push({'type': type});
	}

	//compensation
	var compensation = req.query.compensation;
	if(compensation) {
		compensation=new RegExp(compensation,'gi');
		condition.$and.push({ 'compensation' : compensation});
	}

	//employer
	var employer = req.query.employer;
	if(employer) {
		condition.$and.push({ 'owner.company_type' : employer });
	}

	condition.$and.push({ 'activated' : true });

	//{$and:[]} error nothing found
	if(condition.$and.length==0) condition={};

	//page
	var page = req.query.page || 0;

	//search
	search_jobs(condition,page,function(err,jobs) {
		res.status(200).send(jobs);
	});
}

function search_jobs(condition,page,callback) {
	Job.find(condition, {}, {
			sort: { date: -1 },
			skip: 10*page,
			limit: 10
		}, function(err, jobs) {
			callback(err, jobs);
		}
	);
}

module.exports.getCompanies = function(req,res) {
	var condition={};
	condition.$and=[];
	condition.$and.push({ 'type': 'Employer' });
	//keyword
	var keyword = req.query.keyword;
	if(keyword) {
		// keyword=keyword.replace(/\s+/g,'|');
		keyword=new RegExp(keyword,'gi');
		condition.$and.push({$or:[
			{ 'profile.company_name': keyword }
			// { 'profile.culture': keyword }
		]});
	}


	//location
	var location = req.query.location;
	if(location) {
		location=location.replace(/\s+/g,'|');
		location=new RegExp(location,'gi');
		condition.$and.push({$or:[
			{ 'address.information': location },
			{ 'address.district': location },
			{ 'address.city': location },
			{ 'address.country': location },
		]});
	}

	//type
	var type = req.query.type;
	if(type) {
		condition.$and.push({ 'profile.type' : type });
	}

	var overall=req.query.overall;
	if(overall) {
		condition.$and.push({ 'profile.overall': {
			$gt: parseInt(overall)-1,
			$lte: parseInt(overall)
		}});
	}

	condition.$and.push({ confirm : true });

	//page
	var page = req.query.page || 0;

	search_companies(condition,req.query.bounded_time,req.query.compensation,page,function(err,coms) {
		res.status(200).send(coms);
	});

}

function search_companies(condition,bounded_time,compensation,page,callback) {
	User.find(condition,{
		key: 1,
		'profile.company_name': 1,
		address: 1,
		profile_image: 1,
		'profile.openjobs': 1
	},{
			sort: { date: -1 },
			skip: 10*page,
			limit: 10
		}, function(err, coms) {
			var ps=[];
			var data=[];
			if(bounded_time || compensation) {
				//date
				bounded_time=bounded_time;
				var day=86400000;
				bounded_time=bounded_time?new Date()-bounded_time*day:new Date(0);

				//compensation
				compensation=new RegExp(compensation,'gi');

				coms.forEach(function(com) {
					ps.push(Job.find({$and:[
							{'owner.company_name':com.profile.company_name},
							{'date': { $gte: bounded_time }},
							{'compensation': compensation}
						]
					},{},{},function (err,jobs) {
						if(jobs.length) {
							data.push(com);
						}
					}).exec());
				});
			}
			else data = coms;

			Promise.all(ps).then(function() {
				callback(err,data);
			});
		}
	);
}

module.exports.getSearch=function (req,res) {
	//keyword
	var keyword = req.query.keyword;
	keyword=keyword?keyword.replace(/\s+/g,'|'):'.*';
	keyword=new RegExp(keyword,'gi');

	searchAll(keyword,function(err,data) {
		res.status(200).send(data);
	});
}
searchAll = function(keyword, callback) {
  data = {people:[], companies:[], jobs:[]};
  User.find({
    $and: [
      { type: 'Employee' },
      { $or: [
        { 'profile.first_name': keyword },
        { 'profile.last_name': keyword },
        { 'profile.occupation.position': keyword }
      ]}
    ]}, {
      key:1,
      'profile.first_name':1,
      'profile.last_name':1,
      'profile.occupation': 1,
      profile_image:1
    }, {
      sort: { created:-1 },
      limit: 10
    }, function(err, users) {
      data.people = users;
      User.find({
        type: 'Employer',
        'profile.company_name': keyword
      }, {
        key:1,
        'profile.company_name':1,
        address: 1,
        profile_image:1
      }, {
        sort: { created:-1 },
        limit: 10
      }, function(err, users) {
        data.companies = users;
        Job.find({
          $or: [
						{position: keyword},
						{'owner.company_name': keyword}
					]
        }, {}, {
          sort: { date: -1 },
          limit: 10
        }, function(err, jobs) {
          data.jobs = jobs;
          callback(err, data);
      });
    });
  });
};
