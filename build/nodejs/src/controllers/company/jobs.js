var mongoose = require('mongoose');
var User = mongoose.model('User');
var Job = mongoose.model('Job');
var ApplyJob = mongoose.model('ApplyJob');

var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;

module.exports.postJob = function(req, res) {
	console.log('Post Job.');
	var user = req.user;
	var company = req.company;
	var json = req.body;

	if(checkNull(json.position, json.type, json.compensation)) {
		var now = new Date();
		var expire_date = json.expire_date?new Date(json.expire_date):now.setDate(now.getDate()+90);
		expire_date.setHours(0,0,0,0);
		var newJob = new Job({
			position : json.position,
			description : json.description,
			type : json.type,
			min_year : json.min_year,
			required_skills : json.required_skills,
			compensation : json.compensation,
			expire_date : expire_date,
			urgent : json.urgent,
			owner: {
				company_name: company.profile.company_name,
				key: company.key,
				company_type: company.profile.type || '',
				address: company.address,
				profile_image: company.profile_image,
				user_type: company.type,
				contact_email: company.profile.contact_email
			},
			date: new Date()
		});
		newJob.save(function(err) {
			if(err) {
				res.status(400).send({"message":err});
			}
			else {
				User.update({key:company.key},{$inc:{'profile.openjobs': 1}},function(err) {
					if(err) res.status(400).send(err);
					else res.status(201).send(newJob);
				});
			}
		});
	}
	else {
		res.status(400).send({"message":"Data is missing."});
	}
};

module.exports.getAllJob = function(req, res) {
	var now = new Date();
	now.setHours(0,0,0,0);

	Job.update({'owner.key': req.company.key, expire_date: {$lt: now}}, {activated: false}, function(err) {
		if(err) res.status(400).send(err);
		else {
			Job.find({'owner.key': req.company.key}, {}, {sort:{date: -1}}, function(err, jobs) {
				if(err) res.status(400).send(err);
				else res.status(200).send(jobs);
			});
		}
	});
};

module.exports.getJob = function(req, res) {
	console.log('Get Job.');
	var key = req.company.key;
	var id = req.params.id;
	Job.findOne({
		'_id': id,
		'owner.key': key
	}, function(err, job) {
		if(err) res.status(400).send(err);
		else if(job) res.status(200).send(job);
		else res.status(404).send({"message": "ID of Job is invalid."});
	});
};

module.exports.putJob = function(req, res) {
	console.log('Put Job.');
	var key = req.company.key;
	var json = req.body;
	if(checkNull(json.position, json.type, json.compensation)) {
		var id = req.params.id;
		Job.update({'owner.key': key,_id: id}, {
			position : json.position,
			description : json.description,
			type : json.type,
			min_year : json.min_year,
			required_skills : json.required_skills,
			compensation : json.compensation,
			expire_date : json.expire_date
		}, function(err, result) {
			if(err) res.status(400).send({"message": "ID of Job is invalid."});
			else {
				ApplyJob.update({'job._id':id},
				{ job: {
						position : json.position,
						description : json.description,
						company_type : json.type,
						min_year : json.min_year,
						required_skills : json.required_skills,
						compensation : json.compensation,
						expire_date : json.expire_date
					}
				},{multi: true}, function(err) {
					if(err) console.error(err);
				});
				res.status(200).send({"message": "Job has been updated."});
			}
		});
	}
};

module.exports.deleteJob = function(req, res) {
	console.log('Delete Job.');
	var id = req.params.id;
	var key = req.company.key;
	Job.findOne({'owner.key': key, _id: id}, function(err, job) {
		if(err) res.status(400).send(err);
		var inc = {$inc:{'profile.openjobs': job.activated?-1:0}};
		Job.remove({'owner.key': key, _id: id}, function(err, removed) {
			if(err) res.status(400).send(err);
			else User.update({key:req.company.key},inc,function(err) {
				if(err) res.status(400).send(err);
				else res.status(200).send({"message":"Job has been deleted."});
			});
		});
	});
};

module.exports.putPin = function(req, res) {
	var id = req.params.id;
	var key = req.company.key;

	Job.findOne({'owner.key': key, _id: id}, function(err, job) {
		if(err) res.status(400).send(err);
		else {
			if(job.urgent) var msg = "Unpin this job.";
			else var msg = "Pin this job";
			job.urgent = !job.urgent;
			job.save(function(err) {
				if(err) res.status(400).send(err);
				else res.status(200).send({"message": msg});
			});
		}
	});
};

module.exports.putActivate = function(req, res) {
	var id = req.params.id;
	var key = req.company.key;

	Job.findOne({'owner.key': key, _id: id}, function(err, job) {
		if(err) res.status(400).send(err);
		else {
			if(job.activated) var msg = "Deactivate this job.";
			else var msg = "Activate this job";
			job.activated = !job.activated;
			job.save(function(err) {
				if(err) res.status(400).send(err);
				else User.update({key:req.company.key},{$inc:{'profile.openjobs': job.activated?1:-1}},function(err) {
					if(err) res.status(400).send(err);
					else res.status(200).send({"message": msg});
				});
			});
		}
	});
};
