var mongoose = require('mongoose');
var User = mongoose.model('User');
var Link = mongoose.model('Link');
var Job = mongoose.model('Job');
var Activity = mongoose.model('Activity');
var Notification = mongoose.model('Notification');
var Resource = mongoose.model('Resource');
var Feed = mongoose.model('Feed');
var ApplyJob = mongoose.model('ApplyJob');

//call tools
var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;

module.exports.getAll = function(req, res) {
	User.find({type: 'Employer'}, {key: 1, 'profile.company_name': 1}, {sort: {'profile.company_name': 1}}, function(err, users) {
		if(err) res.status(400).send(err);
		else res.status(200).send(users);
	});
};

module.exports.putInfo = function(req, res) {
	console.log("Put Info.");
	var key = req.user.key;
  var json = req.body;
	if(checkNull(json.company_name, json.type)) {
		var update = req.user.profile;
		update.first_name = json.first_name;
		update.last_name = json.last_name;
		update.company_name = json.company_name;
		update.type = json.type;
		User.update({key: key}, {profile: update}, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				Job.update({'owner.key':key}, {
					'owner.company_name': update.company_name,
					'owner.company_type': update.type
				}, { multi: true }, function(err) {
					Activity.find({key: key}, function(err, activities) {
						if(err) res.status(400).send({"message": err});
						else {
							//update comment and reply
							activities.forEach(function(act, index) {
								var query = {};
								if(act.activity.resource_id) {
									Resource.findById(act.activity.resource_id, function(err, resource) {
										if(resource) {
											var comment_ind = tools.findArray(resource.comments, act.activity.comment_id);
											query['comments.'+comment_ind+'.owner.name'] = update.company_name;
											Resource.update({_id: act.activity.resource_id}, query, function(err) {
												if(err) console.error(err);
											});
										}
									});
								}
								else {
									Feed.findOne({_id: act.activity.feed_id}, function(err, feed) {
										if(feed) {
											var comment_ind = tools.findArray(feed.comments, act.activity.comment_id);
											if(act.activity.reply_id) {
												var reply_ind = tools.findArray(feed.comments[comment_ind].replies, act.activity.reply_id);
												query['comments.'+comment_ind+'.replies.'+reply_ind+'.owner.name'] = update.company_name;
											}
											else query['comments.'+comment_ind+'.owner.name'] = update.company_name;
											Feed.update({_id: act.activity.feed_id}, query, function(err) {
												if(err) console.error(err);
											});
										}
									});
								}
							});
						}
					});
					//update feed
					Feed.update({'owner.key': key},{'owner.name': update.company_name},{multi: true}, function(err) {
						if(err) console.error(err);
					});
					//update noti
					Notification.update({'from.key': key},{'from.name': update.company_name},{multi: true}, function(err) {
						if(err) console.error(err);
					});
					//update resource
					Resource.update({'owner.key': key},{'owner.name': update.company_name},{multi: true}, function(err) {
						if(err) console.error(err);
					});
					//update job and applyjob
					ApplyJob.update({'to.key': key}, {'to.name': update.company_name},{multi: true}, function(err) {
						if(err) console.error(err);
					});
					res.status(200).send({"message":"Info has been updated."});
				});
			}
		});
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.putAddress = function(req, res) {
	console.log("Put Address.");
	var key = req.user.key;
  var json = req.body;
	if(checkNull(json.contact_email, json.tel, json.information, json.lat, json.lng)) {
		var contact_email = json.contact_email;
		var tel = json.tel;
		var address = {
			information: json.information,
			description: json.description,
			district: json.district,
			city: json.city,
			country: json.country,
			zip_code: json.zip_code,
			location: [json.lng, json.lat]
		};
		User.update({key: key}, {
			'profile.contact_email': contact_email,
			'profile.tel': tel,
			address: address
		}, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				Job.update({'owner.key':key}, {
					'owner.address': address,
					'owner.contact_email': contact_email
				}, {multi: true}, function(err) {
					res.status(200).send({"message":"Address has been updated."});
				});

				User.find({type: 'Employee'}, function(err, users) {
					users.forEach(function(user) {
						var check_update = 0;
						var profile = user.profile;

						if(profile.occupation.company_key == key) {
							profile.occupation.address = address;
							chec_update = 1;
						}
						profile.experiences.forEach(function(exp, ind) {
							if(exp.company_key == key) {
								profile.experiences[ind].address = address;
								check_update = 1;
							}
						});

						if(check_update) {
							var update = {
									'profile.experiences': profile.experiences,
									'profile.occupation.address': profile.occupation.address
							};
							user.update(update, function(err) {
									if(err) console.error(err);
							});
						}
					});
				});
			}
		});
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.putOverview = function(req, res) {
	console.log("put Overview.");
	var key = req.user.key;
	var json = req.body;
	var update = req.user.profile;
	update.overview = json.overview;
	console.log(update);
	User.update({key:key}, {profile:update}, function(err) {
		if(err) {
			res.status(400);
			res.send({"message":err});
		}
		else {
			res.status(200);
			res.send({"message":"Overview has been updated."});
		}
	});
};

module.exports.putVideo = function(req, res) {
	console.log("put Video");
	var key = req.user.key;
	var json = req.body;
	User.update({key:key}, {'profile.video':json.video}, function(err) {
		if(err) res.status(400).send({"message":err});
		else res.status(200).send({"message": "Video has been updated."});
	});
};

module.exports.putCulture = function(req, res) {
	console.log("put Culture.");
	var key = req.user.key;
	var json = req.body;
	var update = req.user.profile;
	update.culture = json.culture;
	console.log(update);
	User.update({key:key}, {profile:update}, function(err) {
		if(err) {
			res.status(400);
			res.send({"message":err});
		}
		else {
			res.status(200);
			res.send({"message":"Culture has been updated."});
		}
	});
};

module.exports.putLink = function(req, res) {
	console.log("Put Link.");
	var key = req.user.key;
	var json = req.body;
	var update = req.user.profile;
	var New = new Link({
		_id : update.links,
		facebook : json.facebook,
		instagram : json.instagram,
		linkedin : json.linkedin,
		youtube : json.youtube,
		website : json.website
	});
	update.links = New;
	console.log(update);
	User.update({key:key}, {profile:update}, function(err) {
		if(err) {
			res.status(400);
			res.send({"message":err});
		}
		else {
			res.status(200);
			res.send({"message":"Link has been updated."});
		}
	});
};

module.exports.postChangePosition = function(req, res) {
	console.log('Post Change Position');
	var key = req.user.key;
	var json = req.body;

	if(json.type == 'employee')
	{
		User.findOne({ key: key, 'profile.employees': json.user_key }, function(err, user) {
			console.log(user);
			if(!user)
				res.status(400).send({"message": "User is not your employee."});
			else
			{
				if(json.change == 'former')
					User.update({ key: key },
						{
							$pull: { 'profile.employees': json.user_key },
							$push: { 'profile.formers': { $each: [json.user_key], $position: 0 } }
						},
						function(err){
							if(!err) res.status(200).send({"message": "Change position complete."});
							else res.status(400).send(err);
						}
					);
				else if(json.change == 'out')
					User.update({ key: key },
						{ $pull: { 'profile.employees': json.user_key } },
						function(err){
							if(!err) res.status(200).send({"message": "Change position complete."});
							else res.status(400);
						}
					);
				else
					res.status(400).send({"message": "Cannot change position to "+json.change+"."});
			}
		});
	}
	else if(json.type == 'former')
	{
		User.findOne({ key: key, 'profile.formers': json.user_key }, function(err, user) {
			console.log(user);
			if(!user)
			{
				res.status(400).send({"message": "User is not your former."});
			}
			else
			{
				if(json.change == 'out')
					User.update({ key: key },
						{ $pull: { 'profile.formers': json.user_key } },
						function(err){
							if(!err) res.status(200).send({"message": "Change position complete."});
							else res.status(400);
						}
					);
				else
					res.status(400).send({"message": "Cannot change position to "+json.change+"."});
			}
		});
	}
	else
	{
		res.status(400).send({"message": "Invalid type value."});
	}
};

module.exports.postChangeRank = function(req, res) {
	var company = req.user;
	var json = req.body;

	User.update({key: json.user_key}, {'profile.rank': json.rank}, function(err) {
		if(err) res.status(400).send(err);
		else res.status(200).send({"message": "Change rank complete!!"});
	});

};
