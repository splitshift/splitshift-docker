var mongoose = require('mongoose');
var User = mongoose.model('User');
var Benefit = mongoose.model('Benefit');

var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;

module.exports.postBenefit = function(req, res) {
	console.log('Post Benefit.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.description,json.category)) {
		var update = req.user.profile;
		if(!update.benefits) {
			update.benefits = [];
		}
		var newbenefit = new Benefit({
			description : json.description,
			category : json.category
		});
		update.benefits.unshift(newbenefit);
		console.log(update);
		User.update({key:key}, {profile:update }, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(201);
				res.send(newbenefit);
			}
		});
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.putBenefit = function(req, res) {
	console.log('Put Benefit.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.description,json.category)) {
		var id = req.params.id;
		var update = req.user.profile;
		var i = 0;
		for(i = 0; i < update.benefits.length ; i++) {
			if(update.benefits[i]._id == id) {
				break;
  		}
		}
		if(i==update.benefits.length) {
			res.status(404);
			res.send({"message":"ID of Benefit is invalid."});
		}
		else {
			var newExp = new Benefit({
				_id : update.benefits[i]._id,
				description : json.description,
				category : json.category
			});
			update.benefits[i] = newExp;
			console.log(update);
			User.update({key:key}, {profile:update }, function(err) {
				if(err) {
					res.status(400);
					res.send({"message":err});
				}
				else {
					res.status(200);
					res.send({"message":"Benefit has been updated."});
				}
			});
		}
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.deleteBenefit = function(req, res) {
	console.log('Delete Benefit.');
	var key = req.user.key;
	var json = req.body;
	var id = req.params.id;
	var update = req.user.profile;
	var i = 0;
	for(i = 0; i < update.benefits.length ; i++) {
		if(update.benefits[i]._id == id) {
			break;
		}
	}
	if(i==update.benefits.length) {
		res.status(400);
		res.send({"message":"ID of Benefit is invalid."});
	}
	else {
		update.benefits.splice(i,1);
		console.log(update);
		User.update({key:key}, {profile:update }, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(200);
				res.send({"message":"Benefit has been deleted."});
			}
		});
	}
};
