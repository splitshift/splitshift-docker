var mongoose = require('mongoose');

var User = mongoose.model('User');

module.exports.getEmployee = function(req, res) {
  var key = req.params.key;

  User.findOne({key: key}, {'profile.employees': 1}, function(err, user) {
    if(err) res.status(400).send(err);
    else if(user) {
      User.find({key: {$in: user.profile.employees}}, function(err, users) {
        if(err) res.status(400).send(err);
        else {
          var employees = [];
          users.forEach(function(emp) {
            employee = {
              key: emp.key,
              name: emp.profile.first_name+' '+emp.profile.last_name,
              profile_image: emp.profile_image,
              type: emp.type,
              rank: emp.profile.rank
            };
            employees.unshift(employee);
          });
          res.status(200).send(employees);
        }
      });
    }
    else res.status(400).send({"message": "user not found"});
  });
};

module.exports.getFormer = function(req, res) {
  var key = req.params.key;

  User.findOne({key: key}, {'profile.formers': 1}, function(err, user) {
    if(err) res.status(400).send(err);
    else if(user) {
      User.find({key: {$in: user.profile.formers}}, function(err, users) {
        if(err) res.status(400).send(err);
        else {
          var formers = [];
          users.forEach(function(frm) {
            former = {
              key: frm.key,
              name: frm.profile.first_name+' '+frm.profile.last_name,
              profile_image: frm.profile_image,
              type: frm.type,
              rank: frm.profile.rank
            };
            formers.unshift(former);
          });
          res.status(200).send(formers);
        }
      });
    }
    else res.status(400).send({"message": "user not found"});
  });
};
