var mongoose = require('mongoose');
var User = mongoose.model('User');
var Review = mongoose.model('Review');

var tools = require('../tools');
var checkNull = tools.checkNull;

module.exports.postReview = function(req, res) {
  console.log('Post Review.');
  var json = req.body;
  var detail = "";
  if(json.detail) detail = json.detail;
  if(checkNull(json.to, json.work, json.benefit, json.career, json.recommend)) {
    User.findOne({key:json.to}, {profile:true}, function(err, company) {
      if(checkNull(company)) {
        var user = company.profile;
        var i = tools.findKeyArray(user.employees, req.user.key);
        var j = tools.findKeyArray(user.formers, req.user.key);
        if(i<user.employees.length || j<user.formers.length) {
          for(i=0;i<user.review.length;i++) {
            if(user.review[i].from.key == req.user.key) {
              break;
            }
          }
          if(i>=user.review.length) {
            var New = new Review({
              from : {
                key: req.user.key,
                name: req.user.profile.first_name+' '+req.user.profile.last_name,
                profile_image: req.user.profile_image,
                user_type: req.user.type
              },
              overall : (json.work+json.benefit+json.career+json.recommend)/4,
              work : json.work,
              benefit : json.benefit,
              career : json.career,
              recommend : json.recommend,
              date: new Date()
            });
            user.review.unshift(New);
            if(!user.overall) {
              user.overall = 0;
            }
            var avg = ((user.overall*(user.review.length-1))+New.overall)/user.review.length;
            user.overall = avg;
            tools.updateData(json.to, res, {profile:user}, function(err) {
              if(!err) {
                res.status(201).send({"review": New, "overall": user.overall});
              }
            });
          }
          else {
            res.status(400).send({"message":"You are already review this employer."});
          }
        }
        else {
          res.status(400).send({"message":"You aren't employee of this employer."});
        }
      }
      else {
        res.status(400).send({"message":"User not found."});
      }
    });
  }
  else {
    res.status(400).send({"message":"Data is missing."});
  }
};

module.exports.deleteReview = function(req, res) {
  console.log('Delete Review.');
  var user = req.user;
  var id = req.params.id;
  var company_key = req.body.company_key;

  User.findOne({key: company_key}, function(err, company) {
    if(err) res.status(400).send(err);
    else if(company) {
      company.update({$pull: {'profile.review': {_id: mongoose.Types.ObjectId(id)}}}, function(err) {
        if(err) res.status(400).send(err);
        else res.status(200).send({"message": "Delete review complete."});
      });
    }
    else res.status(400).send({"message": "company not found"});
  });

};
