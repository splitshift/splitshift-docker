var mongoose = require('mongoose');

var ApplyJob = mongoose.model('ApplyJob');

module.exports.getApplyJobs = function(req, res) {
  var user = req.user;

  ApplyJob.find({'from.key': user.key}, {}, {sort: {date: -1}}, function(err, applyjobs) {
    if(err) res.status(200).send(err);
    else res.status(200).send(applyjobs);
  });
};

module.exports.delApplyJob = function(req, res) {
  var id = req.params.id;
  var user = req.user;

  ApplyJob.findById(id, function(err, applyjob) {
    if(err) res.status(400).send(err);
    else if(applyjob) {
      if(applyjob.from.key == user.key) {
        applyjob.remove(function(err) {
          if(err) res.status(400).send(err);
          else res.status(200).send({"message": "applyjob has been remove."});
        });
      }
      else res.status(200).send({"message": "you cannot remove applyjob."});
    }
    else res.status(400).send({"message": "applyjob not found."});
  });
};
