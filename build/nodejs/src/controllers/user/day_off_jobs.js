var mongoose = require('mongoose');
var User = mongoose.model('User');
var DayOffJob = mongoose.model('DayOffJob');

var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;

module.exports.getAllDayOffJob = function(req, res) {
	console.log('Get All Day off Job.');
	User.find({type:'Employee'}, function(err, user) {
		if(err) {
			res.status(404);
			res.status({"message":"Error when searching."});
		}
		else if(!user) {
			res.status(404);
			res.send({"message":"No user that have day off job."});
		}
		else {
			var day_off_jobs = [];
			for(var i=0;i<user.length;i++) {
				console.log(user[i]);
				if(user[i].profile.day_off_jobs) {
					console.log('have user.');
					for(var j=0;j<user[i].profile.day_off_jobs.length;j++) {
						var New = user[i].profile.day_off_jobs[j];
						New.user_key = user[i].key;
						day_off_jobs.unshift(New);
					}
				}
			}
			res.status(200);
			res.send(day_off_jobs);
		}
	});
};

module.exports.getMyDayOffJob = function(req, res) {
	console.log('Get My Day off Job.');
	var profile = req.user.profile;
	res.status(200);
	res.send(profile.day_off_jobs);
};

module.exports.postDayOffJob = function(req, res) {
	console.log('Post Day off Job.');
	var json = req.body;
	var key = req.user.key;
	if(checkNull(json.name,json.contact,json.fee,json.detail)) {
		var update = req.user.profile;
		if(!update.day_off_jobs) {
			update.day_off_jobs = [];
		}
		var newDoj = new DayOffJob({
			name : json.name,
			contact : json.contact,
			fee : json.fee,
			detail : json.detail
		});
		update.day_off_jobs.unshift(newDoj);
		console.log(update);
		updateData(key, res, {profile:update}, function(err) {
			if(!err) {
				res.status(200);
				res.send({"_id": newDoj._id});
			}
		});
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.getDayOffJob = function(req, res) {
	console.log("Get Day off job.");
	var id = req.params.id;
	var update = req.user.profile.day_off_jobs;
	var i = 0;
	for(i = 0; i < update.length ; i++) {
		if(update[i]._id == id) {
			break;
		}
	}
	if(i==update.length) {
		res.status(404);
		res.send({"message":"ID of Day off job is invalid."});
	}
	else {
		res.status(200);
		res.send(update[i]);
	}
};

module.exports.putDayOffJob = function(req, res) {
	console.log('Put Day off Job.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.name,json.contact,json.fee,json.detail)) {
		var id = req.params.id;
		var update = req.user.profile;
		var i = 0;
		for(i = 0; i < update.day_off_jobs.length ; i++) {
			if(update.day_off_jobs[i]._id == id) {
				break;
  		}
		}
		if(i==update.day_off_jobs.length) {
			res.status(400);
			res.send({"message":"ID of Day off Job is invalid."});
		}
		else {
			var newDoj = new DayOffJob({
				_id : update.day_off_jobs[i]._id,
				name : json.name,
				contact : json.contact,
				fee : json.fee,
				detail : json.detail
			});
			update.day_off_jobs[i] = newDoj;
			console.log(update);
			User.update({key:key}, {profile:update }, function(err) {
				if(err) {
					res.status(400);
					res.send({"message":err});
				}
				else {
					res.status(200);
					res.send({"message":"Day off Job has been updated."});
				}
			});
		}
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.deleteDayOffJob = function(req, res) {
	console.log('Delete Day off Job.');
	var key = req.user.key;
	var json = req.body;
	var id = req.params.id;
	var update = req.user.profile;
	var i = 0;
	for(i = 0; i < update.day_off_jobs.length ; i++) {
		if(update.day_off_jobs[i]._id == id) {
			break;
		}
	}
	if(i==update.day_off_jobs.length) {
		res.status(400);
		res.send({"message":"ID of Day off Job is invalid."});
	}
	else {
		update.day_off_jobs.splice(i,1);
		console.log(update);
		User.update({key:key}, {profile:update }, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(200);
				res.send({"message":"Day off Job has been deleted."});
			}
		});
	}
};
