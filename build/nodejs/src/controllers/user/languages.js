var mongoose = require('mongoose');
var User = mongoose.model('User');
var Language = mongoose.model('Language');

var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;

module.exports.postLanguage = function(req, res) {
	console.log('Post Language.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.language,json.proficiency)) {
		var update = req.user.profile;
		if(!update.languages) {
			update.languages = [];
		}
		var newLag = new Language({
			language : json.language,
			proficiency : json.proficiency
		});
		update.languages.unshift(newLag);
		console.log(update);
		User.update({key:key}, {profile:update}, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(201);
				res.send(newLag);
			}
		});
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.putLanguage = function(req, res) {
	console.log('Put Language.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.language,json.proficiency)) {
	  var id = req.params.id;
		var update = req.user.profile;
		var i = 0;
		for(i = 0; i < update.languages.length ; i++) {
			if(update.languages[i]._id == id) {
				break;
  		}
		}
		if(i==update.languages.length) {
			res.status(400);
			res.send({"message":"ID of Language is invalid."});
		}
		else {
			var newLag = new Language({
				_id : update.languages[i]._id,
				language : json.language,
  			proficiency : json.proficiency
			});
			update.languages[i] = newLag;
			console.log(update);
			User.update({key:key}, {profile:update }, function(err) {
				if(err) {
					res.status(400);
					res.send({"message":err});
				}
				else {
					res.status(200);
					res.send({"message":"Language has been updated."});
				}
			});
		}
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.deleteLanguage = function(req, res) {
	console.log('Delete Language.');
	var key = req.user.key;
	var json = req.body;
	var id = req.params.id;
	var update = req.user.profile;
	var i = 0;
	for(i = 0; i < update.languages.length ; i++) {
		if(update.languages[i]._id == id) {
			break;
		}
	}
	if(i==update.languages.length) {
		res.status(400);
		res.send({"message":"ID of Language is invalid."});
	}
	else {
		update.languages.splice(i,1);
		console.log(update);
		User.update({key:key}, {profile:update }, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(200);
				res.send({"message":"Language has been deleted."});
			}
		});
	}
};
