var mongoose = require('mongoose');
var User = mongoose.model('User');
var Course = mongoose.model('Course');

var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;

module.exports.postCourse = function(req, res) {
	console.log('Post Course.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.name,json.start_date,json.end_date)) {
		var update = req.user.profile;
		if(!update.courses) {
			update.courses = [];
		}
		var newAwd = new Course({
			name : json.name,
			start_date: json.start_date,
			end_date: json.end_date,
			description: json.description,
			link: json.link
		});
		update.courses.unshift(newAwd);
		console.log(update);
		User.update({key:key}, {profile:update}, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(201);
				res.send(newAwd);
			}
		});
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.putCourse = function(req, res) {
	console.log('Put Course.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.name,json.start_date,json.end_date)) {
		var id = req.params.id;
		var update = req.user.profile;
		var i = 0;
		for(i = 0; i < update.courses.length ; i++) {
			if(update.courses[i]._id == id) {
				break;
  		}
		}
		if(i==update.courses.length) {
			res.status(400);
			res.send({"message":"ID of Course is invalid."});
		}
		else {
			var newAwd = new Course({
				_id : update.courses[i]._id,
				name : json.name,
				start_date: json.start_date,
				end_date: json.end_date,
				description: json.description,
				link: json.link
			});
			update.courses[i] = newAwd;
			console.log(update);
			User.update({key:key}, {profile:update }, function(err) {
				if(err) {
					res.status(400);
					res.send({"message":err});
				}
				else {
					res.status(200);
					res.send({"message":"Course has been updated."});
				}
			});
		}
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.deleteCourse = function(req, res) {
	console.log('Delete Course.');
	var key = req.user.key;
	var json = req.body;
	var id = req.params.id;
	var update = req.user.profile;
	var i = 0;
	for(i = 0; i < update.courses.length ; i++) {
		if(update.courses[i]._id == id) {
				break;
			}
		}
		if(i==update.courses.length) {
			res.status(400);
			res.send({"message":"ID of Course is invalid."});
		}
		else {
			update.courses.splice(i,1);
			console.log(update);
			User.update({key:key}, {profile:update }, function(err) {
				if(err) {
					res.status(400);
					res.send({"message":err});
				}
				else {
					res.status(200);
					res.send({"message":"Course has been deleted."});
				}
			});
		}
};
