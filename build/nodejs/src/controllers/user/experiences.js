var mongoose = require('mongoose');
var User = mongoose.model('User');
var Experience = mongoose.model('Experience');
var Request = mongoose.model('Request');

var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;
var findArray = tools.findArray;

module.exports.postExperience = function(req, res) {
	console.log('Post Experience.');
	var key = req.user.key;
	var json = req.body;console.log(json.address);
	if(checkNull(json.position,json.workplace,json.start_date)) {
		var update = req.user.profile;
		if(!update.experiences) {
			update.experiences = [];
		}
		var newExp = new Experience({
			position : json.position,
			workplace : json.workplace,
			address : {
				address: json.address,
				district: json.district,
		  	zip_code: json.zip_code,
		  	city: json.city
			},
			company_key : json.company_key,
			start_date : new Date(json.start_date),
			role : json.role
		});
		var oldCompany = null;

		User.findOne({key:json.company_key}, function(err, user) {
			if(!user && json.company_key) {
				res.status(400).send({"message":"Company not found."});
			}
			else {
				if(user) {
					update.occupation.address = user.address;
					newExp.address = user.address;
				}
				if(!json.end_date) {
					if(update.occupation.experience_id) {
						var id = update.occupation.experience_id;
						var now = findArray(update.experiences, id);
						var d = new Date(json.start_date);
						d.setMonth(d.getMonth() - 1);
						if(now < update.experiences.length) {
							update.experiences[now].end_date = d;
							oldCompany = update.experiences[now].company_key;
							update.rank = "Employee";
						}
					}
					update.occupation = {
						position: newExp.position,
						experience_id: newExp._id,
						workplace: newExp.workplace,
						address: newExp.address,
						company_key: newExp.company_key
					};

					Request.update({'from.key': key}, {'from.occupation': update.occupation.position}, function(err) {
						if(err) console.error(err);
					});
				}
				else {
					newExp.end_date = json.end_date;
				}
				update.experiences.unshift(newExp);

				User.update({key:key}, {profile:update }, function(err) {
					if(err) {
						res.status(400);
						res.send({"message":err});
					}
					else {
						if(checkNull(json.company_key)) {
							User.findOne({key:json.company_key}, function(err, user) {
								if(checkNull(user)){
									if(user.type == "Employer") {
										if(checkNull(json.end_date)) {
											user.profile.formers.push(req.user.key);
										}
										else {
											user.profile.employees.push(req.user.key);
										}
										tools.updateData(json.company_key, res, {profile:user.profile}, function(err) {
											if(!err) {
												if(checkNull(oldCompany)) {
													User.findOne({key:oldCompany, type:"Employer"}, function(err, company) {
														if(checkNull(company)) {
															var i = tools.findKeyArray(company.profile.employees, req.user.key);
															company.profile.employees.splice(i, 1);
															company.profile.formers.unshift(req.user.key);
															User.update({key:company.key}, {profile:company.profile},function(err) {
																if(err) {
																	res.status(400).send({"message":"Update old company error."});
																}
																else {
																	res.status(201).send(newExp);
																}
															});
														}
														else {
															res.status(400).send({"message":"Company not found."});
														}
													});
												}
												else {
													res.status(201).send(newExp);
												}
											}
										});
									}
									else {
										res.status(400).send({"message":"This isn't key of employer."});
									}
								}
								else {
									res.status(400).send({"message":"Company not found."});
								}
							});
						}
						else {
							res.status(201).send(newExp);
						}
					}
				});
			}
		});
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.putExperience = function(req, res) {
	console.log('Put Experience.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.position,json.workplace,json.start_date)) {
		var id = req.params.id;
		var update = req.user.profile;
		var i = findArray(update.experiences,id);
		if(i==update.experiences.length) {
			res.status(400);
			res.send({"message":"ID of Experience is invalid."});
		}
		else {
			var oldCompany = update.experiences[i].company_key;
			var oldEnd_date = update.experiences[i].end_date;
			var newExp = new Experience({
				_id : update.experiences[i]._id,
				position : json.position,
				company_key : json.company_key,
				address: {
					address: json.address,
					district: json.district,
				  zip_code: json.zip_code,
				  city: json.city
				},
				workplace : json.workplace,
				start_date : json.start_date,
				role : json.role
			});

			if(newExp._id.equals(update.occupation.experience_id) && json.end_date) {
				update.occupation.experience_id = '';
				update.rank = "Employee";
			}

			User.findOne({key:json.company_key}, function(err, user) {
				if(!user && json.company_key) {
					res.status(400).send({"message":"Company not found."});
				}
				else {
					if(user) {
						update.occupation.address = user.address;
						newExp.address = user.address;
					}

					if(json.end_date) {
						newExp.end_date = json.end_date;
					}
					else {
						if(!update.experiences[i]._id.equals(update.occupation.experience_id) && update.occupation.experience_id) {
							var old = findArray(update.experiences, update.occupation.experience_id);
							update.experiences[old].end_date = new Date();
							if(checkNull(update.experiences[old].company_key)) {
								User.findOne({key:update.experiences[old].company_key, type:"Employer"}, function(err, company) {
									if(err) console.log(err);
									var n = tools.findKeyArray(company.profile.employees, req.user.key);
									company.profile.employees.splice(n, 1);
									company.profile.formers.unshift(req.user.key);
									User.update({key:company.key}, {profile:company.profile}, function(err) {
										if(err)
											res.status(400).send({"message":"Update old company position error."});
									});
								});
							}
						}
						update.occupation = {
							position: newExp.position,
							experience_id: update.experiences[i]._id,
							workplace: newExp.workplace,
							address: newExp.address,
							company_key: newExp.company_key
						};

						Request.update({'from.key': key}, {'from.occupation': update.occupation.position}, function(err) {
							if(err) console.error(err);
						});
					}

					if(checkNull(json.company_key)) {
						User.findOne({key:json.company_key}, function(err, user) {
							if(user) {
								update.occupation.address = user.address;
								newExp.address = user.address;
							}
							else res.status(400).send({"message":"Company not found."});
						});
					}

					update.experiences[i] = newExp;
					console.log(update);
					User.update({key:key}, {profile:update }, function(err) {
						if(err) {
							res.status(400);
							res.send({"message":err});
						}
						else {
							var i;
							if(oldCompany != newExp.company_key || oldEnd_date != newExp.end_date) {
								if(checkNull(oldCompany)) {
									User.findOne({key:oldCompany, type:"Employer"}, function(err, company) {
										if(checkNull(company)) {
											if(checkNull(oldEnd_date)) {
												i = tools.findKeyArray(company.profile.formers, req.user.key);
												company.profile.formers.splice(i, 1);
											}
											else {
												i = tools.findKeyArray(company.profile.employees, req.user.key);
												company.profile.employees.splice(i, 1);
											}
											User.update({key:company.key}, {profile:company.profile}, function(err) {
												if(checkNull(newExp.company_key)) {
													User.findOne({key:newExp.company_key, type:"Employer"}, function(err, company) {
														if(checkNull(company)) {
															if(checkNull(newExp.end_date)) {
																company.profile.formers.unshift(req.user.key);
															}
															else {
																company.profile.employees.unshift(req.user.key);
															}
															User.update({key:company.key}, {profile:company.profile}, function(err) {
																if(err) {
																	res.status(400).send({"message":"Update new company error."});
																}
																else {
																	res.status(200).send({"message":"Experience has been updated."});
																}
															});
														}
														else {
															res.status(400).send({"message":"Company not found."});
														}
													});
												}
												else {
													res.status(200).send({"message":"Experience has been updated."});
												}
											});
										}
										else {
											res.status(400).send({"message":"Company not found."});
										}
									});
								}
								else if(checkNull(newExp.company_key)) {
									User.findOne({key:newExp.company_key, type:"Employer"}, function(err, company) {
										if(checkNull(company)) {
											if(checkNull(newExp.end_date)) {
												company.profile.formers.unshift(req.user.key);
											}
											else {
												company.profile.employees.unshift(req.user.key);
											}
											User.update({key:company.key}, {profile:company.profile}, function(err) {
												if(err) {
													res.status(400).send({"message":"Update new company error."});
												}
												else {
													res.status(200).send({"message":"Experience has been updated."});
												}
											});
										}
									});
								}
								else {
									res.status(200).send({"message":"Experience has been updated."});
								}
							}
							else {
								res.status(200);
								res.send({"message":"Experience has been updated."});
							}
						}
					});
				}
			});
		}
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.deleteExpereince = function(req, res) {
	console.log('Delete Experience.');
	var key = req.user.key;
	var json = req.body;
	var id = req.params.id;
	var update = req.user.profile;
	var i = 0;
	for(i = 0; i < update.experiences.length ; i++) {
		if(update.experiences[i]._id == id) {
			break;
		}
	}
	if(i==update.experiences.length) {
		res.status(400);
		res.send({"message":"ID of Experience is invalid."});
	}
	else {
		var oldCompany = update.experiences[i].company_key;
		var oldEnd_date = update.experiences[i].end_date;
		if(update.experiences[i]._id.equals(update.occupation.experience_id)) {
			update.occupation.experience_id = '';
			update.rank = "Employee";
		}
		update.experiences.splice(i,1);
		console.log(update);
		User.update({key:key}, {profile:update }, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				if(checkNull(oldCompany)) {
					User.findOne({key:oldCompany, type:"Employer"}, function(err, company) {
						if(checkNull(company)) {
							if(checkNull(oldEnd_date)) {
								i = tools.findKeyArray(company.profile.formers, req.user.key);
								company.profile.formers.splice(i, 1);
							}
							else {
								i = tools.findKeyArray(company.profile.employees, req.user.key);
								company.profile.employees.splice(i, 1);
							}
							User.update({key:company.key}, {profile:company.profile}, function(err) {
								if(err) {
									res.status(400).send({"message":"Delete company error."});
								}
								else {
									res.status(200).send({"message":"Experience has been deleted."});
								}
							});
						}
						else {
							res.status(400).send({"message":"Company not found."});
						}
					});
				}
				else {
					res.status(200);
					res.send({"message":"Experience has been deleted."});
				}
			}
		});
	}
};
