var mongoose = require('mongoose');
var User = mongoose.model('User');
var Address = mongoose.model('Address');
var Activity = mongoose.model('Activity');
var Request = mongoose.model('Request');
var Notification = mongoose.model('Notification');
var ApplyJob = mongoose.model('ApplyJob');
var Resource = mongoose.model('Resource');
var Feed = mongoose.model('Feed');

//call tools
var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;

module.exports.putInfo = function(req, res) {
	console.log("PutInfo.");
	var key = req.user.key;
  var json = req.body;
	if(checkNull(json.first_name,json.last_name)) {
		var update = req.user.profile;
		update.first_name = json.first_name;
		update.last_name = json.last_name;
		var bd = json.birth_day.split('-');
		update.birth_day = new Date(bd[2]+'-'+bd[1]+'-'+bd[0]);
		update.gender = json.gender;
		if(json.address) {
			var address = {
				information: json.address.information,
				description: json.address.description,
				district: json.address.district,
				city: json.address.city,
				country: json.address.country,
				zip_code: json.address.zip_code,
				location: [json.address.lng, json.address.lat]
			};
		}

		User.update({key:key}, {address: address,profile:update}, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				//update feed
				Feed.update({'owner.key': key},{'owner.name': update.first_name+' '+update.last_name},{multi: true}, function(err) {
					if(err) console.error(err);
				});
				//update comment and reply
				Activity.find({key: key}, function(err, activities) {
					if(err) res.status(400).send({"message": err});
					else {
						activities.forEach(function(act, index) {
							var query = {};
							if(act.activity.resource_id) {
								Resource.findById(act.activity.resource_id, function(err, resource) {
									if(resource) {
										var comment_ind = tools.findArray(resource.comments, act.activity.comment_id);
										query['comments.'+comment_ind+'.owner.name'] = update.first_name+' '+update.last_name;
										Resource.update({_id: act.activity.resource_id}, query, function(err) {
											if(err) console.error(err);
										});
									}
								});
							}
							else {
								Feed.findOne({_id: act.activity.feed_id}, function(err, feed) {
									if(feed) {
										var comment_ind = tools.findArray(feed.comments, act.activity.comment_id);
										if(act.activity.reply_id) {
											var reply_ind = tools.findArray(feed.comments[comment_ind].replies, act.activity.reply_id);
											query['comments.'+comment_ind+'.replies.'+reply_ind+'.owner.name'] = update.first_name+' '+update.last_name;
										}
										else query['comments.'+comment_ind+'.owner.name'] = update.first_name+' '+update.last_name;
										Feed.update({_id: act.activity.feed_id}, query, function(err) {
											if(err) console.error(err);
										});
									}
								});
							}
						});
					}
				});
				//update request
				Request.update({'from.key': key},{'from.name': update.first_name+' '+update.last_name},{multi: true}, function(err) {
					if(err) console.error(err);
				});
				//update noti
				Notification.update({'from.key': key},{'from.name': update.first_name+' '+update.last_name},{multi: true}, function(err) {
					if(err) console.error(err);
				});
				//update resource
				Resource.update({'owner.key': key},{'owner.name': update.first_name+' '+update.last_name},{multi: true}, function(err) {
					if(err) console.error(err);
				});
				//update review and applyjob
				if(req.user.type == 'Employee') {
					User.update({type: 'Employer', 'profile.reviews.from.key': req.user.key},
											{'profile.reviews.$.from.name': req.user.profile.first_name+' '+req.user.profile.last_name},
											{multi: true}, function(err) {
						if(err) console.error(err);
					});
					ApplyJob.update({'from.key': key}, {'from.name': update.first_name+' '+update.last_name}, {multi:true}, function(err) {
						if(err) console.error(err);
					});
				}
				res.status(200).send({"message":"Info has been updated."});
			}
		});
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.putAbout = function(req, res) {
	console.log("putAbout.");
	var key = req.user.key;
	var json = req.body;
	var update = req.user.profile;
	update.about = json.about;
	console.log(update);
	User.update({key:key}, {profile:update}, function(err) {
		if(err) {
			res.status(400);
			res.send({"message":err});
		}
		else {
			res.status(200);
			res.send({"message":"About has been updated."});
		}
	});
};

module.exports.putSkill = function(req, res) {
	console.log('Put Skill.');
	var key = req.user.key;
	var json = req.body;
	var skills = [];

	json.skills.forEach(function(skill, idx) {
	  if(/\S/.test(skill)) {
			skills.push(skill);
		}
	});

  User.update({key:key}, {'profile.skills':skills}, function(err) {
  	if(err) {
  		res.status(400).send({"message":err});
		}
		else {
  		res.status(200).send({"message":"Skills has been updated."});
  	}
  });
};
