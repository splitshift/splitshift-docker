var mongoose = require('mongoose');
var User = mongoose.model('User');
var Interest = mongoose.model('Interest');

var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;

module.exports.postInterest = function(req, res) {
	console.log('Post Interest.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.category,json.value)) {
		var update = req.user.profile;
		if(!update.interests) {
			update.interests = [];
		}
		var newInt = new Interest({
			category : json.category,
			value : json.value
		});
		update.interests.unshift(newInt);
		console.log(update);
		User.update({key:key}, {profile:update}, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(201);
				res.send(newInt);
			}
	  });
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.putInterest = function(req, res) {
	console.log('Put Interest.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.category,json.value)) {
		var id = req.params.id;
		var update = req.user.profile;
		var i = 0;
		for(i = 0; i < update.interests.length ; i++) {
			if(update.interests[i]._id == id) {
				break;
  		}
		}
		if(i==update.interests.length) {
			res.status(400);
			res.send({"message":"ID of Interest is invalid."});
		}
		else {
			var newInt = new Interest({
				_id : update.interests[i]._id,
				category : json.category,
				value : json.value
			});
			update.interests[i] = newInt;
			console.log(update);
			User.update({key:key}, {profile:update }, function(err) {
				if(err) {
					res.status(400);
					res.send({"message":err});
				}
				else {
					res.status(200);
					res.send({"message":"Interest has been updated."});
				}
			});
		}
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.deleteInterest = function(req, res) {
	console.log('Delete Interest.');
	var key = req.user.key;
	var id = req.params.id;
	var update = req.user.profile;
	var i = 0;
	for(i = 0; i < update.interests.length ; i++) {
		if(update.interests[i]._id == id) {
			break;
		}
	}
	if(i==update.interests.length) {
		res.status(400);
		res.send({"message":"ID of Interest is invalid."});
	}
	else {
		update.interests.splice(i,1);
		console.log(update);
		User.update({key:key}, {profile:update }, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(200);
				res.send({"message":"Interest has been deleted."});
			}
		});
	}
};
