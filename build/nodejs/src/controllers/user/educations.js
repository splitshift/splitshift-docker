var mongoose = require('mongoose');
var User = mongoose.model('User');
var Education = mongoose.model('Education');

var tools = require('../tools');
var updateData = tools.updateData;
var checkNull = tools.checkNull;

module.exports.postEducation = function(req, res) {
	console.log('Post Education.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.field,json.school,json.start_date,json.graduate_date)) {
		var update = req.user.profile;
		if(!update.educations) {
			update.educations = [];
		}
		var newEdu = new Education({
			field : json.field,
			school : json.school,
			start_date : json.start_date,
			graduate_date : json.graduate_date,
			description : json.description,
			gpa : json.gpa
		});
		update.educations.unshift(newEdu);
		console.log(update);
		User.update({key:key}, {profile:update}, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(201);
				res.send(newEdu);
			}
	  });
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.putEducation = function(req, res) {
	console.log('Put Education.');
	var key = req.user.key;
	var json = req.body;
	if(checkNull(json.field,json.school,json.start_date,json.graduate_date)) {
		var id = req.params.id;
		var update = req.user.profile;
		var i = 0;
		for(i = 0; i < update.educations.length ; i++) {
			if(update.educations[i]._id == id) {
				break;
  		}
		}
		if(i==update.educations.length) {
			res.status(400);
			res.send({"message":"ID of Education is invalid."});
		}
		else {
			var newEdu = new Education({
				_id : update.educations[i]._id,
				field : json.field,
				school : json.school,
				start_date : json.start_date,
				graduate_date : json.graduate_date,
				description : json.description,
				gpa : json.gpa
			});
			update.educations[i] = newEdu;
			console.log(update);
			User.update({key:key}, {profile:update }, function(err) {
				if(err) {
					res.status(400);
					res.send({"message":err});
				}
				else {
					res.status(200);
					res.send({"message":"Education has been updated."});
				}
			});
		}
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.deleteEducation = function(req, res) {
	console.log('Delete Education.');
	var key = req.user.key;
	var json = req.body;
	var id = req.params.id;
	var update = req.user.profile;
	var i = 0;
	for(i = 0; i < update.educations.length ; i++) {
		if(update.educations[i]._id == id) {
			break;
		}
	}
	if(i==update.educations.length) {
		res.status(400);
		res.send({"message":"ID of Education is invalid."});
	}
	else {
		update.educations.splice(i,1);
		console.log(update);
		User.update({key:key}, {profile:update }, function(err) {
			if(err) {
				res.status(400);
				res.send({"message":err});
			}
			else {
				res.status(200);
				res.send({"message":"Education has been deleted."});
			}
		});
	}
};
