var mongoose = require('mongoose');
var User = mongoose.model('User');
var Job = mongoose.model('Job');

module.exports.getStatAll = function(req, res) {
  var result = {};

  User.find({type: {$ne: 'Admin'}}, function(err, users) {
    if(err) res.status(400).send(err);
    else {
      var members = 0;
      var companies = 0;
      var people = 0;

      users.forEach(function(user) {
        members += 1;
        if(user.type == 'Employer') companies += 1;
        if(user.type == 'Employee') people += 1;
      });
      result.members = members;
      result.companies = companies;
      result.people = people;

      Job.count({}, function(err, jobs) {
        if(err) res.status(400).send(err);
        else {
          result.jobs = jobs;
          res.status(200).send(result);
        }
      });
    }
  });
};
