var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var rand = require('generate-key');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var request = require('request');
var FB = require('fb');
var config = require('../config');

//sendgrid
var sendGrid = require('sendgrid').mail;
var sg = require('sendgrid').SendGrid(config.sendgrid.api_key);

//google
var google = require('googleapis');
var oauth2Client = new google.auth.OAuth2();

//model
var Subscribe = mongoose.model('Subscribe');
var User = mongoose.model('User');
var UserProfile = mongoose.model('UserProfile');
var CompanyProfile = mongoose.model('CompanyProfile');
var Gallery = mongoose.model('Gallery');

//call tools from tools.js
var tools = require('./tools');
var verifyToken = tools.verifyToken;
var verifyKey = tools.verifyKey;
var updateData = tools.updateData;
var checkNull = tools.checkNull;
var saveUser = tools.saveUser;

FB.options({
    appId:          config.facebook.appId,
    appSecret:      config.facebook.appSecret
});

module.exports.postRegister = function(req, res) {
	console.log(req.body);
  var json = req.body;
  /* Check register_type*/
  if(json.register_type == "Simple") {
		console.log("Simple Register.");
    simpleRegister(json, res);
  }
  else if(json.register_type == 'Facebook') {
		console.log("Facebook Register.");
    facebookRegister(json, res);
  }
  else if(json.register_type == 'Google') {
    console.log("Google Register.");
    googleRegister(json, res);
  }
  else {
    res.status(400);
    res.send({message:"Register type is not correct"});
  }
};

module.exports.postRegisterAdmin = function(req, res) {
	console.log('Admin Register');
	var json = req.body;
	if(tools.checkNull(json.email, json.password, json.type) && json.type == "Admin") {
		User.findOne({email:json.email}, function(err, user) {
			if(user) {
				res.status(409);
				res.send({"message":"Email address already taken."});
			}
			else {
				var newUser = new User({
					email: json.email,
					password: bcrypt.hashSync(json.password, bcrypt.genSaltSync(8), null),
					type: json.type,
					register_type: 'Simple',
					key: rand.generateKey(8),
          created: new Date(),
          confirm: true,
          profile_image: '/images/profiles/admin.png',
          banner: '/images/banners/admin.jpg'
				});
        newUser.profile = new CompanyProfile();
        newUser.profile.company_name = 'Splitshift';
				newUser.save(function(err) {
					if(err) {
						res.status(400);
						res.send({"message":"Save error."});
					}
					else {
						var token = jwt.sign({key: newUser.key, type: newUser.type}, config.jwt.secret, { expiresIn : '2h' });
						res.status(201);
	          res.send({ token:token, type:newUser.type, key:newUser.key });
					}
				});
			}
		});
	}
	else {
		res.status(400);
		res.send({"message":"Data is missing."});
	}
};

module.exports.postLogin = function(req, res) {
  /*
  `get data for login and save to db`
  */
  var json = req.body;
  if(json.login_type == "Simple") {
		console.log("Simple Login.");
		if(!json.email || !json.password) {
			res.status(400);
			res.send({"message":"Email or password is not found."});
		}
		else {
	    User.findOne({email:json.email}, function(err, user) {
	      if(err) {
					res.status(400);
	        res.send({"message":err});
				}
	      else if(!user) {
	        res.status(400);
	        res.send({"message": 'Email is invalid.'});
	      }
	      else {
          if(!user.confirm) return res.status(400).send({"message": "You must confirm email address before login."});
					if(json.login_type != user.register_type) {
						res.status(400);
						res.send({"message":"Login type not match with register_type."});
					}
	        else if(!bcrypt.compareSync(json.password, user.password)) {
	          res.status(400);
	          res.send({"message": 'Password is invalid.'});
	        }
	        else {
	          var token = jwt.sign({key: user.key, type: user.type}, config.jwt.secret, { expiresIn : '2h' });
	          res.send({ token:token, type:user.type, key:user.key });
	        }
	      }
	    });
		}
  }
  else if(json.login_type == "Facebook") {
		console.log("Facebook Login.");
		FB.setAccessToken(json.access_token);
		FB.api('me', function (me) {
			if(!me || me.error) {
				console.log(!me ? 'error occurred' : me.error);
        res.status(400).send({"message": me.error});
      }
      else {
        User.findOne({email:json.email, facebook_id:me.id}, function(err, user) {
          if(!user) {
            res.status(400);
            res.send({"message":"Facebook id is invalid."});
          }
					else if(json.login_type != user.register_type) {
						res.status(400);
						res.send({"message":"Login type not match with register_type."});
					}
          else {
            var token = jwt.sign({key: user.key, type: user.type}, config.jwt.secret, { expiresIn : '2h' });
            res.send({ token:token, type:user.type, key:user.key });
          }
				});
			}
		});
	}
  else if(json.login_type == "Google") {
		console.log("Google Login.");
    var token = json.access_token;
    oauth2Client.verifyIdToken(token, config.google.client_id, function(err, login) {
      if(err) {
        res.status(400).send({"message": "Token error!!"});
      }
      else if(login) {
        var payload = login.getPayload();
        User.findOne({google_id: payload.sub}, function(err, user) {
          if(!user) {
            res.status(400);
            res.send({"message": "Google id is invalid."});
          }
					else if(json.login_type != user.register_type) {
						res.status(400);
						res.send({"message": "Login type not match with register_type."});
					}
          else {
            var token = jwt.sign({key: user.key, type: user.type}, config.jwt.secret, { expiresIn : '2h' });
            res.send({ token:token, type:user.type, key:user.key });
          }
				});
			}
      else {
        res.status(400).send({"message": "Login error!!"});
      }
    });
	}
  else {
    res.status(400);
    res.send({message:"Log in type is not correct"});
  }
};

function simpleRegister(json, res) {
  console.log('Simple Register.');
  if(!json.email || !json.password) {
    res.status(400);
    res.send({ "message" : "Email or password is not found."});
  }
  else {
    createUser(json, null, null, function(err, newUser) {
      if(err) {
				res.status(400);
				res.send({ "message": err});
			}
      else
        saveUser(newUser, res, function(err) {
					if(!err) {
						var token = jwt.sign({user_key: newUser.key, method: 'confirm'}, config.jwt.secret);
		        res.status(201);
		        res.send({"message": "Register complete, Please confirm your email address."});
            //send mail
            sendmail(newUser, token);
					}
				});
    });
  }
}

function facebookRegister(json, res) {
  console.log('Facebook Register.');
  if(!json.email || !json.access_token ) {
    res.status(400);
    res.send({ "message" : "Token or Email is not found."});
  }
  else {
		FB.setAccessToken(json.access_token);
		FB.api('me', function (me) {
		  if(!me || me.error) {
				console.log(!me ? 'error occurred' : me.error);
				res.status(400).send({"message" : me.error});
		  }
      else {
        User.findOne({facebook_id:me.id}, function(err, user) {
          if(user) {
            res.status(409);
            res.send({"message": "Facebook is already used."});
          }
          else {
            createUser(json, me.id, null, function(err, newUser) {
              if(err){
                res.send({"message":err});
							}
              else {
								saveUser(newUser, res, function(err) {
									if(!err) {
										var token = jwt.sign({user_key: newUser.key, method: 'confirm'}, config.jwt.secret);
										res.status(201);
										res.send({"message": "Register complete, Please confirm your email address."});
                    //send mail
                    sendmail(newUser, token);
									}
            		});
							}
        		});
      		}
    		});
			}
		});
	}
}

function googleRegister(json, res) {
  console.log('Google Register.');
  if(!json.email || !json.access_token ) {
    res.status(400);
    res.send({ "message" : "Token or Email is not found."});
  }
  else {
		var token = json.access_token;
		oauth2Client.verifyIdToken(token, config.google.client_id, function(err, login) {
      if(err) {
        res.status(400).send({"message": "Token error!!"});
      }
      else if(login) {
        var payload = login.getPayload();
        User.findOne({google_id:payload.sub}, function(err, user) {
          if(user) {
            res.status(409);
            res.send({"message": "Google id is already used."});
          }
          else {
            createUser(json, null, payload.sub, function(err, newUser) {
              if(err){
                res.send({"message":err});
							}
              else {
								saveUser(newUser, res, function(err) {
									if(!err) {
										var token = jwt.sign({user_key: newUser.key, method: 'confirm'}, config.jwt.secret);
										res.status(201);
										res.send({"message": "Register complete, Please confirm your email address."});
                    //send mail
                    sendmail(newUser, token);
									}
            		});
							}
        		});
      		}
    		});
			}
      else {
        res.status(400).send({"message": "Register Error!!"});
      }
		});
	}
}

function createUser(json, facebook_id, google_id, callback){
  console.log('Create user.');
  var newUser = new User();
  newUser.key = rand.generateKey(8);
	newUser.email = json.email;
  if(facebook_id) {
    newUser.facebook_id = facebook_id;
	}
  else if(google_id) {
    newUser.google_id = google_id;
  }
  if(json.password) {
    newUser.password = bcrypt.hashSync(json.password, bcrypt.genSaltSync(8), null);
	}
	if(json.type == 'Employee') {
		if(checkNull(json.first_name,json.last_name)) {
	    newUser.profile = new UserProfile();
		  newUser.profile.first_name = json.first_name;
		  newUser.profile.last_name = json.last_name;
		}
		else return callback('Data is missing.',null);
  }
  else if(json.type == 'Employer') {
		if(checkNull(json.company_name, json.first_name, json.last_name)) {
	    newUser.profile = new CompanyProfile();
			newUser.profile.first_name = json.first_name;
			newUser.profile.last_name = json.last_name;
	    newUser.profile.company_name = json.company_name;
		}
		else return callback('Data is missing.',null);
  }
  else {
    return callback('User type is invalid.', null);
  }
	newUser.type = json.type;
	newUser.register_type = json.register_type;
  newUser.created = new Date();
  return callback(null, newUser);
}

function sendmail(user, token) {
  fs.readFile('./views/register_mail.html', 'utf8', function(err, data) {
    if(err) console.error("File not found." + err);
    else {
      mail = new sendGrid.Mail();
      email = new sendGrid.Email("info@splitshift.com", "Splitshift");
      mail.setFrom(email);
      mail.setSubject("Confirm your email address");

      personalization = new sendGrid.Personalization();
      email = new sendGrid.Email(user.email);
      personalization.addTo(email);

      substitution = new sendGrid.Substitution("%firstName%", user.profile.first_name);
      personalization.addSubstitution(substitution);
      substitution = new sendGrid.Substitution("%lastName%", user.profile.last_name);
      personalization.addSubstitution(substitution);
      substitution = new sendGrid.Substitution("%url%", config.confirm_url+token);
      personalization.addSubstitution(substitution);

      mail.addPersonalization(personalization);
      content = new sendGrid.Content("text/html", data);
      mail.addContent(content);

      var requestBody = mail.toJSON();
      var request = sg.emptyRequest();
      request.method = 'POST';
      request.path = '/v3/mail/send';
      request.body = requestBody;
      sg.API(request, function (response) {
        console.log(response.statusCode);
        console.log(response.body);
      });
    }
  });
}
