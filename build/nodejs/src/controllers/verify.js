var tools = require('./tools');

module.exports.verifyHR = function(req, res, next) {
  var token = req.headers.authorization;
  tools.verifyToken(token, res, function(err, decoded) {
    if(decoded) {
      tools.verifyKey(decoded.key, res, function(err, user) {
        if(user) {
          if(user.type == 'Employer') {
            req.user = user;
            req.company = user;
            next();
          }
          else {
            tools.verifyKey(user.profile.occupation.company_key, res, function(err, company) {
              if(company) {
                if(company.profile.employees.indexOf(user.key) == -1 || user.profile.rank !== 'HR') {
                  res.status(403).send({"message":"You are not HR.Can't use employer function."});
                }
                else {
                  req.user = user;
                  req.company = company;
                  next();
                }
              }
            });
          }
        }
      });
    }
  });
};

module.exports.verifyCompany = function(req, res, next) {
  var token = req.headers.authorization;
  tools.verifyToken(token, res, function(err, decoded) {
    if(decoded) {
      tools.verifyKey(decoded.key, res, function(err, user) {
        if(user) {
          if(user.type == "Employer") {
            req.user = user;
            next();
          }
          else {
            res.status(403);
            res.send({"message":"You are Employee.Can't use employer function."});
          }
        }
      });
    }
  });
};

module.exports.verifyUser = function(req, res, next) {
  console.log('verify User');
  var token = req.headers.authorization;
  tools.verifyToken(token, res, function(err, decoded) {
    if(decoded) {
      tools.verifyKey(decoded.key, res, function(err, user) {
        if(user) {
          if(user.type == "Employee") {
            req.user = user;
            next();
          }
          else {
            res.status(403);
            res.send({"message":"You are Employer.Can't use employee function."});
          }
        }
      });
    }
  });
};

module.exports.verifyAdmin = function(req, res, next) {
  console.log('verify Admin');
  var token = req.headers.authorization;
  tools.verifyToken(token, res, function(err, decoded) {
    if(decoded) {
      tools.verifyKey(decoded.key, res, function(err, user) {
        if(user) {
          if(user.type == "Admin") {
            req.user = user;
            next();
          }
          else {
            res.status(403);
            res.send({"message":"Only admin can use."});
          }
        }
      });
    }
  });
};

module.exports.verifyAll = function(req, res, next) {
  console.log('verify All');
  var token = req.headers.authorization;
  tools.verifyToken(token, res, function(err, decoded) {
    if(decoded) {
      tools.verifyKey(decoded.key, res, function(err, user) {
        if(user) {
          req.user = user;
          next();
        }
      });
    }
  });
};
