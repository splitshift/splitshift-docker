$(document).ready(function(){

  // Notification
  $(window).click(function() {
    //Hide the menus if visible
    $('.profile--menu').hide();
    $('.noti--menu').hide();
    $('.mesg--menu').hide();
    $('.connect--menu').hide();
  });

  // show and hide all menu on header
  $('#profile--menubtn').click(function(){
    event.stopPropagation();
    $('.profile--menu').toggle();
    $('.noti--menu').hide();
    $('.mesg--menu').hide();
    $('.connect--menu').hide();
  });
  // $('.noti--mesg').click(function(){
  //   event.stopPropagation();
  //   $('.mesg--menu').toggle();
  //   $('.noti--menu').hide();
  //   $('.profile--menu').hide();
  //   $('.connect--menu').hide();
  // });
  // $('.noti--noti').click(function(){
  //     event.stopPropagation();
  //     $('.noti--menu').toggle();
  //     $('.profile--menu').hide();
  //     $('.mesg--menu').hide();
  //     $('.connect--menu').hide();
  // });
  // $('.noti--connect').click(function(){
  //   event.stopPropagation();
  //   $('.connect--menu').toggle();
  //   $('.noti--menu').hide();
  //   $('.mesg--menu').hide();
  //   $('.profile--menu').hide();
  // });

  // what we offer for hover
  $('.banner--what').mouseenter(function(){
    $("body").addClass("overflow");
  });
  $('.banner--what').mouseleave(function(){
    $("body").removeClass("overflow")
  });
  $('.hover--banner').mouseleave(function(){
    $("body").removeClass("overflow");
  });

  // go to top
  $('#btn--gotop').click(function () {
      $("html, body").animate({ scrollTop: 0 }, "slow");
  });

  // header
  $(window).resize(function() {

    var headdersize = $(this).width();
     if (headdersize > 1200) {
      $(".drawer").removeClass("opened");
      $("body").removeClass("overflow");
      showDrawer = false;
    }

  });

  // close modal
  $('.btn-submit').click(function() {
    $('#myModal__primary').modal('hide');
  });

  // gmap for company and profile
  $('.popup-youtube, .popup-gmaps').magnificPopup({
  		disableOn: 700,
  		type: 'iframe',
  		mainClass: 'mfp-fade',
  		removalDelay: 160,
  		preloader: false,

  		fixedContentPos: false
  	});


  // information for register
  $('input[type=radio][name=type]').change(function() {
        if (this.value == 'employer') {
            $('.modal--employer').show();
        }
        else if (this.value == 'employee') {
            $('.modal--employer').hide();
        }
    });




  // profile and company
  $('.btn-photo').click(function() {
    $('.timeline-photo').show();
    $('.newsfeed').hide();
    $(window).scrollTop($('.timeline-photo').offset().top - 100);
  });

  $('.btn-timeline').click(function() {
    $('.timeline-photo').hide();
    $('.newsfeed').show();
    $(window).scrollTop($('.newsfeed').offset().top - 100);
  });

  // magnific-popup
  $('.image-link').magnificPopup({type:'image'});

  $('.popup-photo').magnificPopup({
    type: 'image'
    // other options
  });

  $('.popup-gallery').each(function() {
    $(this).magnificPopup({
		  delegate: 'a',
		  type: 'image',
		  tLoading: 'Loading image #%curr%...',
		  mainClass: 'mfp-img-mobile',
		  gallery: {
			  enabled: true,
			  navigateByImgClick: true,
			  preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		  },
		  image: {
			  tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			  titleSrc: function(item) {
				  return item.el.attr('title');
			  }
		  }
	  });
  });




  // btn--expand readmore and showless
  var checkbtn = 1;
  $(".btn--expand").click(function() {
    $(".text--expand").toggle();
    if (checkbtn === 1){
      $(".btn--expand").text("Show less..");
      checkbtn = 0;
    }else if (checkbtn === 0) {
      $(".btn--expand").text("Read more..");
      checkbtn = 1;
    }
  });

  // btn--expand Reviewer
  $(".review--btnexpand").click(function(){
    $($(this).data('target')).toggle();
    $("i",this).toggleClass("fa fa-chevron-up fa fa-chevron-down");
  });

  // modal header shift
  $(document).on('show.bs.modal', function(){
    var e = document.createElement('div');
    e.className = 'modal-scrollbar-measure';
    document.body.appendChild(e);
    var padding = e.offsetWidth - e.clientWidth;
    $('#header--bar').css('padding-right', padding);
    document.body.removeChild(e);
  });

  $(document).on('hidden.bs.modal', function () {
    $('#header--bar').css('padding-right', '');
  });

  $('.feed__reply').click(function() {
    $('.reply__comment').show();
    $('.feed__reply').css("display","none");
  });

  // advance search on jobs page
  $('.ad__search').click(function() {
    $('.jobbar__detail').css('display','block');
    $(this).css('display','none');
  });


  // owl-carousel tap on resource page


});
