var Jimp = require('jimp');

module.exports = function(path, cb) {
  Jimp.read(path, function (err, img) {
    if(err) return cb(err);
    if(img) {
      h = img.bitmap.height;
      w = img.bitmap.width;
      if(h > 960 || w > 960) {
        img.resize(w >= h?960:Jimp.AUTO, h >= w?960:Jimp.AUTO).write(path);
      }
    }
    return cb(null);
  });
};
