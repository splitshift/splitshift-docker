var mongoose = require('mongoose');

var Feed = mongoose.model('Feed');
var Resource = mongoose.model('Resource');

var config = require('../config');
var url = config.url.slice(0,-1);

module.exports.getMeta = function(route, type, id, callback) {
  var meta = {
    url: url + route,
    type: 'website',
    image: url + '/splitshift_banner.jpg',
    title: 'Splitshift',
    description: 'Welcome to Splitshift.com – the first online NETWORKING, EDUCATION and JOB MATCHING website connecting current and aspiring professionals of all levels in the hospitality and service industries throughout Thailand and Southeast Asia.'
  };

  if(type == 'feed') {
    Feed.findById(id, function(err, feed) {
      if(err) console.error(err);
      else if(feed) {
        meta.title = 'splitshift.com';
        meta.description = feed.text;
        if(feed.photos.length != 0)
          meta.image = url + feed.photos[0].substring(6);
        else if(feed.video)
          meta.image = "http://img.youtube.com/vi/" + feed.video.substring(24) + "/0.jpg";
      }
      return callback(meta);
    });
  }
  else if(type == 'resource') {
    Resource.findById(id, function(err, resource) {
      if(err) console.error(err);
      else if(resource) {
        meta.title = resource.title;
        meta.description = resource.short_description;
        meta.image = resource.featured_image ? url + resource.featured_image.substring(6) : "http://img.youtube.com/vi/" + (resource.featured_video ? resource.featured_video.substring(24) : "") + "/0.jpg";
      }
      return callback(meta);
    });
  }
  else return callback(meta);
};
