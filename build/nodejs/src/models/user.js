var mongoose = require( 'mongoose' );

var SubscribeSchema = new mongoose.Schema({
  email: { type:String, unique: true},
  date: Date
});

var UserSchema = new mongoose.Schema({
  key: {type: String, unique: true, required: true}, /* Random 0-9a-z 8 charactor */
  email: {type: String, unique: true, required: true},
  password: String,
  facebook_id: String,
  google_id: String,
  linkedin_id: String,
  type: String,
  profile: mongoose.Schema.Types.Mixed,
  address: {
     information: { type: String, default: '' },
     description: String,
     district: String,
     city: String,
     country: String,
     zip_code: String,
     location: {
       type: [Number], default: [1, 1], index: '2dsphere'
     }
  },
  followers: [String],
  following: [String],
  connected: [String],
  albums: [mongoose.Schema.Types.Mixed],
  photos: [mongoose.Schema.Types.Mixed],
  feeds: [mongoose.Schema.Types.Mixed],
  profile_image: String,
  banner: String,
  register_type: String,
  created: {type: Date, default: new Date() },
  confirm: {type: Boolean, default: false}
});

var UserProfileSchema = new mongoose.Schema({
  first_name: String,
  last_name: String,
  about: String,
  occupation: {
    type:mongoose.Schema.Types.Mixed,
    default: {experience_id: '', position: '', address: '', workplace: ''}
  },
  interests: [mongoose.Schema.Types.Mixed],
  skills: [String],
  languages: [mongoose.Schema.Types.Mixed],
  courses : [mongoose.Schema.Types.Mixed],
  experiences: [mongoose.Schema.Types.Mixed],
  educations: [mongoose.Schema.Types.Mixed],
  day_off_jobs: [mongoose.Schema.Types.Mixed],
  rank: { type:String, default:"Employee"},
  birth_day: Date,
  gender: String
});

var CompanyProfileSchema = new mongoose.Schema({
  first_name: String,
  last_name: String,
  company_name: String,
  type: {type: String, default: ''},
  overview: String,
  openjobs: {type: Number, default: 0},
  employees: [String],
  formers: [String],
  group: String,
  review: [mongoose.Schema.Types.Mixed],
  overall : {type: Number, default: 0},
  culture: String,
  benefits: [mongoose.Schema.Types.Mixed],
  links: mongoose.Schema.Types.Mixed,
  video: String,
  contact_email: String,
  tel: String,
});

var GallerySchema = new mongoose.Schema({
  image: String
});

var ExploreSchema = new mongoose.Schema({
  title:String,
  description: String,
  category: String
});

var ApplyJobSchema = new mongoose.Schema({
  from: mongoose.Schema.Types.Mixed,
  to: mongoose.Schema.Types.Mixed,
  tel: String,
  email: String,
  cover_letter: String,
  date: {type:Date, default:new Date()},
  files: [mongoose.Schema.Types.Mixed],
  job: mongoose.Schema.Types.Mixed,
  status: String
});

var ChatSchema = new mongoose.Schema({
  users: [String],
  messages: [
    {
      message: String,
      message_type: String,
      path: String,
      date: {type:Date, default:new Date()},
      owner: String
    }
  ],
  seen: mongoose.Schema.Types.Mixed
});

var RequestSchema = new mongoose.Schema({
  from: {
    key: String,
    name: String,
    profile_image: String,
    occupation: String,
    user_type: String
  },
  to: String,
  seen: {type: Boolean, default: false},
  date: {type: Date, default: new Date()}
});


mongoose.model('Request', RequestSchema);
mongoose.model('Subscribe', SubscribeSchema);
mongoose.model('User', UserSchema);
mongoose.model('UserProfile',UserProfileSchema);
mongoose.model('CompanyProfile',CompanyProfileSchema);
mongoose.model('Gallery',GallerySchema);
mongoose.model('Explore',ExploreSchema);
mongoose.model('ApplyJob',ApplyJobSchema);
mongoose.model('Chat', ChatSchema);

mongoose.model('User').on('index', function(err) {if(err) console.error('error:',err);})
