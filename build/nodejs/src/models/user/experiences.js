var mongoose = require('mongoose');

var ExperienceSchema = new mongoose.Schema({
  position: String,
  workplace: String,
  address: mongoose.Schema.Types.Mixed,
  company_key: String,
  role: String,
  start_date: Date,
  end_date: Date
});

mongoose.model('Experience',ExperienceSchema);
