var mongoose = require('mongoose');

var InterestSchema = new mongoose.Schema({
  category: String,
  value: String
});

mongoose.model('Interest',InterestSchema);
