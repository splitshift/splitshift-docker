var mongoose = require('mongoose');

var TrainingCourseSchema = new mongoose.Schema({
  name : String,
  start_date : Date,
  end_date : Date,
  description: String,
  link: String
});

mongoose.model('Course', TrainingCourseSchema);
