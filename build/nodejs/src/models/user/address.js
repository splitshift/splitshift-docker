var mongoose = require('mongoose');

var AddressSchema = new mongoose.Schema({
  lat: Number,
  lng: Number,
  information: String
});

mongoose.model('Address',AddressSchema);
