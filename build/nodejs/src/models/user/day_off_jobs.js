var mongoose = require('mongoose');

var DayOffJobSchema = new mongoose.Schema({
  name: String,
  contact: String,
  fee: String,
  detail: String
});

mongoose.model('DayOffJob',DayOffJobSchema);
