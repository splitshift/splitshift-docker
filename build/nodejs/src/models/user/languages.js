var mongoose = require('mongoose');

var LanguageSchema = new mongoose.Schema({
  language: String,
  proficiency: String
});

mongoose.model('Language',LanguageSchema);
