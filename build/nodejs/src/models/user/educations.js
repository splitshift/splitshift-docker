var mongoose = require('mongoose');

var EducationSchema = new mongoose.Schema({
  field: String,
  school: String,
  start_date: Date,
  graduate_date: Date,
  description: String,
  gpa: { type: Number, min:0.00 , max:4.00}
});

mongoose.model('Education',EducationSchema);
