var mongoose = require( 'mongoose' );

var ImageSchema = new mongoose.Schema({
  path: String,
  owner: String
});

var AlbumSchema = new mongoose.Schema({
  name: String,
  photos: [mongoose.Schema.Types.ObjectId],
  date: {type:Date, default:new Date()}
});

var PhotoSchema = new mongoose.Schema({
  path: String,
  feed_id: mongoose.Schema.Types.ObjectId,
  album_id: mongoose.Schema.Types.ObjectId,
  privacy: String,
  date: {type:Date, default:new Date()}
});

var FeedSchema = new mongoose.Schema({
  text: String,
  photos: [String],
  video: String,
  likes: [String],
  comments: [mongoose.Schema.Types.Mixed],
  date: {type:Date, default:new Date()},
  privacy: String,
  owner: {
    key: String,
    name: String,
    profile_image: String,
    user_type: String
  }
});

var CommentSchema = new mongoose.Schema({
  comment: String,
  date: {type:Date, default:new Date()},
  owner: {
    key: String,
    name: String,
    profile_image: String,
    user_type: String
  },
  replies: [mongoose.Schema.Types.Mixed]
});

var ReplySchema = new mongoose.Schema({
  reply: String,
  owner: {
    key: String,
    name: String,
    profile_image: String,
    user_type: mongoose.Schema.Types.Mixed
  },
  date: {type:Date, default:new Date()}
});

mongoose.model('Reply', ReplySchema);
mongoose.model('Comment', CommentSchema);
mongoose.model('Feed', FeedSchema);
mongoose.model('Photo', PhotoSchema);
mongoose.model('Album', AlbumSchema);
mongoose.model('Image', ImageSchema);
