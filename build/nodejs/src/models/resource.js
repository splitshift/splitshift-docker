var mongoose = require('mongoose');

var ResourceSchema = new mongoose.Schema(
  {
    title: String,
    short_description: String,
    content: String,
    featured_image: String,
    featured_video: String,
    category: String,
    department: String,
    comments: [mongoose.Schema.Types.Mixed],
    like: [String],
    owner: {
      key: String,
      name: String,
      profile_image: String,
      user_type: String
    },
    is_approved: {type: Boolean, default: false},
    date: {type: Date, default: new Date()}
  }
);

var CommentResourceSchema = new mongoose.Schema(
  {
    comment: String,
    date: {type:Date, default:new Date()},
    owner: {
      key: String,
      name: String,
      profile_image: String,
      user_type: String
    }
  }
);

mongoose.model('Resource', ResourceSchema);
mongoose.model('CommentResource', CommentResourceSchema);
