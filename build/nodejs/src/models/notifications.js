var mongoose = require('mongoose');

var NotificationSchema = new mongoose.Schema({
  from: {
    key: String,
    name: String,
    profile_image: String,
    user_type: String
  },
  to: String,
  notification_type: String,
  feed: {
    owner: String,
    feed_id: mongoose.Schema.Types.ObjectId,
    comment_id: mongoose.Schema.Types.ObjectId,
    reply_id: mongoose.Schema.Types.ObjectId
  },
  resource: {
    resource_id: mongoose.Schema.Types.ObjectId,
    comment_id: mongoose.Schema.Types.ObjectId
  },
  applyjob: mongoose.Schema.Types.ObjectId,
  seen: {type: Boolean, default: false},
  date: {type: Date, default: new Date()}
});

mongoose.model('Notification', NotificationSchema);
