var mongoose = require('mongoose');

var ActivitiesSchema = new mongoose.Schema({
  key: String,
  activity: {
    owner: String,
    feed_id: mongoose.Schema.Types.ObjectId,
    comment_id: mongoose.Schema.Types.ObjectId,
    reply_id: mongoose.Schema.Types.ObjectId,
    resource_id: mongoose.Schema.Types.ObjectId
  }
});

mongoose.model('Activity',ActivitiesSchema);
