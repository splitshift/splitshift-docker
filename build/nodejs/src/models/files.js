var mongoose = require( 'mongoose' );

var FilesSchema = new mongoose.Schema({
  path: String,
  original_name: String,
  owner: String
});

mongoose.model('File', FilesSchema);
