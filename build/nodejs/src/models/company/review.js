var mongoose = require('mongoose');

var ReviewSchema = new mongoose.Schema({
  from: {
    key: String,
    name: String,
    profile_image: String,
    user_type: String
  },
  overall: Number,
  work: Number,
  benefit: Number,
  career: Number,
  recommend: Number,
  date: {type:Date, default:new Date()}
});

mongoose.model('Review', ReviewSchema);
