var mongoose = require('mongoose');

var LinkSchema = new mongoose.Schema({
  facebook: String,
  instagram: String,
  linkedin: String,
  youtube: String,
  website: String
});

mongoose.model('Link',LinkSchema);
