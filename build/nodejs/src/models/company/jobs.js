var mongoose = require('mongoose');

var JobSchema = new mongoose.Schema({
  position: String,
  description: String,
  type: String,
  min_year: Number,
  required_skills: [String],
  compensation: String,
  urgent: {type: Boolean, default: false},
  activated: {type: Boolean, default: true},
  expire_date: Date,
  owner: {
    company_name: String,
    key: String,
    profile_image: String,
    company_type: { type: String, default: '' },
    address: {
       information: { type: String, default: '' },
       description: {type: String, default: ''},
       district: {type: String, default: ''},
       city: {type: String, default: ''},
       country: {type: String, default: ''},
       zip_code: {type: String, default: ''},
       location: { type: [Number], default: [1, 1], index: '2dsphere' }
    },
    contact_email: String
  },
  date: { type: Date, default: new Date() }
});

mongoose.model('Job', JobSchema);
mongoose.model('Job').on('index', function(err) {if(err) console.error('error:',err);})
