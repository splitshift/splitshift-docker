var mongoose = require('mongoose');

var BenefitSchema = new mongoose.Schema({
  description: String,
  category: String
});

mongoose.model('Benefit',BenefitSchema);
