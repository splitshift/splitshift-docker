import moment from 'moment';

let showDrawer = false;
let showSidebar = false;

const JQueryTool = {
  triggerUpload(name, evt) {
    console.log("Uploda Click");
    $("#" + name).click();
  },
  getMonth(date) {
    return moment(date).format("MM");
  },
  getYear(date) {
    return moment(date).format("YYYY");
  },
  changeDateFormat(date, lang='en') {
    moment.locale(lang);
    return moment(date).add('years', lang === 'th' ? 543 : 0).format("MMMM YYYY");
  },
  changeDatePickerFormat(date) {
    return moment(date).format("YYYY-MM-DD");
  },
  changeBirthDayDatePickerFormat(date) {
    return moment(date).format("DD-MM-YYYY");
  },
  changeBirthDayFormat(date) {
    return moment(date).format("DD MMMM YYYY");
  },
  changeDateAgo(date) {
    return moment(date).fromNow();
  },
  diffExperienceDate(start_date, end_date, lang='en') {
    let start = moment(start_date);
    let end = moment(end_date);

    let month_diff = end.diff(start, 'months');
    let year_diff = 0;

    if(month_diff > 12) {
      year_diff = parseInt(month_diff / 12);
      month_diff %= 12;
    }

    let wording = "";

    if(year_diff > 0)
      wording += year_diff + (lang === 'th' ? " ปี" : " Year") + (year_diff > 1 && lang === 'en'? "s " : " ");

    wording += month_diff + (lang === 'th' ? " เดือน" : " Month") + (month_diff > 1 && lang === 'en'? "s" : "");

    return wording;

  },
  clickEditMenu(evt) {

    var $dropdown = $(evt.target).find('.edit-menu__dropdown')

    $dropdown.toggleClass('opened');

    if ($dropdown.hasClass('opened'))
      event.stopPropagation();

    $('body').one('click', function (event) {
      if (!$(event.target).closest('.edit-menu').length)
        $dropdown.removeClass('opened');
    });

  },
  triggerOwlCarousel() {
    $('.owl-carousel').owlCarousel({
      loop: false,
      margin: 20,
      nav: true,
      responsive:{
          300:{
              items:1
          },
          550:{
              items:2
          },
          730:{
              items:3
          },
          1000:{
              items:4
          },
          1200:{
              items:5
          }
      },
      navText: [
        "", ""]
    });
  },
  drawer() {
    $(".topbar__btn--toggle").click(function() {
      event.stopPropagation();
      if(showDrawer === false){
        $(".drawer").addClass("opened");
        $("body").addClass("overflow");
        showDrawer = true;
      }else {
        $(".drawer").removeClass("opened");
        $("body").removeClass("overflow");
        showDrawer = false;
      }
    });
  },
  moreinfo() {
    let checkinfo = true;
		let showDetail = false;

		$("#more-info").click(function() {
      if(checkinfo === true) {
        $("#more-info-detail").show();
        $(this).text("Less Infomation");
        showDetail = true;
        checkinfo = false;
      } else if(checkinfo === false) {
        $("#more-info-detail").hide();
        $(this).text("More Infomation");
        showDetail = false;
        checkinfo = true;
        if($.contains('.more')) {
          $(window).scrollTop($('.more').offset().top);
        }
      }
    });
  },
  popupGallery() {
    $('.popup-gallery').each(function() {
      $(this).magnificPopup({
  		  delegate: 'a',
  		  type: 'image',
  		  tLoading: 'Loading image #%curr%...',
  		  mainClass: 'mfp-img-mobile',
  		  gallery: {
  			  enabled: true,
  			  navigateByImgClick: true,
  			  preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  		  },
  		  image: {
  			  tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
  			  titleSrc: function(item) {
  				  return item.el.attr('title');
  			  }
  		  }
  	  });
    });
  },
  backendDrawer() {
    var showDetail = true;
    var checkinfo = true;

    $(window).resize(function() {

      var windowsize = $(this).width();

      if (windowsize > 990) {
          $("#Mysidenav").css('width',"230px");
          $('#main header#admin__header').css('margin-left','230px');
          $('.container-content').css('margin-left','230px');
          // $('.container-content').css('margin-right','-230px');
          showSidebar = true;
      }else {
        if(showSidebar === true){
          $("#Mysidenav").css('width',"0px");
          $('#main header#admin__header').css('margin-left','0px');
          $('.container-content').css('margin-left','0px');
          $('.container-content').css('margin-right','0px');
          showSidebar = false;
        }
      }

       if (windowsize > 1000) {
        $("#more-info-detail").hide();
        $("#more-info").parent('div').parent('div').parent('div').hide();
        $("#more-info").text("More Infomation");
        showDetail = false;
        checkinfo = true;
        if($.contains('.more'))
          $(window).scrollTop($('.more').offset().top - 100);
      }else {
        if(!showDetail){
          $("#more-info").text("More Infomation");
          $("#more-info").parent('div').parent('div').parent('div').show();
          checkinfo = true;
          if($.contains('.more'))
            $(window).scrollTop($('.more').offset().top - 100);
        }
      }

    });

    $('.sidebar__toggle').click(function() {
      if(showSidebar === false){
        $("#Mysidenav").css('width',"230px");
        $('#main header#admin__header').css('margin-left','230px');
        $('.container-content').css('margin-right','-230px');
        $('.container-content').css('margin-left','230px');
        showSidebar = true;
      }else if(showSidebar === true){
        $("#Mysidenav").css('width',"0px");
        $('#main header#admin__header').css('margin-left','0px');
        $('.container-content').css('margin-left','0px');
        $('.container-content').css('margin-right','0px');
        showSidebar = false;
      }
    });
  },
  sidebarToggle() {


    var windowsize = $(window).width();

    if(windowsize <= 990) {
      if(showSidebar === false){
        $("#Mysidenav").css('width',"230px");
        $('#main header#admin__header').css('margin-left','230px');
        $('.container-content').css('margin-right','-230px');
        $('.container-content').css('margin-left','230px');
        showSidebar = true;
      }else if(showSidebar === true){
        $("#Mysidenav").css('width',"0px");
        $('#main header#admin__header').css('margin-left','0px');
        $('.container-content').css('margin-left','0px');
        $('.container-content').css('margin-right','0px');
        showSidebar = false;
      }
    }

  },
  showAdvanceSearch(evt) {
    $('.jobbar__detail').css('display','block');
    $(evt.target).css('display','none');
  },
  toggleDrawer() {
    $(".drawer").removeClass("opened");
    $("body").removeClass("overflow");
    showDrawer = false;
  },
  toPostRegister() {
    // Next Step
    $("#myModal__Register").modal('hide');
    setTimeout(function() { $("#myModal__Regisform").modal('show') }, 500)
  },
  animateScrollTo(id) {
    $('html, body').animate({
        scrollTop: $(id).offset().top - 200
    }, 1000);
  }
}

export default JQueryTool;
