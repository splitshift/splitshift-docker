const configLang = {
  "en": {
    heroCover: ["FIND BETTER JOBS.", "FIND GREAT PEOPLE."],
    heroDetail: "Connect | Discover | Excel",
    registerBtn: "Create Your FREE Profile",
    registerHeader: "Join the Hospitality Community today.",
    formData: ["Employee","Employer","Company Name","Firstname","Lastname","Email address","Sending...","Submit"],
    wwo: ['WHAT', 'WE', 'OFFER'],
    form: {
      register: ['Employee', 'Employer', 'Company Name', 'Firstname', 'Lastname', 'Email address', 'I agree to Splitshift’s', 'User Agreement', 'Register'],
      pre_register: ['Register with email', 'Already a member?', 'Log in'],
      login: ['Email', 'Password', 'Remember me', 'Forgot password?', 'SIGN IN', 'Don\'t have an account?', 'Sign up'],
      password: 'Password',
      confirm_password: 'Confirm Password'
    },
    resume: {
      interest: 'My Interests',
      skill: 'Skills',
      about_me: 'About Me',
      work_exp: 'Work Experience',
      education: 'Education',
      course: 'Course',
      languages: 'Languages'
    },
    wwoHeader: [
      "Showcase your unique Profile",
      "Connect with people",
      "Search Jobs",
      "Explore Companies",
      "Online Education Resources",
      "Community Buzz"
    ],
    wwoDetail: [
      "Your current colleagues, boss and friends know who you really are but what about potential employers? Your unique Splitshift profile gives companies an insight into the real you and your dedication, interests, skills and talents.",
      "As the old saying goes it’s not what you know it’s who you know. Make a host of useful connections in the Splitshift community and you never know what golden opportunities might be just around the corner.",
      "With a fully-interactive map and essential information regarding your potential employer’s business and location plus real world reviews from existing staff Splitshift’s job search is tailor-made to help you find that perfect position.",
      "Everything you need to know about who could potentially be your next employer and the start of a new and exciting chapter in your life. Here you will find all the essential facts and figures literally at your fingertips.",
      "Think of Splitshift Resources as your own private online tutor with a vast array of hospitality topics that are geared towards you and giving you the chance to up your game and become a star in the job-seeker’s market.",
      "Find out what’s ‘buzzing’ in the world of hospitality in Thailand and around Southeast Asia. Get the latest news from your favorite companies and see what your work colleagues and like-minded industry professionals are up to."
    ],
    whysplitshift: {
      header: "WHAT IS<span> SPLITSHIFT</span>",
      paragraphs: [
        "Welcome to Splitshift.com – the first online NETWORKING, EDUCATION and JOB MATCHING website connecting current and aspiring professionals of all levels in the hospitality and service industries throughout Thailand and Southeast Asia.",
        "Your current colleagues, boss, friends and family know who you really are but what about potential employers? Your unique Splitshift profile gives companies an insight into the real you and your passions, interests and talents.",
        "Connect directly with like-minded professionals and hiring managers and build your very own industry network. Follow your dream company and get firsthand inside into their work environment and company culture.",
        "Take advantage of our free Education Resources as well as virtual mentorship, inspiration and advice from industry professionals.",
        "For Businesses, <a href=\"http://www.splitshift.com\">Splitshift.com</a> offers an effective and pro-active way to source all levels of hospitality positions via our automatic matching process, which tees up vacancies to matching talents.",
      ],
      readmore: "Read More",
      readless: "Show Less"
    },
    stat: [
      'SPLITSHIFT', 'STATS', 'members', 'jobs', 'companies'
    ],
    headers: [
      'BUZZ', 'PEOPLE', 'JOBS', 'COMPANIES', 'RESOURCES', 'Employer', 'Login'
    ],
    aboutus: {
      header: "SERVING THE WORLD OF SERVICE",
      vision: {
        header: "<span5>OUR <span>VISION</span></span5>",
        detail: "Our vision is to build an online community that brings like-minded people together and provides stepping-stones and inspiration for everyone interested in the service sector in Southeast Asia – one person, one job and one career at a time."
      },
      whatis: {
        header: "<span5>WHAT IS <span>SPLITSHIFT</span></span5>",
        detail: "Splitshift.com is the first professional online Networking, Education and Job Matching website connecting current and aspiring professionals in the hospitality and service industries throughout Thailand and Southeast Asia."
      },
      why: {
        header: "<span5>WHY <span>SPLITSHIFT</span></span5>",
        detail: [
          "Splitshift.com allows everyone interested in the service sector to create a professional and personalized profile that serves as ‘online resume’ for jobseekers, and as ‘employer branding platform’ for businesses and hiring managers. ",
          "Rather than focusing purely on work experience and education, Splitshift.com encourages people to showcase more of their personality, interests and passions and likewise for businesses to offer insider information about their work environment and company culture. ",
          "People can browse for jobs via advanced search options, receive relevant job alerts and follow their favourite companies.",
          "Hiring managers can post jobs, receive candidate alerts, browse through all the available profiles and instantly reach out to candidates that match their requirements rather than waiting for them to apply.",
          "Splitshift.com enables a seamless two-way communication between users and hiring managers which makes the entire recruitment process much more personal and efficient for everyone.",
          "Splitshift.com offers free access to a vast archive of online training and education resources provided by our community members including video tutorials, online seminars, training manuals, virtual mentorship and advice from industry professionals as well as useful checklists, templates and references related to the hospitality and service industry.",
          "Splitshift.com aims to provide the basic educational and practical tools for aspiring professionals who want to learn more about the traits and skills required and boost their confidence to pursue a meaningful career in hospitality and other service industries."
        ]
      }
    },
    buzz: {
      search: 'search for posts by keywords...'
    },
    feed: {
      post: {
        employee: 'Show your work and interests ...',
        employer: 'What’s new at your workplace...',
        action: ['Post', 'Edit Post']
      },
      card: {
        type: ['Photo', 'Video'],
        action: ['Like', 'Share', 'Comment', 'Like'],
        comment: 'Write a comment'
      },
      status: {
        public: 'Public',
        timeline: 'My Timeline',
        connected: 'Connection',
        employer: 'Employer',
        onlyme: 'Only Me',
        employee: 'Employee'
      }
    },
    people_search: {
      header: 'PEOPLE YOU MAY KNOW',
      intro: 'Make Great Connections & Build Your Industry Network',
      see_profile: 'See Profile',
      people: ['People', 'Name, Company, Keywords ...'],
      location: ['Location', 'City, District ...']
    },
    job_search: {
      intro: 'Search Thousands Hospitality and Service Industry Jobs',
      job: ['Job', 'Job Title, Keywords, Company ...'],
      location: ['Location', 'City, District ...']
    },
    company_search: {
      intro: 'Find Your Dream Company',
      company: ['Company', 'Company Name, Keywords ...'],
      location: ['Location', 'City, District ...']
    },
    advanced_search: {
      title: 'Advanced Search',
      interest_title: 'Interests',
      interest: [
        "Sport",
        "Art",
        "Photography",
        "Design",
        "Health/Fitness",
        "Music",
        "Movies",
        "Reading",
        "Traveling",
        "Cooking",
        "Shopping",
        "Fashion",
        "Food & Drinks",
        "Cocktails",
        "Technology",
        "Books",
        "Pets",
        "History",
        "Dancing",
        "Writing",
        "Social Media",
        "Cars",
        "Motorcycle",
        "Gaming",
        "Architecture",
        "Innovation",
        "Entrepreneur",
        "Finance",
        "Wealth",
        "Politics",
        "Nature",
        "Management",
        "Career",
        "Marketing",
        "Sales",
        "Philosophy",
        "Self-Improvement",
        "Education",
        "Other"
      ],
      workexp_title: 'Work Experience',
      workexp: {"[0,1]": "Less than one year", "[1,2]": "Between 1 and 2 years", "[2,3]": "Between 2 and 3 years", "[3,5]": "Between 3 and 5 years", "[5,100]": "More than 5 years" },
      distance_title: 'Distance',
      distance: {5000: "Within 5 km", 10000: "Within 10 km", 15000: "Within 15 km", 20000: "Within 20 km", 25000: "Within 25 km", 50000: "Within 50 km", 100000: "Within 100 km"},
      date_posted_title: 'Date Posted',
      date_posted: {1: "Past 24 Hours",7: "Past Week",14: "Past 2 Weeks",30: "Past Month",60: "Past 2 Months",90: "Past 3 Months" },
      job_type_title: 'Job Type',
      job_type: ["All Job Types", "Full Time", "Part Time", "Temporary", "Casual/Hourly", "Internship", "Freelance", "Contract"],
      compensation_title: 'Compensation',
      compensation: ["Based on experience", "< 9000 THB", "9,000 - 15,000 THB", "15,000 - 30,000 THB", "30,000 - 50,000 THB", "50,000 - 100,000 THB", "100,000 - 200,000 THB", "> 200,000 THB"],
      company_type_title: 'Company Type',
      company_type: ["All", "Hotel", "Restaurant", "Bar", "Nightclub", "Retail", "Tourism", "Spa", "Catering", "Others"],
      employee_rating_title: 'Employee Rating',
      lastest_jobs_title: 'Latest Jobs'
    },
    user_profile: {
      menus: ['Your Address', 'Timeline', 'Photos', 'My Resume', 'Send Message', 'Follow', 'Following', 'Request Sent', 'Connect', 'Connected', 'connection', 'View your public profile', 'Edit Profile'],
      aboutme: 'About Me',
      aboutme_placeholder: 'Tell people a little bit about yourself...what motivates you? What is your dream job?',
      workexp: {
        header: 'Work Experience',
        add: 'ADD New Experience'
      },
      interest: {
        header: 'My Interests',
        list: [
          "Sport",
          "Art",
          "Photography",
          "Design",
          "Health/Fitness",
          "Music",
          "Movies",
          "Reading",
          "Traveling",
          "Cooking",
          "Shopping",
          "Fashion",
          "Food & Drinks",
          "Cocktails",
          "Technology",
          "Books",
          "Pets",
          "History",
          "Dancing",
          "Writing",
          "Social Media",
          "Cars",
          "Motorcycle",
          "Gaming",
          "Architecture",
          "Innovation",
          "Entrepreneur",
          "Finance",
          "Wealth",
          "Politics",
          "Nature",
          "Management",
          "Career",
          "Marketing",
          "Sales",
          "Philosophy",
          "Self-Improvement",
          "Education",
          "Other"
        ]
      },
      skill: {
        header: 'Skills',
        placeholder: 'Add your skill i.e. singing, painting'
      },
      education: {
        header: 'Education',
        add: 'ADD New Education'
      },
      course: {
        header: 'Training and Courses',
        add: 'ADD New Course'
      },
      following: 'Following',
      language: {
        header: 'Languages',
        add: 'Add Language'
      },
      editor: {
        add: 'ADD',
        edit: 'EDIT',
        delete: 'DELETE',
        link: 'Link',
        cancel: 'Cancel',
        upload: {
          profile: 'upload profile picture',
          banner: ['Upload Background', 'Choose from your device', 'Choose template']
        },
        card: ['Edit Your Profile', 'Firstname Lastname', 'Your home address', 'Home address will not be visible to others', 'Date of birth ex. 21-07-1998', 'Your birth date will not be visible to others', 'Select Gender', 'Male', 'Female', 'Save Profile Changes', 'Cancel'],
        workexp: ['Company Name', 'Position', 'Address', 'District', 'City', 'Zipcode', 'Start Date', 'End Date', 'Current Job'],
        education: ['School', 'Field', 'Description', 'Start Date', 'End Date'],
        course: ['Name', 'Link', 'Description', 'Start Date', 'End Date'],
        language: ['Language', 'Native Language', 'Fluent', 'Advanced', 'Intermediate', 'Beginner']
      }
    },
    company_profile: {
      menus: ['Timeline', 'Photos', 'See Public Profile', 'Edit Your Profile', 'Send Message', 'Company Profile', 'Follow', 'Following'],
      review: {
        summary: 'Company Rating Summary',
        lastest: 'Latest Reviews',
        side: ['Overall', 'Work Environment and Culture', 'Compensation and Benefits', 'Career Opportunities', 'Recommend to a friend']
      },
      benefit_title: 'Benefits',
      benefit: [
        "5 Days Work Week",
        "Free Duty Meals",
        "Free Uniform",
        "Provident Fund",
        "Social Security Fund",
        "Health Insurance",
        "Workmen's Compensation Fund",
        "Out-patient Medical Claim",
        "Service Charge",
        "Paid Vacation and Public Holidays",
        "Wedding Leave & Allowance",
        "Child Birth Allowance",
        "Funeral Allowance",
        "Annual Bonus Plan",
        "Life Insurance",
        "Annual Check-up",
        "Training & Development",
        "Loyalty & Recognition Awards",
        "Staff Discounts"
      ],
      editor: {
        add: 'ADD',
        edit: 'EDIT',
        delete: 'DELETE',
        link: 'Link',
        cancel: 'Cancel',
        upload: {
          profile: 'Upload Company Logo',
          banner: ['Upload Background', 'Choose from your device', 'Choose template']
        }
      },
      header: {
        positions: 'Positions',
        people: 'People',
        gallery: 'Gallery',
        culture: 'Culture',
        benefits: 'Benefits',
        location: 'Location',
        overview: 'Overview',
        videos: 'Videos'
      }
    },
    menus: {
      action: ['My Profile', 'Main Menu', 'Settings', 'Log Out'],
      notification: ['Messages', 'Notifications', 'Pending Invitations']
    },
    backend: {
      sidebar: ['My Profile', 'Company Profile', 'Statistics', 'Job Posting', 'Job Applications', 'Applications', 'Add Resource', 'HR Management', 'Settings'],
      resource: {
        header: ['Resource Management', 'Resource Name', 'Owner', 'Status'],
        add: 'Add New Resource',
        title: 'Create Resource',
        form: ['Title', 'Category', 'Department', 'Featured', 'Image', 'Video', 'Short Description', 'Content'],
        category: ["Videos", "Seminars/Courses", "Training Manuals", "Templates/Checklists", "Articles/Links", "Virtual Mentorship"],
        department: [
          "Cross-Departmental",
          "F&B Service",
          "Culinary",
          "Housekeeping",
          "Sales & Marketing",
          "Finance",
          "Front Office",
          "Human Resources",
          "Engineering",
          "IT",
          "Security",
          "Miscellaneous"
        ],
        action: ['Create Resource', 'Edit Resource', 'Cancel']
      },
      user: {
        application: ['Application List', 'Company', 'Information', 'Status']
      }
    },
    setting: {
      title: 'Settings',
      delete: [
        'Delete your account',
        "Deactivating your account will disable your profile and remove your name and photo from most things you've shared on Splitshift. Some information may still be visible to others, such as your name in their connection list and messages you sent.",
        'I want to remove my account'
      ],
      password: [
        'Change your password',
        'Old Password',
        'Your old password',
        'New Password',
        'Type your new password',
        'Type your new password again',
        'Change Password'
      ],
      primary: [
        'Change your primary email',
        'Your primary email',
        'Set new primary email',
        'Type your primary email',
        'Confirm new primary email'
      ]
    },
    resource: {
      title: 'TAP INTO OUR VAST ARCHIVE OF INDUSTRY RESOURCES.',
      search_placeholder: 'Keyword, Department  ...',
      department: 'DEPARTMENTS',
      category: 'CATEGORY',
      subtitle: 'Share your knowledge & get recognized by our community',
      add: 'Add Resource',
      all: 'All',
      header1: ['AVAILABLE', 'RESOURCES'],
      header2: ['LASTEST', 'RESOURCES']
    },
    footer: {
      mail: 'Need help?',
      about: ['About', 'About us', 'Login', 'Register', 'Privacy and data policy', 'User agreement'],
      discover: ['Discover', 'Community blog', 'Jobs', 'Companies', 'Networking', 'Resources', 'EMPLOYER'],
      setting: ['Settings', 'Language', 'Country'],
      newsletter: ['Newsletter', 'Email Address...', 'Subscribe']
    }
  },
  "th": {
    heroCover: ["ค้นหางานที่ดียิ่งขึ้น", "พร้อมกับคนทำงานที่มีคุณภาพ"],
    heroDetail: "เชื่อมโยง | เฟ้นหา | เพื่อความเป็นเลิศ",
    registerBtn: "สร้างประวัติส่วนตัวของคุณ ฟรี",
    registerHeader: "ร่วมเป็นสมาชิกชุมชนคนทำงานบริการ วันนี้",
    formData: ["บุคคลทั่วไป","บริษัท","ชื่อบริษัท","ชื่อจริง","นามสกุล","อีเมล์","กำลังส่ง...","ส่งข้อมูล"],
    wwo: ['สิ่งที่', 'เรา', 'นำเสนอ'],
    form: {
      register: ['ลูกจ้าง', 'นายจ้าง', 'ชื่อบริษัท', 'ชื่อจริง', 'นามสกุล', 'อีเมล์', 'ยอมรับ', 'ข้อตกลงของเว็บไซต์', 'ลงทะเบียน'],
      pre_register: ['เลือกลงทะเบียนด้วย อีเมล์', 'คุณเป็นสมาชิกแล้วหรือไม่', 'Log in'],
      login: ['อีเมล์', 'พาสเวิร์ด', 'จดจำฉัน', 'ลืมรหัสผ่าน', 'เข้าสู่ระบบ', 'ถ้ายังไม่มีประวัติ', 'ลงทะเบียนใหม่'],
      password: 'รหัสผ่าน',
      confirm_password: 'ยืนยันรหัสผ่าน'
    },
    resume: {
      interest: 'ความสนใจของฉัน',
      skill: 'ทักษะ',
      about_me: 'เกี่ยวกับฉัน',
      work_exp: 'ประสบการณ์การทำงาน',
      education: 'การศึกษา',
      course: 'การอบรมและหลักสูตร',
      languages: 'ภาษา'
    },
    wwoHeader: [
      "แสดงประวัติส่วนตัวของคุณให้โดดเด่นเหนือใคร",
      "รอบรู้ข้อมูลเกี่ยวกับบริษัทในฝันของคุณ",
      "เชื่อมโยงผู้คนเข้าด้วยกัน",
      "แหล่งข้อมูลการเรียนรู้ออนไลน์",
      "ค้นหาตำแหน่งงานว่าง",
      "แบ่งปันความสามารถของคุณ"
    ],
    wwoDetail: [
      "คุณจะนำหน้าคนอื่นไปอีกขั้นและดึงดูดบริษัทต่างๆให้สนใจตัวคุณมากขึ้นด้วยรูปแบบประวัติส่วนตัวที่เป็นเอกลักษณ์ของ Splitshift ซึ่งสามารถแสดงถึงบุคลิกภาพ ความสนใจและความรักในงานของคุณได้อย่างชัดเจน",
      "คุณสามารถค้นหาข้อมูลต่างๆที่คุณต้องการทราบเกี่ยวกับบริษัทที่คุณจะทำงานด้วยในอนาคต และเตรียมความพร้อมในการเริ่มต้นทำงานใหม่ที่น่าติ่นเต้นของคุณ",
      "คุณจะได้ประโยชน์จากความสัมพันธ์ในชุมชนSplitshiftอย่างเต็มที่ รวมถึงได้รับโอกาสที่เปิดกว้างให้คุณอยู่เสมอจากการบอกต่อแบบมิตรภาพของสมาชิกในเว็บไซต์",
      "Splitshift เป็นติวเตอร์ส่วนตัวของคุณที่พรั่งพร้อมด้วยความรู้ที่กว้างขวางในสายงานบริการซึ่งจะนำพาคุณไปสู่โอกาสความก้าวหน้าในอาชีพและมีคุณสมบัติที่นายจ้างต้องการ",
      "Splitshift ช่วยค้นหาตำแหน่งที่เหมาะกับคุณ ด้วยระบบการวางแผนแบบอินเตอร์แอคทีฟซึ่งให้คุณสามารถหาข้อมูลของสายงานที่คุณต้องการและเชื่อมโยงเข้ากับตำแหน่งงานว่างในปัจจุบัน รวมทั้งทราบรีวิวจากพนักงานที่ทำงานอยู่ในบริษัทนั้นๆอีกด้วย",
      "หากคุณกำลังมองหารายได้เสริมอยู่ คุณสามารถโพสต์หางานพิเศษของคุณในชุมชนSplitshiftเพื่อสร้างรายได้เสริมในเวลาว่าง สมาชิกของเรากำลังมองหาและต้องการจ้างคนที่มีความสามารถเช่นคุณ"
    ],
    whysplitshift: {
      header: "<span>SPLITSHIFT</span> คืออะไร",
      paragraphs: [
        "ยินดีต้อนรับเข้าสู่ Splitshift.com เว็บไซต์แห่งแรกของโลกที่เป็นศูนย์รวมเครือข่ายทางด้านอาชีพ ความรู้ และการจัดหางาน เพื่อให้คุณสามารถเชื่อมต่อและพัฒนาตนเองในสายงานอุตสาหกรรมโรงแรมและการบริการในประเทศไทยและภูมิภาคเอเชียตะวันออกเฉียงใต้",
        "ในตอนนี้เพื่อนร่วมงาน หัวหน้า เพื่อนๆ และครอบครัวของคุณรู้จักคุณดีอยู่แล้ว แต่คุณจะทำให้บริษัทที่คุณต้องการทำงานด้วยรู้จักคุณได้อย่างไร? Splitshift มีรูปแบบการบันทึกประวัติส่วนตัวที่โดดเด่น ทำให้นายจ้างทราบข้อมูลของคุณ รวมไปถึงความใฝ่ฝัน ความสนใจ และความสามารถของคุณได้ดียิ่งขึ้น",
        "คุณสามารถติดต่อกับเพื่อนร่วมงานและนายจ้างที่มีทัศนคติเหมือนกันได้โดยตรง และสร้างเครือข่ายทางด้านอาชีพในแบบที่คุณต้องการได้",
        "คุณยังได้รับสิทธิพิเศษจากแหล่งข้อมูลเพื่อการเรียนรู้ของเรา และเรายังมีผู้เชี่ยวชาญที่จะช่วยสร้างแรงบันดาลใจและให้คำปรึกษาทางด้านอาชีพแก่คุณอีกด้วย",
        "สำหรับองค์กรทั่วไป <a href=\"http://www.splitshift.com\">Splitshift.com</a> ให้บริการจัดหาพนักงานทุกตำแหน่งแบบเชิงรุกและมีประสิทธิภาพ ผ่านทางระบบจับคู่งานแบบอัตโนมัติ ซึ่งสามารถค้นหาผู้สมัครที่มีคุณสมบัติตรงตามตำแหน่งที่ต้องการ",
      ],
      readmore: "อ่านต่อ",
      readless: "ย่อลง"
    },
    stat: [
      'Splitshift', 'สถิติ', 'สมาชิก', 'งาน', 'บริษัท'
    ],
    headers: [
      'ข่าวน่ารู้', 'บุคคล', 'งาน', 'บริษัทต่างๆ', 'ทรัพยากร', 'นายจ้าง', 'เข้าสู่ระบบ'
    ],
    aboutus: {
      header: "ศูนย์รวมโลกแห่งการบริการ",
      vision: {
        header: "<span5><span>วิสัยทัศน์</span> ของเรา</span5>",
        detail: "วิสัยทัศน์ของเราคือการสร้างชุมชนบนอินเตอร์เน็ตที่จะดึงผู้ที่มีหัวใจเดียวกัน ให้เข้ามาอยู่ร่วมกัน ให้การสนับสนุนและสร้างแรงบันดาลใจสำหรับทุกคนที่สนใจในงานภาคบริการในภูมิภาคเอเชียตะวันออกเฉียงใต้ – หนึ่งคน, หนึ่งงาน, และหนึ่งอาชีพ ในหนึ่งครั้ง"
      },
      whatis: {
        header: "<span5><span>SPLITSHIFT</span> คืออะไร?</span5>",
        detail: "Splitshift.com เป็นศูนย์รวมเครือข่ายสื่อสังคมออนไลน์ มืออาชีพแห่งแรก, มุ่งเน้นเพื่อการศึกษา เป็นแหล่งหางานและหาคนทำงานเพื่อเชื่อมต่อกับมืออาชีพยุคใหม่ซึ่งเป็นที่สนใจอย่างมากในสาขาการโรงแรมและอุตสาหกรรมการบริการทั่วประเทศไทย ร่วมไปถึงเอเชียตะวันออกเฉียงใต้"
      },
      why: {
        header: "<span5>ทำไมต้องเป็น <span>\"SPLITSHIFT\"</span> ?</span5>",
        detail: [
          "Splitshift.com ช่วยให้ทุกคนที่สนใจในวิชาชีพการบริการ มีช่องทางสร้างประวัติผลงานวิชาชีพและข้อมูลส่วนตัวที่เป็น 'เรซูเม่ออนไลน์ (Online Resume)' สำหรับผู้มองหางานและเป็นเสมือน “เวทีสร้างแบรนด์สินค้า” ของนายจ้าง สำหรับธุรกิจและผู้จัดการที่ต้องการจ้างคนเข้าทำงาน ",
          "แทนที่จะมุ่งเน้นไปในเรื่องของประสบการณ์การทำงานและการศึกษาล้วนๆ Splitshift.com มุ่งส่งเสริมให้คนกล้าแสดงออกในเรื่องของบุคลิกภาพ ความสนใจส่วนตัวและแรงทะเยอทะยานที่จะวิ่งไปข้างหน้า แต่ในทำนองเดียวกันสำหรับธุรกิจก็จะนำเสนอข้อมูลภายในเกี่ยวกับสภาพแวดล้อมการทำงานและวัฒนธรรมของบริษัท",
          "ผู้คนสามารถเรียกดูเพื่อค้นหางานผ่านทางเมนูการค้นหาขั้นสูง, ขอรับการแจ้งเตือนเมื่อมีงานที่เกี่ยวข้องเข้ามา และติดตามบริษัทที่ชื่นชอบของพวกเขาได้",
          "นายจ้างก็สามารถโพสต์งานต่างๆ รับการแจ้งเตือนเมื่อมีผู้สมัครที่เข้าข่าย เรียกดูประวัติผู้สมัครที่มีอยู่ทั้งหมด และสามารถติดต่อไปหาผู้สมัครที่ตรงกับความต้องการของพวกเขาได้ทันที แทนที่จะรอให้พวกเขามาสมัครเมื่อมีการประกาศรับสมัคร",
          "Splitshift.com ช่วยส่งเสริมการสื่อสารแบบสองทางระหว่างผู้ใช้และผู้จัดการที่กำลังหาคนมาทำงาน ทำให้กระบวนการสรรหาคนมีความเป็นส่วนตัวและมีประสิทธิภาพมากขึ้นสำหรับทุกคน",
          "Splitshift.com ให้บริการฟรีในการเข้าถึงแหล่งทรัพยากรที่เก็บข้อมูลได้มากมายมหาศาลเกี่ยวกับด้านการฝึกอบรมและการศึกษาออนไลน์ เป็นบริการที่จัดทำให้โดยสมาชิกในชุมชนของเรา ได้แก่ บทเรียนวิดีโอ สัมมนาออนไลน์ คู่มือฝึกอบรม การสอนในรูปแบบเสมือนจริงและการให้คำแนะนำจากผู้เชี่ยวชาญด้านอุตสาหกรรมมืออาชีพ ตลอดจนมีรายการตรวจสอบที่เป็นประโยชน์ ตัวอย่างแม่แบบและการอ้างอิงที่เกี่ยวข้องกับงานโรงแรมและอุตสาหกรรมการบริการ",
          "Splitshift.com จึงมีจุดมุ่งหมายเพื่อให้การศึกษาและเครื่องมือปฏิบัติงานขั้นพื้นฐาน สำหรับมืออาชีพที่มีไฟในการทำงาน ผู้ต้องการเรียนรู้เพิ่มเติมเกี่ยวกับลักษณะนิสัยและทักษะที่จำเป็น เพื่อเพิ่มความมั่นใจของพวกเขาที่จะแสวงหางานอาชีพที่มีความหมายในสาขางานโรงแรมและอุตสาหกรรมการบริการอื่น ๆ"
        ]
      }
    },
    buzz: {
      search: 'ค้นหาโฟสต์ด้วยคำหลัก'
    },
    feed: {
      post: {
        employee: 'โชว์งานของคุณและสิ่งที่คุณสนใจ',
        employer: 'ระบุงานและความสนใจของคุณ',
        action: ['โพสต์', 'แก้ไข']
      },
      card: {
        type: ['รูปภาพ', 'วิดีโอ'],
        action: ['ถูกใจ', 'แชร์', 'ความคิดเห็น', 'ถูกใจ'],
        comment: 'เขียนความคิดเห็น'
      },
      status: {
        public: 'สาธารณะ',
        timeline: 'ไทม์ไลน์ของฉัน',
        connected: 'คอนเนกชั่นของฉัน',
        employee: 'ลูกจ้าง',
        employer: 'นายจ้าง',
        onlyme: 'เฉพาะฉัน'
      }
    },
    people_search: {
      header: 'ผู้คนที่คุณอาจรู้จัก',
      intro: 'เชื่อมต่อและสร้างเครือข่ายธุรกิจของคุณให้ยิ่งใหญ่',
      see_profile: 'ดูประวัติ',
      people: ['ผู้คน', 'ชื่อ, บริษัท, คำหลัก ...'],
      location: ['ที่ตั้ง', 'จังหวัด, เขต ...']
    },
    job_search: {
      intro: 'ค้นหาตำแหน่งงานด้านอุตสาหกรรมบริการ นับพันตำแหน่ง',
      job: ['งาน', 'ชื่องาน, คำหลัก, บริษัท ...'],
      location: ['ที่ตั้ง', 'จังหวัด, เขต ...']
    },
    company_search: {
      intro: 'ค้นหาบริษัทในฝันของคุณให้พบ',
      company: ['บริษัท', 'บริษัท, คำหลัก ...'],
      location: ['ที่ตั้ง', 'จังหวัด, เขต ...']
    },
    advanced_search: {
      title: 'ค้นหาขั้นสูง',
      interest_title: 'ความสนใจ',
      interest: [
        "กีฬา",
        "ศิลปะ",
        "การถ่ายภาพ",
        "ออกแบบ",
        "สุขภาพ/การออกกำลังกาย",
        "ดนตรี",
        "ภาพยนตร์",
        "อ่านหนังสือ",
        "การเดินทางท่องเที่ยว",
        "การทำอาหาร",
        "ช็อปปิ้ง",
        "แฟชั่น",
        "อาหารและเครื่องดื่ม",
        "งานเลี้ยงค็อกเทล",
        "เทคโนโลยี",
        "หนังสือ",
        "สัตว์เลี้ยง",
        "ประวัติศาสตร์",
        "การเต้นรำ",
        "การเขียน",
        "สื่อสังคม",
        "รถยนต์",
        "จักรยานยนต์",
        "การเล่นเกม",
        "สถาปัตยกรรม",
        "นวัตกรรม",
        "ผู้ประกอบการ",
        "การเงิน",
        "ความร่ำรวย",
        "การเมือง",
        "สิ่งแวดล้อม",
        "การจัดการ",
        "การงานอาชีพ",
        "การตลาด",
        "การขาย",
        "ปรัชญา",
        "การพัฒนาตนเอง",
        "การศึกษา",
        "อื่นๆ"
      ],
      workexp_title: 'ประสบการณ์การทำงาน',
      workexp: {"[0,1]": "น้อยกว่า หนึ่ง ปี", "[1,2]": "ระหว่าง 1 ถึง 2 ปี", "[2,3]": "ระหว่าง 2 ถึง 3 ปี", "[3,5]": "ระหว่าง 3 ถึง 5 ปี", "[5,100]": "มากกว่า 5 ปี" },
      distance_title: 'ระยะทาง',
      distance: {5000: "ภายใน 5 กม.", 10000: "ภายใน 10 กม.", 15000: "ภายใน 15 กม.", 20000: "ภายใน 20 กม.", 25000: "ภายใน 25 กม.", 50000: "ภายใน 50 กม.", 100000: "ภายใน 100 กม."},
      date_posted_title: 'วันที่ลงประกาศ',
      date_posted: {1: "24 ชม. ที่ผ่านมา", 7: "สัปดาห์ที่ผ่านมา",14: "2 สัปดาห์ที่ผ่านมา",30: "เดือนที่ผ่านมา",60: "2 เดือนที่ผ่านมา",90: "3 เดือนที่ผ่านมา" },
      job_type_title: 'ประเภทงาน',
      job_type: ["งานทุกประเภท", "เต็มเวลา", "พาร์ทไทม์", "ชั่วคราว", "ชั่วครั้งชั่วคราว/จ้างเป็นชั่วโมง", "ฝึกงาน", "อิสระ", "สัญญาจ้าง"],
      compensation_title: 'เงินชดเชย',
      compensation: ["ขึ้นอยู่กับประสบการณ์", "< 9000 บาท", "9,000 – 15,000 บาท", "15,000 – 30,000 บาท", "30,000 – 50,000 บาท", "50,000 - 100,000 บาท", "100,000 - 200,000 บาท", "> 200,000 บาท"],
      company_type_title: 'ประเภทบริษัท',
      company_type: ["ทุกประเภท", "โรงแรม", "ภัตตาคาร", "บาร์", "ไนท์คลับ", "ร้านค้าปลีก", "การท่องเที่ยว", "สปา", "การจัดเลี้ยง", "อื่นๆ"],
      employee_rating_title: 'คะแนนการทำงานของพนักงาน',
      lastest_jobs_title: 'การทำงานล่าสุด'
    },
    user_profile: {
      menus: ['ที่อยู่ของคุณ', 'ไทม์ไลน์', 'รูปภาพ', 'ประวัติส่วนบุคคลของฉัน', 'ส่งข้อความ', 'ติดตาม', 'กำลังติดตาม', 'ส่งเชื่อมต่อแล้ว', 'เชื่อมต่อ', 'เชื่อมต่อแล้ว', 'เชื่อมต่อ', 'ดูประวัติสาธารณะของคุณ', 'แก้ไขประวัติ'],
      aboutme: 'เกี่ยวกับฉัน',
      aboutme_placeholder: 'บอกความเป็นตัวคุณ อะไรคือแรงจูงใจ หรือความฝันของคุณคืออะไร',
      interest: {
        header: 'ความสนใจของฉัน',
        list: [
          "กีฬา",
          "ศิลปะ",
          "ถ่ายภาพ",
          "ออกแบบ",
          "สุขภาพ/ออกกำลังกาย",
          "ดนตรี",
          "ภาพยนตร์",
          "อ่านหนังสือ",
          "เดินทางท่องเที่ยว",
          "ทำอาหาร",
          "ช็อปปิ้ง",
          "แฟชั่น",
          "อาหารและเครื่องดื่ม",
          "ค๊อกเทล",
          "เทคโนโลยี",
          "หนังสือ",
          "สัตว์เลี้ยง",
          "ประวัติศาสตร์",
          "เต้นรำ",
          "การเขียน",
          "สื่อสังคม",
          "รถยนต์",
          "จักรยานยนต์",
          "เกมส์",
          "สถาปัตยกรรม",
          "นวัตกรรม",
          "พ่อค้านักธุรกิจ",
          "การเงิน",
          "ความมั่งคั่ง",
          "การเมือง",
          "ธรรมชาติ",
          "การจัดการ",
          "การงานอาชีพ",
          "การตลาด",
          "การขาย",
          "ปรัชญา",
          "การพัฒนาตนเอง",
          "การศึกษา",
          "อื่นๆ"
        ]
      },
      workexp: {
        header: 'ประสบการณ์การทำงาน',
        add: 'เพิ่มประสบการณ์ใหม่'
      },
      skill: {
        header: 'ทักษะ',
        placeholder: 'เพิ่มทักษะใหม่ของคุณ เช่น ร้องเพลง, วาดภาพ'
      },
      education: {
        header: 'การศึกษา',
        add: 'เพิ่มการศึกษาใหม่'
      },
      course: {
        header: 'การอบรมและหลักสูตร',
        add: 'เพิ่มหลักสูตรใหม่'
      },
      following: 'การติดตาม',
      language: {
        header: 'ภาษา',
        add: 'เพิ่มภาษา'
      },
      editor: {
        add: 'เพิ่ม',
        edit: 'แก้ไข',
        delete: 'ลบ',
        link: 'ลิงค์',
        cancel: 'ยกเลิก',
        upload: {
          profile: 'อัพโหลดภาพของคุณ',
          banner: ['อัพโหลดพื้นหลัง', 'เลือกจากอุปกรณ์ของคุณ', 'เลือกตัวอย่างแม่แบบ']
        },
        card: ['แก้ไขประวัติของคุณ', 'ชื่อ นามสกุล', 'ที่อยู่ของคุณ', 'ที่อยู่ของคุณจะไม่แสดงผลให้คนอื่นเห็น', 'วันเกิด ตัวอย่าง 21-07-1998', 'วันเกิดของคุณจะไม่แสดงผลให้คนอื่นเห็น', 'เลือกเพศ', 'ชาย', 'หญิง', 'บันทึกการเปลี่ยนแปลงประวัติ', 'ยกเลิก'],
        workexp: ['ชื่อบริษัท', 'ตำแหน่ง', 'ที่อยู่', 'เขต', 'เมือง', 'รหัสไปรษณีย์', 'วันที่เริ่มต้น', 'วันที่สิ้นสุด', 'งานปัจจุบัน'],
        education: ['โรงเรียน', 'สาขา', 'รายละเอียด', 'วันที่เริ่มต้น', 'วันที่สิ้นสุด'],
        course: ['ชื่อ', 'ลิงค์', 'รายละเอียด', 'วันที่เริ่มต้น', 'วันที่สิ้นสุด'],
        language: ['ภาษา', 'ภาษาแม่', 'คล่องแคล่ว', 'ชั้นสูง', 'ปานกลาง', 'เพิ่งเริ่มต้น']
      }
    },
    company_profile: {
      menus: ['ไทม์ไลน์', 'รูปภาพ', 'ดูประวัติสาธารณะของคุณ', 'แก้ไขข้อมูลของคุณ', 'ส่งข้อความ', 'ประวัติบริษัท', 'ติดตาม', 'กำลังติดตาม'],
      review: {
        summary: 'สรุปการจัดอันดับบริษัท',
        lastest: 'ความเห็นล่าสุด',
        side: ['โดยรวม', 'สภาพแวดล้อมการทำงานและวัฒนธรรม', 'เงินชดเชยและผลประโยชน์', 'โอกาสทางอาชีพ', 'แนะนำให้เพื่อนรู้จัก', 'ติดตาม', 'กำลังติดตาม']
      },
      benefit_title: 'ผลประโยชน์',
      benefit: [
        "วันทำงาน 5 วันต่อสัปดาห์",
        "อาหารฟรีระหว่างทำงาน",
        "เครื่องแบบฟรี",
        "กองทุนเลี้ยงชีพ",
        "กองทุนประกันสังคม",
        "ประกันสุขภาพ",
        "กองทุนเงินชดเชยคนงาน",
        "ค่ารักษาพยาบาลผู้ป่วยนอก",
        "ค่าบริการ",
        "Paid Vacation and Public Holidays",
        "Wedding Leave & Allowance",
        "Child Birth Allowance",
        "Funeral Allowance",
        "โบนัสประจำปี",
        "ประกันชีวิต",
        "Annual Check-up",
        "Training & Development",
        "Loyalty & Recognition Awards",
        "Staff Discounts"
      ],
      editor: {
        upload: {
          profile: 'อัพโหลดภาพบริษัทของคุณ',
          banner: ['อัพโหลดพื้นหลัง', 'เลือกจากอุปกรณ์ของคุณ', 'เลือกตัวอย่างแม่แบบ']
        }
      },
      header: {
        positions: 'ตำแหน่งงาน',
        people: 'ผู้คน',
        gallery: 'อัลบั้มภาพ',
        culture: 'วัฒนธรรม',
        benefits: 'ผลประโยชน์',
        location: 'สถานที่',
        overview: 'ภาพรวมบริษัท',
        videos: 'วิดีโอ'
      }
    },
    menus: {
      action: ['ประวัติของฉัน', 'เมนูหลัก', 'การตั้งค่า', 'ออกจากระบบ'],
      notification: ['ข้อความ', 'การแจ้งเตือน', 'รอการเชิญ']
    },
    backend: {
      sidebar: ['ประวัติของฉัน', 'ประวัติของบริษัท', 'สถิติทั้งหมด', 'ประกาศหางาน', 'จัดการใบสมัครงาน', 'การสมัครงาน', 'เพิ่มทรัพยากร', 'จัดการ HR', 'การตั้งค่า'],
      resource: {
        header: ['การจัดการทรัพยากร', 'ชื่อทรัพยากร', 'เจ้าของ', 'สถานภาพ'],
        add: 'เพิ่มทรัพยากร',
        title: 'สร้างทรัพยากร',
        form: ['ตำแหน่ง', 'ประเภท', 'แผนก', 'คุณสมบัติ', 'ภาพ', 'วิดีโอ', 'รายละเอียดสั้นๆ', 'เนื้อหา'],
        category: ["วิดีโอ", "สัมมนา/หลักสูตร", "การฝึกอบรม/คู่มือ", "ตัวอย่าง/รายการตรวจสอบ", "บทความ/ลิงค์", "การสอนเสมือนจริง"],
        department: [
          "ฝึกต่างแผนก",
          "บริการอาหารและเครื่องดื่ม",
          "การทำอาหาร",
          "แม่บ้าน",
          "การขายและการตลาด",
          "ฝ่ายการเงิน",
          "สำนักงานส่วนหน้า",
          "ทรัพยากรบุคคล",
          "วิศวกรรม",
          "ไอที",
          "การรักษาความปลอดภัย",
          "เบ็ดเตล็ด"
        ],
        action: ['สร้างทรัพยากร', 'แก้ไขทรัพยากร', 'ยกเลิก']
      },
      user: {
        application: ['รายชื่อการสมัคร', 'บริษัทสมัครงาน ', 'ข้อมูล', 'สถานภาพ']
      }
    },
    setting: {
      title: 'การตั้งค่า',
      delete: [
        'ลบบัญชีของคุณออก',
        'การลบบัญชีของคุณ อาจทำให้ข้อมูลของคุณหายไปจนใช้ไม่ได้อีก ชื่อและรูปถ่ายของคุณจะถูกลบออกจากระบบของเราที่คุณได้แชร์ไว้ในสปลิตชิฟต์ ข้อมูลบางอย่างอาจยังมีคนอื่นเห็นอยู่ เช่น ชื่อของคุณในบัญชีที่เชื่อมต่อกับเพจของเขาหรือข้อความที่คุณส่งไป',
        'ฉันต้องการลบบัญชีของฉันออก'
      ],
      password: [
        'เปลี่ยนรหัสผ่านของคุณ',
        'รหัสผ่านเก่า',
        'รหัสผ่านเก่าของคุณ',
        'รหัสผ่านใหม่',
        'พิมพ์รหัสผ่านใหม่ของคุณ',
        'พิมพ์รหัสผ่านใหม่ของคุณอีกครั้ง',
        'เปลี่ยนรหัสผ่าน'
      ],
      primary: [
        'เปลี่ยนอีเมลหลักของคุณ',
        'อีเมลหลักของคุณ',
        'ตั้งอีเมลหลักใหม่',
        'พิมพ์อีเมลหลักใหม่ของคุณ',
        'ยืนยันอีเมลหลักใหม่'
      ]
    },
    resource: {
      title: 'เข้ามาค้นหาคลังข้อมูลของเราสิ',
      search_placeholder: 'ค้นหา, แผนก  ...',
      department: 'แผนก',
      category: 'หมวด',
      subtitle: 'เอาความรู้ของคุณมาแชร์กันและโชว์ความสามารถให้เป็นที่ยอมรับของบริษัทเรา',
      add: 'เพิ่มบุคลากร',
      all: 'ทุกประเภท',
      header1: ['ทรัพยากรที่มีให้', ''],
      header2: ['ทรัพยากรล่าสุด', '']
    },
    footer: {
      mail: 'ต้องการความช่วยเหลือ?',
      about: ['เกี่ยวกับ', 'เกี่ยวกับเรา', 'เข้าสู่ระบบ', 'ลงทะเบียน', 'ความเป็นส่วนตัวและนโยบาย', 'ข้อตกลงผู้ใช้'],
      discover: ['ค้นพบ', 'บล็อกชุมชน', 'งาน', 'บริษัท', 'เครือข่าย', 'ทรัพยากร', 'นายจ้าง'],
      setting: ['การตั้งค่า', 'ภาษา', 'ประเทศ'],
      newsletter: ['จดหมายข่าว', 'ที่อยู่อีเมล์', 'สมัครเป็นสมาชิก']
    }
  }
};

export default configLang;
