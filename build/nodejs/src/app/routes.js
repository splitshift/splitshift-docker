import React from 'react'
import { Route, IndexRoute, hashHistory } from 'react-router'

import backendRoutes from './backendRoutes'
import adminRoutes from './adminRoutes'

import ConfirmEmail from './components/ConfirmEmail'
import Privacy from './components/Frontend/Privacy'
import UserAgreement from './components/Frontend/UserAgreement'

import AppContainer from './containers/AppContainer'
import HomeContainer from './containers/HomeContainer'
import AboutusContainer from './containers/AboutusContainer'
import ForgetPasswordContainer from './containers/ForgetPasswordContainer'
import UserProfileContainer from './containers/UserProfileContainer'
import CompanyProfileContainer from './containers/CompanyProfileContainer'
import UserResumeContainer from './containers/UserResumeContainer'
import CompanyResumeContainer from './containers/CompanyResumeContainer'
import BuzzContainer from './containers/BuzzContainer'
import SettingContainer from './containers/SettingContainer'

import SearchContainer from './containers/SearchContainer'
import PeopleSearchContainer from './containers/PeopleSearchContainer'
import CompanySearchContainer from './containers/CompanySearchContainer'
import JobSearchContainer from './containers/JobSearchContainer'

import ResourceContainer from './containers/ResourceContainer'
import ResourceSearchContainer from './containers/ResourceSearchContainer'
import ResourceArticleContainer from './containers/ResourceArticleContainer'

export default (
  <Route>

    <Route path="/" component={AppContainer}>

      <IndexRoute name="home"  component={HomeContainer} />

      <Route name="confirm_email" path="/api/confirm/:token" component={ConfirmEmail} />
      <Route name="search" path="search" component={SearchContainer} />

      <Route name="forget_password" path="forgot/password/:id" component={ForgetPasswordContainer} />
      <Route name="language" path="/:language">
        <IndexRoute name="home"  component={HomeContainer} />

        <Route name="aboutus" path="aboutus" component={AboutusContainer} />
        <Route name="privacy" path="privacy" component={Privacy} />
        <Route name="user_agreement" path="user-agreement" component={UserAgreement} />
        <Route name="people-search" path="people-search" component={PeopleSearchContainer} />
        <Route name="setting" path="setting" component={SettingContainer} />
        <Route name="job-search" path="job-search" component={JobSearchContainer} />
        <Route name="company-search" path="company-search" component={CompanySearchContainer} />

        <Route name="user_profile" path="profile" component={UserProfileContainer} />
        <Route name="user_public_profile" path="user/:key" component={UserProfileContainer} />
        <Route name="user_image_profile" path="user/:key/photo" component={UserProfileContainer} />
        <Route name="user_feed_single" path="user/:key/feed/:feed_id" component={UserProfileContainer} />

        <Route name="company_profile" path="company" component={CompanyProfileContainer} />
        <Route name="company_public_profile" path="company/:key" component={CompanyProfileContainer} />
        <Route name="company_image_profile" path="company/:key/photo" component={CompanyProfileContainer} />
        <Route name="company_feed_single" path="company/:key/feed/:feed_id" component={CompanyProfileContainer} />

        <Route name="buzz" path="buzz" component={BuzzContainer} />

        <Route name="resource_index" path="resource" component={ResourceContainer} />
        <Route name="resource_search" path="resource/search" component={ResourceSearchContainer} />
        <Route name="resource_article" path="resource/article/:id" component={ResourceArticleContainer} />
      </Route>
    </Route>

    {backendRoutes}
    {adminRoutes}

    <Route name="resume_language" path="/:language">
      <Route name="user_resume_profile" path="user/:key/resume" component={UserResumeContainer} />
      <Route name="company_resume_profile" path="company/:key/profile" component={CompanyResumeContainer} />
    </Route>

  </Route>
);
