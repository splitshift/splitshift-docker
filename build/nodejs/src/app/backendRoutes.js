import React from 'react';
import { Route, IndexRoute, hashHistory } from 'react-router' ;

import BackendContainer from './containers/BackendContainer';

import UserDashboardContainer from './containers/UserDashboardContainer';
import CompanyDashboardContainer from './containers/CompanyDashboardContainer';

import ApplicationContainer from './containers/ApplicationContainer';

import JobContainer from './containers/Job/IndexContainer';
import JobCreateContainer from './containers/Job/CreateContainer';

import HRManagementContainer from './containers/HRManagementContainer';

import AdminResourceContainer from './containers/Admin/ResourceContainer';
import AdminResourceCreatorContainer from './containers/Admin/ResourceCreatorContainer';

import ApplyJobContainer from './containers/ApplyJobContainer';

export default (
  <Route name="language" path="/:language">
    <Route path="dashboard" component={BackendContainer}>

      <Route name="dashboard_user" path="user" component={UserDashboardContainer} />
      <Route name="dashboard_company" path="company" component={CompanyDashboardContainer} />

      <Route name="application" path="application" component={ApplicationContainer} />

      <Route name="job" path="job" component={JobContainer} />
      <Route name="job_create" path="job/create" component={JobCreateContainer} />
      <Route name="job_edit" path="job/edit/:id" component={JobCreateContainer} />

      <Route name="hr_management" path="hr" component={HRManagementContainer} />

      <Route name="resource" path="resource/index" component={AdminResourceContainer} />
      <Route name="resource_create" path="resource/create" component={AdminResourceCreatorContainer} />
      <Route name="resource_edit" path="resource/edit/:id" component={AdminResourceCreatorContainer} />

      <Route name="applyjob" path="apply" component={ApplyJobContainer} />

    </Route>
  </Route>
);
