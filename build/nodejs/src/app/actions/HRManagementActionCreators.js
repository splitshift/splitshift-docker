import AppDispatcher from '../AppDispatcher';
import constants from '../constants';

import HRAPI from '../apis/HRAPI';

const HRManagementActionCreators = {

  getEmployee(company_key) {
    AppDispatcher.dispatchAsync(HRAPI.getEmployee(company_key), {
      request: constants.GET_EMPLOYEE_IN_COMPANY,
      success: constants.GET_EMPLOYEE_IN_COMPANY_SUCCESS,
      failure: constants.GET_EMPLOYEE_IN_COMPANY_FAIL
    });
	},

  changeRank(token, info) {
    AppDispatcher.dispatchAsync(HRAPI.changeRank(token, info), {
      request: constants.CHANGE_RANK,
      success: constants.CHANGE_RANK_SUCCESS,
      failure: constants.CHANGE_RANK_FAIL
    });
	}


}

export default HRManagementActionCreators;
