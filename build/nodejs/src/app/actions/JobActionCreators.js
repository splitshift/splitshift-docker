import AppDispatcher from '../AppDispatcher';
import constants from '../constants';

import UploadAPI from '../apis/UploadAPI';
import JobAPI from '../apis/JobAPI';

const JobActionCreators = {

  chooseJob(job_index) {
		AppDispatcher.dispatch({
			type: constants.CHOOSE_JOB_COMPANY,
			payload: {
				response: job_index
			}
		});
	},

  updateJobDraft(info) {
		AppDispatcher.dispatch({
			type: constants.UPDATE_JOB_DRAFT,
			payload: {
				response: info
			}
		});
	},

  resetDraft() {
    AppDispatcher.dispatch({
			type: constants.RESET_JOB_DRAFT,
		});
  },

  changeApplyJobFilter(filter) {
    AppDispatcher.dispatch({
			type: constants.CHANGE_APPLY_JOB_FILTER,
			payload: {
				response: filter
			}
		});
  },

  updateApplyJobDraft(info) {
		AppDispatcher.dispatch({
			type: constants.UPDATE_APPLY_JOB_DRAFT,
			payload: {
				response: info
			}
		});
	},

  uploadFileApplyJob(token, info) {
    AppDispatcher.dispatchAsync(UploadAPI.uploadFile(token, info), {
      request: constants.UPLOAD_APPLY_FILE,
      success: constants.UPLOAD_APPLY_FILE_SUCCESS,
      failure: constants.UPLOAD_APPLY_FILE_FAIL
    });
	},

  getJob(token, job_id) {
    AppDispatcher.dispatchAsync(JobAPI.getJob(token, job_id), {
      request: constants.GET_JOB,
      success: constants.GET_JOB_SUCCESS,
      failure: constants.GET_JOB_FAIL
    });
  },

  getAllJob(token) {
    AppDispatcher.dispatchAsync(JobAPI.getAllJob(token), {
      request: constants.GET_ALL_JOB,
      success: constants.GET_ALL_JOB_SUCCESS,
      failure: constants.GET_ALL_JOB_FAIL
    });
  },

  createNewJob(token, info) {
    AppDispatcher.dispatchAsync(JobAPI.postNewJob(token, info), {
			request: constants.POST_NEW_JOB,
			success: constants.UPDATE_JOB_SUCCESS,
			failure: constants.UPDATE_JOB_FAIL
		});
  },

  updateJob(token, info) {
    AppDispatcher.dispatchAsync(JobAPI.putUpdateJob(token, info), {
      request: constants.UPDATE_JOB,
      success: constants.UPDATE_JOB_SUCCESS,
      failure: constants.UPDATE_JOB_FAIL
    });
  },

  deleteJob(token, info) {
    AppDispatcher.dispatchAsync(JobAPI.deleteJob(token, info), {
      request: constants.DELETE_JOB,
      success: constants.UPDATE_JOB_SUCCESS,
      failure: constants.UPDATE_JOB_FAIL
    });
  },

  activateJob(token, job_id) {
    AppDispatcher.dispatchAsync(JobAPI.activateJob(token, job_id), {
      request: constants.UPDATE_ACTIVATE_JOB,
      success: constants.UPDATE_ACTIVATE_JOB_SUCCESS,
      failure: constants.UPDATE_ACTIVATE_JOB_FAIL
    });
  },

  getAllApplyJob(token) {
    AppDispatcher.dispatchAsync(JobAPI.getAllApplyJob(token), {
      request: constants.GET_ALL_APPLY_JOB,
      success: constants.GET_APPLY_JOB_SUCCESS,
      failure: constants.GET_APPLY_JOB_FAIL
    });
  },

  getApplyJob(token, info) {
    AppDispatcher.dispatchAsync(JobAPI.getApplyJob(token, info), {
      request: constants.GET_APPLY_JOB,
      success: constants.GET_APPLY_JOB_SUCCESS,
      failure: constants.GET_APPLY_JOB_FAIL
    });
  },

  searchApplyJob(search) {
    AppDispatcher.dispatch({
      type: constants.SEARCH_APPLY_JOB,
			payload: search
    })
  },

  getEmployeeApplyJob(token) {
    AppDispatcher.dispatchAsync(JobAPI.getEmployeeApplyJob(token), {
      request: constants.GET_EMPLOYEE_APPLY_JOB,
      success: constants.GET_EMPLOYEE_APPLY_JOB_SUCCESS,
      failure: constants.GET_EMPLOYEE_APPLY_JOB_FAIL
    });
  },

  sendApplyJob(token, info) {
    AppDispatcher.dispatchAsync(JobAPI.postApplyJob(token, info), {
      request: constants.POST_APPLY_JOB,
      success: constants.POST_APPLY_JOB_SUCCESS,
      failure: constants.POST_APPLY_JOB_FAIL
    });
  },

  changeAppleJobStatus(token, applyjob_id ,info) {
    AppDispatcher.dispatchAsync(JobAPI.changeAppleJobStatus(token, applyjob_id, info), {
      request: constants.CHANGE_APPLY_JOB_STATUS,
      success: constants.CHANGE_APPLY_JOB_STATUS_SUCCESS,
      failure: constants.CHANGE_APPLY_JOB_STATUS_FAIL
    });
  },

  deleteApplyJob(token, apply_job_id) {
    AppDispatcher.dispatchAsync(JobAPI.deleteApplyJob(token, apply_job_id), {
      request: constants.DELETE_APPLY_JOB,
      success: constants.DELETE_APPLY_JOB_SUCCESS,
      failure: constants.DELETE_APPLY_JOB_FAIL
    });
  },

  deleteApplication(token, apply_job_id) {
    AppDispatcher.dispatchAsync(JobAPI.deleteApplication(token, apply_job_id), {
      request: constants.DELETE_APPLY_JOB,
      success: constants.DELETE_APPLY_JOB_SUCCESS,
      failure: constants.DELETE_APPLY_JOB_FAIL
    });
  },

  forwardEmail(token, info) {
    AppDispatcher.dispatchAsync(JobAPI.forwardEmail(token, info), {
      request: constants.SEND_FORWARD_EMAIL,
      success: constants.SEND_FORWARD_EMAIL_SUCCESS,
      failure: constants.SEND_FORWARD_EMAIL_FAIL
    });
  }

}

export default JobActionCreators;
