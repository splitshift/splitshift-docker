import AppDispatcher from '../AppDispatcher';
import { browserHistory } from 'react-router';
import constants from '../constants';

import LoginAPI from '../apis/LoginAPI';
import RegisterAPI from '../apis/RegisterAPI';
import UserProfileAPI from '../apis/UserProfileAPI';
import CompanyProfileAPI from '../apis/CompanyProfileAPI';

const AuthenticationActionCreators = {
	guest() {
		AppDispatcher.dispatch({
			type: constants.AS_GUEST
		});
	},
	login(info) {
		AppDispatcher.dispatchAsync(LoginAPI.login(info), {
			request: constants.LOGIN,
      success: constants.LOGIN_SUCCESS,
      failure: constants.LOGIN_FAIL
		});
	},
	updateLogin(info) {
		AppDispatcher.dispatch({
			type: constants.UPDATE_LOGIN,
			payload: {info}
		});
	},
	loginAlready(){

		// Get lastest session data
		AppDispatcher.dispatch({
			type: constants.LOGIN_ALREADY,
			payload: {
				response: {
					'token': localStorage.getItem('token'),
					'key': localStorage.getItem('key'),
					'type': localStorage.getItem('type')
				}
			}
		});
	},
	logout() {
		// Remove session data
		AppDispatcher.dispatch({
			type: constants.LOGOUT,
			payload: {
				'token': localStorage.getItem('token'),
				'key': localStorage.getItem('key'),
				'type': localStorage.getItem('type')
			}
		});
	},
	createRegister(info) {
		AppDispatcher.dispatch({
			type: constants.REGISTER_UPDATE,
			payload: {info}
		});
	},
	updateRegister(info) {
		AppDispatcher.dispatch({
			type: constants.REGISTER_UPDATE,
			payload: {info}
		});
	},
	register(info) {
		// Verrify Data
    AppDispatcher.dispatchAsync(RegisterAPI.register(info), {
      request: constants.REGISTER,
      success: constants.REGISTER_SUCCESS,
      failure: constants.REGISTER_FAIL
    });
  },
	loadProfile(key) {
		AppDispatcher.dispatchAsync(UserProfileAPI.getUserProfile(key), {
			request: constants.GET_USER_OWNER_PROFILE,
			success: constants.GET_USER_OWNER_PROFILE_SUCCESS,
			failure: constants.GET_USER_PROFILE_FAIL
		});
	},
	loadCompanyProfile(key) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.getProfile(key), {
			request: constants.GET_USER_OWNER_PROFILE,
			success: constants.GET_USER_OWNER_PROFILE_SUCCESS,
			failure: constants.GET_USER_PROFILE_FAIL
		});
	},
	forgetPassword(info) {
		AppDispatcher.dispatchAsync(RegisterAPI.forgetPassword(info), {
			request: constants.FORGET_PASSWORD,
			success: constants.FORGET_PASSWORD_SUCCESS,
			failure: constants.FORGET_PASSWORD_FAIL
		})
	},
	changePasswordFormForgot(info, token) {
		AppDispatcher.dispatchAsync(RegisterAPI.changePasswordFormForgot(info, token), {
			request: constants.CHANGE_FORGET_PASSWORD,
			success: constants.CHANGE_FORGET_PASSWORD_SUCCESS,
			failure: constants.CHANGE_FORGET_PASSWORD_FAIL
		})
	}
};

export default AuthenticationActionCreators;
