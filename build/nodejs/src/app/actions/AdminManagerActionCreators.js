import AppDispatcher from '../AppDispatcher';
import { browserHistory } from 'react-router';
import constants from '../constants';

import AdminAPI from '../apis/AdminAPI';

const AdminManagerActionCreators = {

	getUsers(token) {
		AppDispatcher.dispatchAsync(AdminAPI.getUsers(token), {
			request: constants.GET_ADMIN_ALL_USER,
      success: constants.GET_ADMIN_ALL_USER_SUCCESS,
      failure: constants.GET_ADMIN_ALL_USER_FAIL
		});
	},

	searchUser(keyword) {
		AppDispatcher.dispatch({
			type: constants.SEARCH_ADMIN_USER,
			payload: { keyword }
		});
	},

	deleteUser(token, user) {
		AppDispatcher.dispatchAsync(AdminAPI.deleteUser(token, user), {
			request: constants.DELETE_USER,
      success: constants.DELETE_USER_SUCCESS,
      failure: constants.DELETE_USER_FAIL
		});
	}

};

export default AdminManagerActionCreators;
