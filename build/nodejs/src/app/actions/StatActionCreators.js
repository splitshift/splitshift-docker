import AppDispatcher from '../AppDispatcher';
import { browserHistory } from 'react-router';
import constants from '../constants';

import StatAPI from '../apis/StatAPI';

const StatActionCreators = {
  getStat() {
    AppDispatcher.dispatchAsync(StatAPI.getStat(), {
			request: constants.GET_STAT,
			success: constants.GET_STAT_SUCCESS,
			failure: constants.GET_STAT_FAIL
		});
  },

  getAdminSummary(token) {
    AppDispatcher.dispatchAsync(StatAPI.getAdminSummary(token), {
			request: constants.GET_ADMIN_DASHBOARD,
			success: constants.GET_ADMIN_DASHBOARD_SUCCESS,
			failure: constants.GET_ADMIN_DASHBOARD_FAIL
		});
  },

  getUserSummary(token) {
    AppDispatcher.dispatchAsync(StatAPI.getUserSummary(token), {
			request: constants.GET_USER_DASHBOARD,
			success: constants.GET_USER_DASHBOARD_SUCCESS,
			failure: constants.GET_USER_DASHBOARD_FAIL
		});
  }

};

export default StatActionCreators;
