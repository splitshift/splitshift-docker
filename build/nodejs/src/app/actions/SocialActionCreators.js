import AppDispatcher from '../AppDispatcher';
import { browserHistory } from 'react-router';
import constants from '../constants';

import SocialAPI from '../apis/SocialAPI';

const SocialActionCreators = {
  putFollow(token, user_key) {
    AppDispatcher.dispatchAsync(SocialAPI.putFollow(token, user_key), {
			request: constants.FOLLOW_PEOPLE,
			success: constants.FOLLOW_PEOPLE_SUCCESS,
			failure: constants.FOLLOW_PEOPLE_FAIL
		});
  },
  setDraft(key, value) {

    let response = {};
    response[key] = value;

    AppDispatcher.dispatch({
      type: constants.SET_SOCIAL_DRAFT,
      payload: { response }
    });
  },
  getConnectRequestList(token) {
    AppDispatcher.dispatchAsync(SocialAPI.getConnectRequestList(token), {
			request: constants.GET_REQUEST_PEOPLE,
			success: constants.GET_REQUEST_PEOPLE_SUCCESS,
			failure: constants.GET_REQUEST_PEOPLE_FAIL
		});
  },
  getNotification(token) {
    AppDispatcher.dispatchAsync(SocialAPI.getNotification(token), {
			request: constants.GET_NOTIFICATION,
			success: constants.GET_NOTIFICATION_SUCCESS,
			failure: constants.GET_NOTIFICATION_FAIL
		});
  },
  putNotificationSeen(token) {
    AppDispatcher.dispatchAsync(SocialAPI.putNotificationSeen(token), {
			request: constants.SEEN_NOTIFICATION,
			success: constants.SEEN_NOTIFICATION_SUCCESS,
			failure: constants.SEEN_NOTIFICATION_FAIL
		});
  },
  deleteNotification(token, notification_id) {
    AppDispatcher.dispatchAsync(SocialAPI.deleteNotification(token, notification_id), {
			request: constants.DELETE_NOTIFICATION,
			success: constants.DELETE_NOTIFICATION_SUCCESS,
			failure: constants.DELETE_NOTIFICATION_FAIL
		});
  },
  postConnectRequest(token, user_key) {
    AppDispatcher.dispatchAsync(SocialAPI.postConnectRequest(token, user_key), {
			request: constants.POST_SEND_REQUEST_PEOPLE,
			success: constants.REQUEST_PEOPLE_SUCCESS,
			failure: constants.REQUEST_PEOPLE_FAIL
		});
  },
  putConnectResponse(token, info) {
    AppDispatcher.dispatchAsync(SocialAPI.putConnectResponse(token, info), {
			request: constants.PUT_RESPONSE_PEOPLE,
			success: constants.REQUEST_PEOPLE_SUCCESS,
			failure: constants.REQUEST_PEOPLE_FAIL
		});
  },
  deleteConnect(token, user_key) {
    AppDispatcher.dispatchAsync(SocialAPI.deleteConnect(token, user_key), {
			request: constants.DELETE_RESPONSE_PEOPLE,
			success: constants.REQUEST_PEOPLE_SUCCESS,
			failure: constants.REQUEST_PEOPLE_FAIL
		});
  },
  getFollowers(user_key) {
    AppDispatcher.dispatchAsync(SocialAPI.getFollowers(user_key), {
			request: constants.GET_FOLLOWER,
			success: constants.GET_FOLLOWER_SUCCESS,
			failure: constants.GET_FOLLOWER_FAIL
		});
  },
  getConnections(user_key) {
    AppDispatcher.dispatchAsync(SocialAPI.getConnections(user_key), {
			request: constants.GET_CONNECTIONS,
			success: constants.GET_CONNECTIONS_SUCCESS,
			failure: constants.GET_CONNECTIONS_FAIL
		});
  }
};

export default SocialActionCreators;
