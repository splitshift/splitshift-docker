import AppDispatcher from '../AppDispatcher';
import constants from '../constants';

import ResourceAPI from '../apis/ResourceAPI';
import UploadAPI from '../apis/UploadAPI';
import AdminAPI from '../apis/AdminAPI';

const ResourceActionCreators = {

  setAdminEditor(name, status) {
    let response = {};
    response[name] = status;

    AppDispatcher.dispatch({
      type: constants.SET_ADMIN_EDITOR,
      payload: { response }
    });

  },

  setLanguage(lang) {
    AppDispatcher.dispatch({
      type: constants.SET_LANGUAGE,
      payload: {
        response: {
          language: lang
        }
      }
    });
  },

  setEditor(name, status) {

    let response = {};
    response[name] = status;

    AppDispatcher.dispatch({
      type: constants.EDIT_PROFILE_CARD,
      payload: { response }
    });
  },

  cancelEditor(name, status) {
    let response = {};
    response[name] = status;

    AppDispatcher.dispatch({
      type: constants.CANCEL_PROFILE_EDIT,
      payload: { response }
    });
  },

  setDraft(name, value) {

    let response = {};
    response[name] = value;

    AppDispatcher.dispatch({
      type: constants.EDIT_DRAFT,
      payload: { response }
    });

  },

  setOldDraft(response) {
    AppDispatcher.dispatch({
      type: constants.EDIT_DRAFT,
      payload: { response }
    });
  },


  // Resource
  resetResource() {
    AppDispatcher.dispatch({
      type: constants.RESET_RESOURCE
    });
  },
  editResourceDraft(response) {
    AppDispatcher.dispatch({
      type: constants.EDIT_RESOURCE_DRAFT,
      payload: { response }
    });
  },
  changeFilter(response) {
    AppDispatcher.dispatch({
      type: constants.EDIT_FILTER_RESOURCE_SEARCH,
      payload: { response }
    });
  },
  updateFeaturedImage(token, info) {
    AppDispatcher.dispatchAsync(UploadAPI.uploadImage(token, info), {
			request: constants.UPLOAD_FEATURED_IMAGE,
      success: constants.UPLOAD_FEATURED_IMAGE_SUCCESS,
      failure: constants.UPLOAD_FEATURED_IMAGE_FAIL
		});
  },
  getAllResource() {
    AppDispatcher.dispatchAsync(ResourceAPI.getResourceIndex(), {
			request: constants.GET_ALL_RESOURCE,
      success: constants.GET_ALL_RESOURCE_SUCCESS,
      failure: constants.GET_ALL_RESOURCE_FAIL
		});
  },
  getAdminResource(token) {
    AppDispatcher.dispatchAsync(ResourceAPI.getAdminResource(token), {
			request: constants.GET_RESOURCE_ADMIN,
      success: constants.GET_RESOURCE_ADMIN_SUCCESS,
      failure: constants.GET_RESOURCE_ADMIN_FAIL
		});
  },
  getResources(token) {
    AppDispatcher.dispatchAsync(ResourceAPI.getResources(token), {
			request: constants.GET_RESOURCE_OTHER,
      success: constants.GET_RESOURCE_OTHER_SUCCESS,
      failure: constants.GET_RESOURCE_OTHER_FAIL
		});
  },
  getResource(id, token) {
    AppDispatcher.dispatchAsync(ResourceAPI.getResource(id, token), {
			request: constants.GET_RESOURCE_SINGLE,
      success: constants.GET_RESOURCE_SINGLE_SUCCESS,
      failure: constants.GET_RESOURCE_SINGLE_FAIL
		});
  },
  approveResource(token, resource) {
		AppDispatcher.dispatchAsync(AdminAPI.approveResource(token, resource), {
			request: constants.POST_APPROVE_RESOURCE,
      success: constants.POST_APPROVE_RESOURCE_SUCCESS,
      failure: constants.POST_APPROVE_RESOURCE_FAIL
		});
	},
  searchResource(info) {
    AppDispatcher.dispatchAsync(ResourceAPI.searchResource(info), {
			request: constants.SEARCH_RESOURCE,
      success: constants.SEARCH_RESOURCE_SUCCESS,
      failure: constants.SEARCH_RESOURCE_FAIL
		});
  },
  createResource(token, info) {
    AppDispatcher.dispatchAsync(ResourceAPI.createResource(token, info), {
			request: constants.ADD_RESOURCE,
      success: constants.MANAGE_RESOURCE_SUCCESS,
      failure: constants.MANAGE_RESOURCE_FAIL
		});
  },
  editResource(token, info) {
    AppDispatcher.dispatchAsync(ResourceAPI.editResource(token, info), {
			request: constants.EDIT_RESOURCE,
      success: constants.MANAGE_RESOURCE_SUCCESS,
      failure: constants.MANAGE_RESOURCE_FAIL
		});
  },
  deleteResource(token, info) {
    AppDispatcher.dispatchAsync(ResourceAPI.deleteResource(token, info), {
			request: constants.DELETE_RESOURCE,
      success: constants.MANAGE_RESOURCE_SUCCESS,
      failure: constants.MANAGE_RESOURCE_FAIL
		});
  },
  likeResource(token, info) {
    AppDispatcher.dispatchAsync(ResourceAPI.likeResource(token, info), {
			request: constants.LIKE_RESOURCE,
      success: constants.LIKE_RESOURCE_SUCCESS,
      failure: constants.LIKE_RESOURCE_FAIL
		});
  },
  commentResource(token, info, resource_id) {
    AppDispatcher.dispatchAsync(ResourceAPI.commentResource(token, info, resource_id), {
			request: constants.COMMENT_RESOURCE,
      success: constants.COMMENT_RESOURCE_SUCCESS,
      failure: constants.COMMENT_RESOURCE_FAIL
		});
  },
  deleteCommentResource(token, info, resource_id) {
    AppDispatcher.dispatchAsync(ResourceAPI.deleteCommentResource(token, info, resource_id), {
			request: constants.DELETE_COMMENT_RESOURCE,
      success: constants.DELETE_COMMENT_RESOURCE_SUCCESS,
      failure: constants.DELETE_COMMENT_RESOURCE_FAIL
		});
  },
  onFilterResource(department) {
    AppDispatcher.dispatch({
      type: constants.FILTER_RESOURCE,
      payload: { department }
    });
  },
  forwardEmail(token, info) {
    AppDispatcher.dispatchAsync(ResourceAPI.forwardEmail(token, info), {
      request: constants.SEND_RESOURCE_FORWARD_EMAIL,
      success: constants.SEND_RESOURCE_FORWARD_EMAIL_SUCCESS,
      failure: constants.SEND_RESOURCE_FORWARD_EMAIL_FAIL
    });
  }

}

export default ResourceActionCreators;
