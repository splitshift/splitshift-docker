import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import UserProfileAPI from '../apis/UserProfileAPI';
import { browserHistory  } from 'react-router';

const UserProfileActionCreators = {
	getUserProfile(key, token) {

		AppDispatcher.dispatchAsync(UserProfileAPI.getUserProfile(key, token), {
			request: constants.GET_USER_PROFILE,
			success: constants.GET_USER_PROFILE_SUCCESS,
			failure: constants.GET_USER_PROFILE_FAIL
		});
	},
	reloadUserProfile(key) {
		AppDispatcher.dispatchAsync(UserProfileAPI.getUserProfile(key), {
			request: constants.RELOAD_USER_PROFILE,
			success: constants.GET_USER_PROFILE_SUCCESS,
			failure: constants.GET_USER_PROFILE_FAIL
		});
	},
	updateProfile(info) {
		AppDispatcher.dispatch({
			type: constants.UPDATE_PROFILE,
			payload: {
				response: info
			}
		});
	},
	resetProfile() {
		AppDispatcher.dispatch({
			type: constants.RESET_USER_PROFILE
		});
	},
	updateInfo(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.updateInfo(token, info), {
			request: constants.UPDATE_INFO_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	updateAbout(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.updateAbout(token, info), {
			request: constants.UPDATE_ABOUT_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	createInterest(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.createInterest(token, info), {
			request: constants.UPDATE_INTEREST_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	editInterest(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.editInterest(token, info), {
			request: constants.UPDATE_INTEREST_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	deleteInterest(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.deleteInterest(token, info), {
			request: constants.DELETE_INTEREST_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	createExperience(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.createExperience(token, info), {
			request: constants.UPDATE_EXPERIENCE_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	editExperience(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.editExperience(token, info), {
			request: constants.UPDATE_EXPERIENCE_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	deleteExperience(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.deleteExperience(token, info), {
			request: constants.DELETE_EXPERIENCE_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	createEducation(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.createEducation(token, info), {
			request: constants.UPDATE_EDUCATION_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	editEducation(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.editEducation(token, info), {
			request: constants.UPDATE_EDUCATION_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	deleteEducation(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.deleteEducation(token, info), {
			request: constants.DELETE_EDUCATION_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	updateSkill(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.updateSkill(token, info), {
			request: constants.UPDATE_SKILL_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	createCourse(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.createCourse(token, info), {
			request: constants.UPDATE_COURSE_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	editCourse(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.editCourse(token, info), {
			request: constants.UPDATE_COURSE_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	deleteCourse(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.deleteCourse(token, info), {
			request: constants.DELETE_COURSE_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	createLanguage(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.createLanguage(token, info), {
			request: constants.UPDATE_LANGUAGE_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	editLanguage(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.editLanguage(token, info), {
			request: constants.UPDATE_LANGUAGE_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	deleteLanguage(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.deleteLanguage(token, info), {
			request: constants.DELETE_LANGUAGE_PROFILE,
			success: constants.UPDATE_PROFILE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	uploadProfileImage(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.uploadProfileImage(token, info), {
			request: constants.UPLOAD_PROFILE_IMAGE,
			success: constants.UPLOAD_PROFILE_IMAGE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	uploadBannerImage(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.uploadBannerImage(token, info), {
			request: constants.UPLOAD_BANNER_IMAGE,
			success: constants.UPLOAD_BANNER_IMAGE_SUCCESS,
			failure: constants.UPDATE_PROFILE_ERROR
		});
	},
	changeBannerImageWithTemplate(token, info) {
		AppDispatcher.dispatchAsync(UserProfileAPI.changeBannerImageWithTemplate(token, info), {
			request: constants.CHANGE_USER_BANNER_IMAGE_WITH_TEMPLATE,
			success: constants.CHANGE_USER_BANNER_IMAGE_WITH_TEMPLATE_SUCCESS,
			failure: constants.CHANGE_USER_BANNER_IMAGE_WITH_TEMPLATE_FAIL
		});
	},
	loadCompany() {
		AppDispatcher.dispatchAsync(UserProfileAPI.loadCompany(), {
			request: constants.LOAD_ALL_COMPANY,
			success: constants.LOAD_ALL_COMPANY_SUCCESS,
			failure: constants.LOAD_ALL_COMPANY_FAIL
		});
	},

}

export default UserProfileActionCreators;
