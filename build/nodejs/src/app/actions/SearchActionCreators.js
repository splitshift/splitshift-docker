import AppDispatcher from '../AppDispatcher';
import constants from '../constants';

import SearchAPI from '../apis/SearchAPI';

const SearchActionCreators = {

  Job(info, page=0) {

    if(page === 0) {
      AppDispatcher.dispatchAsync(SearchAPI.job(info, page), {
  			request: constants.SEARCH_JOB,
  			success: constants.SEARCH_JOB_SUCCESS,
  			failure: constants.SEARCH_JOB_FAIL
  		});
    }else {
      AppDispatcher.dispatchAsync(SearchAPI.job(info, page), {
  			request: constants.SEARCH_JOB_MORE,
  			success: constants.SEARCH_JOB_MORE_SUCCESS,
  			failure: constants.SEARCH_JOB_FAIL
  		});
    }

  },

  People(info, page=0) {

    if(page === 0) {
      AppDispatcher.dispatchAsync(SearchAPI.people(info, page), {
  			request: constants.SEARCH_PEOPLE,
  			success: constants.SEARCH_PEOPLE_SUCCESS,
  			failure: constants.SEARCH_PEOPLE_FAIL
  		});
    }else {
      AppDispatcher.dispatchAsync(SearchAPI.people(info, page), {
  			request: constants.SEARCH_PEOPLE_MORE,
  			success: constants.SEARCH_PEOPLE_MORE_SUCCESS,
  			failure: constants.SEARCH_PEOPLE_FAIL
  		});
    }

  },

  Company(info, page=0) {

    if(page === 0) {
      AppDispatcher.dispatchAsync(SearchAPI.company(info, page), {
  			request: constants.SEARCH_COMPANY,
  			success: constants.SEARCH_COMPANY_SUCCESS,
  			failure: constants.SEARCH_COMPANY_FAIL
  		});
    }else {
      AppDispatcher.dispatchAsync(SearchAPI.company(info, page), {
  			request: constants.SEARCH_COMPANY_MORE,
  			success: constants.SEARCH_COMPANY_MORE_SUCCESS,
  			failure: constants.SEARCH_COMPANY_FAIL
  		});
    }

  },

  All(info) {
    AppDispatcher.dispatchAsync(SearchAPI.all(info), {
			request: constants.SEARCH_ALL,
			success: constants.SEARCH_ALL_SUCCESS,
			failure: constants.SEARCH_ALL_FAIL
		});
  },

  clearFilter() {
    AppDispatcher.dispatch({
      type: constants.CLEAR_FILTER
    });
  },

  updateFilter(response) {
    AppDispatcher.dispatch({
      type: constants.UPDATE_FILTER,
      payload: { response }
    });
  },

  chooseJobSearch(index) {
    AppDispatcher.dispatch({
      type: constants.CHOOSE_JOB_SEARCH,
      payload: { job_index: index }
    });
  }

}

export default SearchActionCreators;
