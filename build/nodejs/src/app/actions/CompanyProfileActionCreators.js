import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import CompanyProfileAPI from '../apis/CompanyProfileAPI';
import { browserHistory  } from 'react-router';

const CompanyProfileActionCreators = {
	getProfile(key, token) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.getProfile(key, token), {
			request: constants.GET_COMPANY_PROFILE,
			success: constants.GET_COMPANY_PROFILE_SUCCESS,
			failure: constants.GET_COMPANY_PROFILE_FAIL
		});
	},
	updateProfile(info) {
		AppDispatcher.dispatch({
			type: constants.UPDATE_COMPANY_PROFILE,
			payload: {
				response: info
			}
		});
	},
	resetProfile() {
		AppDispatcher.dispatch({
			type: constants.RESET_COMPANY_PROFILE
		});
	},
	updateInformation(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.updateInformation(token, info), {
			request: constants.UPDATE_INFORMATION_COMPANY_PROFILE,
			success: constants.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateOverview(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.updateOverview(token, info), {
			request: constants.UPDATE_OVERVIEW_COMPANY_PROFILE,
			success: constants.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateCulture(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.updateCulture(token, info), {
			request: constants.UPDATE_CULTURE_COMPANY_PROFILE,
			success: constants.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateAddress(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.updateAddress(token, info), {
			request: constants.UPDATE_ADDRESS_COMPANY_PROFILE,
			success: constants.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateVideo(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.updateVideo(token, info), {
			request: constants.UPDATE_VIDEO_COMPANY_PROFILE,
			success: constants.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	createBenefit(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.createBenefit(token, info), {
			request: constants.UPDATE_BENEFIT_COMPANY_PROFILE,
			success: constants.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	deleteBenefit(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.deleteBenefit(token, info), {
			request: constants.DELETE_BENEFIT_COMPANY_PROFILE,
			success: constants.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	updateSocialLink(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.updateSocialLink(token, info), {
			request: constants.UPDATE_SOCIAL_LINK_COMPANY_PROFILE,
			success: constants.UPDATE_COMPANY_PROFILE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	uploadProfileImage(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.uploadProfileImage(token, info), {
			request: constants.UPLOAD_PROFILE_IMAGE,
			success: constants.UPLOAD_PROFILE_IMAGE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},
	uploadBannerImage(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.uploadBannerImage(token, info), {
			request: constants.UPLOAD_BANNER_IMAGE,
			success: constants.UPLOAD_BANNER_IMAGE_SUCCESS,
			failure: constants.UPDATE_COMPANY_PROFILE_FAIL
		});
	},

	changeBannerImageWithTemplate(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.changeBannerImageWithTemplate(token, info), {
			request: constants.CHANGE_COMPANY_BANNER_IMAGE_WITH_TEMPLATE,
			success: constants.CHANGE_COMPANY_BANNER_IMAGE_WITH_TEMPLATE_SUCCESS,
			failure: constants.CHANGE_COMPANY_BANNER_IMAGE_WITH_TEMPLATE_FAIL
		});
	},

	reviewCompany(token, info) {
		AppDispatcher.dispatchAsync(CompanyProfileAPI.reviewCompany(token, info), {
			request: constants.REVIEW_COMPANY,
			success: constants.REVIEW_COMPANY_SUCCESS,
			failure: constants.REVIEW_COMPANY_FAIL
		});
	}

};

export default CompanyProfileActionCreators;
