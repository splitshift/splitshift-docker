import AppDispatcher from '../AppDispatcher';
import constants from '../constants';

import MessageAPI from '../apis/MessageAPI';
import UploadAPI from '../apis/UploadAPI';

const MessageActionCreators = {

  setMessageDraft(type, value, people_id) {
    let response  = {};
    response.type = type;
    response.message = value;
    response.to = people_id;

    AppDispatcher.dispatch({
      type: constants.EDIT_MESSAGE_DRAFT,
      payload: { response }
    });
  },

  chooseOwner(user) {
    AppDispatcher.dispatch({
      type: constants.CHOOSE_OWNER_MESSAGE,
      payload: { response: user }
    });
  },

  chooseOther(profile) {
    AppDispatcher.dispatch({
      type: constants.CHOOSE_OTHER_MESSAGE,
      payload: { response: profile }
    });
  },

  chooseMessage(message) {
    AppDispatcher.dispatch({
      type: constants.GET_MESSAGE_SUCCESS,
      payload: { response: message }
    });
  },

  sendFile(token, type, value, people_id) {

    this.setMessageDraft(type, value, people_id);

    if(type == 'image') {
      AppDispatcher.dispatchAsync(UploadAPI.uploadImage(token, value), {
  			request: constants.UPLOAD_MESSAGE_IMAGE,
        success: constants.UPLOAD_MESSAGE_IMAGE_SUCCESS,
        failure: constants.UPLOAD_MESSAGE_FILE_FAIL
  		});
    }else {
      AppDispatcher.dispatchAsync(UploadAPI.uploadFile(token, value), {
  			request: constants.UPLOAD_MESSAGE_FILE,
        success: constants.UPLOAD_MESSAGE_FILE_SUCCESS,
        failure: constants.UPLOAD_MESSAGE_FILE_FAIL
  		});
    }

  },

  sendMessage(token, info) {
    AppDispatcher.dispatchAsync(MessageAPI.sendMessage(token, info), {
			request: constants.SEND_MESSAGE,
      success: constants.SEND_MESSAGE_SUCCESS,
      failure: constants.SEND_MESSAGE_FAIL
		});

  },

  getAllMessage(token) {
    AppDispatcher.dispatchAsync(MessageAPI.getAllMessage(token), {
			request: constants.GET_ALL_MESSAGE,
      success: constants.GET_ALL_MESSAGE_SUCCESS,
      failure: constants.GET_ALL_MESSAGE_FAIL
		});
  },

  getMessageWithPeople(token, people_id) {
    AppDispatcher.dispatchAsync(MessageAPI.getMessageWithPeople(token, people_id), {
			request: constants.GET_MESSAGE,
      success: constants.GET_MESSAGE_SUCCESS,
      failure: constants.GET_MESSAGE_FAIL
		});
  }

}

export default MessageActionCreators;
