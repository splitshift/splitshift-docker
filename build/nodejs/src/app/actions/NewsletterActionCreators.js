import AppDispatcher from '../AppDispatcher';
import { browserHistory } from 'react-router';
import constants from '../constants';

import NewsletterAPI from '../apis/NewsletterAPI';

const NewsletterActionCreators = {
   getNewsletters(token) {
     AppDispatcher.dispatchAsync(NewsletterAPI.getNewsletters(token), {
       request: constants.GET_NEWSLETTER,
       success: constants.GET_NEWSLETTER_SUCCESS,
       failure: constants.GET_NEWSLETTER_FAIL
     });
   },

   postNewsletter(info) {
    AppDispatcher.dispatchAsync(NewsletterAPI.postNewsletter(info), {
			request: constants.SEND_NEWSLETTER,
			success: constants.SEND_NEWSLETTER_SUCCESS,
			failure: constants.SEND_NEWSLETTER_FAIL
		});
  },

  sendEmailSingle(token, info) {
    AppDispatcher.dispatchAsync(NewsletterAPI.sendEmailSingle(token, info), {
			request: constants.SEND_EMAIL_SINGLE,
			success: constants.SEND_EMAIL_SUCCESS,
			failure: constants.SEND_EMAIL_FAIL
		});
  },

  sendEmailSubscibers(token, info) {
    AppDispatcher.dispatchAsync(NewsletterAPI.sendEmailSubscibers(token, info), {
			request: constants.SEND_EMAIL_SUBSCRIBER,
			success: constants.SEND_EMAIL_SUCCESS,
			failure: constants.SEND_EMAIL_FAIL
		});
  }

};

export default NewsletterActionCreators;
