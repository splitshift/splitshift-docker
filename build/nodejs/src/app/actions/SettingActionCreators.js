import AppDispatcher from '../AppDispatcher';
import constants from '../constants';

import SettingAPI from '../apis/SettingAPI';

const SettingActionCreators = {

  changeDraft(response) {
    AppDispatcher.dispatch({
      type: constants.CHANGE_SETTING_DRAFT,
      payload: { response }
    })
  },

  deleteAccount(token) {
    AppDispatcher.dispatchAsync(SettingAPI.deleteAccount(token), {
			request: constants.DELETE_ACCOUNT,
			success: constants.DELETE_ACCOUNT_SUCCESS,
			failure: constants.DELETE_ACCOUNT_FAIL
		});
  },

  changePassword(token, info) {
    AppDispatcher.dispatchAsync(SettingAPI.changePassword(token, info) , {
			request: constants.CHANGE_PASSWORD,
			success: constants.CHANGE_PASSWORD_SUCCESS,
			failure: constants.CHANGE_PASSWORD_FAIL
		});
  },

  changePrimaryEmail(token, info) {
    AppDispatcher.dispatchAsync(SettingAPI.changePrimaryEmail(token, info), {
			request: constants.CHANGE_PRIMARY_EMAIL,
			success: constants.CHANGE_PRIMARY_EMAIL_SUCCESS,
			failure: constants.CHANGE_PRIMARY_EMAIL_FAIL
		});
  }


}

export default SettingActionCreators;
