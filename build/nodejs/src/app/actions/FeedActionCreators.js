import AppDispatcher from '../AppDispatcher';
import constants from '../constants';

import FeedAPI from '../apis/FeedAPI';
import UploadAPI from '../apis/UploadAPI';

const FeedActionCreators = {

  setOwner(user) {
    AppDispatcher.dispatch({
      type: constants.SET_OWNER_FEED,
      payload: { owner: user }
    });
  },

  setFeedDraft(name, value) {

    let response = {};
    response[name] = value;

    AppDispatcher.dispatch({
      type: constants.EDIT_FEED_DRAFT,
      payload: { response }
    });
  },

  editImage() {
    AppDispatcher.dispatch({
      type: constants.EDIT_FEED_IMAGE
    });
  },

  editVideo() {
    AppDispatcher.dispatch({
      type: constants.EDIT_FEED_VIDEO
    });
  },

  onReply(comment) {
    AppDispatcher.dispatch({
      type: constants.ON_REPLY,
      payload: comment
    });
  },

  resetFeed() {
    AppDispatcher.dispatch({
      type: constants.RESET_FEED,
    });
  },

  chooseEditFeed(feed, index, user) {
    AppDispatcher.dispatch({
      type: constants.CHOOSE_EDIT_FEED,
      payload: { feed: feed, index: index, owner: user}
    });
  },

  cancelEditFeed() {
    AppDispatcher.dispatch({
      type: constants.CANCEL_EDIT_FEED,
    });
  },

  uploadImage(token, image) {
    AppDispatcher.dispatchAsync(UploadAPI.uploadImage(token, image), {
      request: constants.UPLOAD_FEED_PHOTO,
      success: constants.UPLOAD_FEED_PHOTO_SUCCESS,
      failure: constants.UPLOAD_FEED_PHOTO_FAIL
    });
  },

  deleteImage(token, key) {
    AppDispatcher.dispatch({
      type: constants.DELETE_FEED_PHOTO,
      payload: {image_key: key}
    });
  },

  getFeed(user_key, token, page=0) {

    if(page == 0) {
      AppDispatcher.dispatchAsync(FeedAPI.getFeed(user_key, token, page), {
  			request: constants.GET_FEED,
        success: constants.GET_FEED_SUCCESS,
        failure: constants.GET_FEED_FAIL
  		});
    }else {
      AppDispatcher.dispatchAsync(FeedAPI.getFeed(user_key, token, page), {
  			request: constants.GET_FEED_MORE,
        success: constants.GET_FEED_MORE_SUCCESS,
        failure: constants.GET_FEED_FAIL
  		});
    }

  },

  getPhotosFeed(user_key, token) {
    AppDispatcher.dispatchAsync(FeedAPI.getPhotosFeed(user_key, token), {
			request: constants.GET_PHOTO_FEED,
      success: constants.GET_PHOTO_FEED_SUCCESS,
      failure: constants.GET_PHOTO_FEED_FAIL
		});
  },

  getAlbums(user_key) {
    AppDispatcher.dispatchAsync(FeedAPI.getAlbums(user_key), {
			request: constants.GET_ALBUM,
      success: constants.GET_ALBUM_SUCCESS,
      failure: constants.GET_ALBUM_FAIL
		});
  },

  getPhotosInAlbum(user_key, info, token) {
    AppDispatcher.dispatchAsync(FeedAPI.getPhotosInAlbum(user_key, info, token), {
			request: constants.GET_PHOTO_IN_ALBUM,
      success: constants.GET_PHOTO_IN_ALBUM_SUCCESS,
      failure: constants.GET_PHOTO_IN_ALBUM_FAIL
		});
  },

  postFeed(token, info) {
    AppDispatcher.dispatchAsync(FeedAPI.postFeed(token, info), {
			request: constants.POST_FEED,
      success: constants.POST_FEED_SUCCESS,
      failure: constants.POST_FEED_FAIL
		});
  },

  editFeed(token, info) {
    AppDispatcher.dispatchAsync(FeedAPI.editFeed(token, info), {
			request: constants.EDIT_POST_FEED,
      success: constants.EDIT_POST_FEED_SUCCESS,
      failure: constants.POST_FEED_FAIL
		});
  },

  changePrivacy(token, info) {
    AppDispatcher.dispatchAsync(FeedAPI.changePrivacy(token, info), {
			request: constants.CHANGE_PRIVACY,
      success: constants.POST_FEED_SUCCESS,
      failure: constants.POST_FEED_FAIL
		});
  },

  deleteFeed(token, info) {
    AppDispatcher.dispatchAsync(FeedAPI.deleteFeed(token, info), {
			request: constants.DELETE_FEED,
      success: constants.DELETE_FEED_SUCCESS,
      failure: constants.DELETE_FEED_FAIL
		});
  },

  deleteBuzzFeed(token, info) {
    AppDispatcher.dispatchAsync(FeedAPI.deleteBuzzFeed(token, info), {
			request: constants.DELETE_FEED,
      success: constants.DELETE_FEED_SUCCESS,
      failure: constants.DELETE_FEED_FAIL
		});
  },

  postCommentFeed(token, info) {
    AppDispatcher.dispatchAsync(FeedAPI.postCommentFeed(token, info), {
			request: constants.POST_COMMENT_FEED,
      success: constants.POST_COMMENT_FEED_SUCCESS,
      failure: constants.POST_COMMENT_FEED_FAIL
		});
  },

  putLikeFeed(token, info) {
    AppDispatcher.dispatchAsync(FeedAPI.putLikeFeed(token, info), {
      request: constants.LIKE_FEED,
      success: constants.LIKE_FEED_SUCCESS,
      failure: constants.LIKE_FEED_FAIL
    });
  },

  deleteCommentFeed(token, info, feed) {
    AppDispatcher.dispatchAsync(FeedAPI.deleteCommentFeed(token, info, feed), {
      request: constants.DELETE_COMMENT_FEED,
      success: constants.DELETE_COMMENT_FEED_SUCCESS,
      failure: constants.DELETE_COMMENT_FEED_FAIL
    });
  },

  postReplyComment(token, info) {
    AppDispatcher.dispatchAsync(FeedAPI.postReplyComment(token, info), {
      request: constants.POST_REPLY_COMMENT_FEED,
      success: constants.POST_REPLY_COMMENT_FEED_SUCCESS,
      failure: constants.POST_REPLY_COMMENT_FEED_FAIL
    });
  },

  deleteReply(token, info, reply) {
    AppDispatcher.dispatchAsync(FeedAPI.deleteReply(token, info, reply), {
      request: constants.DELETE_REPLY_COMMENT_FEED,
      success: constants.DELETE_REPLY_COMMENT_FEED_SUCCESS,
      failure: constants.DELETE_REPLY_COMMENT_FEED_FAIL
    });
  },

  getBuzzFeed(page=0) {

    if(page === 0) {
      AppDispatcher.dispatchAsync(FeedAPI.getBuzzFeed(page), {
        request: constants.GET_BUZZ_FEED,
        success: constants.GET_BUZZ_FEED_SUCCESS,
        failure: constants.GET_BUZZ_FEED_FAIL
      });
    }else {
      AppDispatcher.dispatchAsync(FeedAPI.getBuzzFeed(page), {
        request: constants.GET_BUZZ_FEED_MORE,
        success: constants.GET_BUZZ_FEED_MORE_SUCCESS,
        failure: constants.GET_BUZZ_FEED_FAIL
      });
    }

  },

  searchBuzzFeed(keyword) {
    AppDispatcher.dispatchAsync(FeedAPI.searchBuzzFeed(keyword), {
      request: constants.SEARCH_BUZZ_FEED,
      success: constants.SEARCH_BUZZ_FEED_SUCCESS,
      failure: constants.SEARCH_BUZZ_FEED_FAIL
    });
  }

}

export default FeedActionCreators;
