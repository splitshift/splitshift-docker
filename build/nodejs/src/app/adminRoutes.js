import React from 'react';
import { Route, IndexRoute, hashHistory } from 'react-router' ;

import AdminAppContainer from './containers/Admin/AppContainer';

import AdminDashboardContainer from './containers/Admin/DashboardContainer';

import AdminResourceContainer from './containers/Admin/ResourceContainer';
import AdminResourceCreatorContainer from './containers/Admin/ResourceCreatorContainer';

import NewsletterContainer from './containers/Admin/NewsletterContainer';
import NewsletterCreateContainer from './containers/Admin/NewsletterCreateContainer';

import AdminUserContainer from './containers/Admin/UserContainer';

export default (
  <Route name="language" path="/:language">
    <Route name="admin" path="admin" component={AdminAppContainer}>

      <IndexRoute name="admin_dashboard" component={AdminDashboardContainer} />

      <Route name="admin_newsletter" path="newsletter/index" component={NewsletterContainer} />
      <Route name="admin_newsletter_create" path="newsletter/create" component={NewsletterCreateContainer} />

      <Route name="admin_resource" path="resource/index" component={AdminResourceContainer} />
      <Route name="admin_resource_create" path="resource/create" component={AdminResourceCreatorContainer} />
      <Route name="admin_resource_edit" path="resource/edit/:id" component={AdminResourceCreatorContainer} />

      <Route name="admin_user" path="user/index" component={AdminUserContainer} />

    </Route>
  </Route>
);
