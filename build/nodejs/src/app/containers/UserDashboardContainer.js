import React, { Component } from 'react';
import { Container } from 'flux/utils';

import StatActionCreators from '../actions/StatActionCreators';
import SocialActionCreators from '../actions/SocialActionCreators'


import UserDashboard from '../components/Backend/UserDashboard';

import UserStore from '../stores/UserStore';
import ResourceStore from '../stores/ResourceStore';

let first_load = false;

class UserDashboardContainer extends Component {

  componentDidMount() {
    if(this.state.user.token && this.state.user.type !== 'Admin') {
     StatActionCreators.getUserSummary(this.state.user.token)
     SocialActionCreators.getConnections(this.state.user.key)
     first_load = true;
   }
  }

  render() {
    return (
      <UserDashboard stat={this.state.resource.stat} language={this.state.resource.lang} />
    )
  }

}

UserDashboardContainer.getStores = () => ([UserStore, ResourceStore]);
UserDashboardContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(UserDashboardContainer);
