import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { browserHistory } from 'react-router';

import CompanyHeader from '../components/Backend/Header/Company';
import Sidebar from '../components/Backend/Sidebar';

import AuthenticationActionCreators from '../actions/AuthenticationActionCreators';

import UserStore from '../stores/UserStore';
import ResourceStore from '../stores/ResourceStore';
import SocialStore from '../stores/SocialStore';

let owner_load = false;

class BackendContainer extends Component {

  componentDidMount() {
    if(localStorage.getItem('token')) {
      AuthenticationActionCreators.loginAlready();

      if(localStorage.getItem('type') == 'Employee')
        AuthenticationActionCreators.loadProfile(localStorage.getItem('key'));
      else if(localStorage.getItem('type') != 'Admin')
        AuthenticationActionCreators.loadCompanyProfile(localStorage.getItem('key'));

    }else
      browserHistory.push('/');

    this.state.resource.lang = this.props.params.language || this.state.resource.lang;
  }

  componentDidUpdate () {
    if (this.props.params.language !== this.state.resource.lang) {
      let url = this.props.location.pathname
      url = url.substr(3)
      browserHistory.push('/' + this.state.resource.lang + url)
    }
  }

  render() {
    let lang = this.props.params.language || this.state.resource.lang
    return (
      <div>
        <div id="main">
          <CompanyHeader language={lang} />
          <div className="container-content">
            {React.cloneElement(this.props.children)}
          </div>
        </div>
        <Sidebar language={lang} user={this.state.user} />
      </div>
    );
  }
}

BackendContainer.getStores = () => ([UserStore, ResourceStore, SocialStore]);
BackendContainer.calculateState = (prevState) => ({
  resource: ResourceStore.getState(),
  user: UserStore.getState(),
  social: SocialStore.getState()
});


export default Container.create(BackendContainer);;
