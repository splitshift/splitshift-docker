import React, {Component} from 'react';
import {Container} from 'flux/utils';

import UserStore from '../stores/UserStore';
import CompanyProfileStore from '../stores/CompanyProfileStore';

import Resume from '../components/Frontend/Form/Company/Resume';

import AuthenticationActionCreators from '../actions/AuthenticationActionCreators';
import CompanyProfileActionCreators from '../actions/CompanyProfileActionCreators';

class CompanyResumeContainer extends Component {

  componentDidMount() {
    if(localStorage.getItem('token')) {
      AuthenticationActionCreators.loginAlready();

      if(localStorage.getItem('type') == 'Employee')
        AuthenticationActionCreators.loadProfile(localStorage.getItem('key'));
      else if(localStorage.getItem('type') != 'Admin')
        AuthenticationActionCreators.loadCompanyProfile(localStorage.getItem('key'));

    }
    else {
      AuthenticationActionCreators.guest();
    }

    CompanyProfileActionCreators.getProfile(this.props.params.key);

  }

  render() {
    return (
      this.state.profile.key ?
        <Resume user={this.state.profile} />
      :
        <div />
    );
  }
}

CompanyResumeContainer.getStores = () => ([UserStore, CompanyProfileStore]);
CompanyResumeContainer.calculateState = (prevState) => ({
	user: UserStore.getState(),
  profile: CompanyProfileStore.getState()
});

export default Container.create(CompanyResumeContainer);
