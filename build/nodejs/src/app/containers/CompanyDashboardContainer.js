import React, { Component } from 'react';
import { Container } from 'flux/utils';

import StatActionCreators from '../actions/StatActionCreators';

import CompanyDashboard from '../components/Backend/CompanyDashboard';

import UserStore from '../stores/UserStore';
import ResourceStore from '../stores/ResourceStore';

let first_load = false;

class CompanyDashboardContainer extends Component {

  componentDidMount() {
    first_load = false;
  }

  componentDidUpdate() {

    if(!first_load && this.state.user.token && this.state.user.type !== 'Admin') {
      StatActionCreators.getUserSummary(this.state.user.token)
      first_load = true;
    }

  }

  render() {
    return (
      <CompanyDashboard stat={this.state.resource.stat} language={this.state.resource.lang} />
    )
  }

}

CompanyDashboardContainer.getStores = () => ([UserStore, ResourceStore]);
CompanyDashboardContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(CompanyDashboardContainer);
