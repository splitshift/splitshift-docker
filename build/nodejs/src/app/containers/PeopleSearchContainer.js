import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import PeopleSearch from '../components/Frontend/PeopleSearch';

import SearchStore from '../stores/SearchStore';
import ResourceStore from '../stores/ResourceStore'

class PeopleSearchContainer extends Component {

  render() {
    return (
      <PeopleSearch search={this.state.search} language={this.state.resource.lang} />
    );
  }
}

PeopleSearchContainer.getStores = () => ([SearchStore, ResourceStore]);
PeopleSearchContainer.calculateState = (prevState) => ({
  search: SearchStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(PeopleSearchContainer);
