import React, {Component} from 'react';
import {Container} from 'flux/utils';

import UserStore from '../stores/UserStore';
import ResourceStore from '../stores/ResourceStore';

import ResourceSearch from '../components/Frontend/Resource/Search';

class ResourceSearchContainer extends Component {
  render() {
    return (
      <ResourceSearch resource={this.state.resource} user={this.state.user} language={this.state.resource.lang} />
    );
  }
}

ResourceSearchContainer.getStores = () => ([UserStore, ResourceStore]);
ResourceSearchContainer.calculateState = (prevState) => ({
	user: UserStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(ResourceSearchContainer);
