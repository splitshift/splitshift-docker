import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import HRManagement from '../components/Backend/HRManagement';

import UserStore from '../stores/UserStore';
import HRStore from '../stores/HRStore';

class HRManagementContainer extends Component {

  render() {
    return (
      this.state.user.token ?
        <HRManagement user={this.state.user} hr={this.state.hr} />
      : <div />
    );
  }
}

HRManagementContainer.getStores = () => ([UserStore, HRStore]);
HRManagementContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  hr: HRStore.getState()
});

export default Container.create(HRManagementContainer);
