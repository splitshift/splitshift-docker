import React, {Component} from 'react';
import {Container} from 'flux/utils';

import ResourceActionCreators from '../actions/ResourceActionCreators';

import UserStore from '../stores/UserStore';
import ResourceStore from '../stores/ResourceStore';

import ResourceArticle from '../components/Frontend/Resource/Article';

class ResourceArticleContainer extends Component {

  componentDidMount() {
    ResourceActionCreators.getResource(this.props.params.id, this.state.user.token);
  }

  render() {
    return (
      typeof this.state.resource.resource._id !== 'undefined' ?
        <ResourceArticle resource_id={this.props.params.id} user={this.state.user} resource={this.state.resource} />
      :
        <div />
    );
  }
}

ResourceArticleContainer.getStores = () => ([UserStore, ResourceStore]);
ResourceArticleContainer.calculateState = (prevState) => ({
	user: UserStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(ResourceArticleContainer);
