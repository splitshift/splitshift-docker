import React, {Component} from 'react';
import {Container} from 'flux/utils';

import UserStore from '../stores/UserStore';
import ResourceStore from '../stores/ResourceStore';

import ResourceIndex from '../components/Frontend/Resource/Index';

class ResourceContainer extends Component {
  render() {
    return (
      <ResourceIndex resource={this.state.resource} language={this.state.resource.lang} user={this.state.user} />
    );
  }
}

ResourceContainer.getStores = () => ([UserStore, ResourceStore]);
ResourceContainer.calculateState = (prevState) => ({
	user: UserStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(ResourceContainer);
