import React, {Component} from 'react';
import {Container} from 'flux/utils';
import LoginComponent from '../components/Frontend/Login';
import LoginStore from '../stores/LoginStore';

const sessionStorage = require('sessionstorage');

class Login extends Component {
  render() {
    return (
			<LoginComponent state={this.state.login} resources={this.state.resources} language={this.props.params.language}/>
    );
  }
}

Login.getStores = () => ([LoginStore]);
Login.calculateState = (prevState) => ({
	login: LoginStore.getState(),
  resources: {
    "en": {
      login: 'Login',
      or: 'or',
      email: 'Email',
      password: 'Password',
    },
    "th": {
      login: 'ล็อคอิน',
      or: 'หรือ',
      email: 'อีเมลล์',
      password: 'รหัสผ่าน',
    }
  }
});
const HomeContainer = Container.create(Login);
export default HomeContainer;
