import React, {Component} from 'react';
import {Container} from 'flux/utils';
import UserStore from '../stores/UserStore';
import RegisterComponent from '../components/Frontend/Register';

class Register extends Component {
  render() {
    return (
      <RegisterComponent resources={this.state.resources[this.props.params.language]} language={this.props.params.language}/>
    );
  }
}
Register.getStores = () => ([UserStore]);
Register.calculateState = (prevState) => ({
	user: UserStore.getState(),
  resources: {
    'en': {
      'register': 'Register',
      'or': 'or',
      'register_button': 'Register with email'
    },
    'th': {
      'register': 'ลงทะเบียน',
      'or': 'หรือ',
      'register_button': 'ลงทะเบียนด้วยอีเมล์'
    }
  }
});
const RegisterContainer = Container.create(Register);
export default RegisterContainer;
