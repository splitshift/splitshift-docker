import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import StatActionCreators from '../actions/StatActionCreators';

import Home from '../components/Frontend/Home';
import ResourceStore from '../stores/ResourceStore';
import UserStore from '../stores/UserStore';

import configLang from '../tools/configLang'

class HomeContainer extends Component {

  componentDidMount() {
    StatActionCreators.getStat();
  }

  render() {
    return (
      <Home resource={configLang[this.state.resource.lang]} lang={this.state.resource.lang} stat={this.state.resource.stat} user={this.state.user} />
    )
  }
}

HomeContainer.getStores = () => ([ResourceStore, UserStore]);
HomeContainer.calculateState = (prevState) => ({
  resource: ResourceStore.getState(),
  user: UserStore.getState()
});

export default Container.create(HomeContainer);
