import React, {Component} from 'react';
import {Container} from 'flux/utils';

import UserStore from '../stores/UserStore';
import UserProfileStore from '../stores/UserProfileStore';

import Resume from '../components/Frontend/Form/User/Resume';

import AuthenticationActionCreators from '../actions/AuthenticationActionCreators';
import UserProfileActionCreators from '../actions/UserProfileActionCreators';

class UserResumeContainer extends Component {

  componentDidMount() {
    if(localStorage.getItem('token')) {
      AuthenticationActionCreators.loginAlready();

      if(localStorage.getItem('type') == 'Employee')
        AuthenticationActionCreators.loadProfile(localStorage.getItem('key'));
      else if(localStorage.getItem('type') != 'Admin')
        AuthenticationActionCreators.loadCompanyProfile(localStorage.getItem('key'));

    }
    else {
      AuthenticationActionCreators.guest();
    }

    UserProfileActionCreators.getUserProfile(this.props.params.key);

  }

  render() {
    return (
      this.state.profile.key ?
        <Resume user={this.state.profile} language={this.props.params.language} />
      :
        <div />
    );
  }
}

UserResumeContainer.getStores = () => ([UserStore, UserProfileStore]);
UserResumeContainer.calculateState = (prevState) => ({
	user: UserStore.getState(),
  profile: UserProfileStore.getState()
});

export default Container.create(UserResumeContainer);
