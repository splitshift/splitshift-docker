import React, { Component } from 'react';
import { Container } from 'flux/utils';

import LoginStore from '../stores/LoginStore';

import ForgetPassword from '../components/Frontend/ForgetPassword';

class ForgetPasswordContainer extends Component {

  render() {
    return (
      <ForgetPassword login={this.state.login} forget_id={this.props.params.id} />
    )
  }

}

ForgetPasswordContainer.getStores = () => ([LoginStore]);
ForgetPasswordContainer.calculateState = (prevState) => ({
  login: LoginStore.getState()
});


export default Container.create(ForgetPasswordContainer);;
