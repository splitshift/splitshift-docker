import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import Search from '../components/Frontend/Search';

import SearchStore from '../stores/SearchStore';
import UserStore from '../stores/UserStore';
import JobStore from '../stores/JobStore';

class SearchContainer extends Component {

  render() {
    return (
      <Search search={this.state.search} user={this.state.user} job={this.state.job} />
    );
  }
}

SearchContainer.getStores = () => ([SearchStore, UserStore, JobStore]);
SearchContainer.calculateState = (prevState) => ({
  search: SearchStore.getState(),
  user: UserStore.getState(),
  job: JobStore.getState()
});

export default Container.create(SearchContainer);
