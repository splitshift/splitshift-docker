import React, { Component } from 'react';
import {Container} from 'flux/utils';

import JobActionCreators from '../../actions/JobActionCreators';

import JobCreate from '../../components/Backend/Job/Create';

import ResourceStore from '../../stores/ResourceStore';
import UserStore from '../../stores/UserStore';
import JobStore from '../../stores/JobStore';

let first_load = false;

class JobCreateContainer extends Component {

  componentDidMount() {
    JobActionCreators.resetDraft();
    first_load = false;
  }

  componentDidUpdate() {

    if(!first_load && typeof this.props.params.id !== 'undefined' && typeof this.state.user.token !== 'undefined') {
      JobActionCreators.getJob(this.state.user.token, this.props.params.id);
      first_load = true;
    }
  }

  render() {
    return (
      typeof this.state.user.token !== 'undefined' && (typeof this.props.params.id === 'undefined' || typeof this.state.job.edit_job_id !== 'undefined') ?
        <JobCreate user={this.state.user} job={this.state.job} language={this.state.resource.lang} />
      :
        <div />
    );
  }
}

JobCreateContainer.getStores = () => ([ResourceStore, UserStore, JobStore]);
JobCreateContainer.calculateState = (prevState) => ({
  resource: ResourceStore.getState(),
  user: UserStore.getState(),
  job: JobStore.getState()
});

export default Container.create(JobCreateContainer);
