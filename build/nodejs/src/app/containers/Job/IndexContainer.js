import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import Job from '../../components/Backend/Job';

import ResourceStore from '../../stores/ResourceStore';
import UserStore from '../../stores/UserStore';
import JobStore from '../../stores/JobStore';


class JobContainer extends Component {

  render() {
    return (
      typeof this.state.user.token !== 'undefined' ?
        <Job user={this.state.user} job={this.state.job} language={this.state.resource.lang} />
      :
       <div />
    );
  }
}

JobContainer.getStores = () => ([ResourceStore, UserStore, JobStore]);
JobContainer.calculateState = (prevState) => ({
  resource: ResourceStore.getState(),
  user: UserStore.getState(),
  job: JobStore.getState()
});

export default Container.create(JobContainer);
