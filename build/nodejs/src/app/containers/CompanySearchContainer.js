import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import CompanySearch from '../components/Frontend/CompanySearch';

import SearchStore from '../stores/SearchStore';
import ResourceStore from '../stores/ResourceStore';

class CompanySearchContainer extends Component {

  render() {
    return (
      <CompanySearch search={this.state.search} language={this.state.resource.lang} />
    );
  }
}

CompanySearchContainer.getStores = () => ([SearchStore]);
CompanySearchContainer.calculateState = (prevState) => ({
  search: SearchStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(CompanySearchContainer);
