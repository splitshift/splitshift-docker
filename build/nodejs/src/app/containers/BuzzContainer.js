import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import BuzzFeed from '../components/Frontend/BuzzFeed';

import FeedActionCreators from '../actions/FeedActionCreators';

import UserStore from '../stores/UserStore';
import FeedStore from '../stores/FeedStore';
import ResourceStore from '../stores/ResourceStore'

class BuzzContainer extends Component {

  componentDidMount() {
    FeedActionCreators.getBuzzFeed();
  }

  render() {
    return (
      this.state.feed.buzz ?
        <BuzzFeed user={this.state.user} feed={this.state.feed} language={this.state.resource.lang} />
      : <div />
    );
  }
}

BuzzContainer.getStores = () => ([UserStore, FeedStore]);
BuzzContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  feed: FeedStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(BuzzContainer);
