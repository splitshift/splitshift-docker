
import React, { Component } from 'react';
import { Container } from 'flux/utils';

import CompanyProfile from '../components/Frontend/CompanyProfile';

// stores
import CompanyProfileStore from '../stores/CompanyProfileStore';
import UserStore from '../stores/UserStore';
import ResourceStore from '../stores/ResourceStore';
import MessageStore from '../stores/MessageStore';
import FeedStore from '../stores/FeedStore';
import JobStore from '../stores/JobStore';

class CompanyProfileContainer extends Component {

	render() {
		return (
			typeof this.props.params.key !== 'undefined' || typeof this.state.user.token !== 'undefined' ?
			<CompanyProfile
				profile={this.state.profile}
				user={this.state.user}
				status={this.state.status}
				profile_key={this.props.params.key}
				feed_id={this.props.params.feed_id}
				lang={this.state.lang}
				msg={this.state.msg}
				feed={this.state.feed}
				job={this.state.job}
				page={this.props.routes[this.props.routes.length - 1].name}
				editor={this.props.routes[this.props.routes.length - 1].name == "company_profile"} />
			: <div />
		)
	}

}

CompanyProfileContainer.getStores = () => ([CompanyProfileStore, UserStore, ResourceStore, MessageStore, JobStore]);
CompanyProfileContainer.calculateState = (prevState) => ({
	profile: CompanyProfileStore.getState(),
	user: UserStore.getState(),
	status: ResourceStore.getState().status,
	lang: ResourceStore.getState().lang,
	msg: MessageStore.getState(),
	feed: FeedStore.getState(),
	job: JobStore.getState()
});

export default Container.create(CompanyProfileContainer);
