import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import ResourceIndex from '../../components/Backend/Resource/Index';

import UserStore from '../../stores/UserStore';
import ResourceStore from '../../stores/ResourceStore';

class AdminResourceContainer extends Component {

  render() {
    return (
      this.state.user.token ?
      <ResourceIndex
        user={this.state.user}
        resource={this.state.resource}
        language={this.state.resource.lang}
         />
        : <div />
    );
  }
}

AdminResourceContainer.getStores = () => ([UserStore, ResourceStore]);
AdminResourceContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(AdminResourceContainer);
