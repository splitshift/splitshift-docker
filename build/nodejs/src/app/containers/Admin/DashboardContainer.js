import React, { Component } from 'react';
import { Container } from 'flux/utils';

import StatActionCreators from '../../actions/StatActionCreators';

import AdminDashboard from '../../components/Backend/AdminDashboard';

import UserStore from '../../stores/UserStore';
import ResourceStore from '../../stores/ResourceStore';

class AdminDashboardContainer extends Component {

  componentDidMount() {
    if(this.state.user.token && this.state.user.type === 'Admin')
      StatActionCreators.getAdminSummary(this.state.user.token)

    StatActionCreators.getStat()

  }

  render() {
    return (
      <AdminDashboard stat={this.state.resource.stat} />
    );
  }
}

AdminDashboardContainer.getStores = () => ([UserStore, ResourceStore]);
AdminDashboardContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(AdminDashboardContainer);
