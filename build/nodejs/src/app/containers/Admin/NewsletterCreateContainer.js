import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import NewsletterCreate from '../../components/Backend/Newsletter/Create';

import UserStore from '../../stores/UserStore';
import NewsletterStore from '../../stores/NewsletterStore';
import ResourceStore from '../../stores/ResourceStore'

class NewsletterCreateContainer extends Component {

  render() {
    return (
      typeof this.state.user.token !== 'undefined' ?
        <NewsletterCreate user={this.state.user} newsletter={this.state.newsletter} language={this.state.language} />
      : <div />
    );
  }

}

NewsletterCreateContainer.getStores = () => ([UserStore, NewsletterStore]);
NewsletterCreateContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  newsletter: NewsletterStore.getState(),
  language: ResourceStore.getState().lang
});

export default Container.create(NewsletterCreateContainer);
