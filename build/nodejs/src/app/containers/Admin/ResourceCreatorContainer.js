import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import ResourceActionCreators from '../../actions/ResourceActionCreators';

import ResourceCreate from '../../components/Backend/Resource/Create';

import UserStore from '../../stores/UserStore';
import ResourceStore from '../../stores/ResourceStore';

let first_load = false;

class AdminResourceCreatorContainer extends Component {

  componentDidMount() {
    ResourceActionCreators.setAdminEditor('edit_resource', true);
    first_load = false;
  }

  componentDidUpdate() {

    if(!first_load && this.props.params.id && this.state.user.token) {
      ResourceActionCreators.getResource(this.props.params.id, this.state.user.token);
      first_load = true;
    }

  }

  render() {
    return (
      typeof this.props.params.id === 'undefined' || (this.props.params.id && typeof this.state.resource.resource._id !== 'undefined') ?
      <ResourceCreate user={this.state.user} resource={this.state.resource} language={this.state.resource.lang} /> : <div />

    );
  }
}

AdminResourceCreatorContainer.getStores = () => ([UserStore, ResourceStore]);
AdminResourceCreatorContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(AdminResourceCreatorContainer);

/*
this.state.resource.dashboard.edit_resource ?
  <ResourceCreate user={this.state.user} resource={this.state.resource} />
:
*/
