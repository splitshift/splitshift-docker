import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import NewsletterIndex from '../../components/Backend/Newsletter/Index';

import UserStore from '../../stores/UserStore';
import NewsletterStore from '../../stores/NewsletterStore';
import ResourceStore from '../../stores/ResourceStore'

class NewsletterContainers extends Component {

  render() {
    return (
      typeof this.state.user.token !== 'undefined' ?
        <NewsletterIndex user={this.state.user} newsletter={this.state.newsletter} language={this.state.language} />
      : <div />
    );
  }

}

NewsletterContainers.getStores = () => ([UserStore, NewsletterStore]);
NewsletterContainers.calculateState = (prevState) => ({
  user: UserStore.getState(),
  newsletter: NewsletterStore.getState(),
  language: ResourceStore.getState().lang
});

export default Container.create(NewsletterContainers);
