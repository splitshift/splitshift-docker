import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { browserHistory } from 'react-router';

import AdminHeader from '../../components/Backend/Header/Admin';
import AdminSidebar from '../../components/Backend/AdminSidebar';

import AuthenticationActionCreators from '../../actions/AuthenticationActionCreators';

import UserStore from '../../stores/UserStore';
import ResourceStore from '../../stores/ResourceStore';
import SocialStore from '../../stores/SocialStore';

let owner_load = false;

class AdminAppContainer extends Component {

  componentDidMount() {
    if(localStorage.getItem('token')) {

      if(localStorage.getItem('type') !== 'Admin')
        browserHistory.push('/');

      AuthenticationActionCreators.loginAlready();
    }else
      browserHistory.push('/');

    this.state.resource.lang = this.props.params.language || this.state.resource.lang;
  }

  componentDidUpdate () {
    if (this.props.params.language !== this.state.resource.lang) {
      let url = this.props.location.pathname
      url = url.substr(3)
      browserHistory.push('/' + this.state.resource.lang + url)
    }
  }


  render() {
    let lang = this.props.params.language || this.state.resource.lang
    return (
      <div>
        <div id="main">
          <AdminHeader language={lang} />
          <div className="container-content">
            {React.cloneElement(this.props.children)}
          </div>
        </div>
        <AdminSidebar language={lang} user={this.state.user} />
      </div>
    );
  }
}

AdminAppContainer.getStores = () => ([UserStore, ResourceStore, SocialStore]);
AdminAppContainer.calculateState = (prevState) => ({
  resource: ResourceStore.getState(),
  user: UserStore.getState(),
  social: SocialStore.getState()
});


export default Container.create(AdminAppContainer);;
