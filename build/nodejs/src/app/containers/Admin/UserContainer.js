import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import AdminManagerActionCreators from '../../actions/AdminManagerActionCreators';

import AdminUserIndex from '../../components/Backend/User/Index';

import UserStore from '../../stores/UserStore';
import DataManagerStore from '../../stores/DataManagerStore';

class AdminUserContainer extends Component {


  render() {
    return (
      this.state.user.token ?
        <AdminUserIndex user={this.state.user} manager={this.state.manager} />
        : <div />
    );
  }

}

AdminUserContainer.getStores = () => ([UserStore, DataManagerStore]);
AdminUserContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  manager: DataManagerStore.getState()
});

export default Container.create(AdminUserContainer);
