import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import JobSearch from '../components/Frontend/JobSearch';

import SearchStore from '../stores/SearchStore';
import UserStore from '../stores/UserStore';
import JobStore from '../stores/JobStore';
import ResourceStore from '../stores/ResourceStore'

class JobSearchContainer extends Component {

  render() {
    return (
      <JobSearch search={this.state.search} user={this.state.user} job={this.state.job} language={this.state.resource.lang} />
    );
  }
}

JobSearchContainer.getStores = () => ([SearchStore, UserStore, JobStore, ResourceStore]);
JobSearchContainer.calculateState = (prevState) => ({
  search: SearchStore.getState(),
  user: UserStore.getState(),
  job: JobStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(JobSearchContainer);
