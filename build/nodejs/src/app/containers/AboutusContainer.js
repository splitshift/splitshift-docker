import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import Aboutus from '../components/Frontend/Aboutus';

import ResourceStore from '../stores/ResourceStore';
import configLang from '../tools/configLang';

let first_load = false;

class AboutusContainer extends Component {

  render() {
    return (
      <Aboutus aboutus={configLang[this.state.resource.lang].aboutus} />
    );
  }
}

AboutusContainer.getStores = () => ([ResourceStore]);
AboutusContainer.calculateState = (prevState) => ({
  resource: ResourceStore.getState()
});

export default Container.create(AboutusContainer);
