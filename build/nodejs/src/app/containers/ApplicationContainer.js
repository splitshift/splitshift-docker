import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import JobActionCreators from '../actions/JobActionCreators';

import Application from '../components/Backend/Application';

import UserStore from '../stores/UserStore';
import JobStore from '../stores/JobStore';
import ResourceStore from '../stores/ResourceStore'

let first_load = false;

class ApplicationContainer extends Component {

  componentDidMount() {
    first_load = false;
    this.FirstLoad();
  }

  componentDidUpdate() {
    this.FirstLoad();
  }

  FirstLoad() {
    if(!first_load && this.state.user.token) {
      JobActionCreators.getEmployeeApplyJob(this.state.user.token);
      first_load = true;
    }
  }

  render() {
    return (
      this.state.user.token ?
      <Application user={this.state.user} job={this.state.job} deleting={this.state.job.deleting} language={this.state.resource.lang} />
      : <div />
    );
  }
}

ApplicationContainer.getStores = () => ([UserStore, JobStore, ResourceStore]);
ApplicationContainer.calculateState = (prevState) => ({
  user: UserStore.getState(),
  job: JobStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(ApplicationContainer);
