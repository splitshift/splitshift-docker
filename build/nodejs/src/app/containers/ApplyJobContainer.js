import React, { Component } from 'react';
import {Container} from 'flux/utils';
import {browserHistory} from 'react-router';

import JobActionCreators from '../actions/JobActionCreators';

import ApplyJob from '../components/Backend/ApplyJob';

import ResourceStore from '../stores/ResourceStore';
import UserStore from '../stores/UserStore';
import JobStore from '../stores/JobStore';

let first_load = false;

class ApplyJobContainer extends Component {


  render() {
    return (
      this.state.user.token && (this.state.user.company_name || this.state.user.rank) ?
      <ApplyJob
        user={this.state.user}
        apply_jobs={this.state.job.apply_jobs}
        apply_job_filter={this.state.job.apply_job_filter}
        language={this.state.resource.lang}
        deleting={this.state.job.deleting} />
      : <div />
    );
  }
}

ApplyJobContainer.getStores = () => ([ResourceStore, UserStore, JobStore]);
ApplyJobContainer.calculateState = (prevState) => ({
  resource: ResourceStore.getState(),
  user: UserStore.getState(),
  job: JobStore.getState()
});

export default Container.create(ApplyJobContainer);
