import React, {Component} from 'react';
import {Container} from 'flux/utils';

import UserProfile from '../components/Frontend/UserProfile';

import UserStore from '../stores/UserStore';
import UserProfileStore from '../stores/UserProfileStore';
import ResourceStore from '../stores/ResourceStore';
import MessageStore from '../stores/MessageStore';
import FeedStore from '../stores/FeedStore';

class UserProfileContainer extends Component {

	render() {
		return (
			typeof this.props.params.key !== 'undefined' || typeof this.state.user.token !== 'undefined' ?
			<UserProfile
				user={this.state.user}
				profile={this.state.profile}
				msg={this.state.msg}
				status={this.state.status}
				lang={this.state.lang}
				feed_id={this.props.params.feed_id}
				profile_key={this.props.params.key}
				feed={this.state.feed}
				page={this.props.routes[this.props.routes.length - 1].name}
				editor={this.props.routes[this.props.routes.length - 1].name == "user_profile"} />
			: <div />
		);
	}

}

UserProfileContainer.getStores = () => ([UserStore, UserProfileStore, ResourceStore, MessageStore, FeedStore]);
UserProfileContainer.calculateState = (prevState) => ({
	user: UserStore.getState(),
	profile: UserProfileStore.getState(),
	status: ResourceStore.getState().status,
	lang: ResourceStore.getState().lang,
	msg: MessageStore.getState(),
	feed: FeedStore.getState()
});

export default Container.create(UserProfileContainer);
