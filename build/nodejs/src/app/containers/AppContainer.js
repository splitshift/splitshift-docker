import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { browserHistory } from 'react-router';

import MainHeader from '../components/Frontend/Header/Main';
import AdminHeader from '../components/Frontend/Header/Admin';
import UserHeader from '../components/Frontend/Header/User';
import Footer from '../components/Frontend/Footer';

import UserStore from '../stores/UserStore';
import LoginStore from '../stores/LoginStore';
import ResourceStore from '../stores/ResourceStore';
import SocialStore from '../stores/SocialStore';
import SearchStore from '../stores/SearchStore';
import MessageStore from '../stores/MessageStore';

import AuthenticationActionCreators from '../actions/AuthenticationActionCreators';

import JQueryTool from '../tools/JQueryTool';

const Pages = ['home','profile','company','user_public_profile','company_public_profile'];
const NoLangPage = ['/search'];

import configLang from '../tools/configLang'

let owner_load = false;

class AppContainer extends Component {

  getLanguage(lang) {
    let url = this.props.location.pathname;

    if(NoLangPage.indexOf(url) !== -1 || url.indexOf('/forgot/password') !== -1 || url.indexOf('/api/confirm/') !== -1)
      return '';

    url = url.substr(3);

    if(lang == 'th' || lang == 'en' || lang == '')
      browserHistory.push('/' + (lang !== "" ? lang : 'th') + url);
    else if(Pages.indexOf(lang) != -1)
      browserHistory.push('/th/' + lang);
    else
      browserHistory.push('/th/');

  }

  componentDidMount() {
    owner_load = false
    if(localStorage.getItem('token')) {
      AuthenticationActionCreators.loginAlready();
      if(localStorage.getItem('type') == 'Employee')
        AuthenticationActionCreators.loadProfile(localStorage.getItem('key'));
      else if(localStorage.getItem('type') != 'Admin')
        AuthenticationActionCreators.loadCompanyProfile(localStorage.getItem('key'));
    }
    else {
      AuthenticationActionCreators.guest();
    }

    this.getLanguage(this.props.params.language || this.state.resource.lang);
    this.state.resource.lang = this.props.params.language || this.state.resource.lang;
  }

  componentDidUpdate() {
    if(typeof this.props.params.language !== 'undefined' && this.props.params.language !== this.state.resource.lang)
      this.getLanguage(this.state.resource.lang)
  }


  render() {

    let currentPage = this.props.routes[this.props.routes.length - 1].name;
    let currentLang = this.props.params.language || this.state.resource.lang;
    let header = "";

    if(typeof this.state.user.token !== 'undefined' && this.state.user.token) {
      if(this.state.user.type == 'Admin')
        header = (<AdminHeader search={this.state.search}  currentPage={currentPage} language={currentLang} user={this.state.user} social={this.state.social} />);
      else
        header = (<UserHeader search={this.state.search} currentPage={currentPage} message={this.state.message} language={currentLang} user={this.state.user} social={this.state.social} />);
    }
    else
      header = (<MainHeader search={this.state.search} currentPage={currentPage} login={this.state.login} user={this.state.user} language={currentLang} />);

    return (
      <div>
        {header}
        <div style={{ minHeight: "1000px" }}>
          {React.cloneElement(this.props.children)}
        </div>
        <Footer user={this.state.user} language={currentLang} />
      </div>
    );
  }
}

AppContainer.getStores = () => ([UserStore, LoginStore, ResourceStore, SocialStore, SearchStore, MessageStore]);
AppContainer.calculateState = (prevState) => ({
  resource: ResourceStore.getState(),
  user: UserStore.getState(),
  login: LoginStore.getState(),
  social: SocialStore.getState(),
  search: SearchStore.getState(),
  message: MessageStore.getState()
});


export default Container.create(AppContainer);;
