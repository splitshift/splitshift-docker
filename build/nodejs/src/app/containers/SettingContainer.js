import React, {Component} from 'react';
import {Container} from 'flux/utils';

import UserStore from '../stores/UserStore';
import ResourceStore from '../stores/ResourceStore';

import Setting from '../components/Frontend/Setting';

class SettingContainer extends Component {
  render() {
    return (
      <Setting resource={this.state.resource} user={this.state.user} language={this.state.resource.lang} />
    );
  }
}

SettingContainer.getStores = () => ([UserStore, ResourceStore]);
SettingContainer.calculateState = (prevState) => ({
	user: UserStore.getState(),
  resource: ResourceStore.getState()
});

export default Container.create(SettingContainer);
