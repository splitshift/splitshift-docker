import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import { browserHistory } from 'react-router';
import { ReduceStore } from 'flux/utils';

class FeedStore extends ReduceStore {
	getInitialState() {
		return {
      feeds: [],
			photos: [],
			load_type: "",
			add_video: false,
			add_photo: false,
			uploading: false,
			deleting: false,
			buzz: false,
			edit_feed_id: null,
			edit_index: null,
			owner: null,
			load_more: false,
			comment_for_reply_id: null,
      draft: {
				photos: []
			}
    }
	}

	reduce(state, action) {

    let nextState = Object.assign({}, state);

		switch (action.type) {

			case constants.RESET_FEED:
				nextState.feeds = []
				return nextState

			case constants.SET_OWNER_FEED:
				let owner = action.payload.owner;
				owner.name = action.payload.owner.type === 'Employee' ? owner.first_name + ' ' + owner.last_name : owner.company_name;
				nextState.owner = owner;
				return nextState;

			case constants.EDIT_FEED_DRAFT:
				nextState.draft = Object.assign({}, state.draft, action.payload.response);
				return nextState;

			case constants.GET_FEED:
			case constants.GET_FEED_MORE:
				nextState.load_more = false;
				return nextState;

			case constants.GET_FEED_SUCCESS:
				nextState.feeds = action.payload.response;

				if(action.payload.response.length >= 10)
					nextState.load_more = true;

				return nextState;

			case constants.GET_FEED_MORE_SUCCESS:
				nextState.feeds = nextState.feeds.concat(action.payload.response);

				if(action.payload.response.length >= 10)
					nextState.load_more = true;

				return nextState;

			case constants.GET_BUZZ_FEED:
			case constants.GET_BUZZ_FEED_MORE:
				nextState.load_more = false;
				return nextState;

			case constants.GET_BUZZ_FEED_SUCCESS:
				nextState.feeds = action.payload.response;

				if(action.payload.response.length >= 10)
					nextState.load_more = true;

				nextState.buzz = true;
				return nextState;

			case constants.GET_BUZZ_FEED_MORE_SUCCESS:
				nextState.feeds = nextState.feeds.concat(action.payload.response);

				if(action.payload.response.length >= 10)
					nextState.load_more = true;

				return nextState;

			case constants.GET_PHOTO_FEED_SUCCESS:
				nextState.photos = action.payload.response.photos;
				return nextState;

			case constants.CHOOSE_EDIT_FEED:
				nextState.edit_feed_id = action.payload.feed._id;
				nextState.edit_index = action.payload.index;
				nextState.draft = action.payload.feed;

				if(action.payload.owner)
					nextState.draft.owner = action.payload.owner;

				if(action.payload.feed.video)
					nextState.add_video = true;
				else if(action.payload.feed.photos.length > 0)
					nextState.add_photo = true;

				return nextState;

			case constants.LIKE_FEED_SUCCESS:

				if(action.payload.response.message.indexOf("Like") != -1)
					nextState.feeds[nextState.draft.like].likes.push(nextState.draft.owner_key);
				else
					nextState.feeds[nextState.draft.like].likes.splice(nextState.feeds[nextState.draft.like].likes.indexOf(nextState.draft.owner_key), 1);

				nextState.draft = {
					photos: []
				};

				return nextState;

			case constants.ON_REPLY:
				nextState.comment_for_reply_id = action.payload._id;
				return nextState;

			case constants.DELETE_FEED_PHOTO:

				if(nextState.edit_feed_id) {
					if(typeof nextState.draft.deleted_photos == 'undefined')
						nextState.draft.deleted_photos = [];

					nextState.draft.deleted_photos.push(nextState.draft.photos[action.payload.image_key]);
				}

				nextState.draft.photos.splice(action.payload.image_key, 1);
				return nextState;

			case constants.DELETE_COMMENT_FEED:
			case constants.DELETE_FEED:
			case constants.DELETE_REPLY_COMMENT_FEED:
				nextState.deleting = true;
				return nextState;

			case constants.DELETE_COMMENT_FEED_SUCCESS:
			case constants.DELETE_FEED_SUCCESS:
			case constants.DELETE_REPLY_COMMENT_FEED_SUCCESS:
				nextState.deleting = false;
				return nextState;

			case constants.EDIT_FEED_IMAGE:
				nextState.add_photo = !nextState.add_photo;
				nextState.add_video = false;

				return nextState;

			case constants.EDIT_FEED_VIDEO:
				nextState.add_video = !nextState.add_video;
				nextState.add_photo = false;

				if(!nextState.add_video && typeof nextState.draft.video !== 'undefined')
					nextState.draft.video = "";

				return nextState;

			case constants.UPLOAD_FEED_PHOTO:
				nextState.uploading = true;
				if(typeof nextState.draft.photos === 'undefined')
					nextState.draft.photos = [];

				if(nextState.edit_feed_id && typeof nextState.draft.new_photos === 'undefined')
					nextState.draft.new_photos = [];

				nextState.draft.photos.push('public/images/loading.svg')

				return nextState;

			case constants.UPLOAD_FEED_PHOTO_SUCCESS:

				nextState.add_video = false;

				if(nextState.edit_feed_id)
					nextState.draft.new_photos.push(action.payload.response.path);

				nextState.draft.photos.pop()
				nextState.draft.photos.push(action.payload.response.path);
				nextState.uploading = false;

				return nextState;

			case constants.GET_FEED_FAIL:
				nextState.feeds = [];
				return nextState;


			case constants.DELETE_COMMENT_FEED_FAIL:
			case constants.DELETE_FEED_FAIL:
			case constants.DELETE_REPLY_COMMENT_FEED_FAIL:
				nextState.deleting = false;
			case constants.POST_FEED_FAIL:
			case constants.POST_COMMENT_FEED_FAIL:
			case constants.POST_REPLY_COMMENT_FEED_FAIL:
				return nextState;

			case constants.POST_FEED_SUCCESS:
				let response = action.payload.response;

				response.owner = nextState.draft.owner;

				if(typeof nextState.feeds !== 'undefined')
					nextState.feeds.unshift(response);

				nextState.draft = {
					photos: []
				}

				nextState.add_video = false;
				nextState.add_photo = false;

				return nextState;

			case constants.POST_COMMENT_FEED_SUCCESS:
				let comment = action.payload.response;
				comment.owner = nextState.owner;
				comment.replies = [];
				nextState.feeds[nextState.draft.feed_index].comments.push(comment);

				return nextState;

			case constants.POST_REPLY_COMMENT_FEED_SUCCESS:
				nextState.feeds[nextState.draft.feed_index].comments[nextState.draft.comment_index].replies.push(action.payload.response);
				nextState.comment_for_reply_id = null;
				return nextState;


			case constants.SEARCH_BUZZ_FEED_SUCCESS:
				nextState.feeds = action.payload.response;
				return nextState;

			case constants.EDIT_POST_FEED_SUCCESS:
				nextState.draft.new_photos = [];
				nextState.feeds[nextState.edit_index] = nextState.draft;

			case constants.CANCEL_EDIT_FEED:
				nextState.edit_feed_id = null;
				nextState.edit_index = null;
				nextState.draft = {
					photos: []
				}
				nextState.add_video = false;
				nextState.add_photo = false;

			default:
				return nextState;

		}

	}
}
export default new FeedStore(AppDispatcher);
