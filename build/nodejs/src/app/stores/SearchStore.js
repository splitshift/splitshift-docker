import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import {ReduceStore} from 'flux/utils';

let counter = 0;

class SearchStore extends ReduceStore {
	getInitialState() {
		return {
			filter: {},
			companies: [],
			people: [],
			jobs: [],
			job_index: null,
			load_more: false,
			searching: false
		};
	}
	reduce(state, action) {

		let nextState = Object.assign({}, state);

    switch(action.type) {

			case constants.SEARCH_PEOPLE:
			case constants.SEARCH_PEOPLE_MORE:
				nextState.searching = true;
				nextState.load_more = false;
				return nextState;

			case constants.SEARCH_COMPANY:
			case constants.SEARCH_COMPANY_MORE:
				nextState.searching = true;
				nextState.load_more = false;
				return nextState;

			case constants.SEARCH_JOB:
			case constants.SEARCH_JOB_MORE:
				nextState.searching = true;
				nextState.load_more = false;
				return nextState;

			case constants.SEARCH_JOB_SUCCESS:
				nextState.jobs = action.payload.response;

				if(action.payload.response.length >= 10)
					nextState.load_more = true;

				nextState.searching = false;
				return nextState;

			case constants.SEARCH_JOB_MORE_SUCCESS:
				nextState.jobs = nextState.jobs.concat(action.payload.response);

				if(action.payload.response.length >= 10)
					nextState.load_more = true;

				nextState.searching = false;
				return nextState;

			case constants.SEARCH_PEOPLE_SUCCESS:
				nextState.people = action.payload.response;
				if(action.payload.response.length >= 15)
					nextState.load_more = true;

				return nextState;

			case constants.SEARCH_PEOPLE_MORE_SUCCESS:
				nextState.people = nextState.people.concat(action.payload.response);
				if(action.payload.response.length >= 15)
					nextState.load_more = true;

				return nextState;

			case constants.SEARCH_COMPANY_SUCCESS:
				nextState.companies = action.payload.response;
				if(action.payload.response.length >= 10)
					nextState.load_more = true;
				nextState.searching = false;
				return nextState;

			case constants.SEARCH_COMPANY_MORE_SUCCESS:
				nextState.companies = nextState.companies.concat(action.payload.response);
				if(action.payload.response.length >= 10)
					nextState.load_more = true;
				nextState.searching = false;
				return nextState;

			case constants.CHOOSE_JOB_SEARCH:
				nextState.job_index = action.payload.job_index;
				return nextState;

			case constants.SEARCH_ALL_SUCCESS:
				nextState = Object.assign({}, nextState, action.payload.response);
				nextState.job_index = null;
				return nextState;

			case constants.UPDATE_FILTER:
				nextState.filter = Object.assign({}, nextState.filter, action.payload.response);
				return nextState;

			case constants.CLEAR_FILTER:
				nextState.filter = {}
				nextState.job_index = null;
				return nextState;

      default:
  			return state;
    }

	}
}
export default new SearchStore(AppDispatcher);
