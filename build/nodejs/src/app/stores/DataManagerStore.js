import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import { ReduceStore } from 'flux/utils';

class DataManagerStore extends ReduceStore {

	getInitialState() {
		return {
			users: [],
			temp_users: [],
			loading: false
		};
	}

	reduce(state, action) {

		let nextState = Object.assign({}, state);

    switch(action.type) {

			case constants.GET_ADMIN_ALL_USER_SUCCESS:
				nextState.users = action.payload.response;
				nextState.temp_users = action.payload.response;
				return nextState;

			case constants.SEARCH_ADMIN_USER:
				nextState.users = state.temp_users.filter((user) => user.email.indexOf(action.payload.keyword) > -1 || user.name.indexOf(action.payload.keyword) > -1)
				return nextState;

			case constants.DELETE_USER:
			case constants.POST_APPROVE_USER_TO_RESOURCE:
				nextState.loading = true;
				return nextState;

			case constants.DELETE_USER_SUCCESS:
			case constants.POST_APPROVE_USER_TO_RESOURCE_SUCCESS:
				nextState.loading = false;
				return nextState;

			case constants.GET_ADMIN_ALL_USER_FAIL:
			case constants.POST_APPROVE_USER_TO_RESOURCE_FAIL:
			case constants.DELETE_USER_FAIL:
				alert(action.payload.error);
				console.error(action.payload.error);
				return nextState;

      default:
        return nextState;
    }

	}
}
export default new DataManagerStore(AppDispatcher);
