import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import {ReduceStore} from 'flux/utils';
import {browserHistory} from 'react-router';

let counter = 0;

class SocialStore extends ReduceStore {
	getInitialState() {
		return {
      actions: [],
      messages: [],
      friends: [],
			friend_index: null,
			seen_notification: false,
			deleting: false
    };
	}
	reduce(state, action) {

		let nextState = Object.assign({}, state);

		switch (action.type) {
      case constants.GET_REQUEST_PEOPLE_SUCCESS:
        nextState.friends = action.payload.response;
        return nextState;

			case constants.SET_SOCIAL_DRAFT:
				nextState = Object.assign({}, nextState, action.payload.response);
				return nextState;

			case constants.GET_NOTIFICATION_SUCCESS:
				nextState.actions = action.payload.response;
				return nextState;

			case constants.REQUEST_PEOPLE_SUCCESS:
				nextState.friends.splice(nextState.friend_index);
				nextState.friend_index = null;
				return nextState;

			case constants.GET_ALL_MESSAGE_SUCCESS:
				console.log("All Message");
				nextState.messages = action.payload.response;
				return nextState;

			case constants.SEEN_NOTIFICATION_SUCCESS:
				nextState.actions = Object.assign([], state.actions);
				for (let i = 0 ; i < nextState.actions.length ; i++) {
					nextState.actions[i] = Object.assign({}, state.actions[i], {seen: true});
				}
				nextState.seen_notification = true;
				return nextState;

			case constants.DELETE_NOTIFICATION:
				nextState.deleting = true;
				return nextState;

			case constants.DELETE_NOTIFICATION_SUCCESS:
				nextState.deleting = false;
				return nextState;

			case constants.GET_MESSAGE_SUCCESS:

				nextState.messages = Object.assign([], state.messages);

				for(let i = 0 ; i < nextState.messages.length ; i++) {

					if(nextState.messages[i]._id === action.payload.response._id)
						nextState.messages[i] = Object.assign({}, state.messages[i], {seen: action.payload.response.seen});

				}

				return nextState;

			case constants.SEEN_NOTIFICATION_FAIL:
			case constants.GET_ALL_MESSAGE_FAIL:
				console.error(action.payload.error);
				return nextState;

			default:
				return state;
		}
	}
}


export default new SocialStore(AppDispatcher);
