import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import { ReduceStore } from 'flux/utils';
import { browserHistory } from 'react-router';

let counter = 0;

const browserStorage = (typeof localStorage === 'undefined') ? new require('node-localstorage').LocalStorage('./scratch') : localStorage;

class UserStore extends ReduceStore {
	getInitialState() {
		return {
			token: browserStorage.getItem('token') || undefined,
			key: browserStorage.getItem('key') || undefined,
			type: browserStorage.getItem('type') || undefined,
			complete: false,
			setting: {
				old_password: "",
				new_password: "",
				new_password_again: "",
				new_primary_email: ""
			}
		};
	}
	reduce(state, action) {

		let nextState = Object.assign({}, state);

		switch (action.type) {
			case constants.REGISTER_UPDATE:
				console.log("Register Update");
				return action.payload.info;
			case constants.REGISTER_FAIL:
				alert(action.payload.error);
				return state;
			case constants.REGISTER_SUCCESS:
				window.location.href = '/th';
				return action.payload.response;

			case constants.LOGIN_ALREADY:
				let response = action.payload.response;

				if(response.type === 'Admin') {
					response.profile_image = '/images/profiles/admin.jpg';
					response.company_name = 'Splitshift';
					response.complete = true;
				}

				return  Object.assign({}, nextState, response);

			case constants.UPLOAD_PROFILE_IMAGE_SUCCESS:
				nextState.profile_image = action.payload.response.path + "?new=" + (++counter);
				return nextState;

			case constants.GET_USER_OWNER_PROFILE_SUCCESS:
				nextState = Object.assign({}, nextState, action.payload.response);
				nextState.complete = true;
				return nextState;

			case constants.GET_USER_PROFILE_FAIL:
			case constants.UPDATE_PROFILE_ERROR:
			case constants.GET_COMPANY_PROFILE_FAIL:
			case constants.UPDATE_COMPANY_PROFILE_FAIL:
			case constants.POST_FEED_FAIL:
			case constants.POST_COMMENT_FEED_FAIL:
			case constants.GET_FEED_FAIL:
			case constants.GET_ALL_MESSAGE_FAIL:
			case constants.GET_REQUEST_PEOPLE_FAIL:
			case constants.GET_NOTIFICATION_FAIL:

				if(action.payload.error.toString().indexOf("Token") < 0 && action.payload.error.toString().indexOf("User not found") < 0)
					return state;

			case constants.LOGOUT:
				localStorage.removeItem('type');
				localStorage.removeItem('token');
				localStorage.removeItem('key');
				return {};

			case constants.CHANGE_SETTING_DRAFT:
				nextState.setting = Object.assign({}, state.setting, action.payload.response);
				return nextState;


			case constants.CHANGE_PRIMARY_EMAIL_SUCCESS:

				alert("Change Primary Email Successful");

				nextState.email = nextState.setting.new_primary_email;
				nextState.setting = {
					old_password: "",
					new_password: "",
					new_password_again: "",
					new_primary_email: ""
				};

				return nextState;


			case constants.CHANGE_PASSWORD_SUCCESS:
				alert("Change Password Successful");

				nextState.setting = {
					old_password: "",
					new_password: "",
					new_password_again: "",
					new_primary_email: ""
				};

				return nextState;

			case constants.CHANGE_PASSWORD_FAIL:
			case constants.CHANGE_PRIMARY_EMAIL_FAIL:
			case constants.DELETE_ACCOUNT_FAIL:
				alert(action.payload.error);
				console.error(action.payload.error);

				return nextState;

			case constants.DELETE_ACCOUNT_SUCCESS:
				alert("Your account has been remove from splitshift.");

				localStorage.removeItem('token');
				localStorage.removeItem('type');
				localStorage.removeItem('email');

				window.location.href = '/th/';

				return state;

			default:
				return state;
		}
	}
}
export default new UserStore(AppDispatcher);
