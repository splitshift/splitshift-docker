import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import {ReduceStore} from 'flux/utils';

let counter = 0;

const initialProfile = {
	culture: "",
	video: "",
	overview: "",
	location: [0, 0],
	links: {},
	loading: false,
	key: ""
};

class CompanyProfileStore extends ReduceStore {
	getInitialState() {
		return {
			draft: {},
			reviewing: false
		};
	}

	reduce(state, action) {
		let nextState = Object.assign({}, state);

		switch (action.type) {
			case constants.LOGOUT:
				return {};

			case constants.RESET_COMPANY_PROFILE:
				return Object.assign({}, nextState, initialProfile);

			case constants.GET_COMPANY_PROFILE:
				// nextState.key = "";
				delete nextState.following;
				return Object.assign({}, nextState, {loading: true});

			case constants.REVIEW_COMPANY:
				nextState.reviewing = true;
				return nextState;

			case constants.GET_COMPANY_PROFILE_SUCCESS:
				return Object.assign({}, nextState, initialProfile, action.payload.response, {loading: false});

			case constants.GET_COMPANY_PROFILE_FAIL:
			case constants.UPDATE_COMPANY_PROFILE_FAIL:
			case constants.REVIEW_COMPANY_FAIL:
				console.error(action.payload.error);
				alert(action.payload.error);
				return state;

			case constants.REVIEW_COMPANY_SUCCESS:
				nextState.reviewing = false;

			case constants.UPDATE_COMPANY_PROFILE_SUCCESS:
				nextState.draft = {};
				return nextState;

			case constants.UPLOAD_PROFILE_IMAGE:
				nextState.profile_image = '/images/loading.svg'
				return nextState

			case constants.UPLOAD_PROFILE_IMAGE_SUCCESS:
				nextState.profile_image = action.payload.response.path + "?new=" + (++counter);
				return nextState

			case constants.FOLLOW_PEOPLE_SUCCESS:
				nextState.following = !nextState.following;
				return nextState;

			case constants.UPLOAD_BANNER_IMAGE:
				nextState.banner = '/images/loading.svg'
				return nextState

			case constants.UPLOAD_BANNER_IMAGE_SUCCESS:
				nextState.banner = action.payload.response.path + "?new=" + (++counter);
				return nextState;

			case constants.CHANGE_COMPANY_BANNER_IMAGE_WITH_TEMPLATE_SUCCESS:
				nextState.banner = state.draft.template_banner;
				nextState.draft = {};
				return nextState;

			case constants.EDIT_DRAFT:
				nextState.draft = Object.assign({}, state.draft, action.payload.response);
				return nextState;

			default:
				return state;
		}
	}

}
export default new CompanyProfileStore(AppDispatcher);
