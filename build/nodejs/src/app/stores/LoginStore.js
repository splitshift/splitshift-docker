import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import { browserHistory } from 'react-router';
import { ReduceStore } from 'flux/utils';

class LoginStore extends ReduceStore {
	getInitialState() {
		return {
			email: '',
			password: '',
			error: false,
			forget: false
		};
	}
	reduce(state, action) {
		let nextState = Object.assign({}, state);

		switch (action.type) {
			case constants.UPDATE_LOGIN:
				return action.payload.info;

			case constants.LOGIN:
				nextState.error = false;
				nextState.forget = false;
				return nextState;

			case constants.LOGIN_SUCCESS:
				// HIDE Login Modal
				// $("#myModal__login").modal('hide');

				localStorage.setItem('type', action.payload.response.type);
				localStorage.setItem('token', action.payload.response.token);
				localStorage.setItem('key', action.payload.response.key);
				state.error = false;
				return {'email': '', 'password': ''};

			case constants.LOGIN_ALREADY:
				return {'email': '', 'password': ''};

			case constants.LOGIN_FAIL:
			case constants.FORGET_PASSWORD_FAIL:
				alert(action.payload.error);
				nextState.error = true;
				return nextState;

			case constants.FORGET_PASSWORD:
			case constants.CHANGE_FORGET_PASSWORD:
				nextState.forget = false;
				return nextState;

			case constants.FORGET_PASSWORD_SUCCESS:
				alert('Send link for change your password to your email')
				nextState.forget = true;
				return nextState;

			case constants.CHANGE_FORGET_PASSWORD_SUCCESS:
				alert('Change new password complete!')
				nextState.forget = true;
				return nextState;

			default:
				return state;
		}
	}
}
export default new LoginStore(AppDispatcher);
