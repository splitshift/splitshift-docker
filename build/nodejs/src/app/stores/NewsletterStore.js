import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import {ReduceStore} from 'flux/utils';

let counter = 0;

class NewsletterStore extends ReduceStore {

	getInitialState() {
		return {
		  list: []
		};
	}

	reduce(state, action) {

		let nextState = Object.assign({}, state);

    switch(action.type) {

      case constants.GET_NEWSLETTER_SUCCESS:
        nextState.list = action.payload.response;
        return nextState;

			case constants.SEND_EMAIL_SUCCESS:
				alert("Send Email Successful")
				return nextState;

      default:
  			return state;
    }

	}
}
export default new NewsletterStore(AppDispatcher);
