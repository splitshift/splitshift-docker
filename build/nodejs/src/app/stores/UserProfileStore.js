import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import {ReduceStore} from 'flux/utils';

let counter = 0;
let initialState = {
	draft: {},
	company_list: [],
	followers: [],
	connections: [],
	key: "",
	loading: false
};

const resetState = {
	banner: "",
	profile_image: "",
	loading: true,
	address: {},
	courses: [],
	educations: [],
	experiences: [],
	interests: [],
	languages: [],
	first_name: "",
	last_name: "",
	about: undefined,
	occupation: {},
	key: ""
}

class UserProfileStore extends ReduceStore {
	getInitialState() {
		return initialState;
	}

	reduce(state, action) {

		let nextState = Object.assign({}, state);

		switch (action.type) {
			case constants.LOGOUT:
				return initialState;
			case constants.RESET_USER_PROFILE:
			case constants.GET_USER_PROFILE:
				return Object.assign({}, nextState, resetState)
			case constants.GET_USER_PROFILE_SUCCESS:
				return Object.assign({}, state, {about:""}, action.payload.response, {loading: false});
			case constants.GET_USER_PROFILE_FAIL:
				console.log(action.payload.error);
				return state;
			case constants.UPDATE_PROFILE:
				return Object.assign({}, state, action.payload.response);
			case constants.EDIT_DRAFT:
				nextState.draft = Object.assign({}, state.draft, action.payload.response);
				return nextState;

			case constants.UPDATE_PROFILE_SUCCESS:
			case constants.CANCEL_PROFILE_EDIT:
				nextState.draft = {};
				return nextState;

			case constants.UPLOAD_PROFILE_IMAGE:
				nextState.profile_image = '/images/loading.svg'
				return nextState

			case constants.UPLOAD_PROFILE_IMAGE_SUCCESS:
				nextState.profile_image = action.payload.response.path + "?new=" + (++counter);
				return nextState

			case constants.UPLOAD_BANNER_IMAGE:
				nextState.banner = '/images/loading.svg'
				return nextState

			case constants.UPLOAD_BANNER_IMAGE_SUCCESS:
				nextState.banner = action.payload.response.path + "?new=" + (++counter);
				return nextState
			case constants.REQUEST_PEOPLE_FAIL:
			case constants.UPDATE_PROFILE_ERROR:
				console.log(action.payload.error);
				return state;
			case constants.FOLLOW_PEOPLE_SUCCESS:
				if(action.payload.response.message.indexOf("Unfollowing") != -1)
					nextState.following = true;
				else
					nextState.following = false;
				return nextState;
			case constants.REQUEST_PEOPLE_SUCCESS:
				console.log("Send Request Successful.");
				console.log(action.payload.response);
				nextState.connected = "requesting";
				return nextState;

			case constants.CHANGE_USER_BANNER_IMAGE_WITH_TEMPLATE_SUCCESS:
				nextState.banner = state.draft.template_banner;
				nextState.draft = {};
				return nextState;

			case constants.LOAD_ALL_COMPANY_SUCCESS:
				nextState.company_list = action.payload.response;
				return nextState;

			case constants.GET_FOLLOWER_SUCCESS:
				nextState.followers = action.payload.response;
				return nextState;

			case constants.GET_CONNECTIONS_SUCCESS:
				nextState.connections = action.payload.response;
				return nextState;

			default:
				return state;
		}
	}
}
export default new UserProfileStore(AppDispatcher);
