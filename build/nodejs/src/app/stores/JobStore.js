import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import {ReduceStore} from 'flux/utils';
import {browserHistory} from 'react-router';

let counter = 0;

class JobStore extends ReduceStore {
	getInitialState() {
		return {
      draft: {},
			apply_draft: {},
			apply_jobs: [],
			full_apply_jobs: [],
			apply_job_filter: "",
			employee_apply_jobs: [],
      jobs: [],
			job_index: null,
			edit_job_id: undefined,
			updating: false,
			deleting: false,
			sending_forward_email: false
    };
	}
	reduce(state, action) {

		let nextState = Object.assign({}, state);

		switch (action.type) {

			case constants.GET_ALL_JOB_SUCCESS:
				nextState.jobs = action.payload.response;
				return nextState;

			case constants.GET_APPLY_JOB_SUCCESS:
				nextState.apply_jobs = action.payload.response;
				nextState.full_apply_jobs = action.payload.response;
				nextState.deleting = false;
				return nextState;

			case constants.SEARCH_APPLY_JOB:
				nextState.apply_jobs = nextState.full_apply_jobs.filter((apply_job) => apply_job.from.name.indexOf(action.payload) !== -1 || apply_job.email.indexOf(action.payload) !== -1)
				return nextState;

			case constants.GET_EMPLOYEE_APPLY_JOB_SUCCESS:
				nextState.employee_apply_jobs = action.payload.response;
				return nextState;

			case constants.GET_JOB:
				nextState.edit_job_id = undefined;
				return nextState;

			case constants.GET_JOB_SUCCESS:
				nextState.edit_job_id = action.payload.response._id;
				nextState.draft = Object.assign({}, state.draft, action.payload.response);
				nextState.draft.old_expire_date = nextState.draft.expire_date;
				return nextState;


			case constants.DELETE_APPLY_JOB:
				nextState.deleting = true;
				return nextState;

			case constants.DELETE_APPLY_JOB_SUCCESS:
				nextState.deleting = false;
				return nextState;

			case constants.CHANGE_APPLY_JOB_FILTER:
				nextState.apply_job_filter = action.payload.response;
				return nextState;

      case constants.UPDATE_JOB_DRAFT:
        nextState.draft = Object.assign({}, nextState.draft, action.payload.response);
        return nextState;

      case constants.UPDATE_JOB_SUCCESS:
			case constants.UPDATE_ACTIVATE_JOB_SUCCESS:
        nextState.draft = {};
				nextState.updating = false;
        return nextState;

			case constants.RESET_JOB_DRAFT:
				nextState.draft = {};
				nextState.draft.expire_date = new Date((new Date()).getTime() + 90*24*60*60*1000);
				nextState.draft.old_expire_date = nextState.draft.expire_date;
				return nextState;

			case constants.CHOOSE_JOB_COMPANY:
				nextState.job_index = action.payload.response;
				return nextState;

			case constants.UPDATE_APPLY_JOB_DRAFT:
				nextState.apply_draft = Object.assign({}, nextState.apply_draft, action.payload.response);
				return nextState;

			case constants.UPLOAD_APPLY_FILE_SUCCESS:
				if(typeof nextState.apply_draft.files === 'undefined')
					nextState.apply_draft.files = [];

				nextState.apply_draft.files.push(action.payload.response);

				return nextState;

			case constants.POST_APPLY_JOB_SUCCESS:
				$("#myModal__positions").modal('hide');
				alert("Apply to this position complete!");
				return nextState;

			case constants.SEND_FORWARD_EMAIL:
				nextState.sending_forward_email = true;
				return nextState;

			case constants.SEND_FORWARD_EMAIL_SUCCESS:
				nextState.sending_forward_email = false;
				alert("Send Forward Email Successful")
				return nextState;

			case constants.CHANGE_APPLY_JOB_STATUS_SUCCESS:
				alert("Change Apply Job Status Complete!");
				return nextState;

			case constants.UPDATE_ACTIVATE_JOB:
			case constants.POST_NEW_JOB:
			case constants.UPDATE_JOB:
			case constants.DELETE_JOB:
				nextState.updating = true;
				return nextState;

			case constants.UPDATE_JOB_FAIL:
			case constants.UPLOAD_APPLY_FILE_FAIL:
			case constants.POST_APPLY_JOB_FAIL:
			case constants.GET_APPLY_JOB_FAIL:
			case constants.DELETE_APPLY_JOB_FAIL:
			case constants.SEND_FORWARD_EMAIL_FAIL:
				alert(action.payload.error);
	      console.error(action.payload.error);
			default:
				return state;
		}
	}
}

export default new JobStore(AppDispatcher);
