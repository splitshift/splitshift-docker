import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import { ReduceStore } from 'flux/utils';

const statusObj = {
  edit_profile_card: false,
  edit_about: false,
  edit_interest: false,
  edit_experience: false,
  edit_skill: false,
  edit_education: false,
  edit_course: false,
  edit_language: false,
  edit_company_profile_card: false,
  edit_company_overview: false,
  edit_company_culture: false,
  edit_company_location: false,
  edit_company_video: false,
  edit_company_benefit: false,
  edit_company_social_link: false,
  edit_datepicker: false,
  deleting: false
};

const dashboardObj = {
  edit_job: false,
  edit_resource: false
};

const initialStat = {
  feed: {
    all: 0,
    public: 0,
    company: {
      all: 0,
      employee: 0,
      public: 4,
      timeline: 0
    },
    user: {
      all: 0,
      connected: 0,
      employer: 0,
      onlyme: 0,
      public: 0,
      timeline: 0
    }
  },
  resource: {
    activated: 0,
    all: 0,
    deactivated: 0,
    approved: {
      all: 0,
      company: 0,
      user: 0
    }
  },
  connections: [],
  message: 0,
  subscribe: 0,
  hr: 0,
  job: {
    all: 0,
    activated: 0,
    deactivated: 0
  },
  applyjob: {
    all: 0,
    declined: 0,
    open: 0
  },
  companies: 0,
  jobs: 0,
  members: 0,
  people: 0
};

class ResourceStore extends ReduceStore {
  getInitialState() {
    return {
      lang: "th",
      status: statusObj,
      filter: {},
      first_load: false,
      owner_load: false,
      dashboard: dashboardObj,
      resources: undefined,
      filtered_resource: undefined,
      department_filter: "",
      resource_array: [],
      resource: {},
      resource_index: null,
      draft: {},
      stat: initialStat,
      editing: false,
      load: false,
      sending_forward_email: false
    };
  }

  reduce(state, action) {

    let nextState = Object.assign({}, state);

    switch (action.type) {

      case constants.UPDATE_JOB_SUCCESS:
        nextState.dashboard = dashboardObj;
        return nextState;

      case constants.SET_LANGUAGE:
        nextState.lang = action.payload.response.language || "th";
        return nextState;

      case constants.GET_USER_PROFILE:
        nextState.first_load = true;
        return nextState;

      case constants.GET_CONNECTIONS_SUCCESS:
        nextState.stat = Object.assign({}, nextState.stat, { connections: action.payload.response });
        return nextState;

      case constants.DELETE_EXPERIENCE_PROFILE:
      case constants.DELETE_EDUCATION_PROFILE:
        nextState.status = Object.assign({}, state.status, {deleting: true});
        return nextState;

      case constants.EDIT_PROFILE_CARD:
      case constants.CANCEL_PROFILE_EDIT:
        nextState.status = Object.assign({}, state.status, action.payload.response);
        return nextState;

      case constants.UPDATE_PROFILE_SUCCESS:
        if(nextState.status.edit_about || nextState.status.edit_experience || nextState.status.edit_education || nextState.status.edit_course || nextState.status.edit_language)
          alert(state.lang === 'th' ? 'สร้างโปรไฟล์ เสร็จสมบูรณ์!' : "Save Your Profile Successful!");
        nextState.status = statusObj;
        return nextState;

      case constants.UPDATE_COMPANY_PROFILE_SUCCESS:
        if(nextState.status.edit_company_culture || nextState.status.edit_company_overview || nextState.status.edit_company_video || nextState.status.edit_company_location || nextState.status.edit_company_social_link)
          alert(state.lang === 'th' ? 'สร้างโปรไฟล์ เสร็จสมบูรณ์!' : "Save Your Profile Successful!");
        nextState.status = statusObj;
        return nextState;

      case constants.GET_USER_OWNER_PROFILE:
        nextState.owner_load = true;
        return nextState;

      case constants.SET_ADMIN_EDITOR:
        nextState.dashboard = Object.assign({}, state.dashboard, action.payload.response);

        if(!nextState.dashboard.edit_resource) {
          nextState.resource = {};
          nextState.draft = {};
        }

        return nextState;


      case constants.RESET_RESOURCE:
        nextState.resources = undefined
        nextState.resource = {}
        return nextState;

      case constants.GET_ALL_RESOURCE_SUCCESS:
        nextState.resources = action.payload.response;
        return nextState;

      case constants.GET_RESOURCE_SINGLE_SUCCESS:
        nextState.resource = action.payload.response;
        nextState.load = false;
        return nextState;

      case constants.UPLOAD_FEATURED_IMAGE_SUCCESS:
        nextState.draft.featured_image = action.payload.response.path;
        return nextState;

      case constants.EDIT_RESOURCE_DRAFT:
        nextState.draft = Object.assign({}, state.draft, action.payload.response);
        return nextState;

      case constants.EDIT_FILTER_RESOURCE_SEARCH:
        nextState.filter = Object.assign({}, state.filter, action.payload.response);
        return nextState;

      case constants.ADD_RESOURCE:
      case constants.EDIT_RESOURCE:
      case constants.DELETE_RESOURCE:
      case constants.POST_APPROVE_RESOURCE:
        nextState.editing = true;
        return nextState;

      case constants.SEARCH_RESOURCE_SUCCESS:
      case constants.GET_RESOURCE_ADMIN_SUCCESS:
      case constants.GET_RESOURCE_OTHER_SUCCESS:
        nextState.resource_array = action.payload.response;
        return nextState;

      case constants.COMMENT_RESOURCE_SUCCESS:
      case constants.LIKE_RESOURCE_SUCCESS:
      case constants.DELETE_COMMENT_RESOURCE_SUCCESS:
        nextState.load = true;
      case constants.MANAGE_RESOURCE_SUCCESS:
      case constants.POST_APPROVE_RESOURCE_SUCCESS:
        nextState.draft = {};
        nextState.editing = false;
        return nextState;

      case constants.MANAGE_RESOURCE_FAIL:
      case constants.DELETE_COMMENT_RESOURCE_FAIL:
      case constants.COMMENT_RESOURCE_FAIL:
      case constants.GET_RESOURCE_ADMIN_FAIL:
        console.error(action.payload.error);
        alert(action.payload.error);
        return nextState;

      case constants.GET_STAT_SUCCESS:
        nextState.stat = Object.assign({}, state.stat, action.payload.response);
        return nextState;

      case constants.GET_USER_DASHBOARD:
      case constants.GET_ADMIN_DASHBOARD:
        nextState.stat = state.stat
        return nextState

      case constants.GET_USER_DASHBOARD_SUCCESS:
      case constants.GET_ADMIN_DASHBOARD_SUCCESS:
        nextState.stat = Object.assign({}, state.stat, action.payload.response);
        return nextState;

      case constants.SEND_RESOURCE_FORWARD_EMAIL:
        nextState.sending_forward_email = true;
        return nextState;

      case constants.SEND_RESOURCE_FORWARD_EMAIL_SUCCESS:
        alert("Send Forward Email Successful")
        nextState.sending_forward_email = false;
        return nextState;

      case constants.SEND_NEWSLETTER_SUCCESS:
        alert("Thank you for subscribing");
        return nextState;

      case constants.REGISTER_SUCCESS:
        if (state.lang === 'th') {
          alert("การลงทะเบียนเสร็จสิ้น กรุณายืนยันอีเมล์ของคุณเพื่อใช้งานต่อไป");
        } else {
          alert("Register Complete! Please confirm your email to use splitshift.");
        }
        return nextState;

      case constants.LOGIN_SUCCESS:
        if(action.payload.response.type === 'Admin')
          window.location.href = '/' + state.lang + '/admin';
        else if(action.payload.response.type === 'Employee')
          window.location.href = '/' + state.lang + '/user/' + action.payload.response.key;
        else
          window.location.href = '/' + state.lang + '/company/' + action.payload.response.key;
        return nextState

      case constants.LOGOUT:
        if(typeof action.payload.token !== 'undefined')
          window.location.href = '/' + state.lang
        return nextState

      case constants.SEND_NEWSLETTER_FAIL:
      case constants.GET_USER_DASHBOARD_FAIL:
        console.error(action.payload.error);
        alert(action.payload.error);
        return nextState;

      case constants.FILTER_RESOURCE:
        let filteredResource = Object.assign({}, nextState.resources);
        Object.keys(filteredResource).map((resource_cat) => {
          filteredResource[resource_cat] = filteredResource[resource_cat].filter((resource) => resource.department === action.payload.department);
        })
        nextState.filtered_resource = filteredResource;
        nextState.department_filter = action.payload.department;
        return nextState;

      default:
        return state;
    }
  }
}

export default new ResourceStore(AppDispatcher);
