import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import { ReduceStore } from 'flux/utils';

class HRStore extends ReduceStore {
	getInitialState() {
		return {
      users: [],
			editing: false
    }
	}

	reduce(state, action) {

    let nextState = Object.assign({}, state);

		switch (action.type) {

      case constants.GET_EMPLOYEE_IN_COMPANY_SUCCESS:
        nextState.users = action.payload.response;
				nextState.editing = false;
        return nextState;

      case constants.CHANGE_RANK_SUCCESS:
				nextState.editing = true;
        return nextState;

			case constants.CHANGE_RANK_FAIL:
				console.error(action.payload.error);
        return nextState;

			default:
				return nextState;

		}

	}
}


export default new HRStore(AppDispatcher);
