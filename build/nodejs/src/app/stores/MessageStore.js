import AppDispatcher from '../AppDispatcher';
import constants from '../constants';
import { ReduceStore } from 'flux/utils';

class MessageStore extends ReduceStore {

	getInitialState() {
		return {
			users: [],
			messages: [],
			draft: {},
			owner_key: undefined,
			profile_key: undefined,
			sending: false,
			uploading: false,
		};
	}

	reduce(state, action) {

		let nextState = Object.assign({}, state);

    switch(action.type) {
			case constants.EDIT_MESSAGE_DRAFT:
				nextState.draft = action.payload.response;
				return nextState;
			case constants.CHOOSE_MESSAGE:
				return Object.assign({}, nextState, action.payload.response);

			case constants.CHOOSE_OWNER_MESSAGE:
				nextState.owner_key = action.payload.response.key;
				return nextState;
			case constants.CHOOSE_OTHER_MESSAGE:
				nextState.profile_key = action.payload.response.key;
				return nextState;

			case constants.GET_MESSAGE:
				// nextState.users = [];
				// nextState.messages = [];
				// nextState.draft = {};
				// nextState.sending = false;
				// nextState.uploading = false;
				return nextState;

			case constants.GET_MESSAGE_SUCCESS:
				nextState = Object.assign({}, nextState, action.payload.response);
				console.log(action.payload.response)
				return nextState;
			case constants.UPLOAD_MESSAGE_IMAGE:
			case constants.UPLOAD_MESSAGE_FILE:
				nextState.uploading = true;
				return nextState;
			case constants.SEND_MESSAGE:
				nextState.sending = true;
				return nextState;
			case constants.SEND_MESSAGE_SUCCESS:
				nextState.draft = {};
				nextState.sending = false;
				return nextState;
			case constants.UPLOAD_MESSAGE_FILE_SUCCESS:
				console.log(action.payload.response);
				nextState.draft.message = action.payload.response.original_name;
				nextState.draft.path = action.payload.response.path;
				nextState.uploading = false;
				return nextState;

			case constants.UPLOAD_MESSAGE_IMAGE_SUCCESS:
				nextState.draft.message = action.payload.response.path;
				nextState.uploading = false;
				return nextState;

			case constants.GET_MESSAGE_FAIL:
			case constants.UPLOAD_MESSAGE_FILE_FAIL:
				console.error(action.payload.error);
				alert('Upload Fail')
				nextState.uploading = false;
				return nextState;
      default:
        return nextState;
    }

	}
}
export default new MessageStore(AppDispatcher);
