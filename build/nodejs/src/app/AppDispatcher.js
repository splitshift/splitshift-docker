import {Dispatcher} from 'flux';

class AppDispatcher extends Dispatcher{
  dispatch(action = {}) {
    // console.log("Dispatched", action.type);
    setTimeout(() => {
      super.dispatch(action);
    }, 1);
  }

  /**
   * Dispatches three actions for an async operation represented by promise.
   */
  dispatchAsync(promise, types, payload){
    const { request, success, failure } = types;

    setTimeout(() => {
      this.dispatch({ type: request, payload: Object.assign({}, payload) });
    }, 1);

    promise.then(
      (response) => {
        this.dispatch({
          type: success,
          payload: Object.assign({}, payload, { response })
        })
      },
      error => this.dispatch({
        type: failure,
        payload: Object.assign({}, payload, { error })
      })
    );
  }

}

export default new AppDispatcher();
