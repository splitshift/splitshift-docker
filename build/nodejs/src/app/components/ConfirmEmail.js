import React, { Component } from 'react'

class ConfirmEmail extends Component {

  constructor(argument) {
    super(argument)
  }

  componentWillMount () {
    let user = JSON.parse($("#user").html())

    localStorage.setItem('key', user.key)
    localStorage.setItem('token', user.token)
    localStorage.setItem('type', user.type)


    setTimeout(() => {
      
      if(user.type === 'Employee')
        window.location.href = '/th/profile'
      else
        window.location.href = '/th/company'

    }, 3000);



  }

  render () {
    return (
      <div style={{ marginTop: '100px', textAlign: 'center' }}>
        <h2>Confirm Email Complete</h2>
        <div>
          <h4>Splitshift is redirecting to Profile Page...</h4>
        </div>

      </div>
    )
  }
}

export default ConfirmEmail
