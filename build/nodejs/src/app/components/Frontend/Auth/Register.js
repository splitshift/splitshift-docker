import React, {Component} from 'react';

import AuthenticationActionCreators from '../../../actions/AuthenticationActionCreators';

import JQueryTool from '../../../tools/JQueryTool';
import configLang from '../../../tools/configLang'

let self;
let auth2;
let loadGAPI;

class AuthRegister extends Component {

  facebookRegister() {
    FB.login((response) => {
			let loginInfo = response;

			if(response.status === "connected") {
				FB.api('/me', {fields: 'first_name, last_name, gender ,age_range, email'}, (response) => {

					AuthenticationActionCreators.createRegister({
						'first_name':response.first_name,
						'last_name':response.last_name,
						'email':response.email,
						'term': false,
						'register_type': 'Facebook',
						'type': 'Employee',
						'access_token': loginInfo.authResponse.accessToken,
					});

          JQueryTool.toPostRegister();

				});
			}

		}, {scope: 'email' });

  }

  googleRegister(googleUser) {

    let profile = googleUser.getBasicProfile();
    let first_name = profile.getGivenName();
    let last_name = profile.getFamilyName();

    AuthenticationActionCreators.createRegister({
      'first_name':first_name,
      'last_name':last_name,
      'email': profile.getEmail(),
      'term': false,
      'register_type': 'Google',
      'type': 'Employee',
      'access_token': googleUser.getAuthResponse().id_token,
    });

    JQueryTool.toPostRegister();

  }

  emailRegister() {

    AuthenticationActionCreators.createRegister({
      'first_name': '',
      'last_name': '',
      'email': '',
      'term': false,
      'register_type': 'Simple',
      'type': 'Employee',
      'password': '',
      'confirmPassword': '',
    });

    JQueryTool.toPostRegister();

  }

  attachSignup(element) {
    auth2.attachClickHandler(element, {}, this.googleRegister);
  }

  initGoogleAuth() {
    gapi.load('auth2', function(){

      auth2 = gapi.auth2.init({
         client_id: '187395441115-as5ie9or8dl1obnegmvmdk14dfeshnvi.apps.googleusercontent.com',
         cookiepolicy: 'single_host_origin',
       })

       self.attachSignup(document.getElementById('customBtn2'));
    });
  }

  componentDidMount() {

    self = this;

    if(typeof gapi !== 'undefined') {
      this.initGoogleAuth()
      loadGAPI = true;
    }

  }

  componentDidUpdate() {
    if(typeof gapi !== 'undefined' && !loadGAPI) {
      this.initGoogleAuth()
      loadGAPI = true;
    }
  }

  render() {
    let formPreregisContent = configLang[this.props.language].form.pre_register

    return (
      <div className="modal fade" id="myModal__Register" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
    		<div className="modal-dialog modal-sm" role="document">
      		<div className="modal-content">

            <div className="modal-header" style={{ borderBottom: "0px", paddingBottom: "0px" }}>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span5 aria-hidden="true">&times;</span5>
              </button>
            </div>

            <div className="modal-body">
      				<div id="modal__btn" className="row">
        				<div className="col-xs-12 col-md-6">
        					<a href="#" data-toggle="modal">
                    <button onClick={this.facebookRegister.bind(this)} className="modal__btn--fb" type="button">
                      <i className="fa fa-facebook" aria-hidden="true"></i> Facebook
                    </button>
                  </a>
        				</div>
                <div className="col-xs-12 col-md-6">
                  <button id="customBtn2" type="button" className="modal__btn--gg">
                    <i className="fa fa-google"></i> Google
                  </button>
        				</div>
        			</div>

              <div className="row">
                <div className="col-xs-12 col-md-12">
                  <hr id="hr-or" />
                  <p className="or">or</p>
                </div>
              </div>

              <div className="modal__btnregis">
                <a href="#">
                  <button onClick={this.emailRegister.bind(this)} className="modal__btn--dark" type="button" data-toggle="modal">
                    {formPreregisContent[0]}
                  </button>
                </a>
              </div>

      			</div>

            <div className="modal-footer">
        			<p>{formPreregisContent[1]}</p>
              <div>
                <a href="#"><button className="modal__btn--light" data-toggle="modal" data-target="#myModal__login" data-dismiss="modal">Log in</button></a>
              </div>
      			</div>

      		</div>
    		</div>
    	</div>
    );
  }
}

export default AuthRegister;
