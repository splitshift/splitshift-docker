import React, {Component} from 'react';
import { Link } from 'react-router'
import AuthenticationActionCreators from '../../../actions/AuthenticationActionCreators';
import configLang from '../../../tools/configLang'

class AuthPostRegister extends Component {

  chooseCustomerType(evt) {
		let user = Object.assign({}, this.props.user);
		user['type'] = evt.target.value;
		AuthenticationActionCreators.updateRegister(user);
	}

  componentDidMount() {
    let temp = Object.assign({}, this.props.user);
		temp.term = false;
    AuthenticationActionCreators.updateRegister(temp);
  }

  thickCheckBox(evt) {
		let temp = Object.assign({}, this.props.user);
		temp.term = !temp.term;
		AuthenticationActionCreators.updateRegister(temp);
	}

  getValue(name, evt) {
		let temp = Object.assign({}, this.props.user);
		temp[name] = evt.target.value;
		AuthenticationActionCreators.updateRegister(temp);
	}

  hidePostRegister(evt) {
    $("#myModal__Regisform").modal('hide');
  }

  submitForm(evt) {

    evt.preventDefault();

		if(!/.+@.+[.].+/.test(this.props.user.email))
			alert('email not valid');
		else if(this.props.user.register_type == 'Simple' && this.props.user.password == '')
			alert('please enter password');
		else if(this.props.user.register_type == 'Simple' && this.props.user.password != this.props.user.confirmPassword)
			alert('password and confirm password not the same');
		else if(!this.props.user.term)
			alert('Please accept our User Agreement');
		else
			AuthenticationActionCreators.register(this.props.user);

	}


  render() {

    let passwordForm = "";
    let formContent = configLang[this.props.language].form
    let formRegisterContent = formContent.register

    if(this.props.user.register_type == 'Simple') {
      passwordForm = (
        <div>
          <label htmlFor="pwd">{formContent.password}</label>
          <input type="password" onChange={this.getValue.bind(this, 'password')} className="form-control" placeholder={formContent.password + " ..."} value={this.props.user.password} />

          <label htmlFor="pwd">{formContent.confirm_password}</label>
          <input type="password" onChange={this.getValue.bind(this, 'confirmPassword')} className="form-control" placeholder={formContent.confirm_password + " ..."} value={this.props.user.confirmPassword} />
        </div>
      );
    }

    return (
      <div className="modal fade" id="myModal__Regisform" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog modal-sm" role="document">
          <div className="modal-content">

            <div className="modal-header" style={{ borderBottom: "0px", paddingBottom: "0px" }}>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span5 aria-hidden="true">&times;</span5>
              </button>
            </div>

            <div className="modal-body">
              <div className="modal__form">
                <div className="form__type">
                  <input type="radio" name="type" value="Employee" id="employee" onChange={this.chooseCustomerType.bind(this)}  defaultChecked={true} />

                  <label htmlFor="employee">
                    <span style={{ fontSize: "13px" }}>
                      <strong>{formRegisterContent[0]}</strong>
                    </span>
                  </label>

                  <input type="radio" name="type" value="Employer" id="employer" onChange={this.chooseCustomerType.bind(this)}  />

                  <label htmlFor="employer">
                    <span style={{ fontSize: "13px" }}>
                      <strong>{formRegisterContent[1]}</strong>
                    </span>
                  </label>

                </div>

                <div className="modal__employ">
                  <div className="form-group">

                    <form onSubmit={this.submitForm.bind(this)} >

                      {this.props.user.type == "Employer"?
                        <div>
                          <label htmlFor="usr">{formRegisterContent[2]}</label>
                          <input type="text" onChange={this.getValue.bind(this, 'company_name')} className="form-control" placeholder={formRegisterContent[2] + " ..."} />
                        </div>
                      :""}

                      {(this.props.user.register_type === "Facebook" && this.props.user.first_name) || (this.props.user.register_type === "Google" && this.props.user.first_name) || (this.props.user.register_type == "Simple")?
                      <div>
                        <label htmlFor="usr">{formRegisterContent[3]}</label>
                        <input type="text" onChange={this.getValue.bind(this,'first_name')} className="form-control" placeholder={formRegisterContent[3] + ' ...'} defaultValue={this.props.user.first_name} />
                        <label htmlFor="usr">{formRegisterContent[4]}</label>
                        <input type="text" onChange={this.getValue.bind(this,'last_name')} className="form-control" placeholder={formRegisterContent[4] + ' ...'} defaultValue={this.props.user.last_name}/>
                        <label htmlFor="pwd">{formRegisterContent[5]}</label>
                        <input type="text" onChange={this.getValue.bind(this, 'email')} className="form-control" placeholder={formRegisterContent[5] + ' ...'} defaultValue={this.props.user.email}  />
                      </div>
                      :""}

                      {passwordForm}

                      <div className="modal__checkbox">
                        <div className="checkbox--remember">
                          <input type="checkbox" id="remember" onChange={this.thickCheckBox.bind(this)} />{formRegisterContent[6]} <Link onClick={this.hidePostRegister.bind(this)} to="/th/user-agreement">{formRegisterContent[7]}</Link>.
                        </div>
                      </div>
                      <div style={{ marginBottom: "10px" }}>
                        <a href="#">
                          <button className="modal__btn--dark" type="submit">{formRegisterContent[8]}</button>
                        </a>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    );
  }
}

export default AuthPostRegister;
