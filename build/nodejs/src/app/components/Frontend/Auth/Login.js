import React, {Component} from 'react';

import AuthenticationActionCreators from '../../../actions/AuthenticationActionCreators';
let loginTrigger = false;
let auth2;
let loadGAPI = false;

import configLang from '../../../tools/configLang'

class AuthLogin extends Component {

  facebookLogin() {
		let info, email, token;
		let self = this;
		FB.login((response) => {
			//console.log(response);
			token = response.authResponse.accessToken;
			if(response.status === "connected") {
				FB.api('/me', {fields: 'first_name, last_name, gender ,age_range, email'}, (response) => {
					//console.log(response);

					email = response.email;
					info = {
						'email': email,
						'access_token': token	,
						'login_type': 'Facebook',
					}

					AuthenticationActionCreators.login(info)
				});
			}
		}, {scope: 'email' });
	}

  onSuccess(googleUser) {

    let profile = googleUser.getBasicProfile();
    let email = profile.getEmail();
    let token = googleUser.getAuthResponse().id_token;

    let info = {
      email: profile.getEmail(),
      access_token: token,
      login_type: 'Google'
    }

    AuthenticationActionCreators.login(info)

  }

  onLoginTrigger(evt) {
    loginTrigger = true;
  }

  simpleLogin(evt) {
    evt.preventDefault();
		if(!/.+@.+[.].+/.test(this.props.state.email)) {
			alert('email not valid');
			console.error('email not valid');
			return;
		}
		let info = {
			'email': this.props.state.email,
			'password': this.props.state.password,
			'login_type': 'Simple',
		}
		AuthenticationActionCreators.login(info);
	}

  getValue(name, evt) {
    let temp = Object.assign({}, this.props.state);
    temp[name] = evt.target.value;
    AuthenticationActionCreators.updateLogin(temp);
  }

  attachSignin(element) {
    auth2.attachClickHandler(element, {}, this.onSuccess);
  }

  initGoogleAuth() {
    let self = this;

    gapi.load('auth2', () => {
      auth2 = gapi.auth2.init({
         client_id: '187395441115-as5ie9or8dl1obnegmvmdk14dfeshnvi.apps.googleusercontent.com',
         cookiepolicy: 'single_host_origin',
       });
       self.attachSignin(document.getElementById('customBtn'));
    });
  }

  componentDidMount() {
    if(typeof gapi !== 'undefined') {
      this.initGoogleAuth()
      loadGAPI = true
    }
  }

  componentDidUpdate() {
    if(typeof gapi !== 'undefined' && !loadGAPI) {
      this.initGoogleAuth()
      loadGAPI = true
    }
  }

  hideLogin(evt) {
    this.props.registerBtnTrigger(true);
    setTimeout(function () { $("#myModal__Register").modal('show') }, 500)
  }

  hideToForgot(evt) {
    $("#myModal__login").modal('hide');
    setTimeout(function () { $("#myModal__forgot").modal('show') }, 500)
  }

  render() {
    let formLoginContent = configLang[this.props.language].form.login

    return (
      <div className="modal fade" id="myModal__login" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog modal-sm" role="document">
            <div className="modal-content">

              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span5 aria-hidden="true">&times;</span5>
                </button>
              </div>

              <div className="modal-body">

                <div id="modal__btn" className="row">
                  <div  className="col-xs-12 col-md-6">
                    <a onClick={this.facebookLogin.bind(this)} href="#"><button type="button" className="modal__btn--fb ">
                      <i className="fa fa-facebook"></i> Facebook
                    </button></a>
                  </div>
                  <div className="col-xs-12 col-md-6">
                    <button id="customBtn" type="button" className="modal__btn--gg">
                      <i className="fa fa-google"></i> Google
                    </button>

                  </div>
                </div>

                <div className="row">
                  <div className="col-xs-12 col-md-12">
                    <hr id="hr-or" />
                    <p className="or">or</p>
                  </div>
                </div>

                <div className="modal__form">
                  <div className="form-group">
                    <form onSubmit={this.simpleLogin.bind(this)}>
                      <label htmlFor="usr">{formLoginContent[0]}</label>
                      <input type="text" defaultValue={this.props.state.email} onChange={this.getValue.bind(this, 'email')} className="form-control" placeholder={formLoginContent[0] + ' ...'} id="usr" />
                      <label htmlFor="pwd">{formLoginContent[1]}</label>
                      <input type="password" defaultValue={this.props.state.password} onChange={this.getValue.bind(this, 'password')} className="form-control" placeholder={formLoginContent[1] + ' ...'} id="pwd" />
                      <div className="modal__checkbox">
                        <div className="checkbox--remember">
                          <input type="checkbox" id="remember" /><strong>{formLoginContent[2]}</strong>
                        </div>
                        <div className="modal__fpwd">
                          <a className="text--fpwd" href="#" onClick={this.hideToForgot.bind(this)}><strong>{formLoginContent[3]}</strong></a>
                        </div>
                      </div>
                      <div>
                        <a href="#">
                          <button className="modal__btn--dark">{formLoginContent[4]}</button>
                        </a>
                      </div>
                    </form>
                  </div>
                </div>

              <div className="modal-footer">
                <p>{formLoginContent[5]} </p>
                <div>
                  <a href="#">
                    <button className="modal__btn--light" data-dismiss='modal' onClick={this.hideLogin.bind(this)}>{formLoginContent[6]}</button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AuthLogin;

/*
<a href="#">
  <button type="button" className="modal__btn--gg" onClick={this.googleLogin.bind(this)}>
    <i className="fa fa-google"></i> Google
  </button>
</a>

*/
