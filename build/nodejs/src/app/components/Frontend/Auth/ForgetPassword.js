import React, { Component } from 'react';

import AuthenticationActionCreators from '../../../actions/AuthenticationActionCreators';

class AuthForgetPassword extends Component {

  componentDidMount() {
    $("#myModal__forgot").modal('hide');
  }

  sendForgetPassword(evt) {
    evt.preventDefault();
    let result = { email: this.refs.email_forget.value };
    AuthenticationActionCreators.forgetPassword(result);
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(nextProps.login.forget && !this.props.login.forget)
      $("#myModal__forgot").modal('hide');

    return true;
  }

  render() {
    return (
      <div className="modal fade" id="myModal__forgot" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog modal-sm" role="document">
            <div className="modal-content">

              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span5 aria-hidden="true">&times;</span5>
                </button>
              </div>

              <div className="modal-body">

                <div className="modal__form">
                  <div className="form-group">
                    <form onSubmit={this.sendForgetPassword.bind(this)}>
                      <label htmlFor="usr">Email</label>
                      <input type="text" ref="email_forget" className="form-control" placeholder="Email..." id="usr" />
                      <div>
                        <button className="modal__btn--dark">SEND EMAIL TO RESET PASSWORD</button>
                      </div>
                    </form>
                  </div>
                </div>

              <div className="modal-footer">
                <p>Don{"'"}t have an account? </p>
                <div>
                  <a href="#">
                    <button className="modal__btn--light"  data-toggle="modal" data-target="#myModal__login" data-dismiss="modal">Sign In</button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default AuthForgetPassword;
