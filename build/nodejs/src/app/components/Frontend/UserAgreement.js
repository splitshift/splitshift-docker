import React, { Component } from 'react'
import { Link } from 'react-router'

class UserAgreement extends Component {
  render () {
    return (
      <div style={{marginTop: "100px"}}>
        <div className="whatweoffer">
          <div className="container container__privacy">
            <h2>Statement of Rights and Responsibilities – Terms of Use</h2>
            <p>
              This Statement of Rights and Responsibilities ("Statement," "Terms," or "SRR") derives from Splitshift and is our terms of service that governs our relationship with users and others who interact with Splitshift, as well as Splitshift brands, products and services, which we call the “Splitshift Services” or “Services”. By using or accessing the Splitshift Services, you agree to this Statement, as updated from time to time in accordance with Section 9 below. Additionally, you will find resources at the end of this document that help you understand how Splitshift works.
            </p>
            <p>
              Because Splitshift provides a wide range of Services, we may ask you to review and accept supplemental terms that apply to your interaction with a specific app, product, or service. To the extent those supplemental terms conflict with this SRR, the supplemental terms associated with the app, product, or service govern with respect to your use of such app, product or service to the extent of the conflict.
            </p>

            <h4>1. Privacy</h4>
            <p>
              Your privacy is very important to us. We designed our <Link to='/privacy'>Data Policy</Link> to make important disclosures about how you can use Splitshift to share with others and how we collect and can use your content and information. We encourage you to read the Data Policy, and to use it to help you make informed decisions.
            </p>

            <h4>2. Sharing Your Content and Information</h4>
            <p>
              You own all of the content and information you post on Splitshift, and you can control how it is shared through your privacy and Splitshift account settings. In addition:
              <ol>
                <li>For content that is covered by intellectual property rights, like photos and videos (IP content), you specifically give us the following permission, subject to your privacy and application settings: you grant us a non-exclusive, transferable, sub-licensable, royalty-free, worldwide license to use any IP content that you post on or in connection with Splitshift (IP License). This IP License ends when you delete your IP content or your account unless your content has been shared with others, and they have not deleted it.</li>
                <li>When you delete IP content, it is deleted in a manner similar to emptying the recycle bin on a computer. However, you understand that removed content may persist in backup copies for a reasonable period of time (but will not be available to others).</li>
                <li>When you publish content or information using the Public setting, it means that you are allowing everyone, including people off Splitshift, to access and use that information, and to associate it with you (i.e., your name and profile picture).</li>
                <li>We always appreciate your feedback or other suggestions about Splitshift, but you understand that we may use your feedback or suggestions without any obligation to compensate you for them (just as you have no obligation to offer them).</li>
              </ol>
            </p>

            <h4>3. Safety</h4>
            <p>
              We do our best to keep Splitshift safe, but we cannot guarantee it. We need your help to keep Splitshift safe, which includes the following commitments by you:
              <ol>
                <li>You will not post unauthorized commercial communications (such as spam) on Splitshift.</li>
                <li>You will not collect users' content or information, or otherwise access Splitshift, using automated means (such as harvesting bots, robots, spiders, or scrapers) without our prior permission.</li>
                <li>You will not engage in unlawful multi-level marketing, such as a pyramid scheme, on Splitshift.</li>
                <li>You will not upload viruses or other malicious code.</li>
                <li>You will not solicit login information or access an account belonging to someone else.</li>
                <li>You will not bully, intimidate, or harass any user.</li>
                <li>You will not post content that: is hate speech, threatening, or pornographic; incites violence; or contains nudity or graphic or gratuitous violence.</li>
                <li>You will not operate a third-party application containing alcohol-related, dating or other mature content (including advertisements) without appropriate age-based restrictions.</li>
                <li>You will not use Splitshift to do anything unlawful, misleading, malicious, or discriminatory.</li>
                <li>You will not do anything that could disable, overburden, or impair the proper working or appearance of Splitshift, such as a denial of service attack or interference with page rendering or other Splitshift functionality.</li>
                <li>You will not facilitate or encourage any violations of this Statement or our policies.</li>
              </ol>
            </p>

            <h4>4. Registration and Account Security</h4>
            <p>
              Splitshift users provide their real names and information, and we need your help to keep it that way. Here are some commitments you make to us relating to registering and maintaining the security of your account:
              <ol>
                <li>You will not provide any false personal information on Splitshift, or create an account for anyone other than yourself without permission.</li>
                <li>You will not create more than one personal account.</li>
                <li>If we disable your account, you will not create another one without our permission.</li>
                <li>You will not use your personal profile primarily for your own commercial gain, and will use a Splitshift Page for such purposes.</li>
                <li>You will not use Splitshift if you are under 13.</li>
                <li>You will not use Splitshift if you are a convicted sex offender.</li>
                <li>You will keep your contact information accurate and up-to-date.</li>
                <li>You will not share your password or let anyone else access your account, or do anything else that might jeopardize the security of your account.</li>
                <li>You will not transfer your account to anyone without first getting our written permission.</li>
                <li>If you select a username or similar identifier for your account, we reserve the right to remove or reclaim it if we believe it is appropriate (such as when a trademark owner complains about a username that does not closely relate to a user's actual name).</li>
              </ol>
            </p>

            <h4>5. Protecting Other People's Rights</h4>
            <p>
              We respect other people's rights, and expect you to do the same.
              <ol>
                <li>You will not post content or take any action on Splitshift that infringes or violates someone else's rights or otherwise violates the law. </li>
                <li>We can remove any content or information you post on Splitshift if we believe that it violates this Statement or our policies.</li>
                <li>If we remove your content for infringing someone else's copyright, and you believe we removed it by mistake, we will provide you with an opportunity to appeal.</li>
                <li>If you repeatedly infringe other people's intellectual property rights, we will disable your account when appropriate.</li>
                <li>You will not use our copyrights or Trademarks or any confusingly similar marks, except as expressly permitted by our prior written permission.</li>
                <li>If you collect information from users, you will: obtain their consent, make it clear you (and not Splitshift) are the one collecting their information, and post a privacy policy explaining what information you collect and how you will use it.</li>
                <li>You will not post anyone's identification documents or sensitive financial information on Splitshift.</li>
                <li>You will not tag users or send email invitations to non-users without their consent. Splitshift offers social reporting tools to enable users to provide feedback about tagging.</li>
              </ol>
            </p>

            <h4>6. Amendments</h4>
            <p>
              <ol>
                <li>We’ll notify you before we make changes to these terms and give you the opportunity to review and comment on the revised terms before continuing to use our Services.</li>
                <li>If we make changes to policies, guidelines or other terms referenced in or incorporated by this Statement, we may provide notice by email.</li>
                <li>Your continued use of the Splitshift Services, following notice of the changes to our terms, policies or guidelines, constitutes your acceptance of our amended terms, policies or guidelines.</li>
              </ol>
            </p>

            <h4>7. Termination</h4>
            <p>
              If you violate the letter or spirit of this Statement, or otherwise create risk or possible legal exposure for us, we can stop providing all or part of Splitshift to you. We will notify you by email or at the next time you attempt to access your account. You may also delete your account or disable your application at any time. In all such cases, this Statement shall terminate, but the following provisions will still apply: 2.2, 2.3, 3-5, and 7-10.
            </p>

            <h4>8. Disputes</h4>
            <p>
              <ol>
                <li>This Statement of Rights and Responsibilities – Terms of Use is made under and shall be construed in accordance with the laws of the Kingdom of Thailand. Any dispute arising from this agreement shall be submitted to a court of competent jurisdiction.</li>
                <li>If anyone brings a claim against us related to your actions, content or information on Splitshift, you will indemnify and hold us harmless from and against all damages, losses, and expenses of any kind (including reasonable legal fees and costs) related to such claim. Although we provide rules for user conduct, we do not control or direct users' actions on Splitshift and are not responsible for the content or information users transmit or share on Splitshift. We are not responsible for any offensive, inappropriate, obscene, unlawful or otherwise objectionable content or information you may encounter on Splitshift. We are not responsible for the conduct, whether online or offline, of any user of Splitshift. </li>
                <li>WE TRY TO KEEP SPLITSHIFT UP, BUG-FREE, AND SAFE, BUT YOU USE IT AT YOUR OWN RISK. WE ARE PROVIDING SPLITSHIFT AS IS WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. WE DO NOT GUARANTEE THAT SPLITSHIFT WILL ALWAYS BE SAFE, SECURE OR ERROR-FREE OR THAT SPLITSHIFT WILL ALWAYS FUNCTION WITHOUT DISRUPTIONS, DELAYS OR IMPERFECTIONS. SPLITSHIFT IS NOT RESPONSIBLE FOR THE ACTIONS, CONTENT, INFORMATION, OR DATA OF THIRD PARTIES, AND YOU RELEASE US, OUR DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS FROM ANY CLAIMS AND DAMAGES, KNOWN AND UNKNOWN, ARISING OUT OF OR IN ANY WAY </li>
              </ol>
            </p>

            <h4>9. Definitions</h4>
            <p>
              <ol>
                <li>By "Splitshift" or” Splitshift Services” we mean the features and services we make available, including through (a) our website at www.splitshift.com and any other Splitshift branded or co-branded websites (including sub-domains, international versions, widgets, and mobile versions); (b) our Platform; (c) social plugins such as the Like button, the Share button and other similar offerings; and (d) other media, brands, products, services, software (such as a toolbar), devices, or networks now existing or later developed. Splitshift reserves the right to designate, in its sole discretion, that certain of our brands, products, or services are governed by separate terms and not this SRR.</li>
                <li>By "information" we mean facts and other information about you, including actions taken by users and non-users who interact with Splitshift.</li>
                <li>By "content" we mean anything you or other users post, provide or share using Splitshift Services.</li>
                <li>By "data" or "user data" or "user's data" we mean any data, including a user's content or information that you or third parties can retrieve from Splitshift or provide to Splitshift through Platform.</li>
                <li>By "post" we mean post on Splitshift or otherwise make available by using Splitshift.</li>
                <li>By "use" we mean use, run, copy, publicly perform or display, distribute, modify, translate, and create derivative works of.</li>
              </ol>
            </p>

            <h4>10. Other</h4>
            <p>
              <ol>
                <li>This Statement makes up the entire agreement between the parties regarding Splitshift, and supersedes any prior agreements.</li>
                <li>If any portion of this Statement is found to be unenforceable, the remaining portion will remain in full force and effect.</li>
                <li>If we fail to enforce any of this Statement, it will not be considered a waiver.</li>
                <li>Any amendment to or waiver of this Statement must be made in writing and signed by us.</li>
                <li>You will not transfer any of your rights or obligations under this Statement to anyone else without our consent.</li>
                <li>All of our rights and obligations under this Statement are freely assignable by us in connection with a merger, acquisition, or sale of assets, or by operation of law or otherwise.</li>
                <li>Nothing in this Statement shall prevent us from complying with the law.</li>
                <li>This Statement does not confer any third party beneficiary rights.</li>
                <li>We reserve all rights not expressly granted to you.</li>
                <li>You will comply with all applicable laws when using or accessing Splitshift.</li>
              </ol>
            </p>

            <p>
              <b>By using or accessing Splitshift Services, you agree that we can collect and use such content and information in accordance with the Data Policy as amended from time to time.</b>
            </p>

            <p>
              To access the Statement of Rights and Responsibilities in  English and Thai change the language setting for your Splitshift session by clicking on the language link in the left corner of most pages. If the Statement is not available in the language you select, we will default to the English version.
            </p>




          </div>
        </div>
      </div>
    )
  }
}

export default UserAgreement
