import React, { Component } from 'react';


let currentMap;

class MapSearch extends Component {

  initialMap(myLatLng) {
    currentMap = new google.maps.Map(document.getElementById('map1'), {
      center: myLatLng,
      zoom: 16,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    });
  }

  componentDidMount() {

    currentMap = new google.maps.Map(document.getElementById('map1'), {
      center: this.props.choose_location,
      zoom: 16,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    });


  }

  componentDidUpdate() {


    if(this.props.new_searchmap) {

      this.initialMap(this.props.choose_location);

      if(this.props.companies.length > 0) {
        this.props.companies.map((company, index) => {

          if(company.address.location[0] === 1 || company.address.location[1] === 1)
            return "";

          let new_marker = new google.maps.Marker({
            position: { lng: company.address.location[0], lat: company.address.location[1] },
            map: currentMap
          });

          let mainwindow = new google.maps.InfoWindow({content: "<b><a href='/th/company/" + company.key +"' >" + company.profile.openjobs + " job" + (company.profile.openjobs > 1 ? 's' : '') + "</a></b>"});
          mainwindow.open(currentMap, new_marker);

        });
      }else if(this.props.jobs.length > 0) {
        this.props.jobs.map((job, index) => {

          if(job.owner.address.location[0] === 1 || job.owner.address.location[1] === 1)
            return "";

          let new_marker = new google.maps.Marker({
            position: { lng: job.owner.address.location[0], lat: job.owner.address.location[1] },
            map: currentMap
          });

          let mainwindow = new google.maps.InfoWindow({content: "<b><a href='/th/company/" + job.owner.key +"' >" + job.position + "</a></b>"});
          mainwindow.open(currentMap, new_marker);

        });
      }

      currentMap.setCenter(this.props.choose_location);
      this.props.triggerSearchMap();
    }

  }

  render() {
    return (
      <div className="jobs__map">
        <div id="map1"></div>
      </div>
    )
  }
}

export default MapSearch;
