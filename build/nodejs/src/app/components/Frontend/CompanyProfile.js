import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';

import CompanyHeader from './Form/Company/Header';
import CompanyPosition from './Form/Company/Position';
import CompanyPeople from './Form/Company/People';
import CompanyCulture from './Form/Company/Culture';
import CompanyBenefit from './Form/Company/Benefit';
import CompanyVideo from './Form/Company/Video';
import CompanyLocation from './Form/Company/Location';
import CompanyOverview from './Form/Company/Overview';
import CompanySocialLink from './Form/Company/SocialLink';
import CompanyJob from './Form/Company/Job';
import CompanyReview from './Form/Company/Review';
import CompanyGallery from './Form/Company/Gallery';
import CompanySummaryReview from './Form/Company/SummaryReview';
import CompanyLastestReview from './Form/Company/LastestReview';

import FormFeedNewsFeed from './Form/Feed/NewsFeed';
import FormFeedPhoto from './Form/Feed/Photo';

import ResourceActionCreators from '../../actions/ResourceActionCreators';
import CompanyProfileActionCreators from '../../actions/CompanyProfileActionCreators';
import MessageActionCreators from '../../actions/MessageActionCreators';
import SocialActionCreators from '../../actions/SocialActionCreators';

import BackgroundTemplate from './BackgroundTemplate';
import Message from './Message';

import JQueryTool from '../../tools/JQueryTool'
import configLang from '../../tools/configLang'

let currentMap;
let button_header_click = false;

class CompanyProfile extends Component {

	loadMessage(evt) {
    MessageActionCreators.chooseOwner(this.props.user);
		MessageActionCreators.chooseOther(this.props.profile);
    MessageActionCreators.getMessageWithPeople(this.props.user.token, this.props.profile.key);
  }

	triggerMap(map) {
		currentMap = map;
	}

	triggerButton(evt) {
		button_header_click = true;
		JQueryTool.animateScrollTo(this.props.page != 'company_image_profile' ? '.newsfeed' : '.timeline-photo')
	}

	changeBannerImageWithTemplate(template_url, evt) {
		ResourceActionCreators.setDraft('template_banner', template_url);
		CompanyProfileActionCreators.changeBannerImageWithTemplate(this.props.user.token, { path: template_url });
		$("#myModal__background").modal('hide');
	}

	toggleMoreInfo(evt) {
		$("#more-info-detail").toggle();
		$(evt.target).html($(evt.target).html() === "More Information" ? "Less Information" : "More Information");
		if(currentMap)
			google.maps.event.trigger(currentMap, 'resize');
	}


	loadNewCompany(profile_key) {
		CompanyProfileActionCreators.getProfile(profile_key, this.props.user.token);
		SocialActionCreators.getFollowers(profile_key);
	}

	componentDidMount() {
		let profile_key = "";
		button_header_click = false;

		if(typeof this.props.profile_key !== 'undefined')
			profile_key = this.props.profile_key
		else if(typeof this.props.user.key !== 'undefined')
			profile_key = this.props.user.key

		this.loadNewCompany(profile_key)

	}

	shouldComponentUpdate(nextProps, nextState) {

		if(nextProps.profile_key && nextProps.profile_key !== this.props.profile_key)
			this.loadNewCompany(nextProps.profile_key)
		else if(typeof nextProps.profile_key === 'undefined' && typeof this.props.profile_key !== 'undefined' && this.props.user.token)
			this.loadNewCompany(this.props.user.key)

		return true;
	}


	setEditor(name) {
    let oldData = {
      first_name: this.props.profile.first_name,
      last_name: this.props.profile.last_name,
      company_name: this.props.profile.company_name,
      type: this.props.profile.type ? this.props.profile.type : "Hotel"
    }
    ResourceActionCreators.setOldDraft(oldData);
    ResourceActionCreators.setEditor(name, true);
  }


	onFollowPeople(evt) {
    SocialActionCreators.putFollow(this.props.user.token, this.props.profile.key)
  }

  render() {
		let formState = {
			profile: this.props.profile,
			status: this.props.status,
			user: this.props.user,
			lang: this.props.lang,
			editor: this.props.editor,
			other: this.props.profile_key && this.props.profile_key != this.props.user.key
		};

		if(typeof this.props.profile.key === 'undefined')
			return <div />

		let menusContent = configLang[this.props.lang].company_profile.menus

		return (
			<div>
				<CompanyHeader {...formState} />
				<CompanyJob user={this.props.user} profile={this.props.profile} job={this.props.job} />
			  <CompanyReview {...formState} />

				<div className="cover__nav">
	        <div className="row">
	          <div className="col-md-8">
	            <div className="navbar__btnlist">
	              <Link to={'/' + this.props.lang + '/company/' + this.props.profile.key} onClick={this.triggerButton.bind(this)} className="btn btn-default reverse">{menusContent[0]}</Link>
	              <Link to={'/' + this.props.lang + '/company/' + this.props.profile.key + '/photo'} onClick={this.triggerButton.bind(this)} className="btn btn-default reverse">{menusContent[1]}</Link>

								{
									this.props.editor ?
										<span className="navbar__editbtn">
											<Link className='btn btn-default reverse' to={ '/' + this.props.lang + '/company/' + this.props.profile.key}>{menusContent[2]}</Link>
											<button className='btn btn-default reverse' onClick={this.setEditor.bind(this, "edit_company_profile_card")}>{menusContent[3]}</button>
										</span>
									: ""
								}


	              {
									this.props.user.token !== 'undefined' && formState.other ?
										<button onClick={this.loadMessage.bind(this)} className="btn btn-default reverse" data-toggle="modal" data-target="#myModal__message">{menusContent[4]}</button>
										: ""
								}

	            </div>
							{
								formState.other && this.props.profile.key ?
								<div className="navbar__btnlist">
									{
										this.props.user.token && typeof this.props.profile.following !== 'undefined' ?
											<button className={ this.props.profile.following ? "btn btn-default reverse" : "btn btn-default"} onClick={this.onFollowPeople.bind(this)}>{ this.props.profile.following ? menusContent[6] : menusContent[7]}</button>
										: ""
									}
								</div>
								: ""
							}

							<div className="navbar__btnlist">
								<a href={'/' + this.props.lang + "/company/" + this.props.profile.key + "/profile"} target="_blank"><button className="btn btn-default reverse">{menusContent[5]}</button></a>
							</div>

	          </div>
						{
							this.props.profile.links ?
		          <div className="col-md-4">
		            <div id="social_footer">
	                <ul>
	                    { this.props.profile.links.facebook ? <li><a href={this.props.profile.links.facebook} target="_blank"><i className="icon-facebook"></i></a></li> : ""}
	                    { this.props.profile.links.instagram ? <li><a href={this.props.profile.links.instagram} target="_blank"><i className="icon-instagram"></i></a></li> : "" }
	                    { this.props.profile.links.youtube ? <li><a href={this.props.profile.links.youtube} target="_blank"><i className="icon-youtube-play"></i></a></li> : "" }
	                    { this.props.profile.links.linkedin ? <li><a href={this.props.profile.links.linkedin} target="_blank"><i className="icon-linkedin"></i></a></li> : "" }
											{ this.props.profile.links.website ? <li><a href={this.props.profile.links.website} target="_blank"><i className="icon-website"></i></a></li> : "" }
	                </ul>
		            </div>
		          </div>
							: ""
						}
	        </div>
				</div>

				<BackgroundTemplate changeBannerImageWithTemplate={this.changeBannerImageWithTemplate.bind(this)}/>

				<div className="container">
					<div className="row" >
						<div className="col-md-4" style={{ paddingRight: "0px" }}>
							<div className="box_style_detail" style={{ marginTop: "16px" }}>

								{ this.props.profile.jobs && this.props.profile.jobs.length > 0 ? <div><CompanyPosition {...formState} /> <hr /></div> : "" }

								{ this.props.profile.employees && this.props.profile.formers && (this.props.profile.employees.length > 0 || this.props.profile.formers.length > 0) ? <div><CompanyPeople {...formState} /><hr /></div> : ""}

								{ this.props.editor || (this.props.profile.culture) ? <div><CompanyCulture {...formState} responsive={false} /><hr /></div> : ""}

								{ this.props.editor || (this.props.profile.benefits && this.props.profile.benefits.length > 0) ? <div><CompanyBenefit {...formState} responsive={false} /><hr /></div> : "" }

								{ this.props.editor || (this.props.profile.video) ? <div><CompanyVideo {...formState} responsive={false} /><hr /></div> : ""}

								{ this.props.profile.key ? <CompanyGallery triggerButton={this.triggerButton.bind(this)} feed={this.props.feed} {...formState} /> : "" }

								{ this.props.editor || (this.props.profile.address && this.props.profile.address.information) ? <div><CompanyLocation {...formState} responsive={false} triggerMap={this.triggerMap.bind(this)} map_id="googleMap1"  /><hr /></div> : "" }

								{ this.props.editor ? <div><CompanySocialLink {...formState} /><hr /></div> : "" }

								{ this.props.profile.key && this.props.profile.review.length > 0 ? <div><CompanySummaryReview {...formState} /><hr /></div> : "" }

								{ this.props.profile.key && this.props.profile.review.length > 0 ? <div><CompanyLastestReview {...formState} /><hr /></div> : "" }

							</div>
						</div>
						<div className="col-md-8">

	            <div className="row" >
	              <div className="col-md-12">
	                <div className='profile-card'>
	                  <div className="row">
	                    <div className="col-md-12">

												{ this.props.profile.jobs && this.props.profile.jobs.length > 0 ? <CompanyPosition {...formState} responsive={true} /> : "" }
												{this.props.profile.employees && this.props.profile.formers && (this.props.profile.employees.length > 0 || this.props.profile.formers.length > 0) ? <CompanyPeople {...formState} responsive={true} /> : ""}
												{ this.props.editor || this.props.profile.overview ? <CompanyOverview {...formState} responsive={true} /> : "" }
												{ this.props.editor || (this.props.profile.culture) ? <CompanyCulture {...formState} responsive={true} /> : "" }
												{ this.props.editor || (this.props.profile.benefits && this.props.profile.benefits.length > 0) ? <CompanyBenefit {...formState} responsive={true} />  : "" }

												<div id="more-info-detail" style={{ display: "none" }}>
													{ this.props.editor || (this.props.profile.video) ? <CompanyVideo {...formState} responsive={true} /> : "" }
													{ this.props.profile.key ? <CompanyGallery triggerButton={this.triggerButton.bind(this)} feed={this.props.feed} {...formState} responsive={true} /> : "" }
													{ this.props.editor || (this.props.profile.address && this.props.profile.address.information) ? <CompanyLocation {...formState} responsive={true} triggerMap={this.triggerMap.bind(this)} map_id="googleMap2" /> : "" }
													{ this.props.profile.key && this.props.profile.review.length > 0 ? <CompanySummaryReview  {...formState} responsive={true} /> : "" }
													{ this.props.profile.key && this.props.profile.review.length > 0 ? <CompanyLastestReview {...formState} responsive={true} /> : "" }
												</div>

												<div className="profile-responsive" style={{ marginBottom: "10px" }}>
				                 <div className="col-md-12">
				                   <div className="btn-cover" style={{ margin: "20px 10px" }}>
				                     <button onClick={this.toggleMoreInfo.bind(this)} className="btn btn-default" style={{ width: "100%", margin:"auto" }}>More Information</button>
				                   </div>
				                 </div>
				                </div>

											</div>
										</div>

									</div>

								</div>
							</div>

							{this.props.editor || this.props.profile.overview ? <CompanyOverview {...formState} responsive={false} /> : "" }

						  { this.props.profile.key &&
								(
									this.props.page !== 'company_image_profile' ?
										<FormFeedNewsFeed button_header_click={button_header_click} editor={this.props.editor} profile={this.props.profile} user={this.props.user} owner={!formState.other} feed={this.props.feed} feed_id={this.props.feed_id} language={this.props.lang} />
										:
										<FormFeedPhoto button_header_click={button_header_click} editor={this.props.editor} profile={this.props.profile} user={this.props.user} owner={!formState.other} feed={this.props.feed} language={this.props.lang} />
								)
							}

						</div>

					</div>
				</div>

			</div>
		);
	}
}
export default CompanyProfile;
