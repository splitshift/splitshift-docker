import React, { Component } from 'react';
import { Container } from 'flux/utils';
import { browserHistory } from 'react-router';

import FormUserProfileCard from './Form/User/ProfileCard';
import FormUserAbout from './Form/User/About';
import FormUserInterest from './Form/User/Interest';
import FormUserWorkExperience from './Form/User/WorkExperience';
import FormUserSkill from './Form/User/Skill';
import FormUserEducation from './Form/User/Education';
import FormUserCourse from './Form/User/Course';
import FormUserFollowing from './Form/User/Following';
import FormUserLanguage from './Form/User/Language';

import FormFeedNewsFeed from './Form/Feed/NewsFeed';
import FormFeedPhoto from './Form/Feed/Photo';

import BackgroundTemplate from './BackgroundTemplate';
import Message from './Message';

import UserProfileActionCreators from '../../actions/UserProfileActionCreators';
import ResourceActionCreators from '../../actions/ResourceActionCreators';
import MessageActionCreators from '../../actions/MessageActionCreators';
import SocialActionCreators from '../../actions/SocialActionCreators';

import JQueryTool from '../../tools/JQueryTool';
import configLang from '../../tools/configLang';

let button_header_click = false;

class UserProfile extends Component {

	changeBannerImage(evt) {
    UserProfileActionCreators.uploadBannerImage(this.props.user.token, evt.target.files[0]);
  }

	changeBannerImageWithTemplate(template_url, evt) {
		ResourceActionCreators.setDraft('template_banner', template_url);
		UserProfileActionCreators.changeBannerImageWithTemplate(this.props.user.token, { path: template_url });
		$("#myModal__background").modal('hide');
	}

	triggerButton(evt) {
		button_header_click = true;
		JQueryTool.animateScrollTo(this.props.page !== 'user_image_profile' ? '.newsfeed' : '.timeline-photo')
	}


	loadNewUser(profile_key) {
		UserProfileActionCreators.getUserProfile(profile_key, this.props.user.token);
		SocialActionCreators.getFollowers(profile_key);
	}

	componentDidMount() {
		let profile_key = "";
		button_header_click = false;

		// Load Resource
		UserProfileActionCreators.loadCompany();
		JQueryTool.moreinfo();

		if(typeof this.props.profile_key !== 'undefined')
			profile_key = this.props.profile_key;
		else if(typeof this.props.user.key !== 'undefined')
			profile_key = this.props.user.key;

		this.loadNewUser(profile_key)
	}

	shouldComponentUpdate(nextProps, nextState) {

		if(nextProps.profile_key && nextProps.profile_key !== this.props.profile_key)
			this.loadNewUser(nextProps.profile_key)
		else if(typeof nextProps.profile_key === 'undefined' && typeof this.props.profile_key !== 'undefined' && this.props.user.token)
			this.loadNewUser(this.props.user.key)

		return true;
	}


	toggleBackgroundMenu() {
		$(this.refs.menu__background).toggleClass("opened");
	}

	render() {
		let formState = {
			user: this.props.user,
			profile: this.props.profile,
			status: this.props.status,
			lang: this.props.lang,
			editor: this.props.editor,
			other: this.props.profile_key && this.props.profile_key != this.props.user.key
		};

		let backgroundStyle = {};
		let editorBannerContent = configLang[this.props.lang].user_profile.editor.upload.banner

		if (this.props.profile.banner) {
			backgroundStyle = {
				background: "url(" + this.props.profile.banner + ") no-repeat center center / cover",
			}
			if (this.props.profile.banner === '/images/loading.svg') backgroundStyle.backgroundSize = "80px 60px"
		}else {
			backgroundStyle = {
				backgroundColor: "#ccc"
			};
		}

		return (
			<div>
				<div style={backgroundStyle} data-natural-width="1400" data-natural-height="470">
					<div className={ this.props.editor ? "profile-cover profile-cover-hover" : "profile-cover" }>
						{ this.props.editor &&
							<div className="edit__cover">
				        <span onClick={this.toggleBackgroundMenu.bind(this)} className="edit__header">{editorBannerContent[0]}<i className="fa fa-camera"></i></span>

				        <div className='edit-menu'>
				          <div ref="menu__background" className='edit-menu__dropdown'>
				            <div className='edit-menu__item' onClick={JQueryTool.triggerUpload.bind(this, "uploadBannerImage")}>
				              <i className='fa fa-pencil'></i> {editorBannerContent[1]}
				            </div>
				            <div className='edit-menu__item' data-toggle="modal" data-target="#myModal__background">
				              <i className='fa fa-picture-o'></i> {editorBannerContent[2]}
				            </div>
				          </div>
				        </div>
								<input id="uploadBannerImage" type="file" onChange={this.changeBannerImage.bind(this)} style={{display: "none"}} />
				      </div>
						}
					</div>
				</div>

				<BackgroundTemplate changeBannerImageWithTemplate={this.changeBannerImageWithTemplate.bind(this)} />

				<div className="container">
					<div className="row">
						<div className="col-md-8 col-profile">
							<div className="row">
								<div className="col-md-12">
									<div className='profile-card'>
										<FormUserProfileCard {...formState} triggerButton={this.triggerButton.bind(this)} />
										<div className="row">
											<div className="col-md-12">
												{this.props.editor || this.props.profile.about ? <FormUserAbout {...formState} /> : ""}
												{this.props.editor || (this.props.profile.interests && this.props.profile.interests.length > 0)?<FormUserInterest {...formState} />:""}

												{this.props.editor || (this.props.profile.skills && this.props.profile.skills.length > 0) ?<FormUserSkill {...formState} responsive={true} />:"" }
												{this.props.editor || (this.props.profile.experiences && this.props.profile.experiences.length > 0) ? <FormUserWorkExperience {...formState} responsive={true} />:"" }
												{this.props.editor || (this.props.profile.educations && this.props.profile.educations.length > 0) ?<FormUserEducation {...formState} responsive={true}  />:"" }

												<div id="more-info-detail" style={{ display: "none" }}>
													{this.props.editor || (this.props.profile.courses && this.props.profile.courses.length > 0) ? <FormUserCourse {...formState} responsive={true} />:"" }
													{this.props.profile.followers.length > 0 ? <div><FormUserFollowing profile={this.props.profile} lang={formState.lang} responsive={true} /></div> : ""}
												  {this.props.editor || (this.props.profile.languages && this.props.profile.languages.length > 0) ?<FormUserLanguage {...formState} responsive={true} />:""}

												</div>

												<div className="profile-responsive" style={{ marginBottom: "10px" }}>
		                      <div className="col-md-12">
		                        <div className="btn-cover" style={{ margin: "0 10px"}}>
		                          <button id="more-info"  className="btn btn-default" style={{ width: "100%", margin: "auto" }}>More Information</button>
		                        </div>
		                      </div>
		                    </div>

											</div>
										</div>
									</div>
								</div>
							</div>

							{ this.props.page !== 'user_image_profile' && this.props.profile.key && !this.props.profile.loading ? <FormFeedNewsFeed button_header_click={button_header_click} editor={this.props.editor} profile={this.props.profile} user={this.props.user} owner={!formState.other} feed={this.props.feed} feed_id={this.props.feed_id} language={this.props.lang} /> : "" }

							{ this.props.page === 'user_image_profile' && this.props.profile.key && !this.props.profile.loading ? <FormFeedPhoto button_header_click={button_header_click} editor={this.props.editor} profile={this.props.profile} user={this.props.user} owner={!formState.other} feed={this.props.feed} language={this.props.lang} /> : "" }


						</div>
						<div className="col-md-4 col-detail">
							<div className="box_style_detail" style={{ marginTop:"15px" }}>

								{this.props.editor || (this.props.profile.experiences && this.props.profile.experiences.length > 0) ? <div><FormUserWorkExperience {...formState} responsive={false} /><hr /></div>: "" }

								{this.props.editor || (this.props.profile.skills && this.props.profile.skills.length > 0) ?<div><FormUserSkill {...formState} responsive={false} /><hr /></div>:""}

								{this.props.editor || (this.props.profile.educations && this.props.profile.educations.length > 0) ? <div><FormUserEducation {...formState} responsive={false}  /><hr /></div>:""}

								{this.props.editor || (this.props.profile.courses && this.props.profile.courses.length > 0) ?<div><FormUserCourse {...formState} responsive={false} /><hr /></div>:""}

								{this.props.profile.followers.length > 0 ? <div><FormUserFollowing lang={formState.lang} profile={this.props.profile} responsive={false} /><hr /></div> : "" }

								{this.props.editor || (this.props.profile.languages && this.props.profile.languages.length > 0) ? <div><FormUserLanguage {...formState} responsive={false} /><hr /> </div> : ""}

							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default UserProfile;
