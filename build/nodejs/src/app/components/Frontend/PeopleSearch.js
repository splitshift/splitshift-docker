import React, { Component } from 'react';
import { Link } from 'react-router';

import SearchActionCreators from '../../actions/SearchActionCreators';

import ConfigData from '../../tools/ConfigData';
import JQueryTool from '../../tools/JQueryTool';
import configLang from '../../tools/configLang'

class PeopleSearch extends Component {

  componentDidMount() {
    $('.ad__search').click(function() {
      $('.jobbar__detail').css('display','block');
      $(this).css('display','none');
    });

    SearchActionCreators.clearFilter();
    SearchActionCreators.People({});
  }

  updateFilter(name, evt) {
    let response = {};
    response[name] = evt.target.value;
    SearchActionCreators.updateFilter(response);
  }

  updateAdvanceFilter(name, value, evt) {
    let response = {};

    if(value == 'All')
      value = "";

    response[name] = value;
    SearchActionCreators.updateFilter(response);
  }

  onSearch(evt) {

    evt.preventDefault();

    $('html, body').animate({
        scrollTop: $(this.refs.jobs__people).offset().top - 200
    }, 1000);

    SearchActionCreators.People(this.props.search.filter);
  }

  loadMore(evt) {
    SearchActionCreators.People(this.props.search.filter, this.props.search.people.length / 15)
  }


  render() {
    let content = configLang[this.props.language].people_search
    let advancedContent = configLang[this.props.language].advanced_search

    return (
      <div>

        <div className="container">
          <div className="header__search people-search">
            <div className="people__title">
              <p><span>{content.intro}</span></p>
            </div>
            <div className="job__search">
              <form onSubmit={this.onSearch.bind(this)}>
                <div className="job__bar">
                  <label htmlFor="job">{content.people[0]}</label>
                  <input id="job" type="text" className="form-control" onChange={this.updateFilter.bind(this, 'keyword')} placeholder={content.people[1]} />
                </div>
                <div className="location__bar">
                  <label htmlFor="location">{content.location[0]}</label>
                  <input id="location" type="text" onChange={this.updateFilter.bind(this, 'location')} className="form-control" placeholder={content.location[1]} />
                  <button className="btn btn-default ad__search ad__search--desktop" type="button" onClick={JQueryTool.showAdvanceSearch.bind(this)}>{advancedContent.title}</button>
                </div>
                <div className="job__btn">
                  <button className="btn btn-default"><i className="fa fa-search"></i></button>
                  <button className="btn btn-default ad__search ad__search--responsive" type="button" onClick={JQueryTool.showAdvanceSearch.bind(this)}>{advancedContent.title}</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div className="jobbar__detail">
          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {this.props.search.filter.interest ? advancedContent.interest[ConfigData.InterestList().indexOf(this.props.search.filter.interest)] : advancedContent.interest_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
              {
                ConfigData.InterestList().map((interest, index) => (
                  <li key={index}><a href="#" onClick={this.updateAdvanceFilter.bind(this, 'interest', interest)}>{advancedContent.interest[index]}</a></li>
                ))
              }
            </ul>
          </div>

          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.work_exp ? advancedContent.workexp[this.props.search.filter.work_exp] : advancedContent.workexp_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
              {
                Object.keys(ConfigData.WorkExperienceList()).map((work, index) => (
                  <li key={index}><a href="#" onClick={this.updateAdvanceFilter.bind(this, 'work_exp', work)}>{advancedContent.workexp[work]}</a></li>
                ))
              }
            </ul>
          </div>
        </div>

        <div className="container" style={{ minHeight: "500px" }}>

          <div className="text--center">
            <h3><b><span5>{content.header}</span5></b></h3>
          </div>

          <hr className="hr-title" />

          <div ref="jobs__people" className="jobs__people">
            {
              this.props.search.people.map((p, index) => (
                <Link key={index} to={'/' + this.props.language + '/user/' + p.key}>
                  <div className="company__item">
                    <div className="job__pic">
                      <img src={p.profile_image ? p.profile_image : "/images/avatar.jpg"} />
                    </div>
                    <div className="search__detail">
                      <p><b><span>{p.profile.first_name + ' ' + p.profile.last_name}</span></b></p>
                      <p><span5>{ p.profile.occupation.position ? p.profile.occupation.position : " "}</span5></p>
                      <p><span6>{ p.profile.occupation.workplace ? p.profile.occupation.workplace : " "}</span6></p>

                    </div>
                    <div className="search__btn">
                      <Link to={'/th/user/' + p.key}>
                        <button className="btn btn-default">{content.see_profile}</button>
                      </Link>
                    </div>
                  </div>
                </Link>
              ))
            }

            <div className="btn__seemore">
            {
              this.props.search.load_more && <button onClick={this.loadMore.bind(this)} className="btn btn-default">{ this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</button>
            }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PeopleSearch;
