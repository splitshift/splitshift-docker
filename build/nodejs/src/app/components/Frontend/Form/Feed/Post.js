import React, { Component } from 'react';

import FeedActionCreators from '../../../../actions/FeedActionCreators';

import ConfigData from '../../../../tools/ConfigData';
import configLang from '../../../../tools/configLang'

const UserPrivacy = {public: 'Public', timeline: 'My Timeline', connected: 'Connection', employer: 'Employer', onlyme: 'Only Me'};
const CompanyPrivacy = {public: 'Public', timeline: 'My Timeline', employee: 'Employee'};

class FormFeedPost extends Component {

  updateDraft(name, evt) {
    let result = evt.target.value;

    if(name == 'video') {
      let youtube_embed = ConfigData.ConvertYoutubeToEmbed(result);
      if(youtube_embed != 'error')
        result = youtube_embed;
    }

    FeedActionCreators.setFeedDraft(name, result);
  }

  uploadImage(evt) {
    FeedActionCreators.uploadImage(this.props.user.token, evt.target.files[0]);
  }

  deleleImage(key, evt) {
    FeedActionCreators.deleteImage(this.props.user.token, key);
  }

  triggerUpload(evt) {
    $(this.refs.upload_feed_image).click()
  }

  cancelEditFeed(evt) {
    FeedActionCreators.cancelEditFeed();
  }

  editVideo() {
    FeedActionCreators.editVideo();
  }

  editImage() {
    FeedActionCreators.editImage();
  }

  editFeed(evt) {
    evt.preventDefault();

    let result = this.props.feed.draft;

    if(this.props.feed.add_photo)
      result.video = "";
    else if(this.props.feed.add_video) {
      result.deleted_photos = result.photos;
      result.photos = [];
      delete result.new_photos;
    }

    FeedActionCreators.editFeed(this.props.user.token, result);
  }

  createFeed(evt) {
    evt.preventDefault();
    let result = this.props.feed.draft;

    if(typeof result.privacy === 'undefined')
      result.privacy = "public";

    if(this.props.feed.add_video)
      delete result.photos;
    else if(this.props.feed.add_photo)
      delete result.video;

    this.refs.status.value = "";
    FeedActionCreators.setFeedDraft('owner', { key: this.props.user.key, name: this.props.user.type === 'Employee' ? this.props.user.first_name + ' ' + this.props.user.last_name : this.props.user.company_name , profile_image: this.props.user.profile_image });

    FeedActionCreators.postFeed(this.props.user.token, result);

  }

  render() {

    let FeedPrivacy = this.props.user.type === 'Employee' ? UserPrivacy : CompanyPrivacy
    let feedContent = configLang[this.props.language || 'th'].feed

    return (
      <article className='feed'>
        <form onSubmit={this.props.feed.edit_feed_id ? this.editFeed.bind(this) : this.createFeed.bind(this)}>

          { this.props.feed.edit_feed_id ? <div className='edit-close-feed' onClick={this.cancelEditFeed.bind(this)} /> : "" }

          <div className='feed__header'>
            <a className='feed__avatar' href='#'>
              <div style={{ background: "url(" + (this.props.user.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px" }} />
            </a>
            <div className='feed__title'>
              <div>{ this.props.user.rank === 'Employee' ? this.props.user.first_name + " " + this.props.user.last_name : this.props.user.company_name }</div>
              <div>Thailand</div>
            </div>
          </div>
          <div className='feed__content'>
            <textarea ref="status" onChange={this.updateDraft.bind(this, 'text')} className='feed__input' placeholder={this.props.user.type === 'Employee' ? feedContent.post.employee : feedContent.post.employer } defaultValue={this.props.feed.draft.text} />

            {
              this.props.feed.add_video ?
                <input className="form-control" type="text" onChange={this.updateDraft.bind(this, "video")} placeholder="place your video url" defaultValue={this.props.feed.draft.video} />
              :""
            }


            {
              this.props.feed.add_photo ?
              (
                <div className='photo-gallery'>
                  {
                    this.props.feed.draft.photos.map((photo, index) => (
                      <div key={index} className='photo-gallery__item'>
                        <div className='photo-gallery__overlay'></div>
                        <i className='photo-gallery__remove fa fa-remove' onClick={this.deleleImage.bind(this, index)}></i>
                        <div className='photo-gallery__image'  style={{ backgroundImage: "url(" + (photo ? photo.substring(6) : "") + ")" }}></div>
                      </div>
                    ))
                  }

                  <div className='photo-gallery__item' onClick={this.triggerUpload.bind(this)}>
                    <div className='photo-gallery__image'  style={{ backgroundImage: "url(/images/add-icon.png)" }}></div>
                  </div>
                </div>
              )

              : ""
            }

          </div>
          <div className='feed__actions'>
            <button type="button" className='feed__button' onClick={this.editImage.bind(this)}><i className='fa fa-picture-o'></i>{feedContent.card.type[0]}</button>
            <input type="file" ref="upload_feed_image" onChange={this.uploadImage.bind(this)} style={{display: "none"}} />
            <button type="button" className='feed__button' onClick={this.editVideo.bind(this)}><i className='fa fa-video-camera'></i>{feedContent.card.type[1]}</button>
            <div className="feed__post">
              <button className='feed__button primary'><i className='fa fa-bullhorn'></i>{this.props.feed.edit_feed_id ? feedContent.post.action[1] : feedContent.post.action[0] }</button>
              <div className="feed__privacy">
                <div className="privacy-select">
                  <select className="form-control" onChange={this.updateDraft.bind(this, 'privacy')} defaultValue={this.props.feed.draft.privacy}>
                    {
                      Object.keys(FeedPrivacy).map((key, index) => (
                        <option key={index} value={key}>{feedContent.status[key]}</option>
                      ))
                    }
                  </select>
                </div>
               </div>
            </div>
          </div>
        </form>
      </article>
    );
  }
}

export default FormFeedPost;
