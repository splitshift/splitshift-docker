import React, { Component } from 'react';

import FeedActionCreators from '../../../../actions/FeedActionCreators';

import JQueryTool from '../../../../tools/JQueryTool';

class FormFeedPhoto extends Component {

  componentDidMount() {
    if(this.props.button_header_click)
      JQueryTool.animateScrollTo(this.refs.photos_feed)

    FeedActionCreators.getPhotosFeed(this.props.profile.key, this.props.user.token);
    JQueryTool.popupGallery();
  }

  render() {
    return (
      <div ref="photos_feed" className="timeline-photo" >
        <div className="timeline-photo__tab">
          <div className="row" style={{ margin: "0px 1px" }}>
            <div className="col-md-3">
            <h3><b><span><i className="fa fa-picture-o"></i> { this.props.language === 'th' ? 'รูปภาพ' : 'Photos' }</span></b></h3>
            </div>
            {/*
              this.props.editor ?
              <div className="col-md-9">
                <div className="btn--manage">
                  <button className='btn btn-default reverse'>Create Album</button>{' '}
                  <button className='btn btn-default reverse'><i className="fa fa-pencil"></i></button>
                </div>
              </div>
              : ""
            */}
          </div>
        </div>

        <div className="timeline-photo__pic">
          <div className="row" style={{ margin: "0px 10px" }}>
            <div ref="photo_timeline" className="popup-gallery">
            {
              this.props.feed.photos.slice(0, 9).map((photo, index) => (
                <a key={index} href={photo.path.substring(6)}>
                  <div className="col-md-3" >
                    <div className="photo__pic" style={{ background: "url(" + ( photo.path ? photo.path.substring(6) : "" ) + ") no-repeat center center", backgroundSize: "cover" }}>
                      <div className="photo--detail">
                        <button className='btn btn-default reverse detail--photo'><i className="fa fa-pencil"></i></button>
                      </div>
                    </div>
                  </div>
                </a>
              ))
            }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FormFeedPhoto;
