import React, { Component } from 'react';
import { Link } from 'react-router';

import FeedActionCreators from '../../../../actions/FeedActionCreators';

import JQueryTool from '../../../../tools/JQueryTool';

class FormFeedComment extends Component {

  onReply(evt) {
    FeedActionCreators.onReply(this.props.comment);
  }

  onPress(evt) {
    if(evt.charCode == 13 || evt.keyCode == 13)
    this.postReplyComment();
  }

  postReplyComment(evt) {
    let reply = {
      "user_key": this.props.feed.owner.key,
      "feed_id": this.props.feed._id,
      "comment_id": this.props.comment_id,
      "reply": this.refs.reply.value
    };


    this.refs.reply.value = "";
    FeedActionCreators.setFeedDraft('feed_index', this.props.feed_index);
    FeedActionCreators.setFeedDraft('comment_index', this.props.comment_index);
    FeedActionCreators.postReplyComment(this.props.user.token, reply);

  }

  deleteComment(evt) {

    let feed = {
      user_key: this.props.feed.owner.key,
      feed_id: this.props.feed._id
    };


    FeedActionCreators.deleteCommentFeed(this.props.user.token, this.props.comment, feed);
  }

  deleteReply(reply, evt) {

    let feed_info = {
      user_key: this.props.feed.owner.key,
      feed_id: this.props.feed._id,
      comment_id: this.props.comment._id
    };

    FeedActionCreators.deleteReply(this.props.user.token, reply, feed_info);

  }

  render() {
    return (
      <div className='feed__comment'>
        <div className="comment__pic">
          <Link to={this.props.comment.owner.user_type == 'Employee' ? "/th/user/" + this.props.comment.owner.key : "/th/company/" + this.props.comment.owner.key }>
            <div style={{ background: "url(" + (this.props.comment.owner.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px" }} />
          </Link>
        </div>
        <div>
          <div>
           {this.props.comment.owner.name}{' '}
           {
             this.props.user.key == this.props.comment.owner.key ?
             <a onClick={this.deleteComment.bind(this)} href="javascript:void(0)">Delete</a>
             : ""
           }
          </div>
          <div>
            {this.props.comment.comment} <br />
            <span style={{ color: "#999"}}>{JQueryTool.changeDateAgo(this.props.comment.date)} • {this.props.comment.replies.length} {this.props.comment.replies.length < 2 ? 'Reply' : 'Replies'}</span>
          </div>
        </div>

        { typeof this.props.user.token !== 'undefined' && this.props.comment_id != this.props.comment._id ? <a onClick={this.onReply.bind(this)} className='feed__reply'>Reply</a> : "" }

        {
          this.props.comment_id == this.props.comment._id ?
            <div className='feed__comment'>
              <div className="comment__pic">
                <div style={{ background: "url(" + (this.props.user.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px", marginLeft: "0px"  }} />
              </div>
              <div>
                <input ref="reply" onKeyPress={this.onPress.bind(this)} type='text' placeholder='Write a reply' />
              </div>
              <div className="feed__send" onClick={this.postReplyComment.bind(this)}>
                <i className="fa fa-paper-plane"></i>
              </div>
            </div>
          : ""
        }

        {
          this.props.comment.replies.map((reply, index) => (
            <div key={index} className='feed__comment'>
              <div className="comment__pic">
                <Link to={reply.owner.user_type == 'Employee' ? "/th/user/" + reply.owner.key : "/th/company/" + reply.owner.key }>
                  <div style={{ background: "url(" + (reply.owner.profile_image  || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px" }} />
                </Link>
              </div>

              <div>
                {
                  this.props.user.key == reply.owner.key ? <div>{reply.owner.name} <a href="javascript:void(0)" onClick={this.deleteReply.bind(this, reply)}>Delete</a></div> : ""
                }

                <div>
                  {reply.reply} <br />
                  <span style={{ color: "#999"}}>{JQueryTool.changeDateAgo(this.props.comment.date)}</span>
                </div>
              </div>
            </div>
          ))
        }

      </div>
    )
  }
}

export default FormFeedComment;
