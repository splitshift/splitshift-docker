import React, { Component } from 'react';
import FeedActionCreators from '../../../../actions/FeedActionCreators';
import configLang from '../../../../tools/configLang'

class FormFeedPostComment extends Component {


  postComment(evt) {
    evt.preventDefault()
    let comment = {
      "user_key": this.props.feed.owner.key,
      "feed_id": this.props.feed._id,
      "comment": this.refs.comment.value
    }
    this.refs.comment.value = ""
    FeedActionCreators.setFeedDraft('feed_index', this.props.feed_index)
    FeedActionCreators.postCommentFeed(this.props.user.token, comment)
  }

  render() {
    let feedContent = configLang[this.props.language].feed
    return (
      <form onSubmit={this.postComment.bind(this)}>
        <div className='feed__comment'>
          <div className="comment__pic">
            <div style={{ background: "url(" + (this.props.user.profile_image   || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px", marginLeft: "0px" }} />
          </div>
          <div>
            <input ref="comment" type='text' placeholder={feedContent.card.comment} />
          </div>
          <div className="feed__send" onClick={this.postComment.bind(this)}>
            <i className="fa fa-paper-plane"></i>
          </div>
        </div>
      </form>
    )
  }
}


export default FormFeedPostComment;
