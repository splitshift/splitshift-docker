import React, { Component } from 'react';

import FeedActionCreators from '../../../../actions/FeedActionCreators';

import JQueryTool from '../../../../tools/JQueryTool'

import FormFeedPost from './Post';
import FormFeedCard from './Card';

let setOwner = false;

class FormFeedNewsFeed extends Component {

  componentDidMount() {
    if(this.props.button_header_click)
      JQueryTool.animateScrollTo(this.refs.news_feed)

    FeedActionCreators.getFeed(this.props.profile.key, this.props.user.token);
  }

  componentDidUpdate() {
    if(!setOwner && this.props.user.complete) {
      FeedActionCreators.setOwner(this.props.user);
      setOwner = true;
    }
  }

  componentWillUnmount() {
    FeedActionCreators.resetFeed()
  }

  loadMore(evt) {
    FeedActionCreators.getFeed(this.props.profile.key, this.props.user.token, this.props.feed.feeds.length / 10);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(!nextProps.feed.deleting && this.props.feed.deleting)
      FeedActionCreators.getFeed(this.props.profile.key, this.props.user.token);

    if((this.props.profile.key != nextProps.profile.key) || (nextProps.user.token && typeof this.props.user.token === 'undefined'))
      FeedActionCreators.getFeed(nextProps.profile.key, nextProps.user.token);

    return true;

  }

  render() {

    let feedState = {
      profile: this.props.profile,
      user: this.props.user,
      feed: this.props.feed,
      language: this.props.language
    }

    return (
      <div ref="news_feed" className="newsfeed" style={{ marginTop: "16px",  marginBottom: "16px"  }}>

        { !this.props.feed.edit_feed_id && this.props.profile.key === this.props.user.key && typeof this.props.feed_id === 'undefined' ? <FormFeedPost {...feedState} /> : "" }

        {
          this.props.feed.feeds.map((f, index) => (this.props.feed.edit_feed_id != null && f._id == this.props.feed.edit_feed_id ?
            <FormFeedPost key={index} {...feedState} />
            :
            (typeof this.props.feed_id === 'undefined' || (this.props.feed_id && this.props.feed_id == f._id)) ? <FormFeedCard language={this.props.language} key={index} user={this.props.user} feed={f} index={index} comment_id={this.props.feed.comment_for_reply_id} /> : ""
          ))
        }

        {
          this.props.feed.load_more ?
          <div className="btn__seemore">
            <button onClick={this.loadMore.bind(this)} className="btn btn-default">{ this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</button>
          </div>
          : ""
        }

      </div>
    );
  }
}

export default FormFeedNewsFeed;
