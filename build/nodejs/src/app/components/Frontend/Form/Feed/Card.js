import React, { Component } from 'react';
import { Link } from 'react-router';

import FeedActionCreators from '../../../../actions/FeedActionCreators';

import Comment from './Comment';
import PostComment from './PostComment';

import JQueryTool from '../../../../tools/JQueryTool'
import configLang from '../../../../tools/configLang'
import ConfigData from '../../../../tools/ConfigData'

const API_URL = () => {
  return window.location.origin;
}

class FormFeedCard extends Component {

  componentDidMount() {
    JQueryTool.popupGallery();
  }

  likeFeed(evt) {
    FeedActionCreators.setFeedDraft('like', this.props.index);
    FeedActionCreators.setFeedDraft('owner_key', this.props.user.key);
    FeedActionCreators.putLikeFeed(this.props.user.token, this.props.feed);
  }

  chooseEditFeed(evt) {
    $(".edit-menu__dropdown").removeClass('opened');
    FeedActionCreators.chooseEditFeed(this.props.feed, this.props.index);
  }

  deleteFeed(evt) {
    $(".edit-menu__dropdown").removeClass('opened');

    if(this.props.user.type === 'Admin')
      FeedActionCreators.deleteBuzzFeed(this.props.user.token, this.props.feed);
    else
      FeedActionCreators.deleteFeed(this.props.user.token, this.props.feed);

  }

  render() {
    let isBuzz = typeof this.props.buzz !== 'undefined' ? true : false
    let isPublic = this.props.feed.privacy === 'public'
    let feedContent = configLang[this.props.language].feed

    return (
      <article className={ isBuzz ? 'feed feed__card' : 'feed' }>

        {
          !isBuzz && isPublic ?
            <div className="feed__buzz">
              <span1>Buzz</span1>
              <img src="/images/company/bookmark-ribbon.png" />
            </div>
          : ""
        }


        {
            typeof this.props.user.key !== 'undefined' && (this.props.feed.owner.key == this.props.user.key || (this.props.user.type === 'Admin' && typeof this.props.buzz !== 'undefined')) ?
            <div className='edit-menu' onClick={JQueryTool.clickEditMenu.bind(this)}>
              <div className='edit-menu__dropdown'>
                {
                  this.props.feed.owner.key == this.props.user.key ?
                    <div className='edit-menu__item'><a href="javascript:void(0)" onClick={this.chooseEditFeed.bind(this)}><i className='fa fa-pencil'></i> Edit post</a></div>
                  : ""
                }

                <div className='edit-menu__item'><a href="javascript:void(0)" onClick={this.deleteFeed.bind(this)}><i className='fa fa-trash'></i> Remove post</a></div>
              </div>
            </div>
          : ""
        }

        <div className='feed__header'>
          <Link className='feed__avatar' to={this.props.feed.owner.user_type === 'Employee' ? "/th/user/" + this.props.feed.owner.key : this.props.feed.owner.user_type !== 'Admin' ? "/th/company/" + this.props.feed.owner.key : "/" }>
            <div style={{ background: "url(" + (this.props.feed.owner.profile_image || "/images/avatar-image.jpg") + ") no-repeat top center / cover", height: "48px" }} />
          </Link>
          <div className='feed__title'>
            <div>{this.props.feed.owner.name}</div>
            <div>
              <Link to={this.props.feed.owner.user_type == 'Employee' ? "/th/user/" + this.props.feed.owner.key + "/feed/" + this.props.feed._id : "/th/company/" + this.props.feed.owner.key + "/feed/" + this.props.feed._id }>{JQueryTool.changeDateAgo(this.props.feed.date)}</Link>{' '}
              •{' '}
              {feedContent.status[this.props.feed.privacy]}
            </div>
          </div>
        </div>
        <div className='feed__content'>
          <div className='feed__paragraph'>
            {this.props.feed.text}
          </div>

          <div className="popup-gallery">
          {
            this.props.feed.photos &&
            this.props.feed.photos.map((photo, index) => {
              let notLastPhoto = (index === 3 && this.props.feed.photos.length > 4)
              return (
                <a className="photo__card" key={index} href={photo ? photo.substring(6) : ""} title={this.props.feed.text} style={{ display: (index > 3 && 'none') }}>
                  { notLastPhoto && <span>+{this.props.feed.photos.length - 4}</span> }
                  <img className={index > 0 ? 'feed__album' : notLastPhoto ? 'feed__photo feed__photo--last' : 'feed__photo'} src={index == 0 && photo ? photo.substring(6) : ""} style={{background: (notLastPhoto ? "linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5))," : "") + "url('" + (photo ? photo.substring(6) : "") + "')  top center no-repeat", backgroundSize: "cover"}}/>
                </a>
              )
            })
          }
          </div>

          { this.props.feed.video ? <iframe className='feed__clip' height="315" src={this.props.feed.video} frameBorder='0' allowFullScreen /> : "" }
        </div>
        <div className='feed__actions'>

          {
            typeof this.props.user.token !== 'undefined' &&
            <span4>
              <button className='feed__button' onClick={this.likeFeed.bind(this)} style={{color: this.props.feed.likes.indexOf(this.props.user.key) != -1 ? "#f60" : "" }}>
                <i className='fa fa-thumbs-up' style={{color: this.props.feed.likes.indexOf(this.props.user.key) != -1 ? "#f60" : "" }}></i>{feedContent.card.action[0]}
              </button>

              <button className='feed__button'
                data-href={"https://www.facebook.com/sharer/sharer.php?app_id=235025906884083&sdk=joey&u=" + API_URL() + (this.props.feed.owner.user_type === 'Employee' ? "/th/user/" + this.props.feed.owner.key + "/feed/" + this.props.feed._id : "/th/company/" + this.props.feed.owner.key + "/feed/" + this.props.feed._id) + "&display=popup&ref=plugin&src=share_button"}
                onClick={(evt) => { return !window.open($(evt.currentTarget).data('href'), 'Facebook', 'width=640,height=580') }}>
                <i className='fa fa-share'></i>{feedContent.card.action[1]}
              </button>

            </span4>
          }

          <div className='feed__counter'>{this.props.feed.comments.length} {feedContent.card.action[2]}{ this.props.feed.comments.length > 1 && this.props.language === 'en' ? 's' : ''} • {this.props.feed.likes.length} {feedContent.card.action[3]}{ this.props.feed.likes.length > 1 && this.props.language === 'en' ? 's' : ''}</div>
        </div>

        <div className='feed__tray'>
          {
            typeof this.props.user.token !== 'undefined' &&
            <PostComment language={this.props.language} user={this.props.user} feed={this.props.feed} feed_index={this.props.index} />
          }

          {
            this.props.feed.comments.map((comment, index) =>
              <Comment key={index} user={this.props.user} feed={this.props.feed} feed_index={this.props.index} comment_id={this.props.comment_id} comment_index={index} comment={comment} />
            )
          }
        </div>

      </article>
    );
  }
}

export default FormFeedCard;
