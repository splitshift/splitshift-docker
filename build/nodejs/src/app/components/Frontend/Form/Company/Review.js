import React, { Component } from 'react';

import CompanyProfileActionCreators from '../../../../actions/CompanyProfileActionCreators';
import ResourceActionCreators from '../../../../actions/ResourceActionCreators';

let overall_calcurate = false;

class CompanyReview extends Component {

  onUpdateReview(name, value, evt) {
    overall_calcurate = false;
    ResourceActionCreators.setDraft(name, value);
  }

  sendReview(evt) {
    let response = this.props.profile.draft;
    response.to = this.props.profile.key;

    if(this.refs.agreement.checked)
      CompanyProfileActionCreators.reviewCompany(this.props.user.token, response);
    else
      alert("Please accept term of use");
  }

  componentDidUpdate(evt) {
    if(!overall_calcurate &&
       typeof this.props.profile.draft !== 'undefined' &&
       typeof this.props.profile.draft.work !== 'undefined' &&
       typeof this.props.profile.draft.benefit !== 'undefined' &&
       typeof this.props.profile.draft.career !== 'undefined' &&
       typeof this.props.profile.draft.recommend !== 'undefined') {
      ResourceActionCreators.setDraft('overall', (this.props.profile.draft.work + this.props.profile.draft.benefit + this.props.profile.draft.career + this.props.profile.draft.recommend)/4 );
      overall_calcurate = true;
    }
  }


  componentDidMount() {
    // click review1
    $('#myModal__review .first-item1').click(function() {
        $("i.first-item1").prevAll().addClass("reviewed");
        $("i.first-item1").nextAll().removeClass("reviewed");
        $("i.first-item1").addClass("reviewed");
    });

    $('#myModal__review .second-item1').click(function() {
        $("i.second-item1").prevAll().addClass("reviewed");
        $("i.second-item1").nextAll().removeClass("reviewed");
        $("i.second-item1").addClass("reviewed");
    });

    $('#myModal__review .third-item1').click(function() {
        $("i.third-item1").prevAll().addClass("reviewed");
        $("i.third-item1").nextAll().removeClass("reviewed");
        $("i.third-item1").addClass("reviewed");
    });

    $('#myModal__review .forth-item1').click(function() {
        $("i.forth-item1").prevAll().addClass("reviewed");
        $("i.forth-item1").nextAll().removeClass("reviewed");
        $("i.forth-item1").addClass("reviewed");
    });

    $('#myModal__review .fifth-item1').click(function() {
        $("i.fifth-item1").prevAll().addClass("reviewed");
        $("i.fifth-item1").addClass("reviewed");
    });

    // click review2
    $('#myModal__review .first-item2').click(function() {
        $("i.first-item2").prevAll().addClass("reviewed");
        $("i.first-item2").nextAll().removeClass("reviewed");
        $("i.first-item2").addClass("reviewed");
    });

    $('#myModal__review .second-item2').click(function() {
        $("i.second-item2").prevAll().addClass("reviewed");
        $("i.second-item2").nextAll().removeClass("reviewed");
        $("i.second-item2").addClass("reviewed");
    });

    $('#myModal__review .third-item2').click(function() {
        $("i.third-item2").prevAll().addClass("reviewed");
        $("i.third-item2").nextAll().removeClass("reviewed");
        $("i.third-item2").addClass("reviewed");
    });

    $('#myModal__review .forth-item2').click(function() {
        $("i.forth-item2").prevAll().addClass("reviewed");
        $("i.forth-item2").nextAll().removeClass("reviewed");
        $("i.forth-item2").addClass("reviewed");
    });

    $('#myModal__review .fifth-item2').click(function() {
        $("i.fifth-item2").prevAll().addClass("reviewed");
        $("i.fifth-item2").addClass("reviewed");
    });

    // click review3
    $('#myModal__review .first-item3').click(function() {
        $("i.first-item3").prevAll().addClass("reviewed");
        $("i.first-item3").nextAll().removeClass("reviewed");
        $("i.first-item3").addClass("reviewed");
    });

    $('#myModal__review .second-item3').click(function() {
        $("i.second-item3").prevAll().addClass("reviewed");
        $("i.second-item3").nextAll().removeClass("reviewed");
        $("i.second-item3").addClass("reviewed");
    });

    $('#myModal__review .third-item3').click(function() {
        $("i.third-item3").prevAll().addClass("reviewed");
        $("i.third-item3").nextAll().removeClass("reviewed");
        $("i.third-item3").addClass("reviewed");
    });

    $('#myModal__review .forth-item3').click(function() {
        $("i.forth-item3").prevAll().addClass("reviewed");
        $("i.forth-item3").nextAll().removeClass("reviewed");
        $("i.forth-item3").addClass("reviewed");
    });

    $('#myModal__review .fifth-item3').click(function() {
        $("i.fifth-item3").prevAll().addClass("reviewed");
        $("i.fifth-item3").addClass("reviewed");
    });

    // click overall review
    $('#myModal__review .first-item4').click(function() {
        $("i.first-item4").prevAll().addClass("reviewed");
        $("i.first-item4").nextAll().removeClass("reviewed");
        $("i.first-item4").addClass("reviewed");
    });

    $('#myModal__review .second-item4').click(function() {
        $("i.second-item4").prevAll().addClass("reviewed");
        $("i.second-item4").nextAll().removeClass("reviewed");
        $("i.second-item4").addClass("reviewed");
    });

    $('#myModal__review .third-item4').click(function() {
        $("i.third-item4").prevAll().addClass("reviewed");
        $("i.third-item4").nextAll().removeClass("reviewed");
        $("i.third-item4").addClass("reviewed");
    });

    $('#myModal__review .forth-item4').click(function() {
        $("i.forth-item4").prevAll().addClass("reviewed");
        $("i.forth-item4").nextAll().removeClass("reviewed");
        $("i.forth-item4").addClass("reviewed");
    });

    $('#myModal__review .fifth-item4').click(function() {
        $("i.fifth-item4").prevAll().addClass("reviewed");
        $("i.fifth-item4").addClass("reviewed");
    });
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.profile.reviewing && this.props.profile.reviewing) {
      alert("Review Company Complete!");
      $("#myModal__review").modal('hide');
      CompanyProfileActionCreators.getProfile(this.props.profile.key, this.props.user.token);
    }

    return true;
  }

  render() {
    return (
      <div className="modal fade" id="myModal__review" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span5 aria-hidden="true">&times;</span5>
              </button>
              <div className="modal-title">
                  <b><span>Rate this Company</span></b>
              </div>
            </div>
            <div className="modal-body">
              <b><p>It only takes a minute!</p>
              <p>Your anonymous review will help other job seekers.</p></b>
              <div className="review--content">
                <p>1. How do you rate the work environment and company culture ?</p>
                <div>
                    <i className="fa fa-circle pointer__review first-item1" onClick={this.onUpdateReview.bind(this, 'work', 1)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review second-item1" onClick={this.onUpdateReview.bind(this, 'work', 2)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review third-item1" onClick={this.onUpdateReview.bind(this, 'work', 3)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review forth-item1" onClick={this.onUpdateReview.bind(this, 'work', 4)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review fifth-item1" onClick={this.onUpdateReview.bind(this, 'work', 5)} aria-hidden="true"></i>
                </div>
              </div>
              <div className="review--content">
                <p>2. How do you rate the compensation and benefits ?</p>
                <div>
                    <i className="fa fa-circle pointer__review first-item2" onClick={this.onUpdateReview.bind(this, 'benefit', 1)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review second-item2" onClick={this.onUpdateReview.bind(this, 'benefit', 2)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review third-item2" onClick={this.onUpdateReview.bind(this, 'benefit', 3)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review forth-item2" onClick={this.onUpdateReview.bind(this, 'benefit', 4)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review fifth-item2" onClick={this.onUpdateReview.bind(this, 'benefit', 5)} aria-hidden="true"></i>
                </div>
              </div>
              <div className="review--content">
                <p>3. How do you rate the career opportunities within this company ?</p>
                <div>
                    <i className="fa fa-circle pointer__review first-item3" onClick={this.onUpdateReview.bind(this, 'career', 1)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review second-item3" onClick={this.onUpdateReview.bind(this, 'career', 2)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review third-item3" onClick={this.onUpdateReview.bind(this, 'career', 3)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review forth-item3" onClick={this.onUpdateReview.bind(this, 'career', 4)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review fifth-item3" onClick={this.onUpdateReview.bind(this, 'career', 5)} aria-hidden="true"></i>
                </div>
              </div>
              <div className="review--content">
                <p>4. Would you recommend this company to your friends ?</p>
                <div>
                    <i className="fa fa-circle pointer__review first-item4" onClick={this.onUpdateReview.bind(this, 'recommend', 1)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review second-item4" onClick={this.onUpdateReview.bind(this, 'recommend', 2)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review third-item4" onClick={this.onUpdateReview.bind(this, 'recommend', 3)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review forth-item4" onClick={this.onUpdateReview.bind(this, 'recommend', 4)} aria-hidden="true"></i>
                    <i className="fa fa-circle pointer__review fifth-item4" onClick={this.onUpdateReview.bind(this, 'recommend', 5)} aria-hidden="true"></i>
                </div>
              </div>
              <div className="review--content" style={{ marginTop: "20px" }}>
                <p><b>Overall Rating</b></p>
                <div>
                  {
                    typeof this.props.profile.draft !== 'undefined' && typeof this.props.profile.draft.overall !== 'undefined' ?
                      [1, 2, 3, 4, 5].map((value, index) => (
                        <i key={index} className="fa fa-thumbs-up overall__review" aria-hidden="true" style={{color: value <= this.props.profile.draft.overall ? "#f60" : "" }}></i>
                      ))
                    : [1, 2, 3, 4, 5].map((value, index) => (
                        <i key={index} className="fa fa-thumbs-up overall__review" aria-hidden="true"></i>
                      ))
                  }

                </div>
              </div>
              <div className="review--policy">
                <div className="modal__checkbox">
                  <div className="checkbox--remember">
                    <input type="checkbox" ref="agreement" name="remember" />I agree to the splitshift's <span>Term of Use</span>. This review of my experience at my current or former employer is truthful.
                  </div>
                </div>
              </div>

            </div>

            <div className="modal-footer">
              <div>
                <a href="javascript:void(0)" onClick={this.sendReview.bind(this)}><button className="btn btn-default float">Submit Review</button></a>
                <a href="#"><button className="btn btn-default float reviewform__btn"  data-dismiss="modal" >Cancel</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CompanyReview;
