import React, { Component } from 'react';

import ResourceActionCreators from '../../../../actions/ResourceActionCreators';
import CompanyProfileActionCreators from '../../../../actions/CompanyProfileActionCreators';
import configLang from '../../../../tools/configLang'

const responsiveStyle = { marginTop: "16px", marginBottom: "16px" };

let expanding = 0;

class CompanyOverview extends Component {

  updateDraft(name, evt) {

    if(!this.props.status.edit_company_overview) {
      ResourceActionCreators.setEditor('edit_company_overview', true);
      $(this.refs.overview).expanding();
      $(this.refs.overview)[0].focus();
    }

    ResourceActionCreators.setDraft(name, evt.target.value);
  }

  updateOverview(evt) {
    evt.preventDefault();
    CompanyProfileActionCreators.updateOverview(this.props.user.token, this.props.profile.draft);
  }

  contentRender(text) {

    let draftText = text;
    draftText = draftText.replace(/\n/g, "<br/>");

    return (
      <p dangerouslySetInnerHTML={{__html: draftText }} />
    );

  }

  editorRender() {
    return (
      this.props.profile && typeof this.props.profile.overview !== "undefined"?
        <form onSubmit={this.updateOverview.bind(this)}>
          <textarea ref="overview" id="company_overview" onChange={this.updateDraft.bind(this, "overview")} className="form-control form-control--mini" defaultValue={this.props.profile.overview} />
          <button className="btn btn-mini btn-default">Save</button>
        </form>
        :""
    );
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.status.edit_company_overview && this.props.status.edit_company_overview)
      CompanyProfileActionCreators.getProfile(this.props.user.key);

    return true;

  }

  componentDidMount() {
    expanding = 0;
  }

  componentDidUpdate() {

    if(expanding < 2 && this.refs.overview) {
      $(this.refs.overview).expanding()
      expanding++;
    }

  }

  render() {
    let header = configLang[this.props.lang].company_profile.header
    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":"box_style_detail"} style={this.props.responsive?{}:responsiveStyle}>
         <h3 className="inner">{header.overview} </h3>
         <div className="row">
            { this.props.editor ? this.editorRender() : this.contentRender(this.props.profile.overview) }
         </div>
      </div>
    );
  }

}

export default CompanyOverview;
