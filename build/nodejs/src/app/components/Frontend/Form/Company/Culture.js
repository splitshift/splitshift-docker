import React, { Component } from 'react';

import ResourceActionCreators from '../../../../actions/ResourceActionCreators';
import CompanyProfileActionCreators from '../../../../actions/CompanyProfileActionCreators';

import configLang from '../../../../tools/configLang'

let expanding = 0;

class CompanyCulture extends Component {

  updateDraft(name, evt) {

    if(!this.props.status.edit_company_culture) {
      ResourceActionCreators.setEditor('edit_company_culture', true);
      $(this.refs.culture).expanding();
      $(this.refs.culture)[0].focus();
    }

    ResourceActionCreators.setDraft(name, evt.target.value);

  }

  updateCulture(evt) {
    evt.preventDefault();
    CompanyProfileActionCreators.updateCulture(this.props.user.token, this.props.profile.draft);
  }

  contentRender(text) {

    let draftText = text;
    draftText = draftText.replace(/\n/g, "<br/>");

    return (
      <div className="row">
        <p dangerouslySetInnerHTML={{__html: draftText }} />
      </div>
    );
  }

  editorRender() {
    return (
      this.props.profile && typeof this.props.profile.culture !== "undefined"?
      <form onSubmit={this.updateCulture.bind(this)}>
        <textarea ref="culture" className="form-control form-control--mini" defaultValue={this.props.profile.culture} onChange={this.updateDraft.bind(this, 'culture')} />
        <button className="btn btn-mini btn-default">Save</button>
      </form>:""
    );
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.status.edit_company_culture && this.props.status.edit_company_culture)
      CompanyProfileActionCreators.getProfile(this.props.user.key);

    return true;

  }

  componentDidMount() {
    expanding = 0;
  }

  componentDidUpdate() {
    if(expanding < 2 && this.refs.culture) {
      $(this.refs.culture).expanding()
      expanding++;
    }
  }

  render() {
    let header = configLang[this.props.lang].company_profile.header

    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
        <h3 className="inner">{header.culture}</h3>
        {this.props.editor ? this.editorRender() : this.contentRender(this.props.profile.culture) }
      </div>
    );
  }
}

export default CompanyCulture;

/*
<div className="text--expand">
  <p>
    This reflects our core brand values of Crafted Comfort, Authentic Culture and Destination Discovery; a belief that we invite our guests to experience and ‘Live the Remarkable’ at our hotels and residences.
  </p>
</div>
<a className="btn--expand">Read more...</a>
*/
