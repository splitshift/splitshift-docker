import React, { Component } from 'react';
import JobActionCreators from '../../../../actions/JobActionCreators';

let userComplete = false;
class CompanyJob extends Component {

  updateDraft(name, evt) {
    let response = {};
    response[name] = evt.target.value;

    JobActionCreators.updateApplyJobDraft(response);
  }

  uploadFile(evt) {
    JobActionCreators.uploadFileApplyJob(this.props.user.token, evt.target.files[0]);
  }

  sendApplyJob(evt) {
    let jobForm = this.props.job.apply_draft;
    let allFilePath = jobForm.files ? jobForm.files.map((file, key) => {
      return file.path;
    }) : [];

    jobForm.job_id = this.props.profile.jobs[this.props.job.job_index]._id;
    jobForm.to = this.props.profile.key;
    jobForm.files = allFilePath;

    JobActionCreators.sendApplyJob(this.props.user.token, jobForm);

  }

  openApplyJobForm(evt) {
    $(evt.target).hide();
    $(".application--form").show();
  }

  componentDidMount() {
    userComplete = false;
  }

  componentDidUpdate() {
    if(!userComplete && this.props.user.complete) {
      let response = {};
      response['email'] = this.props.user.email;
      JobActionCreators.updateApplyJobDraft(response);
      userComplete = true;
    }
  }

  componentWillUnmount() {
    $("#myModal__positions").modal('hide');
  }

  render() {
    return (
      <div className="modal fade" id="myModal__positions" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span5 aria-hidden="true">&times;</span5>
              </button>
              <div className="modal-title">
                <b>
                  <span>{ this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].position : ""}</span>
                </b>
              </div>
            </div>

            <div className="modal-body">
              <div className="job--description">
                <p>

                  <b>Type</b> <br />
                  <span>{ this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].type : ""}</span> <br />
                  <br />

                  <b>Min.Required Experience</b> <br />
                  <span>{ this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].min_year : ""} years</span> <br />
                  <br />

                  <b>Compensation</b> <br />
                  <span>{ this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].compensation : ""}</span> <br />
                  <br />

                  <b>Job Details</b> <br />
                  <div className="text--detail">
                    <span>
                      { this.props.job.job_index != null ? this.props.profile.jobs[this.props.job.job_index].description : ""}
                    </span>
                  </div>

                  <br />

                  <b>Required Skills</b> <br />

                  {
                    this.props.job.job_index != null ?
                      <span>
                        {
                          this.props.profile.jobs[this.props.job.job_index].required_skills.map((skill, index) => (
                            <span key={index} ><i className="fa fa-star"></i> {skill} <br /></span>
                          ))
                        }
                      </span>
                    : ""
                  }

                </p>
              </div>
            </div>

            <div className="modal-footer">

              {
                this.props.user.type === 'Employee' ?
                <div>
                  <button className="modal__btn--light modal__btn--responsive applyjob none" onClick={this.openApplyJobForm.bind(this)} >Apply Now</button>
                </div>
                : ""
              }

              <div className="application--form">
                <label htmlFor="letter">Cover Letter</label>
                <textarea onChange={this.updateDraft.bind(this, 'cover_letter')} className="form-control form-control--mini" rows="5" placeholder='Dear..' id="letter"></textarea>
                <div className="col-md-6 col-sm-12" style={{ padding: "0px", paddingRight: "5px" }}>
                  <label htmlFor="phone">Phone</label>
                  <input type="text" onChange={this.updateDraft.bind(this, 'tel')} className="form-control form-control--mini" placeholder="Phone Number..." id="phone" />
                </div>
                {
                  this.props.user.complete ?
                  <div className="col-md-6 col-sm-12" style={{ padding: "0px" }}>
                    <label htmlFor="email">Email</label>
                    <input type="text" onChange={this.updateDraft.bind(this, 'email')} className="form-control form-control--mini" defaultValue={this.props.user.email} placeholder="Email Address..."  id="email" />
                  </div>
                  : ""
                }

                <div className="attach__file">
                  <p><b>File Attachments</b></p> <br />
                  {
                    this.props.job.job_index != null && typeof this.props.job.apply_draft.files !== "undefined" ?
                      <div style={{textAlign: "left"}}>
                        {
                          this.props.job.apply_draft.files.map((file, index) => (
                            <div key={index} ><i className="fa fa-star"></i> {file.original_name} <br /></div>
                          ))
                        }
                      </div>
                    : ""
                  }
                  <input onChange={this.uploadFile.bind(this)} type="file" name="attachfile" />
                </div>


                <button onClick={this.sendApplyJob.bind(this)} className="modal__btn--light" >Submit Application</button>
              </div>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default CompanyJob;
