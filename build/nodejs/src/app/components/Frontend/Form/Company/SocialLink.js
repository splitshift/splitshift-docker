import React, { Component } from 'react';

import ResourceActionCreators from '../../../../actions/ResourceActionCreators';
import CompanyProfileActionCreators from '../../../../actions/CompanyProfileActionCreators';

class CompanySocialLink extends Component {

  updateDraft(name, evt) {

    if(!this.props.status.edit_company_social_link)
      ResourceActionCreators.setEditor('edit_company_social_link', true);

    ResourceActionCreators.setDraft(name, evt.target.value);

  }

  updateSocialLink(evt) {
    evt.preventDefault();

    let socialLink = Object.assign({}, this.props.profile.links, this.props.profile.draft);
    CompanyProfileActionCreators.updateSocialLink(this.props.user.token, socialLink);
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.status.edit_company_social_link && this.props.status.edit_company_social_link)
      CompanyProfileActionCreators.getProfile(this.props.user.key);

    return true;

  }


  editorRender() {
    return (
      this.props.profile && this.props.profile.key ?
      <form onSubmit={this.updateSocialLink.bind(this)}>
        <input type="text" className="form-control form-control--mini" onChange={this.updateDraft.bind(this, 'facebook')} placeholder="Facebook Link" defaultValue={this.props.profile.links.facebook} />
        <input type="text" className="form-control form-control--mini" onChange={this.updateDraft.bind(this, 'instagram')} placeholder="Instagram Link" defaultValue={this.props.profile.links.instagram} />
        <input type="text" className="form-control form-control--mini" onChange={this.updateDraft.bind(this, 'youtube')} placeholder="Youtube Link" defaultValue={this.props.profile.links.youtube} />
        <input type="text" className="form-control form-control--mini" onChange={this.updateDraft.bind(this, 'linkedin')} placeholder="Linkedin Link" defaultValue={this.props.profile.links.linkedin} />
        <input type="text" className="form-control form-control--mini" onChange={this.updateDraft.bind(this, 'website')} placeholder="Website Link" defaultValue={this.props.profile.links.website} />
        <span6>Social media icon will be displayed below background picture.</span6> <br />
        <button className="btn btn-mini btn-default">Save</button>
      </form> : ""
    );
  }

  render() {
    return (
      <div>
        <h3 className="inner">Social Link</h3>
        {this.editorRender()}
      </div>
    )
  }
}

export default CompanySocialLink;
