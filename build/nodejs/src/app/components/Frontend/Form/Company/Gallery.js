import React, { Component } from 'react';
import { Link } from 'react-router';

import FeedActionCreators from '../../../../actions/FeedActionCreators';

import JQueryTool from '../../../../tools/JQueryTool';
import configLang from '../../../../tools/configLang'

class CompanyGallery extends Component {

  componentDidMount() {
    FeedActionCreators.getPhotosFeed(this.props.profile.key, this.props.user.token);
    JQueryTool.popupGallery();
  }

  render() {
    let headerContent = configLang[this.props.lang].company_profile.header.gallery
    return (
      this.props.feed.photos.length > 0 ?
      <div>
        <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
         <h3 className="inner">
          {headerContent}
         </h3>
         <div className="row">
          <div className="popup-gallery">
            {
              this.props.feed.photos.map((photo, index) => (
                <a key={index} href={photo.path.substring(6)}>
                  <div className="people--responsive">
                    <div className="col-md-4 col-sm-6 col-xs-6" style={{ padding: "0px 2px" }}>
                      <div className="people__pic">
                        <img src={photo.path.substring(6)}/>
                      </div>
                    </div>
                  </div>
                </a>
              ))
            }
          </div>
         </div>
         <Link to={"/th/company/" + this.props.profile.key + "/photo"} onClick={this.props.triggerButton.bind(null)} style={{ color:"#aaa", textDecoration: "underline", float: "right" }}>{ this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</Link>
        </div>
        {!this.props.responsive ? <hr /> : ""}
      </div>
      : <div />
    );
  }
}

export default CompanyGallery;
