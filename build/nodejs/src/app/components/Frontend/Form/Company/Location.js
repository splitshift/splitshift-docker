import React, { Component } from 'react';

import ResourceActionCreators from '../../../../actions/ResourceActionCreators';
import CompanyProfileActionCreators from '../../../../actions/CompanyProfileActionCreators';
import configLang from '../../../../tools/configLang'

import Map from '../../Map';

let choose_location;

class CompanyLocation extends Component {

  changeLatLng(lat, lng, evt){
    ResourceActionCreators.setDraft('lat', lat);
    ResourceActionCreators.setDraft('lng', lng);
  }

  updateDraft(name, evt) {
    ResourceActionCreators.setDraft(name, evt.target.value);
  }

  updateLocation(evt) {
    evt.preventDefault();

    ResourceActionCreators.setEditor('edit_company_location', true);

    // Update Address
    let result = {
      information: this.props.profile.draft.information ? this.props.profile.draft.information : this.props.profile.address && this.props.profile.address.information ? this.props.profile.address.information : "",
      description: this.props.profile.draft.description ? this.props.profile.draft.description : this.props.profile.address && this.props.profile.address.description ? this.props.profile.address.description : "",
      city: this.props.profile.draft.city ? this.props.profile.draft.city : this.props.profile.address.city || "",
      country: "Thailand",
      district: this.props.profile.draft.district ? this.props.profile.draft.district : this.props.profile.address.district || "",
      zip_code: this.props.profile.draft.zip_code ? this.props.profile.draft.zip_code : this.props.profile.address.zip_code || "",
      tel: this.props.profile.draft.tel ? this.props.profile.draft.tel : this.props.profile.tel,
      contact_email: this.props.profile.draft.contact_email ? this.props.profile.draft.contact_email :  this.props.profile.contact_email,
      lat: this.props.profile.draft.lat ? this.props.profile.draft.lat : this.props.profile.address && this.props.profile.address.lat ? this.props.profile.address.lat : "13.7563309",
      lng: this.props.profile.draft.lng ? this.props.profile.draft.lng : this.props.profile.address && this.props.profile.address.lng ? this.props.profile.address.lng : "100.5017651"
    };

    CompanyProfileActionCreators.updateAddress(this.props.user.token , result);

  }

  contentRender(onEdit=false) {
    return (
      <div>
        {
          !onEdit ?
          <div>
            <p>
              {this.props.profile.address && this.props.profile.address.information ? this.props.profile.address.information + ", " : "" }
              {this.props.profile.address && this.props.profile.address.district ? this.props.profile.address.district + ", " : ""}
              {this.props.profile.address && this.props.profile.address.city ? this.props.profile.address.city + " " : ""}
              {this.props.profile.address && this.props.profile.address.zip_code ? this.props.profile.address.zip_code + " " : "" }
              Thailand

            </p>
            <p>Tel: {this.props.profile.tel}</p>
            <p>E-mail: {this.props.profile.contact_email}</p>

            <h6>Map</h6>
            <p>{this.props.profile.address && this.props.profile.address.description}</p>
          </div>
          : ""
        }

        <div className="row">
          <div className="location__map">

            <Map map_id={this.props.map_id} changeLatLng={this.changeLatLng} address={this.props.profile.address} triggerMap={this.props.triggerMap.bind(null)} />

          </div>
        </div>
      </div>
    );
  }

  editorRender() {
    return (
      this.props.profile && this.props.profile.address ?
      <form onSubmit={this.updateLocation.bind(this)}>

        {this.contentRender(true)}

        <p>You can move pin to your location</p>

        <div>Tel</div>
        <input type="text" className="form-control form-control--mini" defaultValue={this.props.profile.tel} onChange={this.updateDraft.bind(this, 'tel')} placeholder="Tel" />

        <div>Contact Email</div>
        <input type="text" className="form-control form-control--mini" defaultValue={this.props.profile.contact_email} onChange={this.updateDraft.bind(this, 'contact_email')} placeholder="Contact Email" />

        <div>Address</div>
        <input id="location" type="text" className="form-control form-control--mini" defaultValue={this.props.profile.address.information} onChange={this.updateDraft.bind(this, 'information')} placeholder="Address" />

        <div>District</div>
        <input id="location" type="text" className="form-control form-control--mini" defaultValue={this.props.profile.address.district} onChange={this.updateDraft.bind(this, 'district')} placeholder="District" />

        <div>City</div>
        <input id="location" type="text" className="form-control form-control--mini" defaultValue={this.props.profile.address.city} onChange={this.updateDraft.bind(this, 'city')} placeholder="City" />

        <div>Zip Code</div>
        <input id="location" type="text" className="form-control form-control--mini" defaultValue={this.props.profile.address.zip_code} onChange={this.updateDraft.bind(this, 'zip_code')} placeholder="Zip Code" />

        <div>Descripe your location</div>
        <textarea className="form-control form-control--mini" rows="5" defaultValue={this.props.profile.address.description} onChange={this.updateDraft.bind(this, 'description')} placeholder="Description" />

        <button className="btn btn-mini btn-default">Save</button>

      </form>
      :""
    );
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.status.edit_company_location && this.props.status.edit_company_location) {
      CompanyProfileActionCreators.getProfile(this.props.user.key);
    }

    return true;

  }

  render() {
    let header = configLang[this.props.lang].company_profile.header

    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
        <h3 className="inner">{header.location} </h3>
        {this.props.editor ? this.editorRender() : this.contentRender()}
      </div>
    );
  }
}

export default CompanyLocation;

/*
<input type="text" className="form-control form-control--mini" defaultValue={this.props.profile.address.location && this.props.profile.address.location[0]} onChange={this.updateDraft.bind(this, 'lat')} placeholder="Lat"/>
<input type="text" className="form-control form-control--mini" defaultValue={this.props.profile.address.location && this.props.profile.address.location[1]} onChange={this.updateDraft.bind(this, 'lng')} placeholder="Lng"/>

*/
