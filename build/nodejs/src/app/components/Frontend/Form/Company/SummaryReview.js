import React, { Component } from 'react'

import configLang from '../../../../tools/configLang';

class CompanySummaryReview extends Component {

  thumbRating(num) {
    return [1, 2, 3, 4, 5].map((n, index) => (
      n <= num ?
        <span key={index}><i className="fa fa-thumbs-up" aria-hidden="true"></i>{' '}</span>
      :
        <span5 key={index}><i className="fa fa-thumbs-up" aria-hidden="true"></i>{' '}</span5>
    ))
  }

  circleRating(num) {
    return [1, 2, 3, 4, 5].map((n, index) => (
      n <= num ?
        <span key={index}><i className="fa fa-circle" aria-hidden="true"></i>{' '}</span>
      :
        <span5 key={index}><i className="fa fa-circle" aria-hidden="true"></i>{' '}</span5>
    ))
  }

  sumReview(reviews) {

    let sum = {
      benefit: 0,
      career: 0,
      recommend: 0,
      work: 0
    };

    reviews.map((review) => {
      sum.benefit += review.benefit;
      sum.career += review.career;
      sum.recommend += review.recommend;
      sum.work += review.work;
    });

    if(reviews.length > 0){
      sum.benefit /= reviews.length;
      sum.career /= reviews.length;
      sum.recommend /= reviews.length;
      sum.work /= reviews.length;
    }

    return sum;
  }

  render() {
    let sumReview = this.props.profile.review ? this.sumReview(this.props.profile.review) : { benefit: 0, career: 0, recommend: 0, work: 0};
    let reviewContent = configLang[this.props.lang].company_profile.review

    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
       <h3 className="inner company__header" style={{ marginBottom: "0px" }}>{reviewContent.summary}</h3>
       <div className="row company__ratingbox">
         <div className="company__rating">
           <p className="title__review overall">{reviewContent.side[0]}</p>
           <p className="point__review thumb">
             <span5>
               {this.thumbRating(this.props.profile.overall)}
               {this.props.profile.overall.toFixed(1)}
             </span5>
           </p>
         </div>
         <div className="company__rating">
           <p className="title__review">{reviewContent.side[1]}</p>
           <p className="point__review">
             <span5>
               {this.circleRating(sumReview.work)}
               {sumReview.work.toFixed(1)}
             </span5>
           </p>
         </div>
         <div className="company__rating">
           <p className="title__review">{reviewContent.side[2]}</p>
           <p className="point__review">
             <span5>
               {this.circleRating(sumReview.benefit)}
               {sumReview.benefit.toFixed(1)}
             </span5>
           </p>
         </div>
         <div className="company__rating">
           <p className="title__review">{reviewContent.side[3]}</p>
           <p className="point__review">
             <span5>
               {this.circleRating(sumReview.career)}
               {sumReview.career.toFixed(1)}
             </span5>
           </p>
         </div>
         <div className="company__rating">
           <p className="title__review">{reviewContent.side[4]}</p>
           <p className="point__review">
             <span5>
               {this.circleRating(sumReview.recommend)}
               {sumReview.recommend.toFixed(1)}
             </span5>
           </p>
         </div>
       </div>
     </div>
    )
  }
}

export default CompanySummaryReview;
