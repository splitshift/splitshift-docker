import React, { Component } from 'react';
import { Link } from 'react-router';

import configLang from '../../../../tools/configLang'

class CompanyPeople extends Component {

  render() {
    let header = configLang[this.props.lang].company_profile.header

    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
        <h3 className="inner">{header.people}</h3>
        <div className="row">
          <ul>
            <li>

              {
                this.props.profile.employees.length > 0 ?
                <div className={this.props.responsive?"people--responsive":"row"}>
                  <b>Present</b>

                  {
                    this.props.profile.employees.length > 3 && <a href="#" style={{ color: "#aaa", textDecoration: "underline", float: "right" }}>{ this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</a>
                  }

                  <br />

                  {
                    this.props.profile.employees.map((employee, index) => (
                      <Link key={index} to={'/th/user/' + employee.key}>
                        <div className="col-md-4" style={{ padding: "0px 2px" }}>
                          <div className="people__pic">
                            <img src={employee.profile_image} />
                            <div className="people__name">
                              <span1>{employee.profile.first_name} </span1> <br />
                              <span1>{employee.profile.last_name}</span1>
                            </div>
                          </div>
                        </div>
                      </Link>
                    ))
                  }

                </div>

                : ""
              }
            </li>
            <br />
            <li>

              {
                this.props.profile.formers.length > 0 ?
                <div className={this.props.responsive?"people--responsive":"row"}>
                  <b>Past</b>

                  {
                    this.props.profile.formers.length > 3 ?
                      <a href="#" style={{ color:"#aaa", textDecoration: "underline", float: "right" }}>{ this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</a>
                    : ""
                  }

                   <br />

                  {
                    this.props.profile.formers.map((former, index) => (
                      <Link key={index} to={'/th/user/' + former.key}>
                        <div className="col-md-4" style={{ padding: "0px 2px" }}>
                          <div className="people__pic">
                            <img src={former.profile_image} />
                            <div className="people__name">
                              <span1>{former.profile.first_name} </span1> <br />
                              <span1>{former.profile.last_name}</span1>
                            </div>
                          </div>
                        </div>
                      </Link>
                    ))
                  }


                </div>
                : ""
              }

            </li>

          </ul>
        </div>
      </div>
    );
  }
}

export default CompanyPeople;
