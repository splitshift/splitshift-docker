import React, { Component } from 'react';
import { Link } from 'react-router';

import ResourceActionCreators from '../../../../actions/ResourceActionCreators';
import CompanyProfileActionCreators from '../../../../actions/CompanyProfileActionCreators';

import ConfigData from '../../../../tools/ConfigData';
import configLang from '../../../../tools/configLang'

let BenefitShow = new Array(ConfigData.BenefitList().length).fill(true);

class CompanyBenefit extends Component {

  updateDraft(name, evt) {

    ResourceActionCreators.setDraft('category', evt.target.value);
    ResourceActionCreators.setDraft('description', evt.target.value);
  }

  createBenefit(evt) {
    evt.preventDefault();

    let response = {};
    response.category = this.props.profile.draft.category ? this.props.profile.draft.category : this.refs.benefit.value;
    response.description = response.category;

    let result = {
      benefits: this.props.profile.benefits.concat(response)
    };

    ResourceActionCreators.setEditor('edit_company_benefit', true);

    CompanyProfileActionCreators.updateProfile(result);
    CompanyProfileActionCreators.createBenefit(this.props.user.token, response);

  }

  deleteBenefit(id, evt) {
    evt.preventDefault();
    if(!this.props.status.edit_company_benefit)
      ResourceActionCreators.setEditor('edit_company_benefit', true);

    let result = {
      benefits: this.props.profile.benefits.filter((benefit, index) => index != id)
    };

    CompanyProfileActionCreators.updateProfile(result);
    CompanyProfileActionCreators.deleteBenefit(this.props.user.token, this.props.profile.benefits[id]);
  }

  benefitListRender(benefits, onEdit) {
    let rows = [];
    let companyContent = configLang[this.props.lang].company_profile

    for(let i = 0 ; i < benefits.length ; i+= 3) {
      let cols = [];
      for(let j = 0 ; j < 3  && i+j < benefits.length ; j++) {
        cols.push(
          <div key={i+j} className="col-md-4 col-sm-4 col-xs-6">
            <div className='company_benefits'>
              <img src={'/images/benefits/' + benefits[i+j].category.toLowerCase() + '.svg'} />
              <h6>{companyContent.benefit[ConfigData.BenefitList().indexOf(benefits[i+j].description)]}</h6>
              {onEdit?<a onClick={this.deleteBenefit.bind(this, i+j)} href="javascript:void(0)">Delete</a>:""}
            </div>
          </div>
        )
      }
      rows.push(<div key={i} className="row">{cols}</div>);
    }

    return rows;
  }

  contentRender(onEdit=false) {
    return (
      this.props.profile.benefits ? this.benefitListRender(this.props.profile.benefits, onEdit) : ""
    );
  }

  editorRender() {
    BenefitShow = new Array(ConfigData.BenefitList().length).fill(true);

    if(this.props.profile && this.props.profile.benefits) {
      this.props.profile.benefits.map((benefit, id) => {
        BenefitShow[ConfigData.BenefitList().indexOf(benefit.category)] = false;
      });
    }

    let companyContent = configLang[this.props.lang].company_profile

    return (
      <form onSubmit={this.createBenefit.bind(this)}>

        {this.contentRender(true)}

        <div className="row">

          <div>
            <select ref="benefit" className="form-control form-control--mini " onChange={this.updateDraft.bind(this)}>
              {
                ConfigData.BenefitList().map((benefit, index) => (
                  BenefitShow[index] && <option key={index} value={benefit}>{companyContent.benefit[index]}</option>
                ))
              }
            </select>
          </div>

          <div>
            <button className="btn btn-mini btn-default">ADD</button>
          </div>
        </div>

      </form>
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(!nextProps.status.edit_company_benefit && this.props.status.edit_company_benefit)
      CompanyProfileActionCreators.getProfile(this.props.user.key);

    return (JSON.stringify(nextProps.profile.benefits) !== JSON.stringify(this.props.profile.benefits)) || (nextProps.status.edit_company_benefit !== this.props.status.edit_company_benefit) || (nextProps.editor !== this.props.editor)
  }

  render() {
    let header = configLang[this.props.lang].company_profile.header
    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
        <h3 className="inner">{header.benefits} </h3> <br />
        {this.props.editor ? this.editorRender() : this.contentRender() }
      </div>
    );
  }
}

export default CompanyBenefit;
