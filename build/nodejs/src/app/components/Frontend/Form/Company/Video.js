import React, { Component } from 'react';

import ResourceActionCreators from '../../../../actions/ResourceActionCreators';
import CompanyProfileActionCreators from '../../../../actions/CompanyProfileActionCreators';

import configLang from '../../../../tools/configLang'
import ConfigData from '../../../../tools/ConfigData';

class CompanyVideo extends Component {

  updateDraft(name, evt) {

    let result = evt.target.value;

    if(name == 'video') {
      let youtube_embed = ConfigData.ConvertYoutubeToEmbed(result);
      if(youtube_embed != 'error')
        result = youtube_embed;
    }

    ResourceActionCreators.setDraft(name, result);
  }

  updateVideo(evt) {
    evt.preventDefault();

    ResourceActionCreators.setEditor('edit_company_video', true);
    CompanyProfileActionCreators.updateVideo(this.props.user.token, this.props.profile.draft);

  }

  contentRender(videoURL) {
    return (
      videoURL ?
      <div className="row">
        <iframe className="feed__clip" height="250" src={videoURL} frameBorder="0" allowFullScreen="" />
      </div> : ""
    );
  }

  editorRender() {

    return (
      this.props.profile && this.props.profile.key ?
      <form onSubmit={this.updateVideo.bind(this)}>
        {this.contentRender(this.props.profile.draft.video ? this.props.profile.draft.video : this.props.profile.video)}
        <input type="text" className="form-control form-control--mini" defaultValue={this.props.profile.video} onChange={this.updateDraft.bind(this, 'video')} />
        <button className="btn btn-mini btn-default">Save</button>
      </form> : ""
    );

  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.status.edit_company_video && this.props.status.edit_company_video)
      CompanyProfileActionCreators.getProfile(this.props.user.key);

    return true;

  }

  render() {
    let header = configLang[this.props.lang].company_profile.header
    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
        <h3 className="inner">{header.videos}</h3>
        {this.props.editor ? this.editorRender() : this.contentRender(this.props.profile.video)}
      </div>
    );
  }
}

export default CompanyVideo;
