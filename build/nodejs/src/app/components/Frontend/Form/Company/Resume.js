import React, { Component } from 'react';

import JQueryTool from '../../../../tools/JQueryTool';

class CompanyResume extends Component {
  constructor (props) {
    super(props)
    this.state = {
      img: 0
    }
  }

  componentDidMount () {
    $("img").on('load', () => {
      this.setState({img: this.state.img + 1})
    })
  }

  onPrint(evt) {
    window.print();
  }

  componentDidUpdate () {
    if(this.state.img === 2) {
      window.print()
      this.setState({img: 0})
    }
  }

  render() {
    return (
      <div className='resume'>

        <div className='resume__bio'>
          <div className='resume__avatar'>
            <img src={this.props.user.profile_image ? this.props.user.profile_image : ""}  />
            <h1>{this.props.user.company_name}</h1>

            <div style={{marginTop: "20px"}}>

              <div className='resume__block'>
                <h1>Contact Details</h1>
                <b>Address:</b> {this.props.user.address.district},  {this.props.user.address.zip_code}, {this.props.user.address.city} <br />
                <b>Telephone:</b> {this.props.user.tel} <br />
                <b>Email Address:</b>  {this.props.user.email}
              </div>

              <div className='resume__block'>
                <h1>Benefits</h1>
                <ul>
                  {
                    this.props.user.benefits.map((benefit, index) => (
                      <li key={index}><b>{benefit.category}</b></li>
                    ))
                  }
                </ul>
              </div>

              <div className='resume__block'>
                <h1>Open Positions</h1>
                <ul>
                  {
                    this.props.user.jobs.map((job, index) => (
                      <li key={index}><b>{job.position}</b></li>
                    ))
                  }
                </ul>
              </div>

            </div>

          </div>


        </div>

        <div className='resume__profile'>
          <div className='resume__block'>
            <h1>Overview</h1>
            <p dangerouslySetInnerHTML={{__html: this.props.user.overview.replace(/\n/g, "<br/>") }} />
          </div>

          <div className='resume__block'>
            <h1>Culture</h1>
            <p dangerouslySetInnerHTML={{__html: this.props.user.culture.replace(/\n/g, "<br/>") }} />
          </div>


        </div>

        <div className='resume__banner'>
          <button className="no-print" onClick={this.onPrint.bind(this)}>Print</button>{' '}
          <img src='/images/splitshift_720.png' />
        </div>

      </div>

    )
  }
}

export default CompanyResume;
