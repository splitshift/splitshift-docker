import React, { Component } from 'react';

import ConfigData from '../../../../tools/ConfigData'
import configLang from '../../../../tools/configLang'
import JQueryTool from '../../../../tools/JQueryTool'

class LastestReview extends Component {

  thumbRating(num) {
    return [1, 2, 3, 4, 5].map((n, index) => (
      n <= num ?
        <span key={index}><i className="fa fa-thumbs-up" aria-hidden="true"></i>{' '}</span>
      :
        <span5 key={index}><i className="fa fa-thumbs-up" aria-hidden="true"></i>{' '}</span5>
    ))
  }

  circleRating(num) {
    return [1, 2, 3, 4, 5].map((n, index) => (
      n <= num ?
        <span key={index}><i className="fa fa-circle" aria-hidden="true"></i>{' '}</span>
      :
        <span5 key={index}><i className="fa fa-circle" aria-hidden="true"></i>{' '}</span5>
    ))
  }

  toggleReview(evt) {
    $(evt.target).children('i').toggleClass("fa-chevron-down");
    $(evt.target).children('i').toggleClass("fa-chevron-up");
    $($(evt.target).data('target')).toggle();
  }



  render() {
    let reviewContent = configLang[this.props.lang].company_profile.review

    return (
      <div className={this.props.responsive ? "box_style_detail_2 profile-responsive" : ""}>
       <h3 className="inner">{reviewContent.lastest} </h3>
       <div className="row">
          {
            this.props.profile.review.map((review, index) => (
              <div key={index}>
                <div className="latest__reviewer">
                  <p><b>{JQueryTool.changeBirthDayFormat(review.date)} | {review.from.type === 'employee' ? ConfigData.ConvertEmployeeLang('Current Employee', this.props.lang) : ConfigData.ConvertEmployeeLang('Former Employee', this.props.lang)}</b></p>
                  <div className="latest__rating">
                    <p className="title__review overall">
                      {reviewContent.side[0]}
                      <button onMouseDown={this.toggleReview.bind(this)} onTouchEnd={this.toggleReview.bind(this)} className="btn btn-default reverse review--btnexpand" data-target={".review--expand" + index}>
                       <i className="fa fa-chevron-down"></i>
                      </button>
                    </p>
                    <p className="point__review thumb">
                      <span5>
                        {this.thumbRating(review.overall)}
                        {review.overall.toFixed(1)}
                      </span5>
                    </p>
                  </div>

                  <div className={"review--expand review--expand" + index}>
                    <div className="latest__rating">
                      <p className="title__review">{reviewContent.side[1]}</p>
                      <p className="point__review">
                        <span5>
                          {this.circleRating(review.work)}
                          {review.work.toFixed(1)}
                        </span5>
                      </p>
                    </div>
                    <div className="latest__rating">
                      <p className="title__review">{reviewContent.side[2]}</p>
                      <p className="point__review">
                        <span5>
                          {this.circleRating(review.benefit)}
                          {review.benefit.toFixed(1)}
                        </span5>
                      </p>
                    </div>
                    <div className="latest__rating">
                      <p className="title__review">{reviewContent.side[3]}</p>
                      <p className="point__review">
                        <span5>
                          {this.circleRating(review.career)}
                          {review.career.toFixed(1)}
                        </span5>
                      </p>
                    </div>
                    <div className="latest__rating">
                      <p className="title__review">{reviewContent.side[4]}</p>
                      <p className="point__review">
                        <span5>
                          {this.circleRating(review.recommend)}
                          {review.recommend.toFixed(1)}
                        </span5>
                      </p>
                    </div>
                  </div>
                </div>
                <hr className="hr-latest__reviewer" />
              </div>
            ))
          }




       </div>
     </div>
    )
  }
}

export default LastestReview;
