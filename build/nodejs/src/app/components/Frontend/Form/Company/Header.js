import React, { Component } from 'react';
import { Link } from 'react-router';

import ResourceActionCreators from '../../../../actions/ResourceActionCreators';
import CompanyProfileActionCreators from '../../../../actions/CompanyProfileActionCreators';

import JQueryTool from '../../../../tools/JQueryTool';
import configLang from '../../../../tools/configLang'

let mapPopupRender = false;

class CompanyHeader extends Component {

  changeProfileImage(evt){
    CompanyProfileActionCreators.uploadProfileImage(this.props.user.token, evt.target.files[0]);
  }

  changeBannerImage(evt) {
    CompanyProfileActionCreators.uploadBannerImage(this.props.user.token, evt.target.files[0]);
  }

  updateDraft(name, evt) {
    ResourceActionCreators.setDraft(name, evt.target.value);
  }

  updateProfile(evt) {
    evt.preventDefault();

    let result = {
      first_name: this.props.profile.draft.first_name ? this.props.profile.draft.first_name : this.props.profile.first_name ,
      last_name: this.props.profile.draft.last_name ? this.props.profile.draft.last_name : this.props.profile.last_name ,
      company_name: this.props.profile.draft.company_name ? this.props.profile.draft.company_name : this.props.profile.company_name ,
      type: this.props.profile.draft.type ? this.props.profile.draft.type : this.props.profile.type
    };

    CompanyProfileActionCreators.updateInformation(this.props.user.token, result);
  }

  setEditor(name) {

    let oldData = {
      first_name: this.props.profile.first_name,
      last_name: this.props.profile.last_name,
      company_name: this.props.profile.company_name,
      type: this.props.profile.type ? this.props.profile.type : "Hotel"
    };

    ResourceActionCreators.setOldDraft(oldData);
    ResourceActionCreators.setEditor(name, true);
  }

  openUploadMenu(evt) {
    $(".edit-menu__upload").toggleClass("opened");
  }

  contentRender() {
    let menusContent = configLang[this.props.lang].company_profile.menus

    return (
      <div className="detail--name">
        <h2>
          <b><span1 className="company--name">{this.props.profile.company_name}</span1></b>
        </h2>
        <h3>
          <b>
            <span1>{this.props.profile.address && this.props.profile.address.city}{' '}

            {
              this.props.profile.address && this.props.profile.address.location ?
                <a className="popup-gmaps" href={"https://maps.google.com/maps?q=" + this.props.profile.address.location[1] + ',' + this.props.profile.address.location[0]}>
                  <span1><i className="fa fa-map-marker" aria-hidden="true"></i></span1>
                </a>
              : ""
            }

            </span1>
          </b>
        </h3>

        {this.props.editor?
          <span className="detail--editbtn">
            <Link className='btn btn-default reverse' to={ '/' + this.props.lang + '/company/' + this.props.profile.key}><span1>{menusContent[2]}</span1></Link>
            <button className='btn btn-default reverse' style={{margin: "10px 0 10px 5px"}} onClick={this.setEditor.bind(this, "edit_company_profile_card")}><span1>{menusContent[3]}</span1></button>
          </span>
          :""
        }

      </div>
    );
  }

  editorRender() {
    return (
      this.props.profile?
      <div className="detail--name">
        <h2><b><span1>Edit Profile</span1></b></h2>
        <form onSubmit={this.updateProfile.bind(this)}>
          <input type="text" className="form-control form-control--mini" onChange={this.updateDraft.bind(this, 'first_name')} defaultValue={this.props.profile.first_name} placeholder="First Name" />
          <input type="text" className="form-control form-control--mini" onChange={this.updateDraft.bind(this, 'last_name')} defaultValue={this.props.profile.last_name} placeholder="Last Name" />
          <input type="text" className="form-control form-control--mini" onChange={this.updateDraft.bind(this, 'company_name')} defaultValue={this.props.profile.company_name} placeholder="Company Name" />
          <select className="form-control form-control--mini" onChange={this.updateDraft.bind(this, 'type')} defaultValue={this.props.profile.type}>
            <option>Hotel</option>
            <option>Restaurant</option>
            <option>Bar</option>
            <option>Nightclub</option>
          </select>
          <button className='btn btn-default reverse'>
            <span1>Save Profile Changes</span1>
          </button>
        </form>
      </div>:""
    );
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.status.edit_company_profile_card && this.props.status.edit_company_profile_card)
      CompanyProfileActionCreators.getProfile(this.props.user.key);

    return true;
  }

  componentDidMount() {
    mapPopupRender = false;
  }

  componentDidUpdate() {

    if(!mapPopupRender && this.props.profile.address && this.props.profile.address.location) {

      $('.popup-gmaps').magnificPopup({
      		disableOn: 700,
      		type: 'iframe',
      		mainClass: 'mfp-fade',
      		removalDelay: 160,
      		preloader: false,
      		fixedContentPos: false
    	});

      mapPopupRender = true;
    }
  }

  render() {

    let backgroundStyle = {}

    if(this.props.profile.draft.template_banner || this.props.profile.banner) {
			backgroundStyle = {
				background: "url(" + (this.props.profile.draft.template_banner || this.props.profile.banner) + ") no-repeat center center / cover"
			};

      if(this.props.profile.banner === '/images/loading.svg')
				backgroundStyle.backgroundSize = "80px 60px"

		}else {
      backgroundStyle = {
        backgroundColor: "#ccc"
      };
    }

    let editorContent = configLang[this.props.lang].company_profile.editor

    return (
      <div style={backgroundStyle} >
        <div className={ this.props.editor ? "company-cover company-cover-hover" : "company-cover" }>
          {this.props.editor ?
            <div className="edit__banner" style={{zIndex: "10"}}>
              <span className="edit__header" onClick={this.openUploadMenu.bind(this)}>{editorContent.upload.banner[0]}<i className="fa fa-camera"></i></span>

              <div className='edit-menu-company'>
                <div className='edit-menu__dropdown edit-menu__upload'>
                  <div className='edit-menu__item' onClick={JQueryTool.triggerUpload.bind(this, "upload-company-banner-image")}>
                    <i className='fa fa-pencil' style={{color: "black"}} ></i> {editorContent.upload.banner[1]}
                  </div>
                  <div className='edit-menu__item' data-toggle="modal" data-target="#myModal__background">
                    <i className='fa fa-picture-o' style={{color: "black"}}></i> {editorContent.upload.banner[2]}
                  </div>
                </div>
              </div>

              <input id="upload-company-banner-image" type="file" onChange={this.changeBannerImage.bind(this)} style={{display: "none"}} />
            </div>
            :""
          }
          <div className="row">
            <div className="company__detail">

              <div className="cover__detail">

                <div className="detail--pic">
                  <img src={this.props.profile.profile_image?this.props.profile.profile_image:"/images/avatar-image.jpg"} />
                  { this.props.editor ?
                    <div className="profilepic__edit" onClick={JQueryTool.triggerUpload.bind(this, "upload-company-profile")}>
                      <p><i className="fa fa-camera"></i> {editorContent.upload.profile}</p>
                      <input id="upload-company-profile" type="file" onChange={this.changeProfileImage.bind(this)} style={{display: "none"}} />
                    </div>: ""
                  }
                </div>

                {this.props.status.edit_company_profile_card?this.editorRender():this.contentRender()}

              </div>

            </div>
          </div>


          {
            !this.props.status.edit_company_profile_card ?

            <div className="cover__review">

              <div className="review--star">
                <h3><b><span1>
                  <p>Overall Employee</p> <p>Satisfaction</p>
                  {
                    [1, 2, 3, 4, 5].map((value, index) => (
                      value <= this.props.profile.overall ?
                      <span key={index}><i className="fa fa-thumbs-up" aria-hidden="true"></i>{' '}</span>
                      :
                      <span6 key={index}><i className="fa fa-thumbs-up" aria-hidden="true"></i>{' '}</span6>
                    ))
                  }
                  {' '}
                   { this.props.profile.overall ? Math.round(this.props.profile.overall * 10) / 10 : "-"}
                </span1></b></h3>
              </div>

              <div className="review--btn">
                {
                  this.props.user.type === 'Employee' && this.props.profile.employees && this.props.profile.employees.filter((employee) => employee.key == this.props.user.key).length > 0 ?
                    <button className='btn btn-default reverse' data-toggle="modal" data-target="#myModal__review"><span1>Review</span1></button>
                    : ""
                }

                <div>
                  <span1>{this.props.profile.review ? this.props.profile.review.length : "0"} Review{this.props.profile.review && this.props.profile.review.length > 1 ? "s" : ""}</span1>
                </div>

              </div>

            </div> : ""
          }



        </div>
      </div>
    );
  }
}

export default CompanyHeader;
