import React, { Component } from 'react';

import JobActionCreators from '../../../../actions/JobActionCreators';
import configLang from '../../../../tools/configLang'

class CompanyPosition extends Component {

  onChooseJob(job_index, evt) {
    JobActionCreators.chooseJob(job_index);
  }

  seeMore(evt) {
    $(".position_more").show();
    $(evt.target).hide();
  }

  render() {
    let header = configLang[this.props.lang].company_profile.header
    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
        <h3 className="inner">{header.positions} </h3>
        <div className="row">
          <ul>
            {
              typeof this.props.profile.jobs !== 'undefined' ?
                this.props.profile.jobs.map((job, index) => (
                  <li key={index} onClick={this.onChooseJob.bind(this, index)} className={ index >= 3 ? "position_more" : ""} style={{ display: index >= 3 ? "none" : "" }}>
                    <div className="position--content" data-toggle="modal" data-target="#myModal__positions">
                      <p>
                        <b>{job.position}</b>
                        <br />

                        Type:{' '}
                        <span style={{color: "#aaa"}} >{job.type}</span> <br />

                        Min.Required Experience:{' '}
                        <span style={{color: "#aaa"}} >{job.min_year} years</span> <br />

                        Compensation:{' '}
                        <span style={{color: "#aaa"}} >{job.compensation}</span>

                      </p>
                    </div>
                  </li>
                ))
              : ""
            }

            {
              this.props.profile.jobs.length > 3 ?
                <a href="javascript:void(0)" style={{ color:"#aaa", textDecoration: "underline", float: "right" }} onClick={this.seeMore.bind(this)}>{ this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</a>
              : ""
            }

          </ul>
        </div>
      </div>
    )
  }
}

export default CompanyPosition;

/*

<a className="btn--expand" style={{ color:"#aaa", textDecoration: "underline", float: "right", marginBottom: "16px" }}>Read more..</a>
*/
