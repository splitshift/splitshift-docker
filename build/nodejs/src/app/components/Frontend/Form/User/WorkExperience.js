import React, { Component } from 'react';
import { Link } from 'react-router';

import UserProfileActionCreators from '../../../../actions/UserProfileActionCreators';
import ResourceActionCreators from '../../../../actions/ResourceActionCreators';

import JQueryTool from '../../../../tools/JQueryTool';
import ConfigData from '../../../../tools/ConfigData';
import configLang from '../../../../tools/configLang';

let count = 0;

class FormUserWorkExperience extends Component {

  createExperience() {
    let result = this.props.profile.draft;

    if(typeof result.start_date === 'undefined')
      result.start_date = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;

    if(typeof result.end_date === 'undefined' || result.end_date || typeof result.current_job === 'undefined' || (!result.current_job))
      result.end_date = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

    result.city = result.city || '-'
    result.district = result.district || '-'
    result.workplace = result.workplace || '-'
    result.zip_code = result.zip_code || '-'

    UserProfileActionCreators.createExperience(this.props.user.token, result);
  }

  editExperience() {
    let result = this.props.profile.draft;
    result.city = result.city || '-'
    result.district = result.district || '-'
    result.workplace = result.workplace || '-'
    result.zip_code = result.zip_code || '-'
    UserProfileActionCreators.editExperience(this.props.user.token, result);
  }

  deleteExperience(id) {
    UserProfileActionCreators.deleteExperience(this.props.user.token, this.props.profile.experiences[id]);
  }

  setDraftEditExperience(id) {
    let selectedExperience = this.props.profile.experiences[id];
    let currentAddress = selectedExperience.address;
    selectedExperience = Object.assign({}, selectedExperience, currentAddress);

    if(typeof selectedExperience.end_date === 'undefined')
      selectedExperience.current_job = true;

    ResourceActionCreators.setOldDraft(selectedExperience);
    this.setEditor("edit_experience");
  }

  setEditor(name, evt) {
    ResourceActionCreators.setEditor(name, true);
  }

  toResumeContent() {
    ResourceActionCreators.cancelEditor("edit_experience", false);
  }

  updateExperience(name, evt) {

    if(name == 'workplace')
      ResourceActionCreators.setDraft('company_key', '');

    let value = evt.target.value;

    if(name == 'current_job') {
      value = evt.target.checked;
      ResourceActionCreators.setDraft("end_date", "");
    }

    else if(name == 'start_date')
      value = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;
    else if(name == 'end_date')
      value = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

    ResourceActionCreators.setDraft(name, value);
  }

  selectChooseCompany(name, key, evt) {
    this.refs.workplace.value = name;
    ResourceActionCreators.setDraft('workplace', name);
    ResourceActionCreators.setDraft('company_key', key);
    $(".search-hint__list").removeClass("opened");
  }

  shouldComponentUpdate(nextProps, nextState) {

    if((!nextProps.status.edit_experience && this.props.status.edit_experience) || (!nextProps.status.deleting && this.props.status.deleting))
      UserProfileActionCreators.reloadUserProfile(this.props.user.key);

    return (JSON.stringify(nextProps.profile.experiences) !== JSON.stringify(this.props.profile.experiences)) || nextProps.status.edit_experience || (nextProps.status.edit_experience !== this.props.status.edit_experience) || (nextProps.editor !== this.props.editor) || (nextProps.lang !== this.props.lang);

  }

  contentRender(onEdit=false) {
    let editorContent = configLang[this.props.lang].user_profile.editor
    let experiencesList = this.props.profile && this.props.profile.experiences?this.props.profile.experiences.map((experience, id) => (
      <li key={id}>
        <p>
          {experience.company_key ? <Link to={'/th/company/' + experience.company_key}>{experience.position}</Link>  : <b>{experience.position}</b> } {' '}
          {onEdit ? (<span>(<a href="javascript:void(0)" onClick={this.setDraftEditExperience.bind(this, id)}>{editorContent.edit}</a> <a href="javascript:void(0)" onClick={this.deleteExperience.bind(this, id)}>{editorContent.delete}</a>)</span>):""} <br />
          {experience.workplace} <br />
          {
            experience.company_key ?
              experience.address.information
            :
              experience.address.address + ", " + experience.address.city + ", " + experience.address.district + ", " + experience.address.zip_code
          }
          <br />
          <span style={{ color:"#aaa" }}>
            {JQueryTool.changeDateFormat(experience.start_date, this.props.lang)} - { experience.end_date ? JQueryTool.changeDateFormat(experience.end_date, this.props.lang) : (this.props.lang === 'th' ? 'ปัจจุบัน' : "Present") }  {' '}
            ({JQueryTool.diffExperienceDate(experience.start_date, experience.end_date, this.props.lang)})
          </span>
        </p>
      </li>
    )):"";

    return (
      <ul>
        {experiencesList}
        <div className="text--expand" />
      </ul>
    );
  }

  editorRender() {
    let expContent = configLang[this.props.lang].user_profile.workexp

    return (
      <div>
        {this.props.status.edit_experience ? this.experienceForm() : (<button className="btn btn-default btn-mini" onClick={this.setEditor.bind(this, "edit_experience")}>{expContent.add}</button>)}
        {this.props.status.edit_experience ? "" : this.contentRender(true)}
      </div>
    );
  }

  experienceForm() {
    count = 0
    let editorContent = configLang[this.props.lang].user_profile.editor
    let editorExpContent = editorContent.workexp

    return (
      this.props.profile?
      <div>
        <div className='search-hint'>
          <input type="text" ref="workplace" onChange={this.updateExperience.bind(this, "workplace")} className="search-hint__input form-control form-control--mini" placeholder={editorExpContent[0]} defaultValue={this.props.profile.draft.workplace} />
          <ul className='search-hint__list'>
            {
              this.props.profile.company_list.map((company, index) => {
                if (company.profile.company_name.indexOf(this.props.profile.draft.workplace) === -1 || count > 10) return ""
                count++
                return (
                  <li key={index} onClick={this.selectChooseCompany.bind(this, company.profile.company_name, company.key)} className='search-hint__item'>{company.profile.company_name}</li>
                )
              })
            }
          </ul>
        </div>

        <input type="text" onChange={this.updateExperience.bind(this, "position")} className="form-control form-control--mini" placeholder={editorExpContent[1]} defaultValue={this.props.profile.draft.position} />

        {
          this.props.profile.draft.company_key ? "" :
          <span>
            <input type="text" onChange={this.updateExperience.bind(this, "address")} className="form-control form-control--mini" placeholder={editorExpContent[2]} defaultValue={this.props.profile.draft.address ? this.props.profile.draft.address : ""} />
            <input type="text" onChange={this.updateExperience.bind(this, "district")} className="form-control form-control--mini" placeholder={editorExpContent[3]} defaultValue={this.props.profile.draft.address ? this.props.profile.draft.district : ""} />
            <input type="text" onChange={this.updateExperience.bind(this, "city")} className="form-control form-control--mini" placeholder={editorExpContent[4]} defaultValue={this.props.profile.draft.address ? this.props.profile.draft.city : ""} />
            <input type="text" onChange={this.updateExperience.bind(this, "zip_code")} className="form-control form-control--mini" placeholder={editorExpContent[5]} defaultValue={this.props.profile.draft.address ? this.props.profile.draft.zip_code : ""} />
          </span>
        }

        <div className="row">
          <div className="col-md-4 col-xs-12">
            <p style={{paddingTop: "5px", margin: "0 0 10px"}}>{editorExpContent[6]}</p>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini " ref="start_date_year" onChange={this.updateExperience.bind(this, 'start_date')} defaultValue={ this.props.profile.draft.start_date ? JQueryTool.getYear(this.props.profile.draft.start_date) : ""}>
              {
                ConfigData.Years().map((year, index) => (
                  <option key={index} value={year}>{this.props.lang === 'th' ? year + 543 : year}</option>
                ))
              }
            </select>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini " ref="start_date_month" onChange={this.updateExperience.bind(this, 'start_date')} defaultValue={ this.props.profile.draft.start_date ? JQueryTool.getMonth(this.props.profile.draft.start_date) : ""}>
              {
                ( this.props.lang === 'th' ? ConfigData.ThaiMonths() : ConfigData.Months()).map((month, index) => (
                  <option key={index} value={month[0]}>{month[1]}</option>
                ))
              }
            </select>
          </div>
        </div>

        {
          this.props.profile.draft.current_job ? "" :
          <div className="row">
            <div className="col-md-4 col-xs-12">
              <p style={{paddingTop: "5px", margin: "0 0 10px"}}>{editorExpContent[7]}</p>
            </div>
            <div className="col-md-4 col-xs-6">
              <select className="form-control form-control--mini" ref="end_date_year" onChange={this.updateExperience.bind(this, 'end_date')} defaultValue={ this.props.profile.draft.end_date ? JQueryTool.getYear(this.props.profile.draft.end_date) : ""}>
                {
                  ConfigData.Years().map((year, index) => (
                    <option key={index} value={year}>{this.props.lang === 'th' ? year + 543 : year}</option>
                  ))
                }
              </select>
            </div>
            <div className="col-md-4 col-xs-6">
              <select className="form-control form-control--mini " ref="end_date_month" onChange={this.updateExperience.bind(this, 'end_date')} defaultValue={ this.props.profile.draft.end_date ? JQueryTool.getMonth(this.props.profile.draft.end_date) : ""}>
                {
                  (this.props.lang === 'th' ? ConfigData.ThaiMonths() : ConfigData.Months()).map((month, index) => (
                    <option key={index} value={month[0]}>{month[1]}</option>
                  ))
                }
              </select>
            </div>
          </div>
        }

        <div className="checkbox--remember" style={{ float: "right" }}>
          <input type="checkbox" onClick={this.updateExperience.bind(this, 'current_job')} defaultChecked={ this.props.profile.draft.current_job ? true : false} /> {editorExpContent[8]}
        </div>

        <div style={{marginTop: "5px"}}>
          <button onClick={this.props.profile.draft._id?this.editExperience.bind(this):this.createExperience.bind(this)} className="btn btn-default btn-mini">{ this.props.profile.draft._id ? editorContent.edit : editorContent.add }</button>{' '}
          <a onClick={this.toResumeContent.bind(this)} href="javascript:void(0)">{editorContent.cancel}</a>
        </div>

      </div>:""
    );
  }

  componentDidUpdate() {
    if(this.props.status.edit_experience) {
      $('.search-hint').each(function () {
        var $input = $(this).children('.search-hint__input');
        var $list = $(this).children('.search-hint__list');

        $input.focus(function () {
          $list.addClass('opened');
        });

        $input.blur(function() {
          setTimeout(function() {
            $list.removeClass('opened');
          }, 500);
        });

      });
    }
  }

  render() {
    let header = configLang[this.props.lang].user_profile.workexp.header
    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
        <h3 className="inner">{header}</h3>
        <div className="row">
          {this.props.editor?this.editorRender():this.contentRender()}
        </div>
      </div>
    );
  }
}

/*
<div className="text--expand">
  <li>
    <p>
      <b>Head Sommelier</b> <br />
      U Sathorn Bangkok – J’AIME Restaurant <br />
      <span style={{ color:"#aaa" }}>Feb. 2015 to Present (1 year 5 months)</span>
    </p>
  </li>
</div>
<a className="btn--expand">Read more..</a>

<input type="text" className="form-control form-control--mini form-datepicker" data-name="start_date" placeholder="Start Date" defaultValue={this.props.profile.draft.start_date ? JQueryTool.changeDatePickerFormat(this.props.profile.draft.start_date) : ''} />
*/

export default FormUserWorkExperience;
