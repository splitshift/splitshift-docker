import React, { Component } from 'react';
import { Link } from 'react-router';

import FormUserConnection from './Connection';

import UserProfileActionCreators from '../../../../actions/UserProfileActionCreators';
import ResourceActionCreators from '../../../../actions/ResourceActionCreators';
import SocialActionCreators from '../../../../actions/SocialActionCreators';
import MessageActionCreators from '../../../../actions/MessageActionCreators';

import JQueryTool from '../../../../tools/JQueryTool';
import configLang from '../../../../tools/configLang'

let mapPopupRender = false;
let cancelEdit = false;

class FormUserProfileCard extends Component {

  toEditor(evt) {
    cancelEdit = false
    ResourceActionCreators.setEditor('edit_profile_card', true);
  }

  cancelEditor(evt) {
    cancelEdit = true
    ResourceActionCreators.setEditor('edit_profile_card', false);
  }

  toEdit(evt) {

    let result = {
      first_name: this.props.profile.first_name,
      last_name: this.props.profile.last_name,
      birth_day: this.props.profile.birth_day || "",
      gender: this.props.profile.gender || "",
      address: {
        information: this.props.profile.address.information,
        lat: 1,
        lng: 1
      }
    };

    UserProfileActionCreators.updateInfo(this.props.user.token, result);

  }

  loadMessage(evt) {
    MessageActionCreators.chooseOwner(this.props.user);
    MessageActionCreators.chooseOther(this.props.profile);

    MessageActionCreators.getMessageWithPeople(this.props.user.token, this.props.profile.key);
  }

  onUpdateProfile(name, evt) {

    let result = {};

    if(name === 'address') {
      result = {
        address: {
          information: evt.target.value,
          lat: 1,
          lng: 1
        }
      }
    }else if(name === 'name') {
      result = {
        first_name: evt.target.value.split(' ')[0],
        last_name: evt.target.value.split(' ')[1]
      };
    }else
      result[name] = evt.target.value;

    UserProfileActionCreators.updateProfile(result);

  }

  onFollowPeople(evt) {
    SocialActionCreators.putFollow(this.props.user.token, this.props.profile.key)
  }

  onConnectRequest(evt) {
    SocialActionCreators.postConnectRequest(this.props.user.token, this.props.profile.key);
  }

  onDisconnectRequest(evt) {
    SocialActionCreators.deleteConnect(this.props.user.token, this.props.profile.key);
  }

  editorRender() {
    let editorContent = configLang[this.props.lang].user_profile.editor.card

    return (
      this.props.profile?
      <div className="profile-card__info" style={{ width: "100%", marginBottom: "20px" }}>
        <h3>{editorContent[0]}</h3>
        <h3>
          <input type="text" className="form-control" onChange={this.onUpdateProfile.bind(this,'name')} placeholder={editorContent[1]} defaultValue={this.props.profile.first_name + ' ' + this.props.profile.last_name} />
        </h3>
        <p>
          <input type="text" className="form-control"  onChange={this.onUpdateProfile.bind(this,'address')}  placeholder={editorContent[2]} defaultValue={this.props.profile.address && this.props.profile.address.information}  />
          <p>{editorContent[3]}</p>
        </p>{' '}
        <p>
          <input type="text" className="form-control"  onChange={this.onUpdateProfile.bind(this,'birth_day')}  placeholder={editorContent[4]} defaultValue={this.props.profile.birth_day && JQueryTool.changeBirthDayDatePickerFormat(this.props.profile.birth_day)}  />
          <p>{editorContent[5]}</p>
        </p>{' '}
        <p>
          <select className="form-control" onChange={this.onUpdateProfile.bind(this,'gender')} defaultValue={this.props.profile.gender}>
            <option value="">{editorContent[6]}</option>
            <option value="male">{editorContent[7]}</option>
            <option value="female">{editorContent[8]}</option>
          </select>
        </p>{' '}
        <button className='btn btn-default' onClick={this.toEdit.bind(this)}>{editorContent[9]}</button>{' '}
        <a href="javascript:void(0)" onClick={this.cancelEditor.bind(this)}>{editorContent[10]}</a>
      </div>
      :""
    );
  }

  contentRender() {
    let menus = configLang[this.props.lang].user_profile.menus

    return (
      <div className="profile-card__info">
        <h3>
          {this.props.profile.first_name} {this.props.profile.last_name} <br />
          <span className="job">
            {this.props.profile.experiences && this.props.profile.experiences.length ? this.props.profile.occupation.position + (this.props.profile.occupation.experience_id==""?" (Past)":"") : ''}
          </span>
        </h3>

        <p>

          {
            this.props.profile.occupation && this.props.profile.occupation.address ?

              this.props.profile.occupation.company_key ?
                <div>
                  <Link to={'/th/company/' + this.props.profile.occupation.company_key }>{(this.props.profile.occupation.workplace ? this.props.profile.occupation.workplace + ", " : "") + this.props.profile.occupation.address.city}</Link>
                  {
                    this.props.profile.occupation.address && this.props.profile.occupation.address.location ?
                      <a className="popup-gmaps" href={"https://maps.google.com/maps?q=" + this.props.profile.occupation.address.location[1] + ',' + this.props.profile.occupation.address.location[0] }>
                        <span>
                          <i className="fa fa-map-marker" aria-hidden="true" style={{fontSize: "24px"}}></i>
                        </span>
                      </a>
                    : ""
                  }
                </div>
              :
              (this.props.profile.occupation.workplace ? this.props.profile.occupation.workplace + ", " : "") + this.props.profile.occupation.address.city
            : ""
          }


          <br />
          { this.props.editor && (menus[0] + ": " + (this.props.profile.address && this.props.profile.address.information)) }
        </p>

        <div>
          <Link to={'/th/user/' + this.props.profile.key} onClick={this.props.triggerButton.bind(null)} className='btn btn-default reverse'>{menus[1]}</Link>{' '}
          <Link to={'/th/user/' + this.props.profile.key + '/photo'} onClick={this.props.triggerButton.bind(null)} className='btn btn-default reverse'>{menus[2]}</Link>{' '}

          {
            typeof this.props.user.token !== 'undefined' && ((this.props.profile.key === this.props.user.key) || this.props.user.type !== 'Employee') &&
            <a href={'/' + this.props.lang + '/user/' + this.props.profile.key + '/resume'} className='btn btn-default reverse' target="_blank">{menus[3]}</a>
          }
          {' '}
          {
            this.props.other && typeof this.props.user.token !== 'undefined' && typeof this.props.profile.key !== 'undefined' && this.props.user.token &&
            <span>
              {
                (!this.props.profile.connected || this.props.user.type !== 'Employee') &&
                <button onClick={this.loadMessage.bind(this)} className='btn btn-default reverse' data-toggle="modal" data-target="#myModal__message">{menus[4]}</button>
              }
              {' '}
              <button className={ this.props.profile.following ? 'btn btn-default reverse' : 'btn btn-default' } onClick={this.onFollowPeople.bind(this)}>{ this.props.profile.following ? menus[5] : menus[6] }</button> {' '}

              {
                this.props.user.type === 'Employee' &&
                <button className={ this.props.profile.connected && this.props.profile.connected !== 'requesting' ? 'btn btn-default reverse' : 'btn btn-default' } onClick={this.props.profile.connected && this.props.profile.connected !== 'requesting' ? this.onConnectRequest.bind(this) : this.onDisconnectRequest.bind(this)}>
                  { this.props.profile.connected ? (this.props.profile.connected === 'requesting' ? menus[7] : menus[8]) : menus[9] }
                </button>
              }
              {' '}
            </span>
          }

        </div>
        <p className="connection">
          <a href="#" data-toggle="modal" data-target="#myModal__connections">
            {this.props.profile.connected_count} {menus[10]}{this.props.lang === 'en' && this.props.profile.connected_count > 1 ? 's' : ''}
          </a>
        </p>
        { this.props.profile.key && <FormUserConnection profile={this.props.profile} /> }
        {
          this.props.editor &&
          <div className="profile-card__editor">
            <Link to={'/' + this.props.lang + '/user/' + this.props.profile.key}><button className='btn btn-default'>{menus[11]}</button></Link>{' '}
            <button className='btn btn-default' onClick={this.toEditor.bind(this)}>{menus[12]}</button>
          </div>
        }
      </div>
    );
  }

  changeProfileImage(evt) {
    UserProfileActionCreators.uploadProfileImage(this.props.user.token, evt.target.files[0]);
  }

  componentDidMount() {
    mapPopupRender = false;
    JQueryTool.popupGallery();
    this.toEditor();
  }

  componentDidUpdate() {
    if(!mapPopupRender && this.props.profile.occupation && this.props.profile.occupation.address && this.props.profile.occupation.address.location) {
      $('.popup-gmaps').magnificPopup({
      		disableOn: 700,
      		type: 'iframe',
      		mainClass: 'mfp-fade',
      		removalDelay: 160,
      		preloader: false,
      		fixedContentPos: false
    	});

      mapPopupRender = true;
    }
  }

  render() {
    let editorContent = configLang[this.props.lang].user_profile.editor
    return (
      <div className='profile-card__container'>
          <div className='profile-card__image'>
            <div className="popup-gallery">
              <a href={this.props.profile.profile_image?this.props.profile.profile_image:"/images/avatar-image.jpg"}>
                <div className="profile-image" style={{ background: "url(" + (this.props.profile.profile_image || "/images/avatar.jpg") + ") no-repeat top center / cover"}} />
              </a>
            </div>
            {
              this.props.editor &&
              <div className="profile__edit">
                <p onClick={JQueryTool.triggerUpload.bind(this, "uploadImage")}><i className="fa fa-camera"></i> {editorContent.upload.profile}</p>
                <input id="uploadImage" type="file" onChange={this.changeProfileImage.bind(this)} style={{display: "none"}} />
              </div>
            }
          </div>

          {
            this.props.profile.key &&
            <div className='profile-card__content'>
              {this.props.status.edit_profile_card && this.props.editor?this.editorRender():this.contentRender()}
            </div>
          }
      </div>
    );
  }
}

export default FormUserProfileCard

//  { this.props.profile.occupation }
