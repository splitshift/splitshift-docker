import React, { Component } from 'react';
import UserProfileActionCreators from '../../../../actions/UserProfileActionCreators';
import ResourceActionCreators from '../../../../actions/ResourceActionCreators';

import configLang from '../../../../tools/configLang'

let initialAboutTextarea;
let expanding = false;

class FormUserAbout extends Component {

  toEdit() {
    UserProfileActionCreators.updateAbout(this.props.user.token ,{
      about: this.props.profile.about
    });
  }

  onUpdateProfile(evt) {

    if(!this.props.status.edit_about) {
      ResourceActionCreators.setEditor('edit_about', true);
      $(this.refs.about).expanding();
      $(this.refs.about).focus();
    }

    UserProfileActionCreators.updateProfile({about: evt.target.value });
  }

  editorRender() {
    let editorContent = configLang[this.props.lang].user_profile.editor
    let aboutmePlaceholder = configLang[this.props.lang].user_profile.aboutme_placeholder
    return (
      this.props.profile && typeof this.props.profile.about !== "undefined"?
      <div>

        { this.contentRender(this.props.profile.draft.about || this.props.profile.about) }

        <textarea id="abc" ref="about" className="form-control form-control--mini" onChange={this.onUpdateProfile.bind(this)} defaultValue={this.props.profile.about} placeholder={aboutmePlaceholder} />

        <div style={{ marginTop: "5px" }}>
          <button className='btn btn-default btn-mini' onClick={this.toEdit.bind(this)}>{editorContent.add}</button>
        </div>

      </div>:""
    );
  }

  contentRender(text) {

    let draftText = text;
    draftText = draftText.replace(/\n/g, "<br/>");

    return (
      <p style={{ wordWrap: "break-word" }} dangerouslySetInnerHTML={{__html: draftText }}></p>
    );
  }

  componentDidMount() {
    expanding = false;
    $(this.refs.about).expanding();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (nextProps.profile.about !== this.props.profile.about) || (nextProps.profile.draft.about !== this.props.profile.draft.about) || (nextProps.editor !== this.props.editor) || (nextProps.lang !== this.props.lang);
  }

  render() {
    let header = configLang[this.props.lang].user_profile.aboutme

    return (
      <div className="box_style_detail_2">
        <h3 className="inner">{header}</h3>
        <div className="row">
          <div className="col-md-12" style={{ paddingRight: "0px", paddingLeft: "0px" }}>
            {this.props.editor?this.editorRender():this.contentRender(this.props.profile.about)}
          </div>
        </div>
      </div>
    );

  }

}

export default FormUserAbout;
