import React, { Component } from 'react';
import { Link } from 'react-router';
import UserProfileActionCreators from '../../../../actions/UserProfileActionCreators';
import ResourceActionCreators from '../../../../actions/ResourceActionCreators';

import ConfigData from '../../../../tools/ConfigData';
import configLang from '../../../../tools/configLang'
import JQueryTool from '../../../../tools/JQueryTool';

class FormUserCourse extends Component {

  createCourse() {
    let result = this.props.profile.draft;

    if(typeof result.start_date === 'undefined')
      result.start_date = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;

    if(typeof result.end_date === 'undefined' || result.end_date || typeof result.current_job === 'undefined' || (!result.current_job))
      result.end_date = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

    UserProfileActionCreators.createCourse(this.props.user.token, result);
  }

  editCourse() {
    UserProfileActionCreators.editCourse(this.props.user.token, this.props.profile.draft);
  }

  deleteCourse(id) {
    this.setEditor("edit_course");
    UserProfileActionCreators.deleteCourse(this.props.user.token, this.props.profile.courses[id]);
  }

  setDraftEditCourse(id) {
    ResourceActionCreators.setOldDraft(this.props.profile.courses[id]);
    this.setEditor("edit_course");
  }

  updateCourse(name, evt) {
    let value = evt.target.value;

    if(name == 'start_date')
     value = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;
    else if(name == 'end_date')
     value = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

    ResourceActionCreators.setDraft(name, value);

  }

  toResumeContent() {
    ResourceActionCreators.cancelEditor('edit_course', false);
  }

  setEditor(name, evt) {
    ResourceActionCreators.setEditor(name, true);
  }

  contentRender(onEdit=false) {
    let editorContent = configLang[this.props.lang].user_profile.editor
    let courseList = this.props.profile.courses?this.props.profile.courses.map((course, id) => (
      <li key={id}>
        <b>{course.name}</b> { course.link && <span><a href={course.link} target="_blank">{editorContent.link}</a></span> }{' '}
        { onEdit &&
          <span>
            <a href="javascript:void(0)" onClick={this.setDraftEditCourse.bind(this, id)}>{editorContent.edit}</a>{' '}
            <a href="javascript:void(0)" onClick={this.deleteCourse.bind(this, id)}>{editorContent.delete}</a>
          </span>
        }  <br />
        { course.description && <span4>{course.description}<br /></span4> }
        <span style={{ color:"#aaa" }}>{JQueryTool.changeDateFormat(course.start_date)} - {JQueryTool.changeDateFormat(course.end_date)} ({JQueryTool.diffExperienceDate(course.start_date, course.end_date)})</span> <br />
        <br />
      </li>
    )):"";

    return (
      <ul style={{ marginBottom: "0px" }}>
        {courseList}
      </ul>
    );
  }

  editorRender() {
    let addContent = configLang[this.props.lang].user_profile.course.add
    return (
      <div>
        {this.props.status.edit_course?this.courseForm():(<button className="btn btn-default btn-mini" onClick={this.setEditor.bind(this, "edit_course")}>{addContent}</button>)}
        {this.props.status.edit_course?"":this.contentRender(true)}
      </div>
    );
  }

  courseForm() {
    let editorContent = configLang[this.props.lang].user_profile.editor
    let editorCourseContent = editorContent.course
    return (
      this.props.profile ?
      <div>

        <input type="text" onChange={this.updateCourse.bind(this, 'name')} className="form-control form-control--mini" placeholder={editorCourseContent[0]} defaultValue={this.props.profile.draft.name} />
        <input type="text" onChange={this.updateCourse.bind(this, 'link')} className="form-control form-control--mini" placeholder={editorCourseContent[1]} defaultValue={this.props.profile.draft.link} />
        <input type="text" onChange={this.updateCourse.bind(this, 'description')} className="form-control form-control--mini" placeholder={editorCourseContent[2]} defaultValue={this.props.profile.draft.description} />

        <div className="row">
          <div className="col-md-4 col-xs-12">
            <p style={{paddingTop: "5px", margin: "0 0 10px"}}>{editorCourseContent[3]}</p>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini " ref="start_date_year" onChange={this.updateCourse.bind(this, 'start_date')} defaultValue={this.props.profile.draft.start_date ? JQueryTool.getYear(this.props.profile.draft.start_date) : ""}>
              {
                ConfigData.Years().map((year, index) => (
                  <option key={index} value={year}>{this.props.lang === 'th' ? year + 543 : year}</option>
                ))
              }
            </select>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini " ref="start_date_month" onChange={this.updateCourse.bind(this, 'start_date')} defaultValue={this.props.profile.draft.start_date ? JQueryTool.getMonth(this.props.profile.draft.start_date) : ""}>
              {
                (this.props.lang === 'th' ? ConfigData.ThaiMonths() : ConfigData.Months()).map((month, index) => (
                  <option key={index} value={month[0]}>{month[1]}</option>
                ))
              }
            </select>
          </div>
        </div>

        <div className="row">
          <div className="col-md-4 col-xs-12">
            <p style={{paddingTop: "5px", margin: "0 0 10px"}}>{editorCourseContent[4]}</p>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini" ref="end_date_year" onChange={this.updateCourse.bind(this, 'end_date')} defaultValue={this.props.profile.draft.end_date ? JQueryTool.getYear(this.props.profile.draft.end_date) : ""}>
              {
                ConfigData.Years().map((year, index) => (
                  <option key={index} value={year}>{this.props.lang === 'th' ? year + 543 : year}</option>
                ))
              }
            </select>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini " ref="end_date_month" onChange={this.updateCourse.bind(this, 'end_date')} defaultValue={this.props.profile.draft.end_date ? JQueryTool.getMonth(this.props.profile.draft.end_date) : ""}>
              {
                (this.props.lang === 'th' ? ConfigData.ThaiMonths() : ConfigData.Months()).map((month, index) => (
                  <option key={index} value={month[0]}>{month[1]}</option>
                ))
              }
            </select>
          </div>
        </div>

        <div style={{marginTop: "5px"}}>
          <button onClick={this.props.profile.draft._id ? this.editCourse.bind(this):this.createCourse.bind(this)} className="btn btn-default btn-mini">{ this.props.profile.draft._id ? editorContent.edit : editorContent.add }</button>{' '}
          <a onClick={this.toResumeContent.bind(this)} href="javascript:void(0)">{editorContent.cancel}</a>
        </div>

      </div> : ""
    );
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.status.edit_course && this.props.status.edit_course)
      UserProfileActionCreators.reloadUserProfile(this.props.user.key);

    return  (JSON.stringify(nextProps.profile.courses) !== JSON.stringify(this.props.profile.courses)) || (nextProps.status.edit_course !== this.props.status.edit_course) || (nextProps.editor !== this.props.editor) || (nextProps.lang !== this.props.lang)
  }


  render() {
    let header = configLang[this.props.lang].user_profile.course.header
    return (
      <div className={this.props.responsive?"box_style_detail_2":""}>
        <h3 className="inner">{header}</h3>
        <div className="row">
          {this.props.editor?this.editorRender():this.contentRender()}
        </div>
      </div>
    );
  }
}

export default FormUserCourse;

/*

<input type="text" data-name="start_date" className="form-control form-control--mini form-datepicker" placeholder="Start Date" defaultValue={this.props.profile.draft.start_date ? JQueryTool.changeDatePickerFormat(this.props.profile.draft.start_date) : ""} />
<input type="text" data-name="end_date" className="form-control form-control--mini form-datepicker" placeholder="End Date" defaultValue={this.props.profile.draft.end_date ? JQueryTool.changeDatePickerFormat(this.props.profile.draft.end_date) : ""} />

*/
