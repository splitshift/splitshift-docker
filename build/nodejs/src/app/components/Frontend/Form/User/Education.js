import React, { Component } from 'react';
import UserProfileActionCreators from '../../../../actions/UserProfileActionCreators';
import ResourceActionCreators from '../../../../actions/ResourceActionCreators';

import JQueryTool from '../../../../tools/JQueryTool';
import ConfigData from '../../../../tools/ConfigData';
import configLang from '../../../../tools/configLang'

class FormUserEducation extends Component {

  createEducation(evt) {
    evt.preventDefault();

    let result = this.props.profile.draft;

    if(typeof result.start_date === 'undefined')
      result.start_date = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;

    if(typeof result.graduate_date === 'undefined' || result.graduate_date || typeof result.current_job === 'undefined' || (!result.current_job))
      result.graduate_date = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

    UserProfileActionCreators.createEducation(this.props.user.token, result);
  }

  editEducation(evt) {
    evt.preventDefault();
    UserProfileActionCreators.editEducation(this.props.user.token, this.props.profile.draft);
  }

  deleteEducation(id) {

    let result = {
      educations: this.props.profile.educations.filter((education, index) => index != id)
    };

    UserProfileActionCreators.updateProfile(result);
    UserProfileActionCreators.deleteEducation(this.props.user.token, this.props.profile.educations[id]);
  }

  setEditor(name, evt) {
    ResourceActionCreators.setEditor(name, true);
  }

  updateEducation(name, evt) {

    let value = evt.target.value;

    if(name == 'start_date')
      value = this.refs.start_date_year.value + '-' + this.refs.start_date_month.value;
    else if(name == 'graduate_date')
      value = this.refs.end_date_year.value + '-' + this.refs.end_date_month.value;

    ResourceActionCreators.setDraft(name, value);
  }

  toResumeContent() {
    ResourceActionCreators.cancelEditor("edit_education", false);
  }

  setDraftEditExperience(id) {
    ResourceActionCreators.setOldDraft(this.props.profile.educations[id]);
    this.setEditor("edit_education");
  }

  contentRender(onEdit=false) {
    let editorContent = configLang[this.props.lang].user_profile.editor
    let educationsList = this.props.profile.educations ? this.props.profile.educations.map((education, id) => (
      <li key={id}>
        <p>
          <i className="fa fa-graduation-cap"></i> <b>{education.field}</b> <br />
          {education.description ? <span4> {education.description} <br /></span4> : ""}
          {education.school ? <span4> {education.school} <br /></span4> : ""}

          <span style={{ color:"#aaa" }}>{JQueryTool.changeDateFormat(education.start_date)} - {education.graduate_date == "" ? "Present" : JQueryTool.changeDateFormat(education.graduate_date)} ({JQueryTool.diffExperienceDate(education.start_date, education.graduate_date)})</span> <br />
          {onEdit?(
            <span>
              <a href="javascript:void(0)" onClick={this.setDraftEditExperience.bind(this, id)}>{editorContent.edit}</a>{' '}
              <a href="javascript:void(0)" onClick={this.deleteEducation.bind(this, id)}>{editorContent.delete}</a>
            </span>
          ):""}
        </p>
      </li>
    )):"";

    return (
      <ul style={{ marginBottom: "0px" }}>
        {educationsList}
      </ul>
    );
  }

  editorRender() {
    let addContent = configLang[this.props.lang].user_profile.education.add
    return (
      <div>
        {this.props.status.edit_education && !this.props.status.deleting?this.educationForm():(<button className="btn btn-default btn-mini" onClick={this.setEditor.bind(this, "edit_education")}>{addContent}</button>)}
        {this.props.status.edit_education?"":this.contentRender(true)}
      </div>
    );
  }

  educationForm() {
    let editorContent = configLang[this.props.lang].user_profile.editor
    let editorEducationContent = editorContent.education

    return (
      this.props.profile?
      <form onSubmit={this.props.profile.draft._id?this.editEducation.bind(this):this.createEducation.bind(this)}>

        <input type="text" onChange={this.updateEducation.bind(this, "school")} className="form-control form-control--mini" placeholder={editorEducationContent[0]} defaultValue={this.props.profile.draft.school} />
        <input type="text" onChange={this.updateEducation.bind(this, "field")} className="form-control form-control--mini" placeholder={editorEducationContent[1]} defaultValue={this.props.profile.draft.field} />
        <input type="text" onChange={this.updateEducation.bind(this, "description")} className="form-control form-control--mini" placeholder={editorEducationContent[2]} defaultValue={this.props.profile.draft.description} />

        <div className="row">
          <div className="col-md-4 col-xs-12">
            <p style={{paddingTop: "5px", margin: "0 0 10px"}}>{editorEducationContent[3]}</p>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini " ref="start_date_year" onChange={this.updateEducation.bind(this, 'start_date')} defaultValue={this.props.profile.draft.start_date ? JQueryTool.getMonth(this.props.profile.draft.start_date) : ""}>
              {
                ConfigData.Years().map((year, index) => (
                  <option key={index} value={year}>{this.props.lang === 'th' ? year + 543 : year}</option>
                ))
              }
            </select>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini " ref="start_date_month" onChange={this.updateEducation.bind(this, 'start_date')} defaultValue={this.props.profile.draft.start_date ? JQueryTool.getMonth(this.props.profile.draft.start_date) : ""}>
              {
                ( this.props.lang === 'th' ? ConfigData.ThaiMonths() : ConfigData.Months()).map((month, index) => (
                  <option key={index} value={month[0]}>{month[1]}</option>
                ))
              }
            </select>
          </div>
        </div>

        <div className="row">
          <div className="col-md-4 col-xs-12">
            <p style={{paddingTop: "5px", margin: "0 0 10px"}}>{editorEducationContent[4]}</p>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini" ref="end_date_year" onChange={this.updateEducation.bind(this, 'graduate_date')} defaultValue={ this.props.profile.draft.graduate_date ? JQueryTool.getYear(this.props.profile.draft.graduate_date) : ""}>
              {
                ConfigData.Years().map((year, index) => (
                  <option key={index} value={year}>{this.props.lang === 'th' ? year + 543 : year}</option>
                ))
              }
            </select>
          </div>
          <div className="col-md-4 col-xs-6">
            <select className="form-control form-control--mini " ref="end_date_month" onChange={this.updateEducation.bind(this, 'graduate_date')} defaultValue={ this.props.profile.draft.graduate_date ? JQueryTool.getMonth(this.props.profile.draft.graduate_date) : ""}>
              {
                (this.props.lang === 'th' ? ConfigData.ThaiMonths() : ConfigData.Months()).map((month, index) => (
                  <option key={index} value={month[0]}>{month[1]}</option>
                ))
              }
            </select>
          </div>
        </div>


        <div style={{marginTop: "5px"}}>
          <button className="btn btn-default btn-mini">{this.props.profile.draft._id ? editorContent.edit : editorContent.add}</button>{' '}
          <a href="javascript:void(0)" onClick={this.toResumeContent.bind(this)}>{editorContent.cancel}</a>
        </div>
      </form>:""
    );
  }

  shouldComponentUpdate(nextProps, nextState) {

    if((!nextProps.status.edit_education && this.props.status.edit_education) || (!nextProps.status.deleting && this.props.status.deleting))
      UserProfileActionCreators.reloadUserProfile(this.props.user.key);

    return (JSON.stringify(nextProps.profile.educations) !== JSON.stringify(this.props.profile.educations)) || (nextProps.status.edit_education !== this.props.status.edit_education) || (nextProps.editor !== this.props.editor) || (nextProps.lang !== this.props.lang);

  }

  render() {
    let header = configLang[this.props.lang].user_profile.education.header
    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive more":""}>
        <h3 className="inner">{header}</h3>
        <div className="row">
          {this.props.editor?this.editorRender():this.contentRender()}
        </div>
      </div>
    );
  }
}

export default FormUserEducation;

/*
<div className="text--expand"></div>
<a className="btn--expand">Read more..</a>

<input type="text" data-name="graduate_date" className="form-control form-control--mini form-datepicker" placeholder="End Date" defaultValue={this.props.profile.draft.graduate_date ? JQueryTool.changeDatePickerFormat(this.props.profile.draft.graduate_date) : ""} />
<input type="text" onChange={this.updateEducation.bind(this, "start_date")} ref="start_date" data-provide="datepicker" className="form-control form-control--mini form-datepicker" placeholder="Start Date" defaultValue={this.props.profile.draft.start_date} />

*/
