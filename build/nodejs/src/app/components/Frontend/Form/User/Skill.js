import React, { Component } from 'react';
import UserProfileActionCreators from '../../../../actions/UserProfileActionCreators';
import ResourceActionCreators from '../../../../actions/ResourceActionCreators';

import configLang from '../../../../tools/configLang'

class FormUserSkill extends Component {
  verifyData(skill) {
    return skill.trim() !== '';
  }

  createSkill(evt) {
    evt.preventDefault();
    if(this.verifyData(this.refs.new_skill.value)) {
      let result = {
        skills: this.props.profile.skills.concat(this.refs.new_skill.value)
      };
      UserProfileActionCreators.updateProfile(result);
      UserProfileActionCreators.updateSkill(this.props.user.token, result);
      this.refs.new_skill.value = "";
    } else alert(this.props.lang === 'th' ? ' พิมพ์ทักษะในการทำงานของคุณ' : "Please type your skill.")

  }

  deleteSkill(id, evt) {
    let result = {
      skills: this.props.profile.skills.filter((skill, index) => index != id)
    }
    UserProfileActionCreators.updateProfile(result);
    UserProfileActionCreators.updateSkill(this.props.user.token, result);
  }

  contentRender(onEdit = false) {
    let editorContent = configLang[this.props.lang].user_profile.editor

    let skillsList = this.props.profile.skills?this.props.profile.skills.map((skill, id) => (
      <li key={id}>
        <i className="fa fa-book"></i> {skill} {' '}
        {onEdit?(<a href="javascript:void(0)" onClick={this.deleteSkill.bind(this, id)}>{editorContent.delete}</a>):""}
      </li>
    )):"";

    return (
      <ul>
        {skillsList}
      </ul>
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    return JSON.stringify(nextProps.profile.skills) !== JSON.stringify(this.props.profile.skills) || (nextProps.status.edit_skill !== this.props.status.edit_skill) || (nextProps.editor !== this.props.editor) || (nextProps.lang !== this.props.lang);
  }

  editorRender() {
    let editorContent = configLang[this.props.lang].user_profile.editor
    let { placeholder } = configLang[this.props.lang].user_profile.skill
    return (
      <div>
        <form onSubmit={this.createSkill.bind(this)}>

          {this.contentRender(true)}

          <div className="row">
            <div className="col-md-9" style={{ paddingLeft: "0px", paddingRight: "0px" }}>
              <input type="text" className="form-control form-control--mini" ref="new_skill" placeholder={placeholder} />
            </div>
            <div className="col-md-3" style={{ paddingRight: "0px", paddingLeft: "0px" }}>
              <button className="btn btn-default btn-mini--right">{ editorContent.add }</button>
            </div>
          </div>
        </form>
      </div>
    );
  }

  render() {
    let { header } = configLang[this.props.lang].user_profile.skill
    return (
      <div className={this.props.responsive?"box_style_detail_2 profile-responsive":""}>
        <h3 className="inner">{header}</h3>
        <div className="row">
          {this.props.editor?this.editorRender():this.contentRender()}
        </div>
      </div>
    );
  }
}

export default FormUserSkill;
