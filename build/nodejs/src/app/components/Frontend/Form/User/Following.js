import React, { Component } from 'react';
import { Link } from 'react-router';

import SocialActionCreators from '../../../../actions/SocialActionCreators';
import configLang from '../../../../tools/configLang'

class FormUserFollowing extends Component {

  constructor(argument){
    super(argument)
    this.state = {
      num: 9
    }
  }

  SeeMoreFollower() {
    this.setState({ num: this.state.num + 3})
  }

  render() {
    let header = configLang[this.props.lang].user_profile.following
    return (
      <div className={this.props.responsive?"box_style_detail_2":""}>
        <h3 className="inner">{header} </h3>
        <div className="row">
          <ul style={{ marginBottom: "0px" }}>
            <li>
              <div className={this.props.responsive?"people--responsive":"row"}>

                {
                  this.props.profile.followers.slice(0, this.state.num).map((follower, index) => (
                    <div key={index} className="col-md-4" style={{ padding: "0px 2px" }}>
                      <Link to={follower.type === 'Employee' ? '/th/user/' + follower.key : '/th/company/' + follower.key }>
                        <div className="people__pic">
                          <img src={follower.profile_image ? follower.profile_image : "/images/avatar-image.jpg"} />
                          <div className="people__name">
                            <span1>{follower.name}</span1>
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))
                }


                {
                  this.props.profile.followers.length > this.state.num ?
                  <div className="row">
                    <div className="col-md-12">
                      <a href="javascript:void(0)" onClick={this.SeeMoreFollower.bind(this)} style={{ color: "#aaa", textDecoration: "underline", float: "right" }}>{ this.props.lang === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</a>
                    </div>
                  </div>
                  : ""
                }


              </div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default FormUserFollowing;

/*

*/
