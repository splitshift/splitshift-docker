import React, { Component } from 'react';
import UserProfileActionCreators from '../../../../actions/UserProfileActionCreators';
import ResourceActionCreators from '../../../../actions/ResourceActionCreators';

import configLang from '../../../../tools/configLang'

class FormUserLanguage extends Component  {
  verifyData(result) {
    return typeof result.language !== 'undefined' && result.language.trim() !== '';
  }

  createLanguage(evt) {
    evt.preventDefault();

    let result = this.props.profile.draft;

    if(typeof result.proficiency === 'undefined')
      result.proficiency = this.refs.proficiency.value;

    if(this.verifyData(result))
      UserProfileActionCreators.createLanguage(this.props.user.token, result);
    else
      alert("Please type your language.");

  }

  editLanguage(evt) {
    evt.preventDefault();

    if(this.verifyData(this.props.profile.draft))
      UserProfileActionCreators.editLanguage(this.props.user.token, this.props.profile.draft);
    else
      alert("Please type your language.");

  }

  deleteLanguage(id) {

    let result = {
      languages: this.props.profile.languages.filter((language, index) => index != id)
    };

    UserProfileActionCreators.updateProfile(result);
    UserProfileActionCreators.deleteLanguage(this.props.user.token, this.props.profile.languages[id]);

  }

  updateLanguage(name, evt) {
    ResourceActionCreators.setDraft(name, evt.target.value);
  }

  setDraftEditCourse(id) {
    ResourceActionCreators.setOldDraft(this.props.profile.languages[id]);
    this.setEditor("edit_language");
  }

  contentRender(onEdit=false) {
    let editorContent = configLang[this.props.lang].user_profile.editor

    let languageList = this.props.profile.languages?this.props.profile.languages.map((language, id) =>
      <li key={id}>
        <p>
          {language.language} <br />
          <span style={{ color:"#aaa" }}>{language.proficiency}</span>
          {onEdit?(<span><br /> <a href="javascript:void(0)" onClick={this.setDraftEditCourse.bind(this, id)}>{editorContent.edit}</a> <a href="javascript:void(0)" onClick={this.deleteLanguage.bind(this, id)}>{editorContent.delete}</a></span>):""}
        </p>
      </li>
    ):"";

    return (
      <ul>
        {languageList}
      </ul>
    );
  }

  editorRender() {
    let addContent = configLang[this.props.lang].user_profile.language.add
    return (
      <div>
        {this.props.status.edit_language?this.languageForm():(<button className="btn btn-default btn-mini" onClick={this.setEditor.bind(this, "edit_language")}>{addContent}</button>)}
        {this.props.status.edit_language?"":this.contentRender(true)}
      </div>
    );
  }

  languageForm() {
    let editorContent = configLang[this.props.lang].user_profile.editor
    let editorLanguageContent = editorContent.language
    return (
      this.props.profile?
      <form onSubmit={this.props.profile.draft._id?this.editLanguage.bind(this):this.createLanguage.bind(this)}>
        <input type="text" onChange={this.updateLanguage.bind(this, 'language')} className="form-control form-control--mini" defaultValue={this.props.profile.draft.language} placeholder={editorLanguageContent[0]} />
        <select ref="proficiency" onChange={this.updateLanguage.bind(this, 'proficiency')} className="form-control form-control--mini" defaultValue={this.props.profile.draft.proficiency}>
          <option value='Native Language'>{editorLanguageContent[1]}</option>
          <option value='Fluent'>{editorLanguageContent[2]}</option>
          <option value='Advanced'>{editorLanguageContent[3]}</option>
          <option value='Intermediate'>{editorLanguageContent[4]}</option>
          <option value='Beginner'>{editorLanguageContent[5]}</option>
        </select>

        <div style={{marginTop: "5px"}}>
          <button className="btn btn-default  btn-mini">{this.props.profile.draft._id? editorContent.edit : editorContent.add }</button>{' '}
          <a onClick={this.toResumeContent.bind(this)} href="javascript:void(0)">{editorContent.cancel}</a>
        </div>

      </form>:""
    )
  }

  setEditor(name, evt) {
    ResourceActionCreators.setEditor(name, true);
  }

  toResumeContent() {
    ResourceActionCreators.setEditor('edit_language', false);
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.status.edit_language && this.props.status.edit_language)
      UserProfileActionCreators.reloadUserProfile(this.props.user.key);

    return JSON.stringify(nextProps.profile.languages) !== JSON.stringify(this.props.profile.languages) || (nextProps.status.edit_language !== this.props.status.edit_language) || (nextProps.editor !== this.props.editor) || (nextProps.lang !== this.props.lang)
  }

  render() {
    let header = configLang[this.props.lang].user_profile.language.header
    return (
      <div className={this.props.responsive?"box_style_detail_2":""}>
        <h3 className="inner">{header}</h3>
        <div className="row">
          {this.props.editor?this.editorRender():this.contentRender()}
        </div>
      </div>
    )
  }
}

export default FormUserLanguage;
