import React, { Component } from 'react';

import JQueryTool from '../../../../tools/JQueryTool';
import configLang from '../../../../tools/configLang'

class Resume extends Component {
  constructor (props) {
    super(props)
    this.state = {
      img: 0
    }
  }

  onPrint(evt) {
    window.print();
  }

  componentDidMount () {
    $("img").on('load', () => {
      this.setState({img: this.state.img + 1})
    })
  }

  componentDidUpdate () {
    if(this.state.img === 2) {
      window.print()
      this.setState({img: 0})
    }
  }

  render() {
    let resumeContent = configLang[this.props.language].resume
    return (
      <div className='resume'>

        <div className='resume__bio'>
          <div className='resume__avatar'>
            <img src={this.props.user.profile_image ? this.props.user.profile_image : ""}  />
            <h1>{this.props.user.first_name} {this.props.user.last_name}</h1>

            <div style={{marginTop: "20px"}}>
              <b>Date of Birth:</b> {this.props.user.birth_day ? JQueryTool.changeBirthDayFormat(this.props.user.birth_day) : "-"} <br />
              <b>Home Address:</b> {this.props.user.address ? this.props.user.address.information : "-"} <br />
              <b>Email</b> {this.props.user.email}
            </div>

          </div>

          {
            this.props.user.interests.length > 0 ?
            <div className='resume__block'>
              <h1>{resumeContent.interest}</h1>
              <ul>

                {
                  this.props.user.interests.map((interest, id) => (
                      <li key={id}>{interest.value}</li>
                  ))
                }

              </ul>
            </div>
            : ""
          }

          {
            this.props.user.skills.length > 0 ?
            <div className='resume__block'>
              <h1>{resumeContent.skill}</h1>
              <ul>
                {
                  this.props.user.skills.map((skill, id) => (
                    <li key={id}>{skill}</li>
                  ))
                }
              </ul>
            </div>
            : ""
          }


        </div>

        <div className='resume__profile'>
          <div className='resume__block'>
            <h1>{resumeContent.about_me}</h1>
            <p dangerouslySetInnerHTML={{__html: this.props.user.about.replace("\n", "<br />") }} />
          </div>

          <div className='resume__block'>
            <h1>{resumeContent.work_exp}</h1>
            <ul style={{ listStyle: "none" }}>
              {
                this.props.user.experiences.map((experience, index) => (
                  <li>
                    <h2>
                      {experience.position}
                      <small>
                      – {experience.workplace}
                      –
                      {
                        experience.company_key ?
                          experience.address.information
                        :
                          experience.address.address + ", " + experience.address.city + ", " + experience.address.district + ", " + experience.address.zip_code
                      }
                      </small>
                    </h2>
                    <div>{JQueryTool.changeDateFormat(experience.start_date)} - { experience.end_date ? JQueryTool.changeDateFormat(experience.end_date) : "Present" }  {' '} ({JQueryTool.diffExperienceDate(experience.start_date, experience.end_date)})</div>
                  </li>
                ))
              }
            </ul>
          </div>


          {
            this.props.user.educations.length > 0 ?
              <div className='resume__block'>
                <h1>Education</h1>
                <ul>
                  {
                    this.props.user.educations.map((education, index) => (
                      <li>
                        <h2>{education.field} – <small>{education.school}</small></h2>
                        <div>{JQueryTool.changeDateFormat(education.start_date)} - {education.graduate_date == "" ? "Present" : JQueryTool.changeDateFormat(education.graduate_date)} ({JQueryTool.diffExperienceDate(education.start_date, education.graduate_date)})</div>
                      </li>
                    ))
                  }
                </ul>
              </div>
            : ""
          }


          {
            this.props.user.courses.length > 0 ?
              <div className='resume__block'>
                <h1>{resumeContent.course}</h1>
                <ul style={{ listStyle: "none" }}>
                  {
                    this.props.user.courses.map((course, index) => (
                      <li>
                        <h2>{course.name} – <small>{course.description}</small></h2>
                        <div>{JQueryTool.changeDateFormat(course.start_date)} - {course.end_date == "" ? "Present" : JQueryTool.changeDateFormat(course.end_date)} ({JQueryTool.diffExperienceDate(course.start_date, course.end_date)})</div>
                      </li>
                    ))
                  }
                </ul>
              </div>
            : ""
          }


          {
            this.props.user.languages.length > 0 ?
            <div className='resume__block'>
              <h1>{resumeContent.language}</h1>
              <ul style={{ listStyle: "none" }}>
                {
                  this.props.user.languages.map((language, id) => (
                    <li key={id}><b>{language.language}</b> – {language.proficiency}</li>
                  ))
                }
              </ul>
            </div>
            : ""
          }

        </div>

        <div className='resume__banner'>
          <button className="no-print" onClick={this.onPrint.bind(this)}>Print</button>{' '}
          <img src='/images/splitshift_720.png' />
        </div>

      </div>

    )
  }
}

export default Resume;
