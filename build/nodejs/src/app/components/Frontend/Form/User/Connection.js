import React, { Component } from 'react';
import { Link } from 'react-router';

import SocialActionCreators from '../../../../actions/SocialActionCreators';

class FormUserConnection extends Component {

  componentDidMount() {
    if(typeof this.props.backend === 'undefined')
      SocialActionCreators.getConnections(this.props.profile.key);
  }

  componentWillUnmount(evt) {
    $("#myModal__connections").modal('hide');
  }

  render() {
    return (
      <div className="modal fade" id="myModal__connections" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span5 aria-hidden="true">&times;</span5>
              </button>
            </div>

            <div className="modal-body">
              <div className="modal__navlist">
                {
                  this.props.profile.connections.map((connection, index) => (
                    <Link key={index} to={'/th/user/' + connection.key}>
                      <li className="modal__navitem">
                        <div className="photo__noti" style={{ background: "url(" + (connection.profile_image || "/images/avatar-image.jpg") + ") no-repeat center center / cover", marginLeft: "0px", marginTop: "-10px"}} />
                        <div>
                          <b>{connection.name}</b>
                        </div>
                      </li>
                    </Link>
                  ))
                }
              </div>
            </div>

          </div>
        </div>
      </div>
    )
  }
}

export default FormUserConnection;
