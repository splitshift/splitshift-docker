import React, { Component } from 'react';

import UserProfileActionCreators from '../../../../actions/UserProfileActionCreators';
import ResourceActionCreators from '../../../../actions/ResourceActionCreators';

import ConfigData from '../../../../tools/ConfigData'
import configLang from '../../../../tools/configLang'

let InterestShow = new Array(ConfigData.InterestList().length).fill(true);
let current_category = "";


class FormUserInterest extends Component {

  deleteInterest(id, evt) {

    if(!this.props.status.edit_interest)
      ResourceActionCreators.setEditor('edit_interest', true);

    let result = {
      interests: this.props.profile.interests.filter((interest, index) => index != id)
    };

    UserProfileActionCreators.updateProfile(result);
    UserProfileActionCreators.deleteInterest(this.props.user.token, this.props.profile.interests[id]);
  }

  createInterest(evt) {
    evt.preventDefault();

    let category = this.props.profile.draft.category ? this.props.profile.draft.category : this.refs.category.value;

    let interestData = {
      category: category,
      value: category === 'Other' ? this.props.profile.draft.value : category
    };

    let result = {
      interests: this.props.profile.interests.concat(interestData)
    };

    if(interestData.category === 'Other')
      this.refs.other.value = "";

    UserProfileActionCreators.updateProfile(result);
    UserProfileActionCreators.createInterest(this.props.user.token, interestData);

  }

  updateInterest(name, evt) {

    if(!this.props.status.edit_interest)
      ResourceActionCreators.setEditor('edit_interest', true);

    ResourceActionCreators.setDraft(name, evt.target.value);
  }

  contentRender(onEdit=false) {
    let interestContent = configLang[this.props.lang].user_profile.interest
    let editorContent = configLang[this.props.lang].user_profile.editor

    return this.props.profile.interests ?
    this.props.profile.interests.map((interest, id) => (
      <div className='profile_interest' key={id}>
        <img src={'/images/interests/' + interest.category.toLowerCase() +'.svg'} />
        <h6 className="mini">{interestContent.list[ConfigData.InterestList().indexOf(interest.value)]}</h6>
        {onEdit ? <a onClick={this.deleteInterest.bind(this, id)} href="javascript:void(0)">{editorContent.delete}</a> : "" }
      </div>
    )) : (<div></div>);
  }

  editorRender() {
    let editorContent = configLang[this.props.lang].user_profile.editor
    let interestContent = configLang[this.props.lang].user_profile.interest

    InterestShow = new Array(ConfigData.InterestList().length).fill(true);

    if(this.props.profile && this.props.profile.interests) {
      this.props.profile.interests.map((interest, id) => {

        if(interest.category !== 'Other')
          InterestShow[ConfigData.InterestList().indexOf(interest.category)] = false;

      });
    }

    return (
      <form onSubmit={this.createInterest.bind(this)}>
        <div className="row">
          {this.contentRender(true)}
        </div>
        <div className="row">
          <div className="col-md-2" style={{ paddingRight: "0px", paddingLeft: "0px" }}>
            <select ref="category" className="form-control form-control--mini" onChange={this.updateInterest.bind(this, 'category')} defaultValue={this.props.profile.draft.category} >
              { ConfigData.InterestList().map((interest, id) => (InterestShow[id] ? (<option key={id} value={interest}>{interestContent.list[id]}</option>): '')) }
            </select>
          </div>
          {
            (current_category === 'Other' || this.props.profile.draft.category === 'Other') &&
            <div className="col-md-4" style={{ paddingRight: "0px", paddingLeft: "0px" }}>
              <input ref="other" type="text" className="form-control form-control--mini" onChange={this.updateInterest.bind(this, 'value')} defaultValue={this.props.profile.draft.value} />
            </div>
          }
          <div className="col-md-4" style={{ paddingRight: "0px", paddingLeft: "0px" }}>
            <button className="btn btn-default btn-mini--right">{editorContent.add}</button>{' '}
          </div>
        </div>
      </form>
    )
  }

  componentDidMount() {
    ResourceActionCreators.setDraft("category", ConfigData.InterestList()[InterestShow.indexOf(true)]);
  }

  componentDidUpdate() {
    current_category = ConfigData.InterestList()[InterestShow.indexOf(true)];
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(!nextProps.status.edit_interest && this.props.status.edit_interest)
      UserProfileActionCreators.reloadUserProfile(this.props.user.key);

    return JSON.stringify(nextProps.profile.interests) !== JSON.stringify(this.props.profile.interests) || nextProps.status.edit_interest || (nextProps.editor !== this.props.editor) || (nextProps.lang !== this.props.lang);
  }

  render() {
    let header = configLang[this.props.lang].user_profile.interest.header
    return (
      <div className="box_style_detail_2">
  			<h3 className="inner">{header}</h3>
		    <div className="row">
          <div className="col-md-12" style={{ paddingRight: "0px", paddingLeft: "0px" }}>
            {this.props.editor ? this.editorRender():this.contentRender()}
          </div>
        </div>
  		</div>
    );
  }
}

export default FormUserInterest;
