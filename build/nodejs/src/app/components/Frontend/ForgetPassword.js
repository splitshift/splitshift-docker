import React, { Component } from 'react';
import { browserHistory } from 'react-router';

import AuthenticationActionCreators from '../../actions/AuthenticationActionCreators';

class ForgetPassword extends Component {

  changePassword(evt) {
    evt.preventDefault();
    let info = { password: this.refs.password.value };
    AuthenticationActionCreators.changePasswordFormForgot(info, this.props.forget_id);
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(nextProps.login.forget && !this.props.login.forget) {
      alert("Change Password Complete!");
      browserHistory.push('/th/');
    }

    return true;
  }

  render() {
    return (
      <div className="container" style={{ marginTop: "100px" }}>
        <form onSubmit={this.changePassword.bind(this)}>
          <label htmlFor="pwd">Password</label>
          <input ref="password" type="password" className="form-control" placeholder="Password..." id="pwd" />
          <div>
            <button className="modal__btn--dark">CHANGE PASSWORD</button>
          </div>
        </form>
      </div>
    )
  }
}

export default ForgetPassword;
