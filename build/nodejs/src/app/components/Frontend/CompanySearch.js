import React, { Component } from 'react';
import { Link } from 'react-router';

import MapSearch from './MapSearch';

import SearchActionCreators from '../../actions/SearchActionCreators';
import ConfigData from '../../tools/ConfigData';
import configLang from '../../tools/configLang'
import JQueryTool from '../../tools/JQueryTool';

let new_searchmap = false;
let choose_location = {lat: 13.71129, lng: 100.50908};

class CompanySearch extends Component {

  componentDidMount() {
    SearchActionCreators.clearFilter();
    SearchActionCreators.Company({});
    new_searchmap = false;
  }

  updateFilter(name, evt) {
    let response = {};
    response[name] = evt.target.value;
    SearchActionCreators.updateFilter(response);
  }

  updateAdvanceFilter(name, value, evt) {
    let response = {};

    if(value == 'All')
      value = "";

    response[name] = value;
    SearchActionCreators.updateFilter(response);
  }

  onSearch(evt) {

    evt.preventDefault();

    $('html, body').animate({
        scrollTop: $(this.refs.jobs__company).offset().top - 200
    }, 1000);

    SearchActionCreators.Company(this.props.search.filter);
  }

  thumbGenerator(num) {
    return (
      Array.apply(1, {length: num}).map((n, index) => (
        <span4 key={index}>
          <i className="fa fa-thumbs-up" aria-hidden="true" style={{ fontSize: "20px" }}></i>{' '}
        </span4>
      ))
    )
  }

  triggerSearchMap() {
    new_searchmap = false;
  }

  loadMore(evt) {
    SearchActionCreators.Company(this.props.search.filter, this.props.search.companies.length / 10)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(!nextProps.search.searching && this.props.search.searching)
      new_searchmap = true;
    return true;
  }

  render() {
    let content = configLang[this.props.language].company_search
    let advancedContent = configLang[this.props.language].advanced_search
    return (
      <div>
        <div className="container">
          <div className="header__search company-search">
            <div className="company__title">
              <p><span>{content.intro}</span></p>
            </div>
            <div className="job__search">
              <form onSubmit={this.onSearch.bind(this)}>
                <div className="job__bar">
                  <label htmlFor="company">{content.company[0]}</label>
                  <input id="company" type="text" className="form-control" onChange={this.updateFilter.bind(this, 'keyword')} placeholder={content.company[1]} />
                </div>
                <div className="location__bar">
                  <label htmlFor="location">{content.location[0]}</label>
                  <input id="location" type="text" className="form-control" placeholder={content.location[1]} />
                  <button className="btn btn-default ad__search ad__search--desktop" type="button" onClick={JQueryTool.showAdvanceSearch.bind(this)}>{advancedContent.title}</button>
                </div>
                <div className="job__btn">
                  <button className="btn btn-default"><i className="fa fa-search"></i></button>
                  <button className="btn btn-default ad__search ad__search--responsive" type="button" onClick={JQueryTool.showAdvanceSearch.bind(this)}>{advancedContent.title}</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div className="jobbar__detail">
          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.type ? advancedContent.company_type[ConfigData.CompanyTypeList().indexOf(this.props.search.filter.type)] : advancedContent.company_type_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
              {
                ConfigData.CompanyTypeList().map((company, index) => (
                 <li key={index}><a href="#" onClick={this.updateAdvanceFilter.bind(this, 'type', company)}>{advancedContent.company_type[index]}</a></li>
                ))
              }
            </ul>
          </div>

          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.overall ? this.thumbGenerator(this.props.search.filter.overall) : advancedContent.employee_rating_title }
              <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
              {
                [5, 4, 3, 2, 1].map((num, index)=> (
                  <li key={index}>
                    <a href="#" onClick={this.updateAdvanceFilter.bind(this, 'overall', num)}>
                      {this.thumbGenerator(num)}
                    </a>
                  </li>
                ))
              }
            </ul>
          </div>

          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.bounded_time ? advancedContent.date_posted[this.props.search.filter.bounded_time] : advancedContent.lastest_jobs_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
              {
                Object.keys(ConfigData.DatePostList()).map((datepost, index) => (
                 <li key={index}><a href="#" onClick={this.updateAdvanceFilter.bind(this, 'bounded_time', datepost)}>{advancedContent.date_posted[datepost]}</a></li>
                ))
              }
            </ul>
          </div>

          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.compensation ? advancedContent.compensation[ConfigData.CompensationList().indexOf(this.props.search.filter.compensation)] : advancedContent.compensation_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
              {
                ConfigData.CompensationList().map((compensation, index) => (
                 <li key={index}><a href="#" onClick={this.updateAdvanceFilter.bind(this, 'compensation', compensation)}>{advancedContent.compensation[index]}</a></li>
                ))
              }
            </ul>
          </div>
        </div>

        <div className="container">

          <MapSearch companies={this.props.search.companies} jobs={[]} new_searchmap={new_searchmap} triggerSearchMap={this.triggerSearchMap.bind(this)} choose_location={choose_location} />

          <div ref="jobs__company" className="jobs__company">

            {
              this.props.search.companies.map((company, index) => (
                <Link key={index} to={'/th/company/' + company.key}>
                  <div className="company__item">
                    <div className="job__pic">
                      <img src={company.profile_image ? company.profile_image : "/images/avatar-image.jpg"}  />
                    </div>
                    <div className="search__detail">
                      <p><b><span>{company.profile.company_name}</span></b></p>
                      <p><span5>{company.address.city ? company.address.city : "-"}</span5></p>
                      <div className="search__btn">
                        <Link to={'/th/company/' + company.key}><button className="btn btn-default">{company.profile.openjobs} open job{company.profile.openjobs > 1 ? "s" : ""}</button></Link>
                      </div>
                    </div>
                  </div>
                </Link>
              ))
            }


            <div className="btn__seemore">
            {
              this.props.search.load_more ?
                <button onClick={this.loadMore.bind(this)} className="btn btn-default">{ this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</button>
              : ""
            }
            </div>


          </div>
        </div>
      </div>
    );
  }
}

export default CompanySearch;
