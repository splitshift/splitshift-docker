import React, { Component } from 'react';
import { Link } from 'react-router';

import ApplyJobModal from './ApplyJobModal';

import SearchActionCreators from '../../actions/SearchActionCreators';
import JobActionCreators from '../../actions/JobActionCreators';

import JQueryTool from '../../tools/JQueryTool';

let userComplete = false;

class Search extends Component {

  componentDidMount() {
    let keyword = this.props.search.filter.all_keyword ? this.props.search.filter.all_keyword : "";
    SearchActionCreators.All(keyword);
  }

  componentDidUpdate() {
    JQueryTool.triggerOwlCarousel();

    if(!userComplete && this.props.user.complete) {
      let response = {};
      response['email'] = this.props.user.email;
      JobActionCreators.updateApplyJobDraft(response);
      userComplete = true;
    }

  }

  updateFilter(name, evt) {
    let response = {};
    response[name] = evt.target.value;
    SearchActionCreators.updateFilter(response);
  }

  chooseJobSearch(index, evt) {
    SearchActionCreators.chooseJobSearch(index);
  }

  onSearch(evt) {
    evt.preventDefault();
    let keyword = this.props.search.filter.all_keyword ? this.props.search.filter.all_keyword : "";
    SearchActionCreators.All(keyword);
  }

  render() {
    return (
      <div className="container">
        <div className="headsearch">
          <form onSubmit={this.onSearch.bind(this)}>
            <div className="search--bar">
              <input type="text" className="form-control" onChange={this.updateFilter.bind(this, 'all_keyword')} placeholder="search, jobs, people, companies ..." defaultValue={this.props.search.filter.all_keyword} />
              <button className="btn btn-default"><i className="fa fa-search"></i></button>
            </div>
          </form>
        </div>
        <hr className="hr-title" />

        {
          this.props.search.people.length > 0 ?
            <div className="search__people">
              <p><h2><b><span5>PEOPLE</span5></b></h2></p>
              <div className="owl-carousel">

                {
                  this.props.search.people.map((p, index) => (
                    <div key={index} className="item">
                      <div className="search__pic">
                        <img src={p.profile_image ? p.profile_image : "/images/avatar-image.jpg"} />
                      </div>
                      <div className="search__detail">
                        <p><b><span>{p.profile.first_name} {p.profile.last_name}</span></b></p>
                        <p><span5>{p.profile.occupation.position}</span5></p>
                      </div>
                      <div className="search__btn">
                        <Link to={'/th/user/' + p.key}><button className="btn btn-default">See Profile</button></Link>
                      </div>
                    </div>
                  ))
                }

                <Link to='/people-search'><div className="item lastitem"><p>SEE MORE + </p></div></Link>
              </div>
            </div>
          : ""
        }


        {
          this.props.search.companies.length > 0 ?
            <div className="search__company">
              <p><h2><b><span5>COMPANIES</span5></b></h2></p>
              <div className="owl-carousel">

              {
                this.props.search.companies.map((company, index) => (
                  <div key={index} className="item">
                    <div className="search__pic">
                      <img src={company.profile_image ? company.profile_image : "/images/avatar-image.jpg"}  />
                    </div>
                    <div className="search__detail">
                      <p><b><span>{company.profile.company_name}</span></b></p>
                      <p><span5>{company.profile.information}</span5></p>
                    </div>
                    <div className="search__btn">
                      <Link to={'/th/company/' + company.key}><button className="btn btn-default">See Company</button></Link>
                    </div>
                  </div>
                ))
              }


                <Link to='/company-search'><div className="item lastitem"><p>SEE MORE + </p></div></Link>
              </div>
            </div>
          : ""
        }


        {
          this.props.search.jobs.length > 0 ?
            <div className="search__jobs">
              <p><h2><b><span5>JOBS</span5></b></h2></p>
              <div className="owl-carousel">

                {
                  this.props.search.jobs.map((job, index) => (
                    <div key={index} className="item">
                      <div className="search__position">
                        <p><b>{job.position}</b></p>
                      </div>
                      <div className="search__pic">
                        <img src={job.owner.profile_image ? job.owner.profile_image : "/images/avatar-image.jpg"} />
                      </div>
                      <div className="search__detail">
                        <p><b><span>{job.owner.company_name}</span></b></p>
                        <p><span5>{JQueryTool.changeDateAgo(job.date)}</span5></p>
                      </div>

                        <div className="search__btn">
                          <button className="btn btn-default" onClick={this.chooseJobSearch.bind(this, index)} data-toggle="modal" data-target="#myModal__positions">See Details</button>
                        </div>

                    </div>
                  ))
                }


                <Link to='/job-search'><div className="item lastitem"><p>SEE MORE + </p></div></Link>
              </div>
            </div>
          : ""
        }


        <ApplyJobModal job={this.props.job} search={this.props.search} user={this.props.user} />

      </div>
    );
  }
}

export default Search;
