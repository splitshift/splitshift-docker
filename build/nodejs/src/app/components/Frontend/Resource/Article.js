import React, { Component } from 'react';
import { Link } from 'react-router';

import ResourceForward from './Forward'

import JQueryTool from '../../../tools/JQueryTool';
import ResourceActionCreators from '../../../actions/ResourceActionCreators';

const API_URL = () => {
  return window.location.origin;
}

class ResourceArticle extends Component {

  constructor (argument) {
    super(argument)
    this.state = {
      forward: false
    }
  }

  componentDidUpdate() {
    if(this.props.resource.load) {
      this.refs.comment.value = "";
      ResourceActionCreators.getResource(this.props.resource_id, this.props.user.token);
    }
  }

  componentWillUnmount() {
    ResourceActionCreators.resetResource()
  }

  likeResource(evt) {
    ResourceActionCreators.likeResource(this.props.user.token, this.props.resource.resource);
  }

  commentResource(evt) {

    evt.preventDefault();

    let comment = {
      comment: this.refs.comment.value
    }

    ResourceActionCreators.commentResource(this.props.user.token, comment, this.props.resource.resource._id);
  }

  deleteCommentResource(comment, evt) {
    ResourceActionCreators.deleteCommentResource(this.props.user.token, comment, this.props.resource.resource._id);
  }

  ForwardResource(evt) {
    this.setState({ forward: true })
  }

  shouldComponentUpdate(nextProps, nextState) {
		return (JSON.stringify(nextProps.resource) !== JSON.stringify(this.props.resource)) || (JSON.stringify(nextProps.user) !== JSON.stringify(this.props.user)) || (this.state.forward !== nextState.forward);
	}

  render() {


    let share_url = API_URL() + '/th/resource/article/' + this.props.resource.resource._id;

    return (
      <div className="container resource3">
        <div className="resource__title">
          <h2><b><span>{this.props.resource.resource.title}</span></b></h2>

          <div>
            <p style={{ width: "100%" }}>
              <span5 style={{ marginTop: "10px" }}>by{' '}
                <Link to={this.props.resource.resource.owner.user_type === 'Employee' ? "/th/user/" + this.props.resource.resource.owner.key : this.props.resource.resource.owner.user_type !== 'Admin' ? "/th/company/" + this.props.resource.resource.owner.key : "/" }>{this.props.resource.resource.owner.name}</Link>
              </span5>
              <span5 style={{ float: 'right' }}><i className="fa fa-clock-o"></i> {JQueryTool.changeDateAgo(this.props.resource.resource.date)}</span5>
            </p>
          </div>

        </div>

        {
          this.props.resource.resource.featured_image ?
            <div className="resource__photo" style={{ background: "url("+ this.props.resource.resource.featured_image.substring(6) +") no-repeat center center", backgroundSize: "cover" }} />
          :
            <div className="resource__video">
              <iframe src={this.props.resource.resource.featured_video} />
            </div>
        }


        <div className="resource__content">
          <div className="ressource__paragraph" style={{ paddingBottom: "0px" }}>

            <div style={{ backgroundColor: "#eeeeee", padding: "10px", marginBottom: "10px" }}>
              <p>{this.props.resource.resource.short_description}</p>
            </div>

            <div dangerouslySetInnerHTML={{__html: this.props.resource.resource.content}}  />

            <div style={{textAlign: "right"}}>

              <a className="share-btn" href="#" data-href={"https://www.facebook.com/sharer/sharer.php?app_id=235025906884083&sdk=joey&u=" + share_url + "&display=popup&ref=plugin&src=share_button"} onClick={(evt) => { return !window.open($(evt.currentTarget).data('href'), 'Facebook', 'width=640,height=580') }} >
                <button className="modal__btn--light"><i className="icon-facebook-squared"></i> Share</button>{' '}
              </a>

              <a className="share-btn" href="#" data-href={"http://www.linkedin.com/shareArticle?mini=true&url=" + share_url} onClick={(evt) => { return !window.open($(evt.currentTarget).data('href'), 'Linkedin', 'width=640,height=580') }}  >
               <button className="modal__btn--light"><i className="icon-linkedin-squared"></i> Share</button>{' '}
              </a>

              {
                !this.state.forward && <button className="modal__btn--light" onClick={this.ForwardResource.bind(this)}>Forward</button>
              }
            </div>

            {
              this.state.forward && <ResourceForward user={this.props.user} resource={this.props.resource.resource} sending_forward_email={this.props.resource.sending_forward_email} />
            }

          </div>

          <article className='feed' style={{ marginTop: "0px", paddingTop: "0px" }}>
            <div className='feed__actions' style={{textAlign: "left"}}>
              <button className='feed__button' onClick={this.likeResource.bind(this)} style={{color: this.props.resource.resource.like.indexOf(this.props.user.key) !== -1 ? "#f60" : ""}}>
                <i className='fa fa-thumbs-up' style={{color:  this.props.resource.resource.like.indexOf(this.props.user.key) !== -1 ? "#f60" : ""}}></i>
                Like
              </button>
              <div className='feed__counter'>{this.props.resource.resource.comments.length} Comment{this.props.resource.resource.comments.length > 1 ? "s" : ""} • {this.props.resource.resource.like.length} Like{this.props.resource.resource.like.length > 1 ? "s" : ""}</div>
            </div>
            <div className='feed__tray'>

              <form onSubmit={this.commentResource.bind(this)}>
                <div className='feed__comment'>
                  <div className="comment__pic">
                    <img src={ this.props.user.profile_image ? this.props.user.profile_image : '/images/avatar-image.jpg' } />
                  </div>
                  <div>
                    <input type='text' ref="comment" placeholder='Write a comment' />
                  </div>
                  <button className="feed__send">
                    <i className="fa fa-paper-plane" style={{right: "-5px"}}></i>
                  </button>
                </div>
              </form>

              {
                this.props.resource.resource.comments.map((comment, index) => (
                  <div className='feed__comment' key={index}>
                    <div className="comment__pic">
                      <Link to={comment.owner.user_type == 'Employee' ? "/th/user/" + comment.owner.key  : "/th/company/" + comment.owner.key }>
                        <img src={ comment.owner.profile_image ? comment.owner.profile_image : "/images/avatar-image.jpg" } />
                      </Link>
                    </div>
                    <div style={{ textAlign: "left"}}>
                      <div>{comment.owner.name} { comment.owner.key == this.props.user.key ? <a onClick={this.deleteCommentResource.bind(this, comment)} href="javascript:void(0)">Delete</a> : ""}</div>
                      <div>{comment.comment}</div>
                    </div>
                  </div>
                ))
              }

            </div>
          </article>

        </div>
      </div>

    );
  }
}

export default ResourceArticle;
