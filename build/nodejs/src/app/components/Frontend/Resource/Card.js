import React, { Component } from 'react';
import { Link } from 'react-router';

import JQueryTool from '../../../tools/JQueryTool';

import ResourceActionCreators from '../../../actions/ResourceActionCreators';

class ResourceCard extends Component {

  render() {
    return (
      <div className="resource__item">
        <Link to={'/th/resource/article/' + this.props.resource._id } style={{color: "#565a5c"}}>
          <div className="resource__pic">
            <img src={ this.props.resource.featured_image ? this.props.resource.featured_image.substring(6) : "http://img.youtube.com/vi/" + (this.props.resource.featured_video ? this.props.resource.featured_video.substring(24) : "") + "/0.jpg" } />
            <br />
          </div>
          <div className="resource__detail">
            <p style={{ height: "40px", "overflow": "hidden", "textOverflow": "ellipsis" }}><b><span>{this.props.resource.title}</span></b></p>
            <p style={{ height: "60px", "overflow": "hidden", "textOverflow": "ellipsis", marginBottom: "35px" }}>{this.props.resource.short_description}</p>
            <p style={{ overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis", textAlign: "left", display: "inline-block", width: "120px" }}>
              <b>By {this.props.resource.owner.name}</b>
            </p>
            <p style={{ float: "right" }}>
              <u>{JQueryTool.changeDateAgo(this.props.resource.date)}</u>
            </p>
          </div>
        </Link>
      </div>
    );
  }
}

export default ResourceCard;
