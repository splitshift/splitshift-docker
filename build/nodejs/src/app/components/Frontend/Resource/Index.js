import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';

import ConfigData from '../../../tools/ConfigData';
import JQueryTool from '../../../tools/JQueryTool';
import configLang from '../../../tools/configLang'

import ResourceCard from './Card';

import ResourceActionCreators from '../../../actions/ResourceActionCreators';

class ResourceIndex extends Component {

  componentDidMount() {

    $('.next').click(function() {
      $('.container.btntab').animate({ scrollLeft:  '+=250' });
    })

    $('.prev').click(function() {
      $('.container.btntab').animate({ scrollLeft:  '-=250' });
    })

    ResourceActionCreators.getAllResource();
  }

  componentWillUnmount() {
    ResourceActionCreators.resetResource()
  }

  onSearch(evt) {
    evt.preventDefault();

    let response = { keyword: this.refs.keyword.value };
    ResourceActionCreators.changeFilter(response);

    browserHistory.push('/th/resource/search');
  }

  onFilterResource(department, evt) {
    this.onClickFilter('department', department);
    ResourceActionCreators.onFilterResource(department);
  }

  onChangeFilter(name, evt) {
    let response = {};
    response[name] = evt.target.value;

    ResourceActionCreators.changeFilter(response);
  }

  onClickFilter(name, value, evt) {
    let response = {};
    response[name] = value;

    ResourceActionCreators.changeFilter(response);
  }

  render() {
    let resourceObj = this.props.resource.department_filter ? this.props.resource.filtered_resource : this.props.resource.resources
    let resourceContent = configLang[this.props.language].resource
    let resourceCategory = configLang[this.props.language].backend.resource.category
    let resourceDepartment = configLang[this.props.language].backend.resource.department

    return (
      <div>
        <div className="resource__cover" style={{ background: "url('/images/resource/Resources Banner.jpg') no-repeat center center", backgroundSize: "cover" }}>
          <div className="cover__text cover__text--resource">
            <div className="cover__text--center">
              <div className="resource__text">
                <h1><span2>{resourceContent.title}</span2></h1>
              </div>
              <div className="resource__search">
                <div className="resource__btn--search">
                  <form onSubmit={this.onSearch.bind(this)}>
                    <div className="input-group">
                      <input ref="keyword" type="text" className="form-control" onChange={this.onChangeFilter.bind(this, 'keyword')} placeholder={resourceContent.search_placeholder} />
                      <span className="input-group-btn">
                        <a href="#" className="btn btn-default" onClick={this.onSearch.bind(this)}>
                          <i className="icon-search"></i>
                        </a>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
              <div className="resource__text">
                <h3 style={{ color: "white" }}>{resourceContent.subtitle}</h3>
              </div>
              <div className="cover__btn">
                {
                  this.props.user.token ?
                  <Link to={ '/' + this.props.language + (this.props.user.type === 'Admin' ? "/admin/resource/index" : "/dashboard/resource/index") }>
                    <button className="btn btn-default" type="button" style={{height: "40px", padding: "5px 30px"}}>
                      <span1>{resourceContent.add}</span1>
                    </button>
                  </Link>
                  :
                  <button className="btn btn-default" data-toggle="modal" data-target="#myModal__login"  type="button" style={{height: "40px", padding: "5px 30px"}}>
                    <span1>{resourceContent.add}</span1>
                  </button>
                }

              </div>
            </div>
          </div>
        </div>

        <div className="resource__menutab">
          <div className="btncontrol prev">
            <button className="btn btn-default"><i className="fa fa-chevron-left"></i></button>
          </div>
          <div className="container btntab">
            <div className="recource__btntab">
              <ul>
                <li>
                  <button onClick={this.onFilterResource.bind(this, "")} className="btn btn-default" style={{ borderBottom: this.props.resource.department_filter === "" ? "5px #f60 solid" : "" }}>{resourceContent.all}</button>
                </li>
                {
                  ConfigData.ResourceDepartment().map((department, index) => (
                    <li key={index}>
                      <button onClick={this.onFilterResource.bind(this, department.toLowerCase())} className="btn btn-default" style={{ borderBottom: department.toLowerCase() === this.props.resource.department_filter  ? "5px #f60 solid" : "" }}>{resourceDepartment[index]}</button>
                    </li>
                  ))
                }
              </ul>
            </div>
          </div>
          <div className="btncontrol next">
            <button className="btn btn-default"><i className="fa fa-chevron-right"></i></button>
          </div>
        </div>

        <div className="container">
          <div className="resource__type">

            <div className="text--center">
              <h2><span5><span>{resourceContent.header1[0]}</span> {resourceContent.header1[1]}</span5></h2>
              <hr className="hr-title" />
            </div>

              <div className="type__banner">
                <div className="row">
                  <div className="col-md-4 col-sm-6">
                    <div className="banner--resource">
                      <Link to='/th/resource/search' onClick={this.onClickFilter.bind(this, 'category', 'videos')}>
                        <div className="resource--photo">
                          <img className="resourcetype__pic" src="/images/resource/Videos.jpg" />
                          <div className="resourcetype__detail">
                            <span1>{resourceCategory[0]}</span1>
                          </div>
                        </div>
                      </Link>
                    </div>
                  </div>

                  <div className="col-md-4 col-sm-6">
                    <div className="banner--resource">
                      <Link to='/th/resource/search' onClick={this.onClickFilter.bind(this, 'category', 'seminars/courses')}>
                        <div className="resource--photo">
                          <img className="resourcetype__pic" src="/images/resource/Seminars Courses.jpg" />
                          <div className="resourcetype__detail">
                            <span1>{resourceCategory[1]}</span1>
                          </div>
                        </div>
                      </Link>
                    </div>
                  </div>

                  <div className="col-md-4 col-sm-6">
                    <div className="banner--resource">
                      <Link to='/th/resource/search' onClick={this.onClickFilter.bind(this, 'category', 'training manuals')}>
                        <div className="resource--photo">
                          <img className="resourcetype__pic" src="/images/resource/Training Manuals.jpg" />
                          <div className="resourcetype__detail">
                            <span1>{resourceCategory[2]}</span1>
                          </div>
                        </div>
                      </Link>
                    </div>
                  </div>

                  <div className="col-md-4 col-sm-6">
                    <div className="banner--resource">
                      <Link to='/th/resource/search' onClick={this.onClickFilter.bind(this, 'category', 'templates/checklists')} >
                        <div className="resource--photo">
                          <img className="resourcetype__pic" src="/images/resource/Templates Checklists.jpg" />
                          <div className="resourcetype__detail">
                            <span1>{resourceCategory[3]}</span1>
                          </div>
                        </div>
                      </Link>
                    </div>
                  </div>

                  <div className="col-md-4 col-sm-6">
                    <div className="banner--resource">
                      <Link to='/th/resource/search'  onClick={this.onClickFilter.bind(this, 'category', 'articles/links')}>
                        <div className="resource--photo">
                          <img className="resourcetype__pic" src="/images/resource/Articles Links.jpg" />
                          <div className="resourcetype__detail">
                            <span1>{resourceCategory[4]}</span1>
                          </div>
                        </div>
                      </Link>
                    </div>
                  </div>


                  <div className="col-md-4 col-sm-6">
                    <div className="banner--resource">
                      <Link to='/th/resource/search' onClick={this.onClickFilter.bind(this, 'category', 'virtual mentorship')}>
                        <div className="resource--photo">
                          <img className="resourcetype__pic" src="/images/resource/Virtual Mentorship.jpg" />
                          <div className="resourcetype__detail">
                            <span1>{resourceCategory[5]}</span1>
                          </div>
                        </div>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>

              <div className="cover__btn" style={{ textAlign: "center" }}>
                {
                  this.props.user.token ?
                  <Link to={this.props.user.type === 'Admin' ? "/admin/resource/index" : "/dashboard/resource/index" }>
                    <button className="btn btn-default" type="button" style={{height: "40px", padding: "5px 30px"}}>
                      <span1>{resourceContent.add}</span1>
                    </button>
                  </Link>
                  :
                  <button className="btn btn-default" data-toggle="modal" data-target="#myModal__login"  type="button" style={{height: "40px", padding: "5px 30px"}}>
                    <span1>{resourceContent.add}</span1>
                  </button>
                }
              </div>

          </div>

          {
            typeof resourceObj !== 'undefined' ?

              ConfigData.ResourceCategoryWithLastest().map((resource_index, index) => {
                let resource_lower = resource_index.toLowerCase()
                if(typeof resourceObj[resource_lower] === 'undefined' || resourceObj[resource_lower].length == 0)
                  return "";

                return (
                  <div className="resource__recent" key={index}>
                    <div className="text--center">
                      <h2><span5>{ resource_lower === 'lastest' ? resourceContent.header2[0] : resourceCategory[ConfigData.ResourceCategory().indexOf(resource_index)] } { resource_lower === 'lastest' ? <span>{resourceContent.header2[1]}</span> : ""}</span5></h2>
                      <hr className="hr-title" />
                    </div>

                    {
                      resourceObj[resource_lower].map((resource, index) => (
                        <ResourceCard key={index} resource={resource} />
                      ))
                    }

                    {
                      resourceObj[resource_lower].length > 4 ?
                        <div className="btn__seemore">
                          <button className="btn btn-default">{ this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</button>
                        </div>
                      : ""
                    }
                  </div>
                )
              })
          : ""
        }





        </div>
      </div>
    );
  }
}

export default ResourceIndex;

/*
style={{ borderBottom: "5px #f60 solid" }}
<h2><span5>LASTEST <span>RESOURCES</span></span5></h2>
*/
