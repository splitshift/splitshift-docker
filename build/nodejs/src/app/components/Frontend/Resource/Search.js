import React, { Component } from 'react';
import { Link } from 'react-router';

import JQueryTool from '../../../tools/JQueryTool';
import ConfigData from '../../../tools/ConfigData';
import configLang from '../../../tools/configLang'

import ResourceActionCreators from '../../../actions/ResourceActionCreators';

class ResourceSearch extends Component {

  componentDidMount() {
    $(".navlist__topic").click(function() {
      $(this).parent(".navlist__filter").children(".navlist__dropdown").toggleClass("open");

      var iconToggle = $(this).children("i");

      if(iconToggle.hasClass("icon-down-dir-1"))
        iconToggle.attr("class", "icon-right-dir-1");
      else
        iconToggle.attr("class", "icon-down-dir-1");
    });

    ResourceActionCreators.searchResource(this.props.resource.filter);
  }

  onChangeFilter(name, evt) {
    let response = {};
    response[name] = evt.target.value;

    ResourceActionCreators.changeFilter(response);
  }

  onClickFilter(name, value, evt) {
    let response = {};
    response[name] = value;

    ResourceActionCreators.changeFilter(response);
  }

  onSearch(evt) {
    evt.preventDefault();
    ResourceActionCreators.searchResource(this.props.resource.filter);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(JSON.stringify(nextProps.resource.filter) !== JSON.stringify(this.props.resource.filter))
      ResourceActionCreators.searchResource(nextProps.resource.filter);
		return true;
	}

  componentWillUnmount() {
    ResourceActionCreators.resetResource()
  }

  render() {
    let resourceContent = configLang[this.props.language].resource
    let resourceCategory = configLang[this.props.language].backend.resource.category
    let resourceDepartment = configLang[this.props.language].backend.resource.department

    return (
      <div>

        <div className="headsearch__resource">
          <div className="search--bar">
            <form onSubmit={this.onSearch.bind(this)}>
              <input type="text" className="form-control" placeholder="keyword ..." onChange={this.onChangeFilter.bind(this, 'keyword')} defaultValue={this.props.resource.filter.keyword} />
              <button className="btn btn-default"><i className="fa fa-search"></i></button>
            </form>
          </div>
        </div>

        <div className="search__resource">

          <div className="row">
            <div className="col-lg-offset-2 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 col-xs-12">

              <div className="col-md-12 col-sm-6 col-xs-6">
                <div className="filter__type">
                  <ul className="nav navbar-nav">
                    <li className="navlist__filter" role="presentation">
                      <span1 className="navlist__topic"><b>{resourceContent.department}</b> <i className="icon-down-dir-1"></i></span1>
                      <ul className="navlist__dropdown open">

                        <li className="navlist__filter" role="presentation">
                          <a href="javascript:void(0)" onClick={this.onClickFilter.bind(this, 'department', '')}>
                            <span1 className={ typeof this.props.resource.filter.department === 'undefined' || this.props.resource.filter.department === '' ? "active" : ""}>{resourceContent.all}</span1>
                          </a>
                        </li>

                        {
                          ConfigData.ResourceDepartment().map((department, index) => (
                            <li className="navlist__filter" role="presentation" key={index}>
                              <a href="javascript:void(0)" onClick={this.onClickFilter.bind(this, 'department', department.toLowerCase())}>
                                <span1 className={ this.props.resource.filter.department && this.props.resource.filter.department == department.toLowerCase() ? "active" : "" }>{resourceDepartment[index]}</span1>
                              </a>
                            </li>
                          ))
                        }
                      </ul>

                    </li>
                  </ul>
                </div>
              </div>

              <div className="col-md-12 col-sm-6 col-xs-6">
                <div className="filter__type">
                  <ul className="nav navbar-nav">
                    <li className="navlist__filter" role="presentation">
                      <span1 className="navlist__topic"><b>{resourceContent.category}</b> <i className="icon-down-dir-1"></i></span1>
                      <ul className="navlist__dropdown open">
                        <li className="navlist__filter" role="presentation">
                          <a href="javascript:void(0)" onClick={this.onClickFilter.bind(this, 'category', '')}>
                            <span1 className={ typeof this.props.resource.filter.category === 'undefined' || this.props.resource.filter.category === '' ? "active" : ""}>{resourceContent.all}</span1>
                          </a>
                        </li>
                        {
                          ConfigData.ResourceCategory().map((category, index) => (
                            <li className="navlist__filter" role="presentation" key={index}>
                              <a href="javascript:void(0)" onClick={this.onClickFilter.bind(this, 'category', category.toLowerCase())}>
                                <span1 className={ this.props.resource.filter.category && this.props.resource.filter.category == category.toLowerCase() ? "active" : "" }>{resourceCategory[index]}</span1>
                              </a>
                            </li>
                          ))
                        }
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>

            </div>

            <div className="col-lg-6 col-md-8 col-sm-12 col-xs-12" style={{ paddingBottom: "16px" }}>

              {
                this.props.resource.resource_array.map((resource, index) => (
                  <Link className="resource-card" to={"/th/resource/article/" + resource._id} key={index} style={{ color: "#4d4948" }}>
                      <img className="resource-card__image" src={ resource.featured_image ? resource.featured_image.substring(6) : "http://img.youtube.com/vi/" + resource.featured_video.substring(24) + "/0.jpg" } />
                      <div className="resource-card__detail">
                        <div className='resource-card__date'>{JQueryTool.changeDateAgo(resource.date)}</div>
                        <div className='resource-card__title'>{resource.title}</div>
                        <div className='resource-card__owner'>{resource.owner.name}</div>
                        <p className='resource-card__text'>{resource.short_description}</p>
                      </div>
                  </Link>
                ))
              }

            </div>

          </div>

        </div>
      </div>
    );
  }
}

export default ResourceSearch;
/*
<div className="btn__seemore">
  <button className="btn btn-default">See More..</button>
</div>
*/
