import React, { Component } from 'react'

import ResourceActionCreators from '../../../actions/ResourceActionCreators';

class ResourceForward extends Component {

  sendForward(evt) {

    evt.preventDefault()

    let email_sender = this.refs.forward_email.value
    let resource_link = "http://www.splitshift.com/th/resource/article/" + this.props.resource._id
    let name = this.props.user.rank === 'Employee' ? this.props.user.first_name + " " + this.props.user.last_name : this.props.user.company_name ;

    let forwardData = "<p>" + name + " share article from www.splitshift.com</p>" +
                      "<p><b>" + this.props.resource.title + "</b></p>" +
                      "<p>" + this.props.resource.short_description + "</p>" +
                      "<p>See Full Article " + resource_link + "</p>"

    let result = {
      to: email_sender,
      data: forwardData
    }

    // Sending Email
    ResourceActionCreators.forwardEmail(this.props.user.token, result);

  }

  render () {

    let draftEmail =  "Share: " + this.props.resource.title +
                      "\n" + this.props.resource.short_description +
                      "\nLink: http://www.splitshift.com/th/resource/article/" + this.props.resource._id

    return (
      <form onSubmit={this.sendForward.bind(this)}>
        <label htmlFor="letter">Forward this resource to</label>
        <input type="text" ref="forward_email" className="form-control form-control--mini" placeholder="Email Address..." />

        <label htmlFor="letter">Message</label>
        <textarea rows="10" placeholder='Dear..' id="letter" className="form-control form-control--mini" defaultValue={draftEmail} readOnly />

        { this.props.sending_forward_email && <span>Sending...</span> }
        <button className="btn btn-default">Send</button>
      </form>
    )
  }
}

export default ResourceForward
