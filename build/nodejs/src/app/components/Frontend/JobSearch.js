import React, { Component } from 'react';
import { Link } from 'react-router';

import MapSearch from './MapSearch';
import ApplyJobModal from './ApplyJobModal';

import SearchActionCreators from '../../actions/SearchActionCreators';
import JobActionCreators from '../../actions/JobActionCreators';

import ConfigData from '../../tools/ConfigData';
import JQueryTool from '../../tools/JQueryTool';
import configLang from '../../tools/configLang'

let new_searchmap = false;
let userComplete = false;
let choose_location = {lat: 13.71129, lng: 100.50908};

class JobSearch extends Component {

  componentDidMount() {
    SearchActionCreators.clearFilter();
    SearchActionCreators.Job({});

    if (navigator.geolocation)
      navigator.geolocation.getCurrentPosition(this.showPosition);
    else
      console.log("Geolocation is not supported by this browser.");

  }

  loadMore(evt) {
    SearchActionCreators.Job(this.props.search.filter, this.props.search.jobs.length / 10)
  }

  componentDidUpdate() {
    if(!userComplete && this.props.user.complete) {
      let response = {};
      response['email'] = this.props.user.email;
      JobActionCreators.updateApplyJobDraft(response);
      userComplete = true;
    }
  }

  chooseJobSearch(index, evt) {
    SearchActionCreators.chooseJobSearch(index);
  }

  updateFilter(name, evt) {
    let response = {};
    response[name] = evt.target.value;
    SearchActionCreators.updateFilter(response);
  }

  updateAdvanceFilter(name, value, evt) {

    let response = {};

    if(value == 'All' || value == 'All Job Types')
      value = "";

    response[name] = value;
    SearchActionCreators.updateFilter(response);
  }

  triggerSearchMap() {
    new_searchmap = false;
  }

  onSearch(evt) {

    evt.preventDefault();

    $('html, body').animate({
        scrollTop: $(this.refs.jobs__jobs).offset().top - 200
    }, 1000);

    SearchActionCreators.Job(this.props.search.filter);
  }

  showPosition(position) {
    choose_location.lat = position.coords.latitude;
    choose_location.lng = position.coords.longitude;
    // SearchActionCreators.Job({ point: "[" + choose_location.lng + "," + choose_location.lat +"]" });
    let response = {};
    response.point = "[" + choose_location.lng + "," + choose_location.lat +"]" ;
    SearchActionCreators.updateFilter(response);
    SearchActionCreators.Job(response);
    new_searchmap = true;
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(!nextProps.search.searching && this.props.search.searching)
      new_searchmap = true;
    return true;
  }

  render() {
    let content = configLang[this.props.language].job_search
    let advancedContent = configLang[this.props.language].advanced_search
    return (
      <div>
        <div className="container">
          <div className="header__search">
            <div className="job__title">
              <p><span>{content.intro}</span></p>
            </div>
            <div className="job__search">
              <form onSubmit={this.onSearch.bind(this)} >
                <div className="job__bar">
                  <label htmlFor="job">{content.job[0]}</label>
                  <input id="job" type="text" className="form-control" onChange={this.updateFilter.bind(this, 'keyword')} placeholder={content.job[1]} />
                </div>
                <div className="location__bar">
                  <label htmlFor="location">{content.location[0]}</label>
                  <input id="location" type="text" className="form-control" onChange={this.updateFilter.bind(this, 'location')} placeholder={content.location[1]} />
                  <button className="btn btn-default ad__search ad__search--desktop" type="button" onClick={JQueryTool.showAdvanceSearch.bind(this)}>{advancedContent.title}</button>
                </div>
                <div className="job__btn">
                  <button className="btn btn-default"><i className="fa fa-search"></i></button>
                  <button className="btn btn-default ad__search ad__search--responsive" type="button" onClick={JQueryTool.showAdvanceSearch.bind(this)}>{advancedContent.title}</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div className="jobbar__detail">
          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.distance ? advancedContent.distance[this.props.search.filter.distance] : advancedContent.distance_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
               {
                 Object.keys(ConfigData.DistanceList()).map((distance_key, index) => (
                  <li key={index} onClick={this.updateAdvanceFilter.bind(this, 'distance', distance_key)}><a href="#" >{advancedContent.distance[distance_key]}</a></li>
                 ))
               }
            </ul>
          </div>

          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.bounded_time ? advancedContent.date_posted[this.props.search.filter.bounded_time] : advancedContent.date_posted_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
              {
                Object.keys(ConfigData.DatePostList()).map((datepost, index) => (
                 <li key={index}><a href="#" onClick={this.updateAdvanceFilter.bind(this, 'bounded_time', datepost)}>{advancedContent.date_posted[datepost]}</a></li>
                ))
              }
            </ul>
          </div>

          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.type ? advancedContent.job_type[ConfigData.JobTypeList().indexOf(this.props.search.filter.type)] : advancedContent.job_type_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
               {
                 ConfigData.JobTypeList().map((jobtype, index) => (
                  <li key={index}><a href="#" onClick={this.updateAdvanceFilter.bind(this, 'type', jobtype)}>{advancedContent.job_type[index]}</a></li>
                 ))
               }
            </ul>
          </div>

          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.compensation ? advancedContent.compensation[ConfigData.CompensationList().indexOf(this.props.search.filter.compensation)] : advancedContent.compensation_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
              {
                ConfigData.CompensationList().map((compensation, index) => (
                 <li key={index}><a href="#" onClick={this.updateAdvanceFilter.bind(this, 'compensation', compensation)}>{advancedContent.compensation[index]}</a></li>
                ))
              }
            </ul>
          </div>

          <div className="btn-group">
            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              { this.props.search.filter.employer ? advancedContent.company_type[ConfigData.CompanyTypeList().indexOf(this.props.search.filter.employer)] : advancedContent.company_type_title } <i className="fa fa-caret-down"></i>
            </button>
            <ul className="dropdown-menu">
              {
                ConfigData.CompanyTypeList().map((employer, index) => (
                 <li key={index}><a href="#" onClick={this.updateAdvanceFilter.bind(this, 'employer', employer)}>{advancedContent.company_type[index]}</a></li>
                ))
              }
            </ul>
          </div>
        </div>

        <div className="container">

          <MapSearch companies={[]} jobs={this.props.search.jobs} new_searchmap={new_searchmap} triggerSearchMap={this.triggerSearchMap.bind(this)} choose_location={choose_location} />

          <div ref="jobs__jobs" className="jobs__jobs">

            {
              this.props.search.jobs.map((job, index) => (
                <a key={index} href="javascript:void(0)" onClick={this.chooseJobSearch.bind(this, index)} data-toggle="modal" data-target="#myModal__positions">
                  <div className="job__item">
                    <div className="job__pic">
                      <img src={job.owner.profile_image ? job.owner.profile_image : "/images/avatar-image.jpg"} />
                    </div>
                    <div className="search__detail">
                      <p><b><span>{job.position} </span></b></p>
                      <p><b><span5>{job.owner.company_name} </span5></b></p>
                      <p><span5>{job.owner.address.city ? job.owner.address.city : "-"} </span5></p>
                      <div className="search__btn">
                        <button className="btn btn-default">{JQueryTool.changeDateAgo(job.date)}</button>
                      </div>
                    </div>
                  </div>
                </a>
              ))
            }


            <div className="btn__seemore">
            {
              this.props.search.load_more ?
                <button onClick={this.loadMore.bind(this)} className="btn btn-default">{ this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</button>
              : ""
            }
            </div>

          </div>

          <ApplyJobModal job={this.props.job} search={this.props.search} user={this.props.user} />


        </div>
      </div>
    );
  }
}

export default JobSearch;
