import React, {Component} from 'react';
import { Link } from 'react-router';

import WhySplitshift from './WhySplitshift'

class Landing extends Component {

  componentDidMount() {
    $('.banner--what').mouseenter(function(){
        $("body").addClass("overflow");
    });
    $('.banner--what').mouseleave(function(){
        $("body").removeClass("overflow");
    });
  }

  render() {
    return (
      <div>
        <div className="cover" style={{ background: "url('/images/landing-black.jpg') no-repeat center center", backgroundSize: "cover" }}>
          <div className="cover__text">
            <div className="cover__text--center">
              <h1><span2>{this.props.resource.heroCover[0]}</span2> <span2>{this.props.resource.heroCover[1]}</span2></h1>
              <h1 style={{marginTop: "0px"}}><span4>{this.props.resource.heroDetail}</span4></h1>
              <div className="cover__btn">
                <button data-toggle="modal" data-target="#myModal__login" className="btn btn-default" type="button">
                  <span1>{this.props.resource.registerBtn}</span1>
                </button>
              </div>
              <h2><span1>{this.props.resource.registerHeader}</span1></h2>
            </div>
          </div>
        </div>

        <WhySplitshift content={this.props.resource.whysplitshift} />

        <div className="whatweoffer">

          <div className="container">
            <div className="text--center">
              <h2><span5>{this.props.resource.wwo[0]} <span>{this.props.resource.wwo[1]}</span> {this.props.resource.wwo[2]}</span5></h2>
              <hr style={{width:"50%"}} />
            </div>

            <div className="gallery">
                <div className="row">
                  <div className="col-md-4">

                    <div className="banner--what">
                      <div className="banner--photo">
                        <img className="banner__pic" src="/images/landing/Showcase your Profile.jpg" />
                        <div className="banner__detail">
                          <span1>{this.props.resource.wwoHeader[0]}</span1>
                        </div>
                      </div>

                      <div className="overlay"></div>

                      <div className="hover--banner hover__left">
                        <img className="hover--pic" src="/images/landing/wwo/Show Case Your Unique Profile.jpg" />
                        <div className="hover--detail">
                          <p><span1>{this.props.resource.wwoDetail[0]}</span1></p>

                          {
                            this.props.user.token ?
                              <Link to={ this.props.user.type === 'Employee' ? '/' + this.props.lang + '/profile' : '/' + this.props.lang + '/company' }>
                                <button className="btn btn-default reverse">GET STARTED</button>
                              </Link>
                            :
                              <button className="btn btn-default reverse" data-toggle="modal" data-target="#myModal__login">
                              GET STARTED
                              </button>
                          }

                        </div>
                      </div>
                    </div>

                  </div>

                  <div className="col-md-4">
                    <div className="banner--what">
                      <div className="banner--photo">
                        <img className="banner__pic" src="/images/landing/Connect with People.jpg" />
                        <div className="banner__detail">
                          <span1>{this.props.resource.wwoHeader[1]}</span1>
                        </div>
                      </div>
                      <div className="overlay"></div>
                      <div className="hover--banner hover__center">
                        <img className="hover--pic" src="/images/landing/wwo/Connect with People.jpg" />
                        <div className="hover--detail">
                          <p><span1>{this.props.resource.wwoDetail[1]}</span1></p>
                          <Link to={ '/' + this.props.lang + '/people-search'}><button className="btn btn-default reverse">CONNECT NOW</button></Link>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="banner--what">
                      <div className="banner--photo">
                        <img className="banner__pic" src="/images/landing/Search Jobs.jpg" />
                        <div className="banner__detail">
                          <span1>{this.props.resource.wwoHeader[2]}</span1>
                        </div>
                      </div>

                      <div className="overlay"></div>
                      <div className="hover--banner hover__right">
                        <img className="hover--pic" src="/images/landing/wwo/Search Jobs.jpg" />
                        <div className="hover--detail">
                          <p><span1>{this.props.resource.wwoDetail[2]}</span1></p>
                          <Link to={ '/' + this.props.lang + '/job-search' }><button className="btn btn-default reverse">SEARCH JOBS</button></Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>



                <div className="row">

                  <div className="col-md-4">
                    <div className="banner--what">
                      <div className="banner--photo">
                        <img className="banner__pic" src="/images/landing/Explore Companies.jpg" />
                        <div className="banner__detail">
                          <span1>{this.props.resource.wwoHeader[3]}</span1>
                        </div>
                      </div>

                      <div className="overlay"></div>
                      <div className="hover--banner hover__left">
                        <img className="hover--pic" src="/images/landing/wwo/Explore Companies.jpg" />
                        <div className="hover--detail">
                          <p><span1>{this.props.resource.wwoDetail[3]}</span1></p>
                          <Link to={ '/' + this.props.lang + '/company-search' }><button className="btn btn-default reverse">BROWSE COMPANIES</button></Link>
                        </div>
                      </div>
                    </div>
                  </div>


                  <div className="col-md-4">
                    <div className="banner--what">
                      <div className="banner--photo">
                        <img className="banner__pic" src="/images/landing/Online Eduction Resources.jpg" />
                        <div className="banner__detail">
                          <span1>{this.props.resource.wwoHeader[4]}</span1>
                        </div>
                      </div>

                      <div className="overlay"></div>
                      <div className="hover--banner hover__center">
                        <img className="hover--pic" src="/images/landing/wwo/Online Education Resources.jpg" />
                        <div className="hover--detail">
                          <p><span1>{this.props.resource.wwoDetail[4]}</span1></p>
                          <Link to={'/' + this.props.lang + '/resource'}><button className="btn btn-default reverse">START LEARNING</button></Link>
                        </div>
                      </div>
                    </div>
                  </div>


                  <div className="col-md-4">
                    <div className="banner--what">
                      <div className="banner--photo">
                        <img className="banner__pic" src="/images/landing/Community Buzz.jpg" />
                        <div className="banner__detail">
                          <span1>{this.props.resource.wwoHeader[5]}</span1>
                        </div>
                      </div>

                      <div className="overlay"></div>
                      <div className="hover--banner hover__right">
                        <img className="hover--pic" src="/images/landing/wwo/Community Buzz.jpg" />
                        <div className="hover--detail">
                          <p><span1>{this.props.resource.wwoDetail[5]}</span1></p>
                          <Link to={'/' + this.props.lang + '/buzz'}><button className="btn btn-default reverse">DISCOVER NOW</button></Link>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

            </div>

            <div className="gallery__responsive">
              <div className="card__what">
                <p><span1><b>{this.props.resource.wwoHeader[0]}</b></span1></p>
                <img className="card--photo" src="/images/landing/wwo/Show Case Your Unique Profile.jpg" />
                <div className="card--detail">
                  <p>
                    {this.props.resource.wwoDetail[0]}
                  </p>

                  {
                    this.props.user.token ?
                      <Link to={ this.props.user.type === 'Employee' ? '/th/profile' : '/th/company' }>
                        <button className="btn btn-default reverse">GET STARTED</button>
                      </Link>
                    :
                      <button className="btn btn-default reverse" data-toggle="modal" data-target="#myModal__login">
                      GET STARTED
                      </button>
                  }

                </div>
              </div>

              <div className="card__what">
                <p><span1><b>{this.props.resource.wwoHeader[1]}</b></span1></p>
                <img className="card--photo" src="/images/landing/wwo/Connect with People.jpg" />
                <div className="card--detail">
                  <p>
                    {this.props.resource.wwoDetail[1]}
                  </p>
                  <Link to={ '/' + this.props.lang + '/people-search' }><button className="btn btn-default reverse">CONNECT NOW</button></Link>
                </div>
              </div>

              <div className="card__what">
                <p><span1><b>{this.props.resource.wwoHeader[2]}</b></span1></p>
                <img className="card--photo" src="/images/landing/wwo/Search Jobs.jpg" />
                <div className="card--detail" >
                  <p>
                    {this.props.resource.wwoDetail[2]}
                  </p>
                  <Link to={ '/' + this.props.lang + '/job-search' }><button className="btn btn-default reverse">SEARCH JOBS</button></Link>
                </div>
              </div>

              <div className="card__what">
                <p><span1><b>{this.props.resource.wwoHeader[3]}</b></span1></p>
                <img className="card--photo" src="/images/landing/wwo/Explore Companies.jpg" />
                <div className="card--detail">
                  <p>
                    {this.props.resource.wwoDetail[3]}
                  </p>
                  <Link to={ '/' + this.props.lang + '/company-search' }><button className="btn btn-default reverse">BROWSE COMPANIES</button></Link>
                </div>
              </div>

              <div className="card__what">
                <p><span1><b>{this.props.resource.wwoHeader[4]}</b></span1></p>
                <img className="card--photo" src="/images/landing/wwo/Online Education Resources.jpg" />
                <div className="card--detail">
                  <p>
                    {this.props.resource.wwoDetail[4]}
                  </p>
                  <Link to={ '/' + this.props.lang + '/resource' }><button className="btn btn-default reverse">START LEARNING</button></Link>
                </div>
              </div>

              <div className="card__what">
                <p><span1><b>{this.props.resource.wwoHeader[5]}</b></span1></p>
                <img className="card--photo" src="/images/landing/wwo/Community Buzz.jpg" />
                <div className="card--detail">
                  <p>
                    {this.props.resource.wwoDetail[5]}
                  </p>
                  <Link to={ '/' + this.props.lang + '/buzz' }><button className="btn btn-default reverse">DISCOVER NOW</button></Link>
                </div>
              </div>

            </div>

            <div className="text--center">
              <div className="whysplitshift__btn--profile">
                <button data-toggle="modal" data-target="#myModal__login" className="btn btn-default" type="button">
                  <span1>{this.props.resource.registerBtn}</span1>
                </button>
              </div>
            </div>

          </div>
        </div>

        <div id="splitshiftstat">
          <div className="splitshiftstatcontainer" style={{ background: "url('/images/stats.jpg') no-repeat center center", backgroundSize: "cover", filter: "alpha(opacity=50)" }}>

            <div className="text--center">
              <h2><span1 style={{marginBottom: "50px"}}>{this.props.resource.stat[0]} <span>{this.props.resource.stat[1]}</span></span1></h2>
            </div>

            <div className="splitshiftstat__stat">
              <div className='stat__item'>
                <img src='/images/stats/member.png' />
                <h5>{this.props.stat.members}</h5>
                <h6>{this.props.resource.stat[2]}</h6>
              </div>

              <div className='stat__item'>
                <img src='/images/stats/job.png' />
                <h5>{this.props.stat.jobs}</h5>
                <h6>{this.props.resource.stat[3]}</h6>
              </div>

              <div className='stat__item'>
                <img src='/images/stats/company.png' />
                <h5>{this.props.stat.companies}</h5>
                <h6>{this.props.resource.stat[4]}</h6>
              </div>
            </div>

          </div>
        </div>

      </div>
    );
  }
}

export default Landing;
