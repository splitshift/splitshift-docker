import React, { Component } from 'react';
import { Link } from 'react-router';

import NewsletterActionCreators from '../../actions/NewsletterActionCreators';
import ResourceActionCreators from '../../actions/ResourceActionCreators';
import configLang from '../../tools/configLang'

class Footer extends Component {

  postNewsletter(evt) {
    evt.preventDefault();
    let info = { email: this.refs.newsletter_email.value };
    NewsletterActionCreators.postNewsletter(info);
  }

  changeLanguage(evt) {
    ResourceActionCreators.setLanguage(evt.target.value);
  }

  render() {
    let data = configLang[this.props.language].footer
    return (
      <footer>
        <div className="container">
          <div className="row">
              <div className="col-md-4 col-sm-3">
                <h3>{data.mail}</h3>
                <a href="mailto:info@splitshift.com" id="email_footer">info@splitshift.com</a>
              </div>
              <div className="col-md-3 col-sm-3">
                <h3>{data.about[0]}</h3>
                <ul>
                    <li><Link to={'/' + this.props.language + '/aboutus' }>{data.about[1]}</Link></li>
                    {
                      typeof this.props.user.token === 'undefined' ?
                      <span>
                        <li><a href="#" data-toggle="modal" data-target="#myModal__login" >{data.about[2]}</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#myModal__login" >{data.about[3]}</a></li>
                      </span>
                      : ""
                    }
                    <li><Link to={'/' + this.props.language + "/privacy" }>{data.about[4]}</Link></li>
                    <li><Link to={'/' + this.props.language + "/user-agreement" }>{data.about[5]}</Link></li>
                </ul>
              </div>
              <div className="col-md-3 col-sm-3">
                <h3>{data.discover[0]}</h3>
                <ul>
                    <li><Link to={'/' + this.props.language + "/buzz" }>{data.discover[1]}</Link></li>
                    <li><Link to={'/' + this.props.language + "/job-search" }>{data.discover[2]}</Link></li>
                    <li><Link to={'/' + this.props.language + "/company-search" }>{data.discover[3]}</Link></li>
                    <li><Link to={'/' + this.props.language + "/people-search" }>{data.discover[4]}</Link></li>
                    <li><Link to={'/' + this.props.language + "/resource" }>{data.discover[5]}</Link></li>
                    {
                      typeof this.props.user.token === 'undefined' ?
                        <li><a href="#" data-toggle="modal" data-target="#myModal__login" ><span>{data.discover[6]}</span></a></li>
                      : ""
                    }
                </ul>
              </div>
              <div className="col-md-2 col-sm-3">
                <h3>{data.setting[0]}</h3>

                <div className="fstyled-select">
                  <select className="form-control" name="lang" id="lang" onChange={this.changeLanguage.bind(this)}>
                    <option value="">{data.setting[1]}</option>
                    <option value="th">Thai</option>
                    <option value="en">English</option>
                  </select>
                </div>

                <div className="fstyled-select">
                  <select className="form-control" name="contries" id="contries">
                    <option value="">{data.setting[2]}</option>
                    <option value="THA">Thailand</option>
                  </select>
                </div>
              </div>
          </div>

          <div className="row" id="social">
              <div className="col-md-6">
                  <div id="social_footer">
                      <ul>
                          <li><a href="https://www.facebook.com/groups/splitshiftasia/" target="_blank"><i className="icon-facebook"></i></a></li>
                          <li><a href="https://plus.google.com/u/0/b/111900430980684152993/111900430980684152993/about" target="_blank"><i className="icon-google"></i></a></li>
                          <li><a href="https://www.instagram.com/splitshiftthailand/" target="_blank"><i className="icon-instagram"></i></a></li>
                          <li><a href="https://www.youtube.com/channel/UC2f6uj4wk7tOv6KUWaZgVDA" target="_blank"><i className="icon-youtube-play"></i></a></li>
                          <li><a href="https://www.linkedin.com/company/6447802?trk=tyah&trkInfo=clickedVertical%3Acompany%2CentityType%3AentityHistoryName%2CclickedEntityId%3Acompany_6447802%2Cidx%3A0" target="_blank"><i className="icon-linkedin"></i></a></li>
                      </ul>
                  </div>
              </div>
              <div className="col-md-5">
                <div className="subscribe-form">
                  <p><span1><strong>{data.newsletter[0]}</strong></span1></p>
                  <form onSubmit={this.postNewsletter.bind(this)}>
                    <div className="input-group">
                        <input ref="newsletter_email" type="text" className="form-control" placeholder={data.newsletter[1]} />
                        <span className="input-group-btn">
                          <button id="input-group-footer" className="btn btn-default">
                          {data.newsletter[2]}
                          </button>
                        </span>
                    </div>
                </form>
              </div>
            </div>
          </div>
          <div className="credit"><p>© Splitshift 2017</p></div>

        </div>
      </footer>
    );
  }
}

export default Footer;
