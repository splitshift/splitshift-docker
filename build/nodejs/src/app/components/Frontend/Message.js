import React, { Component } from 'react';

import MessageActionCreators from '../../actions/MessageActionCreators';

let lastImageLength = 0

class Message extends Component {

  updateDraft(type, evt) {
    let value = evt.target.value;
    MessageActionCreators.setMessageDraft(type, value, this.props.profile_key);
  }

  setFileType(type, evt) {
    $(this.refs.upload_file).data('name', type);
    $(this.refs.upload_file).click();
  }

  sendMessage(evt) {
    evt.preventDefault();
    this.refs.message_box.value = "";

    MessageActionCreators.sendMessage(this.props.user.token, this.props.msg.draft);
  }

  sendFile(evt) {
    MessageActionCreators.sendFile(this.props.user.token, $(evt.target).data('name'), evt.target.files[0], this.props.profile_key);
  }

  reloadMessage(evt) {
    MessageActionCreators.getMessageWithPeople(this.props.user.token, this.props.profile_key);
  }

  componentDidUpdate() {

     if($('.popup-photo').length > lastImageLength) {
       $('.popup-photo').magnificPopup({
         type: 'image'
       });
       lastImageLength = $('.popup-photo').length
     }

    if($(".mesg__chat")[0].scrollHeight > 0)
      $(".mesg__chat").scrollTop($(".mesg__chat")[0].scrollHeight);
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.msg.uploading && this.props.msg.uploading){
      MessageActionCreators.sendMessage(nextProps.user.token, nextProps.msg.draft);
    }else if(!nextProps.msg.sending && this.props.msg.sending)
      this.reloadMessage()

    return JSON.stringify(nextProps.msg) !== JSON.stringify(this.props.msg);
  }

  render() {

    return (
      <div className="modal fade" id="myModal__message" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
    		<div className="modal-dialog" role="document">
          <form onSubmit={this.sendMessage.bind(this)}>
        		<div className="modal-content">

              <div className="modal-header">
                <div className="modal-title">
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span5 aria-hidden="true">&times;</span5>
                  </button>
                  <b><span>Send Message</span></b>
                </div>

              </div>

          		<div className="modal-body">

                <div className="mesg__chat">

                  {
                    this.props.msg.messages.length > 0 ? this.props.msg.messages.map((message, index) => (
                      <div key={index} className={this.props.user.key == message[0].owner ? "mesg--send" : "mesg--recieve"}>
                        <div className="mesg__pic">
                          <img src={ this.props.msg.users[index % 2].profile_image ? this.props.msg.users[index % 2].profile_image : "/images/avatar-image.jpg" }/>
                        </div>
                        <div className="mesg_conversation">
                          { message.map((m, i) => (
                            <div key={i}>
                              { m.message_type == 'text' ? <p>{m.message}</p> : ""}
                              { m.message_type == 'image' ? <a className="popup-photo" href={m.message.substring(6)} style={{display: "block", marginBottom: "20px"}}><img src={m.message.substring(6)} style={{ maxWidth: "200px" }} /></a> : ""}
                              { m.message_type == 'file' ? <a href={m.path ? m.path.substring(6) : m.message.substring(6)} target="_blank" download><p>{m.message}</p></a> : "" }
                            </div>))
                          }
                          {
                            this.props.msg.uploading &&
                            <div>
                              <p>Uploading...</p>
                            </div>
                          }
                        </div>
                      </div>
                    )) : ""
                  }

                </div>

                <textarea ref="message_box" onChange={this.updateDraft.bind(this, 'text')} rows="1" placeholder='Type a message..'></textarea>

          		</div>

          		<div className="modal-footer">
                <div className="mesg__function">
                  <a href="#"><button className="modal__btn--light" type="button" onClick={this.setFileType.bind(this, "image")} ><i className="fa fa-picture-o" aria-hidden="true"></i></button></a>{' '}
                  <a href="#"><button className="modal__btn--light" type="button" onClick={this.setFileType.bind(this, "file")}><i className="fa fa-paperclip" aria-hidden="true"></i></button></a>

                  <input ref="upload_file" onChange={this.sendFile.bind(this)} type="file" data-name="file" style={{display: "none"}} />
                </div>
                <div>
                  <a className="hide-refresh" href="javascript:void(0)" onClick={this.reloadMessage.bind(this)}>
                    Refresh <i className="fa fa-refresh" aria-hidden="true" style={{ fontSize: '16px' }}></i>
                  </a>
                  <a href="#"><button className="modal__btn--light" >Send</button></a>
                  <a className="hide-refresh__responsive" href="javascript:void(0)" onClick={this.reloadMessage.bind(this)}>
                    Refresh <i className="fa fa-refresh" aria-hidden="true" style={{ fontSize: '16px' }}></i>
                  </a>

                </div>

          		</div>
        		</div>
          </form>
    		</div>
    	</div>
    );
  }
}

export default Message;
