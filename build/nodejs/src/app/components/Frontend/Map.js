import React, { Component } from 'react';

let map1 = {};
let marker1 = {};
let self;

const defaultLat = 13.7563309;
const defaultLng = 100.5017651;

class Map extends Component {

  handleEvent(evt) {
    self.props.changeLatLng(evt.latLng.lat(), evt.latLng.lng());
  }

  initialize(LagLngObj) {

    // let LagLngObj = new google.maps.LatLng(defaultLat, defaultLng);

    let mapProp = {
      center: LagLngObj,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map1[this.props.map_id] = new google.maps.Map(document.getElementById(this.props.map_id), mapProp);
    marker1[this.props.map_id] = new google.maps.Marker({
      map: map1[this.props.map_id],
      position: LagLngObj,
      draggable: true
    });

    if(this.props.map_id === 'googleMap2')
      this.props.triggerMap(map1[this.props.map_id])

    marker1[this.props.map_id].addListener('dragend', this.handleEvent);

  }

  componentDidMount() {
    self = this;

    // if(this.props.address)
    //   this.props.changeLatLng(this.props.address.lat ? this.props.address.lat : defaultLat,	this.props.address.lng ? this.props.address.lng : defaultLng);

    let newLatlng;

    if(this.props.address.location[0] === 1 && this.props.address.location[1]) {
      newLatlng = new google.maps.LatLng(defaultLat, defaultLng);
      self.props.changeLatLng(defaultLat, defaultLng);
    }else
      newLatlng = new google.maps.LatLng(this.props.address.location[1], this.props.address.location[0]);

    // this.props.changeLatLng

    google.maps.event.addDomListener(window, 'load', this.initialize(newLatlng));
  }

  componentDidUpdate() {

    // console.log("Location");
    // console.log(this.props.address.location);

    // marker1[this.props.map_id].setPosition( new google.maps.LatLng( this.props.address.location[1], this.props.address.location[0] ) );
    // map1[this.props.map_id].setCenter(new google.maps.LatLng( this.props.address.location[1], this.props.address.location[0] ));
  }

  render() {
    return (
      <div id={this.props.map_id}>
        <span style={{ color: "#aaa" }}>loading...</span>
      </div>
    )
  }
}

export default Map;
