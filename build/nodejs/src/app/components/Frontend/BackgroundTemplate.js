import React, { Component } from 'react';

import ConfigData from '../../tools/ConfigData';

class BackgroundTemplate extends Component {
  render() {
    return (
      <div className="modal fade" id="myModal__background" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span5 aria-hidden="true">&times;</span5>
              </button>
              <div className="modal-title">
                  <b>
                    <span>Choose Your Background</span>
                  </b>
              </div>
            </div>

            <div className="modal-body">
              <div className="row">
                {
                  ConfigData.BackgroundTemplateList().map((template, index) => (
                    <div key={index} className="col-md-3 col-xs-6" style={{ paddingRight: "0px", paddingLeft: "0px", cursor: "pointer"}} >
                      <div onClick={this.props.changeBannerImageWithTemplate.bind(null, template + ".jpg")} className="photo__pic" style={{ background: "url(" + template + "-small.jpg) no-repeat center center", backgroundSize: "cover" }} />
                    </div>
                  ))
                }
              </div>

            </div>

          </div>
        </div>
      </div>
    )
  }
}

export default BackgroundTemplate;
