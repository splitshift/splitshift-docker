import React, { Component } from 'react';

class Aboutus extends Component {

  render() {
    return (

      <div>
        <div className="cover cover__aboutus" style={{ background: "url('/images/aboutus.jpg') no-repeat center center", backgroundSize: "cover"}}>
          <div className="cover__text">
            <div className="cover__text--center">
              <h1><span2>{this.props.aboutus.header}</span2></h1>
            </div>
          </div>
        </div>

        <div>
          <div className="whatweoffer">
            <div className="container container__aboutme">
              <div className="text--center">
                <h2 dangerouslySetInnerHTML={{ __html: this.props.aboutus.vision.header }} />
                <hr style={{width:"50%"}} />

                <div className="row">
                  <div className="col-md-12">
                    <div className="whysplitshift__text">
                      {this.props.aboutus.vision.detail}
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div className="whatweoffer">
            <div className="container container__aboutme" >
              <div className="text--center">
                <h2 dangerouslySetInnerHTML={{ __html: this.props.aboutus.whatis.header }} />
                <hr style={{width:"50%"}} />

                <div className="row">
                  <div className="col-md-12">
                    <div className="whysplitshift__text">
                      {this.props.aboutus.whatis.detail}
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div className="whatweoffer">
            <div className="container container__aboutme" >
              <div className="text--center">
                <h2 dangerouslySetInnerHTML={{ __html: this.props.aboutus.why.header }} />
                <hr style={{width:"50%"}} />

                <div className="row">
                  <div className="col-md-12">
                    <div className="whysplitshift__text">
                      { this.props.aboutus.why.detail.map((d, index) => <p key={index}>{d}</p>) }
                    </div>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>


      </div>
    )
  }
}

export default Aboutus;
