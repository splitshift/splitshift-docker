import React, { Component } from 'react';

import SettingActionCreators from '../../actions/SettingActionCreators';

import configLang from '../../tools/configLang'

class Setting extends Component {

  changeDraft(name, evt) {
    let response = {};
    response[name] = evt.target.value;

    SettingActionCreators.changeDraft(response);

  }

  changePassword(evt) {
    evt.preventDefault()
    if(this.props.user.setting.new_password !== this.props.user.setting.new_password_again) {
      alert("New password not same");
      return "";
    }

    let result = {
      old_password: this.props.user.setting.old_password,
      new_password: this.props.user.setting.new_password
    };

    this.refs.old_password.value = "";
    this.refs.new_password.value = "";
    this.refs.new_password_again.value = "";

    SettingActionCreators.changePassword(this.props.user.token, result);

  }

  changePrimaryEmail(evt) {
    evt.preventDefault();

    let result = {
      email: this.props.user.setting.new_primary_email,
    };

    SettingActionCreators.changePrimaryEmail(this.props.user.token, result);

  }


  deleteAccount(evt) {
    if(confirm('confirm to delete your account'))
      SettingActionCreators.deleteAccount(this.props.user.token);
  }

  render() {
    let settingContent = configLang[this.props.language].setting
    return (
      <div className="container" style={{ marginTop: "50px" }}>

        <h1 style={{ marginBottom: "20px" }}>{settingContent.title}</h1>

        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">{settingContent.delete[0]}</h3>
          </div>
          <div className="panel-body">
            <p>
              {settingContent.delete[1]}
            </p>
            <button onClick={this.deleteAccount.bind(this)} className="btn btn-danger">{settingContent.delete[2]}</button>
          </div>
        </div>


        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">{settingContent.password[0]}</h3>
          </div>
          <div className="panel-body">
            <form onSubmit={this.changePassword.bind(this)}>
              <p>
                {settingContent.password[1]}
                <input type="password" ref="old_password" className="form-control form-control--mini" placeholder={settingContent.password[2]} onChange={this.changeDraft.bind(this, 'old_password')} /> <br />
                {settingContent.password[3]}
                <input type="password" ref="new_password" className="form-control form-control--mini" placeholder={settingContent.password[4]} onChange={this.changeDraft.bind(this, 'new_password')} style={{ marginTop: "10px"}} />
                <input type="password" ref="new_password_again" className="form-control form-control--mini" placeholder={settingContent.password[5]} onChange={this.changeDraft.bind(this, 'new_password_again')}  /> <br />
              </p>
              <button className="btn btn-default">{settingContent.password[6]}</button>
            </form>
          </div>
        </div>

        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">{settingContent.primary[0]}</h3>
          </div>
          <div className="panel-body">
            <form onSubmit={this.changePrimaryEmail.bind(this)}>
              <p>
                <p>{settingContent.password[1]}: <b>{this.props.user.email}</b> </p>

                {settingContent.password[2]}
                <input type="text" className="form-control form-control--mini" placeholder={settingContent.password[3]} onChange={this.changeDraft.bind(this, 'new_primary_email')}/>
              </p>

              <button className="btn btn-info">{settingContent.password[4]}</button>
            </form>
          </div>
        </div>

      </div>
    )
  }

}

export default Setting;
