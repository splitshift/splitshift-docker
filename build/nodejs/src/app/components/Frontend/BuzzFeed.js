import React, { Component } from 'react';

import FormFeedPost from './Form/Feed/Post';
import FormFeedCard from './Form/Feed/Card';

import FeedActionCreators from '../../actions/FeedActionCreators';
import configLang from '../../tools/configLang'

let firstUser = false;

class BuzzFeed extends Component {

  componentDidMount() {
    firstUser = false;
    FeedActionCreators.getBuzzFeed();
  }

  componentDidUpdate() {
    if(!firstUser && this.props.user.complete){
      FeedActionCreators.setOwner(this.props.user);
      firstUser = true;
    }
  }

  componentWillUnmount() {
    FeedActionCreators.resetFeed()
  }

  loadMore(evt) {
    FeedActionCreators.getBuzzFeed(this.props.feed.feeds.length / 10);
  }

  searchBuzzFeed(evt) {
    evt.preventDefault();
    FeedActionCreators.searchBuzzFeed(this.refs.buzz_search_desktop.value || this.refs.buzz_search_responsive.value);
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.feed.deleting && this.props.feed.deleting)
      FeedActionCreators.getBuzzFeed();


    return true;
  }

  render() {

    let buzzContent = configLang[this.props.language].buzz

    return (
      <div className="container" style={{marginBottom: "100px", paddingRight: "0px", paddingLeft: "0px" }}>

        <div className="desktop__search" style={{paddingTop: "80px"}} />

        <div className="suggest__responsive">
          <div className="col-md-12">
            <form onSubmit={this.searchBuzzFeed.bind(this)}>
              <div className="buzz__search" style={{ marginTop: "70px", marginBottom: "30px", backgroundColor: "white", paddingBottom: "10px", boxShadow: "0 1px 4px -1px rgba(170,170,170,.1)", paddingTop: "20px", height: "80px", borderRadius: "4px" }} >
                <input ref="buzz_search_responsive" type="text" className="form-control form-control--mini" placeholder={buzzContent.search} />
                <button className="btn btn-default"><i className="fa fa-search"></i></button>
              </div>
            </form>
          </div>

          {
            typeof this.props.user.token !== 'undefined' ?
              <div className="col-md-12">
                <FormFeedPost language={this.props.language} user={this.props.user} profile={this.props.user} feed={this.props.feed} />
              </div>
            : ""
          }

        </div>

        <div className="col-md-6 buzz__newsfeed left">
          <div className="newsfeed">
            {
              typeof this.props.user.token !== 'undefined' ?
                <div className="desktop__search">
                  <FormFeedPost language={this.props.language} user={this.props.user} profile={this.props.user} feed={this.props.feed} />
                </div>
              : ""
            }

            {
              this.props.feed.feeds.map((b, index) => (
                index < this.props.feed.feeds.length/2 ? <FormFeedCard language={this.props.language} key={index} user={this.props.user} feed={b} index={index} comment_id={this.props.feed.comment_for_reply_id} buzz={true} /> : ""
              ))
            }

          </div>
        </div>

       <div className="col-md-6 buzz__newsfeed right">
         <div className="newsfeed">
            <form onSubmit={this.searchBuzzFeed.bind(this)}>
             <div className="buzz__search desktop__search" style={{ backgroundColor: "white", paddingBottom: "10px", boxShadow: "0 1px 4px -1px rgba(170,170,170,.1)", paddingTop: "20px", height: "80px", borderRadius: "4px" }} >
               <input ref="buzz_search_desktop" type="text" className="form-control form-control--mini" placeholder={buzzContent.search} />
               <button className="btn btn-default"><i className="fa fa-search"></i></button>
             </div>
           </form>

           {
             this.props.feed.feeds.map((b, index) => (
               index >= this.props.feed.feeds.length/2 ? <FormFeedCard language={this.props.language} key={index} user={this.props.user} feed={b} index={index} comment_id={this.props.feed.comment_for_reply_id} buzz={true} /> : ""
             ))
           }

         </div>
       </div>


       {
         this.props.feed.load_more ?
          <div className="col-md-12">
           <div className="btn__seemore">
             <button onClick={this.loadMore.bind(this)} className="btn btn-default">{ this.props.language === 'th' ? 'ดูข้อมูลเพิ่ม' : 'See More' }</button>
           </div>
          </div>
         : ""
       }

      </div>
    );
  }
}

export default BuzzFeed;
