import React, { Component } from 'react';
import { Link } from 'react-router';

import JobActionCreators from '../../actions/JobActionCreators';

class ApplyJobModal extends Component {

  uploadFile(evt) {
    JobActionCreators.uploadFileApplyJob(this.props.user.token, evt.target.files[0]);
  }

  updateDraft(name, evt) {
    let response = {};
    response[name] = evt.target.value;

    JobActionCreators.updateApplyJobDraft(response);
  }

  sendApplyJob(evt) {
    let jobForm = this.props.job.apply_draft;
    let allFilePath = jobForm.files ? jobForm.files.map((file, key) => {
      return file.path;
    }) : [];

    jobForm.job_id = this.props.search.jobs[this.props.search.job_index]._id;
    jobForm.to = this.props.search.jobs[this.props.search.job_index].owner.key;
    jobForm.files = allFilePath;

    JobActionCreators.sendApplyJob(this.props.user.token, jobForm);
  }

  sendForward(evt) {

    let email_sender = this.refs.forward_email.value
    let currentJob = this.props.search.jobs[this.props.search.job_index]

    let name = this.props.user.rank === 'Employee' ? this.props.user.first_name + " " + this.props.user.last_name : this.props.user.company_name ;

    let forwardData = "<p>" + name + " found a job you may be interested in at www.splitshift.com</p><br />" +
                      "<p><b>Position</b>: " + currentJob.position + "</p>" +
                      "<p><b>Type</b>: " + currentJob.type + "</p>" +
                      "<p><b>Min.Required Experience</b>: " + currentJob.min_year + "</p>" +
                      "<p><b>Compensation</b>: " + currentJob.compensation + "</p>" +
                      "<p><b>Job Details</b>:<br />" + currentJob.description + "</p>" +
                      "<p><b>Required Skills</b>:<br />" + (currentJob.required_skills.map((skill, index) => skill)) + "</p>" +
                      "<p><br />See more detail: <a href='http://www.splitshift.com/th/company/" + currentJob.owner.key + "'>http://www.splitshift.com/th/company/" + currentJob.owner.key + "</a></p>"

    let result = {
      to: email_sender,
      data: forwardData
    }

    // Sending Email
    JobActionCreators.forwardEmail(this.props.user.token, result);

  }

  openApplyJobForm(evt) {
    $(".btn-command").show();
    $(evt.target).hide();
    $(".forward--form").hide();
    $(".application--form").show();
  }

  forwardApplyJob(evt) {
    $(".btn-command").show();
    $(evt.target).hide();
    $(".forward--form").show();
    $(".application--form").hide();
  }

  componentWillUnmount() {
    $("#myModal__positions").modal('hide');
  }

  render() {

    let currentJob = this.props.search.jobs[this.props.search.job_index]
    let forwardText = this.props.search.job_index !== null ?
                            "Position: " + currentJob.position +
                            "\nType: " + currentJob.type +
                            "\nMin.Required Experience: " + currentJob.min_year +
                            "\nCompensation: " + currentJob.compensation +
                            "\n\nJob Details:\n" + currentJob.description +
                            "\n\nRequired Skills:\n" + (currentJob.required_skills.map((skill, index) => skill))
                           : "";

    return (
      <div className="modal fade" id="myModal__positions" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span5 aria-hidden="true">&times;</span5>
                </button>
                <div className="modal-title">
                    <b>
                      <span>{this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].position }</span>
                    </b>
                </div>
              </div>

              <div className="modal-body">
                <div className="job--description">
                  <p>

                    <b>Type</b> <br />
                    <span>{this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].type }</span> <br />
                    <br />

                    <b>Min.Required Experience</b> <br />
                    <span>{this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].min_year } years</span> <br />
                    <br />

                    <b>Compensation</b> <br />
                    <span>{this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].compensation } (Based on Experience)</span> <br />
                    <br />

                    <b>Job Details</b> <br />
                    <div className="text--detail">
                      <span>{this.props.search.job_index != null && this.props.search.jobs[this.props.search.job_index].description }</span>
                    </div>
                    <br />

                    <b>Required Skills</b> <br />
                    {
                      this.props.search.job_index != null &&  this.props.search.jobs[this.props.search.job_index].required_skills.map((skill, index) => (
                        <span key={index}>
                          <i className="fa fa-star"></i> {skill} <br />
                        </span>
                      ))
                    }

                  </p>
                </div>
              </div>

              <div className="modal-footer">

                <div style= {{ textAlign: "center" }}>

                  {
                    this.props.search.job_index != null && typeof this.props.user.token !== 'undefined'?
                      <button onClick={this.forwardApplyJob.bind(this)} className="modal__btn--light modal__btn--left modal__btn--responsive btn-command">Forward</button>
                    : ""
                  }


                  <div>

                    {
                      this.props.user.token && this.props.user.type === 'Employee' ?
                        <button className="modal__btn--light modal__btn--right modal__btn--responsive btn-command applyjob none" onClick={this.openApplyJobForm.bind(this)} >Apply Now</button>
                      : ""
                    }

                    <Link to={this.props.search.job_index != null ? '/th/company/' + this.props.search.jobs[this.props.search.job_index].owner.key : '/th/company/'}><button className="modal__btn--light modal__btn--right modal__btn--responsive" >See Company Profile</button></Link>

                  </div>
                </div>


                {
                  this.props.user.token ?
                  <div className="application--form" style={{ clear: "both" }}>
                      <label htmlFor="letter">Cover Letter</label>
                      <textarea rows="5" onChange={this.updateDraft.bind(this, 'cover_letter')}  placeholder='Dear..' id="letter"></textarea>
                      <div className="col-md-6 col-sm-12" style={{ padding: "0px", paddingRight: "5px" }}>
                        <label htmlFor="phone">Phone</label>
                        <input type="text" onChange={this.updateDraft.bind(this, 'tel')} className="form-control form-control--mini" placeholder="Phone Number..." id="phone" />
                      </div>
                      {
                        this.props.user.complete ?
                        <div className="col-md-6 col-sm-12" style={{ padding: "0px" }}>
                          <label htmlFor="email">Email</label>
                          <input type="text" onChange={this.updateDraft.bind(this, 'email')} className="form-control form-control--mini" placeholder="Email Address..." id="email" defaultValue={this.props.user.email} />
                        </div>
                        : ""
                      }
                      <div className="attach__file">
                        <p><b>File Attachments</b></p>
                        {
                          this.props.search.job_index != null && typeof this.props.job.apply_draft.files !== "undefined" ?
                            <div style={{textAlign: "left"}}>
                              {
                                this.props.job.apply_draft.files.map((file, index) => (
                                  <div key={index} ><i className="fa fa-star"></i> {file.original_name} <br /></div>
                                ))
                              }
                            </div>
                          : ""
                        }
                        <input onChange={this.uploadFile.bind(this)}  type="file" name="attachfile" />
                      </div>
                    <button   onClick={this.sendApplyJob.bind(this)}  className="modal__btn--light modal__btn--responsive" >Submit Application</button>
                  </div>
                  : ""
                }


                {
                  this.props.user.token &&
                  <div className="forward--form" style={{ clear: "both" }}>

                    <label htmlFor="letter">Forward this job to</label>
                    <input type="text" ref="forward_email" className="form-control form-control--mini" placeholder="Email Address..." />

                    <label htmlFor="letter">Message</label>

                    {
                      this.props.search.job_index != null &&
                      <textarea rows="10" placeholder='Dear..' id="letter" value={forwardText} readOnly />
                    }

                    { this.props.job.sending_forward_email && <span>Sending...</span> }
                    {' '}
                    <button onClick={this.sendForward.bind(this)} className="modal__btn--light modal__btn--responsive" style={{ marginTop: "10px" }}>Forward</button>

                  </div>
                }


              </div>
            </div>
          </div>
      </div>
    )
  }
}

export default ApplyJobModal;
