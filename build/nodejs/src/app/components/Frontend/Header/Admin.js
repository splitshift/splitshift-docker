import React, {Component} from 'react';
import { Link } from 'react-router';

import DrawerHeader from './Drawer';
import NavList from './NavList';
import SearchBar from './SearchBar';
import AuthenticationActionCreators from '../../../actions/AuthenticationActionCreators';

import JQueryTool from '../../../tools/JQueryTool';
const menuClass = ['profile--menu', 'noti--menu', 'mesg--menu', 'connect--menu'];

class AdminHeader extends Component {

  onLogout() {
    AuthenticationActionCreators.logout();
  }

  triggerMenu(name, evt) {
    evt.stopPropagation();

    menuClass.map((menu, index) => {
      if(name == menu)
        $("." + name).toggle();
      else
        $("." + menu).hide();
    });
  }

  componentDidMount() {
    JQueryTool.drawer();
  }

  render() {
    return (
      <header id="header--bar">
        <div className="topbar">
          <div className="container">
            <div className="container-fluid">
              <nav className="navbar navbar-default">
                <button type="button" className="topbar__btn--toggle">
                  <span1><i className="fa fa-bars"></i></span1>
                </button>

                <div className="navbar-header">
                  <div className="navbar__logo">
                    <Link to={'/' + this.props.language} className="navbar-brand">
                      <img className="logo--large" src="/images/logo-splitshift.png" alt="logo" />
                    </Link>
                  </div>
                  <div className="navbar__logo">
                    <Link to={'/' + this.props.language} className="navbar-brand">
                      <img className="logo--small" src="/images/logo-splitshift-responsive.png" alt="logo" />
                    </Link>
                  </div>
                </div>

                <div className="nav__notification nav__admin">
                  <ul className="nav navbar-nav navbar-right">
                    <li>
                      <div className="user__icon">
                        <img id="profile--menubtn" src="/images/profiles/admin.png" onClick={this.triggerMenu.bind(this, 'profile--menu')} />
                        <div className="profile--menu">
                          <div className="up--menu">
                            <b><i className="fa fa-caret-up"></i></b>
                          </div>
                          <nav className="navbar-default">
                            <div className="profile__navlist">
                              <Link to={ '/' + this.props.language + "/admin"}>
                                <li className="noti__navitem">
                                <span>Main Menu</span>
                                </li>
                              </Link>

                              <a href="javascript:void(0)" onClick={this.onLogout.bind(null)}>
                                <li className="noti__navitem">
                                  <span>Log Out</span>
                                </li>
                              </a>
                            </div>
                          </nav>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>


                <div className="appbar" id="myNavbar">
                  <div className="nav__btnlist">
                    <ul className="nav navbar-nav navbar-right">

                      <li className="nav__btn--search">
                        <SearchBar search={this.props.search} />
                      </li>

                    </ul>
                  </div>

                  <NavList currentPage={this.props.currentPage} language={this.props.language} />
                </div>
              </nav>


              <DrawerHeader search={this.props.search} user={this.props.user} language={this.props.language} />

            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default AdminHeader;
