import React, { Component } from 'react';
import SearchActionCreators from '../../../actions/SearchActionCreators';
import { browserHistory } from 'react-router';

class SearchBar extends Component {

  updateFilter(name, evt) {
    let response = {};
    response[name] = evt.target.value;
    SearchActionCreators.updateFilter(response);
  }

  onSearch(evt) {
    evt.preventDefault();
    let keyword = this.props.search.filter.all_keyword ? this.props.search.filter.all_keyword : "";
    SearchActionCreators.All(keyword);
    browserHistory.push('/search');
    $(".dropdown-search").removeClass("open");
  }

  render() {
    return (
      <div className="dropdown dropdown-search">
        <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="icon-search"></i></a>
        <div className="dropdown-menu">
          <form onSubmit={this.onSearch.bind(this)}>
            <div className="input-group">

                <input type="text" onChange={this.updateFilter.bind(this, 'all_keyword')} className="form-control" placeholder={ this.props.language === 'th' ? 'ค้นหา บุคคล งาน บริษัทต่างๆ' : 'search people, jobs, companies ...' } />
                <span className="input-group-btn">
                  <button className="btn btn-default">
                    <i className="icon-search"></i>
                  </button>
                </span>

            </div>
          </form>
        </div>
      </div>
    )
  }
}

export default SearchBar;
