import React, { Component } from 'react';
import { Link } from 'react-router';

import AuthLogin from '../Auth/Login';
import AuthRegister from '../Auth/Register';
import AuthPostRegister from '../Auth/PostRegister';
import AuthForgetPassword from '../Auth/ForgetPassword';

import DrawerHeader from './Drawer';
import NavList from './NavList';
import SearchBar from './SearchBar';

import ResourceActionCreators from '../../../actions/ResourceActionCreators';
import JQueryTool from '../../../tools/JQueryTool';

let registerTrigger = false;

class MainHeader extends Component {

  componentDidMount() {
    JQueryTool.drawer();
  }

  changeLanguage(evt) {
    ResourceActionCreators.setLanguage(evt.target.value);
  }

  registerBtnTrigger(state) {
    registerTrigger = state;
    this.setState({});
  }

  render() {
    return (
      <header id="header--bar">
        <div className="topbar">
          <div className="container">
            <div className="container-fluid">

            <nav className="navbar navbar-default">
              <button type="button" className="topbar__btn--toggle">
                <span1><i className="fa fa-bars"></i></span1>
              </button>

              <div className="navbar-header">
                <div className="navbar__logo">
                  <Link to={'/' + this.props.language} className="navbar-brand">
                    <img className="logo__large" src="/images/logo-splitshift.png" alt="logo" />
                  </Link>
                </div>
              </div>

              <div className="topbar__btn--language">
                <div className="form-group">
                  <select className="form-control" id="lang" onChange={this.changeLanguage.bind(this)} value={this.props.language} >
                    <option value="th" >THA</option>
                    <option value="en" >ENG</option>
                  </select>
                </div>
              </div>

                <div className="appbar" id="myNavbar">

                  <div className="nav__btnlist">
                  <ul className="nav navbar-nav navbar-right">

                    <li className="nav__btn--search">
                      <SearchBar language={this.props.language} search={this.props.search} />
                    </li>

                    <li className="nav__btn--employer">
                      <a href="#">
                        <button className="btn btn-default" data-toggle="modal" data-target="#myModal__login"  type="button">
                          { this.props.language === 'th' ? 'นายจ้าง' : 'Employer' }
                        </button>
                      </a>
                    </li>
                    <li className="nav__btn--user">
                      <a href="#">
                        <button className="btn btn-default" data-toggle="modal" data-target="#myModal__login" type="button">
                          <i className="icon-user-1"></i> { this.props.language === 'th' ? 'เข้าสู่ระบบ' : 'Login' }
                        </button>
                      </a>
                    </li>
                  </ul>
                </div>

                <NavList currentPage={this.props.currentPage} language={this.props.language} />
              </div>
            </nav>

            <DrawerHeader search={this.props.search} user={this.props.user} language={this.props.language} />
            </div>

            <AuthLogin registerBtnTrigger={this.registerBtnTrigger.bind(this)} state={this.props.login} language={this.props.language}  />
            <AuthRegister registerTrigger={registerTrigger} registerBtnTrigger={this.registerBtnTrigger.bind(this)} language={this.props.language}  />
            <AuthPostRegister user={this.props.user} language={this.props.language}  />
            <AuthForgetPassword login={this.props.login} language={this.props.language}  />

          </div>
        </div>
      </header>
    );
  }
}

export default MainHeader;
