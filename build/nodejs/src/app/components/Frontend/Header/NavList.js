import React, { Component } from 'react';
import { Link } from 'react-router';
import configLang from '../../../tools/configLang'

class NavList extends Component {
  render() {
    let headers = configLang[this.props.language].headers
    let navLink = [
      {
        name: headers[0],
        link: "/" + this.props.language + "/buzz",
        route_name: 'buzz'
      },
      {
        name: headers[1],
        link: "/" + this.props.language + "/people-search",
        route_name: "people-search"
      },
      {
        name: headers[2],
        link: "/" + this.props.language + "/job-search",
        route_name: "job-search"
      },
      {
        name: headers[3],
        link: "/" + this.props.language + '/company-search',
        route_name: "company-search"
      },
      {
        name: headers[4],
        link: "/" + this.props.language + "/resource",
        route_name: "resource_index"
      }
    ];

    return (
      <div className="nav__navlist">
        <ul className="nav navbar-nav navbar-right">
          {
            navLink.map((nav, index) => (
              <li key={index} className={ this.props.currentPage == nav.route_name ? "navlist__item navlist__item--active" : "navlist__item"} role="presentation">
                <Link to={nav.link}><span1>{nav.name}</span1></Link>
              </li>
            ))
          }
        </ul>
      </div>
    )
  }
}

export default NavList;
