import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';

import SearchActionCreators from '../../../actions/SearchActionCreators';
import JQueryTool from '../../../tools/JQueryTool';
import configLang from '../../../tools/configLang'

class DrawerHeader extends Component {

  updateFilter(name, evt) {
    let response = {};
    response[name] = evt.target.value;
    SearchActionCreators.updateFilter(response);
  }

  onSearch(evt) {
    evt.preventDefault();
    let keyword = this.props.search.filter.all_keyword ? this.props.search.filter.all_keyword : "";
    SearchActionCreators.All(keyword);
    browserHistory.push('/search');
    JQueryTool.toggleDrawer();
  }

  toggleDrawer() {
    JQueryTool.toggleDrawer()
  }

  render() {
    let headers = configLang[this.props.language].headers

    return (
      <div className="drawer" onClick={this.toggleDrawer.bind(this)}>
        <div className="drawer__list">
          <ul className="navbar-default">

            <div className="drawer__btnlist">
              <li className="drawer__btn--search">
                <form onSubmit={this.onSearch.bind(this)}>
                  <div className="input-group">
                    <input type="text" className="form-control" onChange={this.updateFilter.bind(this, 'all_keyword')} placeholder="search people, jobs, companies ..." />
                    <span className="input-group-btn">
                      <button className="btn btn-default" style={{ marginLeft : 0 }}>
                        <i className="icon-search"></i>
                      </button>
                    </span>
                  </div>
                </form>
              </li>
            </div>

            <div className="drawer__navlist">
              <Link to={"/" + this.props.language + "/buzz"} onClick={JQueryTool.toggleDrawer.bind(this)}><li className="drawer__navitem" role="presentation">
                <span1>{headers[0]}</span1>
              </li></Link>
              <Link to={"/" + this.props.language + "/people-search"} onClick={JQueryTool.toggleDrawer.bind(this)} ><li className="drawer__navitem" role="presentation">
                <span1>{headers[1]}</span1>
              </li></Link>
              <Link to={"/" + this.props.language + "/job-search"} onClick={JQueryTool.toggleDrawer.bind(this)} ><li className="drawer__navitem" role="presentation">
                <span1>{headers[2]}</span1>
              </li></Link>
              <Link to={"/" + this.props.language + "/company-search"} onClick={JQueryTool.toggleDrawer.bind(this)} ><li className="drawer__navitem" role="presentation">
                <span1>{headers[3]}</span1>
              </li></Link>
              <Link to={"/" + this.props.language + "/resource"} onClick={JQueryTool.toggleDrawer.bind(this)} ><li className="drawer__navitem" role="presentation">
                <span1>{headers[4]}</span1>
              </li></Link>
              {
                this.props.user && typeof this.props.user.token === 'undefined' ?
                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal__login" onClick={JQueryTool.toggleDrawer.bind(this)}>
                  <li className="drawer__navitem" role="presentation">
                    <span1>{headers[5]}</span1>
                  </li>
                </a>
                : ""
              }
              {
                this.props.user && typeof this.props.user.token === 'undefined' ?
                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal__login" onClick={JQueryTool.toggleDrawer.bind(this)}>
                  <li className="drawer__navitem" role="presentation">
                    <span1>{headers[6]}</span1>
                  </li>
                </a>
                : ""
              }
            </div>

          </ul>
        </div>
      </div>
    )
  }
}

export default DrawerHeader;
