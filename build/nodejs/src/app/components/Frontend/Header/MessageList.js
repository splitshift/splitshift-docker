import React, { Component } from 'react';

import MessageActionCreators from '../../../actions/MessageActionCreators';

import JQueryTool from '../../../tools/JQueryTool';

class MessageList extends Component {

  loadMessage(other, evt) {

    MessageActionCreators.chooseOwner(this.props.user);
    MessageActionCreators.chooseOther({key: other});

    MessageActionCreators.getMessageWithPeople(this.props.user.token, other);

  }

  render() {

    let message = this.props.message;
    let other = "";

    if(typeof message.users[0] !== 'string')
      other = message.users[0];
    else
      other = message.users[1];

    return (
      <a href="#">
        <li className={ this.props.responsive ? "modal__navitem" : "noti__navitem" } onClick={this.loadMessage.bind(this, other.key)} data-toggle="modal" data-target="#myModal__message">
          <img className="photo__noti" src={other.profile_image} />
          <div>
            <b>{other.name}</b>
            <span className="date">{JQueryTool.changeDateAgo(message.messages[message.messages.length - 1].date)}</span>
          </div>
          <div>
            {
              message.messages[message.messages.length - 1].message_type == 'image' ? 'Sent an image.' : message.messages[message.messages.length - 1].message
            }
          </div>
        </li>
      </a>
    )
  }
}

export default MessageList;
