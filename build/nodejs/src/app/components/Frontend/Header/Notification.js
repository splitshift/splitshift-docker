import React, { Component } from 'react' ;
import { Link } from 'react-router';

import FriendList from './FriendList';
import NotificationList from './NotificationList';
import MessageList from './MessageList';

import SocialActionCreators from '../../../actions/SocialActionCreators';

import JQueryTool from '../../../tools/JQueryTool';
import ConfigData from '../../../tools/ConfigData';
import configLang from '../../../tools/configLang'

const menuClass = ['profile--menu', 'noti--menu', 'mesg--menu', 'connect--menu'];


class Notification extends Component {

  triggerMenu(name, evt) {
    evt.stopPropagation();

    menuClass.map((menu, index) => {
      if(name == menu)
        $("." + name).toggle();
      else
        $("." + menu).hide();
    });

    if(name === 'noti--menu' && !this.props.social.seen_notification)
      SocialActionCreators.putNotificationSeen(this.props.user.token)

  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.social.deleting && this.props.social.deleting)
      SocialActionCreators.getNotification(this.props.user.token);

    return (JSON.stringify(nextProps.user) !== JSON.stringify(this.props.user)) || (JSON.stringify(this.props.social.actions) !== JSON.stringify(nextProps.social.actions)) || (JSON.stringify(this.props.social.messages) !== JSON.stringify(nextProps.social.messages)) || (this.props.social.friends.length !== nextProps.social.friends.length) || (nextProps.language !== this.props.language);
  }

  render() {
    let menusContent = configLang[this.props.language].menus
    let actionContent = menusContent.action
    let notiContent = menusContent.notification

    return (
      <div className="nav__notification">
        <ul className="nav navbar-nav navbar-right">
          <li>
            <div className="dropdown dropdown-search noti--mesg" onClick={this.triggerMenu.bind(this, 'mesg--menu')}>
              {
                this.props.social.messages.filter((message) => !message.seen[this.props.user.key]).length ?
                <div className="noti__icon">
                  <img src="/images/icon-dot.png" />
                  <div><p>{this.props.social.messages.filter((message) => !message.seen[this.props.user.key]).length}</p></div>
                </div>
                : ""
              }

              <a className="headder__noti dropdown-toggle" data-toggle="dropdown">
                <i className="icon-mail"></i>
              </a>
            </div>
          </li>
          <div className={"mesg--menu " + (this.props.user.type !== 'Employee' && 'mesg--menu-company')}>
            <div className="up--menu">
              <b><i className="fa fa-caret-up"></i></b>
            </div>
            <nav className="navbar-default">
              <div className="noti__navlist">
                <a href="#"><li className="profile__navitem">
                  <span><b>{notiContent[0]} ({this.props.social.messages.length})</b></span>
                </li></a>

                {
                  this.props.social.messages.map((message, index) => (
                    <MessageList key={index} user={this.props.user} message={message} responsive={false} />
                  ))
                }

                {
                  this.props.social.messages.length > 5 ?
                    <a href="#">
                      <li className="seeall" data-toggle="modal" data-target="#myModal__mesgmenu">
                        <span><b>see all</b></span>
                      </li>
                    </a>
                  : ""
                }

                </div>
              </nav>
          </div>

          <div className="modal fade" id="myModal__mesgmenu" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <div className="modal-title">
                        <b><span>{notiContent[0]} ({this.props.social.messages.length})</span></b>
                    </div>
                  </div>
                  <div className="modal-body">
                    <div className="modal__navlist">
                      {
                        this.props.social.messages.map((message, index) => (
                          <MessageList key={index} user={this.props.user} message={message} responsive={true} />
                        ))
                      }

                      </div>
                  </div>
                </div>
              </div>
          </div>

          <li>
            <div className="dropdown dropdown-search noti--noti" onClick={this.triggerMenu.bind(this, 'noti--menu')}>
              {
                this.props.social.actions.filter((action) => !action.seen).length > 0 ?
                <div className="noti__icon">
                  <img src="/images/icon-dot.png" />
                  <div><p>{this.props.social.actions.length}</p></div>
                </div>
                : ""
              }
              <a className="headder__noti dropdown-toggle" data-toggle="dropdown">
                <i className="icon-flag-empty"></i>
              </a>
            </div>
          </li>
          <div className={"noti--menu " + (this.props.user.type !== 'Employee' && 'noti--menu-company')}>
            <div className="up--menu">
              <b><i className="fa fa-caret-up"></i></b>
            </div>
            <nav className="navbar-default">
              <div className="noti__navlist">
                <a href="#">
                  <li className="profile__navitem">
                    <span><b>{notiContent[1]} ({this.props.social.actions.length})</b></span>
                  </li>
                </a>

                {
                  this.props.social.actions.slice(0, 5).map((action, index) => (
                    <NotificationList key={index} action={action} user={this.props.user} responsive={false} />
                  ))
                }

                {
                  this.props.social.actions.length > 5 ?
                  <a href="#">
                    <li className="seeall" data-toggle="modal" data-target="#myModal__notimenu">
                      <span><b>see all</b></span>
                    </li>
                  </a>
                  : ""
                }


                </div>
              </nav>
          </div>

          <div className="modal fade" id="myModal__notimenu" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <div className="modal-title">
                        <b><span>{notiContent[1]} ({this.props.social.actions.length})</span></b>
                    </div>
                  </div>
                  <div className="modal-body">
                    <div className="modal__navlist">

                      {
                        this.props.social.actions.map((action, index) => (
                          <NotificationList key={index} action={action} user={this.props.user} responsive={true} />
                        ))
                      }

                      </div>
                  </div>
                </div>
              </div>
          </div>

          {
            this.props.user.type == 'Employee' ?
              <li>
                <div className="dropdown dropdown-search noti--connect" onClick={this.triggerMenu.bind(this, 'connect--menu')}>
                  {
                    this.props.social.friends.length ?
                    <div className="noti__icon">
                      <img src="/images/icon-dot.png" />
                      <div><p>{this.props.social.friends.length}</p></div>
                    </div>
                    : ""
                  }
                  <a className="headder__noti" className="dropdown-toggle" data-toggle="dropdown">
                    <i className="icon-user"></i>
                  </a>
                </div>
              </li>
            : ""
          }

          <div className="connect--menu">
            <div className="up--menu">
              <b><i className="fa fa-caret-up"></i></b>
            </div>
            <nav className="navbar-default">
              <div className="noti__navlist">
                <a href="#"><li className="profile__navitem">
                  <span><b>{notiContent[2]} ({this.props.social.friends.length})</b></span>
                </li></a>

                {
                  this.props.social.friends.slice(0, 5).map((friend, index) => (
                    <FriendList language={this.props.language} key={index} friend_index={index} friend={friend} user={this.props.user} type="notification" />
                  ))
                }

                {
                  this.props.social.friends.length > 5 ?
                  <a href="#">
                    <li className="seeall" data-toggle="modal" data-target="#myModal__connectmenu">
                      <span><b>see all</b></span>
                    </li>
                  </a>
                  : ""
                }

                </div>
              </nav>
          </div>

          <div className="modal fade" id="myModal__connectmenu" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <div className="modal-title">
                        <b><span>{notiContent[2]} ({this.props.social.friends.length})</span></b>
                    </div>
                  </div>
                  <div className="modal-body">
                    <div className="modal__navlist">

                      {
                        this.props.social.friends.map((friend, index) => (
                          <FriendList key={index} friend_index={index} friend={friend} user={this.props.user} type="modal" />
                        ))
                      }

                      </div>
                  </div>
                </div>
              </div>
          </div>

          <li>
            <div className="user__icon">
              <div className="user__iconimage" onClick={this.triggerMenu.bind(this, 'profile--menu')} style={{ background: "url(" + (this.props.user.profile_image || "/images/avatar.jpg") + ") no-repeat center center / cover"}} />
              <div className="profile--menu">
                <div className="up--menu">
                  <b><i className="fa fa-caret-up"></i></b>
                </div>
                <nav className="navbar-default">
                  <div className="profile__navlist">
                    <Link to={this.props.user.type === 'Employee' ? '/' + this.props.language + '/profile': '/' + this.props.language + '/company'}>
                      <li className="noti__navitem">
                        <span>{actionContent[0]}</span>
                      </li>
                    </Link>

                    <Link to={ '/' + this.props.language + (this.props.user.type === 'Employee' ? "/dashboard/user" : "/dashboard/company") }>
                      <li className="noti__navitem">
                        <span>{actionContent[1]}</span>
                      </li>
                    </Link>

                    <Link to={ '/' + this.props.language + "/setting" }>
                      <li className="noti__navitem">
                        <span>{actionContent[2]}</span>
                      </li>
                    </Link>

                    <a href="javascript:void(0)" onClick={this.props.onLogout.bind(null)}>
                      <li className="noti__navitem">
                        <span>{actionContent[3]}</span>
                      </li>
                    </a>
                  </div>
                </nav>
              </div>
            </div>
          </li>

        </ul>
      </div>
    )
  }
}

export default Notification;

/*
  <a href="#"><li className="seeall" data-toggle="modal" data-target="#myModal__notimenu">
    <span><b>see all</b></span>
  </li></a>
*/
