import React, { Component } from 'react';
import { Link } from 'react-router';

import JQueryTool from '../../../tools/JQueryTool';
import SocialActionCreators from '../../../actions/SocialActionCreators';

class FriendList extends Component {


  onResponseConnect(response, evt) {

    let result = {
      request_id: this.props.friend._id,
      response: response
    }

    SocialActionCreators.setDraft("friend_index", this.props.friend_index);
    SocialActionCreators.putConnectResponse(this.props.user.token, result);

  }

  render() {
    return (
      <li className={this.props.type == "notification" ? "noti__navitem" : "modal__navitem" }>
        <Link to={ "/" + this.props.language + "/user/" + this.props.friend.from.key}>
          <img className="photo__noti" src={this.props.friend.from.profile_image ? this.props.friend.from.profile_image : "/images/avatar-image.jpg"} />
          <div>
            <b>{this.props.friend.from.name}</b>
            <span className="dateconnect">{JQueryTool.changeDateAgo(this.props.friend.date)}</span>
          </div>
          <div>
            wants to connect with you.
          </div>
        </Link>
        <div className="connect__action">
          <ul>
            <li onClick={this.onResponseConnect.bind(this, true)}><i className="fa fa-check"></i></li>
            <li onClick={this.onResponseConnect.bind(this, false)}><i className="fa fa-times"></i></li>
          </ul>
        </div>
      </li>
    );
  }
}

export default FriendList;
