import React, { Component } from 'react';
import { Link } from 'react-router';

import JQueryTool from '../../../tools/JQueryTool';
import ConfigData from '../../../tools/ConfigData';

import SocialActionCreators from '../../../actions/SocialActionCreators';

class NotificationList extends Component {
  //
  // componentDidMount () {
  //   console.log("Notification List");
  //   console.log(this.props.action);
  // }

  deleteNotification(notification) {
    SocialActionCreators.deleteNotification(this.props.user.token, notification._id)
  }


  render() {
    return (
        <li className={ this.props.responsive ? "modal__navitem" : "noti__navitem" }>

          <div className="photo__noti" style={{ background: "url(" + (this.props.action.from.profile_image || "/images/avatar-image.jpg") + ") no-repeat center center / cover", marginLeft: "0px"}} />

          <div>
            <b>{this.props.action.from.name} </b> {ConfigData.ConvertNotification(this.props.action.notification_type, this.props.action.feed)}
          </div>

          <div>
            {JQueryTool.changeDateAgo(this.props.action.date)}
          </div>

          <div className="connect__action">
            <li style={{ padding: "4px", cursor: "pointer" }} onClick={this.deleteNotification.bind(this, this.props.action)}><i className="fa fa-times"></i></li>
          </div>

        </li>
    )
  }
}

export default NotificationList;
