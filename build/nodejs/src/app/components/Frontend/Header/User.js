import React, {Component} from 'react';
import { Link } from 'react-router';

import Notification from './Notification';
import DrawerHeader from './Drawer';
import NavList from './NavList';
import SearchBar from './SearchBar';
import Message from '../Message';

import AuthenticationActionCreators from '../../../actions/AuthenticationActionCreators';
import MessageActionCreators from '../../../actions/MessageActionCreators';
import SocialActionCreators from '../../../actions/SocialActionCreators';

import JQueryTool from '../../../tools/JQueryTool';

class UserHeader extends Component {

  onLogout() {
    AuthenticationActionCreators.logout();
  }

  componentDidMount() {
    SocialActionCreators.getConnectRequestList(this.props.user.token);
    MessageActionCreators.getAllMessage(this.props.user.token);
    SocialActionCreators.getNotification(this.props.user.token);
    JQueryTool.drawer();
  }

  render() {
    return (
      <header id="header--bar">
        <div className="topbar">
          <div className="container">
            <div className="container-fluid">
              <nav className="navbar navbar-default">
                <button type="button" className="topbar__btn--toggle">
                  <span1><i className="fa fa-bars"></i></span1>
                </button>

                <div className="navbar-header">
                  <div className="navbar__logo">
                    <Link to={'/' + this.props.language} className="navbar-brand">
                      <img className="logo--large" src="/images/logo-splitshift.png" alt="logo" />
                    </Link>
                  </div>
                  <div className="navbar__logo">
                    <Link to={'/' + this.props.language} className="navbar-brand">
                      <img className="logo--small" src="/images/logo-splitshift-responsive.png" alt="logo" />
                    </Link>
                  </div>
                </div>

                <Notification user={this.props.user} language={this.props.language} social={this.props.social} onLogout={this.onLogout.bind(this)} />
                <Message user={this.props.user} msg={this.props.message} profile_key={this.props.message.profile_key} />

                <div className="appbar" id="myNavbar">
                  <div className="nav__btnlist">
                    <ul className="nav navbar-nav navbar-right">
                      <li className="nav__btn--search">
                        <SearchBar language={this.props.language} search={this.props.search} />
                      </li>
                    </ul>
                  </div>

                  <NavList currentPage={this.props.currentPage} language={this.props.language} />
                </div>
              </nav>


              <DrawerHeader search={this.props.search} user={this.props.user} language={this.props.language}/>

            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default UserHeader;
