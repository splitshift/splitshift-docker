import React, {Component} from 'react';

class WhySplitshift extends Component {

  componentDidMount() {

    let self = this;

    $(".whysplitshift__btn--expand").click(function() {
      $(".whysplitshift__text--expand").toggle();
      var btnText = $(".whysplitshift__btn--expand").text();
      $(".whysplitshift__btn--expand").text(btnText === self.props.content.readmore ? self.props.content.readless : self.props.content.readmore);
    });
  }

  render() {

    return (
      <div className="whysplitshift">
        <div className="container">
          <div className="text--center">
            <h2 dangerouslySetInnerHTML={{__html: this.props.content.header }} />
            <hr style={{width: "50%"}} />
          </div>

          <div className="row">
            <div className="col-md-6 col-xs-12">
              <div className="whysplitshift__text">
                <p>
                  {this.props.content.paragraphs[0]}
                </p>
                <p>
                  {this.props.content.paragraphs[1]}
                </p>
                <p>
                  {this.props.content.paragraphs[2]}
                </p>
              </div>


              <div className="whysplitshift__text--expand">
                <p>
                  {this.props.content.paragraphs[3]}
                </p>
                <p dangerouslySetInnerHTML={{__html: this.props.content.paragraphs[4] }}/>

              </div>

              <a className="whysplitshift__btn--expand">{this.props.content.readmore}</a>

            </div>
            <div className="col-md-6 col-xs-12">
              <div className="whysplitshift__video">
                <object width="100%" height="300" data="https://www.youtube.com/embed/aBJO1DQmf_4?rel=0">
                    Your browser does not support the video tag.
                </object>
              </div>
            </div>
          </div>
        </div>
      </div>
    );

  }
}

export default WhySplitshift;
