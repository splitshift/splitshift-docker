import React, { Component } from 'react';

class Loading extends Component {

  componentDidMount() {
    $('#preloader').fadeOut('slow',function(){
      $(this).remove();
    });
  }
  render() {
    
    return (
      <div id="preloader" />
    )
  }
}

export default Loading;
