import React, { Component } from 'react';
import { Link } from 'react-router';

import ResourceActionCreators from '../../../actions/ResourceActionCreators';

class AdminHeader extends Component {

  changeLanguage(evt) {
    ResourceActionCreators.setLanguage(evt.target.value)
  }

  render() {
    return (
      <header id="admin__header">
        <div className="topbar">
            <div className="container-fluid">

            <nav className="navbar navbar-default">

                <button type="button" className="sidebar__toggle">
                  <span1><i className="fa fa-bars"></i></span1>
                </button>

                <div className="navbar-header">
                  <div className="navbar__logo">
                    <Link to={'/' + this.props.language} className="navbar-brand">
                      <img className="logo__large" src="/images/logo-splitshift.png" alt="logo" />
                    </Link>
                  </div>
                </div>

                <div className="topbar__btn--language">
                  <form role="form">
                    <div className="form-group">
                      <select className="form-control" onChange={this.changeLanguage.bind(this)} id="lang" value={this.props.language}>
                        <option value="th">THA</option>
                        <option value="en">ENG</option>
                      </select>
                    </div>
                  </form>
                </div>


                <div className="appbar" id="myNavbar">

                <div className="nav__navlist">
                  <ul className="nav navbar-nav navbar-right">
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/buzz"}><span1>BUZZ</span1></Link></li>
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/people-search"}><span1>PEOPLE</span1></Link></li>
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/job-search"}><span1>JOBS</span1></Link></li>
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/company-search"}><span1>COMPANIES</span1></Link></li>
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/resource"}><span1>RESOURCES</span1></Link></li>
                  </ul>
                </div>

              </div>


            </nav>
            </div>

          </div>
      </header>

    )
  }
}

export default AdminHeader;
