import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';

import ResourceActionCreators from '../../../actions/ResourceActionCreators';
import configLang from '../../../tools/configLang'

class CompanyHeader extends Component {

  changeLanguage(evt) {
    ResourceActionCreators.setLanguage(evt.target.value)
  }

  render() {
    let headerContent = configLang[this.props.language].headers

    return (
      <header id="admin__header">
        <div className="topbar">
            <div className="container-fluid">

            <nav className="navbar navbar-default">
                <button type="button" className="sidebar__toggle">
                  <span1><i className="fa fa-bars"></i></span1>
                </button>

                <div className="navbar-header">
                  <div className="navbar__logo">
                    <Link to={'/' + this.props.language} className="navbar-brand">
                      <img className="logo__large" src="/images/logo-splitshift.png" alt="logo" />
                    </Link>
                  </div>
                </div>

                <div className="topbar__btn--language">
                  <form role="form">
                    <div className="form-group">
                      <select onChange={this.changeLanguage.bind(this)} className="form-control" id="lang" value={this.props.language}>
                        <option value="th">THA</option>
                        <option value="en">ENG</option>
                      </select>
                    </div>
                  </form>
                </div>


                <div className="appbar" id="myNavbar">

                  <div className="nav__btnlist">
                  <ul className="nav navbar-nav navbar-right">
                    <li className="nav__btn--search">
                      <div className="dropdown dropdown-search">
                          <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="icon-search"></i></a>
                          <div className="dropdown-menu">
                            <form action="search.html">
                              <div className="input-group">
                                <input type="text" className="form-control" placeholder="search, jobs, people, companies ..." />
                                <span className="input-group-btn">
                                  <a href="search.html" className="btn btn-default" type="submit">
                                    <i className="icon-search"></i>
                                  </a>
                                </span>
                              </div>
                            </form>
                          </div>
                      </div>
                    </li>
                  </ul>
                </div>

                <div className="nav__navlist">
                  <ul className="nav navbar-nav navbar-right">
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/buzz"}><span1>{headerContent[0]}</span1></Link></li>
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/people-search"}><span1>{headerContent[1]}</span1></Link></li>
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/job-search"}><span1>{headerContent[2]}</span1></Link></li>
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/company-search"}><span1>{headerContent[3]}</span1></Link></li>
                    <li className="navlist__item" role="presentation">
                      <Link to={'/' + this.props.language + "/resource"}><span1>{headerContent[4]}</span1></Link></li>
                  </ul>
                </div>

              </div>


            </nav>
            </div>

          </div>
      </header>

    )
  }
}

export default CompanyHeader;
