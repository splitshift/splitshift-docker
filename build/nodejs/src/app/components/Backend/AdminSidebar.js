import React, { Component } from 'react';
import { Link } from 'react-router';

import AuthenticationActionCreators from '../../actions/AuthenticationActionCreators';
import JQueryTool from '../../tools/JQueryTool';

class AdminSidebar extends Component {

  onLogout() {
    AuthenticationActionCreators.logout();
  }

  componentDidMount() {
    JQueryTool.backendDrawer();
  }

  render() {
    return (
      <div id="Mysidenav" className="sidenav">
        <div className="sidebar__list">
          <ul className="navbar-default">
            <div className="sidebar__mainnavlist">
              <Link to={"/" + this.props.language}><li className="sidebar__titlemain" role="presentation">
                <img src={this.props.user.profile_image} />
                <span1><b>Admin</b></span1>
              </li></Link>

              <Link to={"/" + this.props.language + "/admin"} onClick={JQueryTool.sidebarToggle.bind(this)}>
                <li className="sidebar__mainnavitem" role="presentation">
                  <div className="icon__mainnavitem">
                    <i className="fa fa-user" aria-hidden="true"></i>
                  </div>
                  <span1>Main Menu</span1>
                </li>
              </Link>


              <Link to={"/" + this.props.language + "/admin/resource/index"} onClick={JQueryTool.sidebarToggle.bind(this)}>
                <li className="sidebar__mainnavitem" role="presentation">
                  <div className="icon__mainnavitem">
                    <i className="fa fa-user" aria-hidden="true"></i>
                  </div>
                  <span1>Resource</span1>
                </li>
              </Link>

              <Link to={"/" + this.props.language + "/admin/newsletter/index"} onClick={JQueryTool.sidebarToggle.bind(this)}>
                <li className="sidebar__mainnavitem" role="presentation">
                  <div className="icon__mainnavitem">
                    <i className="fa fa-user" aria-hidden="true"></i>
                  </div>
                  <span1>Newsletter</span1>
                </li>
              </Link>


              <Link to={"/" + this.props.language + "/admin/user/index"} onClick={JQueryTool.sidebarToggle.bind(this)}>
                <li className="sidebar__mainnavitem" role="presentation">
                  <div className="icon__mainnavitem">
                    <i className="fa fa-user" aria-hidden="true"></i>
                  </div>
                  <span1>User Management</span1>
                </li>
              </Link>

              <Link to="#" onClick={this.onLogout.bind(this)}>
                <li className="sidebar__mainnavitem" role="presentation">
                  <div className="icon__mainnavitem">
                    <i className="fa fa-user" aria-hidden="true"></i>
                  </div>
                  <span1>Logout</span1>
                </li>
              </Link>


              </div>
            </ul>
          </div>

        <div className="sidebar">
          <div className="sidebar__list">
            <ul className="navbar-default">

              <div className="sidebar__navlist">
                <Link to={"/" + this.props.language + "/buzz"}><li className="sidebar__navitem" role="presentation">
                  <span1>BUZZ</span1>
                </li></Link>
                <Link to={"/" + this.props.language + "/people-search"}><li className="sidebar__navitem" role="presentation">
                  <span1>PEOPLE</span1>
                </li></Link>
                <Link to={"/" + this.props.language + "/job-search"}><li className="sidebar__navitem" role="presentation">
                  <span1>JOBS</span1>
                </li></Link>
                <Link to={"/" + this.props.language + "/company-search"}><li className="sidebar__navitem" role="presentation">
                  <span1>COMPANIES</span1>
                </li></Link>
                <Link to={"/" + this.props.language + "/resource"}><li className="sidebar__navitem" role="presentation">
                  <span1>RESOURCES</span1>
                </li></Link>
              </div>

            </ul>
          </div>
        </div>

      </div>
    )
  }
}

export default AdminSidebar;
