import React, { Component } from 'react'
import { Link } from 'react-router'

const CompanyPrivacy = {public: 'Public', timeline: 'My Timeline', employee: 'Employee'}

class CompanyDashboard extends Component {

  render() {
    let cardStyle = {
      backgroundColor: "#ddd",
      margin: "10px",
      padding: "10px"
    };

    return (
      <div>

        <div className="dashboardmenu__responsive">
          <h3>Main Menu</h3>
          <Link to={'/' + this.props.language +'/dashboard/job'}  className="btn btn-default">Job Posting</Link>{' '}
          <Link to={'/' + this.props.language +'/dashboard/apply'} className="btn btn-default">Job Applications</Link>{' '}
          <Link to={'/' + this.props.language +'/dashboard/resource/index'} className="btn btn-default">Add Resource</Link>{' '}
          <Link to={'/' + this.props.language +'/dashboard/hr'} className="btn btn-default">HR Management</Link>{' '}
          <Link to={'/' + this.props.language +'/setting'} className="btn btn-default">Settings</Link>
        </div>

         <h2>Company Statistics</h2>
         <hr />

          <h4>Posts</h4>

          <div className="row">
            <div className="col-md-3" style={cardStyle}>
              <h5>Total</h5>
              {this.props.stat.feed.all}
            </div>

            {
              Object.keys(CompanyPrivacy).map((privacy, index) => (
                <div className="col-md-3" style={cardStyle} key={index}>
                  <h5>{CompanyPrivacy[privacy]}</h5>
                  {this.props.stat.feed[privacy] || "0"}
                </div>
              ))
            }

          </div>

         <hr />


         <h4>Resources</h4>

         <div className="row">
           <div className="col-md-3" style={cardStyle}>
             <h5>Total</h5>
             {this.props.stat.resource.all}
           </div>
           <div className="col-md-3" style={cardStyle}>
             <h5>Approved</h5>
             {this.props.stat.resource.activated}
           </div>
           <div className="col-md-3" style={cardStyle}>
             <h5>Pending Approval</h5>
             {this.props.stat.resource.deactivated}
           </div>
         </div>


         <hr />

         <h4>Jobs</h4>

         <div className="row">
           <div className="col-md-3" style={cardStyle}>
             <h5>Total</h5>
             {this.props.stat.job.all}
           </div>
           <div className="col-md-3" style={cardStyle}>
             <h5>Activated</h5>
             {this.props.stat.job.activated}
           </div>
           <div className="col-md-3" style={cardStyle}>
             <h5>Deactivated</h5>
             {this.props.stat.job.deactivated}
           </div>
         </div>

         <hr />

         <h4>Applications</h4>

         <div className="row">
           <div className="col-md-3" style={cardStyle}>
             <h5>Total</h5>
             {this.props.stat.applyjob.all}
           </div>
           <div className="col-md-3" style={cardStyle}>
             <h5>Open</h5>
             {this.props.stat.applyjob.open}
           </div>
           <div className="col-md-3" style={cardStyle}>
             <h5>Declined</h5>
             {this.props.stat.applyjob.declined}
           </div>
         </div>

         <hr />

         <h4>HR Management</h4>
         <div className="row">
           <div className="col-md-3" style={cardStyle}>
             <h5>Total</h5>
             {this.props.stat.hr}
           </div>
         </div>



      </div>
    );
  }

}

export default CompanyDashboard;
