import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';

import ResourceActionCreators from '../../../actions/ResourceActionCreators';
import JobActionCreators from '../../../actions/JobActionCreators';

import JQueryTool from '../../../tools/JQueryTool';
import ConfigData from '../../../tools/ConfigData';

class JobCreate extends Component {


  onEdit(name, evt) {
    let response = {};
    let result = evt.target.value;

      if(name == 'expire_date') {
      if(result)
        result = new Date((new Date()).getTime() + result*24*60*60*1000);
      else
        result = this.props.job.draft.old_expire_date;
    }

    response[name] = result;
    JobActionCreators.updateJobDraft(response);
  }

  onAddSkill(evt) {

    evt.preventDefault();

    let skill = this.refs.new_skill.value;
    this.refs.new_skill.value = "";

    let response = this.props.job.draft;

    if(typeof response.required_skills === 'undefined')
      response.required_skills = [skill];
    else
      response.required_skills.push(skill);

    JobActionCreators.updateJobDraft(response);
  }

  onRemoveSkill(index, evt) {

    let response = this.props.job.draft;
    response.required_skills.splice(index, 1);

    JobActionCreators.updateJobDraft(response);
  }

  createJob(evt) {
    evt.preventDefault();

    let result = this.props.job.draft;

    if(typeof result.type === 'undefined')
      result.type = this.refs.job_type.value;

    if(typeof result.compensation === 'undefined')
      result.compensation = this.refs.compensation.value;

    JobActionCreators.createNewJob(this.props.user.token, result);
  }

  editJob(evt) {
    evt.preventDefault();

    JobActionCreators.updateJob(this.props.user.token, this.props.job.draft);
  }

  componentWillUnmount() {
    JobActionCreators.resetDraft()
  }

  shouldComponentUpdate(nextProps, nextState) {


    if(!nextProps.job.updating && this.props.job.updating) {

      let alert_word = "Add Job Complete!";

      if(this.props.job.draft._id)
        alert_word = "Edit Job Complete!";

      alert(alert_word);

      browserHistory.push('/' + this.props.language + '/dashboard/job');
    }

    return true;

  }


  render() {
    return (
      <div>
        <form>
          <div className="row">
            <div className="col-md-8 col-md-offset-2">

              Job Title
              <input type="text" className="form-control form-control--mini " placeholder="Position" onChange={this.onEdit.bind(this, 'position')} defaultValue={this.props.job.draft.position} />

              Job Type
              <select ref="job_type" className="form-control form-control--mini " onChange={this.onEdit.bind(this, 'type')} defaultValue={this.props.job.draft.type} >
                {
                  ConfigData.JobTypeList(1).map((job, index) => (
                    <option key={index} value={job}>{job}</option>
                  ))
                }
              </select>

              Job Description
              <textarea className="form-control form-control--mini " onChange={this.onEdit.bind(this, 'description')} defaultValue={this.props.job.draft.description}  />

              Minimum Year Experience (Number only)
              <input type="number" className="form-control form-control--mini" placeholder="Min Year" onChange={this.onEdit.bind(this, 'min_year')} defaultValue={this.props.job.draft.min_year}  />

              Compensation
              <select ref="compensation" className="form-control form-control--mini " onChange={this.onEdit.bind(this, 'compensation')} defaultValue={this.props.job.draft.compensation}  >
                {
                  ConfigData.CompensationList().map((compensation, index) => (
                    <option key={index}>{compensation}</option>
                  ))
                }
              </select>



              Required Skill

              <div className="row">
                <form onSubmit={this.onAddSkill.bind(this)}>


                  <div className="col-md-12">

                    {
                      typeof this.props.job.draft.required_skills !== 'undefined' ?
                        <table className="table table-bordered">
                          {
                            this.props.job.draft.required_skills.map((skill, index) => (
                              <tr key={index}>
                                <th> {skill} </th>
                                <th> <a href="javascript:void(0)" onClick={this.onRemoveSkill.bind(this, index)}>Remove</a> </th>
                              </tr>
                            ))
                          }
                        </table>
                      : ""
                    }

                  </div>

                  <div className="col-md-9">
                    <input type="text" className="form-control form-control--mini" ref="new_skill" placeholder="Job Skill..." />
                  </div>
                  <div className="col-md-3">
                    <button className="btn btn-default btn-mini--right">ADD</button>
                  </div>
                </form>
              </div>

              Expired Date on <b>{ JQueryTool.changeDatePickerFormat(this.props.job.draft.expire_date) }</b>
              <div className="row">
                <div className="col-md-9">
                  <select className="form-control form-control--mini " onChange={this.onEdit.bind(this, 'expire_date')} >
                    <option value="">Default</option>
                    <option value="60">in 60 days from now</option>
                    <option value="30">in 30 days from now</option>
                  </select>
                </div>
              </div>

              <button type="button" onClick={ this.props.job.draft._id ? this.editJob.bind(this) : this.createJob.bind(this)} className="btn btn-default btn-mini">{ this.props.job.draft._id ? 'Edit Job' : 'Create Job' }</button>{' '}
              <Link to={'/' + this.props.language + '/dashboard/job'} className="btn btn-default btn-mini">Cancel</Link>

            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default JobCreate;
