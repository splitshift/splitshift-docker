import React, { Component } from 'react';
import { Link } from 'react-router';

import ResourceActionCreators from '../../actions/ResourceActionCreators';
import JobActionCreators from '../../actions/JobActionCreators';
import AuthenticationActionCreators from '../../actions/AuthenticationActionCreators';

import ConfigData from '../../tools/ConfigData';
import JQueryTool from '../../tools/JQueryTool';

let applyJobStatusList = {};

class ApplyJob extends Component {

  componentDidMount() {
    JobActionCreators.getAllApplyJob(this.props.user.token);
  }

  changeAppleJobStatus(apply_job, apply_job_index, evt) {
    JobActionCreators.changeAppleJobStatus(this.props.user.token, apply_job._id, {status: this.refs['applyjob_status_' + apply_job_index].value});
  }

  changeApplyJobFilter(filter, evt) {
    JobActionCreators.changeApplyJobFilter(filter);
  }

  deleteApplyJob(apply_job, evt) {
    JobActionCreators.deleteApplyJob(this.props.user.token, apply_job._id);
  }

  onSearch(evt) {
    JobActionCreators.searchApplyJob(evt.target.value)
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.deleting && this.props.deleting)
      JobActionCreators.getAllApplyJob(this.props.user.token);

    return true;
  }

  render() {


    let decline_status = ConfigData.ApplyJobStatus()[ConfigData.ApplyJobStatus().length - 1].toLowerCase();
    let apply_jobs = this.props.apply_jobs.filter((apply_job, index) => this.props.apply_job_filter ? apply_job.status === this.props.apply_job_filter : apply_job.status !== decline_status);


    return (
      <div className="row">
        <div className="col-md-12">

          <h4>
            {
              this.props.user.type === 'Employee' ?
                <div>
                  You are HR in { this.props.user.occupation.workplace } Company <br /> <br />
                </div>
              : ""
            }

            You have {apply_jobs.length} Job Applications
          </h4>

          <div>
            <input type="text" className="form-control form-control--mini" onChange={this.onSearch.bind(this)} placeholder="search from name, email" />
          </div>

          <div>
            <button onClick={this.changeApplyJobFilter.bind(this, "")} className="btn btn-default btn-mini">Current Applications</button>{' '}
            <button onClick={this.changeApplyJobFilter.bind(this, decline_status)} className="btn btn-mini">Declined Applications</button>
          </div>

          <table className="table">
            <thead style={{ backgroundColor: "white" }}>
              <tr>
                <th width="25%">Owner</th>
                <th width="45%">Application From </th>
                <th width="30%">Status</th>
              </tr>
            </thead>
            <tbody>
              { apply_jobs.map((apply_job, index) => (
                <tr key={index}>
                  <td>
                    <img src={ apply_job.from.profile_image ? apply_job.from.profile_image : "/images/avatar-image.jpg"} style={{width: "100px"}} />
                    <h5>{apply_job.from.name}</h5>
                    <Link to={'/' + this.props.language + '/user/' + apply_job.from.key}>See Profile</Link> <br />
                    <a href={'/' + this.props.language + '/user/' + apply_job.from.key + '/resume'} target="_blank">See Resume</a>
                  </td>
                  <td>
                    <div>
                      <b style={{color: "#f60"}}>Cover Letter </b> <br />
                      {apply_job.cover_letter}
                    </div>
                     <br />
                    <div>
                      <b style={{color: "#f60"}}> Email </b>  <br />
                      {apply_job.email}
                    </div>
                     <br />
                    <div>
                      <b style={{color: "#f60"}}> Tel </b>  <br />
                      {apply_job.tel}
                    </div>
                    <br />
                    <div>
                      <b style={{color: "#f60"}}> Date Submitted </b>  <br />
                      {JQueryTool.changeBirthDayFormat(apply_job.date)}
                    </div>
                     <br />
                     {
                       apply_job.files.length > 0 ?
                       <div>
                         <b style={{color: "#f60"}}> File Attachment </b>  <br />
                         {
                           apply_job.files.map((file, index) => (
                             <div key={index}><a href={file.substring(6)} target='_blank'>File {index + 1}</a></div>
                           ))
                         }
                       </div>
                       : ""
                     }
                  </td>
                  <td>

                    {
                      apply_job.status !== decline_status ?
                      <div className="optionbtn-apply__responsive">
                        <select ref={"applyjob_status_" + index} className="form-control" defaultValue={apply_job.status} >
                        {
                          ConfigData.ApplyJobStatus().map((status, index) => (
                            <option key={index} value={status.toLowerCase()}>{status}</option>
                          ))
                        }
                        </select>
                        <button onClick={this.changeAppleJobStatus.bind(this, apply_job, index)} className="btn btn-default btn-mini">Change Status</button>{' '}
                        <button onClick={this.deleteApplyJob.bind(this, apply_job)} className="btn btn-danger btn-mini">Remove Application</button>
                      </div>
                      :
                      <div>
                        Declined <br />
                        <button onClick={this.deleteApplyJob.bind(this, apply_job)} className="btn btn-danger btn-mini">Remove Application</button>
                      </div>
                    }

                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ApplyJob;
