import React, { Component } from 'react';
import { Link } from 'react-router';

import AdminManagerActionCreators from '../../../actions/AdminManagerActionCreators';


class AdminUserIndex extends Component {

  componentDidMount() {
    AdminManagerActionCreators.getUsers(this.props.user.token);
  }

  toDeleteUser(user, evt) {

    if(confirm('Are you sure to remove user ?'))
      AdminManagerActionCreators.deleteUser(this.props.user.token, user);

  }

  onSearch(evt) {
    AdminManagerActionCreators.searchUser(evt.target.value);
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.manager.loading && this.props.manager.loading)
      AdminManagerActionCreators.getUsers(this.props.user.token);

    return true;

  }

  render() {
    return (
      <div className="row">

        <div className="col-md-12">
          <h4>
            User Management
          </h4>

          <div>
            <input onChange={this.onSearch.bind(this)} type="text" className="form-control" placeholder="Search Email, Name" />
          </div>

          <table className="table">
            <thead style={{ backgroundColor: "#f60", color: "white" }}>
              <tr>
                <th width="30%">Email</th>
                <th width="15%">Type</th>
                <th width="30%">Name</th>
                <th width="15%">Status</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {
                this.props.manager.users.map((user, index) => (
                  <tr key={index} style={{ backgroundColor: user.type === 'Employee' ? 'white' : '' }}>
                    <td>
                      {user.email} <br />
                      {
                        user.confirm ?
                        <Link to={ user.type === 'Employee' ? '/th/user/' + user.key : '/th/company/' + user.key }>Go To Profile</Link>
                        : ""
                      }

                    </td>
                    <td>{user.type}</td>
                    <td>{user.name}</td>
                    <td>
                      {user.confirm ? "Confirmed" : "Not confirm" }
                    </td>
                    <td>
                      <button className="btn btn-default btn-mini reverse" onClick={this.toDeleteUser.bind(this, user)}>Delete</button>
                    </td>
                  </tr>
                ))
              }

            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default AdminUserIndex;
