import React, { Component } from 'react';
import { Link } from 'react-router'

import HRManagementActionCreators from '../../actions/HRManagementActionCreators';

let new_load = false;

class HRManagement extends Component {

  componentDidMount() {
    HRManagementActionCreators.getEmployee(this.props.user.key);
  }

  componentDidUpdate() {

    console.log(this.props.hr.users)

    if(new_load) {
      HRManagementActionCreators.getEmployee(this.props.user.key);
      new_load = false;
    }

  }

  changeToHR(user, key) {

    let result = {
      user_key: user.key,
      rank: user.rank === "Employee" ? "HR" : "Employee"
    };

    HRManagementActionCreators.changeRank(this.props.user.token, result);

  }

  shouldComponentUpdate(nextProps, nextState) {

    if(nextProps.hr.editing && !this.props.hr.editing)
      new_load = true;

    return true;
  }

  render() {
    return (
      <div>
        <h2>HR Management</h2>

        <p>The people show in this page is user work in your company.</p>

        <table className="table">
          <thead style={{ backgroundColor: "white" }}>
            <tr>
              <th>Name</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.hr.users.map((user, index) => (
                <tr key={index}>
                  <td>
                    <Link to={'/th/user/' + user.key}>
                      <img src={user.profile_image} width="100px" />
                      <p>{user.name}</p>
                    </Link>
                  </td>
                  <td>
                    {user.rank}
                  </td>
                  <td>
                    <button onClick={this.changeToHR.bind(this, user)} className="btn btn-default btn-mini">{ user.rank !== 'HR' ? "Change to HR" : "Remove from HR" }</button> <br />
                  </td>
                </tr>
              ))
            }

          </tbody>
        </table>

      </div>
    )
  }
}

export default HRManagement;
