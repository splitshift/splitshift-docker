import React, { Component } from 'react';
import { Link } from 'react-router';

import NewsletterActionCreators from '../../../actions/NewsletterActionCreators';

import JQueryTool from '../../../tools/JQueryTool';

class NewsletterIndex extends Component {

  componentDidMount() {
    NewsletterActionCreators.getNewsletters(this.props.user.token)
  }

  render() {
    return (
      <div>
        <h4>
          Newsletter
          <span style={{ float: "right"}}>
            <Link to={'/' + this.props.language + '/admin/newsletter/create'} className="btn btn-default">Send Newsletter</Link>
          </span>
        </h4>

        <table className="table">
          <thead style={{ backgroundColor: "#f60", color: "white" }}>
            <tr>
              <th width="60%">Email</th>
              <th width="20%">Date</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.newsletter.list.map((newsletter, index) => (
                <tr key={index}>
                  <td>{newsletter.email}</td>
                  <td>{JQueryTool.changeBirthDayFormat(newsletter.date)}</td>
                  <td></td>
                </tr>
              ))
            }
          </tbody>
        </table>

      </div>
    );
  }

}

export default NewsletterIndex;
