import React, { Component } from 'react'

import NewsletterActionCreators from '../../../actions/NewsletterActionCreators';

class NewsletterCreate extends Component {

  componentDidMount() {
    CKEDITOR.replace( 'editor2' );
  }

  sendSubscriberEmail(evt) {
    evt.preventDefault();
    let title = this.refs.title.value;
    let content = CKEDITOR.instances.editor2.getData();

    let info = {
      subject: this.refs.title.value,
      html: CKEDITOR.instances.editor2.getData()
    }

    NewsletterActionCreators.sendEmailSubscibers(this.props.user.token, info);

  }

  sendExampleEmail(evt) {

    let info = {
      email: this.refs.example_email.value,
      subject:  this.refs.title.value,
      html: CKEDITOR.instances.editor2.getData()
    }

    NewsletterActionCreators.sendEmailSingle(this.props.user.token, info);

  }

  render() {
    return (
      <div>
        <form onSubmit={this.sendSubscriberEmail.bind(this)}>
          <div className="row">
            <div className="col-md-12">
              <h2>Newsletter Create</h2>
              <hr />
            </div>

            <div className="col-md-9 col-md-offset-1">
              Title
              <input ref="title" type="text" className="form-control form-control--mini" placeholder="Title" />

              Content
              <textarea name="editor2" ref="content" className="form-control form-control--mini" />

              <div style={{ textAlign: "right", padding: "10px" }}>
                Send to Test Email
                <input ref="example_email" type="text"/>
                <button type='button' onClick={this.sendExampleEmail.bind(this)} className="btn btn-default btn-mini">Send</button>
              </div>

              <div>
                <button className="btn btn-default">Send Email to All Subscribers</button>
              </div>

            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default NewsletterCreate
