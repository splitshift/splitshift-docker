import React, { Component } from 'react';
import { Link } from 'react-router';

import ResourceActionCreators from '../../actions/ResourceActionCreators';
import JobActionCreators from '../../actions/JobActionCreators';
import AuthenticationActionCreators from '../../actions/AuthenticationActionCreators';

import JQueryTool from '../../tools/JQueryTool';

class Job extends Component {


  onDeleteJob(job, evt) {
    if(confirm("Sure to remove ?"))
      JobActionCreators.deleteJob(this.props.user.token, job);
  }

  onActivateJob(job_id, evt) {
    JobActionCreators.activateJob(this.props.user.token, job_id);
  }

  componentDidMount() {
    JobActionCreators.getAllJob(this.props.user.token);
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.job.updating && this.props.job.updating)
      JobActionCreators.getAllJob(this.props.user.token);

    return JSON.stringify(this.props.job.jobs) !== JSON.stringify(nextProps.job.jobs);
  }


  render() {
    return (
      <div className="row">
        <div className="col-md-12">

          <h4>

            <span style={{ float: "right"}}>
              <Link to={'/' + this.props.language + '/dashboard/job/create'} className="btn btn-default">Add New Job</Link>
            </span>

            You have { this.props.job.jobs.filter((job) => job.activated).length } Active Jobs

          </h4>

          <table className="table">
            <thead style={{ backgroundColor: "white" }}>
              <tr>
                <th>Job</th>
                <th>Expiry Date</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {
                typeof this.props.job.jobs !== 'undefined' ?
                this.props.job.jobs.map((job, index) => (
                  <tr key={index}>
                    <th>
                      <span style={{color: "#f60"}}>{job.position}</span>
                    </th>
                    <th>
                      {
                        JQueryTool.changeDatePickerFormat(job.expire_date)
                      }
                    </th>
                    <th>
                      <button onClick={this.onActivateJob.bind(this, job._id)} className="btn btn-default btn-mini">
                        {
                          !job.activated ? 'Deactivated' : 'Activated'
                        }
                      </button>
                    </th>
                    <th>
                      <div className="optionbtn__responsive">
                        <Link to={'/' + this.props.language + '/dashboard/job/edit/' + job._id} className="btn btn-default btn-mini reverse">Edit</Link>{' '}
                        <button className="btn btn-default btn-mini reverse" onClick={ this.onDeleteJob.bind(this, job, index) } >Delete</button>
                      </div>
                    </th>
                  </tr>
                ))
                : ""
              }

            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Job;
