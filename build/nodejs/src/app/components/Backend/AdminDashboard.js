import React, { Component } from 'react';

const CompanyPrivacy = {public: 'Public', timeline: 'My Timeline', employee: 'Employee'}
const UserPrivacy = {public: 'Public', timeline: 'My Timeline', connected: 'Connection', employer: 'Employer', onlyme: 'Private'};

class AdminDashboard extends Component {


  componentDidUpdate() {
    console.log(this.props.stat)
  }

  render() {

    let cardStyle = {
      backgroundColor: "#ddd",
      margin: "10px",
      padding: "10px"
    };

    return (
      <div>
        <h2>Splitshift Statistics </h2>

        <hr />

         <h4>Posts</h4>

         <div className="row">
           <div className="col-md-3" style={cardStyle}>
             <h5>Total</h5>
             {this.props.stat.feed.all || "0"}
           </div>
         </div>

         <div className="row">
           <div className="col-md-3" style={cardStyle}>
             <h5>Employee Total</h5>
             {this.props.stat.feed.company.all || "0"}
           </div>

           {
             Object.keys(CompanyPrivacy).map((privacy, index) => (
               <div className="col-md-3" style={cardStyle} key={index}>
                 <h5>Employee {CompanyPrivacy[privacy]}</h5>
                 {this.props.stat.feed.company[privacy] || "0"}
               </div>
             ))
           }
         </div>

         <div className="row">
           <div className="col-md-3" style={cardStyle}>
             <h5>User Total</h5>
             {this.props.stat.feed.user.all || "0"}
           </div>

           {
             Object.keys(UserPrivacy).map((privacy, index) => (
               <div className="col-md-3" style={cardStyle} key={index}>
                 <h5>User {UserPrivacy[privacy]}</h5>
                 {this.props.stat.feed.user[privacy] || "0"}
               </div>
             ))
           }
         </div>

         <div className="row">
           <div className="col-md-3" style={cardStyle}>
             <h5>Admin Total</h5>
             {this.props.stat.feed.admin || "0"}
           </div>
         </div>


        <hr />

        <h4>Resource</h4>

        <div className="row">
          <div className="col-md-3" style={cardStyle}>
            <h5>Total Approved</h5>
            {this.props.stat.resource.approved.all}
          </div>
          <div className="col-md-3" style={cardStyle}>
            <h5>User Approved</h5>
            {this.props.stat.resource.approved.user || "0"}
          </div>
          <div className="col-md-3" style={cardStyle}>
            <h5>Employer Approved</h5>
            {this.props.stat.resource.approved.company || "0"}
          </div>
        </div>

        <hr />


        <h4>Jobs</h4>

        <div className="row">
          <div className="col-md-3" style={cardStyle}>
            <h5>Total</h5>
            {this.props.stat.job.all}
          </div>
          <div className="col-md-3" style={cardStyle}>
            <h5>Active Jobs</h5>
            {this.props.stat.job.activated}
          </div>
          <div className="col-md-3" style={cardStyle}>
            <h5>Deactivated Jobs</h5>
            {this.props.stat.job.deactivated}
          </div>
          <div className="col-md-3" style={cardStyle}>
            <h5>Applications</h5>
            {typeof this.props.stat.applyjob === 'number' ? this.props.stat.applyjob : '0'}
          </div>
        </div>

        <hr />

        <div style={{fontSize: "20"}}>
          <p>Number of Users <b>{this.props.stat.people}</b></p>
          <p>Number of Employers <b>{this.props.stat.companies}</b></p>
          <p>Number of Messages <b>{this.props.stat.message}</b></p>
          <p>Number of Likes <b>{this.props.stat.like}</b></p>
          <p>Number of Comments <b>{this.props.stat.comment}</b></p>
        </div>
      </div>
    );
  }

}

export default AdminDashboard;
