import React, { Component } from 'react'
import { Link } from 'react-router'

import FormUserConnection from '../Frontend/Form/User/Connection'

const UserPrivacy = {public: 'Public', timeline: 'My Timeline', connected: 'Connection', employer: 'Employer', onlyme: 'Private'};

class UserDashboard extends Component {

  render() {
    let cardStyle = {
      backgroundColor: "#ddd",
      margin: "10px",
      padding: "10px"
    };

    return (
      <div>

        <div className="dashboardmenu__responsive">
          <h4>Main Menu</h4>
          <Link to={'/' + this.props.language + '/dashboard/application'}  className="btn btn-default">Application</Link>{' '}
          <Link to={'/' + this.props.language + '/dashboard/resource/index'} className="btn btn-default">Add Resource</Link>{' '}
          <Link to={'/' + this.props.language + '/setting'} className="btn btn-default">Setting</Link>
        </div>

        <h2>My Statistics</h2>
        <hr />

         <h4>Posts</h4>

         <div className="row">
           <div className="col-md-3" style={cardStyle}>
             <h5>Total</h5>
             {this.props.stat.feed.all}
           </div>
           {
             Object.keys(UserPrivacy).map((privacy, index) => (
               <div className="col-md-3" key={index} style={cardStyle}>
                 <h5>{ UserPrivacy[privacy] }</h5>
                 {this.props.stat.feed[privacy] || "0" }
               </div>
             ))
           }

         </div>

        <hr />


        <h4>Resources</h4>

        <div className="row">

          <div className="col-md-3" style={cardStyle}>
            <h5>Total</h5>
            {this.props.stat.resource.all}
          </div>

          <div className="col-md-3" style={cardStyle}>
            <h5>Approved</h5>
            {this.props.stat.resource.activated}
          </div>

          <div className="col-md-3" style={cardStyle}>
            <h5>Pending Approval</h5>
            {this.props.stat.resource.deactivated}
          </div>
        </div>

        <hr />
        <h4>Connections</h4>
        <div className="row">
          <div className="col-md-3" style={cardStyle}>
            <h5>Total</h5>
            {this.props.stat.connections.length}
          </div>
        </div>

        <a href="#" data-toggle="modal" data-target="#myModal__connections">See Connections</a>
        <FormUserConnection profile={this.props.stat} backend={true} />

        <hr />

        <h4>Applications</h4>
        <div className="row">
          <div className="col-md-3" style={cardStyle}>
            <h5>Total</h5>
            {this.props.stat.applyjob.all}
          </div>
          <div className="col-md-3" style={cardStyle}>
            <h5>Open</h5>
            {this.props.stat.applyjob.open}
          </div>
          <div className="col-md-3" style={cardStyle}>
            <h5>Decline</h5>
            {this.props.stat.applyjob.declined}
          </div>
        </div>



      </div>
    );
  }
}

export default UserDashboard;
