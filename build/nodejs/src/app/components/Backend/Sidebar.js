import React, { Component } from 'react';
import { Link } from 'react-router';

import JQueryTool from '../../tools/JQueryTool';
import configLang from '../../tools/configLang'

class Sidebar extends Component {
  componentDidMount() {
    JQueryTool.backendDrawer();
  }

  render() {
    let SidebarContent = configLang[this.props.language].backend.sidebar
    let headerContent = configLang[this.props.language].headers

    return (
      <div id="Mysidenav" className="sidenav">
        <div className="sidebar__list">
          <ul className="navbar-default">
            <div className="sidebar__mainnavlist">
              <a href="#"><li className="sidebar__titlemain" role="presentation">
                <img src={this.props.user.profile_image} />
                <span1><b>{this.props.user.type === 'Employee' ? this.props.user.first_name + ' ' + this.props.user.last_name : this.props.user.company_name}</b></span1>
              </li></a>

              <Link to={ '/' + this.props.language + (this.props.user.type === 'Employee' ? "/profile" : "/company") } >
                <li className="sidebar__mainnavitem" role="presentation">
                  <div className="icon__mainnavitem">
                    <i className="fa fa-user" aria-hidden="true"></i>
                  </div>
                  <span1>{ this.props.user.type === 'Employee' ? SidebarContent[0] : SidebarContent[1] }</span1>
                </li>
              </Link>

              <Link to={ '/' + this.props.language + (this.props.user.type === 'Employee' ? '/dashboard/user' : '/dashboard/company') }  onClick={JQueryTool.sidebarToggle.bind(this)}>
                <li className="sidebar__mainnavitem" role="presentation">
                  <div className="icon__mainnavitem">
                    <i className="fa fa-user" aria-hidden="true"></i>
                  </div>
                  <span1>{SidebarContent[2]}</span1>
                </li>
              </Link>

              {
                this.props.user.type !== 'Employee' &&
                <Link to={'/' + this.props.language + "/dashboard/job"} onClick={JQueryTool.sidebarToggle.bind(this)}>
                  <li className="sidebar__mainnavitem" role="presentation">
                    <div className="icon__mainnavitem">
                      <i className="fa fa-user" aria-hidden="true"></i>
                    </div>
                    <span1>{SidebarContent[3]}</span1>
                  </li>
                </Link>
              }


              {
                (this.props.user.type !== 'Employee' || this.props.user.rank === 'HR') &&
                <Link to={'/' + this.props.language + "/dashboard/apply"} onClick={JQueryTool.sidebarToggle.bind(this)}>
                  <li className="sidebar__mainnavitem" role="presentation">
                    <div className="icon__mainnavitem">
                      <i className="fa fa-user" aria-hidden="true"></i>
                    </div>
                    <span1>{SidebarContent[4]}</span1>
                  </li>
                </Link>

              }

              {
                this.props.user.type === 'Employee' &&
                <Link to={'/' + this.props.language + "/dashboard/application"} onClick={JQueryTool.sidebarToggle.bind(this)}>
                  <li className="sidebar__mainnavitem" role="presentation">
                    <div className="icon__mainnavitem">
                      <i className="fa fa-user" aria-hidden="true"></i>
                    </div>
                    <span1>{SidebarContent[5]}</span1>
                  </li>
                </Link>

              }

              <Link to={'/' + this.props.language + "/dashboard/resource/index"} onClick={JQueryTool.sidebarToggle.bind(this)}>
                <li className="sidebar__mainnavitem" role="presentation">
                  <div className="icon__mainnavitem">
                    <i className="fa fa-user" aria-hidden="true"></i>
                  </div>
                  <span1>{SidebarContent[6]}</span1>
                </li>
              </Link>


              {
                this.props.user.type !== 'Employee' &&
                <Link to={'/' + this.props.language + "/dashboard/hr"} onClick={JQueryTool.sidebarToggle.bind(this)}>
                  <li className="sidebar__mainnavitem" role="presentation">
                    <div className="icon__mainnavitem">
                      <i className="fa fa-user" aria-hidden="true"></i>
                    </div>
                    <span1>{SidebarContent[7]}</span1>
                  </li>
                </Link>
              }

              <Link to={'/' + this.props.language + '/setting'}>
                <li className="sidebar__mainnavitem" role="presentation">
                  <div className="icon__mainnavitem">
                    <i className="fa fa-user" aria-hidden="true"></i>
                  </div>
                  <span1>{SidebarContent[8]}</span1>
                </li>
              </Link>

              </div>
            </ul>
          </div>

        <div className="sidebar">
          <div className="sidebar__list">
            <ul className="navbar-default">

              <div className="sidebar__navlist">
                <Link to={'/' + this.props.language + "/buzz"}><li className="sidebar__navitem" role="presentation">
                  <span1>{headerContent[0]}</span1>
                </li></Link>
                <Link to={'/' + this.props.language + "/people-search"}><li className="sidebar__navitem" role="presentation">
                  <span1>{headerContent[1]}</span1>
                </li></Link>
                <Link to={'/' + this.props.language + "/job-search"}><li className="sidebar__navitem" role="presentation">
                  <span1>{headerContent[2]}</span1>
                </li></Link>
                <Link to={'/' + this.props.language + "/company-search"}><li className="sidebar__navitem" role="presentation">
                  <span1>{headerContent[3]}</span1>
                </li></Link>
                <Link to={'/' + this.props.language + "/resource"}><li className="sidebar__navitem" role="presentation">
                  <span1>{headerContent[4]}</span1>
                </li></Link>
              </div>

              <div className="sidebar__btnlist">
                <li className="sidebar__btn--search">
                  <form>
                    <div className="input-group">
                      <input type="text" className="form-control" placeholder="search, jobs, people, companies ..." />
                      <span className="input-group-btn">
                        <Link to="/th/" className="btn btn-default" type="button">
                          <i className="icon-search"></i>
                        </Link>
                      </span>
                    </div>
                  </form>
                </li>
              </div>

            </ul>
          </div>
        </div>

      </div>
    )
  }
}

export default Sidebar;
