import React, { Component } from 'react';

import JobActionCreators from '../../actions/JobActionCreators';
import configLang from '../../tools/configLang'

class Application extends Component {

  deleteApplication(apply_job, evt) {
    JobActionCreators.deleteApplication(this.props.user.token, apply_job._id)
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.deleting && this.props.deleting)
      JobActionCreators.getEmployeeApplyJob(this.props.user.token);

    return true;
  }

  render() {
    let AppContent = configLang[this.props.language].backend.user.application
    return (
      <div>
        <h4>{AppContent[0]}</h4>

        <table className="table">
          <thead style={{ backgroundColor: "white" }}>
            <tr>
              <th>{AppContent[1]}</th>
              <th>{AppContent[2]}</th>
              <th>{AppContent[3]}</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.job.employee_apply_jobs.map((apply_job, index) => (
                <tr key={index}>
                  <td>
                    {apply_job.to.name} <br />
                    <button onClick={this.deleteApplication.bind(this, apply_job)} className="btn btn-danger btn-mini">Remove Application</button>
                  </td>
                  <td>

                    <b>Email:</b> {apply_job.email} <br />
                    <b>Tel:</b> {apply_job.tel} <br />
                    <b>Cover Letter:</b> <br />
                    {apply_job.cover_letter} <br />

                  </td>
                  <td style={{ verticalAlign: "middle" }}>
                    <b>{apply_job.status}</b>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    )
  }
}

export default Application;
