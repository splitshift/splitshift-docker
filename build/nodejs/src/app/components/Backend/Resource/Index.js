import React, { Component } from 'react';
import { Link } from 'react-router';

import ResourceActionCreators from '../../../actions/ResourceActionCreators';
import configLang from '../../../tools/configLang'

class ResourceIndex extends Component {

  componentDidMount() {
    if(this.props.user.type !== 'Admin')
      ResourceActionCreators.getResources(this.props.user.token);
    else
      ResourceActionCreators.getAdminResource(this.props.user.token);
  }

  toDeleteResource(resource, evt) {
    ResourceActionCreators.deleteResource(this.props.user.token, resource);
  }

  approveResource(resource, evt) {
    ResourceActionCreators.approveResource(this.props.user.token, resource);
  }

  shouldComponentUpdate(nextProps, nextState) {

    if(!nextProps.resource.editing && this.props.resource.editing) {
      if(this.props.user.type !== 'Admin')
        ResourceActionCreators.getResources(this.props.user.token);
      else
        ResourceActionCreators.getAdminResource(this.props.user.token);
    }

    return true;

  }

  render() {
    let resource_list;
    let resourceContent = configLang[this.props.language].backend.resource

    if(this.props.user.type !== 'Admin')
     resource_list = this.props.resource.resource_array.filter((resource, index) => resource.owner.key == this.props.user.key);
    else
     resource_list = this.props.resource.resource_array;

    return (
      <div className="row">
        <div className="col-md-12">
          <h4>
            {resourceContent.header[0]}
            <span style={{ float: "right"}}>
              <Link to={ '/' + this.props.language + (this.props.user.type === 'Admin' ? '/admin/resource/create' : '/dashboard/resource/create') } className="btn btn-default">{resourceContent.add}</Link>
            </span>
          </h4>

          <table className="table">
            <thead style={{ backgroundColor: "white" }}>
              <tr>
                <th>{resourceContent.header[1]}</th>
                <th>{resourceContent.header[2]}</th>
                <th>{resourceContent.header[3]}</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {
                resource_list.map((resource, index) => (
                  <tr key={index}>
                    <td>
                      {
                        resource.is_approved ?
                          <Link to={'/th/resource/article/' + resource._id }>{resource.title}</Link>
                        :
                          <div>
                            { resource.title} <br />
                            { this.props.user.type === 'Admin' || (resource.owner.key === this.props.user.key) ? (<Link to={'/th/resource/article/' + resource._id }>Preview</Link>) : "" }
                          </div>
                      }
                    </td>
                    <td>
                      {resource.owner.name} ({resource.owner.user_type})
                    </td>
                    <td>
                      {
                        this.props.user.type === 'Admin' ?
                          <button className={"btn btn-default btn-mini" + ( !resource.is_approved ? " reverse" : "") } onClick={this.approveResource.bind(this, resource)}>{ !resource.is_approved ? 'Approve' :  'Approved' }</button>
                        : <div>{ !resource.is_approved ? 'Pending' : <span>Approved</span> }</div>
                      }
                    </td>
                    <td>
                      <Link to={ '/' + this.props.language + (this.props.user.type === 'Admin' ? '/admin/resource/edit/' + resource._id : '/dashboard/resource/edit/' + resource._id) } className="btn btn-default btn-mini reverse"  style={{marginBottom: "5px"}} >Edit</Link>{' '}
                      <button className="btn btn-default btn-mini reverse" style={{marginBottom: "5px"}}  onClick={this.toDeleteResource.bind(this, resource)}>Delete</button>
                    </td>
                  </tr>
                ))
              }

            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ResourceIndex;
