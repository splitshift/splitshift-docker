import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';

import ResourceActionCreators from '../../../actions/ResourceActionCreators';
import ConfigData from '../../../tools/ConfigData';
import configLang from '../../../tools/configLang'

class ResourceCreate extends Component {

  componentDidMount(){
    CKEDITOR.replace( 'editor1' );
  }

  createResource() {
    let result = Object.assign({}, this.props.resource.resource, this.props.resource.draft);

    result.content = CKEDITOR.instances.editor1.getData();

    if(typeof result.department === 'undefined')
      result.department = this.refs.department.value;

    if(typeof result.category === 'undefined')
      result.category =  this.refs.category.value;

    if(this.props.resource.resource._id)
      ResourceActionCreators.editResource(this.props.user.token, result);
    else
      ResourceActionCreators.createResource(this.props.user.token, result);


  }

  onCancelEditResource() {
    ResourceActionCreators.setAdminEditor('edit_resource', false);
  }

  editDraft(name, evt) {
    let response = {};
    response[name] = evt.target.value;

    if(name === 'featured_video') {
      let youtube_embed = ConfigData.ConvertYoutubeToEmbed(evt.target.value);
      if(youtube_embed != 'error')
          response[name]  = youtube_embed;
    }

    ResourceActionCreators.editResourceDraft(response);
  }

  uploadFeaturedImage(evt) {
    ResourceActionCreators.updateFeaturedImage(this.props.user.token, evt.target.files[0]);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(!nextProps.resource.editing && this.props.resource.editing) {
      alert("Add Resource Complete!");
      ResourceActionCreators.setAdminEditor('edit_resource', false);

      if(this.props.user.type === 'Admin')
        browserHistory.push('/' + this.props.language + '/admin/resource/index');
      else
        browserHistory.push('/' + this.props.language + '/dashboard/resource/index');

    }
    return true
  }

  render() {
    let resourceContent = configLang[this.props.language].backend.resource
    return (
      <div>
        <form>
          <div className="row">
            <div className="col-md-12">
              <h2>{resourceContent.title}</h2>
              <hr />
            </div>

            <div className="col-md-9 col-md-offset-1">

              {resourceContent.form[0]}
              <input type="text" className="form-control form-control--mini" onChange={this.editDraft.bind(this, 'title')} placeholder={resourceContent.form[0]}  defaultValue={this.props.resource.resource.title} />

              {resourceContent.form[1]}
              <select ref="category" className="form-control form-control--mini" onChange={this.editDraft.bind(this, 'category')} defaultValue={this.props.resource.resource.category}>
                {
                  ConfigData.ResourceCategory().map((category, index) => (
                    <option key={index} value={category.toLowerCase()}>{resourceContent.category[index]}</option>
                  ))
                }
              </select>

              {resourceContent.form[2]}
              <select ref="department" className="form-control form-control--mini" onChange={this.editDraft.bind(this, 'department')} defaultValue={this.props.resource.resource.department}>
                {
                  ConfigData.ResourceDepartment().map((department, index) => (
                    <option key={index} value={department.toLowerCase()}>{resourceContent.department[index]}</option>
                  ))
                }
              </select>

              {resourceContent.form[3]}
              <div>


                <input type="radio" name="type" value="image" id="featured_image"  onChange={this.editDraft.bind(this, 'feature_type')} defaultChecked={ typeof this.props.resource.resource.featured_image !== 'undefined' && this.props.resource.resource.featured_image !== null ? true : false } />{' '}

                <label htmlFor="featured_image">
                  <span style={{ fontSize: "13px" }}>
                    <strong>{resourceContent.form[4]}</strong>
                  </span>
                </label>{' '}

                <input type="radio" name="type" value="video" id="featured_video"  onChange={this.editDraft.bind(this, 'feature_type')} defaultChecked={ typeof this.props.resource.resource.featured_video !== 'undefined' && this.props.resource.resource.featured_video !== null ? true : false }  />{' '}

                <label htmlFor="featured_video">
                  <span style={{ fontSize: "13px" }}>
                    <strong>{resourceContent.form[5]}</strong>
                  </span>
                </label>

              </div>

              {
                typeof this.props.resource.draft.feature_type !== 'undefined' || typeof this.props.resource.resource._id !== 'undefined' ?
                  <div>
                    {
                      this.props.resource.draft.feature_type === 'video' || (typeof this.props.resource.draft.feature_type === 'undefined' && typeof this.props.resource.resource.featured_video !== 'undefined' &&  this.props.resource.resource.featured_video !== null) ?
                        <span>
                          Video Preview
                          {
                            typeof this.props.resource.draft.featured_video !== 'undefined' || typeof this.props.resource.resource.featured_video !== 'undefined' ?
                              <iframe className="feed__clip" height="315" src={ this.props.resource.draft.featured_video ? this.props.resource.draft.featured_video : this.props.resource.resource.featured_video } frameBorder="0" allowFullScreen="" />
                            : ""
                          }
                          <input type="text" className="form-control form-control--mini" onChange={this.editDraft.bind(this, 'featured_video')} placeholder="Video URL" defaultValue={this.props.resource.resource.featured_video}  />
                        </span>
                      :
                        <span>
                          {
                            typeof this.props.resource.draft.featured_image !== 'undefined' || typeof this.props.resource.resource.featured_image !== 'undefined' ?
                              <img src={ this.props.resource.draft.featured_image ? this.props.resource.draft.featured_image.substring(6) : this.props.resource.resource.featured_image ? this.props.resource.resource.featured_image.substring(6) : ""} style={{width: "100%"}} />
                            : ""
                          }

                          Upload Image
                          <input type="file" className="form-control" onChange={this.uploadFeaturedImage.bind(this)} />
                        </span>
                    }
                  </div>
                : ""
              }


              {resourceContent.form[6]}
              <textarea className="form-control form-control--mini " onChange={this.editDraft.bind(this, 'short_description')} defaultValue={this.props.resource.resource.short_description}/>

              {resourceContent.form[7]}
              <textarea name="editor1" ref="content" className="form-control form-control--mini" defaultValue={this.props.resource.resource.content} />

              <button type="button" className="btn btn-default" onClick={this.createResource.bind(this)}>{ this.props.resource.resource._id ? resourceContent.action[1] : resourceContent.action[0] }</button>{' '}
              <Link to={ '/' + this.props.language + (this.props.user.type === 'Admin' ? '/admin/resource/index' : '/dashboard/resource/index') } className="btn btn-default" onClick={this.onCancelEditResource.bind(this)}>{resourceContent.action[2]}</Link>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default ResourceCreate;
