import ajax from './ajax'

const StatAPI = {

  getStat() {
    return ajax.getFetch('/api/stat/all');
  },

  getAdminSummary(token) {
    return ajax.getFetch('/api/admin/metric', token);
  },

  getUserSummary(token) {
    return ajax.getFetch('/api/dashboard', token);
  }

}

export default StatAPI;
