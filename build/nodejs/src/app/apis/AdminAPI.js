import ajax from './ajax'

const AdminAPI = {

  getUsers(token) {
    return ajax.getFetch('/api/admin/user', token);
  },

  approveResource(token, resource) {
    return ajax.putFetch('/api/admin/approve', token, {resource_id: resource._id });
  },

  deleteUser(token, user) {
    return ajax.deleteFetch('/api/admin/user/' + user.key, token);
  }

};

export default AdminAPI;
