import ajax from './ajax'

const SocialAPI = {

  putFollow(token, user_key) {
    return ajax.putFetch('/api/following', token, { to : user_key });
  },

  getFollowers(user_key) {
    return ajax.getFetch('/api/following/' + user_key)
  },

  getConnectRequestList(token) {
    return ajax.getFetch('/api/request', token)
  },

  getNotification(token) {
    return ajax.getFetch('/api/notification', token)
  },

  putNotificationSeen(token) {
    return ajax.putFetch('/api/notification/seen', token);
  },

  deleteNotification(token, notification_id) {
    return ajax.deleteFetch('/api/notification/' + notification_id, token);
  },

  postConnectRequest(token, user_key) {
    return ajax.postFetch('/api/connect', token, {to: user_key});
  },

  putConnectResponse(token, info) {
    return ajax.putFetch('/api/connect', token, info);
  },

  deleteConnect(token , user_key) {
    return ajax.deleteFetch('/api/connect', token, {to: user_key});
  },

  getConnections(user_key) {
    return ajax.getFetch('/api/connect/' + user_key);
  }

}

export default SocialAPI;
