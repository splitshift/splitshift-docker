import ajax from './ajax'

const ResourceAPI = {

  getResources(token) {
    return ajax.getFetch('/api/resource', token);
  },

  searchResource(info) {
    let filter = {
      keyword: info.keyword ? info.keyword : "",
      category: info.category ? info.category : "",
      department: info.department ? info.department : ""
    };

    return ajax.getFetch('/api/search/resource?keyword=' + filter.keyword + '&category='+ filter.category + '&department=' + filter.department);
  },

  getResource(id, token) {
    return ajax.getFetch('/api/resource/' + id, token);
  },

  getResourceIndex() {
    return ajax.getFetch('/api/resource/index');
  },

  getAdminResource(token) {
    return ajax.getFetch('/api/admin/resource', token);
  },

  createResource(token, info) {
    return ajax.postFetch('/api/resource/', token, info);
  },

  editResource(token, info) {
    return ajax.putFetch('/api/resource/' + info._id, token, info);
  },

  likeResource(token, info) {
    return ajax.putFetch('/api/resource/' + info._id + '/like', token);
  },

  deleteResource(token, info) {
    return ajax.deleteFetch('/api/resource/' + info._id, token);
  },

  commentResource(token, info, resource_id) {
    return ajax.postFetch('/api/resource/' + resource_id + '/comment', token, info);
  },

  deleteCommentResource(token, info, resource_id) {
    return ajax.deleteFetch('/api/resource/' + resource_id + '/comment/' + info._id, token);
  },

  forwardEmail(token, info) {
    return ajax.postFetch('/api/forward/resource', token, info);
  }

};

export default ResourceAPI;
