import ajax from './ajax'

const ProfileAPI = {

	getUserProfile(key, token) {

		return ajax.getFetch('/api/profile/' + key, token)
	},

	updateInfo(token, info) {
    return ajax.putFetch("/api/user/info", token, info);
	},

  updateAbout(token, info) {
    return ajax.putFetch("/api/user/about", token, info);
	},

  updateInterest(token, info) {
    return ajax.putFetch("/api/user/interest", token, info);
  },

  createInterest(token, info) {

    let data = {
      category: info.category?info.category:"Art",
      value: info.value?info.value:"Art"
    };

    return ajax.postFetch("/api/user/interest", token, data);
  },

  editInterest(token, info) {
    return ajax.putFetch("/api/user/interest/" + info._id, token, info);
  },

  deleteInterest(token, info) {
    return ajax.deleteFetch("/api/user/interest/" + info._id, token);
  },

  createExperience(token, info) {
    let data = Object.assign({}, info);
    data.position = data.position ? data.position : "";
    data.workplace = data.workplace ? data.workplace : "";
    data.start_date = data.start_date ? data.start_date : "";
    data.end_date = data.end_date ? data.end_date : "";
    data.address = data.address ? data.address : "";
    data.company_key = data.company_key ? data.company_key : "";

    return ajax.postFetch("/api/user/experience", token, data);
  },

  editExperience(token, info) {
    return ajax.putFetch("/api/user/experience/" + info._id, token, info);
  },

  deleteExperience(token, info) {
    return ajax.deleteFetch("/api/user/experience/" + info._id, token);
  },

  updateSkill(token, info) {
    return ajax.putFetch('/api/user/skill', token, info);
  },

  createEducation(token, info) {

    let data = {
      field: info.field?info.field:"",
      school: info.school?info.school:"",
      start_date: info.start_date?info.start_date:"",
      graduate_date: info.graduate_date?info.graduate_date:"",
      description: info.description?info.description:"",
      gpa: info.gpa?info.gpa:"",
    };

    console.log("Create Education");
    console.log(data);

    return ajax.postFetch("/api/user/education", token, data);
  },

  editEducation(token, info) {
    return ajax.putFetch("/api/user/education/" + info._id, token, info);
  },

  deleteEducation(token, info) {
    return ajax.deleteFetch("/api/user/education/" + info._id, token);
  },

  createCourse(token, info) {

    let data = {
      name: info.name ? info.name : "",
      start_date: info.start_date ? info.start_date : "",
      end_date: info.end_date ? info.end_date : "",
      description: info.description,
      link: info.link
    };

    return ajax.postFetch("/api/user/course",token, data);
  },

  editCourse(token, info) {
    return ajax.putFetch("/api/user/course/" + info._id, token, info);
  },

  deleteCourse(token, info) {
    return ajax.deleteFetch("/api/user/course/" + info._id, token);
  },

  createLanguage(token, info) {
    return ajax.postFetch("/api/user/language", token, info);
  },

  editLanguage(token, info) {
    return ajax.putFetch("/api/user/language/" + info._id, token, info);
  },

  deleteLanguage(token, info) {
    return ajax.deleteFetch("/api/user/language/" + info._id, token);
  },

  uploadProfileImage(token, info) {
    let data = new FormData();
    data.append("avatar", info);
    return ajax.putImageFetch('/api/profileimage', token, data);
  },

  uploadBannerImage(token, info) {
    let data = new FormData();
    data.append("avatar", info);
    return ajax.putImageFetch('/api/banner', token, data);
  },

  changeBannerImageWithTemplate(token, data) {
    return ajax.putFetch('/api/banner/template', token, data);
  },

  loadCompany() {
    return ajax.getFetch('/api/company/all')
  }

};

export default ProfileAPI;
