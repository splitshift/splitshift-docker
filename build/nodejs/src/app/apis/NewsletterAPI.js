import ajax from './ajax'

const FeedAPI = {

  getNewsletters(token) {
    return ajax.getFetch('/api/subscribe', token);
  },

  postNewsletter(info) {
    return ajax.postFetch('/api/subscribe', "", info);
  },

  sendEmailSingle(token, info) {
    return ajax.postFetch('/api/send/email', token, info);
  },

  sendEmailSubscibers(token, info) {
    return ajax.postFetch('/api/send/subscribe', token, info);
  }

};

export default FeedAPI;
