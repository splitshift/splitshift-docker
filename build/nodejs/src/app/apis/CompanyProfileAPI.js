import ajax from './ajax'

const CompanyProfileAPI = {

	getProfile(key, token) {
		return ajax.getFetch('/api/profile/' + key, token)
	},

  updateInformation(token, info) {
    return ajax.putFetch('/api/company/info', token, info);
  },

  updateAddress(token, info) {
    return ajax.putFetch('/api/company/address', token, info);
  },

  updateOverview(token, info) {
    return ajax.putFetch('/api/company/overview', token, info);
  },

  updateCulture(token, info) {
    return ajax.putFetch('/api/company/culture', token, info);
  },

  updateVideo(token, info) {
    return ajax.putFetch('/api/company/video', token, info);
  },

  createBenefit(token, info) {
    return ajax.postFetch('/api/company/benefit', token, info);
  },

  deleteBenefit(token, info) {
    return ajax.deleteFetch('/api/company/benefit/' + info._id, token);
  },

  updateSocialLink(token, info) {
    return ajax.putFetch('/api/company/link', token, info);
  },

  uploadProfileImage(token, info) {
    let data = new FormData();
    data.append("avatar", info);
    return ajax.putImageFetch('/api/profileimage', token, data);
  },

  uploadBannerImage(token, info) {
    let data = new FormData();
    data.append("avatar", info);
    return ajax.putImageFetch('/api/banner', token, data);
  },

  changeBannerImageWithTemplate(token, data) {
    return ajax.putFetch('/api/banner/template', token, data);
  },

  reviewCompany(token, info) {
    return ajax.postFetch("/api/user/review", token, info);
  }

};

export default CompanyProfileAPI;
