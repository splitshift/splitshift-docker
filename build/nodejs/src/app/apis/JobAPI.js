import ajax from './ajax'

const JobAPI = {

  postNewJob(token, info) {
    return ajax.postFetch('/api/company/job', token, info);
  },

  getAllJob(token) {
    return ajax.getFetch('/api/company/job/all', token);
  },

  getEmployeeApplyJob(token) {
    return ajax.getFetch('/api/user/applyjob', token);
  },

  getJob(token, job_id) {
    return ajax.getFetch('/api/company/job/' + job_id, token);
  },

  putUpdateJob(token, info) {
    return ajax.putFetch('/api/company/job/' + info._id, token, info);
  },

  deleteJob(token, info) {
    return ajax.deleteFetch('/api/company/job/' + info._id, token);
  },

  activateJob(token, job_id) {
    return ajax.putFetch('/api/company/job/' + job_id + '/activate', token);
  },

  getAllApplyJob(token) {
    return ajax.getFetch('/api/applyjob', token);
  },

  getApplyJob(token, info) {
    return ajax.getFetch('/api/applyjob/' + info._id, token);
  },

  changeAppleJobStatus(token, applyjob_id ,info) {
    return ajax.putFetch('/api/applyjob/' + applyjob_id + '/status', token, info);
  },

  postApplyJob(token, info) {
    return ajax.postFetch('/api/applyjob', token, info);
  },

  deleteApplyJob(token, apply_job_id) {
    return ajax.deleteFetch('/api/applyjob/' + apply_job_id, token);
  },

  deleteApplication(token, apply_job_id) {
    return ajax.deleteFetch('/api/user/applyjob/' + apply_job_id, token);
  },

  forwardEmail(token, info) {
    return ajax.postFetch('/api/forward/job', token, info);
  }

};

export default JobAPI;
