import ajax from './ajax'

const LoginAPI = {
  login(info) {
    return ajax.postFetch('/api/login', '', info)
	}
};

export default LoginAPI;
