import ajax from './ajax'

const FeedAPI = {

  getFeed(user_key, token, page) {
    return ajax.getFetch('/api/feed/' + user_key + '?page=' + page, token)
  },

  getPhotosFeed(user_key, token) {
    return ajax.getFetch('/api/photo?user_key=' + user_key, token)
  },

  getAlbums(user_key) {
    return ajax.getFetch('/api/album?user_key=' + user_key);
  },

  getPhotosInAlbum(user_key, info, token) {
    return ajax.getFetch('/api/album/' + info._id + '?user_key=' + user_key, token);
  },

  getBuzzFeed(page) {
    return ajax.getFetch('/api/buzz?page=' + page)
  },

  searchBuzzFeed(keyword) {
    return ajax.getFetch('/api/search/buzz?keyword=' + keyword);
  },

  postFeed(token, info) {
    return ajax.postFetch('/api/feed', token, info);
  },

  editFeed(token, info) {
    return ajax.putFetch('/api/feed/' + info._id, token, info);
  },

  changePrivacy(token, info) {
    return ajax.putFetch('/api/feed/' + info._id + '/privacy', token, info);
  },

  deleteFeed(token, info) {
    return ajax.deleteFetch('/api/feed/' + info._id, token);
  },

  deleteBuzzFeed(token, info) {
    return ajax.deleteFetch('/api/buzz/' + info._id, token);
  },

  likeFeed(token, info) {
    return ajax.postFetch('/api/feed/' + info._id + '/like', token, info);
  },

  unlikeFeed(token, info) {
    return ajax.deleteFetch('/api/feed' + info._id + '/like', token);
  },

  postCommentFeed(token, info) {
    return ajax.postFetch('/api/comment', token, info);
  },

  deleteCommentFeed(token, info, feed) {
    return ajax.deleteFetch('/api/comment/' + info._id, token, feed);
  },

  postReplyComment(token, info) {
    return ajax.postFetch('/api/reply', token, info);
  },

  deleteReply(token, info, reply) {
    return ajax.deleteFetch('/api/reply/' + info._id, token, reply);
  },

  putLikeFeed(token, info) {
    return ajax.putFetch('/api/feed/' + info._id + '/like', token, { user_key: info.owner.key });
  }

};

export default FeedAPI;
