const API_URL= () => {
  return window.location.origin;
}

const API_HEADERS = {'Accept': 'application/json','Content-Type': 'application/json'}
import 'whatwg-fetch'

let status;

const ajax = {
  getFetch(url, token) {
    return fetch(API_URL() + url, {
      method: 'get',
      headers: {'Content-Type': 'application/json', 'Authorization': token ? token : ""},
    }).then((response) => {
      status = response.status
      return response.json();
    }).then((data) => {
      if(status != 200)
        throw new Error(data.message);

      return data;
    })
  },

  postFetch(url, token, info) {
     return fetch(API_URL() + url, {
  			method: 'post',
  			headers: {'Content-Type': 'application/json', 'Authorization': token ? token : ""},
  			body: JSON.stringify(info)
  		}).then((response) => {
        status = response.status;
  			return response.json();
  		}).then((data) => {
        if(status != 200 && status != 201)
          throw new Error(data.message);
  			return data
  		});
  },

  putFetch(url, token, info) {
     return fetch(API_URL() + url, {
  			method: 'put',
  			headers: {'Content-Type': 'application/json', 'Authorization': token ? token : ""},
  			body: JSON.stringify(info)
  		}).then((response) => {
        status = response.status;
  			return response.json();
  		}).then((data) => {
        if(status != 200 && status != 201)
          throw new Error(data.message);
  			return data
  		});
  },

  deleteFetch(url, token, info) {
    return fetch(API_URL() + url, {
  			method: 'delete',
  			headers: {'Content-Type': 'application/json', 'Authorization': token},
        body: JSON.stringify(info)
  		}).then((response) => {
        status = response.status;
  			return response.json();
  		}).then((data) => {
        if(status != 200)
          throw new Error(data.message);
  			return data;
  		});
  },

  putImageFetch(url, token, info) {
     return fetch(API_URL() + url, {
  			method: 'put',
  			headers: {'Authorization': token},
  			body: info
  		}).then((response) => {
        status = response.status;
  			return response.json();
  		}).then((data) => {
        if(status != 200)
          throw new Error(data.message);
  			return data
  		});
  }

}

export default ajax
