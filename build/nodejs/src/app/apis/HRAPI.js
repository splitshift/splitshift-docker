import ajax from './ajax'

const HRAPI = {

  getEmployee(company_key) {
    return ajax.getFetch('/api/company/' + company_key + '/employee');
  },

  changeRank(token, info) {
    return ajax.postFetch('/api/company/rank', token, info);
  }

}


export default HRAPI;
