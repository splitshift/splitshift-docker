import ajax from './ajax'

let RegisterAPI = {
  register(info) {
    return ajax.postFetch('/api/register', '', info)
	},
  forgetPassword(info) {
    return ajax.postFetch('/api/forgot/password', '', info)
  },
  changePasswordFormForgot(info, token) {
    return ajax.putFetch('/api/forgot/password/' + token, '', info)
  }
};

export default RegisterAPI;
