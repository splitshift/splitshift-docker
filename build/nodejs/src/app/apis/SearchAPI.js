import ajax from './ajax'

const SearchAPI = {

  job(info, page) {

    let filter = {
      keyword: info.keyword ? info.keyword : "",
      location: info.location ? info.location : "",
      point: info.point ? info.point : "",
      distance: info.distance ? info.distance : "",
      bounded_time: info.bounded_time ? info.bounded_time : "",
      type: info.type ? info.type : "",
      compensation: info.compensation ? info.compensation : "",
      employer: info.employer ? info.employer : ""
    };

    return ajax.getFetch('/api/search/jobs?keyword=' + filter.keyword + '&location=' + filter.location + '&point=' + filter.point + '&distance=' + filter.distance + '&bounded_time=' + filter.bounded_time + '&type=' + filter.type + '&compensation=' + filter.compensation + '&employer=' + filter.employer + '&page=' + page);
  },

  people(info, page) {

    let filter = {
      keyword: info.keyword ? info.keyword : "",
      interest: info.interest ? info.interest : "",
      work_exp: info.work_exp ? info.work_exp : "",
      location: info.location ? info.location : ""
    };

    return ajax.getFetch('/api/search/people?keyword=' + filter.keyword + '&interest=' + filter.interest + '&work_exp=' + filter.work_exp + '&location=' + filter.location + '&page=' + page);
  },

  company(info, page) {

    let filter = {
      keyword: info.keyword ? info.keyword : "",
      type: info.type ? info.type : "",
      overall: info.overall ? info.overall : "",
      location: info.location ? info.location : "",
      bounded_time: info.bounded_time ? info.bounded_time : "",
      compensation: info.compensation ? info.compensation : ""
    };

    return ajax.getFetch('/api/search/companies?keyword=' + filter.keyword + '&type=' + filter.type + '&overall=' + filter.overall + '&location=' + filter.location + '&bounded_time=' + filter.bounded_time + '&compensation=' + filter.compensation + '&page=' + page);
  },

  all(info) {
    return ajax.getFetch('/api/search?keyword=' + info);
  }

};

export default SearchAPI;
