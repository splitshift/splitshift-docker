import ajax from './ajax'

const SettingAPI = {

  deleteAccount(token) {
    return ajax.deleteFetch('/api/account', token);
  },

  changePassword(token, info) {
    return ajax.putFetch('/api/change/password', token, info);
  },

  changePrimaryEmail(token, info) {
    return ajax.putFetch('/api/email', token, info);
  }

}


export default SettingAPI;
