const API_URL= () => {
  return window.location.origin;
}

const API_HEADERS = {'Accept': 'application/json','Content-Type': 'application/json'}

let status;

const UploadAPI = {

  postFetch(url, token, info) {

     return fetch(API_URL() + url, {
  			method: 'post',
  			headers: {'Authorization': token},
  			body: info
  		}).then((response) => {

        status = response.status;
  			return response.json();

  		}).then((data) => {

        if(status != 201)
          throw new Error(data.message);

  			return data
  		});

  },

  deleteFetch(url, token) {
    return fetch(API_URL() + url, {
  			method: 'delete',
  			headers: {'Authorization': token},
  		}).then((response) => {
        status = response.status;
  			return response.json();
  		}).then((data) => {
        if(status != 200)
          throw new Error(data.message);
  			return data;
  		});
  },

  uploadImage(token, info) {
    let data = new FormData();
    data.append("avatar", info);

    return this.postFetch('/api/image', token, data);
  },

  deleteImage(token, info) {
    return this.deleteFetch('/api/image', token, info);
  },

  uploadFile(token, info) {

    let data = new FormData();
    data.append("file", info);

    return this.postFetch('/api/file', token, data);
  }
}

export default UploadAPI;
