import ajax from './ajax'

const MessageAPI = {

  sendMessage(token, info) {
    return ajax.postFetch('/api/message', token, info)
  },

  getAllMessage(token) {
    return ajax.getFetch('/api/chat/all', token)
  },

  getMessageWithPeople(token, people_id) {
    return ajax.getFetch('/api/chat?key=' + people_id, token)
  }
}

export default MessageAPI;
