var file = require('../../controllers/files');

var verify = require('../../controllers/verify');
var allVerify = verify.verifyAll;

module.exports = function(router) {
  router.post('/file', allVerify, file.postFile);
  router.delete('/file', allVerify, file.deleteFile);
  router.post('/upload/file', allVerify, file.postFile);
  router.delete('/upload/file', allVerify, file.deleteFile);
};
