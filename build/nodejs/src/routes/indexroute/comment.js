var comment = require('../../controllers/feeds/comments');

var verify = require('../../controllers/verify');
var allVerify = verify.verifyAll;

module.exports = function(router) {
  router.post('/comment', allVerify, comment.postComment);
  router.delete('/comment/(:id)', allVerify, comment.deleteComment);
};
