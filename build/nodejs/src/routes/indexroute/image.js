var image = require('../../controllers/feeds/images');

var verify = require('../../controllers/verify');
var allVerify = verify.verifyAll;

module.exports = function(router) {
  router.post('/image', allVerify, image.postImage);
  router.delete('/image', allVerify, image.deleteImage);
};
