var reply = require('../../controllers/feeds/replies');

var verify = require('../../controllers/verify');
var allVerify = verify.verifyAll;

module.exports = function(router) {
  router.post('/reply', allVerify, reply.postReply);
  router.delete('/reply/(:id)', allVerify, reply.deleteReply);
};
