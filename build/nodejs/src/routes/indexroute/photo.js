var photo = require('../../controllers/photo/albums');

var verify = require('../../controllers/verify');
var allVerify = verify.verifyAll;

module.exports = function(router) {
  router.post('/album', allVerify, photo.postAlbum);
  router.put('/album/(:id)', allVerify, photo.putAlbum);
  router.delete('/album/(:id)', allVerify, photo.deleteAlbum);
  router.get('/album', photo.getAlbum);
  router.get('/album/(:id)', photo.getPhotoAlbum);
  router.get('/photo', photo.getPhoto);
};
