var feed = require('../../controllers/feeds/feeds');
var image = require('../../controllers/feeds/images');
var like = require('../../controllers/feeds/like');
var buzz = require('../../controllers/feeds/buzz');

var verify = require('../../controllers/verify');
var allVerify = verify.verifyAll;
var verifyAdmin = verify.verifyAdmin;

module.exports = function(router) {
  router.get('/buzz', buzz.getBuzz);
  router.delete('/buzz/(:id)', verifyAdmin, buzz.deleteBuzz, image.deleteImage);
  router.get('/feed/(:key)', feed.getFeed);
  router.post('/feed', allVerify, feed.postFeed);
  router.delete('/feed/(:id)', allVerify, feed.deleteFeed, image.deleteImage);
  router.put('/feed/(:id)', allVerify, feed.putFeed, image.deleteImage);
  router.put('/feed/(:id)/privacy', allVerify, feed.putFeedPrivacy);
  router.put('/feed/(:id)/like', allVerify, like.putLike);
};
