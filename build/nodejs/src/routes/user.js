var express = require('express');
var router = express.Router();

var ctrlAuthentication = require('../controllers/authentication');
var ctrlUserFunction = require('../controllers/user/userFunction');
var ctrlUserExperiences = require('../controllers/user/experiences');
var ctrlUserEducatios = require('../controllers/user/educations');
var ctrlUserCourses = require('../controllers/user/courses');
var ctrlUserLanguages = require('../controllers/user/languages');
var ctrlUserDayOffJobs = require('../controllers/user/day_off_jobs');
var ctrlUserInterests = require('../controllers/user/interests');
var ctrlUserApplyJobs = require('../controllers/user/applyjob');
var review = require('../controllers/user/review');

var verify = require('../controllers/verify');
var verifyUser = verify.verifyUser;

//review
router.post('/review', verifyUser, review.postReview);
router.delete('/review/(:id)', verifyUser, review.deleteReview);

//info
router.put('/info',verifyUser, ctrlUserFunction.putInfo);

//about
router.put('/about',verifyUser, ctrlUserFunction.putAbout);

//experience
router.post('/experience',verifyUser, ctrlUserExperiences.postExperience);
router.put('/experience/(:id)',verifyUser, ctrlUserExperiences.putExperience);
router.delete('/experience/(:id)',verifyUser, ctrlUserExperiences.deleteExpereince);

//skill
router.put('/skill',verifyUser, ctrlUserFunction.putSkill);

//education
router.post('/education',verifyUser, ctrlUserEducatios.postEducation);
router.put('/education/(:id)', verifyUser, ctrlUserEducatios.putEducation);
router.delete('/education/(:id)', verifyUser, ctrlUserEducatios.deleteEducation);

//course
router.post('/course', verifyUser, ctrlUserCourses.postCourse);
router.put('/course/(:id)', verifyUser, ctrlUserCourses.putCourse);
router.delete('/course/(:id)', verifyUser, ctrlUserCourses.deleteCourse);

//interest
router.post('/interest', verifyUser, ctrlUserInterests.postInterest);
router.put('/interest/(:id)', verifyUser, ctrlUserInterests.putInterest);
router.delete('/interest/(:id)', verifyUser, ctrlUserInterests.deleteInterest);

//language
router.post('/language', verifyUser, ctrlUserLanguages.postLanguage);
router.put('/language/(:id)', verifyUser, ctrlUserLanguages.putLanguage);
router.delete('/language/(:id)', verifyUser, ctrlUserLanguages.deleteLanguage);

//applyjob
router.get('/applyjob', verifyUser, ctrlUserApplyJobs.getApplyJobs);
router.delete('/applyjob/(:id)', verifyUser, ctrlUserApplyJobs.delApplyJob);

//dayoffjob
router.post('/day-off-job', verifyUser, ctrlUserDayOffJobs.postDayOffJob);
router.get('/day-off-job', verifyUser, ctrlUserDayOffJobs.getMyDayOffJob);
router.get('/day-off-job/(:id)', verifyUser, ctrlUserDayOffJobs.getDayOffJob);
router.put('/day-off-job/(:id)', verifyUser, ctrlUserDayOffJobs.putDayOffJob);
router.delete('/day-off-job/(:id)', verifyUser, ctrlUserDayOffJobs.deleteDayOffJob);

module.exports = router;
