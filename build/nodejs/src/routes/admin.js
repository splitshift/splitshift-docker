var express = require('express');
var router = express.Router();

var approve = require('../controllers/admin/approve');
var resource = require('../controllers/admin/resource');
var user = require('../controllers/admin/user');
var stat = require('../controllers/admin/stat');

var verify = require('../controllers/verify');
var verifyAdmin = verify.verifyAdmin;

// approve company to post buzz
router.put('/approve', verifyAdmin, approve.putApprove);
router.get('/resource', verifyAdmin, resource.getAllResource);
router.get('/user', verifyAdmin, user.getAllUser);
router.delete('/user/(:key)', verifyAdmin, user.deleteUser);
router.get('/metric', verifyAdmin, stat.getStat);

module.exports = router;
