var express = require('express');
var router = express.Router();

var user = require('./user');
var company = require('./company');
var admin = require('./admin');

// Controller
var ctrlAuthentication = require('../controllers/authentication');
var ctrlImageController = require('../controllers/imageController');
var ctrlProfile = require('../controllers/profile');
var ctrlSubscribe = require('../controllers/subscribe');
var ctrlUserDayOffJobs = require('../controllers/user/day_off_jobs');
var send = require('../controllers/send');
var applyJob = require('../controllers/applyjob');
var search = require('../controllers/search/search');
var search2 = require('../controllers/search/search2');
var follow = require('../controllers/follow');
var request = require('../controllers/connect/connect');
var chat = require('../controllers/chat');
var notification = require('../controllers/notification');
var resource = require('../controllers/resource');
var stat = require('../controllers/stat');
var account = require('../controllers/account');
var check_admin = require('../controllers/check_admin');
var buzz = require('../controllers/feeds/buzz');
var forward = require('../controllers/forward');
var dashboard = require('../controllers/dashboard');

require('./indexroute/image.js')(router);
require('./indexroute/feed.js')(router);
require('./indexroute/comment.js')(router);
require('./indexroute/reply.js')(router);
require('./indexroute/photo.js')(router);
require('./indexroute/file.js')(router);

var verify = require('../controllers/verify');
var verifyUser = verify.verifyUser;
var verifyCompany = verify.verifyCompany;
var allVerify = verify.verifyAll;
var verifyHR = verify.verifyHR;

router.use('/user', user);
router.use('/company', company);
router.use('/admin', admin);

router.get('/dashboard', allVerify, dashboard.getStat);

//profile
router.get('/profile/(:id)', ctrlProfile.getProfile);

// Route
router.post('/register', ctrlAuthentication.postRegister);
router.post('/register/admin', ctrlAuthentication.postRegisterAdmin);
router.post('/login',ctrlAuthentication.postLogin);
router.delete('/account', allVerify, account.deleteAccount);
router.put('/change/password', allVerify, account.changePassword);
router.get('/confirm/(:token)', account.confirmEmail);
router.put('/email', allVerify, account.putEmail);
router.post('/forgot/password', account.forgotPassword);
router.put('/forgot/password/(:token)', account.changeForgotPassword);

//image
router.put('/profileimage', ctrlImageController.putProfileImage);
router.put('/banner', ctrlImageController.putBanner);
router.put('/banner/template', allVerify, ctrlImageController.putBannerTemplate);
router.post('/upload/image', allVerify, ctrlImageController.postImage);

//Subscribe
router.post('/subscribe', ctrlSubscribe.postSubscribe);
router.get('/subscribe', verify.verifyAdmin, ctrlSubscribe.getSubscribe);
router.post('/send/subscribe', verify.verifyAdmin, send.sendEmailToSubscribe);
router.post('/send/email', verify.verifyAdmin, send.sendEmailToSingle);

//apply job
router.post('/applyjob', verifyUser, applyJob.PostApplyJob);
router.get('/applyjob/(:id)', verifyHR, applyJob.GetApplyJob);
router.get('/applyjob', verifyHR, applyJob.GetAllApplyJob);
router.get('/search/applyjob', verifyHR, applyJob.searchApplyJob);
router.put('/applyjob/(:id)/status', verifyHR, applyJob.putStatus);
router.delete('/applyjob/(:id)', verifyHR, applyJob.delApplyJob);

//DayOffJob
router.get('/day-off-job/', ctrlUserDayOffJobs.getAllDayOffJob);

//Search
router.get('/search', search2.getSearch);
router.get('/search/people', search2.getPeople);
router.get('/search/jobs', search2.getJobs);
router.get('/search/companies', search2.getCompanies);
router.get('/search/buzz', buzz.searchBuzz);

//Follow
router.put('/following', allVerify, follow.putFollow);
router.get('/following/(:user_key)', follow.getFollowing);
router.get('/follower/(:user_key)', follow.getFollowers);

//connect
router.get('/connect/(:user_key)', request.getConnect);
router.post('/connect', allVerify, request.postConnect);
router.put('/connect', allVerify, request.putRequest);
router.get('/request', allVerify, request.getAllRequest);
router.delete('/connect', allVerify, request.deleteConnect);

//Chat
router.post('/message', allVerify, chat.postChat);
router.get('/chat', allVerify, chat.getChat);
router.get('/chat/all', allVerify, chat.getAllChat);

//Notification
router.get('/notification', allVerify, notification.getNotification);
router.put('/notification/seen', allVerify, notification.putNotification);
router.delete('/notification/(:id)', allVerify, notification.delNotification);

//Resource
router.get('/resource/index', resource.getResource);
router.get('/resource', allVerify, resource.getAllResource);
router.post('/resource', allVerify, resource.postResource);
router.get('/resource/(:id)', resource.getSingleResource);
router.put('/resource/(:id)', allVerify, resource.putResource);
router.delete('/resource/(:id)', allVerify, resource.deleteResource);
router.get('/search/resource', resource.getSearchResource);
router.post('/resource/(:id)/comment', allVerify, resource.postComment);
router.delete('/resource/(:id)/comment/(:comment_id)', allVerify, resource.deleteComment);
router.put('/resource/(:id)/like', allVerify, resource.putLike);

router.get('/stat/all', stat.getStatAll);

router.get('/check_admin', check_admin);

router.post('/forward/(:type)', allVerify, forward.sendForward);

module.exports = router;
