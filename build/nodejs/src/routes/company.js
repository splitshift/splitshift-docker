var express = require('express');
var router = express.Router();

var ctrlAuthentication = require('../controllers/authentication');
var ctrlCompanyFunction = require('../controllers/company/companyFunction');
var ctrlCompanyJobs = require('../controllers/company/jobs');
var ctrlCompanyBenefits = require('../controllers/company/benefits');
var ctrlCompanyUser = require('../controllers/company/user');

var verify = require('../controllers/verify');
var verifyCompany = verify.verifyCompany;
var verifyAll = verify.verifyAll;
var verifyHR = verify.verifyHR;

//get all company for autocomplete
router.get('/all', ctrlCompanyFunction.getAll);

//company info
router.put('/info', verifyCompany, ctrlCompanyFunction.putInfo);

//company address
router.put('/address', verifyCompany, ctrlCompanyFunction.putAddress);

//company overview
router.put('/overview', verifyCompany, ctrlCompanyFunction.putOverview);

//company culture
router.put('/culture', verifyCompany, ctrlCompanyFunction.putCulture);

//company benefit
router.post('/benefit', verifyCompany, ctrlCompanyBenefits.postBenefit);
router.put('/benefit/(:id)', verifyCompany, ctrlCompanyBenefits.putBenefit);
router.delete('/benefit/(:id)', verifyCompany, ctrlCompanyBenefits.deleteBenefit);

//job
router.post('/job', verifyHR, ctrlCompanyJobs.postJob);
router.get('/job/all', verifyHR, ctrlCompanyJobs.getAllJob);
router.get('/job/(:id)', verifyHR, ctrlCompanyJobs.getJob);
router.put('/job/(:id)', verifyHR, ctrlCompanyJobs.putJob);
router.delete('/job/(:id)', verifyHR, ctrlCompanyJobs.deleteJob);
router.put('/job/(:id)/pin', verifyHR, ctrlCompanyJobs.putPin);
router.put('/job/(:id)/activate', verifyHR, ctrlCompanyJobs.putActivate);

//link
router.put('/link', verifyCompany, ctrlCompanyFunction.putLink);

//change position
router.post('/change', verifyCompany, ctrlCompanyFunction.postChangePosition);
router.post('/rank', verifyCompany, ctrlCompanyFunction.postChangeRank);

//video
router.put('/video', verifyCompany, ctrlCompanyFunction.putVideo);

//employees, formers
router.get('/(:key)/employee', ctrlCompanyUser.getEmployee);
router.get('/(:key)/former', ctrlCompanyUser.getFormer);

module.exports = router;
