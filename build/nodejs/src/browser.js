import React from 'react';
import {render} from 'react-dom';
import { Router, browserHistory} from 'react-router';

import routes from './app/routes';

let handleCreateElement = (Component, props) => {
    return <Component {...props} />;
}

render((
    <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory} createElement={handleCreateElement}>
      {routes}
    </Router>
  ), document.getElementById('root'));
