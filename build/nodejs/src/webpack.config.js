var path = require('path');
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');

var config = {
  devtool: '#eval-source-map',
  entry: [
    './browser.js',
    'webpack-dev-server/client?http://0.0.0.0:8080', // WebpackDevServer host and port
    'webpack/hot/dev-server' // "only" prevents reload on syntax errors
  ],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    })
  ],
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      loaders: ["react-hot",'babel'],
      exclude: /node_modules/,
      include: path.resolve(__dirname)
    },{
      test: /\.css$/,
      loader: 'style!css'
    }]
  },
  devServer: {
    contentBase: "./public",
    colors: true,
    historyApiFallback: true,
    port: 8080,
    host: '0.0.0.0',
    proxy: {
      '/api/**': {
        target: 'http://localhost:5000',
        secure: false
      }
    },
    headers: { 'Access-Control-Allow-Origin': '*' }
  },
  watchOptions: {
    poll: true
  }
};

if(process.env.NODE_ENV == 'production') {
  config.devtool = 'cheap-module-source-map';
  delete config['devServer'];
  config['entry'] = [
    './browser.js',
  ];
}

module.exports = config;
